﻿#if UNITY_EDITOR
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using UnityEditor;
using UnityEngine;
using System.Threading;

public class CretePreviewPng : Editor
{
    [MenuItem("Tools/Crete preview PNG")]
    public static void CreatePreview()
    {
        var selectedAsset = Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.DeepAssets);
        if (selectedAsset == null || selectedAsset.Length == 0)
            return;
        
        foreach (UnityEngine.Object obj in selectedAsset)
        {
            var assetPath = Path.GetFullPath(AssetDatabase.GetAssetPath(obj.GetInstanceID()));
            var dir = Path.GetDirectoryName(assetPath);
            var fil = Path.GetFileNameWithoutExtension(assetPath);
            var prevPathName = Path.Combine(dir,fil)+"_preview.png";

            if (System.IO.File.Exists(prevPathName))
                continue;
            
            var preview = AssetPreview.GetAssetPreview(obj);
            while (preview == null)
            {
                preview = AssetPreview.GetAssetPreview(obj);
                Thread.Sleep(80);
            }

            var data = preview.EncodeToPNG();
            File.WriteAllBytes(prevPathName, data);
        }
    }
}


#endif