﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface IBFSNode<T> where T : IBFSNode<T>
{
    int Hash { get; set; }
    bool IsWalkable { get; set; }
    
    bool Visited { get; set; }
    float Weight { get; set; }

    List<int> Neighbors { get; set; }
    List<float> ArchsWeight { get; }
}

public abstract class BFSPathFinding<T> : MonoBehaviour where T : class, IBFSNode<T>
{
    /// <summary>
    /// 
    /// </summary>
    protected abstract List<T> Graph { get; }

    /// <summary>
    /// 
    /// </summary>
    private readonly Queue<T> _queue = new Queue<T>();

    /// <summary>
    /// 
    /// </summary>
    public int NodesCount
    {
        get { return Graph == null ? 0 : Graph.Count; }
    }

    // --------------------------------------------------------------------------------------------
    // Grapth manipulation
    // --------------------------------------------------------------------------------------------
    /// <summary>
    /// 
    /// </summary>
    /// <param name="n1"></param>
    /// <param name="n2"></param>
    /// <returns></returns>
    private static int Sort(T n1, T n2)
    {
        if (n1.Hash > n2.Hash)
            return 1;
        if (n1.Hash < n2.Hash)
            return -1;
        return 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="node"></param>
    protected void AddNode(T node)
    {
        if (Graph == null || node == null)
            return;
        node.Hash = (int)Utils.GetHash();
        Graph.Add(node);
        Graph.Sort(Sort);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="node"></param>
    public void RemoveNode(T node)
    {
        if (Graph == null || node == null)
            return;
        UnlinkAll(node);
        Graph.Remove(node);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="hash"></param>
    public void RemoveNode(int hash)
    {
        if (Graph == null)
            return;

        var node = Search(hash);
        if (node == null)
            return;

        RemoveNode(node);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="node1"></param>
    /// <param name="node2"></param>
    public void Link(T node1, T node2)
    {
        if (Graph == null)
            return;

        if (node1 == null || node2 == null || node1 == node2)
            return;

        if (node1.Neighbors == null)
            node1.Neighbors = new List<int>();

        if (node2.Neighbors == null)
            node2.Neighbors = new List<int>();

        if (node1.Neighbors.Contains(node2.Hash))
            return;

        node1.Neighbors.Add(node2.Hash);
        node2.Neighbors.Add(node1.Hash);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="node1"></param>
    /// <param name="node2"></param>
    public void Unlink(T node1, T node2)
    {
        if (Graph == null)
            return;

        if (node1 == null || node2 == null || 
            node1.Neighbors == null || node1.Neighbors.Count == 0 || 
            node2.Neighbors == null || node2.Neighbors.Count == 0)
            return;

        node1.Neighbors.Remove(node2.Hash);
        node2.Neighbors.Remove(node1.Hash);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="point"></param>
    private void UnlinkAll(T point)
    {
        if (Graph == null)
            return;

        if (point == null || point.Neighbors == null || point.Neighbors.Count == 0)
            return;

        point.Neighbors.ForEach(neighborHash =>
        {
            var neightbor = Search(neighborHash);
            if (neightbor != null)
                neightbor.Neighbors.Remove(point.Hash);
        });

        point.Neighbors.Clear();
        point.Neighbors = null;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="hash"></param>
    /// <returns></returns>
    private T Search(int hash)
    {
        var lower = 0;
        var upper = Graph.Count - 1;

        while (lower <= upper)
        {
            var middle = lower + (upper - lower) / 2;

            var comparisonResult = 0;

            if (hash > Graph[middle].Hash)
                comparisonResult = 1;
            else
            if (hash < Graph[middle].Hash)
                comparisonResult = -1;

            if (comparisonResult < 0)
            {
                upper = middle - 1;
            }
            else if (comparisonResult > 0)
            {
                lower = middle + 1;
            }
            else
            {
                return Graph[middle];
            }
        }
        return Graph[~lower];
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="hash"></param>
    /// <returns></returns>
    public T GetNode(int hash)
    {
        return Search(hash);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="action"></param>
    public void ForEach(Action<T> action)
    {
        if (Graph == null)
            return;
        Graph.ForEach(action);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public T GetRandom()
    {
        return RandomHelper.Any(Graph);
    }

    // --------------------------------------------------------------------------------------------
    // Path finding
    // --------------------------------------------------------------------------------------------
    /// <summary>
    /// Calculate over nodes arch weights    
    /// Call once on initialize
    /// </summary>
    public abstract void CalculateArchsWeights();

    /// <summary>
    /// Calculate weight for node in realtime
    /// </summary>
    /// <param name="node"></param>
    /// <param name="archIndex"></param>
    public virtual float CalculateWeight(T node, int neighbourIndex)
    {
        return node.Weight + node.ArchsWeight[neighbourIndex];
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual void ResetGraph()
    {
        Graph.ForEach(n =>
        {
            n.Weight = int.MaxValue;
            n.Visited = false;
        });
    }

    /// <summary>
    /// 
    /// </summary>
    private void ProcessQueue()
    {
        while (_queue.Count > 0)
        {
            var node = _queue.Dequeue();
            node.Visited = true;

            var index = 0;
            node.Neighbors.ForEach(neighborHash =>
            {
                var child = Search(neighborHash);
                if (child!=null && child.IsWalkable)
                {
                    if (!child.Visited)
                    {
                        child.Weight = CalculateWeight(node, index);
                        _queue.Enqueue(child);
                    }
                }
                index++;
            });
        }

        if (_queue.Count > 0)
            ProcessQueue();
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    private T FindMinWeightNode(T node)
    {
        if (node.Neighbors.Count == 1)
            return Search(node.Neighbors[0]);

        var result = Search(node.Neighbors[0]);
        node.Neighbors.ForEach(neighborHash =>
        {
            var child = Search(neighborHash);

            if (!child.IsWalkable)
                return;

            if (child.Weight < result.Weight)
                result = child;
        });
        
        return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="from"></param>
    /// <param name="to"></param>
    /// <param name="path"></param>
    public void CalculatePath(T from, T to, List<T> path)
    {
        path.Clear();
        ResetGraph();

        if (from == to)
            return;

        from.Weight = 0;
        _queue.Enqueue(from);
        ProcessQueue();
        
        do
        {
            path.Insert(0, to);
            to = FindMinWeightNode(to);
        } while (to != from);

        path.Insert(0, from);
    }
}