﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class VectorPath : MonoBehaviour
{
    [SerializeField]
    public float Tolerance = 0.05f;
    [SerializeField]
    public List<Vector3> Path = new List<Vector3>();

#if UNITY_EDITOR
    public bool Selected;
#endif

    public void Add(Vector3 worldPoint)
    {
        Path.Add(worldPoint);
    }

    public void Remove(Vector3 worldPoint)
    {
        var index = PointIndex(worldPoint);
        if (index < 0)
            return;
        Remove(index);
    }

    public void Remove(int index)
    {
        Path.RemoveAt(index);
    }

    public int PointIndex(Vector3 worldPoint)
    {
        var index = 0;
        foreach (var vector3 in Path)
        {
            if (Vector3.Distance(vector3, worldPoint) <= Tolerance)
                return index;
            index++;
        }
        return -1;
    }

    public void SetPoint(int index, Vector3 worldPoint)
    {
        Path[index] = worldPoint;
    }

#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        foreach (var vector3 in Path)
            Gizmos.DrawSphere(vector3, Tolerance);

        if (Path.Count > 1)
        {
            Gizmos.color = Selected ? Color.blue : Color.yellow;
            for (var i = 1; i < Path.Count; i++)
            {
                Gizmos.DrawLine(Path[i - 1], Path[i]);
            }
        }

        /*for (int i = 0; i < Path.Count; i++)
        {
            //Cant draw between the endpoints
            //Neither do we need to draw from the second to the last endpoint
            //...if we are not making a looping line
            if (i == Path.Count - 1)
            {
                continue;
            }

            DisplayCatmullRomSpline(i);
        }*/
    }
#endif

    Vector3 ReturnCatmullRom(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        var a = 0.5f * (2f * p1);
        var b = 0.5f * (p2 - p0);
        var c = 0.5f * (2f * p0 - 5f * p1 + 4f * p2 - p3);
        var d = 0.5f * (-p0 + 3f * p1 - 3f * p2 + p3);
        var pos = a + (b * t) + (c * t * t) + (d * t * t * t);
        return pos;
    }

    //Display a spline between 2 points derived with the Catmull-Rom spline algorithm
    void DisplayCatmullRomSpline(int pos)
    {
        //Clamp to allow looping
        var p0 = Path[ClampListPos(pos - 1)];
        var p1 = Path[pos];
        var p2 = Path[ClampListPos(pos + 1)];
        var p3 = Path[ClampListPos(pos + 2)];


        //Just assign a tmp value to this
        var lastPos = Vector3.zero;

        //t is always between 0 and 1 and determines the resolution of the spline
        //0 is always at p1
        for (float t = 0; t < 1; t += 0.1f)
        {
            //Find the coordinates between the control points with a Catmull-Rom spline
            Vector3 newPos = ReturnCatmullRom(t, p0, p1, p2, p3);

            //Cant display anything the first iteration
            if (t == 0)
            {
                lastPos = newPos;
                continue;
            }

            Gizmos.DrawLine(lastPos, newPos);
            lastPos = newPos;
        }

        //Also draw the last line since it is always less than 1, so we will always miss it
        Gizmos.DrawLine(lastPos, p2);
    }


    //Clamp the list positions to allow looping
    //start over again when reaching the end or beginning
    int ClampListPos(int pos)
    {
        if (pos < 0)
        {
            pos = Path.Count - 1;
        }

        if (pos > Path.Count)
        {
            pos = 1;
        }

        else if (pos > Path.Count - 1)
        {
            pos = 0;
        }

        return pos;
    }
}

