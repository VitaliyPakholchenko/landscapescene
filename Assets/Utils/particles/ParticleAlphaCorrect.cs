﻿using UnityEngine;
using System.Collections;

public class ParticleAlphaCorrect : MonoBehaviour
{
    private Renderer _renderer;
    private ParticleSystem _particleSystem;
	// Use this for initialization
	void Start ()
	{
	    _renderer = GetComponent<Renderer>();
	    _particleSystem = GetComponentInChildren<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if (_renderer == null || _particleSystem == null)
	    {
	        DestroyImmediate(this);
            return;
	    }

        var particleList = new ParticleSystem.Particle[_particleSystem.particleCount];
        _particleSystem.GetParticles(particleList);
        for (var i = 0; i < particleList.Length; ++i)
        {
            particleList[i].startColor = new Color(particleList[i].startColor.r, particleList[i].startColor.g, particleList[i].startColor.b,
                particleList[i].startColor.a * _renderer.material.color.a);
        }

        _particleSystem.SetParticles(particleList, _particleSystem.particleCount);
	}
}
