﻿using System;
using UnityEngine;
using System.Collections.Generic;

public static class MeshExtension
{
    /// <summary>
    /// Decide whether a point is within a mesh, in a good yet simplistic way.
    /// It works best with convex meshes, whereas a concave mesh is simply
    /// treated as a convex mesh and so it will not be totally exact.
    /// (For a torus/donut mesh it will be like it didn't have a hole.)
    /// </summary>
    public static bool PointIsWithin(this Mesh mesh, Vector3 point)
    {
        var p = point - mesh.bounds.center;

        for (var i = 0; i < mesh.vertices.Length; i += 3)
        {
            var a = mesh.vertices[i] - mesh.bounds.center;
            var b = mesh.vertices[i + 1] - mesh.bounds.center;
            var c = mesh.vertices[i + 2] - mesh.bounds.center;
            if (RayWithinTriangle(p, a, b, c))
                return true;
        }
        return false;
    }

    /// <summary>
    /// Decide whether a point is within a mesh, in a good yet simplistic way.
    /// It works best with convex meshes, whereas a concave mesh is simply
    /// treated as a convex mesh and so it will not be totally exact.
    /// (For a torus/donut mesh it will be like it didn't have a hole.)
    /// </summary>
    public static bool PointIsWithin(this MeshCollider mesh, Vector3 point)
    {
        var p = point - mesh.bounds.center;
        
        for (var i = 0; i < mesh.sharedMesh.vertices.Length; i += 3)
        {
            var a = mesh.sharedMesh.vertices[i] - mesh.bounds.center;
            var b = mesh.sharedMesh.vertices[i + 1] - mesh.bounds.center;
            var c = mesh.sharedMesh.vertices[i + 2] - mesh.bounds.center;
            if (RayWithinTriangle(p, a, b, c))
                return true;
        }
        return false;
    }

    /// <summary>
    /// Radiate out from the origin through the given point to see whether
    /// the ray would hit the triangle and the point is closer to the origin than the triangle.
    /// The triangle is specified by v0, v1 and v2.
    /// </summary>
    private static bool RayWithinTriangle(Vector3 point, Vector3 v0, Vector3 v1, Vector3 v2)
    {
        Vector3 intersectionPoint;
        if (RayIntersectsTriangle(point, v0, v1, v2, out intersectionPoint))
        {
            float pointDist = point.sqrMagnitude;
            float intersectionDist = intersectionPoint.sqrMagnitude;
            return (pointDist < intersectionDist);
        }
        return false;
    }

    /// <summary>
    /// Radiate out from the origin through the given point to see whether
    /// the ray would hit the triangle.
    /// The triangle is specified by v0, v1 and v2.
    /// </summary>
    private static bool RayIntersectsTriangle(Vector3 direction, Vector3 v0, Vector3 v1, Vector3 v2, out Vector3 intersectionPoint)
    {
        intersectionPoint = new Vector3();

        Vector3 e1 = v1 - v0;
        Vector3 e2 = v2 - v0;

        Vector3 h = Vector3.Cross(direction, e2);
        float a = Vector3.Dot(e1, h);

        if (a > -0.00001 && a < 0.00001)
            return false;

        float f = 1 / a;
        Vector3 s = Vector3.zero - v0;
        float u = f * Vector3.Dot(s, h);

        if (u < 0.0 || u > 1.0)
            return false;

        Vector3 q = Vector3.Cross(s, e1);
        float v = f * Vector3.Dot(direction, q);

        if (v < 0.0 || u + v > 1.0)
            return false;

        // At this stage we can compute t to find out where
        // the intersection point is on the line.
        float t = f * Vector3.Dot(e2, q);

        if (t > 0.00001) // ray intersection
        {
            intersectionPoint[0] = direction[0] * t;
            intersectionPoint[1] = direction[1] * t;
            intersectionPoint[2] = direction[2] * t;
            return true;
        }

        // At this point there is a line intersection
        // but not a ray intersection.
        return false;
    }

    /// <summary>
    /// Decide whether a point is within a mesh, in a good yet simplistic way.
    /// It works best with convex meshes, whereas a concave mesh is simply
    /// treated as a convex mesh and so it will not be totally exact.
    /// (For a torus/donut mesh it will be like it didn't have a hole.)
    /// </summary>
    public static Vector3 RandomPoint(this MeshCollider mesh)
    {
        var centers = new List<Vector3>();
        for (var i = 0; i < mesh.sharedMesh.vertices.Length; i += 3)
        {
            var a = mesh.sharedMesh.vertices[i] - mesh.bounds.center;
            var b = mesh.sharedMesh.vertices[i + 1] - mesh.bounds.center;
            var c = mesh.sharedMesh.vertices[i + 2] - mesh.bounds.center;

            var d = (a + b + c)/3f;
            centers.Add(d);
        }
        return RandomHelper.Any(centers) + mesh.bounds.center;
    }
}