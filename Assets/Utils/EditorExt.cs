﻿#if UNITY_EDITOR

using System;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class EditorExt
{
    public static void EditorListLayout(string fieldName, List<string> list)
    {
        EditorGUILayout.BeginVertical();
        GUILayout.Label(fieldName, EditorStyles.boldLabel);
        var delIndex = -1;
        for (var i = 0; i < list.Count; i++)
        {
            
            EditorGUILayout.BeginHorizontal();
            list[i] = EditorGUILayout.TextField(list[i]);
            if (GUILayout.Button("Del"))
                delIndex = i;
            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.EndVertical();
        if (GUILayout.Button("Add " + fieldName))
            list.Add(string.Empty);

        if (delIndex >= 0)
            list.RemoveAt(delIndex);
    }

    public static bool EditorListPopupLayout(string fieldName, string valueName, List<string> list, List<string> values)
    {
        if (values == null || values.Count == 0)
            return false;

        EditorGUILayout.BeginVertical();
        GUILayout.Label(fieldName, EditorStyles.boldLabel);

        var changed = false;
        var delIndex = -1;
        for (var i = 0; i < list.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();

            var index = values.IndexOf(list[i]);
            if (index < 0)
                index = 0;

            var indexOld = index;

            index = EditorGUILayout.Popup(valueName, index, values.ToArray());
            list[i] = values[index];

            if (indexOld != index)
                changed = true;

            if (GUILayout.Button("Del"))
                delIndex = i;
            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.EndVertical();
        if (GUILayout.Button("Add " + valueName))
        {
            list.Add(string.Empty);
            changed = true;
        }

        if (delIndex >= 0)
        {
            list.RemoveAt(delIndex);
            changed = true;
        }

        return changed;
    }

    public static void EditorListLayout(string fieldName, List<int> list)
    {
        GUILayout.Label(fieldName, EditorStyles.boldLabel);
        var delIndex = -1;
        for (var i = 0; i < list.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            list[i] = EditorGUILayout.IntField(list[i]);
            if (GUILayout.Button("Del"))
                delIndex = i;
            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Add " + fieldName))
            list.Add(0);

        if (delIndex >= 0)
            list.RemoveAt(delIndex);
    }

    public static void EditorListLayout(string fieldName, ref List<GameObject> list)
    {
        GUILayout.Label(fieldName, EditorStyles.boldLabel);
        var delIndex = -1;
        for (var i = 0; i < list.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            list[i] = (GameObject)EditorGUILayout.ObjectField(list[i], typeof(GameObject), false);
            if (GUILayout.Button("Del"))
                delIndex = i;
            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Add " + fieldName))
            list.Add(null);

        if (delIndex >= 0)
            list.RemoveAt(delIndex);
    }

    public static void EditorListLayout(string fieldName, List<AudioClip> list)
    {
        GUILayout.Label(fieldName, EditorStyles.boldLabel);
        var delIndex = -1;
        for (var i = 0; i < list.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            list[i] = (AudioClip)EditorGUILayout.ObjectField(list[i], typeof(AudioClip), false);
            if (GUILayout.Button("Del"))
                delIndex = i;
            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Add " + fieldName))
            list.Add(null);

        if (delIndex >= 0)
            list.RemoveAt(delIndex);
    }

   
    public static void EditorArrayLayout(string fieldName, float[] list)
    {
        GUILayout.Label(fieldName, EditorStyles.boldLabel);
        for (var i = 0; i < list.Length; i++)
        {
            EditorGUILayout.BeginHorizontal();
            list[i] = EditorGUILayout.FloatField(list[i]);
            EditorGUILayout.EndHorizontal();
        }
    }
    
    public static void CheckFolder(string path)
    {
        var checkpath = Path.Combine(Application.dataPath, path);
        if (Directory.Exists(checkpath))
            return;
        Directory.CreateDirectory(checkpath);
    }

    public static void DrawLine(Vector3 p1, Vector3 p2, float width)
    {
        int count = Mathf.CeilToInt(width); // how many lines are needed.
        if (count == 1)
            Gizmos.DrawLine(p1, p2);
        else
        {
            var view = SceneView.currentDrawingSceneView;

            Camera c = Camera.current;
            if (c == null)
            {
                Debug.LogError("Camera.current is null");
                return;
            }

            Vector3 v1 = (p2 - p1).normalized; // line direction
            Vector3 v2 = (view.camera.transform.position - p1).normalized; // direction to camera
            Vector3 n = Vector3.Cross(v1, v2); // normal vector
            for (int i = 0; i < count; i++)
            {
                Vector3 o = n * ((float)i / (count - 1) - 0.5f);
                Gizmos.DrawLine(p1 + o, p2 + o);
            }
        }
    }

    public void DrawWireDisc(Vector3 position, Vector3 normal, float radius, Color color)
    {
        Handles.color = color;
        Handles.DrawWireDisc(position, normal, radius);
    }

    public static Vector3 GroundHeight(Vector3 v, string tag)
    {
        var ground = new RaycastHit();
        if (string.IsNullOrEmpty(tag))
        {
            if (Physics.Raycast(v, -Vector3.up, out ground, Mathf.Infinity))
                v.y = ground.point.y;

            if (Physics.Raycast(v, Vector3.up, out ground, Mathf.Infinity))
                v.y = ground.point.y;

            // try casting from really high up next
            if (Physics.Raycast(v + Vector3.up * 1000f, -Vector3.up, out ground))
                v.y = ground.point.y;
        }
        else
        {
            if (Physics.Raycast(v, -Vector3.up, out ground, Mathf.Infinity) && ground.transform.tag == tag)
                v.y = ground.point.y;

            if (Physics.Raycast(v, Vector3.up, out ground, Mathf.Infinity) && ground.transform.tag == tag)
                v.y = ground.point.y;

            // try casting from really high up next
            if (Physics.Raycast(v + Vector3.up * 1000f, -Vector3.up, out ground) && ground.transform.tag == tag)
                v.y = ground.point.y;
        }
        return v;
    }
    
    [MenuItem("GameObject/ToGround %g")]
    private static void ToGround()
    {
        var go = Selection.activeObject as GameObject;
        if (go == null)
            return;
        go.transform.position = GroundHeight(go.transform.position, string.Empty);
    }

    public static SceneView FindSceneView()
    {
        return SceneView.lastActiveSceneView ?? EditorWindow.GetWindow<SceneView>();
    }

    /// <summary>
    //	This makes it easy to create, name and place unique new ScriptableObject asset files.
    /// </summary>
    public static void CreatePrefab<T>(string pathName) where T : MonoBehaviour
    {
        var object_ = new GameObject();
        object_.name = Path.GetFileName(pathName);
        object_.AddComponent<T>();

        if (pathName == "")
            pathName = "Assets/" + typeof(T).Name;
        pathName += ".prefab";

        PrefabUtility.CreatePrefab(pathName, object_);
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = object_;
        GameObject.DestroyImmediate(object_);
    }

    public static void CreateAsset<T>(string pathname) where T : ScriptableObject
    {
        var asset = ScriptableObject.CreateInstance<T>();
        if (pathname == "")
            pathname = "Assets/" + typeof(T).Name;
        pathname += ".asset";
        AssetDatabase.CreateAsset(asset, pathname);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    public static void CreateAsset(Type type, string pathname)
    {
        var asset = ScriptableObject.CreateInstance(type);

        if (pathname == "")
            pathname = "Assets/" + type.Name;
        pathname += ".asset";

        AssetDatabase.CreateAsset(asset, pathname);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}




#endif