﻿using System.Collections.Generic;
using UnityEngine;


public enum OptionType { OPTION_BOOL, OPTION_INT, OPTION_STRING, OPTION_KEY };

public class OptionInfo
{
    public string _id, _desc, _cat;
    public OptionType _type;
    public bool b_ref;
    public int i_ref;
    public string s_ref;
    public KeyCode k_ref;


    public OptionInfo(string id, bool option, string desc, string cat)
    {
        _id = id;
        _desc = desc;
        _cat = cat;
        _type = OptionType.OPTION_BOOL;
        b_ref = option;
    }

    public OptionInfo(string id, KeyCode option, string desc, string cat)
    {
        _id = id;
        _desc = desc;
        _cat = cat;
        _type = OptionType.OPTION_KEY;
        k_ref = option;
    }

    public OptionInfo(string id, string option, string desc, string cat)
    {
        _id = id;
        _desc = desc;
        _cat = cat;
        _type = OptionType.OPTION_KEY;
        s_ref = option;
    }

    /**
     * Loads an option value from the corresponding YAML.
     * @param node Options YAML node.
     */

    public void load(string prefix)
    {
        /*switch (_type)
	{
	case OPTION_BOOL:
        *(_ref.b) = node[_id].as<bool>(_def.b);
		break;
	case OPTION_INT:
        *(_ref.i) = node[_id].as<int>(_def.i);
		break;
	case OPTION_KEY:
        *(_ref.k) = (SDLKey)node[_id].as<int>(_def.k);
		break;
	case OPTION_STRING:
        *(_ref.s) = node[_id].as<std::string>(_def.s);
		break;
	}*/
    }

    public void load(Dictionary<string, string> map)
    {
        /*string id = _id;
    std::transform(id.begin(), id.end(), id.begin(), ::tolower);
	std::map<std::string, std::string>::const_iterator it = map.find(id);
	if (it != map.end())
	{
		std::string value = it.second;
std::stringstream ss;
bool b;
int i;
		switch (_type)
		{
		case OPTION_BOOL:
			ss << std::boolalpha << value;
			ss >> std::boolalpha >> b;
            *(_ref.b) = b;
			break;
		case OPTION_INT:
			ss << std::dec << value;
			ss >> std::dec >> i;
            *(_ref.i) = i;
			break;
		case OPTION_KEY:
			ss << std::dec << value;
			ss >> std::dec >> i;
            *(_ref.k) = (SDLKey)i;
			break;
		case OPTION_STRING:
            *(_ref.s) = value;
			break;
		}
	}*/
    }

    public void save()
    {
        /*switch (_type)
	{
	case OPTION_BOOL:
		node[_id] = *(_ref.b);
		break;
	case OPTION_INT:
		node[_id] = *(_ref.i);
		break;
	case OPTION_KEY:
		node[_id] = (int)*(_ref.k);
		break;
	case OPTION_STRING:
		node[_id] = *(_ref.s);
		break;
	}*/
    }

/**
 * Resets an option back to its default value.
 */

    public void reset()
    {
        /*switch (_type)
	{
	case OPTION_BOOL:
        *(_ref.b) = _def.b;
		break;
	case OPTION_INT:
        *(_ref.i) = _def.i;
		break;
	case OPTION_KEY:
        *(_ref.k) = _def.k;
		break;
	case OPTION_STRING:
        *(_ref.s) = _def.s;
		break;
	}*/
    }
}
