﻿using System.Collections.Generic;
using System.Linq;

public static class Extentions
{
    public static void Resize<T>(ref IList<T> list, int size)
    {
        if (size > list.Count)
            for (var i = 0; i <= size - list.Count; i++)
                list.Add(default(T));
        else if (size < list.Count)
            for (var i = 0; i <= list.Count - size; i++)
                list.RemoveAt(list.Count - 1);
    }

    public static IEnumerable<T> GetReverse<T>(this IList<T> list)
    {
        for (var i = list.Count - 1; i >= 0; i--)
            yield return list[i];
    }

    public static IEnumerable<T> ReverseEx<T>(this IEnumerable<T> coll)
    {
        var quick = coll as IList<T>;
        if (quick == null)
        {
            foreach (var item in coll.Reverse()) yield return item;
        }
        else
        {
            for (var ix = quick.Count - 1; ix >= 0; --ix)
            {
                yield return quick[ix];
            }
        }
    }
}

