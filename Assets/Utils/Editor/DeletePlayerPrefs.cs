﻿using UnityEditor;
using UnityEngine;

public class DeletePlayerPrefs : Editor
{
      [MenuItem("Tools/DeletePlayerPrefs")]
		static void DoDeselect() {
            if (EditorUtility.DisplayDialog("Delete all editor preferences?",
                "Are you sure you want to delete all the editor preferences?, this action cannot be undone.",
                "Yes",
                "No"))
            PlayerPrefs.DeleteAll();

        }
}
