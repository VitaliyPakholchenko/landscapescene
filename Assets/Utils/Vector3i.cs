﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum DirectionTypes
{
    MiddleMiddleBask,
    RightMiddleBask,
    RightMiddleMiddle,
    RightMiddleForward,
    MiddleMiddleForward,
    LeftMiddleForward,
    LeftMiddleMiddle,
    LeftMiddleBask,
    LeftDownBask,
    LeftUpBask,
    LeftDownMiddle,
    LeftUpMiddle,
    LeftDownForward,
    LeftUpForward,
    MiddleDownBask,
    MiddleUpBask,
    MiddleDownMiddle,
    MiddleUpMiddle,
    MiddleDownForward,
    MiddleUpForward,
    RightDownBask,
    RightUpBask,
    RightDownMiddle,
    RightUpMiddle,
    RightDownForward,
    RightUpForward,
    MiddleMiddleMiddle
};

[System.Serializable]
public struct Vector3i
{
    public int x;
    public int y;
    public int z;

    public Vector3i(int newX, int newY, int newZ)
    {
        x = newX;
        y = newY;
        z = newZ;
    }

    public Vector3i(float newX, float newY, float newZ)
    {
        x = (int)newX;
        y = (int)newY;
        z = (int)newZ;
    }

    public Vector3i(Vector3 vector)
    {
        x = (int)vector.x;
        y = (int)vector.y;
        z = (int)vector.z;
    }

    public Vector3i(Vector3i vector)
    {
        x = vector.x;
        y = vector.y;
        z = vector.z;
    }

    public override string ToString()
    {
        return string.Format("({0}, {1}, {2})", x, y, z);
    }

    public Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }

    public Vector3i Clone()
    {
        return new Vector3i(x, y, z);
    }

    public static Vector3i operator *(Vector3i left, Vector3i right)
    {
        return new Vector3i(left.x * right.x, left.y * right.y, left.z * right.z);
    }

    public static Vector3i operator /(Vector3i left, Vector3i right)
    {
        return new Vector3i(left.x / right.x, left.y / right.y, left.z / right.z);
    }

    public static Vector3i operator *(Vector3i left, float f)
    {
        return new Vector3i(left.x * f, left.y * f, left.z * f);
    }

    public static Vector3i operator +(Vector3i left, Vector3i right)
    {
        return new Vector3i(left.x + right.x, left.y + right.y, left.z + right.z);
    }

    public static Vector3i operator +(Vector3i left, Vector3 right)
    {
        return new Vector3i(left.x + right.x, left.y + right.y, left.z + right.z);
    }

    public static Vector3i operator +(Vector3i left, int i)
    {
        return new Vector3i(left.x + i, left.y + i, left.z + i);
    }

    public static Vector3i operator -(Vector3i left, Vector3i right)
    {
        return new Vector3i(left.x - right.x, left.y - right.y, left.z - right.z);
    }

    public static Vector3i operator /(Vector3i numerator, float denominator)
    {
        return new Vector3i(numerator.x / denominator, numerator.y / denominator, numerator.z / denominator);
    }

    public static Vector3i operator /(Vector3i numerator, int denominator)
    {
        return new Vector3i(numerator.x / denominator, numerator.y / denominator, numerator.z / denominator);
    }

    public static bool operator ==(Vector3i left, Vector3i right)
    {
        return left.x == right.x && left.y == right.y && left.z == right.z;
    }

    public static bool operator !=(Vector3i left, Vector3i right)
    {
        return left.x != right.x || left.y != right.y || left.z != right.z;
    }

    public static Vector3i RoundFromVector3(Vector3 vector)
    {
        return new Vector3i(Mathf.Round(vector.x), Mathf.Round(vector.y), Mathf.Round(vector.z));
    }

    public static Vector3i Negate(Vector3i vector)
    {
        return new Vector3i(-vector.x, -vector.y, -vector.z);
    }
    
    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return x ^ (y << 8) ^ (z << 16);
    }

    /// <summary>
    /// Calculates the distance squared between 2 points. No sqrt(), not floating point math, and sometimes it's all you need.
    /// </summary>
    /// <param name="pos1">Position of first square.</param>
    /// <param name="pos2">Position of second square.</param>
    /// <param name="considerZ">considerZ Whether to consider the z coordinate.</param>
    /// <returns>Distance</returns>
    public static int DistanceSq(Vector3i pos1, Vector3i pos2, bool considerZ=true)
    {
        var x = pos1.x - pos2.x;
        var y = pos1.y - pos2.y;
        var z = considerZ ? (pos1.z - pos2.z) : 0;
        return x * x + y * y + z * z;
    }

    public Vector3i Up { get { return new Vector3i(x, y + 1, z); } }
    public Vector3i Down { get { return new Vector3i(x, y - 1, z); } }
    public Vector3i Left { get { return new Vector3i(x - 1, y, z); } }
    public Vector3i Right { get { return new Vector3i(x + 1, y, z); } }
    public Vector3i Front { get { return new Vector3i(x, y, z + 1); } }
    public Vector3i Back { get { return new Vector3i(x, y, z - 1); } }

    public static Vector3i zero = new Vector3i(0, 0, 0);
    public static Vector3i one = new Vector3i(1, 1, 1);
    public static Vector3i oneTileNorth = new Vector3i(0, -1, 0);
    public static Vector3i oneTileEast = new Vector3i(1, 0, 0);
    public static Vector3i oneTileSouth = new Vector3i(0, 1, 0);
    public static Vector3i oneTileWest = new Vector3i(-1, 0, 0);

    public static readonly Dictionary<DirectionTypes, Vector3i> Deltas = new Dictionary<DirectionTypes, Vector3i>
    {
        {DirectionTypes.LeftUpBask, new Vector3i(-1, 1, -1)},
        {DirectionTypes.LeftUpMiddle, new Vector3i(-1, 1, 0)},
        {DirectionTypes.LeftUpForward, new Vector3i(-1, 1, 1)},
        {DirectionTypes.MiddleUpBask, new Vector3i(0, 1, -1)},
        {DirectionTypes.MiddleUpMiddle, new Vector3i(0, 1, 0)},
        {DirectionTypes.MiddleUpForward, new Vector3i(0, 1, 1)},
        {DirectionTypes.RightUpBask, new Vector3i(1, 1, -1)},
        {DirectionTypes.RightUpMiddle, new Vector3i(1, 1, 0)},
        {DirectionTypes.RightUpForward, new Vector3i(1, 1, 1)},

        {DirectionTypes.LeftMiddleBask, new Vector3i(-1, 0, -1)},
        {DirectionTypes.LeftMiddleMiddle, new Vector3i(-1, 0, 0)},
        {DirectionTypes.LeftMiddleForward, new Vector3i(-1, 0, 1)},
        {DirectionTypes.MiddleMiddleBask, new Vector3i(0, 0, -1)},
        {DirectionTypes.MiddleMiddleForward, new Vector3i(0, 0, 1)},
        {DirectionTypes.RightMiddleBask, new Vector3i(1, 0, -1)},
        {DirectionTypes.RightMiddleMiddle, new Vector3i(1, 0, 0)},
        {DirectionTypes.RightMiddleForward, new Vector3i(1, 0, 1)},

        {DirectionTypes.LeftDownBask, new Vector3i(-1, -1, -1)},
        {DirectionTypes.LeftDownForward, new Vector3i(-1, -1, 1)},
        {DirectionTypes.LeftDownMiddle, new Vector3i(-1, -1, 0)},
        {DirectionTypes.MiddleDownBask, new Vector3i(0, -1, -1)},
        {DirectionTypes.MiddleDownMiddle, new Vector3i(0, -1, 0)},
        {DirectionTypes.MiddleDownForward, new Vector3i(0, -1, 1)},
        {DirectionTypes.RightDownBask, new Vector3i(1, -1, -1)},
        {DirectionTypes.RightDownMiddle, new Vector3i(1, -1, 0)},
        {DirectionTypes.RightDownForward, new Vector3i(1, -1, 1)},

        {DirectionTypes.MiddleMiddleMiddle, new Vector3i(0, 0, 0)},
    };

    public static int MiddleMiddleBask
    {
        get { return (int) DirectionTypes.MiddleDownBask; }
    }
    public static int RightMiddleBask
    {
        get { return (int)DirectionTypes.RightMiddleBask; }
    }
    public static int RightMiddleMiddle
    {
        get { return (int)DirectionTypes.RightMiddleMiddle; }
    }
    public static int RightMiddleForward
    {
        get { return (int)DirectionTypes.RightMiddleForward; }
    }
    public static int MiddleMiddleForward
    {
        get { return (int)DirectionTypes.MiddleMiddleForward; }
    }
    public static int LeftMiddleForward
    {
        get { return (int)DirectionTypes.LeftMiddleForward; }
    }
    public static int LeftMiddleMiddle
    {
        get { return (int)DirectionTypes.LeftMiddleMiddle; }
    }
    public static int LeftMiddleBask
    {
        get { return (int)DirectionTypes.LeftMiddleBask; }
    }
    public static int LeftDownBask
    {
        get { return (int)DirectionTypes.LeftDownBask; }
    }
    public static int LeftUpBask
    {
        get { return (int)DirectionTypes.LeftUpBask; }
    }
    public static int LeftDownMiddle
    {
        get { return (int)DirectionTypes.LeftDownMiddle; }
    }
    public static int LeftUpMiddle
    {
        get { return (int)DirectionTypes.LeftUpMiddle; }
    }
    public static int LeftDownForward
    {
        get { return (int)DirectionTypes.LeftDownForward; }
    }
    public static int LeftUpForward
    {
        get { return (int)DirectionTypes.LeftUpForward; }
    }
    public static int MiddleDownBask
    {
        get { return (int)DirectionTypes.MiddleDownBask; }
    }
    public static int MiddleUpBask
    {
        get { return (int)DirectionTypes.MiddleUpBask; }
    }
    public static int MiddleDownMiddle
    {
        get { return (int)DirectionTypes.MiddleDownMiddle; }
    }
    public static int MiddleUpMiddle
    {
        get { return (int)DirectionTypes.MiddleUpMiddle; }
    }
    public static int MiddleDownForward
    {
        get { return (int)DirectionTypes.MiddleDownForward; }
    }
    public static int MiddleUpForward
    {
        get { return (int)DirectionTypes.MiddleUpForward; }
    }
    public static int RightDownBask
    {
        get { return (int)DirectionTypes.RightDownBask; }
    }
    public static int RightUpBask
    {
        get { return (int)DirectionTypes.RightUpBask; }
    }
    public static int RightDownMiddle
    {
        get { return (int)DirectionTypes.RightDownMiddle; }
    }
    public static int RightUpMiddle
    {
        get { return (int)DirectionTypes.RightUpMiddle; }
    }
    public static int RightDownForward
    {
        get { return (int)DirectionTypes.RightDownForward; }
    }
    public static int RightUpForward
    {
        get { return (int)DirectionTypes.RightUpForward; }
    }
    public static int MiddleMiddleMiddle
    {
        get { return (int)DirectionTypes.MiddleMiddleMiddle; }
    }
    
    public static Vector3i GetDelta(DirectionTypes value)
    {
        return Deltas[value];
    }

    public static DirectionTypes GetValue(Vector3i delta)
    {
        return DirectionTypes.RightMiddleBask;
    }

    public static DirectionTypes GetType(int value)
    {
        return (DirectionTypes)value;
    }

    public static DirectionTypes GetDirection(Vector3i begin, Vector3i end)
    {
        var dirx = end.x - begin.x;
        var diry = end.y - begin.y;

        /*if (Mathf.Approximately(dirx, 0) && diry < 0)
            return DirectionTypes.MiddleUp;
        if (dirx > 0 && diry < 0)
            return DirectionTypes.MiddleUpRight;
        if (dirx > 0 && Mathf.Approximately(diry, 0))
            return DirectionTypes.MiddleRight;
        if (dirx > 0 && diry > 0)
            return DirectionTypes.MiddleDownRight;
        if (Mathf.Approximately(dirx, 0) && diry > 0)
            return DirectionTypes.MiddleDown;
        if (dirx < 0 && diry > 0)
            return DirectionTypes.MiddleDownLeft;
        if (dirx < 0 && Mathf.Approximately(diry, 0))
            return DirectionTypes.MiddleLeft;
        if (dirx < 0 && diry < 0)
            return DirectionTypes.MiddleUpLeft;*/

        return DirectionTypes.RightMiddleBask;
    }

    public static DirectionTypes Inverse(DirectionTypes value)
    {
       /* switch (value)
        {
            case DirectionTypes.MiddleUp:
                return DirectionTypes.MiddleDown;
            case DirectionTypes.MiddleUpRight:
                return DirectionTypes.MiddleDownLeft;
            case DirectionTypes.MiddleRight:
                return DirectionTypes.MiddleLeft;
            case DirectionTypes.MiddleDownRight:
                return DirectionTypes.MiddleUpLeft;
            case DirectionTypes.MiddleDown:
                return DirectionTypes.MiddleUp;
            case DirectionTypes.MiddleDownLeft:
                return DirectionTypes.MiddleUpRight;
            case DirectionTypes.MiddleLeft:
                return DirectionTypes.MiddleRight;
            case DirectionTypes.MiddleUpLeft:
                return DirectionTypes.MiddleDownRight;
            case DirectionTypes.MiddleMiddle:
                return DirectionTypes.MiddleMiddle;
        }*/
        return DirectionTypes.RightMiddleBask;
    }
}