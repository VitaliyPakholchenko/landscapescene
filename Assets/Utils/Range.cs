﻿    using System;
    using System.Globalization;
    using UnityEngine;

    public struct Range : IEquatable<Range>, IFormattable
    {
		public Single Minimum;
		public Single Maximum;

        public Range(Single minimum, Single maximum)
        {
            this.Minimum = minimum;
            this.Maximum = maximum;
        }

		public Single Size
		{
			get { return Mathf.Abs(Maximum - Minimum); }
		}
        
        static public Range Parse(String value)
        {
            return Range.Parse(value, CultureInfo.InvariantCulture);
        }
        
        static public Range Parse(String value, IFormatProvider format)
        {
            if (!value.StartsWith("[") || !value.EndsWith("]"))
                goto badformat;

            var numberFormat = NumberFormatInfo.GetInstance(format);

            var groupSeperator = numberFormat.NumberGroupSeparator.ToCharArray();

            var endpoints = value.Trim(new char[] { '[', ']' }).Split(groupSeperator);

            if (endpoints.Length != 2)
                goto badformat;

            return new Range
            {
                Minimum = Single.Parse(endpoints[0], NumberStyles.Float, numberFormat),
                Maximum = Single.Parse(endpoints[1], NumberStyles.Float, numberFormat)
            };

        badformat:
            throw new FormatException("Value is not in ISO 31-11 format for a closed interval.");
        }

        public override Boolean Equals(System.Object obj)
        {
            if (obj != null)
                if (obj is Range)
                    return this.Equals((Range)obj);

            return false;
        }

        public Boolean Equals(Range value)
        {
            return this.Minimum.Equals(value.Minimum) &&
                   this.Maximum.Equals(value.Maximum);
        }

        public override Int32 GetHashCode()
        {
            return this.Minimum.GetHashCode() ^ this.Maximum.GetHashCode();
        }

        public override String ToString()
        {
            return this.ToString("G", CultureInfo.InvariantCulture);
        }

        public String ToString(IFormatProvider formatProvider)
        {
            return this.ToString("G", formatProvider);
        }

        public String ToString(String format, IFormatProvider formatProvider)
        {
            NumberFormatInfo numberFormat = NumberFormatInfo.GetInstance(formatProvider);

            String minimum = this.Minimum.ToString(format, numberFormat);
            String maximum = this.Maximum.ToString(format, numberFormat);
            String seperator = numberFormat.NumberGroupSeparator;

            return String.Format(formatProvider, "[{0}{1}{2}]", minimum, seperator, maximum);
        }

        static public Boolean operator ==(Range x, Range y)
        {
            return x.Equals(y);
        }

        static public Boolean operator !=(Range x, Range y)
        {
            return !x.Equals(y);
        }

        static public implicit operator Range(Single value)
        {
            return new Range
            {
                Minimum = value,
                Maximum = value
            };
        }
    }
