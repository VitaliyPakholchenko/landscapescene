﻿using System;
using UnityEngine;

public class СhaseMover : MonoBehaviour
{
    private Transform _destination;
    private float _speed;

    public event Action<GameObject, Transform> OnCatch;

    public void Catch()
    {
        if (OnCatch != null)
            OnCatch(gameObject, _destination);
        OnCatch = null;
        Destroy(this);
    }

    public void Update()
    {
        if (_destination == null)
        {
            Catch();
            return;
        }

        var delta = _destination.position - transform.position;
        var ndelta = Vector3.Normalize(delta);
        transform.position += ndelta * (Time.deltaTime*_speed);

        var distance = Vector3.Distance(transform.position, _destination.position);
        if (distance < 0.2f)
            Catch();
    }

    public static СhaseMover Create(GameObject moved, Transform destination, float speed)
    {
        var cm = moved.AddComponent<СhaseMover>();
        cm._destination = destination;
        cm._speed = speed;
        return cm;
    }
}

