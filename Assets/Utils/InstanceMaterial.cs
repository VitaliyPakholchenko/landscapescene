﻿using UnityEngine;

[RequireComponent(typeof (MeshRenderer))]
public class InstanceMaterial : MonoBehaviour
{
    public void Start()
    {
        var mr = GetComponent<MeshRenderer>();
        mr.material = new Material(mr.material);
    }
}
