﻿using UnityEngine;

public abstract class SingletonBehaviour<T> : MonoBehaviour where T : SingletonBehaviour<T>
{
    public static T Instance { get; private set; }

    public static void Create()
    {
        var sobject = new GameObject(typeof(T).Name);
        Instance = sobject.AddComponent<T>();
        Instance.Initialize();
    }

    public abstract void Initialize();
}