﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum DirectionTypes2D
{
    Up = 0,
    UpRight = 1,
    Right = 2,
    DownRight = 3,
    Down = 4,
    DownLeft = 5,
    Left = 6,
    UpLeft = 7,
    Middle = 8,    
};

[Serializable]
public struct Vector2i
{
    [SerializeField] public int X;
    [SerializeField] public int Y;

    public Vector2i(int x, int y)
    {
        X = x;
        Y = y;
    }

    public static Vector2i Zero
    {
        get { return new Vector2i(0, 0); }
    }

    private static readonly Dictionary<DirectionTypes2D, Vector2i> _deltas = new Dictionary<DirectionTypes2D, Vector2i>
    {
        {DirectionTypes2D.Up, new Vector2i(0, -1)},
        {DirectionTypes2D.UpRight, new Vector2i(1, -1)},
        {DirectionTypes2D.Right, new Vector2i(1, 0)},
        {DirectionTypes2D.DownRight, new Vector2i(1, 1)},
        {DirectionTypes2D.Down, new Vector2i(0, 1)},
        {DirectionTypes2D.DownLeft, new Vector2i(-1, 1)},
        {DirectionTypes2D.Left, new Vector2i(-1, 0)},
        {DirectionTypes2D.UpLeft, new Vector2i(-1, -1)},
        {DirectionTypes2D.Middle, new Vector2i(0, 0)},
    };

    public static int Up
    {
        get { return (int) DirectionTypes2D.Up; }
    }

    public static int UpRight
    {
        get { return (int) DirectionTypes2D.UpRight; }
    }

    public static int Right
    {
        get { return (int) DirectionTypes2D.Right; }
    }

    public static int DownRight
    {
        get { return (int) DirectionTypes2D.DownRight; }
    }

    public static int Down
    {
        get { return (int) DirectionTypes2D.Down; }
    }

    public static int DownLeft
    {
        get { return (int) DirectionTypes2D.DownLeft; }
    }

    public static int Left
    {
        get { return (int) DirectionTypes2D.Left; }
    }

    public static int UpLeft
    {
        get { return (int) DirectionTypes2D.UpLeft; }
    }

    public static int Middle
    {
        get { return (int) DirectionTypes2D.Middle; }
    }

    public static Vector2i GetDelta(DirectionTypes2D value)
    {
        return _deltas[value];
    }
    
    public static Vector2i operator +(Vector2i left, Vector2i right)
    {
        return new Vector2i(left.X + right.X, left.Y + right.Y);
    }

    public static Vector2i operator +(Vector2i left, int i)
    {
        return new Vector2i(left.X + i, left.Y + i);
    }

    public static Vector2i operator -(Vector2i left, Vector2i right)
    {
        return new Vector2i(left.X - right.X, left.Y - right.Y);
    }

    public static bool operator ==(Vector2i left, Vector2i right)
    {
        return left.X == right.X && left.Y == right.Y;
    }

    public static bool operator !=(Vector2i left, Vector2i right)
    {
        return left.X != right.X || left.Y != right.Y;
    }

    public override bool Equals(object obj)
    {
        return base.Equals(obj);
    }

    public override int GetHashCode()
    {
        return X ^ (Y << 8);
    }

    public static DirectionTypes2D GetDirection(Vector2i begin, Vector2i end)
    {
        var dirx = end.X - begin.X;
        var diry = begin.Y - end.Y;

        if (Mathf.Approximately(dirx, 0) && diry < 0)
            return DirectionTypes2D.Down;
        if (dirx > 0 && diry < 0)
            return DirectionTypes2D.UpRight;
        if (dirx > 0 && Mathf.Approximately(diry, 0))
            return DirectionTypes2D.Right;
        if (dirx > 0 && diry > 0)
            return DirectionTypes2D.DownRight;
        if (Mathf.Approximately(dirx, 0) && diry > 0)
            return DirectionTypes2D.Up;
        if (dirx < 0 && diry > 0)
            return DirectionTypes2D.DownLeft;
        if (dirx < 0 && Mathf.Approximately(diry, 0))
            return DirectionTypes2D.Left;
        if (dirx < 0 && diry < 0)
            return DirectionTypes2D.UpLeft;

        return DirectionTypes2D.Middle;
    }

    public static Vector2i GetDestination(Vector2i position, DirectionTypes2D direction)
    {
        switch(direction)
        {
            case DirectionTypes2D.Up:
                return position + new Vector2i(0, -1);
            case DirectionTypes2D.UpRight:
                return position + new Vector2i(1, -1);                
            case DirectionTypes2D.Right:
                return position + new Vector2i(1, 0);
            case DirectionTypes2D.DownRight:
                return position + new Vector2i(1, 1);
            case DirectionTypes2D.Down:
                return position + new Vector2i(0, 1);                
            case DirectionTypes2D.DownLeft:
                return position + new Vector2i(-1, 1);                
            case DirectionTypes2D.Left:
                return position + new Vector2i(-1, 0);                
            case DirectionTypes2D.UpLeft:
                return position + new Vector2i(-1, -1);                            
        }
        return position;
    }

    public static DirectionTypes2D GetValue(Vector2i delta)
    {
        return DirectionTypes2D.Middle;
    }

    public static DirectionTypes2D GetType(int value)
    {
        return (DirectionTypes2D) value;
    }

    public static DirectionTypes2D GetRelative(DirectionTypes2D lookTo, DirectionTypes2D param)
    {
        if (param== DirectionTypes2D.Right)
        { 
            switch (lookTo)
            {
                case DirectionTypes2D.Up:
                    return DirectionTypes2D.Right;
                case DirectionTypes2D.UpRight:
                    return DirectionTypes2D.DownRight;
                case DirectionTypes2D.Right:
                    return DirectionTypes2D.Down;
                case DirectionTypes2D.DownRight:
                    return DirectionTypes2D.DownLeft;
                case DirectionTypes2D.Down:
                    return DirectionTypes2D.Left;
                case DirectionTypes2D.DownLeft:
                    return DirectionTypes2D.UpLeft;
                case DirectionTypes2D.Left:
                    return DirectionTypes2D.Up;
                case DirectionTypes2D.UpLeft:
                    return DirectionTypes2D.UpRight;
            }
            return DirectionTypes2D.Middle;
        }
        else if (param==DirectionTypes2D.Left)
        {
            switch (lookTo)
            {
                case DirectionTypes2D.Up:
                    return DirectionTypes2D.Left;
                case DirectionTypes2D.Down:
                    return DirectionTypes2D.Right;
                case DirectionTypes2D.Left:
                    return DirectionTypes2D.Down;
                case DirectionTypes2D.Right:
                    return DirectionTypes2D.Up; //asd
            }
        }

        return DirectionTypes2D.Middle;
    }

    public static DirectionTypes2D GetBackDirection(DirectionTypes2D currentDir)
    {
        switch (currentDir)
        {
            case DirectionTypes2D.Up:
                return DirectionTypes2D.Down;
            case DirectionTypes2D.Down:
                return DirectionTypes2D.Up;
            case DirectionTypes2D.Left:
                return DirectionTypes2D.Right;
            case DirectionTypes2D.Right:
                return DirectionTypes2D.Left;
        }
        return currentDir;
    }


    public static DirectionTypes2D Inverse(DirectionTypes2D value)
    {
        switch (value)
        {
            case DirectionTypes2D.Up:
                return DirectionTypes2D.Down;
            case DirectionTypes2D.UpRight:
                return DirectionTypes2D.DownLeft;
            case DirectionTypes2D.Right:
                return DirectionTypes2D.Left;
            case DirectionTypes2D.DownRight:
                return DirectionTypes2D.UpLeft;
            case DirectionTypes2D.Down:
                return DirectionTypes2D.Up;
            case DirectionTypes2D.DownLeft:
                return DirectionTypes2D.UpRight;
            case DirectionTypes2D.Left:
                return DirectionTypes2D.Right;
            case DirectionTypes2D.UpLeft:
                return DirectionTypes2D.DownRight;
            case DirectionTypes2D.Middle:
                return DirectionTypes2D.Middle;
        }
        return DirectionTypes2D.Middle;
    }
}


