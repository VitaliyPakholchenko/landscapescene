﻿using System;
using UnityEngine;

public class TimeBomb : MonoBehaviour
{
    public float Duration;
    private float _elapsed;
    private bool _complete;

    public GameObject Owner { get; private set; }

    public event Action<GameObject> OnComplete;

    public void Complete()
    {
        if (_complete)
            return;

        _complete = true;

        if (OnComplete != null)
        {
            var complete = OnComplete;
            OnComplete = null;
            complete(Owner);
        }

        Owner = null;
    }

    private void Initialize(float seconds)
    {
        Duration = seconds;
    }

    public void Stop()
    {
        Complete();
        DestroyImmediate(gameObject);
    }

    private void Update()
    {
        if (_complete)
            return;

        _elapsed += Time.deltaTime;
        if (_elapsed >= Duration)
        {
            Complete();
            DestroyImmediate(gameObject);
        }
    }

    public static TimeBomb Create(GameObject owner, float seconds, string name)
    {
        var tbObj = new GameObject("TimeBomb_" + name);
        var tb = tbObj.AddComponent<TimeBomb>();

        if (owner != null)
            tb.Owner = owner;

        tb.Initialize(seconds);
        return tb;
    }

    private void OnDestroy()
    {
        Complete();
    }
}