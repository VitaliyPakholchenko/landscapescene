﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static partial class Utils
{
    public static GameObject CreateGameObject(GameObject source, Component parent, Vector3 xyz, Quaternion rot)
    {
        return CreateGameObject(source, parent != null ? parent.gameObject : null, xyz, rot);
    }

    public static void SetParent(GameObject source, GameObject newParent)
    {
        if (source != null)
        {
            SetParent(source.transform, newParent != null ? newParent.transform : null);
        }
    }

    public static void SetParent(Transform source, Transform newParent)
    {
        if (source != null && source.parent != newParent)
        {
            var oldLocalPosition = source.localPosition;
            var oldLocalRotation = source.localRotation;
            var oldLocalScale = source.localScale;

            source.parent = newParent;
            source.localPosition = oldLocalPosition;
            source.localRotation = oldLocalRotation;
            source.localScale = oldLocalScale;
        }
    }

    public static GameObject CreateGameObject(GameObject gameObject, GameObject parent, Vector3 xyz, Quaternion rot)
    {
        if (gameObject != null)
        {
            gameObject = (GameObject) GameObject.Instantiate(gameObject, xyz, rot);

            if (parent != null)
            {
                SetParent(gameObject.transform, parent.transform);
            }
        }

        return gameObject;
    }

    public static GameObject CreateGameObject(string name, Component parent, HideFlags hideFlags = 0)
    {
        return CreateGameObject(name, parent != null ? parent.gameObject : null, hideFlags);
    }

    public static GameObject CreateGameObject(string name, GameObject parent, HideFlags hideFlags = 0)
    {
        var gameObject = new GameObject(name);

        gameObject.hideFlags = hideFlags;

        if (parent != null)
        {
            gameObject.transform.parent = parent.transform;
            gameObject.transform.localPosition = Vector3.zero;
            gameObject.transform.localRotation = Quaternion.identity;
            gameObject.transform.localScale = Vector3.one;
        }

        return gameObject;
    }

	public static void SetLayer(GameObject root, int layer, bool recusrively = false)
	{
		if (root != null)
		{
			if (root.layer != layer)
			{
				root.layer = layer;
			}
			
			if (recusrively == true)
			{
				foreach (Transform t in root.transform)
				{
					SetLayer(t.gameObject, layer, true);
				}
			}
		}
	}
	
	public static void SetTag(GameObject root, string tag, bool recusrively = false)
	{
		if (root != null && tag != null)
		{
			if (root.tag != tag)
			{
				root.tag = tag;
			}
			
			if (recusrively == true)
			{
				foreach (Transform t in root.transform)
				{
					SetTag(t.gameObject, tag, true);
				}
			}
		}
	}

    public static void Enable(this GameObject obj)
    {
        var comps = obj.GetComponents<MonoBehaviour>();
        foreach (var component in comps)
            component.enabled = true;
    }

    public static void Disable(this GameObject obj)
    {
        var comps = obj.GetComponents<MonoBehaviour>();
        foreach (var component in comps)
            component.enabled = false;
    }

    public static void Visible(this GameObject obj)
    {
        var comps = obj.GetComponents<MonoBehaviour>();
        foreach (var component in comps)
            component.enabled = false;
    }

    public static T CreateChild<T>(this GameObject parent) where T : MonoBehaviour
    {
        var object_ = new GameObject();
        object_.transform.SetParent(parent.transform);
        object_.name = typeof (T).Name;
        return object_.AddComponent<T>();
    }

    public static GameObject CreateChild(this GameObject parent)
    {
        var object_ = new GameObject();
        object_.transform.SetParent(parent.transform);
        return object_;
    }

    public static GameObject GetChildByName(this GameObject parent, string name)
    {
        var child = parent.transform.Find(name);
        return child != null ? child.gameObject : null;
    }

    /*public static T AddComponent<T>(this MonoBehaviour component) where T : Component
        {
#if UNITY_EDITOR
            if (component.gameObject.GetComponent<T>() != null)
                Debug.Log("Component is enabled: " + typeof(T).Name);
#endif
            return component.gameObject.AddComponent<T>();
        }*/

    public static void PositionX(this GameObject object_, float x)
    {
        object_.transform.position = new Vector3(x, object_.transform.position.y, object_.transform.position.z);
    }

    public static void PositionY(this GameObject object_, float y)
    {
        object_.transform.position = new Vector3(object_.transform.position.x, y, object_.transform.position.z);
    }

    public static void PositionZ(this GameObject object_, float z)
    {
        object_.transform.position = new Vector3(object_.transform.position.x, object_.transform.position.y, z);
    }


    public static void ForEachResursive(this GameObject object_, Action<GameObject> action)
    {
        if (action == null)
            return;

        foreach (Transform child in object_.transform)
        {
            action(child.gameObject);
            child.gameObject.ForEachResursive(action);
        }
    }

    public static T GetComponentInChildren<T>(this GameObject object_, string objectName) where T : UnityEngine.Object
    {
        var components = object_.GetComponentsInChildren<T>();
        return components.FirstOrDefault(c => c.name == objectName);
    }

    public static void DestroyGameObject(Component component)
    {
        if (component != null)
        {
            DestroyObject(component.gameObject);
        }
    }

    public static GameObject DestroyGameObject(GameObject gameObject)
    {
        return (GameObject)DestroyObject(gameObject);
    }

    public static List<GameObject> DestroyGameObjects(List<GameObject> gameObjects)
    {
        if (gameObjects != null)
        {
            for (var i = 0; i < gameObjects.Count; i++)
            {
                gameObjects[i] = DestroyGameObject(gameObjects[i]);
            }
        }
        return null;
    }

    public static GameObject[] DestroyGameObjects(GameObject[] gameObjects)
    {
        if (gameObjects != null)
        {
            for (var i = 0; i < gameObjects.Length; i++)
            {
                gameObjects[i] = DestroyGameObject(gameObjects[i]);
            }
        }

        return null;
    }

    public static void DestroyChildren(GameObject parent)
    {
        if (parent != null && parent.transform.childCount > 0)
        {
            for (var i = parent.transform.childCount - 1; i >= 0; i--)
            {
                DestroyGameObject(parent.transform.GetChild(i).gameObject);
            }
        }
    }

    public static List<T> DestroyObjects<T>(List<T> unityObjects)
        where T : UnityEngine.Object
    {
        if (unityObjects != null)
        {
            for (var i = 0; i < unityObjects.Count; i++)
            {
                unityObjects[i] = DestroyObject(unityObjects[i]);
            }
        }

        return null;
    }

    public static T[] DestroyObjects<T>(T[] unityObjects)
        where T : UnityEngine.Object
    {
        if (unityObjects != null)
        {
            for (var i = 0; i < unityObjects.Length; i++)
            {
                unityObjects[i] = DestroyObject(unityObjects[i]);
            }
        }

        return null;
    }

    public static T DestroyObject<T>(T unityObject)
        where T : UnityEngine.Object
    {
        if (unityObject != null)
        {
            if (UnityEngine.Application.isPlaying == true)
            {
                UnityEngine.Object.Destroy(unityObject);
            }
            else
            {
                UnityEngine.Object.DestroyImmediate(unityObject);
            }
        }

        return null;
    }

#if UNITY_EDITOR == true
    public static void HideWireframe(GameObject go)
    {
        if (go != null) HideWireframe(go.GetComponent<Renderer>());
    }

    public static void HideWireframe(Renderer r)
    {
#if HIDE_WIREFRAME_IN_EDITOR == true
		UnityEditor.EditorUtility.SetSelectedWireframeHidden(r, true);
#else
        UnityEditor.EditorUtility.SetSelectedWireframeHidden(r, false);
#endif
    }

    public static void HideGameObject(Component c)
    {
        if (c != null) HideGameObject(c.gameObject);
    }

    public static void HideGameObject(GameObject go)
    {
        if (go != null)
        {
#if HIDE_HIERARCHY_IN_EDITOR == true
			go.hideFlags = HideFlags.HideInHierarchy;
#else
            go.hideFlags = 0;
#endif
        }
    }

    public static void ShowGameObject(Component c)
    {
        if (c != null) ShowGameObject(c.gameObject);
    }

    public static void ShowGameObject(GameObject go)
    {
        if (go != null) go.hideFlags = 0;
    }

    public static void HideComponent(Component c)
    {
        if (c != null)
        {
#if HIDE_HIERARCHY_IN_EDITOR == true
			c.hideFlags = HideFlags.HideInInspector;
#else
            c.hideFlags = 0;
#endif
        }
    }

    public static void ShowComponent(Component c)
    {
        if (c != null) c.hideFlags = 0;
    }
#endif
}