﻿using System.Linq;


    using System;
    using System.Collections.Generic;
    using UnityEngine;

public static class RandomHelper
{
    // -------------------------------------------------------------------
    public static void Reset()
    {
        var currTime = DateTime.Now;
        UnityEngine.Random.seed = (int) currTime.Ticks;
    }

    // -------------------------------------------------------------------
    public static void Reset(int seed)
    {
        UnityEngine.Random.seed = seed;
    }

    // -------------------------------------------------------------------
    public static bool Bool
    {
        get { return Int(1) == 1; }
    }

    // -------------------------------------------------------------------
    public static int Int(int min, int max)
    {
        return UnityEngine.Random.Range(min, max + 1);
    }

    // -------------------------------------------------------------------
    public static int Int(int max)
    {
        return UnityEngine.Random.Range(0, max + 1);
    }

    // -------------------------------------------------------------------
    public static bool Probability(int max)
    {
        return Int(max) > max - 1;
    }

    // -------------------------------------------------------------------
    public static List<int> IntUnique(int min, int max, int count)
    {
        var result = new List<int>();
        while (result.Count < count)
        {
            l1:
            var index = Int(min, max);
            if (result.Contains(index))
                goto l1;
            result.Add(index);
        }
        return result;
    }

    // -------------------------------------------------------------------
    public static float Float()
    {
        return UnityEngine.Random.value;
    }

    // -------------------------------------------------------------------
    public static float Float(float min, float max)
    {
        return Float()*(max - min) + min;
    }

    // -------------------------------------------------------------------
    public static byte Byte(byte min, byte max)
    {
        return (byte) Int(min, max);
    }

    // -------------------------------------------------------------------
    public static byte Byte()
    {
        return (byte) Int(255);
    }

    // -------------------------------------------------------------------
    public static string String(params string[] values)
    {
        int rand = Int(0, values.Length - 1);
        return values[rand];
    }

    // -------------------------------------------------------------------
    public static Vector2 Vector2(Vector2 min, Vector2 max)
    {
        return new Vector2(Float(min.x, max.x), Float(min.y, max.y));
    }

    // -------------------------------------------------------------------
    public static Vector3 Vector3(Vector3 min, Vector3 max)
    {
        return new Vector3(Float(min.x, max.x), Float(min.y, max.y), Float(min.z, max.z));
    }

    // -------------------------------------------------------------------
    public static Vector3 Vector3(Vector3 max)
    {
        var fx = Float(0, max.x);
        var fy = Float(0, max.y);
        var fz = Float(0, max.z);
        return new Vector3(fx, fy, fz);
    }

    // -------------------------------------------------------------------
    public static Vector3 Normal
    {
        get
        {
            var randomNormalVector = new Vector3(
                Float(-1.0f, 1.0f),
                Float(-1.0f, 1.0f),
                Float(-1.0f, 1.0f));
            randomNormalVector.Normalize();
            return randomNormalVector;
        }
    }

    // -------------------------------------------------------------------
    public static Vector2 RandomCirclePoint(float radius)
    {
        var angleRadians = Float(0f, Mathf.PI);
        radius = Float(0.0f, radius);
        return new Vector2(Mathf.Cos(angleRadians)*radius,
            Mathf.Sin(angleRadians)*radius);
    }

    // -------------------------------------------------------------------
    public static Vector3 RandomCirclePoint3D(float radius)
    {
        var angleRadians = Float(-Mathf.PI, Mathf.PI);
        radius = Float(0.0f, radius);
        return new Vector3(Mathf.Cos(angleRadians)*radius,
            Mathf.Sin(angleRadians)*radius, 0);
    }

    // -------------------------------------------------------------------
    public static Color Color
    {
        get { return new Color(Float(0.0f, 1.0f), Float(0.0f, 1.0f), Float(0.0f, 1.0f), Float(0.0f, 1.0f)); }
    }

    // -------------------------------------------------------------------
    public static Vector2 UnitVector()
    {
        var radians = Float(-Mathf.PI, Mathf.PI);
        return new Vector2
        {
            x = Mathf.Cos(radians),
            y = Mathf.Sin(radians)
        };
    }

    // -------------------------------------------------------------------
    public static Vector3 UnitVector3()
    {
        var radians = Float(-Mathf.PI, Mathf.PI);
        var z = Float(-1f, 1f);
        var t = Mathf.Sqrt(1f - (z*z));

        var planar = new Vector2
        {
            x = Mathf.Cos(radians)*t,
            y = Mathf.Sin(radians)*t
        };

        return new Vector3
        {
            x = planar.x,
            y = planar.y,
            z = z
        };
    }

    // -------------------------------------------------------------------
    public static float Variation(float value, float variation)
    {
        float min = (value - variation),
            max = (value + variation);
        return Float(min, max);
    }

    // -------------------------------------------------------------------
    public static T AnyWhere<T>(List<T> collection, Func<T, bool> func)
    {
        if (func == null)
            return Any(collection);
        var intermediate = collection.Where(func).ToArray();
        return Any(intermediate);
    }

    // -------------------------------------------------------------------
    public static T Any<T>(IEnumerable<T> collection)
    {
        var enumerable = collection as IList<T> ?? collection.ToList();

        if (enumerable.Count == 0)
            return default(T);

        if (enumerable.Count == 1)
            return enumerable[0];

        var index = Int(enumerable.Count - 1);

        return enumerable[index];
    }

    // -------------------------------------------------------------------
    /*public static T AnyWhere<T>(IEnumerable<T> collection)
        {
            var enumerable = collection.GetEnumerator();
            var count = collection.Count();

            if (enumerable.Count == 0)
                return default(T);

            if (enumerable.Count == 1)
                return enumerable[0];

            var index = Int(enumerable.Count - 1);

            return enumerable[index];
        }*/

    // -------------------------------------------------------------------
    public static T AnyAddition<T>(List<T> list, T value)
    {
        if (list.Count == 1)
            return list[0];

        var index = list.IndexOf(value);
        if (index < 0)
            return Any(list);

        var index2 = Int(list.Count - 1);
        while (index2 == index)
            index2 = Int(list.Count - 1);

        return list[index2];
    }

    // -------------------------------------------------------------------
    public static T Any<T>(ref T[] list)
    {
        var index = Int(list.Length - 1);
        return list[index];
    }

    // -------------------------------------------------------------------
    public static T Any<T>(ref List<T> list)
    {
        var index = Int(list.Count - 1);
        return list[index];
    }

    // -------------------------------------------------------------------
    public static T AnyAddition<T>(ref T[] list, T value)
    {
        if (list.Length == 1)
            return list[0];

        var index = -1;
        for (var i = 0; i < list.Length; i++)
        {
            if (!list[i].Equals(value)) continue;
            index = i;
            break;
        }

        if (index < 0)
            return Any(ref list);

        var len = list.Length - 1;
        var index2 = Int(len);
        while (index2 == index)
            index2 = Int(len);

        return list[index2];
    }

    public static void Shuffle<T>(this IList<T> list)
    {
        var n = list.Count;
        System.Random rnd = new System.Random();
        while (n > 1)
        {
            var k = (rnd.Next(0, n) % n);
            n--;
            var value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}

