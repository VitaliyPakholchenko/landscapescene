﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct PairIntListInt : IEquatable<PairIntListInt>
{
    [SerializeField]
    public int Key;

    [SerializeField]
    public List<int> Value;

    public override int GetHashCode()
    {
        unchecked
        {
            return (Key * 397) ^ Value.GetHashCode();
        }
    }

    public bool Equals(PairIntListInt other)
    {
        return Key == other.Key && Value == other.Value;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is PairIntInt && Equals((PairIntInt)obj);
    }

    public PairIntListInt(int key, List<int> value)
    {
        Key = key;
        Value = value.ToList();
    }

    public void Set(int key, List<int> value)
    {
        Key = key;
        Value = value.ToList();
    }

    public static bool operator ==(PairIntListInt p1, PairIntListInt p2)
    {
        return p1.Key.Equals(p2.Key) && p1.Value.Equals(p2.Value);
    }

    public static bool operator !=(PairIntListInt p1, PairIntListInt p2)
    {
        return !p1.Key.Equals(p2.Key) || !p1.Value.Equals(p2.Value);
    }

#if UNITY_EDITOR
    public bool EditorLayout(string keyName, string valueName)
    {
        EditorGUILayout.BeginVertical();

        EditorGUILayout.BeginHorizontal();
        Key = EditorGUILayout.IntField(keyName, Key);
        if (GUILayout.Button("Del"))
        {
            EditorGUILayout.EndHorizontal();
            return true;
        }
        EditorGUILayout.EndHorizontal();

        var delIndex = -1;

        EditorGUILayout.LabelField("Values");
        for (var i = 0; i < Value.Count; i++)
        {
            EditorGUILayout.BeginHorizontal();
            Value[i] = EditorGUILayout.IntField(Value[i]);
            if (GUILayout.Button("Del"))
                delIndex = i;
            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndHorizontal();

        if (delIndex>=0)
            Value.RemoveAt(delIndex);

        if (Value.Count == 0)
            return true;

        return false;
    }
#endif
}