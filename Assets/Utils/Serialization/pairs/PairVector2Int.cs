﻿using UnityEngine;
using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct PairVector2Int : IEquatable<PairVector2Int>
{
    [SerializeField]
    public Vector2 Key;

    [SerializeField]
    public int Value;

    public override int GetHashCode()
    {
        unchecked
        {
            return (Key.GetHashCode() * 397) ^ Value;
        }
    }

    public bool Equals(PairVector2Int other)
    {
        return Key == other.Key && Value == other.Value;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is PairVector2Int && Equals((PairVector2Int)obj);
    }

    public PairVector2Int(Vector2 key, int value)
    {
        Key = key;
        Value = value;
    }

    public void Set(Vector2 key, int value)
    {
        Key = key;
        Value = value;
    }

    public void SetVaue(int value)
    {
        Value = value;
    }

    public static bool operator ==(PairVector2Int p1, PairVector2Int p2)
    {
        return p1.Key.Equals(p2.Key) && p1.Value.Equals(p2.Value);
    }

    public static bool operator !=(PairVector2Int p1, PairVector2Int p2)
    {
        return !p1.Key.Equals(p2.Key) || !p1.Value.Equals(p2.Value);
    }

#if UNITY_EDITOR
    public void EditorLayout(string keyName, string valueName)
    {
        /*EditorGUILayout.BeginHorizontal();
        Key = EditorGUILayout.IntField(keyName, Key);
        Value = EditorGUILayout.IntField(valueName, Value);
        EditorGUILayout.EndHorizontal();*/
    }
#endif
}