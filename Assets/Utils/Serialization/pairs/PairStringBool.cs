﻿using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct PairStringBool : IEquatable<PairStringBool>
{
    [SerializeField]
    public string Key;

    [SerializeField]
    public bool Value;

    public override int GetHashCode()
    {
        unchecked
        {
            return (Key.GetHashCode() * 397) ^ Value.GetHashCode();
        }
    }

    public bool Equals(PairStringBool other)
    {
        return Key == other.Key && Value == other.Value;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is PairStringBool && Equals((PairStringBool)obj);
    }

    public PairStringBool(string key, bool value)
    {
        Key = key;
        Value = value;
    }

    public void Set(string key, bool value)
    {
        Key = key;
        Value = value;
    }

    public static bool operator ==(PairStringBool p1, PairStringBool p2)
    {
        return p1.Key.Equals(p2.Key) && p1.Value.Equals(p2.Value);
    }

    public static bool operator !=(PairStringBool p1, PairStringBool p2)
    {
        return !p1.Key.Equals(p2.Key) || !p1.Value.Equals(p2.Value);
    }

#if UNITY_EDITOR
    public void EditorLayout(string keyName, string valueName)
    {
        EditorGUILayout.BeginHorizontal();
        Key = EditorGUILayout.TextField(keyName, Key);
        Value = EditorGUILayout.Toggle(valueName, Value);
        EditorGUILayout.EndHorizontal();
    }
#endif
}