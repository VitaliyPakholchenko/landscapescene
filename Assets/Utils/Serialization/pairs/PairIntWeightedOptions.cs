﻿using UnityEngine;
using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct PairIntWeightedOptions : IEquatable<PairIntWeightedOptions>
{
    [SerializeField]
    public int Key;

    [SerializeField]
    public WeightedOptions Value;

    public override int GetHashCode()
    {
        unchecked
        {
            return (Key * 397) ^ Value.GetHashCode();
        }
    }

    public bool Equals(PairIntWeightedOptions other)
    {
        return Key == other.Key && Value == other.Value;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is PairIntWeightedOptions && Equals((PairIntWeightedOptions)obj);
    }

    public PairIntWeightedOptions(int key, WeightedOptions value)
    {
        Key = key;
        Value = value;
    }

    public void Set(int key, WeightedOptions value)
    {
        Key = key;
        Value = value;
    }

    public static bool operator ==(PairIntWeightedOptions p1, PairIntWeightedOptions p2)
    {
        return p1.Key.Equals(p2.Key) && p1.Value.Equals(p2.Value);
    }

    public static bool operator !=(PairIntWeightedOptions p1, PairIntWeightedOptions p2)
    {
        return !p1.Key.Equals(p2.Key) || !p1.Value.Equals(p2.Value);
    }

#if UNITY_EDITOR
    public void EditorLayout(string keyName, string wKeyName, string wValueName)
    {
        EditorGUILayout.BeginVertical();
        Key = EditorGUILayout.IntField(keyName, Key);
        Value.EditorLayout(string.Empty, wKeyName, wValueName);
        EditorGUILayout.EndVertical();
    }
#endif

}