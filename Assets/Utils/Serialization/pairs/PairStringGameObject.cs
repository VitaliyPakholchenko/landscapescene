﻿using UnityEngine;
using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct PairStringGameObject : IEquatable<PairStringGameObject>
{
    [SerializeField]
    public string Key;

    [SerializeField]
    public GameObject Value;

    public bool Equals(PairStringGameObject other)
    {
        return EqualityComparer<string>.Default.Equals(Key, other.Key);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is PairStringGameObject && Equals((PairStringGameObject)obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (EqualityComparer<string>.Default.GetHashCode(Key) * 397) ^ EqualityComparer<GameObject>.Default.GetHashCode(Value);
        }
    }

    public PairStringGameObject(string key, GameObject value)
    {
        Key = key;
        Value = value;
    }

    public void Set(string key, GameObject value)
    {
        Key = key;
        Value = value;
    }

    public static bool operator ==(PairStringGameObject p1, PairStringGameObject p2)
    {
        return p1.Key.Equals(p2.Key) && p1.Value.Equals(p2.Value);
    }

    public static bool operator !=(PairStringGameObject p1, PairStringGameObject p2)
    {
        return !p1.Key.Equals(p2.Key) || !p1.Value.Equals(p2.Value);
    }

#if UNITY_EDITOR
    public void EditorLayout(string keyName, string valueName)
    {
        EditorGUILayout.BeginHorizontal();
        Key = EditorGUILayout.TextField(keyName, Key);
        Value = EditorGUILayout.ObjectField(valueName, Value, typeof(GameObject), false) as GameObject;
        EditorGUILayout.EndHorizontal();
    }
#endif
}