﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class Pair<T1,T2>
{
    public T1 Key;
    public T2 Value;

    public Pair(T1 key, T2 value)
    {
        Key = key;
        Value = value;
    }
}

