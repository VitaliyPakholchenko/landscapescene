﻿using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct PairIntFloat : IEquatable<PairIntFloat>
{
    [SerializeField]
    public int Key;

    [SerializeField]
    public float Value;

    public override int GetHashCode()
    {
        unchecked
        {
            return (Key * 397) ^ Value.GetHashCode();
        }
    }

    public bool Equals(PairIntFloat other)
    {
        return Key == other.Key && Value.Equals(other.Value);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is PairIntFloat && Equals((PairIntFloat)obj);
    }

    public PairIntFloat(int key, float value)
    {
        Key = key;
        Value = value;
    }

    public void Set(int key, int value)
    {
        Key = key;
        Value = value;
    }

    public void SetVaue(int value)
    {
        Value = value;
    }

    public static bool operator ==(PairIntFloat p1, PairIntFloat p2)
    {
        return p1.Key.Equals(p2.Key) && p1.Value.Equals(p2.Value);
    }

    public static bool operator !=(PairIntFloat p1, PairIntFloat p2)
    {
        return !p1.Key.Equals(p2.Key) || !p1.Value.Equals(p2.Value);
    }

#if UNITY_EDITOR
    public void EditorLayout(string keyName, string valueName)
    {
        EditorGUILayout.BeginHorizontal();
        Key = EditorGUILayout.IntField(keyName, Key);
        Value = EditorGUILayout.FloatField(valueName, Value);
        EditorGUILayout.EndHorizontal();
    }
#endif
}