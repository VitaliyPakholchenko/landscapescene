﻿using UnityEngine;
using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct PairStringString : IEquatable<PairStringString>
{
    [SerializeField]
    public string Key;

    [SerializeField]
    public string Value;

    public bool Equals(PairStringString other)
    {
        return EqualityComparer<string>.Default.Equals(Key, other.Key) && EqualityComparer<string>.Default.Equals(Value, other.Value);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is PairStringInt && Equals((PairStringInt)obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (EqualityComparer<string>.Default.GetHashCode(Key) * 397) ^ EqualityComparer<string>.Default.GetHashCode(Value);
        }
    }

    public PairStringString(string key, string value)
    {
        Key = key;
        Value = value;
    }

    public void Set(string key, string value)
    {
        Key = key;
        Value = value;
    }

    public static bool operator ==(PairStringString p1, PairStringString p2)
    {
        return p1.Key.Equals(p2.Key) && p1.Value.Equals(p2.Value);
    }

    public static bool operator !=(PairStringString p1, PairStringString p2)
    {
        return !p1.Key.Equals(p2.Key) || !p1.Value.Equals(p2.Value);
    }

#if UNITY_EDITOR
    public void EditorLayout(string keyName, string valueName)
    {
        /*EditorGUILayout.BeginHorizontal();
        Key = EditorGUILayout.TextField(keyName, Key);
        Value = EditorGUILayout.IntField(valueName, Value);
        EditorGUILayout.EndHorizontal();*/
    }
#endif
}