﻿using UnityEngine;
using System;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct PairStringAudio : IEquatable<PairStringAudio>
{
    [SerializeField]
    public string Key;

    [SerializeField]
    public AudioClip Value;

    public bool Equals(PairStringAudio other)
    {
        return EqualityComparer<string>.Default.Equals(Key, other.Key);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is PairStringInt && Equals((PairStringInt)obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (EqualityComparer<string>.Default.GetHashCode(Key) * 397) ^ Value.GetHashCode();
        }
    }

    public void Set(string key, AudioClip value)
    {
        Key = key;
        Value = value;
    }

    public void SetKey(string key)
    {
        Key = key;
    }

    public static bool operator ==(PairStringAudio p1, PairStringAudio p2)
    {
        return p1.Key.Equals(p2.Key) && p1.Value.Equals(p2.Value);
    }

    public static bool operator !=(PairStringAudio p1, PairStringAudio p2)
    {
        return !p1.Key.Equals(p2.Key) || !p1.Value.Equals(p2.Value);
    }

#if UNITY_EDITOR
    public bool EditorLayout(string keyName)
    {
        var result = false;
        
        return result;
    }
#endif
}