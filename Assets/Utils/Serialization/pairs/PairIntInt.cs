﻿using UnityEngine;
using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public struct PairIntInt : IEquatable<PairIntInt>
{
    [SerializeField]
    public int Key;

    [SerializeField]
    public int Value;

    public override int GetHashCode()
    {
        unchecked
        {
            return (Key * 397) ^ Value;
        }
    }

    public bool Equals(PairIntInt other)
    {
        return Key == other.Key && Value == other.Value;
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is PairIntInt && Equals((PairIntInt) obj);
    }

    public PairIntInt(int key, int value)
    {
        Key = key;
        Value = value;
    }

    public void Set(int key, int value)
    {
        Key = key;
        Value = value;
    }

    public void SetVaue(int value)
    {
        Value = value;
    }

    public static bool operator ==(PairIntInt p1, PairIntInt p2)
    {
        return p1.Key.Equals(p2.Key) && p1.Value.Equals(p2.Value);
    }

    public static bool operator !=(PairIntInt p1, PairIntInt p2)
    {
        return !p1.Key.Equals(p2.Key) || !p1.Value.Equals(p2.Value);
    }

    #if UNITY_EDITOR
    public void EditorLayout(string keyName, string valueName)
    {
        EditorGUILayout.BeginHorizontal();
        Key = EditorGUILayout.IntField(keyName, Key);
        Value = EditorGUILayout.IntField(valueName, Value);
        EditorGUILayout.EndHorizontal();
    }
    #endif
}