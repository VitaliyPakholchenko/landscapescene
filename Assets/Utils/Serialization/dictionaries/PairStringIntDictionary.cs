﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class PairStringIntDictionary : IEnumerable<PairStringInt>
{
    [SerializeField]
    public List<PairStringInt> Pairs = new List<PairStringInt>();

    public int Count
    {
        get { return Pairs.Count; }
    }

    public bool ContainsKey(string key)
    {
        return Pairs.Any(v => EqualityComparer<string>.Default.Equals(key, v.Key));
    }

    public int this[string index]
    {
        get
        {
            return
                (from i in Pairs where EqualityComparer<string>.Default.Equals(index, i.Key) select i.Value)
                    .FirstOrDefault();
        }


        set
        {
            for (var i = 0; i < Pairs.Count; i++)
                if (EqualityComparer<string>.Default.Equals(index, Pairs[i].Key))
                {
                    Pairs[i] = new PairStringInt(Pairs[i].Key, value);
                    return;
                }
            Pairs.Add(new PairStringInt(index, value));
        }
    }

    public PairStringInt this[int index]
    {
        get { return Pairs[index]; }
        set { Pairs[index] = value; }
    }

    public void Set(int index, string key, int value)
    {
        Pairs[index].Set(key, value);
    }

    public virtual void Set(string key, int value)
    {
        var index = Pairs.FindIndex(p => p.Key == key);
        if (index < 0)
        {
            Pairs.Add(new PairStringInt(key, value));
            return;
        }

        Pairs[index].Set(key, value);
    }

    public string GetKey(int index)
    {
        var iterator = Pairs.GetEnumerator();
        while (iterator.MoveNext())
        {
            index--;
            if (index == 0)
                return iterator.Current.Key;
        }

        return string.Empty;
    }

    public List<int> GetValues()
    {
        var result = new List<int>();
        Pairs.ForEach(p => result.Add(p.Value));
        return result;
    }

    public List<string> GetKeys()
    {
        var result = new List<string>();
        Pairs.ForEach(p => result.Add(p.Key));
        return result;
    }

    public virtual void Add(string key, int value)
    {
        Pairs.Add(new PairStringInt(key, value));
    }

    public virtual void Remove(string key)
    {
        Pairs.RemoveAll(v => EqualityComparer<string>.Default.Equals(key, v.Key));
    }

    public PairStringIntDictionary GetCopy()
    {
        var copy = new PairStringIntDictionary();
        foreach (var item in Pairs)
            copy.Add(item.Key, item.Value);
        return copy;
    }

    public void AddRange(PairStringIntDictionary dictionary)
    { }

    public void ForEach(Action<PairStringInt> action)
    {
        
    }

    public IEnumerator<PairStringInt> GetEnumerator()
    {
        return Pairs.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public virtual void Load(BinaryReader reader)
    {
        var count = reader.ReadInt32();
        for (var i = 0; i < count; i++)
        {
            var key = reader.ReadString();
            var value = reader.ReadInt32();
            Add(key,value);
        }
    }

    public virtual void Save(BinaryWriter writer)
    {
        /*string prefix;
	    node = _qty;
	    return node;*/
    }

#if UNITY_EDITOR
    public void EditorLayout(string fieldName, string keyName, string valueName)
    {
        EditorGUILayout.BeginVertical();

        if (!string.IsNullOrEmpty(fieldName))
            GUILayout.Label(fieldName, EditorStyles.boldLabel);

        var delIndex = -1;
        for (var i = 0; i < Pairs.Count; i++)
        {
            var p = Pairs[i];

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label(keyName, EditorStyles.boldLabel);
            p.Key = EditorGUILayout.TextField(p.Key);
            GUILayout.Label(valueName, EditorStyles.boldLabel);
            p.Value = EditorGUILayout.IntField(p.Value);

            Pairs[i] = p;

            if (GUILayout.Button("Del"))
                delIndex = i;

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();

        if (GUILayout.Button("Add"))
            Pairs.Add(new PairStringInt());
        
        if (delIndex>=0)
            Pairs.RemoveAt(delIndex);
    }
#endif
}