﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class PairIntIntDictionary : IEnumerable<PairIntInt>
{
    [SerializeField]
    public List<PairIntInt> Pairs = new List<PairIntInt>();

    public int Count
    {
        get { return Pairs.Count; }
    }

    public int this[int index]
    {
        get
        {
            foreach (var i in Pairs)
            {
                if (index == i.Key)
                    return i.Value;
            }
            return default(int);
        }

        set
        {
            for (var i = 0; i < Pairs.Count; i++)
                if (EqualityComparer<int>.Default.Equals(index, Pairs[i].Key))
                {
                    Pairs[i] = new PairIntInt(Pairs[i].Key, value);
                    return;
                }
            Pairs.Add(new PairIntInt(index, value));
        }
    }

    public bool ContainsKey(int key)
    {
        return Pairs.Any(v => key == v.Key);
    }

    public PairIntInt Getpair(int index)
    {
        return Pairs[index];
    }

    public void Set(int index, int key, int value)
    {
        Pairs[index].Set(key, value);
    }

    public int GetKey(int index)
    {
        var iterator = Pairs.GetEnumerator();
        while (iterator.MoveNext())
        {
            index--;
            if (index == 0)
                return iterator.Current.Key;
        }

        return -1;
    }

    public List<int> GetValues()
    {
        var result = new List<int>();
        Pairs.ForEach(p => result.Add(p.Value));
        return result;
    }

    public List<int> GetKeys()
    {
        var result = new List<int>();
        Pairs.ForEach(p => result.Add(p.Key));
        return result;
    }

    public virtual void Add(int key, int value)
    {
        Pairs.Add(new PairIntInt(key, value));
    }

    public virtual void Remove(int key)
    {
        Pairs.RemoveAll(v => EqualityComparer<int>.Default.Equals(key, v.Key));
    }

    public PairIntIntDictionary GetCopy()
    {
        var copy = new PairIntIntDictionary();
        foreach (var item in Pairs)
            copy.Add(item.Key, item.Value);
        return copy;
    }

    public void AddRange(PairStringIntDictionary dictionary)
    { }

    public IEnumerator<PairIntInt> GetEnumerator()
    {
        return Pairs.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }


#if UNITY_EDITOR
    public void EditorLayout(string fieldName, string keyName, string valueName)
    {
        EditorGUILayout.BeginVertical();
        GUILayout.Label(fieldName, EditorStyles.boldLabel);

        var delIndex = -1;
        for (var i = 0; i < Pairs.Count; i++)
        {
            var p = Pairs[i];

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label(keyName, EditorStyles.boldLabel);
            p.Key = EditorGUILayout.IntField(p.Key);
            GUILayout.Label(valueName, EditorStyles.boldLabel);
            p.Value = EditorGUILayout.IntField(p.Value);

            if (GUILayout.Button("Del"))
                delIndex = i;

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndVertical();

        if (GUILayout.Button("Add"))
            Pairs.Add(new PairIntInt());

        if (delIndex >= 0)
            Pairs.RemoveAt(delIndex);
    }
#endif
}