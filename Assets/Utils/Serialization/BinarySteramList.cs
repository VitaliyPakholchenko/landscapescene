﻿using System;
using System.Collections.Generic;
using System.IO;

public interface IBinaryStreamItem
{
    void Save(BinaryWriter writer);
    void Load(BinaryReader reader);
}

public class BinarySteramList<T> : List<T> where T : IBinaryStreamItem
{
    public void Save(BinaryWriter writer)
    {
        writer.Write(Count);
        ForEach(i=>i.Save(writer));
    }

    public void Load(BinaryReader reader)
    {
        var count = reader.ReadInt32();
        for (var i = 0; i < count; i++)
        {
            var item = Activator.CreateInstance<T>();
            item.Load(reader);
            Add(item);
        }
    }
}

