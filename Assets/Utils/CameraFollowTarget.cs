﻿using System;
using UnityEngine;

public class CameraFollowTarget : MonoBehaviour
{
    public Vector3 Offset;
    public Vector3 Rotation;

    public float PositionSoftness = 1;
    public float RotationSoftness = 1;

    // --------------------------------------------------------------------------------------------
    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, transform.position + Offset);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position + Offset, 2);
    }
}

