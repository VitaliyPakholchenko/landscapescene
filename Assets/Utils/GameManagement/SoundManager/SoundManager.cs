﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using System.Collections.Generic;
using System.Linq;

public enum AudioGroupTypes
{
    Master = 0,
    Music = 1,
    SFX = 2,
    Speaking=3
}

[Serializable]
public class SoundManager : MonoBehaviour
{
    private const float DBRangeTOValueCoef = -0.8f; // where -0.8 is -80db/100    
    private const float AudioSourcesCleanapFrequency = 3f;

    public AudioMixer AudioMixer;
    public AudioMixerGroup GroupMaster;
    public AudioMixerGroup GroupMusic;
    public AudioMixerGroup GroupSFX;
    public AudioMixerGroup GroupSpeaking;

    public bool AutoChangeMusics { get; set; }

    private Range PitchVariationsRange = new Range(0.95f, 1.05f);    
    private AudioSource _masterMusicSource { get; set; }
    private AudioSource _secondMusicSource { get; set; }
    private bool _fadeMusics { get; set; }

    private float _audioSourcesCleanap;

    [SerializeField]
    private SoundCollection _musics;

    [SerializeField]
    private SoundCollection _sounds;
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <param name="value"></param>
    public void SetVolume(AudioGroupTypes channel, float value)
    {        
        value = ((1.0f - value) * 100.0f) * DBRangeTOValueCoef;
        value = (value > 20.0f) ? 20.0f : value < 0 ? 0 : value;
        AudioMixer.SetFloat(channel.ToString() + "Volume", value);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="channel"></param>
    /// <returns></returns>
    public float GetVolume(AudioGroupTypes channel)
    {
        float vol;
        AudioMixer.GetFloat(channel.ToString() + "Volume", out vol);        
        return 1.0f - ((vol / DBRangeTOValueCoef) / 100.0f);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public float GetNewPitch()
    {
        return RandomHelper.Float(PitchVariationsRange.Minimum, PitchVariationsRange.Maximum);
    }

    /// <summary>
    /// 
    /// </summary>
    public void ApplyVolume()
    {
        TimeBomb.Create(gameObject, 0.1f, "music_status").OnComplete += delegate
        {
            if (_masterMusicSource == null)
            {
                if (GetVolume(AudioGroupTypes.Music) > 0.0f)
                    PlayRandomMusic();
                return;
            }

            if (GetVolume(AudioGroupTypes.Music) < 0.1f)
                _masterMusicSource.Stop();
            else if (!_masterMusicSource.isPlaying)
                if (_masterMusicSource.clip != null)
                    _masterMusicSource.Play();
                else
                    PlayRandomMusic();
        };
    }

    private void PlayMusic(AudioClip _clip)
    {
        if (_clip == null)
        {
            Debug.Log("(PlayMusic) Audio clip absent: " + name);
            return;
        }

        if (_masterMusicSource == null)
        {
            var asObject = new GameObject("PrimaryMusicSource");
            asObject.transform.SetParent(gameObject.transform);
            _masterMusicSource = asObject.AddComponent<AudioSource>();
        }

        if (_masterMusicSource.isPlaying)
        {

        }

        _masterMusicSource.outputAudioMixerGroup = GroupMusic;
        _masterMusicSource.clip = _clip;
        _masterMusicSource.loop = !AutoChangeMusics;

        if (GetVolume(AudioGroupTypes.Music) < 0.1f)
            return;

        _masterMusicSource.Play();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    public void PlayMusic(string name)
    {
        if (_musics == null)
            return;

        var clip = _musics.GetClip(name);
       PlayMusic(clip);
    }

    /// <summary>
    /// 
    /// </summary>
    public void PlayRandomMusic()
    {
        if (_musics == null)
            return;

        var clip = _musics.Any();
        PlayMusic(clip);
    }

    /// <summary>
    /// 
    /// </summary>
    public void StopMusic()
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="clip"></param>
    /// <param name="haosFix"></param>
    /// <param name="pitch"></param>
    /// <param name="volume"></param>
    /// <param name="looped"></param>
    private void PlaySound(AudioClip clip, bool haosFix = false, float pitch = 1, bool looped = false)
    {
        var volume = GetVolume(AudioGroupTypes.SFX);
        if (volume < 0.1f)
            return;
        
        if (clip == null)
            return;

        if (haosFix)
        {
            var sources = GetComponentsInChildren<AudioSource>();
            if (sources.Length>0)
            {
                if (sources.FirstOrDefault(audio => audio.clip == clip) != null)
                    return;
            }
        }

        var asObject = new GameObject("AudioSource");
        asObject.transform.SetParent(gameObject.transform);
        
        var freeSource = asObject.AddComponent<AudioSource>();
        freeSource.outputAudioMixerGroup = GroupSFX;
        freeSource.clip = clip;
        freeSource.pitch = pitch;        
        freeSource.loop = looped;
        freeSource.volume = volume;
        freeSource.Play();

        Destroy(asObject, looped ? 100000 : clip.length);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="clipName"></param>
    /// <param name="haosFix"></param>
    /// <param name="pitch"></param>
    /// <param name="looped"></param>
    public void PlaySound(string clipName, bool haosFix = false, float pitch = 1, bool looped = false)
    {
        if (_sounds == null)
            return;

        var clip = _sounds.GetClip(clipName);
        if (clip == null)
        {
            Debug.Log("Audio clip absent: " + clipName);
            return;
        }

        PlaySound(clip, haosFix, pitch, looped);
    }

    /// <summary>
    /// 
    /// </summary>
    public void StopSound(string clipName)
    {
        foreach (var obj in gameObject.transform)
        {
            var go = obj as GameObject;
            if (go != null)
            {
                var asource = go.GetComponent<AudioSource>();
                if (asource != null && asource.clip.name == clipName)
                    asource.Stop();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void StopAllSounds()
    {
        foreach (var obj in gameObject.transform)
        {
            var go = obj as GameObject;
            if (go != null)
            {
                var asource = go.GetComponent<AudioSource>();
                if (asource != null)
                    asource.Stop();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    void Update()
    {
        if (AutoChangeMusics)
        {
        }

        _audioSourcesCleanap += Time.deltaTime;
        if (_audioSourcesCleanap >= AudioSourcesCleanapFrequency)
        {
            _audioSourcesCleanap = 0;
            for (var i = 0; i < transform.childCount; i++)
            {
                var co = transform.GetChild(i);
                var ass = co.GetComponent<AudioSource>();
                if (ass==null)
                    continue;

                if (!ass.isPlaying)
                {
                    Destroy(ass.gameObject);
                    break;
                }
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void Save()
    {
        for (var i=0; i<4; i++)
            PlayerPrefs.SetFloat(((AudioGroupTypes)i).ToString() + "Volume", GetVolume((AudioGroupTypes)i));
    }

    /// <summary>
    /// 
    /// </summary>
    public void Load()
    {
        for (var i = 0; i < 4; i++)
            SetVolume(((AudioGroupTypes)i), PlayerPrefs.GetFloat(((AudioGroupTypes)i).ToString() + "Volume"));
    }
}