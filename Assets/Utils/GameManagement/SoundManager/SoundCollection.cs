﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class SoundCollection : ScriptableObject
{
    [SerializeField]
    private List<PairStringAudio> _clips;
    
    public int Count { get { return _clips.Count; } }

    public AudioClip GetClip(string name)
    {
        return _clips.FirstOrDefault(cl => cl.Key == name).Value;
    }

    public AudioClip Any()
    {
        if (Count < 1)
            return null;

        return RandomHelper.Any(_clips).Value;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(SoundCollection))]
    public class SoundCollectionEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            SoundCollection targetCollection = target as SoundCollection;

            if (targetCollection._clips == null)
                targetCollection._clips = new List<PairStringAudio>();

            EditorGUILayout.BeginVertical();

            var delIndex = -1;

            for (int i = 0; i < targetCollection._clips.Count; i++)
            {
                var tc = targetCollection._clips[i];

                EditorGUILayout.BeginVertical();

                var key = EditorGUILayout.TextField("Clip Name", tc.Key);
                var value = EditorGUILayout.ObjectField("AudioClip", tc.Value, typeof(AudioClip), false) as AudioClip;

                if (GUILayout.Button("Del", GUILayout.MaxWidth(128)))
                    delIndex = i;

                if (value != null && value != tc.Value)
                    key = value.name;

                if (tc.Key != key || tc.Value != value)
                    targetCollection._clips[i] = new PairStringAudio {Key = key, Value = value};

                EditorGUILayout.EndVertical();
            }

            if (delIndex >= 0)
                targetCollection._clips.RemoveAt(delIndex);

            if (GUILayout.Button("Add clip"))
                targetCollection._clips.Add(new PairStringAudio());

            EditorGUILayout.EndVertical();

        }

        public void SaveChanges()
        {
            if (target != null)
            {
                AssetDatabase.Refresh();
                EditorUtility.SetDirty(target);
                AssetDatabase.SaveAssets();
            }
        }

        private void OnDisable()
        {
            SaveChanges();
        }
    }
#endif
}
