﻿#if _INTEGRATION && _STORE

using UnityEngine;
using System.Collections;
using UnityEngine.Purchasing;

public class InAppProduct : ScriptableObject
{

	public string UnityIAPID;
	public string AppleStoreID;
	public string GoogleStoreID;
	public string WindowsStoreID;

	public ProductType Type;

	public bool Internal;
	public string Category;

	public int Value;
	public int Cost;
}

#endif