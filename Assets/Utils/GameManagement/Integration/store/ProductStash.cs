﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ProductStash
{
    private int count;
    private List<string> IDs = new List<string>();
    private List<string> receipts = new List<string>();
    private string noData = "";

    public void Initialize()
    {
        count = Profiles.GetInt("purchaseCount", -1);
        Debug.Log("ProductStash Initialize: Initing for " + count);
        for(int i = 0; i <= count; i++)
        {
            Debug.Log("ProductStash Initialize: Initing arrays for index " + i);
            IDs.Add(Profiles.GetString("purchaseID" + i.ToString()));
            receipts.Add(Profiles.GetString("purchaseReceipt" + i.ToString()));
        }
    }

    public int Add(string purchaseID, string receipt)
    {
        var i = -1;
        var current = -1;
        var empty = false;
        if(IDs != null)
        {
            foreach(string str in IDs)
            {
                i++;
                if(str.Equals(noData))
                {
                    current = i;
                    empty = true;
                    break;
                }
            }
        }

        if(empty)
        {
            Debug.Log("ProductStash Add: There is empty place on " + current);
            IDs[current] = purchaseID;
            receipts[current] = receipt;
            Profiles.SetString("purchaseID" + current, purchaseID);
            Profiles.SetString("purchaseReceipt" + current, receipt);
            PlayerPrefs.Save();
            return current;
        } else
        {
            Debug.Log("ProductStash Add: There is NO empty place in index");
            IDs.Add(purchaseID);
            receipts.Add(receipt);
            count += 1;
            Profiles.Set("purchaseCount", count);
            Profiles.SetString("purchaseID" + count, purchaseID);
            Profiles.SetString("purchaseReceipt" + count, receipt);
            PlayerPrefs.Save();
            return count;
        }
    }

    public bool Remove(int index)
    {
        if(index == -1)
            return false;

        if(index == count)
        {
            Debug.Log("ProductStash Remove: Last elem for index " + index);
            IDs.RemoveAt(index);
            receipts.RemoveAt(index);
            Profiles.Remove("purchaseID" + index);
            Profiles.Remove("purchaseReceipt" + index);
            count -= 1;
            Profiles.Set("purchaseCount", count);
        } else
        {
            Debug.Log("ProductStash Remove: Non-last elem for index " + index);
            IDs[index] = noData;
            receipts[index] = noData;
            Profiles.SetString("purchaseID" + index, noData);
            Profiles.SetString("purchaseReceipt" + index, noData);
        }
        PlayerPrefs.Save();
        return true;
    }

    public bool Remove(string purchaseID, string receipt)
    {
        var id = -1;
        foreach(var purID in IDs)
        {
            var check = -1;
            foreach(string purRec in receipts)
            {
                check++;
                if(purID.Equals(purchaseID) && purRec.Equals(receipt))
                {
                    id = check;
                    break;
                }
            }
        }

        return Remove(id);
    }

    public int GetLast(out string purchaseID, out string receipt)
    {
        if(IDs.Count == 0)
        {
            purchaseID = null;
            receipt = null;
            return -1;
        }
        Debug.Log("ProductStash GetLast: Before While Cycle");
        while(IDs[count].Equals(noData))
        {
            Debug.Log("ProductStash GetLast: While cycle" + IDs[count]);
            Remove(count);
        }

        purchaseID = IDs[count];
        receipt = receipts[count];
        return count;
    }
}
