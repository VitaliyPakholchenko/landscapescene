﻿#if _STORE

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Purchasing;
using System;
using System.Linq;

public class Store : IStoreListener
{
    public const float PurchaseProcessingMaxTime = 11f;

    /// <summary>
    /// 
    /// </summary>
    private IGameManager GameManager;

    /// <summary>
    /// 
    /// </summary>
    private static IStoreController _controller;

    /// <summary>
    /// 
    /// </summary>
    private static IExtensionProvider _extensionProvider;

    /// <summary>
    /// 
    /// </summary>
    private readonly List<InAppProduct> _products = new List<InAppProduct>();

    /// <summary>
    /// 
    /// </summary>
    private readonly Dictionary<string, Pair<InAppProduct, Action<InAppProduct, PurchaseFailureReason>>> _pendingProducts =
        new Dictionary<string, Pair<InAppProduct, Action<InAppProduct, PurchaseFailureReason>>>();

    /// <summary>
    /// 
    /// </summary>
    private readonly ProductStash _stash = new ProductStash();

    /// <summary>
    /// 
    /// </summary>
    private bool _initialized;
    
    /// <summary>
    /// 
    /// </summary>
    public event Func<IModel> OnShowWait;

    /// <summary>
    /// 
    /// </summary>
    public bool WaitForPurchased;

    /// <summary>
    /// 
    /// </summary>
    private string _currentTransactionID;

    /// <summary>
    /// 
    /// </summary>
    private bool _storePurchaseProcessing;

    /// <summary>
    /// 
    /// </summary>
    private bool _serverResponded;
    
    /// <summary>
    /// 
    /// </summary>
    public void Initialize(IGameManager gm)
    {
        GameManager = gm;

        _stash.Initialize();

        var objects = Resources.LoadAll("InAppProducts");
        if (objects == null || objects.Length == 0)
            return;

        _products.Clear();

        foreach (var product in objects.OfType<InAppProduct>())
            _products.Add(product);

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        foreach (var prod in _products)
        {
            Debug.Log("Product name: " + prod.UnityIAPID + " " + prod.Category);
            builder.AddProduct(prod.UnityIAPID, prod.Type,
                new IDs() { { prod.AppleStoreID, AppleAppStore.Name }, { prod.GoogleStoreID, GooglePlay.Name }, });
        }

        UnityPurchasing.Initialize(this, builder);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="controller"></param>
    /// <param name="extensionProvider"></param>
    public void OnInitialized(IStoreController controller, IExtensionProvider extensionProvider)
    {
        Debug.Log("Store initialized");
        _controller = controller;
        _extensionProvider = extensionProvider;
        _initialized = true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="error"></param>
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("Store innitialized error: " + error);
        _initialized = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="product"></param>
    /// <param name="failureReason"></param>
    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}",
            product.definition.storeSpecificId, failureReason));

        var item = _products.Find(x => x.UnityIAPID == product.definition.id);
        if (item == null)
            return;

        _stash.Remove(product.definition.id, product.receipt);

        var pending = _pendingProducts[product.definition.id];
        if (pending.Value != null)
            pending.Value(pending.Key, failureReason);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="args"></param>
    /// <returns></returns>
    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        try
        {
            var product = args.purchasedProduct;

            Debug.Log("IntegrationStore ServerConfirmPurchase: Purchase Successfull for item " + product.definition.id +
                      "! We will check receipt on server now!");

            var index = _stash.Add(product.definition.id, product.receipt);
            _currentTransactionID = product.transactionID;
            _storePurchaseProcessing = false;

            //GameManager.Analytics.ApplayEvent();
            //AnalyticsController.Instance.EventPurchase(id, item.Value, item.Cost, AnalyticsPurchaseResult.Success,
            //  currentTransactionID);

            ////GameGlobals.Instance.CurrencyCount.Analytics.SetAddOperationReason(CurrencyAddReason.Purchase);
            ////GameGlobals.Instance.CurrencyCount.Analytics.SetAddOperationReason(CurrencyAddReason.Purchase);
            ////GameGlobals.Instance.CurrencyCount.Add(item.Value);

            _stash.Remove(index);
            WaitForPurchased = false;
            _serverResponded = true;
            WaitForPurchased = true;

            var pending = _pendingProducts[product.definition.id];
            _pendingProducts.Remove(product.definition.id);
            if (pending.Value != null)
                pending.Value(pending.Key, PurchaseFailureReason.Unknown);
            
            return PurchaseProcessingResult.Complete;
        }
        catch (Exception e)
        {
            Debug.Log("Error occured on ProcessPurchase() method execution: " + e.Message);
            return PurchaseProcessingResult.Complete;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="inAppProduct"></param>
    /// <returns></returns>
    public void BuyProduct(InAppProduct inAppProduct, Action<InAppProduct, PurchaseFailureReason> _purchasedComplete)
    {
        if (!inAppProduct.Internal)
        {
            var product = _controller.products.WithID(inAppProduct.UnityIAPID);
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));

                _pendingProducts.Add(product.definition.id,
                    new Pair<InAppProduct, Action<InAppProduct, PurchaseFailureReason>>(inAppProduct,
                        _purchasedComplete));

                _storePurchaseProcessing = true;
                _serverResponded = false;

                _controller.InitiatePurchase(product);
            }
        }
        else
        {
            if (_purchasedComplete!=null)
                _purchasedComplete(inAppProduct,PurchaseFailureReason.Unknown);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void PurchaseProcessing()
    {
        PurchaseProcessing(PurchaseProcessingMaxTime);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="timeLeft"></param>
    private void PurchaseProcessing(float timeLeft)
    {
        if (!_storePurchaseProcessing)
            return;

        var time = Time.time;
        if (timeLeft < 0f)
        {
            RestoreTransactions();
            return;
        }

        TimeBomb.Create(null, 1f, "purchaseProcessing").OnComplete += delegate
        {
            PurchaseProcessing(timeLeft - (Time.time - time));
        };
    }
    
    /// <summary>
    /// 
    /// </summary>
    public void RestoreTransactions()
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            var apple = _extensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                Debug.Log("RestorePurchases continuing: " + result +
                          ". If no further messages, no purchases available to restore.");
            });

            // TODO: Clear logs
            // Directory.Delete(Path.Combine (Path.Combine (Application.persistentDataPath, "Unity"), "UnityPurchasing"), true);
        }
        else
        {
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void RefreshAppReceipt()
    {
        var apple = _extensionProvider.GetExtension<IAppleExtensions>();
        apple.RefreshAppReceipt(
            r => Debug.Log("InegrationStore: Application receipt is fetched from Store"),
            () => Debug.Log("InegrationStore: Application receipt fetch fail!"));
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="money"></param>
    /// <param name="purchaseName"></param>
    /// <returns></returns>
    public bool CheskInternalPurchaseByName(int money, string purchaseName)
    {
        return _products.Any(p => p.Internal && p.Cost <= money && p.name==purchaseName);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="money"></param>
    /// <param name="category"></param>
    /// <returns></returns>
    public bool CheskInternalPurchaseByCategory(int money, string category)
    {
        return _products.Any(p => p.Internal && p.Cost <= money && p.Category == category);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="category"></param>
    /// <returns></returns>
    public List<InAppProduct> GetProductsByCategory(string category)
    {
        return _products.Where(p => p.Category == category).ToList();
    }
}

#endif