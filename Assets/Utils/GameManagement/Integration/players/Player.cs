﻿using System;
using UnityEngine;
using System.Collections;

public class Player
{
    //--------------------------------------------------------
    // Social data
    //--------------------------------------------------------

    // Don't access directly (Icon can be null). Use bottom function.
    private Texture Icon;
    public string PatchIcon;
    public string UserName;
    public string UserForname;
    public bool InGame;

    //--------------------------------------------------------
    // Remote Base Data
    //--------------------------------------------------------

    public int LiveCount=1;
    public int LevelIndex=1;
    public int Score=0;
    public int WallPassLevel = 15;

    //--------------------------------------------------------
    // Destroy
    //--------------------------------------------------------

    public void Destroy()
    {
        Icon = null;
    }

    public void LoadIcon(Action<Texture> func)
    {
        if (Icon != null)
        {
            func(Icon);
            return;
        }
        new LoadRemoteSource().LoadImgFromURL(PatchIcon, delegate(Texture texture)
        {
            Icon = texture;
            func(texture);
        });
    }
}
