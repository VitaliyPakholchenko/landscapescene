﻿using UnityEngine;

namespace ADS
{	
    public abstract class ADHost : MonoBehaviour
    {
        public delegate void ADEvent();

        public event ADEvent onRewardedVideoAdOpened;
        public event ADEvent onRewardedVideoAdClosed;

        public event ADEvent onRewardedVideoStart;
        public event ADEvent onRewardedVideoStop;
        public event ADEvent onRewardedVideoDidFinished;

        virtual protected void OnRewardedVideoAdOpened()
        {
            Debug.Log("RewardedVideoAdOpened");

            if (onRewardedVideoAdOpened != null)
            {
                onRewardedVideoAdOpened();
            }
        }

        virtual protected void OnRewardedVideoAdClosed()
        {
            Debug.Log("RewardedVideoAdClosed");

            if (onRewardedVideoAdClosed != null)
            {
                onRewardedVideoAdClosed();
            }
        }

        virtual protected void OnVideoStart()
        {
            Debug.Log("RewardedVideoStart");

            if (onRewardedVideoStart != null)
            {
                onRewardedVideoStart();
            }
        }

        virtual protected void OnVideoEnd()
        {
            Debug.Log("RewardedVideoEnd");

            if (onRewardedVideoStop != null)
            {
                onRewardedVideoStop();
            }
        }

        virtual protected void OnRewardedVideoFinished()
        {
            Debug.LogFormat("RewardedVideoFinished");

            if (onRewardedVideoDidFinished != null)
            {
                onRewardedVideoDidFinished();
            }
        }
    }
}
