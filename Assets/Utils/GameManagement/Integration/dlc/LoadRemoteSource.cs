﻿using System;
using UnityEngine;
using System.Collections;

public class LoadRemoteSource
{
    public void LoadImgFromURL(string imgURL, Action<Texture> callback)
    {
        // Need to use a Coroutine for the WWW call, using Coroutiner convenience class
        Coroutiner.StartCoroutine(
            LoadImgEnumerator(imgURL, callback)
        );
    }

    IEnumerator LoadImgEnumerator(string imgURL, Action<Texture> callback)
    {
        WWW www = new WWW(imgURL);
        yield return www;

        if (www.error != null)
        {
            Debug.LogError(www.error);
            yield break;
        }

        callback(www.texture); 
    }
}
