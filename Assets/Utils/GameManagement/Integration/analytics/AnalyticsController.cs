﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;

public class AnalyticsController
{
    private bool isValidState = false;
    private double sessionTime = 0.0f;
    private int sessionStartedWithLevelIndex = 0;
    
    // Events
    public const string EVENT_APPVERSION = "game_version";
    public const string PARAM_APPVER = "version";

    public Gender UserGender { get; set; }
    public int UserAge { get; set; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="integration"></param>
    public void Initialize(IGameManager gm)
    {
        UserAge = 0;
        UserGender = UnityEngine.Analytics.Gender.Unknown;

        Analytics.SetUserId(gm.UserId);
        Analytics.CustomEvent(EVENT_APPVERSION, new Dictionary<string, object>
        {
            {PARAM_APPVER, Application.version}
        });

        UpdateUserInfo();

        isValidState = true;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userAge"></param>
    /// <param name="userGender"></param>
    public void SetUserInfo(int userAge, UnityEngine.Analytics.Gender userGender)
    {
        //configuration.UserAge = userAge;
        //configuration.UserGender = userGender;
        //UpdateUserinfo();
    }

    /// <summary>
    /// 
    /// </summary>
    public void UpdateUserInfo()
    {
        if (!isValidState)
            return;

        Debug.Log("AnalyticsController UpdateUserinfo:: ");
        
        Analytics.SetUserGender(UserGender);
        Analytics.SetUserBirthYear(System.DateTime.Now.Year - UserAge);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="analyticsEvent"></param>
    public void ApplayEvent(IAnalyticsEvent analyticsEvent)
    {
        if (!isValidState)
            return;
        Debug.Log("AnalyticsController EventApplay: " + analyticsEvent.GetType().Name);
        analyticsEvent.Applay(this);
    }
}




/*
#if UNITY_ANALYTICS

using System;
using UnityEngine;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class AnalyticsUnity
{
    // #3 block
    public void EventSessionStart(int currentLevelIndex)
    {
        if (!isValidState)
            return;

		UnityEngine.Analytics.Analytics.CustomEvent(AnalyticsSettings.EVENT_SESSION_START, Vector3.one);

        sessionTime = Time.realtimeSinceStartup;
        sessionStartedWithLevelIndex = currentLevelIndex;
    }

    public void EventSessionEnd(int currentLevelIndex)
    {
        if (!isValidState)
            return;

        double fullSessionTime = Time.realtimeSinceStartup - sessionTime;
        int levelCompletedDuringSession = Math.Abs(currentLevelIndex - sessionStartedWithLevelIndex);

        UnityEngine.Analytics.Analytics.CustomEvent(AnalyticsSettings.EVENT_SESSION_END, new Dictionary<string, object>
        {
            {AnalyticsSettings.PARAM_TIME, fullSessionTime},
            {AnalyticsSettings.PARAM_LEVELS_COMPLETED, levelCompletedDuringSession}
        });
    }

    // #4 block
    public void EventTollBoothPassed(int levelIndex, WallPassReason reson)
    {
        if (!isValidState)
            return;

        string tbID = AnalyticsController.Instance.GetCurrentLevelNameByIndex(levelIndex);
        UnityEngine.Analytics.Analytics.CustomEvent(AnalyticsSettings.EVENT_TOLLBOOTH_PASSED, new Dictionary<string, object>
        {
            { AnalyticsSettings.PARAM_TOLLBOOTHID, tbID },
            { AnalyticsSettings.PARAM_REASON_PTB, reson }
        });
    }


    // #5 block
    public void EventIncreaseGems(int levelIndex, CurrencyAddReason reason, int value)
    {
        if (!isValidState)
            return;
        string levelID = AnalyticsController.Instance.GetCurrentLevelNameByIndex(levelIndex);
        UnityEngine.Analytics.Analytics.CustomEvent(AnalyticsSettings.EVENT_INCREASE_GEMS, new Dictionary<string, object>
        {
            { AnalyticsSettings.PARAM_LEVEL_ID, levelID },
            { AnalyticsSettings.PARAM_REASON_CAR, reason },
            { AnalyticsSettings.PARAM_AMOUNT, value }
        });
    }

    public void EventDecreaseGems(int levelIndex, CurrencySubReason reason, int value)
    {
        if (!isValidState)
            return;
        string levelID = AnalyticsController.Instance.GetCurrentLevelNameByIndex(levelIndex);
        UnityEngine.Analytics.Analytics.CustomEvent(AnalyticsSettings.EVENT_DECREASE_GEMS, new Dictionary<string, object>
        {
            { AnalyticsSettings.PARAM_LEVEL_ID, levelID },
            { AnalyticsSettings.PARAM_REASON_CSR, reason },
            { AnalyticsSettings.PARAM_AMOUNT, value }
        });

    }

    // #6 block
    public void EventFirstGemsAdd(int levelIndex, int value)
    {
        if (!isValidState)
            return;
        string levelID = AnalyticsController.Instance.GetCurrentLevelNameByIndex(levelIndex);
        UnityEngine.Analytics.Analytics.CustomEvent(AnalyticsSettings.EVENT_FIRST_GEMSADD, new Dictionary<string, object>
        {
            { AnalyticsSettings.PARAM_LEVEL_ID, levelID },
            { AnalyticsSettings.PARAM_AMOUNT, value.ToString() }
        });
    }

    public void EventFirstGemsSub(int levelIndex, int value)
    {
        if (!isValidState)
            return;
        string levelID = AnalyticsController.Instance.GetCurrentLevelNameByIndex(levelIndex);
        UnityEngine.Analytics.Analytics.CustomEvent(AnalyticsSettings.EVENT_FIRST_GEMSSUB, new Dictionary<string, object>
        {
            { AnalyticsSettings.PARAM_LEVEL_ID, levelID },
            { AnalyticsSettings.PARAM_AMOUNT, value.ToString() }
        });
    }

    // #7 block
    public void EventUI(string uiWindow, string uiElement)
    {
        if (!isValidState)
            return;
        if (string.IsNullOrEmpty(uiElement))
            UnityEngine.Analytics.Analytics.CustomEvent(AnalyticsSettings.EVENT_UIEVENT_PREFIX + uiWindow, null);
        else
            UnityEngine.Analytics.Analytics.CustomEvent(AnalyticsSettings.EVENT_UIEVENT_PREFIX + uiWindow, new Dictionary<string, object>
            {
                { AnalyticsSettings.PARAM_UIELEMENT, uiElement }
            });
    }       
}
#endif*/
