﻿#if USING_FACEBOOK

using UnityEngine;
using System;
using System.Collections.Generic;
using Facebook.Unity;
using System.Linq;

// Class responsible for Facebook Graph API calls in Friend Smash!
//
// The Facebook Graph API allows us to fetch information about the player and their friends for the permissions
// they grant with Facebook Login.
// We can use this data to provide a set of real people to play against, showing names and pictures
// of the player's friends to make the game experience feel even more personal.
//
// For more details on the Graph API see: https://developers.facebook.com/docs/graph-api/overview
// See https://developers.facebook.com/docs/unity/reference/current/FB.API for Unity specific details
public static class FBGraph
{
#region PlayerInfo
    // Once a player successfully logs in, we can welcome them by showing their name
    // and profile picture on the home screen of the game. This information is returned
    // via the /me/ endpoint for the current player. We'll call this endpoint via the
    // SDK and use the results to personalize the home screen.
    //
    // Make a Graph API GET call to /me/ to retrieve a player's information
    // See: https://developers.facebook.com/docs/graph-api/reference/user/

    public static event Action OnUpdateUsersData;
    public static event Action<Player> OnCompleteGetPlayer;
	public static event Action OnFinishedGetFriends;

    public static void GetPlayerInfo()
    {
        string queryString = "/me?fields=id,first_name,picture.width(120).height(120)";
        FB.API(queryString, HttpMethod.GET, GetPlayerInfoCallback);
    }

    private static void GetPlayerInfoCallback(IGraphResult result)
    {
        Debug.Log("GetPlayerInfoCallback");
        if (result.Error != null)
        {
            Debug.LogError(result.Error);
            return;
        }
        Debug.Log(result.RawResult);

        Player nPlayer = new Player();

        // Save player name
        string name;
        if (result.ResultDictionary.TryGetValue("first_name", out name))
            nPlayer.UserName = name;

        // save Player id
        string id;
        if (result.ResultDictionary.TryGetValue("id", out id))
            nPlayer.UserID = id;

        //Fetch player profile picture from the URL returned

        string playerImgUrl = GraphUtil.DeserializePictureURL(result.ResultDictionary);

        if (playerImgUrl != null)
            nPlayer.PatchIcon = playerImgUrl;

        // GraphUtil.LoadImgFromURL(playerImgUrl, delegate(Texture pictureTexture)

      /*  new LoadRemoteSource().LoadImgFromURL(playerImgUrl, delegate (Texture pictureTexture)
        {
            // Setup the User's profile picture
            if (pictureTexture != null)
                nPlayer.Icon = pictureTexture;

            if (OnCompleteGetPlayer != null)
                OnCompleteGetPlayer(nPlayer);
        });*/

        if (OnCompleteGetPlayer != null)
            OnCompleteGetPlayer(nPlayer);
        if (OnUpdateUsersData != null)
            OnUpdateUsersData();



   
    }

    // In the above request it takes two network calls to fetch the player's profile picture.
    // If we ONLY needed the player's profile picture, we can accomplish this in one call with the /me/picture endpoint.
    //
    // Make a Graph API GET call to /me/picture to retrieve a players profile picture in one call
    // See: https://developers.facebook.com/docs/graph-api/reference/user/picture/
    public static void GetPlayerPicture()
    {
        FB.API(GraphUtil.GetPictureQuery("me", 128, 128), HttpMethod.GET, delegate(IGraphResult result)
        {
            Debug.Log("PlayerPictureCallback");
            if (result.Error != null)
            {
                Debug.LogError(result.Error);
                return;
            }
            if (result.Texture ==  null)
            {
                Debug.Log("PlayerPictureCallback: No Texture returned");
                return;
            }

            // Setup the User's profile picture
           // Integration.Instance.Facebook.Player.Icon = result.Texture;

            // Redraw the UI
           // Integration.Instance.Facebook.UpdateFBData();
        });
    }
#endregion

#region Friends
    // We can fetch information about a player's friends via the Graph API user edge /me/friends
    // This endpoint returns an array of friends who are also playing the same game.
    // See: https://developers.facebook.com/docs/graph-api/reference/user/friends
    //
    // We can use this data to provide a set of real people to play against, showing names
    // and pictures of the player's friends to make the experience feel even more personal.
    //
    // The /me/friends edge requires an additional permission, user_friends. Without
    // this permission, the response from the endpoint will be empty. If we know the user has
    // granted the user_friends permission but we see an empty list of friends returned, then
    // we know that the user has no friends currently playing the game.
    //
    // Note:
    // In this instance we are making two calls, one to fetch the player's friends who are already playing the game
    // and another to fetch invitable friends who are not yet playing the game. It can be more performant to batch 
    // Graph API calls together as Facebook will parallelize independent operations and return one combined result.
    // See more: https://developers.facebook.com/docs/graph-api/making-multiple-requests
    //

    public static event Action<Player> OnCompleteGetFriend;

  
    public static void GetFriends ()
    {
        string queryString = "/me/friends?fields=id,first_name,picture.width(128).height(128)&limit=100";
        FB.API(queryString, HttpMethod.GET, GetFriendsCallback);
    }

    private static void GetFriendsCallback(IGraphResult result)
    {
        if (result.Error != null)
        {
            Debug.LogError(result.Error);
            return;
        }
        Debug.Log(result.RawResult);

        // Store /me/friends result

        object dataList;
        if (result.ResultDictionary.TryGetValue("data", out dataList))
        {
            var friendsList = (List<object>) dataList;
            var counter = 0;
            foreach (var friendItem in friendsList)
            {
                var entry = (Dictionary<string, object>) friendItem;
                string userId = (string)entry["id"];
                Player nPlayer = new Player();
                nPlayer.UserID = userId;
                nPlayer.InGame = true;
                nPlayer.UserName = (string)entry["first_name"];

                object pictureObj;
                if (entry.TryGetValue("picture", out pictureObj))
                {
                    var pictureData = (Dictionary<string, object>)(((Dictionary<string, object>)pictureObj)["data"]);
                    
                    string userImgUrl = (string)pictureData["url"];
                    nPlayer.PatchIcon = userImgUrl;
                }

              if (OnCompleteGetFriend != null)
                    OnCompleteGetFriend(nPlayer);

                if (OnUpdateUsersData != null)
                    OnUpdateUsersData();

                // Limit friend list to 50 people, so that FB won't freeze on logging in.
                counter++;
                if(counter > 50)
                    break;
            }
        }
		if (OnFinishedGetFriends != null)
			OnFinishedGetFriends();
    }

   
    // We can fetch information about a player's friends who are not yet playing our game
    // via the Graph API user edge /me/invitable_friends
    // See more about Invitable Friends here: https://developers.facebook.com/docs/games/invitable-friends
    //
    // The /me/invitable_friends edge requires an additional permission, user_friends.
    // Without this permission, the response from the endpoint will be empty.
    //
    // Edge: https://developers.facebook.com/docs/graph-api/reference/user/invitable_friends
    // Nodes returned are of the type: https://developers.facebook.com/docs/graph-api/reference/user-invitable-friend/
    // These nodes have the following fields: profile picture, name, and ID. The ID's returned in the Invitable Friends
    // response are not Facebook IDs, but rather an invite tokens that can be used in a custom Game Request dialog.
    //
    // Note! This is different from the following Graph API:
    // https://developers.facebook.com/docs/graph-api/reference/user/friends
    // Which returns the following nodes:
    // https://developers.facebook.com/docs/graph-api/reference/user/
    //
    public static void GetInvitableFriends ()
    {
        string queryString = "/me/invitable_friends?fields=id,first_name,picture.width(128).height(128)&limit=100";
        FB.API(queryString, HttpMethod.GET, GetInvitableFriendsCallback);
    }
    
    private static void GetInvitableFriendsCallback(IGraphResult result)
    {
        if (result.Error != null)
        {
            Debug.LogError(result.Error);
            return;
        }
        Debug.Log(result.RawResult);

        // Store /me/friends result

        object dataList;
        if (result.ResultDictionary.TryGetValue("data", out dataList))
        {
            var friendsList = (List<object>)dataList;

            foreach (var friendItem in friendsList)
            {
                var entry = (Dictionary<string, object>)friendItem;
                string userId = (string)entry["id"];
                Player nPlayer = new Player();
                nPlayer.UserID = userId;
                nPlayer.InGame = false;
                nPlayer.UserName = (string)entry["first_name"];

                object pictureObj;
                if (entry.TryGetValue("picture", out pictureObj))
                {
                    var pictureData = (Dictionary<string, object>)(((Dictionary<string, object>)pictureObj)["data"]);

                    string userImgUrl = (string)pictureData["url"];
                    nPlayer.PatchIcon = userImgUrl;
                }

                if (OnCompleteGetFriend != null)
                    OnCompleteGetFriend(nPlayer);

                if (OnUpdateUsersData != null)
                    OnUpdateUsersData();
            }
        }
		if (OnFinishedGetFriends != null)
			OnFinishedGetFriends();

    }
#endregion
}
#endif