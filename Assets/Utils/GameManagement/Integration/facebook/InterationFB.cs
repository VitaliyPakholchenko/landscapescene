﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

#if USING_FACEBOOK
using Facebook.Unity;
using Facebook.Unity.Example;

public class InterationFB
{
    // flags
    public bool FBActivation;

    // --------------------------------------------------

    public event Action OnFBActivation;
    public event Action OnFBLogined;
    public event Action OnFBUnlogined;

    public event Action<IAppInviteResult> OnFBInviteRequest;
    public event Action<IAppRequestResult> RequestResult;
    public event Action<IShareResult> OnFBSahreRequest;
    public event Action OnFBFriends;
    public event Action OnUpdateSocial;
    



    // --------------------------------------------------------------------------- 
    // User and friends
    // --------------------------------------------------------------------------- 
    public Player Player {get; set;}
    public readonly List<Player> Friends = new List<Player>();

	public event Action<Player> OnGetPlayer = delegate {};
	public event Action<Player> OnGetFriend = delegate {};
	public event Action<Player> OnGetInvitableFriend = delegate {};


	//---------------------------------------------------------
	// Lives, Toll booth events
	//---------------------------------------------------------
	// id, from, friend_id
	public event Action<string, string, string> OnLife = delegate {};
	// id, from, friend_id, level
	public event Action<string, string, string, int> OnPass = delegate {};
	// id, from, friend_id
	public event Action<string, string, string> OnAskLife = delegate {};
	// id, from, friend_id, level
	public event Action<string, string, string, int> OnAskPass = delegate {};


	//---------------------------------------------------------
	// Objects ID 
	//---------------------------------------------------------

	private const string LifeID = "1042862902465980";
	private const string PassID = "137567923317144";

	public const string ASK_LIFE = "alife";
	public const string ASK_PASS = "apass";
	public const string SEND_LIFE = "slife";
	public const string SEND_PASS = "spass";

	//---------------------------------------------------------
	// privates
	//--------------------------------------------------------

	private float GetApprequestsTimer = 0.0f;
	private const float GetApprequestsDelta = 3.0f;

    //---------------------------------------------------------
    // Destroy
    //--------------------------------------------------------

    public void Destroy()
    {
        OnFBActivation = null;
        OnFBLogined = null;
        OnFBUnlogined = null;

        OnUpdateSocial = null;
		OnFBFriends = null;
        FBActivation = false;

		OnGetPlayer = null;
		OnGetFriend = null;
		OnGetInvitableFriend = null;
    }

	public bool IsLoggedIn { 
		get
		{
		    if (Integration.Instance.InternetAvailable)
		        return FB.IsLoggedIn;
		    else
		        return false;
		}
	}

    //---------------------------------------------------------
    // Initialization FB 
    //---------------------------------------------------------

    public void Intialize()
    {
		Debug.Log("FB: Start init FB");

		if (!FB.IsInitialized) 
		{
			Debug.Log("FB:  is Not init");

			FB.Init (InitCallback, OnHideUnity);
		}
		else 
		{
			Debug.Log("FB:  already initialized");
			FB.ActivateApp ();
			FBActivation = true;
			InitCallback();
		}
    }

    public void GamePause()
    {
        if (FB.IsInitialized)
        {
            // App Launch events should be logged on app launch & app resume
            // See more: https://developers.facebook.com/docs/app-events/unity#quickstart
            FBAppEvents.LaunchEvent();
        }
        else
        {
            FB.Init(InitCallback);
        }
    }

    //---------------------------------------------------------
    // Init FB
    //---------------------------------------------------------

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
			Debug.Log("FB: initialized");
			Debug.Log(String.Format("FB:  Is login: {0}", FB.IsLoggedIn));

            GUI.enabled = true;

            if (OnFBActivation != null)
            {
                OnFBActivation();
                OnFBActivation = null;
            }

            FBActivation = true;

			if (IsLoggedIn) 
				Login();
        }
        else
        {
			Debug.Log("FB: Failed to Initialize the Facebook SDK");
            FBActivation = false;
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    //---------------------------------------------------------
    // Login Logout 
    //---------------------------------------------------------

    public void Login()
    {
        /*if(GameGlobals.Instance.Integration.HasConnection())
        {
    		if (FB.IsLoggedIn) 
    		{
				Debug.Log("FB: Login: Already logged in");
    			if(OnFBLogined!=null)
    				OnFBLogined();
    			
    			OnFBLogined = null;
    			LoginReadCallback(null);

    			return;
    		}
			Debug.Log("FB: Login with read permissions");
            var perms = new List<string>() {"public_profile", "email", "user_friends"};
    		FB.LogInWithReadPermissions(perms, LoginReadCallback);
            //FB.LogInWithPublishPermissions(new List<string>() { "publish_actions" }, AuthCallback);
        }*/
    }

	private void LoginReadCallback(ILoginResult result)
	{
		var error = (result!=null) ? result.Error : null;
		if (!string.IsNullOrEmpty (error)) 
		{
			Debug.Log(string.Format("FB: Login error: {0}", error));
		}

		if(IsLoggedIn)
		{
			var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
			string perm = null;
			if (aToken.Permissions != null)
				foreach (var p in aToken.Permissions) 
				{
					if (string.IsNullOrEmpty (p) || p == "publish_actions") 
					{
						if(string.IsNullOrEmpty (p))
							Debug.Log("FB: empty permissions");
						else
							Debug.Log("FB: publish permission is found");

						perm = p;
						break;
					}
				}

			if(!String.IsNullOrEmpty(perm))
			{
				Debug.Log("FB: FB already has publish permision");
				AuthCallback(null);
				return;
			}

			//Debug.Log("FB: Login with publish permition");
			//FB.LogInWithPublishPermissions(new List<string>() { "publish_actions" }, AuthCallback);
			AfterLoggedIn();
		}
	}

    private void AuthCallback(ILoginResult result)
    {
		var error = (result!=null) ? result.Error : null;
		if (!string.IsNullOrEmpty (error)) 
			Debug.Log(string.Format("FB: Login publish error: {0}", error));
		else
			Debug.Log(string.Format("FB: Login publish success"));

        if (FB.IsLoggedIn)
        {
			AfterLoggedIn();
        }
 		if(result!= null && result.Cancelled)
        {
			Debug.Log("FB: User cancelled login");
        }
    }

	private void AfterLoggedIn()
	{
		// AccessToken class will have session details
		var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
		// Print current access token's User ID
		Debug.Log(aToken.UserId);
		// Print current access token's granted permissions
		Debug.Log("FB: granted permissions");
		foreach(string perm in aToken.Permissions)
		{
			Debug.Log(perm);
		}

		Debug.Log("FB: call OnFBLogined");
		if (OnFBLogined != null)
		{
			OnFBLogined();
			OnFBLogined = null;
		}
		GetFriends();
	}

	public void GetFriends()
	{
		Debug.Log("FB: GetFriends");
		FBGraph.OnUpdateUsersData += UpdateFBData;

		Friends.Clear();

		FBGraph.OnCompleteGetPlayer += (Player nPlayer) => 
		{ 
			Player = nPlayer; 
			OnGetPlayer(nPlayer); 
		}; 

        FBGraph.GetPlayerInfo();
		FBGraph.OnCompleteGetFriend += getFriend;
		FBGraph.OnFinishedGetFriends += finishedGetFriends;
			
		FBGraph.GetFriends();
	}

    private void getFriend(Player player)
    {
		if(player == null)
			return;
		
		Debug.Log( String.Format("FB: getFriends: {0}, inGame: {1}", player.UserName, player.InGame));

        Friends.Add(player);
        OnGetFriend(player);
    }

    private void getInvFriend(Player player)
    {		
		if(player == null)
			return;

		Debug.Log( String.Format("FB: getFriends: {0}, inGame: {1}", player.UserName, player.InGame));

        Friends.Add(player);
        OnGetInvitableFriend(player);
    }

    private void finishedGetInvFriends()
    {
		Debug.Log("FB: finishedGetInvFriends");
        FBGraph.OnCompleteGetFriend -= getInvFriend;
        FBGraph.OnFinishedGetFriends -= finishedGetInvFriends;
    }

    private void finishedGetFriends()
    {
        FBGraph.OnCompleteGetFriend -= getFriend;
        FBGraph.OnFinishedGetFriends -= finishedGetFriends;
        FBGraph.OnCompleteGetFriend += getInvFriend;
        FBGraph.OnFinishedGetFriends += finishedGetInvFriends;
		Debug.Log("FB: GetInvFriends");
        FBGraph.GetInvitableFriends();
    }

    public void LogOut()
    {
        FB.LogOut();
		Friends.Clear();
        if (OnFBUnlogined != null)
        {
            OnFBUnlogined();
            OnFBUnlogined = null;
        }
    }

    //---------------------------------------------------------
    // Wall Post
    //---------------------------------------------------------

	public void AskTollBoothPass(List<string> friendsid, string message, int level)
    {
		if (!FB.IsLoggedIn)
			return;

		Debug.Log("FB: FB.Apprequest started");

		FB.AppRequest(
			message: message, 
			actionType: OGActionType.ASKFOR,
			objectId: PassID,
			to: friendsid,
			data: ASK_PASS +"#"+level.ToString(),
			title: "Ask friends for pass",
			callback: AppRequestFeedSareCallBack
		);
    }

	public void SendTollBoothPass(List<string> friendsid, string message, int level)
	{
		if (!FB.IsLoggedIn)
			return;

		Debug.Log("FB: FB.Apprequest started");

		FB.AppRequest(
			message: message, 
			actionType: OGActionType.SEND,
			objectId: PassID,
			to: friendsid,
			data: SEND_PASS+"#"+level.ToString(),
			title: "Send a pass to friend",
			callback: AppRequestFeedSareCallBack
		);
	}


    public void AskLife(List<string> friendsid, string message, DateTime time)
    {
        if (!FB.IsLoggedIn)
            return;

		Debug.Log("FB: FB.Apprequest started");

		FB.AppRequest(
			message: message, 
			actionType: OGActionType.ASKFOR,
			objectId: LifeID,
			to: friendsid,
			data: ASK_LIFE, 
			title: "Ask friends for lives",
			callback: AppRequestFeedSareCallBack
		);
    }

	public void SendLife(List<string> friendsid, string message, DateTime time)
	{
		if (!FB.IsLoggedIn)
			return;

		Debug.Log("FB: FB.send live started");

		FB.AppRequest(
			message: message, 
			actionType: OGActionType.SEND,
			objectId: LifeID,
			to: friendsid,
			data: SEND_LIFE, 
			title: "Send a life to friend",
			callback: AppRequestFeedSareCallBack
		);
	}

    //---------------------------------------------------------
    // Invite
    //---------------------------------------------------------

    public void Invite(List<string> idUsers = null)
    {
        if (!FB.IsLoggedIn)
            return;

        //https://developers.facebook.com/docs/unity/reference/current/FB.AppRequest

        var title = Localization.GetLocalizedString("key_gameinv");
        var message = Localization.GetLocalizedString("key_jointofun");

        FB.AppRequest(
                 message,
                 idUsers,
                 null,
                 null,
                 null,
                 string.Empty,
                 title,
                 AppRequestFeedSareCallBack
                 );
    }

    //---------------------------------------------------------
    // Mobile
    //---------------------------------------------------------

    public void MobileInvite( Uri appLinkUrl, Uri previewImageUrl = null)
    {
        if (!FB.IsLoggedIn)
            return;

        //https://developers.facebook.com/docs/unity/reference/current/FB.Mobile.AppInvite
        FB.Mobile.AppInvite(appLinkUrl, previewImageUrl, AppRequestInviteCallBack);
    }

    //---------------------------------------------------------
    // FeedShare
    //---------------------------------------------------------

    public void FeedShare(string toId, string title, string description, Uri pictUrl = null, Uri link = null)
    {
        if (!FB.IsLoggedIn)
            return;

        //https://developers.facebook.com/docs/unity/reference/current/FB.FeedShare

        FB.FeedShare(
            toId,
            link, 
            title,
            "",
            description,
            pictUrl,
            "",
            AppRequestFeedSareCallBack);
    }


    //---------------------------------------------------------
    // REQUESTS
    //---------------------------------------------------------

    private void AppRequestFeedSareCallBack(IAppRequestResult result)
    {
        if (result.Error != null)
        {
            Debug.LogError(result.Error);
            return;
        }

        object obj;

        if (result.ResultDictionary.TryGetValue("cancelled", out obj))
        {
			Debug.Log("FB: Request cancelled");
        }
        else if (result.ResultDictionary.TryGetValue("request", out obj))
        {
			Debug.Log("FB: Request sent");
        }

        if (RequestResult != null)
            RequestResult(result);
    }

    // -------------------------------------------

    private void AppRequestInviteCallBack(IAppInviteResult result)
    {
        if (result.Error != null)
        {
            Debug.LogError(result.Error);
            return;
        }

        object obj;

        if (result.ResultDictionary.TryGetValue("cancelled", out obj))
        {
			Debug.Log("FB: Invite cancelled");
        }
        else if (result.ResultDictionary.TryGetValue("request", out obj))
        {
			Debug.Log("FB: Invite sent");   
        }

        if (OnFBInviteRequest != null)
            OnFBInviteRequest(result);
    }

    // -------------------------------------------

    private void AppRequestFeedSareCallBack(IShareResult result)
    {
        if (result.Error != null)
        {
            Debug.LogError(result.Error);
            return;
        }

        object obj;

        if (result.ResultDictionary.TryGetValue("cancelled", out obj))
        {
			Debug.Log("FB: FeedSare cancelled");
        }
        else if (result.ResultDictionary.TryGetValue("request", out obj))
        {
			Debug.Log("FB: FeedSare sent");
        }

        if (OnFBSahreRequest != null)
            OnFBSahreRequest(result);
    }

    // --------------------------------------------------------------
    public void UpdateFBData()
    {
        if (OnUpdateSocial != null)
            OnUpdateSocial();
    }

    public void Connect(Action CompleteConnect = null)
    {
        if (!Integration.Instance.InternetAvailable)
            return;

        if (!IsLoggedIn)
        {
            if (!FBActivation)
            {
                OnFBActivation += delegate
                {
                    if (CompleteConnect != null)
                        OnUpdateSocial += CompleteConnect;

                    Login();
                };

                Intialize();
            }
            else
            {
                if (CompleteConnect != null)
                    OnUpdateSocial += CompleteConnect;

                Login();
            }
        }
    }

    private void GetApprequests()
	{
		string queryString = "/me/apprequests";
		FB.API(queryString, HttpMethod.GET, GetApprequestsCallback);
	}

	private void GetApprequestsCallback(IGraphResult result)
	{
		Debug.Log("FB: GetApprequestsCallback callback");

		if(!String.IsNullOrEmpty(result.Error))
		{
			Debug.Log("FB: GetApprequestsCallback error: " +result.Error);
			return;
		}	
			
		object dataList;
		if (!result.ResultDictionary.TryGetValue("data", out dataList))
		{
			Debug.Log("FB: GetApprequestsCallback: no data in result");
			return;
		}

		var list = (List<object>) dataList;
		Debug.Log(dataList.ToString());
		foreach (var item in list)
		{
			var entry = (Dictionary<string, object>) item;
			Debug.Log(item.ToString());
			var type = (string)entry["data"];
			var id = (string)entry["id"];

			if(String.IsNullOrEmpty(type) || String.IsNullOrEmpty(id))
				continue;

			var tmp1 = (Dictionary<string, object>)entry["from"];
			string name = String.Empty;
			string friendId = String.Empty;
			if(tmp1!=null)
			{
				name = (string)tmp1["name"];
				friendId = (string)tmp1["id"];
			}

			if(type == ASK_LIFE)
			{
				OnAskLife(id, name, friendId);
			}
			else if(type== SEND_LIFE)
			{
				OnLife(id, name, friendId);
			}
			else if(type.StartsWith(ASK_PASS))
			{
				var level = getLevelFromData(type);
				if(level<0)
					continue;

				OnAskPass(id, name, friendId, level);
			} 
			else if(type.StartsWith(ASK_PASS))
			{
				var level = getLevelFromData(type);
				if(level<0)
					continue;

				OnPass(id, name, friendId, level);
			} 

			Debug.Log("FB: GetApprequestsCallback: Received "+type);
		}

	}
		
	private int getLevelFromData(string data)
	{
		int index = data.LastIndexOf("#");
		if(index<0)
			return -1;

		var levelStr = data.Substring(index+1);
		int level = -1;
		if(!int.TryParse(levelStr, out level))
			return -1;

		return level;
	}

	public void DeleteApprequest(string id)
	{
		if(String.IsNullOrEmpty(id))
			return;

		FB.API(id, HttpMethod.DELETE, null); 
	}

	public void Update(float delta)
	{
		if(!IsLoggedIn || !Friends.Any())
			return;

		GetApprequestsTimer-= delta;

		if(GetApprequestsTimer <=0.0)
		{
			GetApprequestsTimer = GetApprequestsDelta;
			GetApprequests();
		}
	}
}
#endif