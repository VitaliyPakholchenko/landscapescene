﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

#if false
public class Inbox 
{
	// Types
	public enum Type
	{
		UNKNOWN,
		LIFE,
		PASS,
		ASK_LIFE,
		ASK_PASS
	}

	public struct Entry
	{
		public string Id;
		public string From;
		public string FriendId;
		public Type Type; 
		public string Body;

		public Entry(string id, string from, string friendId, Type type, string body)
		{
			Id = id;
			From = from;
			FriendId = friendId;
			Type = type;
			Body = body;
		}

		public Entry(string str)
		{
			//TODO: need to use json/xml
			var items = str.Split(';');
			Id = items.Length >= 1 ? items[0] : string.Empty;
			From = items.Length >= 2 ? items[1] : string.Empty;
			FriendId = items.Length >= 3 ? items[2] : string.Empty;

			int type;
			Type = items.Length >= 4 ? (Type)( int.TryParse(items[3], out type) ? type: (int) Type.UNKNOWN ) : Type.UNKNOWN;
			Body = items.Length >= 5 ? items[4] : string.Empty;
		}

		public override string ToString ()
		{
			//TODO: need to use json/xml
			return string.Format("{0};{1};{2};{3};{4}", 
				String.IsNullOrEmpty(Id) ? string.Empty : Id,
				String.IsNullOrEmpty(From) ? string.Empty : From,
				String.IsNullOrEmpty(FriendId) ? string.Empty : FriendId,
				((int)Type).ToString(),
				String.IsNullOrEmpty(Body) ? string.Empty : Body
			);
		}
	}

	//Consts
	const string PROFILE_KEY = "inbox";

	//Data

	private List<Entry> InboxList = new List<Entry>();
	private InterationFB Facebook;

	// Events
	public event Action<Entry> OnEntryAdded = delegate {};
	public event Action<Entry> OnEntryConsumed = delegate {};

	// Serialize

	public void Initialize()
	{
		Facebook = Integration.Facebook;
		var strings = Profiles.GetStringArray(PROFILE_KEY);

		foreach(var s in strings)
		{
			InboxList.Add(new Entry(s));
		}
#if UNITY_EDITOR
		InboxList.Clear();

		if(InboxList.Count == 0)
		{
			OnLife("1", "Friend 1", "Friend 1");
			OnLife("2", "Friend 2", "Friend 2");
			OnLife("3", "Friend 3", "Friend 3");


			OnPass("4", "Friend 4", "Friend 4", 1);
			OnPass("5", "Friend 5", "Friend 5", 1);
			OnPass("6", "Friend 6", "Friend 6", 1);

			OnAskLife("7", "Friend 7", "Friend 7");
			OnAskLife("8", "Friend 8", "Friend 8");
			OnAskLife("9", "Friend 9", "Friend 9");

			OnAskPass("10", "Friend 10", "Friend 10", 1);
			OnAskPass("11", "Friend 11", "Friend 11", 1);
			OnAskPass("12", "Friend 12", "Friend 12", 1);

		}
#endif
	}

    public void Flush()
    {
        var strings = InboxList.Select(entry => entry.ToString()).ToList();
        Profiles.Set(PROFILE_KEY, strings);
    }

    public void Consume(string id)
    {
        if (InboxList.Count > 0)
        {
            var etr = InboxList.FirstOrDefault(ml => ml.Id == id);
            InboxList.RemoveAt(InboxList.IndexOf(etr));
			OnEntryConsumed(etr);
        }
    }

    // delegates
    public void OnLife(string id, string from, string friendId)
	{
		Debug.Log("Inbox: OnLife");
		var entry = new Entry(id, from, friendId, Type.LIFE, string.Empty);
		InboxList.Insert(0, entry);
		OnEntryAdded(entry);
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
		Facebook.DeleteApprequest(id);
#endif
	}

	public void OnAskLife(string id, string from, string friendId)
	{
		Debug.Log("Inbox: OnAskLife");
		var entry = new Entry(id, from, friendId, Type.ASK_LIFE, string.Empty);
		InboxList.Insert(0, entry);
		OnEntryAdded(entry);
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
		Facebook.DeleteApprequest(id);
#endif
	}

	public void OnPass(string id, string from, string friendId, int level)
	{
		Debug.Log("Inbox: OnPass");
		var entry = new Entry(id, from, friendId, Type.PASS, level.ToString());
		// ignore pass if a friend already sends a pass
		if( InboxList.Exists(e => e.FriendId == friendId) )
			return;

		InboxList.Insert(0, entry);
		OnEntryAdded(entry);
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
		Facebook.DeleteApprequest(id);
#endif
	}

	public void OnAskPass(string id, string from, string friendId, int level)
	{
		Debug.Log("Inbox: OnAskPass");
		var entry = new Entry(id, from, friendId, Type.ASK_PASS, level.ToString());
		InboxList.Insert(0, entry);
		OnEntryAdded(entry);
#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
		Facebook.DeleteApprequest(id);
#endif
	}

	// Empty filter means get all entries
	public List<Entry> GetEntries(HashSet<Type> filter)
	{
		return InboxList.Where(e => filter.Contains(e.Type)).ToList();
	}
}
#endif