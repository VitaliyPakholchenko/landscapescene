﻿using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using System.Data;
using System.Linq;
using Excel;
using System.Reflection;
using UnityEditor;
using System.IO;
#endif

[Serializable]
public class Localization : ScriptableObject
{
    [SerializeField]
    public List<LocalizationLanguageData> Data = new List<LocalizationLanguageData>();
    [SerializeField]
    public List<string> Keys = new List<string>();
    [NonSerialized]
    private LocalizationLanguageData currentLanguageData;

    /// <summary>
    /// 
    /// </summary>
    private static Localization _instance;
    public static Localization Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = Resources.Load<Localization>("localization");
                if (_instance != null)
                {
                    _instance.currentLanguageData = _instance.Data.Find(fLangData => fLangData.Lang == SystemLanguage.English);
                    return _instance;
                }
                _instance = new Localization();
            }
            return _instance;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="lang"></param>
    public void SetLanguage(SystemLanguage lang)
    {
        var newLang = Data.Find(fLangData => fLangData.Lang == lang);
        if (newLang != null)
            currentLanguageData = newLang;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <param name="parameters"></param>
    /// <returns></returns>
    public string Get(string key, params object[] parameters)
    {
        if (currentLanguageData == null)
            return "Language is Unknown";

        var keyIndex = Keys.FindIndex((fkey) => fkey.Equals(key));
        return keyIndex == -1 ? key : string.Format(currentLanguageData.Value[keyIndex], parameters);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public string Get(string key)
    {
        if (currentLanguageData == null)
            return "Language is Unknown";
        var keyIndex = Keys.FindIndex((fkey) => fkey.Equals(key));
        return keyIndex == -1 ? key : currentLanguageData.Value[keyIndex];
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="keys"></param>
    /// <returns></returns>
    public string GetSeveral(string keys)
    {
        if (string.IsNullOrEmpty(keys))
            return keys;

        var messageKeys = keys.Split(' ');
        if (messageKeys.Length == 1)
            return Get(keys);

        var inserts = new List<string>();
        foreach (var messageKey in messageKeys)
        {
            if (messageKey.Contains("~"))
                inserts.Add(messageKey.Remove(0,1));
        }

        var text = string.Empty;
        var insertsIndex = 0;
        foreach (var messageKey in messageKeys)
        {
            if (messageKey.Contains("~"))
                continue;

            if (messageKey.Contains("@"))
            {
                text += " " + inserts[insertsIndex] + " ";
                insertsIndex++;
                continue;
            }

            text += " " + Get(messageKey);
        }

        return text;
    }

    public static string GetLocalizedString(string key)
    {
        return Instance.Get(key);
    }

    public static string GetLocalizedString(string key, object[] parameters)
    {
        return Instance.Get(key);
    }

#if UNITY_EDITOR
    [MenuItem("Localization/ImportLocalization (xls)")]
    static void ImportLocalizationXLS()
    {
        var sourcePath = GetPath();
        var dstPath = EditorUtility.OpenFilePanel("Import localization *.xls", sourcePath, "xls");

        if (!string.IsNullOrEmpty(dstPath))
        {
            using (var stream = new FileStream(dstPath, FileMode.Open))
            {
                var rdr = ExcelReaderFactory.CreateBinaryReader(stream);
                if (rdr == null)
                    return;

                var pathName = "Assets/Resources/localization.asset";

                //if (File.Exists(pathName))
                //    File.Delete(pathName);

                var localization = CreateInstance<Localization>();
                ProjectWindowUtil.CreateAsset(localization, pathName);
                ParseXLS(localization, rdr);
            }
        }
    }

    [MenuItem("Localization/ImportLocalization (xlsx)")]
    static void ImportLocalization()
    {
        var sourcePath = GetPath();
        var dstPath = EditorUtility.OpenFilePanel("Import localization (xlsx)", sourcePath, "xlsx");

        if (!string.IsNullOrEmpty(dstPath))
        {
            using (var stream = new FileStream(dstPath, FileMode.Open))
            {
                var rdr = ExcelReaderFactory.CreateOpenXmlReader(stream);
                if (rdr == null)
                    return;

                var pathName = "Assets/Resources/localization.asset";

                var localization = CreateInstance<Localization>();
                ProjectWindowUtil.CreateAsset(localization, pathName);
                ParseXLSX(localization, rdr);
            }
        }
    }

    [MenuItem("Localization/ExportLocalization")]
    static void ExportLocalization()
    {
    }

    static string GetPath()
    {
        var codeBase = Assembly.GetExecutingAssembly().CodeBase;
        var uri = new UriBuilder(codeBase);
        var path = Uri.UnescapeDataString(uri.Path);
        return Path.GetFullPath(new Uri(Path.Combine(Path.GetDirectoryName(path), "../../")).AbsolutePath);
    }

    private static void ParseXLS(Localization local, IExcelDataReader reader)
    {
        reader.IsFirstRowAsColumnNames = false;
        var spreadsheetDocument = reader.AsDataSet();
        var table = spreadsheetDocument.Tables[0];

        var col_index = 0;
        string current_Key = string.Empty;

        // add language
        foreach (DataColumn column in table.Columns)
        {
            if (col_index < 2)
            {
                col_index++;
                continue;
            }

            current_Key = table.Rows[0][column].ToString();

            if (string.IsNullOrEmpty(current_Key))
                break;

            var lang = SystemLanguage.Unknown;
            try
            {
                lang = (SystemLanguage)Enum.Parse(typeof(SystemLanguage), current_Key);
            }
            catch
            {
                continue;
            }

            var key = new LocalizationLanguageData
            {
                Lang = lang
            };

            local.Data.Add(key);
        }

        if (!local.Data.Any())
            return;

        // add data
        for (var i = 1; i < table.Rows.Count; i++)
        {
            var ckey = table.Rows[i][1].ToString();

            if (string.IsNullOrEmpty(ckey))
                break;

            local.Keys.Add(ckey);

            // add value
            col_index = 2;
            foreach (var cLang in local.Data)
            {
                var cval = table.Rows[i][col_index].ToString();
                cLang.Value.Add(cval);
                col_index++;
            }
        }

        //
        AssetDatabase.Refresh();
        EditorUtility.SetDirty(local);
        AssetDatabase.SaveAssets();
    }

    static void ParseXLSX(Localization local, IExcelDataReader reader)
    {
        reader.IsFirstRowAsColumnNames = false;
        DataSet spreadsheetDocument = reader.AsDataSet();
        var table = spreadsheetDocument.Tables[0];

        var col_index = 0;
        string lang_str = string.Empty;

        // add language
        foreach (DataColumn column in table.Columns)
        {
            if (col_index < 2)
            {
                col_index++;
                continue;
            }

            lang_str = table.Rows[0][column].ToString();

            if (string.IsNullOrEmpty(lang_str))
                break;

            var lang = SystemLanguage.Unknown;
            try
            {
                lang = (SystemLanguage)Enum.Parse(typeof(SystemLanguage), lang_str);
            }
            catch
            {
                continue;
            }
            
            local.Data.Add(new LocalizationLanguageData { Lang = lang });
        }

        if (!local.Data.Any())
            return;

        // Add Keys
        for (var i = 1; i < table.Rows.Count; i++)
        {
            // add key
            var ckey = table.Rows[i][1].ToString();
            if (string.IsNullOrEmpty(ckey))
                break;

            local.Keys.Add(ckey);
        }

        // Add data
        var column_index = 2;
        foreach (var cLang in local.Data)
        {
            for (var i = 1; i < table.Rows.Count; i++)
            {
                var cval = table.Rows[i][column_index].ToString();
                cLang.Value.Add(cval);
            }
            column_index++;
        }

        //
        AssetDatabase.Refresh();
        EditorUtility.SetDirty(local);
        AssetDatabase.SaveAssets();
    }
#endif
}

