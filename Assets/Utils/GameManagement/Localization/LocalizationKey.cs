﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class LocalizationLanguageData
{
    [SerializeField] public SystemLanguage Lang;
    [SerializeField] public List<string> Value = new List<string>();
}