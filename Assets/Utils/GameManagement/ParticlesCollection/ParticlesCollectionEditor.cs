﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(ParticlesCollection))]
public class ParticlesCollectionEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var pc = target as ParticlesCollection;
        pc.OnInspectorGUI();
    }

    public void SaveChanges()
    {
        if (target != null)
        {
            AssetDatabase.Refresh();
            EditorUtility.SetDirty(target);
            AssetDatabase.SaveAssets();
        }
    }

    private void OnDisable()
    {
        SaveChanges();
    }
}

#endif