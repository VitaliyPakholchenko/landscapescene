﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
public class ParticlesCollection : ScriptableObject
{
    [SerializeField]
    private List<GameObject> _effects;

    public void Emmit(string particleName, Vector3 position)
    {
        var effect = _effects.FirstOrDefault(ef => ef.name == particleName);
        if (effect==null)
        {
            Debug.Log("Particle effect " + particleName + " is absent.");
            return;
        }
        GameObject.Instantiate(effect, position, Quaternion.identity);
    }

#if UNITY_EDITOR
    public void OnInspectorGUI()
    {
        if (_effects == null)
            _effects = new List<GameObject>();
        EditorExt.EditorListLayout("Particle", ref _effects);
    }
#endif
}
