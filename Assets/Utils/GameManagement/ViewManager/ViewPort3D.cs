﻿using System;
using UnityEngine;
using UnityEngine.UI;


#if UNITY_EDITOR
using UnityEditor;
#endif

public class ViewPort3D : RawImage
{
    /// <summary>
    /// 
    /// </summary>
    private Camera _camera { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public GameObject Model { get; set; }


    /// <summary>
    /// 
    /// </summary>
    void Update()
    {
        if (_camera == null)
            _camera = GetComponent<Camera>();
        _camera.Render();
        Graphics.Blit(texture, _camera.targetTexture);
    }

    public void AddViewObject(GameObject object_)
    {
        if (object_ == null)
            return;

        if (_camera == null)
            _camera = GetComponent<Camera>();

        Model = Instantiate(object_, _camera.transform.position, Quaternion.identity) as GameObject;
        Model.layer = LayerMask.NameToLayer("ViewPort");
        Model.transform.SetParent(gameObject.transform);
        
        var objRenderer = object_.GetComponent<MeshRenderer>();

        while (true)
        {
            var min = _camera.WorldToScreenPoint(Model.transform.TransformPoint(objRenderer.bounds.center + objRenderer.bounds.min));
            var max = _camera.WorldToScreenPoint(Model.transform.TransformPoint(objRenderer.bounds.center + objRenderer.bounds.max));

            if (min.x < 0 || max.x < 0 || min.y < 0 || max.y < 0 || min.z <= 0 || max.z <= 0)
            {
                var pos = Model.transform.position;
                pos.z += 0.1f;
                Model.transform.position = pos;
                continue;
            }

            break;
        }
    }

    public void Destroy()
    {
        Destroy(_camera.gameObject);
        Destroy(Model);
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(ViewPort3D))]
    public class ViewPort3DEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var view = (ViewPort3D)target;
            if (GUILayout.Button("CreateTarget"))
            {
                view._camera.targetTexture= new RenderTexture(128, 128, 16, RenderTextureFormat.ARGB32);
                view._camera.targetTexture.Create();
            }
        }
    }

    [MenuItem("GameObject/UI/ViewPort3D")]
    public static void Create()
    {
        var vpObj = new GameObject("ViewPort3D");

        var vp = vpObj.AddComponent<ViewPort3D>();
        var vpcamera = vpObj.AddComponent<Camera>();

        vpcamera.targetTexture = new RenderTexture(128, 128, 16, RenderTextureFormat.ARGB32);
        vpcamera.targetTexture.Create();
        vpcamera.clearFlags = CameraClearFlags.SolidColor;
        vpcamera.cullingMask =  LayerMask.NameToLayer("ViewPort");

        if (Selection.activeObject != null)
        {
            var pt = Selection.activeObject as GameObject;
            if (pt!=null)
                vpObj.transform.SetParent(pt.transform,false);
        }
    }
#endif

}

