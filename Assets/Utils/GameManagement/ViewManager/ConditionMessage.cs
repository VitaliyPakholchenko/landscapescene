﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ConditionMessage : Model<ConditionMessage>
{
    /// <summary>
    /// 
    /// </summary>
    public event Func<bool> ReceiveCondition;

    /// <summary>
    /// 
    /// </summary>
    public override bool OutsideTap
    {
        get { return false; }
    }

    /// <summary>
    /// 
    /// </summary>
    void Update()
    {
        if(ReceiveCondition != null && ReceiveCondition())
            (this as IModel).Close();
    }
}
