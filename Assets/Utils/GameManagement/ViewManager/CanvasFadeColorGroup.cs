﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using DG.Tweening.Core;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class CanvasFadeColorGroup : MonoBehaviour
{
    [SerializeField]
    [HideInInspector]
    private Color _color;
    public Color Color
    {
        get { return _color; }
        set
        {
            _color = value;
            SetColor(_color);
        }
    }

    [SerializeField]
    [HideInInspector]
    private Color _fadeColor = Color.gray;
    public Color FadeColor
    {
        get { return _fadeColor; }
        set { _fadeColor = value; }
    }

    private bool _fadeIn;
    private bool _fadeOut;
    private Tweener _fadeTweener;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="color"></param>
    public void SetColor(Color color)
    {
        var img = gameObject.GetComponent<Image>();
        if (img != null)
            img.material.SetColor("_FadeColor", color);
        else
        {
            var text = gameObject.GetComponent<Text>();
            if (text != null)
                text.material.SetColor("_FadeColor", color);
        }

        for (var i = 0; i < gameObject.transform.childCount; i++)
            SetColorRecursive(gameObject.transform.GetChild(i).gameObject, color);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="color"></param>
    private void SetColorRecursive(GameObject obj, Color color)
    {
        var img = obj.GetComponent<Image>();
        if (img != null)
            img.material.SetColor("_FadeColor", color);
        else
        {
            var text = obj.GetComponent<Text>();
            if (text != null)
                text.material.SetColor("_FadeColor", color);
        }

        for (var i = 0; i < obj.transform.childCount; i++)
            SetColorRecursive(obj.transform.GetChild(i).gameObject, color);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fadeTime"></param>
    public void FadeIn(float fadeTime)
    {
        if (_fadeIn)
            return;

        _fadeIn = true;

        if (_fadeOut)
            _fadeTweener.Kill();

        _fadeTweener = DOTween.To(() => Color, x => Color = x, FadeColor, fadeTime).OnKill(()=> _fadeIn = false);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fadeTime"></param>
    public void FadeOut(float fadeTime)
    {
        if (_fadeOut)
            return;

        _fadeOut = true;

        if (_fadeIn)
            _fadeTweener.Kill();

        _fadeTweener = DOTween.To(() => Color, x => Color = x, Color.white, fadeTime).OnKill(()=> _fadeOut = false);
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(CanvasFadeColorGroup), true)]
    public class CanvasFadeColorGroupEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var group = target as CanvasFadeColorGroup;
            group.Color = EditorGUILayout.ColorField("Color", group.Color);
            group.FadeColor = EditorGUILayout.ColorField("Fade Color", group.FadeColor);
        }
    }
#endif
}
