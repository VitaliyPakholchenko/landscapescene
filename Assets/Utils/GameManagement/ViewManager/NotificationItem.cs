﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class NotificationItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public string HintKey { get; set; }

    public Image Image;
    public Text Text;

    public event Func<bool> ReceiveCondition;
    public event Action OnClick;
    public event Action OnReceived;

    public Tweener Pulse;

    private NotificationPanel _panel;
    private float _hintTime;
    private bool _mouseOver;
    private float _showTime;
    private bool _timeMode;
    private bool _received;

    public void Receive()
    {
        if (OnReceived != null)
            OnReceived();
        _panel.RemoveNotify(this);
    }

    public void OnDestroy()
    {
        if (Pulse != null)
            Pulse.Kill();

        OnReceived = null;
        OnClick = null;
        ReceiveCondition = null;
        OnReceived = null;
        Pulse = null;
    }

    public void Update()
    {
        if (_received)
            return;

        if (ReceiveCondition != null && ReceiveCondition())
        {
            _received = true;
            Receive();
            return;
        }

        if (_mouseOver)
        {
            _hintTime += Time.deltaTime;
        }

        if (_timeMode)
        {
            _showTime -= Time.deltaTime;
            if (_showTime <= 0)
            {
                _received = true;
                Receive();
            }
        }
    }

    public static NotificationItem Create(NotificationPanel panel, Sprite sprite, string hintKeys, float showTime = -1)
    {
        var nGameObject = new GameObject();
        var item = nGameObject.AddComponent<NotificationItem>();

        item.Image = nGameObject.AddComponent<Image>();
        item.Image.sprite = sprite;
        item.Image.SetNativeSize();

        item.HintKey = hintKeys;
        item._panel = panel;

        if (showTime > 0)
        {
            item._showTime = showTime;
            item._timeMode = true;
        }

        return item;
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
        Image.color = Color.red;
        _mouseOver = true;
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
        Image.color = Color.white;
        _mouseOver = false;
        _hintTime = 0.0f;
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
    {
        if (OnClick != null)
            OnClick();
    }
}

