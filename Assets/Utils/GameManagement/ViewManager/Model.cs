﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


#if UNITY_EDITOR
using UnityEditor;
#endif

public enum DialogResultType
{
    Ok,
    Cancel,
    Yes,
    No,
    Unknown,

}

public interface IModel
{
    GameObject gameObject { get; }
    ViewManager ViewManager { get; set; }

    bool Unique { get; }
    bool Blocker { get; }
    bool OutsideTap { get; }
    bool OverrideSorting { get; set; }
    DialogResultType DialogResult { get; set; }

    int Layer { get; set; }

    event Func<bool> CloseCondition;
    event UnityAction<IModel> OnClosed;
    event UnityAction OnShowed;

    void FadeIn(float fadeTime);
    void FadeOut(float fadeTime);
    
    void Show();
    void Close();
    void DoOutsideTap();
}

[RequireComponent(typeof(Canvas))]
[RequireComponent(typeof(CanvasGroup))]
[RequireComponent(typeof(CanvasRenderer))]
[RequireComponent(typeof(GraphicRaycaster))]
[Serializable]
public abstract class Model<T> : CanvasFadeColorGroup, IModel where T: Model<T>
{
    /// <summary>
    /// 
    /// </summary>
    public int Layer
    {
        get { return GetComponent<Canvas>().sortingOrder; }
        set { GetComponent<Canvas>().sortingOrder = value; }
    }

    /// <summary>
    /// 
    /// </summary>
    public bool OverrideSorting
    {
        get { return GetComponent<Canvas>().overrideSorting; }
        set { GetComponent<Canvas>().overrideSorting = value; }
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual bool Unique {
        get { return true; }
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual bool Blocker {
        get { return false; }
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual bool OutsideTap
    {
        get { return true; }
    }

    /// <summary>
    /// 
    /// </summary>
    public DialogResultType DialogResult { get; set; }

    /// <summary>
    /// Parent view
    /// </summary>
    public IModel Parent { get; set; }

    /// <summary>
    /// 
    /// </summary>
    ViewManager IModel.ViewManager { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [NonSerialized]
    public bool IsTransparent;

    /// <summary>
    /// 
    /// </summary>
    public UnityEvent OnOutsideTap;

    /// <summary>
    /// 
    /// </summary>
    public event UnityAction<IModel> OnClosed;

    /// <summary>
    /// 
    /// </summary>
    public event UnityAction OnShowed;

    /// <summary>
    /// 
    /// </summary>
    public event Func<bool> CloseCondition;

    /// <summary>
    /// 
    /// </summary>
    protected virtual void Show()
    {
        gameObject.SetActive(true, true);

        DialogResult = DialogResultType.Unknown;

        if (OutsideTap)
            OnOutsideTap.AddListener(() => (this as IModel).Close());

        SetTransparency();
        FromTransparency().OnComplete(Showed);
    }

    /// <summary>
    /// 
    /// </summary>
    void IModel.Show()
    {
        Show();
    }

    /// <summary>
    /// 
    /// </summary>
    protected virtual void Showed()
    {
        if (OnShowed != null)
            OnShowed();
    }

    /// <summary>
    /// 
    /// </summary>
    void IModel.DoOutsideTap()
    {
        if (OnOutsideTap != null)
        {
            OnOutsideTap.Invoke();
            OnOutsideTap = null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    protected void Close()
    {
        ToTransparency().OnKill(Closed);
    }

    /// <summary>
    /// Close view
    /// </summary>
    /// <returns></returns>
    void IModel.Close()
    {
        Close();
    }

    /// <summary>
    /// 
    /// </summary>
    protected virtual void Closed()
    {
        gameObject.SetActive(false, true);

        (this as IModel).ViewManager.RemoveView(this);

        // Need coll after view removed
        if (OnClosed != null)
            OnClosed(this);
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual void Update()
    {
        if (CloseCondition!=null && CloseCondition())
            (this as IModel).Close();
    }

    /// <summary>
    /// 
    /// </summary>
    private void SetTransparency()
    {
        if (IsTransparent)
            return;

        IsTransparent = true;
        GetComponent<CanvasGroup>().alpha = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public Tweener FromTransparency()
    {
        if (!IsTransparent)
            return null;

        IsTransparent = false;
        return GetComponent<CanvasGroup>().DOFade(1, (this as IModel).ViewManager.ViewsFadeTime);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public Tweener ToTransparency()
    {
        if (IsTransparent)
            return null;

        IsTransparent = true;
        return GetComponent<CanvasGroup>().DOFade(0f, (this as IModel).ViewManager.ViewsFadeTime);
    }

    /// <summary>
    /// 
    /// </summary>
    protected virtual void OnDestroy()
    {
        CloseCondition = null;
        OnClosed = null;
        OnOutsideTap = null;
    }
}

// ------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------
public static class Extensions
{
    public static void SetActive(this GameObject object_, bool value, bool recursive)
    {
        object_.SetActive(value);
        foreach (var child in object_.transform)
        {
            var childTransform = child as Transform;
            if (childTransform==null)
                continue;
            childTransform.gameObject.SetActive(value,recursive);
        }
    }
}