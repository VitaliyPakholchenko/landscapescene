﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;


#if UNITY_EDITOR
using UnityEditor;
#endif

public class ViewManager : MonoBehaviour
{
    /// <summary>
    /// 
    /// </summary>
    [NonSerialized]
    private Camera _camera;
    public Camera Camera
    {
        get { return _camera ?? (_camera = gameObject.GetComponentInChildren<Camera>()); }
    }

    /// <summary>
    /// Upper view
    /// </summary>
    public IModel Upper
    {
        get
        {
            if (_modelsList.Count == 0)
                return null;

            IModel upper = null;

            _modelsList.ForEach(v=>
            {
                if (upper == null || v.Layer > upper.Layer)
                    upper = v;
            });

            return upper;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    private readonly List<IModel> _modelsList = new List<IModel>();
    public int Count
    {
        get { return _modelsList.Count; }
    }

    /// <summary>
    /// 
    /// </summary>
    public NotificationPanel NotifyPanel;

    /// <summary>
    /// 
    /// </summary>
    public RectTransform FadePanel;

    /// <summary>
    /// 
    /// </summary>
    public bool FadeScreen;

    /// <summary>
    /// 
    /// </summary>
    public float ScreenFadeTime = 2.5f;

    /// <summary>
    /// 
    /// </summary>
    public float ScreenFadeMax = 0f;

    /// <summary>
    /// 
    /// </summary>
    public float ViewsFadeTime = 0.05f;

    /// <summary>
    /// 
    /// </summary>
    public float ViewsFadeMax = 0.5f;

    /// <summary>
    /// 
    /// </summary>
    public Color FadeColor = Color.black;

    /// <summary>
    /// 
    /// </summary>
    public bool OutsideTap;

    /// <summary>
    /// 
    /// </summary>
    public event Action OnAllClosed;

    /// <summary>
    /// 
    /// </summary>
    private Tweener _fadeTweener;

    /// <summary>
    /// 
    /// </summary>
    private bool _fadeIn;

    /// <summary>
    /// 
    /// </summary>
    private bool _fadeOut;

    //Camera.main.orthographicSize = 3f*(float) Screen.height/(float) Screen.width;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T Show<T>() where T : MonoBehaviour, IModel
    {
        if (Upper != null && Upper.Blocker)
            return null;

        if (_modelsList.Any(v => v.GetType() == typeof (T) && v.Unique))
            return null;

        var view = gameObject.GetComponentInChildren<T>(true);
        return Show(view); 
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="view"></param>
    /// <param name="param"></param>
    /// <returns></returns>
    private T Show<T>(T model) where T : MonoBehaviour, IModel
    {
        if (model == null)
        {
            Debug.Log("View not found: " + typeof(T).Name);
            return null;
        }

        if (!model.Unique)
        {
            var vo = (GameObject)GameObject.Instantiate(model.gameObject, model.transform, true);
            vo.transform.SetParent(transform);
            model = vo.GetComponent<T>();
        }

        if (Upper != null)
        {
            model.OverrideSorting = true;
            model.Layer = Upper.Layer + 1;
            Upper.FadeIn(ViewsFadeTime);
        }
        else
        {
            if (FadePanel != null && FadeScreen)
            {
                var canvas = FadePanel.GetComponent<Canvas>();
                canvas.sortingOrder = 1;
                FadePanel.gameObject.SetActive(true);
            }

            model.OverrideSorting = true;
            model.Layer = 2;
        }

        _modelsList.Add(model);

        model.ViewManager = this;
        model.Show();

        return model;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="view"></param>
    public void RemoveView(IModel model)
    {
        _modelsList.Remove(model);

        var upper = Upper;

        if (upper != null)
            upper.FadeOut(ViewsFadeTime);
        else
        {
            if (FadePanel != null && FadeScreen)
                FadePanel.gameObject.SetActive(false);

            if (OnAllClosed != null)
                OnAllClosed();
        }

        if (!model.Unique)
            Destroy(model.gameObject);
    }

    private void Update()
    {
        if (!OutsideTap || _modelsList.Count == 0)
            return;

        var upper = Upper;
        if (upper != null && upper.OutsideTap)
        {
            var pos = Vector2.zero;
            var input = false;

            if (Input.touchCount > 0)
            {
                pos = Input.touches[0].position;
                input = true;
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    pos = Input.mousePosition;
                    input = true;
                }
            }

            if (!input)
                return;

            var pointerData = new PointerEventData(EventSystem.current)
            {
                pointerId = -1,
                position = pos
            };

            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerData, results);

            if (results.Count > 0)
            {
                if (results[0].gameObject.layer == LayerMask.NameToLayer("UI"))
                    return;
                upper.DoOutsideTap();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public Tweener FadeIn(float maxFade = -1)
    {
        if (_fadeIn || !FadeScreen)
            return null;

        FadePanel.gameObject.SetActive(true);

        if (_fadeOut)
        {
            _fadeOut = false;
            _fadeTweener.Kill();
        }

        if (maxFade < 0)
            maxFade = ScreenFadeMax;

        FadePanel.GetComponent<Image>().color = FadeColor;

        _fadeTweener = FadePanel.GetComponent<CanvasGroup>()
            .DOFade(maxFade, ScreenFadeTime)
            .Play().OnKill(() =>
            {
                _fadeIn = false;
                _fadeTweener = null;
            });

        return _fadeTweener;
    }

    /// <summary>
    /// 
    /// </summary>
    public Tweener FadeOut()
    {
        if (_fadeOut || !FadeScreen)
            return null;

        FadePanel.gameObject.SetActive(true);

        if (_fadeIn)
        {
            _fadeIn = false;
            _fadeTweener.Kill();
        }

        _fadeTweener = FadePanel.GetComponent<CanvasGroup>()
            .DOFade(0, ScreenFadeTime)
            .Play().OnKill(() =>
            {
                _fadeOut = false;
                _fadeTweener = null;
                FadePanel.gameObject.SetActive(false);
            });

        return _fadeTweener;
    }

    /// <summary>
    /// 
    /// </summary>
    public void FadeInNow()
    {
        if (_fadeIn || !FadeScreen)
            _fadeTweener.Kill(true);

        FadePanel.gameObject.SetActive(true);

        if (_fadeOut)
        {
            _fadeOut = false;
            _fadeTweener.Kill();
        }

        FadePanel.GetComponent<CanvasGroup>().alpha = 1;
    }

    /// <summary>
    /// 
    /// </summary>
    public void FadeOutNow()
    {
        if (!FadeScreen)
            return;

        if (_fadeOut)
            _fadeTweener.Kill(true);

        FadePanel.gameObject.SetActive(true);

        if (_fadeIn)
        {
            _fadeIn = false;
            _fadeTweener.Kill();
        }

        FadePanel.GetComponent<CanvasGroup>().alpha = 0;
    }

#if UNITY_EDITOR
    [MenuItem("GameManagement/View Manager/Create")]
    static void Create()
    {
        if (LayerMask.GetMask("UI")<0)
            throw new UnassignedReferenceException("Layer UI must be assigned in Layer Manager.");

        var vmo = new GameObject("ViewManager");
        vmo.AddComponent<RectTransform>();
        vmo.AddComponent<Canvas>();
        vmo.AddComponent<CanvasScaler>();
        vmo.AddComponent<GraphicRaycaster>();
        vmo.AddComponent<CanvasRenderer>();
        vmo.AddComponent<CanvasGroup>();
        var vm = vmo.AddComponent<ViewManager>();
        vmo.layer = LayerMask.NameToLayer("UI");

        var cameraObject = new GameObject("ViewCamera");
        cameraObject.transform.SetParent(vmo.transform);
        cameraObject.layer = LayerMask.NameToLayer("UI");
        cameraObject.AddComponent<ViewCamera>();

        var camera = cameraObject.AddComponent<Camera>();
        camera.orthographic = true;
        camera.orthographicSize = 5;
        camera.cullingMask = 1 << 5;
        camera.depth = 1;
        camera.clearFlags = CameraClearFlags.Depth;

        var fadePanel = new GameObject("FadePanel");
        fadePanel.transform.SetParent(vmo.transform);
        var fpImage = fadePanel.AddComponent<Image>();
        fpImage.color = Color.black;
        var fmCanvas = fadePanel.AddComponent<Canvas>();
        fmCanvas.overrideSorting = true;
        fmCanvas.sortingOrder = 1;
        fadePanel.SetActive(false);
        vm.FadePanel = fadePanel.GetComponent<RectTransform>();

        var esObject = new GameObject("Event System");
        esObject.AddComponent<EventSystem>();
        esObject.layer = LayerMask.NameToLayer("UI");
        esObject.transform.SetParent(vmo.transform);
    }

    [MenuItem("GameManagement/View Manager/Setup Shader")]
    static void SetupShaders()
    {
        var vm = GameObject.Find("ViewManager");
        if (vm == null)
            return;
        var vmw = vm.GetComponent<ViewManager>();

        for (var i = 0; i < vm.transform.childCount; i++)
        {
            var child = vm.transform.GetChild(i).gameObject;
            if (child == vmw.FadePanel.gameObject)
                continue;
            SetupShadersRecursive(child);
        }
    }

    static void SetupShadersRecursive(GameObject obj)
    {
        var img = obj.GetComponent<Image>();
        if (img != null)
            img.material = new Material(Shader.Find("UI/Faded"));
        else
        {
            var text = obj.GetComponent<Text>();
            if (text != null)
                text.material = new Material(Shader.Find("UI/Faded"));
        }

        for (var i = 0; i < obj.transform.childCount; i++)
        {
            var child = obj.transform.GetChild(i).gameObject;
            SetupShadersRecursive(child);
        }
    }

#endif
}