﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;


public class NotificationPanel : MonoBehaviour
{
    public Sprite NotifySprite;
    public RectTransform NotifyZone;

    private readonly Dictionary<string, NotificationItem> _items = new Dictionary<string, NotificationItem>();

    public NotificationItem Notify(string hintKeys)
    {
        if (_items.ContainsKey(hintKeys))
            return null;

        var item = NotificationItem.Create(this, NotifySprite, hintKeys);
        item.gameObject.transform.SetParent(NotifyZone.transform);

        var imageWidth = item.Image.sprite.rect.width;
        item.gameObject.transform.localPosition = new Vector3(NotifyZone.rect.width*0.5f, imageWidth*0.5f,0);
        item.gameObject.transform.localScale = Vector3.one;

        var finitshX = (-(NotifyZone.rect.width*0.5f)) + (_items.Count*(imageWidth + 4));

        item.transform.DOLocalMoveX(finitshX,1.5f, false).SetEase(Ease.OutBack,1.1f).Play().OnComplete(() =>
        {
            item.Pulse = item.Image.transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 1f)
           .SetEase(Ease.InBack).SetLoops(-1, LoopType.Yoyo).Play();
        });

        _items.Add(hintKeys, item);

        return item;
    }

    public void RemoveNotify(NotificationItem notify)
    {
        var key =_items.FirstOrDefault(p => p.Value == notify).Key;
        _items.Remove(key);
        Destroy(notify.gameObject);
    }
}

