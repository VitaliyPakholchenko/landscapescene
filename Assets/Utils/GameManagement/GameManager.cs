﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if _INTEGRATION
using System.Net;
#endif

public interface IGameManager
{
    string UserId { get; }
    bool InternetAvailable { get; }

#if _INTEGRATION && _ANALYTICS
    AnalyticsController Analytics { get; }
#endif
}

public abstract class GameManager<T> : MonoBehaviour, IGameManager where T : GameManager<T>
{
    /// <summary>
    /// 
    /// </summary>
    public static T Instance { get; private set; }

    /// <summary>
    /// 
    /// </summary>
    public SoundManager SoundManager;
    
    /// <summary>
    /// 
    /// </summary>
    public ViewManager ViewManager;
    
    /// <summary>
    /// 
    /// </summary>
    public ParticlesCollection Particles;
    
    /// <summary>
    /// 
    /// </summary>
    //User info
    public string UserId { get; private set; }

    /// <summary>
    /// 
    /// </summary>
    public bool InternetAvailable { get; private set; }

    public bool Initialized { get; private set; }
#if _INTEGRATION

    /// <summary>
    /// Social
    /// </summary>
#if _FACEBOOK
    public readonly InterationFB Facebook = new InterationFB();
#endif

    /// <summary>
    /// Remote Base Data
    /// </summary>
#if _DB
    public DataBaseAccess DataBase { get; private set; }
#endif

    /// <summary>
    /// Unity In-App Purchase
    /// </summary>
#if _STORE
    public Store Store { get; private set; }
#endif

    /// <summary>
    /// 
    /// </summary>
#if _ANALYTICS
    public AnalyticsController Analytics { get; private set; }
#endif

#endif

    /// <summary>
    /// 
    /// </summary>
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = GameObject.FindObjectOfType<T>();
            DontDestroyOnLoad(Instance.gameObject);

            var firstStart = PlayerPrefs.GetInt("FirstStart") < 1;
            if (firstStart)
            {
                FirestStartInitialize();
                PlayerPrefs.SetInt("FirstStart", 1);
            }
            else
            {
                Initialize();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    protected virtual void FirestStartInitialize()
    {
        Initialize();
    }

    /// <summary>
    /// 
    /// </summary>
    protected virtual void Initialize()
    {
        UserId = "stm.post@gmail.com";//PlayerPrefs.GetString("UserId");
        if (string.IsNullOrEmpty(UserId))
        {
            UserId = Utils.GetId();
            PlayerPrefs.SetString("UserId", UserId);
        }

#if _INTEGRATION

#if _FACEBOOK
        Facebook.Intialize();
#endif

#if _ANALYTICS
        Analytics = new AnalyticsController();
        Analytics.Initialize(this);
#endif

#if _STORE
        Store = new Store();
        Store.Initialize(this);
#endif

#if _DB
        DataBase = new DataBaseAs
#endif

#if _NETWORKING
#endif

#endif

        Initialized = true;
    }

    /// <summary>
    /// 
    /// </summary>
	public virtual void Update ()
    {		
	}

#if _INTEGRATION
    /// <summary>
    /// 
    /// </summary>
    private void CheckInternet()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        StartCoroutine(CheckInternetAndroid());
#endif

#if UNITY_IOS || UNITY_EDITOR
        CheckInternetIos();
#endif
    }

#if UNITY_ANDROID && !UNITY_EDITOR
    IEnumerator CheckInternetAndroid()
    {
        const float timeout = 10f;
        float startTime = Time.timeSinceLevelLoad;
        Ping ping = new Ping("192.168.1.121");

        while (true)
        {
            if (ping.isDone)
            {
                InternetAvailable = true;
                yield break;
            }
            if (Time.timeSinceLevelLoad - startTime > timeout)
            {
                InternetAvailable = false;
                yield break;
            }

            yield return new WaitForEndOfFrame();
        }
    }
#endif

#if UNITY_IOS || UNITY_EDITOR
    private void CheckInternetIos()
    {
        try
        {
            var client = new WebClient();
            client.OpenReadCompleted += (sender, args) =>
            {
                InternetAvailable = (args.Error == null);
            };
            client.OpenReadAsync(new Uri("http://google.com"));
        }
        catch (Exception ex)
        {
            InternetAvailable = false;
        }
    }
#endif

#endif

    /// <summary>
    /// 
    /// </summary>
    public virtual void Save()
    {
        if (SoundManager != null)
            SoundManager.Save();

        PlayerPrefs.Save();
    }

    /// <summary>
    /// 
    /// </summary>
    protected virtual void Load()
    {
        if (SoundManager != null)
            SoundManager.Load();
    }

    /// <summary>
    /// 
    /// </summary>
    private void OnApplicationQuit()
    {
        Save();
    }
}
