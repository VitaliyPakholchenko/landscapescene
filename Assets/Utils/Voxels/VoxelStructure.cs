﻿using UnityEngine;
using System;
using System.Linq;
using System.Globalization;
using UnityEngine.Rendering;

#if UNITY_EDITOR
using UnityEditor;
#endif

[RequireComponent (typeof (MeshFilter))]
[RequireComponent (typeof (MeshRenderer))]
[RequireComponent (typeof (MeshCollider))]
[Serializable]
public class VoxelStructure : MonoBehaviour
{
	[SerializeField]
	public VoxelPallette pallette;
    public VoxelSpace VoxelSpace { get; private set; }

    //The action enum, used to determine which action to take when interacting
    public enum Action{
		Add=0,
		Erase,
		Paint,
		MaxPull,
		Flood,
        Copy,
		None
	}
	
	public enum VoxelFace{
		Front=0,
		Back,
		Left,
		Right,
		Top,
		Bottom
	}
	
	//The current voxel used in all commands
	//public VoxelType CurrentVoxelType = VoxelType.Stone;
	//The current action, used to determine how to hand user interactions
	public static Action LMB_Action = Action.Add;
	public static Action RMB_Action = Action.Erase;
	public static Action Static_Action = Action.Erase;
	
	//flatVoxels, the data structure behind it all. Using a flat array to represent a 3d array, in the hopes of some
	//slight optimizations. Further testing should be done to determine if that is really the case.
	[SerializeField]
	[HideInInspector]
	public int[,,] flatVoxels;
	
	//The Effective size of the voxel structure. Should never exceed the static WIDTH, HEIGHT, or DEPTH values
	//It's possible to increase these to the point that the generated mesh would be too large, resulting in an error
	//In the future, the code will just create a new gameobject if this upper limit is reached.	
	public int Width 	= 8;
	public int Height 	= 8;
	public int Depth 	= 8;
	
	//Used to store mesh data during construction
    private Vector3[] _verts;
    private Vector2[] _uvs;
    private Color32[] _colors;
    private MeshFilter _mf;
    private int[] _tris;

    private VoxelTemplate fType, kType, rType, lType, tType, bType, voxelType;
	private Color32 voxelColor;

    public bool IsDirty;

    public static VoxelStructure Create(GameObject parent, int index, float x, float y, float z, int chunkSizeW,
        int chunkSizeH, int chunkSizeD, Vector3 offset, VoxelPallette palette, VoxelSpace space)
    {
        var vStructure = new GameObject("vs" + index.ToString(CultureInfo.InvariantCulture));
        vStructure.transform.parent = parent.transform;
        vStructure.transform.position = new Vector3(x * chunkSizeW, y * chunkSizeH, z * chunkSizeD) + offset;

        var structure = vStructure.AddComponent<VoxelStructure>();
        structure.pallette = palette;

        structure.Width = chunkSizeW;
        structure.Height = chunkSizeH;
        structure.Depth = chunkSizeD;

        structure.VoxelSpace = space;

        structure.flatVoxels = new int[chunkSizeW, chunkSizeH, chunkSizeD];

        var renderer = vStructure.GetComponent<MeshRenderer>();
        renderer.shadowCastingMode = ShadowCastingMode.On;
        renderer.receiveShadows = true;
        renderer.lightProbeUsage = LightProbeUsage.Off;
        renderer.reflectionProbeUsage = ReflectionProbeUsage.BlendProbes;

        var collider = vStructure.GetComponent<MeshCollider>();
        collider.convex = false;
        collider.isTrigger = false;

        return structure;
    }

    //The magic:
	//Loop through each voxel, 
	//based on visibility/neighbors, 
	//determine which of the faces should be created,
	//if they should, get the verts, tris, and uvs for the face and
	//add it to the mesh.
    public void Draw()
    {
        if (pallette == null)
        {
            Debug.LogError("No Pallette Selected");
        }
        else
        {
            _mf = GetComponent<MeshFilter>();

            ClampDimensions();

            var currentStepCount = 1;
            var currentStepFour = currentStepCount * 4;
            var currentStepSix = currentStepCount * 6;

            if (_verts == null || _verts.Length != Width * Height * Depth * 24)
            {
                _verts = new Vector3[Width * Height * Depth * 24];
                _tris = new int [Width * Height * Depth * 36];
                _uvs = new Vector2[Width * Height * Depth * 24];
                _colors = new Color32[Width * Height * Depth * 24];
            }
            else
            {
                Array.Clear(_verts, 0, _verts.Length);
                Array.Clear(_tris, 0, _tris.Length);
                Array.Clear(_uvs, 0, _uvs.Length);
                Array.Clear(_colors, 0, _colors.Length);
            }

            //The Main Drawing Loop. Lots of room for optimization,
            //but it seems to run quickly enough.
            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    for (var z = 0; z < Depth; z++)
                    {
                        var c = GetVoxel(x, y, z);

                        voxelType = GetVoxelType(c);

                        if (voxelType != null && voxelType.shouldDraw)
                        {
                            voxelColor = voxelType.color;

                            if (!voxelType.drawFacesInCenter)
                            {
                                #region voxel sides

                                //Get all neighboring voxels to determine whether or not to draw the corresponding face
                                var f = GetVoxel(x, y, z - 1);
                                fType = GetVoxelType(f);
                                var k = GetVoxel(x, y, z + 1);
                                kType = GetVoxelType(k);
                                var r = GetVoxel(x + 1, y, z);
                                rType = GetVoxelType(r);
                                var l = GetVoxel(x - 1, y, z);
                                lType = GetVoxelType(l);
                                var t = GetVoxel(x, y + 1, z);
                                tType = GetVoxelType(t);
                                var b = GetVoxel(x, y - 1, z);
                                bType = GetVoxelType(b);

                                //front face
                                if (VoxelSpace.DrawFront && voxelType.drawFront &&
                                    (fType == null || fType.drawFacesInCenter || !fType.shouldDraw))
                                {
                                    _verts[currentStepFour + 0] = new Vector3(x, y, z);
                                    _verts[currentStepFour + 1] = new Vector3(x, y + 1, z);
                                    _verts[currentStepFour + 2] = new Vector3(x + 1, y + 1, z);
                                    _verts[currentStepFour + 3] = new Vector3(x + 1, y, z);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 'f');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                                //back face
                                if (VoxelSpace.DrawBack && voxelType.drawBack &&
                                    (kType == null || kType.drawFacesInCenter || !kType.shouldDraw))
                                {
                                    _verts[currentStepFour + 0] = new Vector3(x + 1, y, z + 1);
                                    _verts[currentStepFour + 1] = new Vector3(x + 1, y + 1, z + 1);
                                    _verts[currentStepFour + 2] = new Vector3(x, y + 1, z + 1);
                                    _verts[currentStepFour + 3] = new Vector3(x, y, z + 1);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 'k');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                                //top
                                if (VoxelSpace.DrawTop && voxelType.drawTop &&
                                    (tType == null || tType.drawFacesInCenter || !tType.shouldDraw))
                                {


                                    _verts[currentStepFour + 0] = new Vector3(x, y + 1, z);
                                    _verts[currentStepFour + 1] = new Vector3(x, y + 1, z + 1);
                                    _verts[currentStepFour + 2] = new Vector3(x + 1, y + 1, z + 1);
                                    _verts[currentStepFour + 3] = new Vector3(x + 1, y + 1, z);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 't');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                                //bottom
                                if (VoxelSpace.DrawBottom && voxelType.drawBottom &&
                                    (bType == null || bType.drawFacesInCenter || !bType.shouldDraw))
                                {

                                    _verts[currentStepFour + 3] = new Vector3(x, y, z);
                                    _verts[currentStepFour + 2] = new Vector3(x, y, z + 1);
                                    _verts[currentStepFour + 1] = new Vector3(x + 1, y, z + 1);
                                    _verts[currentStepFour + 0] = new Vector3(x + 1, y, z);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 'b');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                                //right
                                if (VoxelSpace.DrawRight && voxelType.drawRight &&
                                    (rType == null || rType.drawFacesInCenter || !rType.shouldDraw))
                                {

                                    _verts[currentStepFour + 0] = new Vector3(x + 1, y, z);
                                    _verts[currentStepFour + 1] = new Vector3(x + 1, y + 1, z);
                                    _verts[currentStepFour + 2] = new Vector3(x + 1, y + 1, z + 1);
                                    _verts[currentStepFour + 3] = new Vector3(x + 1, y, z + 1);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 'r');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                                //left
                                if (VoxelSpace.DrawLeft && voxelType.drawLeft &&
                                    (lType == null || lType.drawFacesInCenter || !lType.shouldDraw))
                                {

                                    _verts[currentStepFour + 3] = new Vector3(x, y, z);
                                    _verts[currentStepFour + 2] = new Vector3(x, y + 1, z);
                                    _verts[currentStepFour + 1] = new Vector3(x, y + 1, z + 1);
                                    _verts[currentStepFour + 0] = new Vector3(x, y, z + 1);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 'l');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }

                                #endregion
                            }

                            //for voxels marked as 'drawFacesInCenter'
                            else
                            {
                                if (VoxelSpace.DrawFront && voxelType.drawFront)
                                {
                                    _verts[currentStepFour + 0] = new Vector3(x, y, z + .5f);
                                    _verts[currentStepFour + 1] = new Vector3(x, y + 1, z + .5f);
                                    _verts[currentStepFour + 2] = new Vector3(x + 1, y + 1, z + .5f);
                                    _verts[currentStepFour + 3] = new Vector3(x + 1, y, z + .5f);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 'f');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                                //back face
                                if (VoxelSpace.DrawBack && voxelType.drawBack)
                                {
                                    _verts[currentStepFour + 3] = new Vector3(x, y, z + .5f);
                                    _verts[currentStepFour + 2] = new Vector3(x, y + 1, z + .5f);
                                    _verts[currentStepFour + 1] = new Vector3(x + 1, y + 1, z + .5f);
                                    _verts[currentStepFour + 0] = new Vector3(x + 1, y, z + .5f);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 'k');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                                //top
                                if (VoxelSpace.DrawTop && voxelType.drawTop)
                                {

                                    _verts[currentStepFour + 0] = new Vector3(x, y + .5f, z);
                                    _verts[currentStepFour + 1] = new Vector3(x, y + .5f, z + 1);
                                    _verts[currentStepFour + 2] = new Vector3(x + 1, y + .5f, z + 1);
                                    _verts[currentStepFour + 3] = new Vector3(x + 1, y + .5f, z);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 't');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                                //bottom
                                if (VoxelSpace.DrawBottom && voxelType.drawBottom)
                                {

                                    _verts[currentStepFour + 3] = new Vector3(x, y + .5f, z);
                                    _verts[currentStepFour + 2] = new Vector3(x, y + .5f, z + 1);
                                    _verts[currentStepFour + 1] = new Vector3(x + 1, y + .5f, z + 1);
                                    _verts[currentStepFour + 0] = new Vector3(x + 1, y + .5f, z);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 'b');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                                //right
                                if (VoxelSpace.DrawRight && voxelType.drawRight)
                                {

                                    _verts[currentStepFour + 0] = new Vector3(x + .5f, y, z);
                                    _verts[currentStepFour + 1] = new Vector3(x + .5f, y + 1, z);
                                    _verts[currentStepFour + 2] = new Vector3(x + .5f, y + 1, z + 1);
                                    _verts[currentStepFour + 3] = new Vector3(x + .5f, y, z + 1);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 'r');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                                //left
                                if (VoxelSpace.DrawLeft && voxelType.drawLeft)
                                {

                                    _verts[currentStepFour + 3] = new Vector3(x + .5f, y, z);
                                    _verts[currentStepFour + 2] = new Vector3(x + .5f, y + 1, z);
                                    _verts[currentStepFour + 1] = new Vector3(x + .5f, y + 1, z + 1);
                                    _verts[currentStepFour + 0] = new Vector3(x + .5f, y, z + 1);

                                    _tris[currentStepSix + 0] = currentStepFour + 0;
                                    _tris[currentStepSix + 1] = currentStepFour + 1;
                                    _tris[currentStepSix + 2] = currentStepFour + 2;

                                    _tris[currentStepSix + 3] = currentStepFour + 2;
                                    _tris[currentStepSix + 4] = currentStepFour + 3;
                                    _tris[currentStepSix + 5] = currentStepFour + 0;

                                    var myuvs = GetVoxelUvs(voxelType, 'l');
                                    _uvs[currentStepFour + 0] = myuvs[0];
                                    _uvs[currentStepFour + 1] = myuvs[1];
                                    _uvs[currentStepFour + 2] = myuvs[2];
                                    _uvs[currentStepFour + 3] = myuvs[3];

                                    _colors[currentStepFour + 0] = voxelColor;
                                    _colors[currentStepFour + 1] = voxelColor;
                                    _colors[currentStepFour + 2] = voxelColor;
                                    _colors[currentStepFour + 3] = voxelColor;

                                    currentStepCount++;
                                    currentStepFour = 4 * currentStepCount;
                                    currentStepSix = 6 * currentStepCount;
                                }
                            }
                        }
                    }
                }
            }

            //trim the arrays
            var subverts = _verts.Take(currentStepCount * 4).ToArray();
            if (subverts.Length < 64000)
            {

                if (VoxelSpace.UsePalletteTexture)
                {
                    if (pallette.AtlasMaterial != null)
                        GetComponent<Renderer>().material = pallette.AtlasMaterial;
                    else
                    {
                        Debug.LogWarning(
                            "There is no atlas material set on the Voxel Pallette. Will use current Renderer Material instead.");
                    }
                }

                var subtris = _tris.Take(currentStepCount * 6).ToArray();
                var subuvs = _uvs.Take(currentStepCount * 4).ToArray();
                var subcolors = _colors.Take(currentStepCount * 4).ToArray();

                var someMesh = _mf.sharedMesh;
                if (someMesh != null)
                {
                    someMesh.Clear();
                }
                else
                {
                    someMesh = new Mesh();
                }

                someMesh.vertices = subverts;
                someMesh.triangles = subtris;
                someMesh.uv = subuvs;
                someMesh.colors32 = subcolors;

                someMesh.RecalculateBounds();
                someMesh.RecalculateNormals();
                someMesh.RecalculateTangents();

                _mf.sharedMesh = someMesh;

#if UNITY_EDITOR
                if (VoxelSpace.GenerateSecondaryUvSet && subverts.Length > 4)
                {
                    Unwrapping.GenerateSecondaryUVSet(_mf.sharedMesh);
                }
#endif
            }
            else
            {
                Debug.LogError(
                    "You are attempting to create a structure that would require more than 64k vertices, which is the upper limit in Unity. Please adjust the size of your structure. In the future, this will no longer happen. I'm sorry!");
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public int GetVoxel(int x, int y, int z)
    {
        if (x >= Width || x < 0 || y >= Height || y < 0 || z >= Depth || z < 0)
            return -1;
        return flatVoxels[x, y, z];
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="newVoxel"></param>
	public void SetVoxel(int x, int y, int z, int newVoxel)
	{
	    if (x >= Width || x < 0 || y >= Height || y < 0 || z >= Depth || z < 0)
	        return;
	    IsDirty = true;
        flatVoxels[x,y,z] = newVoxel;
	}

    /// <summary>
    /// 
    /// </summary>
    /// <param name="v"></param>
    /// <param name="dir"></param>
    public void MaxExtrude(Vector3i v, Vector3 dir, int selectedVoxelType)
    {
        var nextVoxelPosition = v + dir;
        var nextVoxel = GetVoxel(v.x, v.y, v.z);
        while (nextVoxel == 0)
        {
            SetVoxel(v.x, v.y, v.z, selectedVoxelType);
            nextVoxelPosition += dir;
            nextVoxel = GetVoxel(v.x, v.y, v.z);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="y"></param>
    public void FloodBasic(int y, int selectedVoxelType)
    {
        for (int x = 0; x < Width; x++)
        {
            for (int z = 0; z < Depth; z++)
            {
                if (GetVoxel(x, y, z) == 0)
                    SetVoxel(x, y, z, selectedVoxelType);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public void ExportMesh()
    {
#if UNITY_EDITOR
        var path = EditorUtility.OpenFolderPanel("Select a folder to export your mesh", "Assets", "");
        Debug.Log(path);
        if (path.Length != 0)
        {
            var betterPath = path.Substring(path.IndexOf("Assets")) + "/";
            Debug.Log(betterPath);
            AssetDatabase.CreateAsset(gameObject.GetComponent<MeshFilter>().sharedMesh,
                betterPath + gameObject.name + ".asset");
        }
#endif
    }

    /// <summary>
    /// Used for a bit of offset in the uvmapping if you find neighboring pixels are 'bleeding' into eachother
    /// </summary>
    private float ONE_PIXEL = .0002f;

    /// <summary>
    /// Determine what UVs to use for a given voxel face.
    /// This may seem odd, but I'm doing this here rather than storing this data in the voxel class,
    /// since we want that class to be as small as possible. This function only runs during mesh 
    /// building, whereas storing all of this data would mean Unity uses a lot more memory... I think!
    /// Abstract this to an 'atlas position' like 0,14 instead of 0f/16f, 14f/16f
    /// </summary>
    /// <param name="voxelType"></param>
    /// <param name="dir"></param>
    /// <returns></returns>
    Vector2[] GetVoxelUvs(VoxelTemplate voxelType, char dir){
		
		if(pallette!=null)
        {
			if(voxelType!=null){
				if(dir=='f' || voxelType.useFrontUvsForAllFaces){
					_uvs[0].x=voxelType.UVOffsetFront.x;
					_uvs[0].y=voxelType.UVOffsetFront.y;
					
					_uvs[1].x=voxelType.UVOffsetFront.x;
					_uvs[1].y=voxelType.UVOffsetFront.y + voxelType.atlasScale.y;
					
					_uvs[2].x=voxelType.UVOffsetFront.x + voxelType.atlasScale.x;
					_uvs[2].y=voxelType.UVOffsetFront.y + voxelType.atlasScale.y;
					
					_uvs[3].x=voxelType.UVOffsetFront.x + voxelType.atlasScale.x;
					_uvs[3].y=voxelType.UVOffsetFront.y;
				}else if(dir=='k'){
					_uvs[0].x=voxelType.UVOffsetBack.x;
					_uvs[0].y=voxelType.UVOffsetBack.y;
					
					_uvs[1].x=voxelType.UVOffsetBack.x;
					_uvs[1].y=voxelType.UVOffsetBack.y + voxelType.atlasScale.y;
					
					_uvs[2].x=voxelType.UVOffsetBack.x + voxelType.atlasScale.x;
					_uvs[2].y=voxelType.UVOffsetBack.y + voxelType.atlasScale.y;
					
					_uvs[3].x=voxelType.UVOffsetBack.x + voxelType.atlasScale.x;
					_uvs[3].y=voxelType.UVOffsetBack.y;
				}else if(dir=='l'){
					_uvs[0].x=voxelType.UVOffsetLeft.x;
					_uvs[0].y=voxelType.UVOffsetLeft.y;
					
					_uvs[1].x=voxelType.UVOffsetLeft.x;
					_uvs[1].y=voxelType.UVOffsetLeft.y + voxelType.atlasScale.y;
					
					_uvs[2].x=voxelType.UVOffsetLeft.x + voxelType.atlasScale.x;
					_uvs[2].y=voxelType.UVOffsetLeft.y + voxelType.atlasScale.y;
					
					_uvs[3].x=voxelType.UVOffsetLeft.x + voxelType.atlasScale.x;
					_uvs[3].y=voxelType.UVOffsetLeft.y;
				}else if(dir=='r'){
					_uvs[0].x=voxelType.UVOffsetRight.x;
					_uvs[0].y=voxelType.UVOffsetRight.y;
					
					_uvs[1].x=voxelType.UVOffsetRight.x;
					_uvs[1].y=voxelType.UVOffsetRight.y + voxelType.atlasScale.y;
					
					_uvs[2].x=voxelType.UVOffsetRight.x + voxelType.atlasScale.x;
					_uvs[2].y=voxelType.UVOffsetRight.y + voxelType.atlasScale.y;
					
					_uvs[3].x=voxelType.UVOffsetRight.x + voxelType.atlasScale.x;
					_uvs[3].y=voxelType.UVOffsetRight.y;
				}else if(dir=='t'){
					_uvs[0].x=voxelType.UVOffsetTop.x;
					_uvs[0].y=voxelType.UVOffsetTop.y;
					
					_uvs[1].x=voxelType.UVOffsetTop.x;
					_uvs[1].y=voxelType.UVOffsetTop.y + voxelType.atlasScale.y;
					
					_uvs[2].x=voxelType.UVOffsetTop.x + voxelType.atlasScale.x;
					_uvs[2].y=voxelType.UVOffsetTop.y + voxelType.atlasScale.y;
					
					_uvs[3].x=voxelType.UVOffsetTop.x + voxelType.atlasScale.x;
					_uvs[3].y=voxelType.UVOffsetTop.y;
				}else if(dir=='b'){
					_uvs[0].x=voxelType.UVOffsetBottom.x;
					_uvs[0].y=voxelType.UVOffsetBottom.y;
					
					_uvs[1].x=voxelType.UVOffsetBottom.x;
					_uvs[1].y=voxelType.UVOffsetBottom.y + voxelType.atlasScale.y;
					
					_uvs[2].x=voxelType.UVOffsetBottom.x + voxelType.atlasScale.x;
					_uvs[2].y=voxelType.UVOffsetBottom.y + voxelType.atlasScale.y;
					
					_uvs[3].x=voxelType.UVOffsetBottom.x + voxelType.atlasScale.x;
					_uvs[3].y=voxelType.UVOffsetBottom.y;
				}
			}
		}

		//Uncomment this if you want to use the offset from above
        //bl
        _uvs[0].x += ONE_PIXEL;
        _uvs[0].y += ONE_PIXEL;

        //tl
        _uvs[1].x += ONE_PIXEL;
        _uvs[1].y -= ONE_PIXEL;

        //tr
        _uvs[2].x -= ONE_PIXEL;
        _uvs[2].y -= ONE_PIXEL;

        //br
        _uvs[3].x -= ONE_PIXEL;
        _uvs[3].y += ONE_PIXEL;

		return _uvs;
	}

    private VoxelTemplate GetVoxelType(int v)
    {
        if (v < pallette.voxelTemplates.Length && v > 0)
            return pallette.voxelTemplates[v];
        return null;
    }

    public void ClampDimensions()
    {
        Width = Mathf.Clamp(Width, 0, 32);
        Height = Mathf.Clamp(Height, 0, 32);
        Depth = Mathf.Clamp(Depth, 0, 32);
    }
}
