﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class Voxwell : Editor
{
	/*[MenuItem("Assets/Create/Voxel Pallette")]
	public static void CreateVoxelPalletteAsset ()
	{
		ScriptableObjectUtility.CreateAsset<VoxelPallette> ();
	}*/

	[MenuItem("Assets/Create/Voxel Template")]
	public static void CreateVoxelTemplateAsset ()
	{
		ScriptableObjectUtility.CreateAsset<VoxelTemplate> ();
	}
}
