﻿using System.Globalization;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;

#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using UnityEngine.Rendering;

public class VoxelSpace : MonoBehaviour
{
    public int Width { get; private set; }
    public int Height { get; private set; }
    public int Depth { get; private set; }

    private int _chunkSizeW;
    private int _chunkSizeH;
    private int _chunkSizeD;

    private int _chunkCountW;
    private int _chunkCountH;
    private int _chunkCountD;

    private VoxelStructure[,,] _chunks;


    //Used to selectively turn on or off different voxel faces.
    //Useful optimization for 2d/2.5d games
    public bool DrawFront = true;
    public bool DrawBack = true;
    public bool DrawTop = true;
    public bool DrawBottom = true;
    public bool DrawLeft = true;
    public bool DrawRight = true;

    public bool GenerateSecondaryUvSet = false;
    public bool UsePalletteTexture = true;

    public Vector3 ScreenOffset
    {
        get { return new Vector3(-((_chunkSizeW * _chunkCountW) * 0.5f), -((_chunkSizeH * _chunkCountH) * 0.5f),
            -((_chunkSizeD * _chunkCountD) * 0.5f));
        }
    }

    public void Create(int width, int height, int depth, int chunkW, int chunkH, int chunkD, VoxelPallette palette)
    {
        Width = width;
        Height = height;
        Depth = depth;

        _chunkSizeW = chunkW > Width ? Width : chunkW;
        _chunkSizeH = chunkH > Height ? Height : chunkH;
        _chunkSizeD = chunkD > Depth ? Depth : chunkD;

        _chunkCountW = Width / _chunkSizeW;
        _chunkCountH = Height / _chunkSizeH;
        _chunkCountD = Depth / _chunkSizeD;
        
        _chunks = new VoxelStructure[_chunkCountW, _chunkCountH, _chunkCountD];

        var index = 0;
        for (var y = _chunkCountH-1; y >=0 ; y--)
        {
            for (var z = 0; z < _chunkCountD; ++z)
            {
                for (var x = 0; x < _chunkCountW; ++x)
                {
                    _chunks[x, y, z] = VoxelStructure.Create(gameObject, index, x, y, z, _chunkSizeW, _chunkSizeH,
                        _chunkSizeD, ScreenOffset, palette, this);

                    index++;
                }
            }
        }
    }

    public void SetVoxel(int x, int y, int z, int newVoxel)
    {
        y = (Height - y) - 1;

        if (x < 0 || y < 0 || z < 0 || x >= Width || y >= Height || z >= Depth)
            return;

        var cx = x / _chunkSizeW;
        var cy = y / _chunkSizeH;
        var cz = z / _chunkSizeD;

        var chunk = _chunks[cx,cy,cz];
        
        if (chunk == null)
            return;

        var coffX = x % _chunkSizeW;
        var coffY = y % _chunkSizeH;
        var coffZ = z % _chunkSizeD;

        chunk.SetVoxel(coffX, coffY, coffZ, newVoxel);
    }

    public int GetVoxel(int x, int y, int z)
    {
        if (x < 0 || y < 0 || z < 0 || x > Width || y > Height || z > Depth)
            return -1;

        var cx = x / _chunkSizeW;
        var cy = y / _chunkSizeH;
        var cz = z / _chunkSizeD;

        var chunk = _chunks[cx, cy, cz];

        if (chunk == null)
            return -1;

        var coffX = x % _chunkSizeW;
        var coffY = y % _chunkSizeH;
        var coffZ = z % _chunkSizeD;

        return chunk.GetVoxel(coffX, coffY, coffZ);
    }

    public void ClearIfLess(int value)
    {
         for (var z = 0; z < Depth; z++)
             for (var x = 0; x < Width; x++)
                 if (GetVoxel(x, value, z) == 0)
                     for (var h=0; h<value; h++)
                         SetVoxel(x,h,z,0);
             
    }

    public void Border(int size)
    {
        for (var z = 0; z < Depth; z++)
            for (var x = 0; x < size; x++)
                for (var h = 1; h < Height; h++)
                {
                    SetVoxel(x, h, z, 0);
                    SetVoxel(Width - x - 1, h, z, 0);
                }

        for (var x = 0; x < Width; x++)
            for (var z = 0; z < size; z++)
                for (var h = 1; h < Height; h++)
                {
                    SetVoxel(x, h, z, 0);
                    SetVoxel(x, h, Depth - z - 1, 0);
                }
    }

    public void Draw()
    {
        foreach (var voxelStructure in _chunks)
            if (voxelStructure.IsDirty)
                voxelStructure.Draw();
    }

    public void GetHeightMap(out float[,] array)
    {
        array = new float[Width, Height];

        for (var z = 0; z < Depth; z++)
            for (var x = 0; x < Depth; x++)
            {
                for (var h = 0; h < Height; h++)
                {
                    var value = GetVoxel(x, h, z);
                    if (value > 0)
                        array[x, z] = h;
                }
            }
    }
}
