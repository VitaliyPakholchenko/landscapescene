﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class VoxelPallette
{
    /// <summary>
    /// 
    /// </summary>
	public Material AtlasMaterial;

    /// <summary>
    /// 
    /// </summary>
	public VoxelTemplate[] voxelTemplates = new VoxelTemplate[1];

    /// <summary>
    /// 
    /// </summary>
    public List<string> voxelNames = new List<string>();

    /// <summary>
    /// 
    /// </summary>
	private List<KeyValuePair<int,int>> _lookup = new List<KeyValuePair<int,int>>();
    
    /// <summary>
    /// For display/UX purposes, we want to have a sorted, filtered list.
    /// To do this, we sort voxel templates by their display oder, while 
    /// constructing a "lookup table" to refer to later when we need to 
    /// get the "real" voxel template index in our voxelTemplates[] array
    /// </summary>
    public void ArrangeVoxelTemplates(){
		//wipe the list
		voxelNames = new List<string>();

		//sort the templates by their display order
		var sorted = voxelTemplates
			.Select((x, i) => new KeyValuePair<VoxelTemplate, int>(x, i))
			.OrderBy( x => { 
					if(x.Key != null) 
						return x.Key.DisplayOrder; 
					else
						return 100000;
					
				} )
			.ToList();

        //wipe the lookup
        _lookup = new List<KeyValuePair<int,int>>();

		//construct the lookup table and the display list (voxelNames)
		for(var i = 0; i<sorted.Count;i++){
			if(sorted[i].Key!=null){
			    _lookup.Add(new KeyValuePair<int, int>(i, sorted[i].Value));
				voxelNames.Add(sorted[i].Key.name);
			}
		}
	}	
}
