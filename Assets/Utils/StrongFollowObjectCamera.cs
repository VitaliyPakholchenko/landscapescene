﻿    using UnityEngine;
    using System;

[Serializable]
public class StrongFollowObjectCamera : MonoBehaviour
{
    public GameObject Target { get; set; }

    [SerializeField]
    public float SmoothXPlus = 0.0f;

    [SerializeField]
    public float SmoothYPlus = 0.0f;

    [SerializeField]
    public float SmoothZPlus = 0.0f;

    [SerializeField]
    public float SmoothXMinus = 0.0f;

    [SerializeField]
    public float SmoothYMinus = 0.0f;

    [SerializeField]
    public float SmoothZMinus = 0.0f;

    [SerializeField]
    public float Damping = 0.5f;

    [SerializeField] public bool ControllX;
    [SerializeField] public bool ControllY;
    [SerializeField] public bool ControllZ;

    void LateUpdate()
    {
        if (Target == null)
            return;

        var dx = Target.transform.position.x * ((Target.transform.position.x > 0) ? SmoothXPlus : SmoothXMinus);
        var dy = -Target.transform.position.y * ((Target.transform.position.y > 0) ? SmoothYPlus : SmoothYMinus);
        var dz = Target.transform.position.z * ((Target.transform.position.z > 0) ? SmoothZPlus : SmoothZMinus);

        var wantedPosition = new Vector3(ControllX ? dx : transform.position.x,
                                         ControllY ? dy : transform.position.y,
                                         ControllZ ? dz : transform.position.z);

        gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, wantedPosition,
            Mathf.Clamp(Damping, 0, 1));
    }
}
