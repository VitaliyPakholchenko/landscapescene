﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Object = UnityEngine.Object;

public delegate void Action<T1, T2, T3, T4, T5>(T1 v1, T2 v2, T3 v3, T4 v4, T5 v5);

public static partial class Utils
{
    //
    public static Vector3[] GetRandomCurvie(Vector3 startPosition, Vector3 endPosition, float curveAmpletuda)
    {
        var curviePoints = new List<Vector3>();

        endPosition.z = 1;
        startPosition.z = 1;

        var direction = Vector3.Normalize(endPosition - startPosition);
        var lenth = (endPosition - startPosition).magnitude;

        var pp1 = RandomHelper.Bool ? new Vector3(-direction.y, direction.x) : new Vector3(direction.y, -direction.x);
        var trinity1 = (direction * (lenth * 0.25f)) + (pp1 * curveAmpletuda);
        var trinity2 = (direction * (lenth * 0.5f)) + (-pp1 * curveAmpletuda);
        var trinity3 = (direction * (lenth * 0.75f)) + (pp1 * curveAmpletuda);

        curviePoints.Add(startPosition);
        curviePoints.Add(trinity1);
        curviePoints.Add(trinity2);
        curviePoints.Add(trinity3);
        curviePoints.Add(endPosition);
        return curviePoints.ToArray();
    }

    public static float GetLenth(ref Vector3[] path)
    {
        if (path.Length < 2)
            return 0;
        var len = 0f;

        for (var i = 1; i < path.Length; i++)
            len += Vector3.Distance(path[i - 1], path[i]);

        return len;
    }

    public static Color HexToColor(long hexColor)
    {
        var a = ((float)((hexColor & 0xFF000000) >> 24)) / 255;
        var r = ((float)((hexColor & 0x00FF0000) >> 16)) / 255;
        var g = ((float)((hexColor & 0x0000FF00) >> 8)) / 255;
        var b = ((float)(hexColor & 0x000000FF)) / 255;
        var color = new Color(r, g, b, a);
        return color;
    }

    public static Color ColorFromBytes(byte r, byte g, byte b, byte a)
    {
        var rf = r / 255f;
        var gf = g / 255f;
        var bf = b / 255f;
        var af = a / 255f;
        return  new Color(rf, gf, bf, af);
    }


    public static Color HexToMultiColor(long hexColor, long animHexColor)
    {
        var a = ((float)((hexColor & 0xFF000000) >> 24));
        var r = ((float)((hexColor & 0x00FF0000) >> 16));
        var g = ((float)((hexColor & 0x0000FF00) >> 8));
        var b = ((float)(hexColor & 0x000000FF));

        var a1 = ((float)((animHexColor & 0xFF000000) >> 24));
        var r1 = ((float)((animHexColor & 0x00FF0000) >> 16));
        var g1 = ((float)((animHexColor & 0x0000FF00) >> 8));
        var b1 = ((float)(animHexColor & 0x000000FF));

        var color = new Color(r * r1 / 255, g * g1 / 255, b * b1 / 255, a * a1 / 255);
        return color;
    }

    public static GameObject ChildByName(this GameObject _object, string name)
    {
        foreach (var trans in _object.transform)
        {
            var gobject = trans as GameObject;
            if (gobject==null)
                continue;

            if (gobject.name == name)
                return gobject;

            var inter = gobject.ChildByName(name);
            if (inter != null)
                return inter;
        }
        return null;
    }
    
    public static List<T> Splice<T>(this List<T> Source, int Start, int Size)
    {
        var retVal = Source.Skip(Start).Take(Size).ToList<T>();
        Source.RemoveRange(Start, Size);
        return retVal;
    }

    public static Vector3 QuadBezierPoint(Vector3 pos1, Vector3 pos2, Vector3 pos3, float t)
    {
        return (1 - t) * (1 - t) * pos1 + 2 * t * (1 - t) * pos2 + t * t * pos3;
    }

    public static void PositionX(this Transform transform, float value)
    {
        var pos = transform.localPosition;
        pos.x = value;
        transform.localPosition = pos;
    }

    public static void PositionY(this Transform transform, float value)
    {
        var pos = transform.localPosition;
        pos.y = value;
        transform.localPosition = pos;
    }

    public static void PositionZ(this Transform transform, float value)
    {
        var pos = transform.localPosition;
        pos.z = value;
        transform.localPosition = pos;
    }

    public static MemoryStream OpenResourcesStream(string pathName)
    {
        var asset = Resources.Load<TextAsset>(pathName);
        return new MemoryStream(asset.bytes);
    }

    public static string[] GetResourcesFileNames(string path, string searchPattern, SearchOption option)
    {
        var dataPath = Path.Combine(Application.dataPath,"Resources");
        dataPath = Path.Combine(dataPath, path);
        var fileNames = Directory.GetFiles(dataPath, searchPattern, option);

        var result = new List<string>();
        foreach (var fileName in fileNames)
        {
            var ind = fileName.IndexOf("Resources", StringComparison.Ordinal);
            var absolet = fileName.Substring(ind+10, fileName.Length - ind - 10);
            result.Add(absolet);
        }

        return result.ToArray();
    }

    public static Material CreateMaterial(string shaderName, int renderQueue = -1)
    {
        var shader = Shader.Find(shaderName);

        if (shader == null)
        {
            Debug.LogError("Failed to find the '" + shaderName + "' shader");
        }

        var material = new Material(shader);

        if (renderQueue != -1)
        {
            material.renderQueue = renderQueue;
        }

        return material;
    }

    public static void SetRenderQueues(Material[] ms, int rq)
    {
        if (ms != null)
        {
            foreach (var m in ms)
            {
                SetRenderQueue(m, rq);
            }
        }
    }

    public static void SetRenderQueue(Material m, int rq)
    {
        if (m != null)
        {
            if (m.renderQueue != rq)
            {
                m.renderQueue = rq;
            }
        }
    }

    public static Vector3 SwapYZ(this Vector3 vector)
    {
        return new Vector3(vector.x,vector.z,vector.y);
    }

    public static Vector2 ToVector2(this Vector3 vector)
    {
        return new Vector2(vector.x, vector.z);
    }

    public static Color FromBytes(byte r, byte g, byte b, byte a)
    {
        return new Color(r/255f,g/25f,b/255f,a/255f);
    }

    public static bool PointInsideSphere(Vector3 worldPoint, Vector3 worldSpgere, float radius)
    {
        var d = Vector3.Distance(worldPoint, worldSpgere);
        return d <= radius;
    }
    
    public static string GetId()
    {
        return Guid.NewGuid().ToString();
    }

    private static MD5 _md5;
    public static uint GetHash()
    {
        if (_md5 == null)
            _md5 = MD5.Create();

        var guid = GetId();
        var hashed = _md5.ComputeHash(Encoding.UTF8.GetBytes(guid));
        return BitConverter.ToUInt32(hashed, 0);
    }

    public class Drawing
    {
        //****************************************************************************************************
        //  static function DrawLine(rect : Rect) : void
        //  static function DrawLine(rect : Rect, color : Color) : void
        //  static function DrawLine(rect : Rect, width : float) : void
        //  static function DrawLine(rect : Rect, color : Color, width : float) : void
        //  static function DrawLine(Vector2 pointA, Vector2 pointB) : void
        //  static function DrawLine(Vector2 pointA, Vector2 pointB, color : Color) : void
        //  static function DrawLine(Vector2 pointA, Vector2 pointB, width : float) : void
        //  static function DrawLine(Vector2 pointA, Vector2 pointB, color : Color, width : float) : void
        //  
        //  Draws a GUI line on the screen.
        //  
        //  DrawLine makes up for the severe lack of 2D line rendering in the Unity runtime GUI system.
        //  This function works by drawing a 1x1 texture filled with a color, which is then scaled
        //   and rotated by altering the GUI matrix.  The matrix is restored afterwards.
        //****************************************************************************************************

        public static Texture2D lineTex;

        public static void DrawLine(Rect rect) { DrawLine(rect, GUI.contentColor, 1.0f); }
        public static void DrawLine(Rect rect, Color color) { DrawLine(rect, color, 1.0f); }
        public static void DrawLine(Rect rect, float width) { DrawLine(rect, GUI.contentColor, width); }
        public static void DrawLine(Rect rect, Color color, float width) { DrawLine(new Vector2(rect.x, rect.y), new Vector2(rect.x + rect.width, rect.y + rect.height), color, width); }
        public static void DrawLine(Vector2 pointA, Vector2 pointB) { DrawLine(pointA, pointB, GUI.contentColor, 1.0f); }
        public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color) { DrawLine(pointA, pointB, color, 1.0f); }
        public static void DrawLine(Vector2 pointA, Vector2 pointB, float width) { DrawLine(pointA, pointB, GUI.contentColor, width); }
        public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color, float width)
        {
            // Save the current GUI matrix, since we're going to make changes to it.
            Matrix4x4 matrix = GUI.matrix;

            // Generate a single pixel texture if it doesn't exist
            if (!lineTex)
            {
                lineTex = new Texture2D(1, 1);
                lineTex.SetPixel(0,0,new Color(1,1,1,1));
            }

            // Store current GUI color, so we can switch it back later,
            // and set the GUI color to the color parameter
            Color savedColor = GUI.color;
            GUI.color = color;

            // Determine the angle of the line.
            float angle = Vector3.Angle(pointB - pointA, Vector2.right);

            // Vector3.Angle always returns a positive number.
            // If pointB is above pointA, then angle needs to be negative.
            if (pointA.y > pointB.y) { angle = -angle; }

            // Use ScaleAroundPivot to adjust the size of the line.
            // We could do this when we draw the texture, but by scaling it here we can use
            //  non-integer values for the width and length (such as sub 1 pixel widths).
            // Note that the pivot point is at +.5 from pointA.y, this is so that the width of the line
            //  is centered on the origin at pointA.
            GUIUtility.ScaleAroundPivot(new Vector2((pointB - pointA).magnitude, width), new Vector2(pointA.x, pointA.y + 0.5f));

            // Set the rotation for the line.
            //  The angle was calculated with pointA as the origin.
            GUIUtility.RotateAroundPivot(angle, pointA);

            // Finally, draw the actual line.
            // We're really only drawing a 1x1 texture from pointA.
            // The matrix operations done with ScaleAroundPivot and RotateAroundPivot will make this
            //  render with the proper width, length, and angle.
            GUI.DrawTexture(new Rect(pointA.x, pointA.y, 1, 1), lineTex);

            // We're done.  Restore the GUI matrix and GUI color to whatever they were before.
            GUI.matrix = matrix;
            GUI.color = savedColor;
        }

        public static void LinePostRender(Material mat, Vector3 start, Vector3 end)
        {
            GL.PushMatrix();
            mat.SetPass(0);
            GL.LoadOrtho();
            GL.Begin(GL.LINES);
            GL.Color(Color.red);
            GL.Vertex(start);
            GL.Vertex(end);
            GL.End();
            GL.PopMatrix();
        }
    }
}

