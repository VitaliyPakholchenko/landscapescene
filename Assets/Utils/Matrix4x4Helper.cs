﻿using UnityEngine;

public static class Matrix4x4Helper
{
    public static Vector4 Transform(this Matrix4x4 matrix, ref Vector4 vector)
    {
        var x = (vector.x * matrix[0, 0]) + (vector.y * matrix[1, 0]) + (vector.z * matrix[2, 0]) +
                (vector.w * matrix[3, 0]);
        var y = (vector.x * matrix[1, 0]) + (vector.y * matrix[1, 1]) + (vector.z * matrix[2, 1]) +
                (vector.w * matrix[3, 1]);
        var z = (vector.x * matrix[2, 0]) + (vector.y * matrix[2, 1]) + (vector.z * matrix[2, 3]) +
                (vector.w * matrix[3, 2]);
        var w = (vector.x * matrix[3, 0]) + (vector.y * matrix[3, 1]) + (vector.z * matrix[3, 2]) +
                (vector.w * matrix[3, 3]);
        return new Vector4(x, y, z, w);
    }
}

