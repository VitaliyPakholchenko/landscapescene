﻿using System;
using UnityEngine;

public class TimeBombMBH : MonoBehaviour
{
    public float Duration;
    private float _elapsed;
    private bool _complete;

    public MonoBehaviour Owner
    {
        get; private set;
    }

    public event Action<TimeBombMBH> OnComplete;

    public void Complete()
    {
        if (_complete)
            return;

        _complete = true;

        if (OnComplete != null)
        {
            var complete = OnComplete;
            OnComplete = null;
            complete(this);
        }

        Owner = null;
    }

    private void Initialize(float seconds)
    {
            
    }

    public void Stop()
    {
        Complete();
        DestroyImmediate(gameObject);
    }

    void Update()
    {
        if (_complete)
            return;

        _elapsed += Time.deltaTime;
        if (_elapsed >= Duration)
        {
            Complete();
            DestroyImmediate(gameObject);
        }
    }

    public static TimeBombMBH Create(MonoBehaviour owner, float seconds, string name)
    {
        var tbObj = new GameObject("TimeBomb_" + name);
        var tb = tbObj.AddComponent<TimeBombMBH>();

        if (owner != null)
            tb.Owner = owner;

        tb.Duration = seconds;

        return tb;
    }

    void OnDestroy()
    {
        Complete();
    }
}