﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[Serializable]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class CircleRenderer : MonoBehaviour
{
    private float _slices = 4;
    public float Slices
    {
        get { return _slices; }
        set { _slices = Mathf.Clamp(value,4,1000); }
    }

    private float _outRadius = 1.0f;
    public float OutRadius
    {
        get { return _outRadius;}
        set { _outRadius = value; }
    }

    public float _inRadius = 0.5f;
    public float InRadius
    {
        get { return _inRadius; }
        set { _inRadius = value; }
    }


    public void Refresh()
    {
         var angleStep = 360.0f / Slices;
         var vertexList = new List<Vector3>();
         var triangleList = new List<int>();
         var quaternion = Quaternion.Euler(0.0f, angleStep, 0.0f);

         // Make first triangle.
         vertexList.Add(new Vector3(0.0f, 0.0f, 0.0f));  // 1. Circle center.
         vertexList.Add(new Vector3(0.0f, OutRadius, 0.0f));  // 2. First vertex on circle outline (radius = 0.5f)
         vertexList.Add(quaternion * vertexList[1]);     // 3. First vertex on circle outline rotated by angle)
                                                         // Add triangle indices.
         triangleList.Add(0);
         triangleList.Add(1);
         triangleList.Add(2);
         for (var i = 0; i < Slices - 1; i++)
         {
             triangleList.Add(0);                      // Index of circle center.
             triangleList.Add(vertexList.Count - 1);
             triangleList.Add(vertexList.Count);
             vertexList.Add(quaternion * vertexList[vertexList.Count - 1]);
         }

        var meshFilter = GetComponent<MeshFilter>();
        meshFilter.sharedMesh = new Mesh();
        meshFilter.sharedMesh.vertices = vertexList.ToArray();
        meshFilter.sharedMesh.triangles = triangleList.ToArray();
    }

#if UNITY_EDITOR
    [MenuItem("GameObject/3D Object/Circle")]
    public static void Create()
    {
        var ciObj = new GameObject("Circle");
        var cr = ciObj.AddComponent<CircleRenderer>();
        cr.Refresh();
    }

    [CustomEditor(typeof(CircleRenderer))]
    public class CircleRendererEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var cr = (CircleRenderer)target;

            cr.OutRadius = EditorGUILayout.FloatField("OutRadius", cr.OutRadius);
            cr.InRadius = EditorGUILayout.FloatField("InRadius", cr.InRadius);
            cr.Slices = EditorGUILayout.FloatField("Slices", cr.Slices);

            if (GUILayout.Button("Refresh"))
            {
                cr.Refresh();
            }
        }
    }
#endif
}

