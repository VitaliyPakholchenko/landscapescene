using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Linq;
using UnityEngine;

public static class StringHelper
{
    // ---------------------------------------------------------------------
    // Object to string
    // ---------------------------------------------------------------------

    public static string ToString<T>(List<T> list)
        where T : struct, IConvertible
    {
        if (list.Count == 0) return string.Empty;
        var result = list.Aggregate(string.Empty, (current, val) => current + (ToString(val) + ","));
        return string.IsNullOrEmpty(result) ? string.Empty : result.Substring(0, result.Length - 1);
    }

    public static string ToString(Vector4 vector)
    {
        return vector.x.ToString(CultureInfo.InvariantCulture) + "," +
               vector.y.ToString(CultureInfo.InvariantCulture) +
               "," + vector.z.ToString(CultureInfo.InvariantCulture) + "," +
               vector.w.ToString(CultureInfo.InvariantCulture);
    }

    public static string ToString(Vector3 vector)
    {
        return vector.x.ToString(CultureInfo.InvariantCulture) + "," +
               vector.y.ToString(CultureInfo.InvariantCulture) +
               "," + vector.z.ToString(CultureInfo.InvariantCulture);
    }

    public static string ToString(Rect value)
    {
        return value.x.ToString(CultureInfo.InvariantCulture) + "," + value.y.ToString(CultureInfo.InvariantCulture) +
               "," + value.width.ToString(CultureInfo.InvariantCulture) + "," +
               value.height.ToString(CultureInfo.InvariantCulture);
    }

    public static string ToString<T>(T value) where T : struct, IConvertible
    {
        return value.ToString(CultureInfo.InvariantCulture);
    }

    public static string TimeToString(float timeSeconds)
    {
        var time = TimeSpan.FromSeconds(timeSeconds);

        var minutes = time.Minutes < 10
            ? "0" + time.Minutes.ToString(CultureInfo.InvariantCulture)
            : time.Minutes.ToString(CultureInfo.InvariantCulture);
        var seconds = time.Seconds < 10
            ? "0" + time.Seconds.ToString(CultureInfo.InvariantCulture)
            : time.Seconds.ToString(CultureInfo.InvariantCulture);

        return minutes + ":" + seconds;
    }

    public static string DateSkillsTimeToString(TimeSpan date, bool addseconds)
    {
        var hours = date.Hours < 10
            ? "0" + date.Hours.ToString(CultureInfo.InvariantCulture)
            : date.Hours.ToString(CultureInfo.InvariantCulture);
        var minutes = date.Minutes < 10
            ? "0" + date.Minutes.ToString(CultureInfo.InvariantCulture)
            : date.Minutes.ToString(CultureInfo.InvariantCulture);
        var seconds = date.Seconds < 10
            ? "0" + date.Seconds.ToString(CultureInfo.InvariantCulture)
            : date.Seconds.ToString(CultureInfo.InvariantCulture);

        if (date.Hours == 0 && date.Minutes == 0)
            return minutes + ":" + seconds;
        else
            return hours + ":" + minutes + (addseconds ? (":" + seconds) : "");
    }

    public static string DateTimeSkillsTimeToString(DateTime date, bool addseconds = false)
    {
        var hours = date.Hour < 10
            ? "0" + date.Hour.ToString(CultureInfo.InvariantCulture)
            : date.Hour.ToString(CultureInfo.InvariantCulture);
        var minutes = date.Minute < 10
            ? "0" + date.Minute.ToString(CultureInfo.InvariantCulture)
            : date.Minute.ToString(CultureInfo.InvariantCulture);
        var seconds = date.Second < 10
            ? "0" + date.Second.ToString(CultureInfo.InvariantCulture)
            : date.Second.ToString(CultureInfo.InvariantCulture);

        if (date.Hour == 0 && date.Minute == 0)
            return minutes + ":" + seconds;
        else
            return hours + ":" + minutes + (addseconds ? (":" + seconds) : "");
    }

    public static string DateTimeToString(DateTime date)
    {
        var minutes = date.Minute < 10
            ? "0" + date.Minute.ToString(CultureInfo.InvariantCulture)
            : date.Minute.ToString(CultureInfo.InvariantCulture);
        var seconds = date.Second < 10
            ? "0" + date.Second.ToString(CultureInfo.InvariantCulture)
            : date.Second.ToString(CultureInfo.InvariantCulture);
        return minutes + ":" + seconds;
    }

    public static string TimeSpanToString(TimeSpan date)
    {
        var minutes = date.Minutes < 10
            ? "0" + date.Minutes.ToString(CultureInfo.InvariantCulture)
            : date.Minutes.ToString(CultureInfo.InvariantCulture);
        var seconds = date.Seconds < 10
            ? "0" + date.Seconds.ToString(CultureInfo.InvariantCulture)
            : date.Seconds.ToString(CultureInfo.InvariantCulture);
        return minutes + ":" + seconds;
    }

    public static string[] EnumToString<T>()
    {
        return Enum.GetNames(typeof (T));
    }

    public static int GetIndex(string[] arry, string value)
    {
        int index = 0;
        foreach (var s in arry)
        {
            if (s == value)
                return index;
            index++;
        }
        return -1;
    }

    public static string ToString<T1, T2>(Dictionary<T1, T2> dictionary)
        where T1 : struct, IConvertible
        where T2 : struct, IConvertible
    {
        if (dictionary == null)
            return string.Empty;

        var result = string.Empty;
        foreach (var pair in dictionary)
            result += ToString(pair.Key) + "," + ToString(pair.Value) + ";";
        return string.IsNullOrEmpty(result) ? string.Empty : result.Substring(0, result.Length - 1);
    }

    public static string ToString<T1>(Dictionary<string, T1> dictionary)
        where T1 : struct, IConvertible
    {
        if (dictionary == null)
            return string.Empty;

        var result = string.Empty;
        foreach (var pair in dictionary)
            result += pair.Key + "," + ToString(pair.Value) + ";";
        return string.IsNullOrEmpty(result) ? string.Empty : result.Substring(0, result.Length - 1);
    }

    // ---------------------------------------------------------------------
    // String to object
    // ---------------------------------------------------------------------
    public static List<T> ToArray<T>(string value)
        where T : struct, IConvertible
    {
        if (string.IsNullOrEmpty(value))
            return new List<T>();

        var values = value.Split(',');
        var result = new List<T>();
        foreach (var val in values)
        {
            T tval = (T) Convert.ChangeType(val, typeof (T), CultureInfo.InvariantCulture);
            result.Add(tval);
        }
        return result;
    }

    // ---------------------------------------------------------------------
    public static Dictionary<T1, T2> ToDictionary<T1, T2>(string value)
        where T1 : struct
        where T2 : struct
    {
        var result = new Dictionary<T1, T2>();

        if (string.IsNullOrEmpty(value))
            return result;

        var values = value.Split(';');

        foreach (var val in values)
        {
            var pair = val.Split(',');
            var p1 = (T1) Convert.ChangeType(pair[0], typeof (T1), CultureInfo.InvariantCulture);
            var p2 = (T2) Convert.ChangeType(pair[1], typeof (T2), CultureInfo.InvariantCulture);
            result.Add(p1, p2);
        }

        return result;
    }

    // ---------------------------------------------------------------------
    public static Dictionary<string, T1> ToDictionary<T1>(string value)
        where T1 : struct
    {
        var result = new Dictionary<string, T1>();

        if (string.IsNullOrEmpty(value))
            return result;

        var values = value.Split(';');

        foreach (var val in values)
        {
            var pair = val.Split(',');
            var p1 = pair[0];
            var p2 = (T1) Convert.ChangeType(pair[1], typeof (T1), CultureInfo.InvariantCulture);
            result.Add(p1, p2);
        }

        return result;
    }

    // ---------------------------------------------------------------------
    public static Vector4 ToVector4(string value)
    {
        value = value.Trim();
        value =
            value.Replace("{", "")
                .Replace("}", "")
                .Replace("X", "")
                .Replace("Y", "")
                .Replace("Z", "")
                .Replace("W", "")
                .Replace(":", "")
                .Replace(' ', ',');
        var values = value.Split(',');
        return values.Length < 4
            ? Vector4.zero
            : new Vector4(ToFloat(values[0]), ToFloat(values[1]), ToFloat(values[2]), ToFloat(values[3]));
    }

    public static Vector3 ToVector3(string value, char splitter)
    {
        if (string.IsNullOrEmpty(value) || value == "null")
            return Vector3.zero;

        value =
            value.Replace("{", "")
                .Replace("}", "")
                .Replace("X", "")
                .Replace("Y", "")
                .Replace("Z", "")
                .Replace(":", "")
                .Replace(' ', ',');

        var xyz = value.Split(splitter);

        if (xyz.Length > 2)
            return new Vector3(ToFloat(xyz[0]), ToFloat(xyz[1]), ToFloat(xyz[2]));

        if (xyz.Length > 1)
            return new Vector3(ToFloat(xyz[0]), ToFloat(xyz[1]), 0.0f);

        return xyz.Length == 1 ? new Vector3(ToFloat(xyz[0]), 0.0f, 0.0f) : Vector3.zero;
    }

    //
    // ---------------------------------------------------------------------
    public static Vector2 ToVector2(string value)
    {
        if (value == string.Empty)
            return Vector2.zero;

        value =
            value.Replace("{", "")
                .Replace("}", "")
                .Replace("X", "")
                .Replace("Y", "")
                .Replace(":", "")
                .Replace(' ', ',');

        var xy = value.Split(',');

        if (xy.Length > 1)
            return new Vector2(ToFloat(xy[0]), ToFloat(xy[1]));

        return xy.Length == 1 ? new Vector2(ToFloat(xy[0]), 0.0f) : Vector2.zero;
    }

    //
    // ---------------------------------------------------------------------
    public static Color ToColor(string value)
    {
        if (value == string.Empty)
            return Color.white;

        value =
            value.Replace("{", "")
                .Replace("}", "")
                .Replace("X", "")
                .Replace("Y", "")
                .Replace("Z", "")
                .Replace("W", "")
                .Replace(":", "")
                .Replace(' ', ',');
        var rgba = value.Split(',');

        return rgba.Length > 3
            ? new Color(ToFloat(rgba[0]), ToFloat(rgba[1]), ToFloat(rgba[2]), ToFloat(rgba[3]))
            : new Color(ToFloat(rgba[0]), ToFloat(rgba[1]), ToFloat(rgba[2]), 1.0f);
    }

    public static T ToEnum<T>(string value)
    {
        value = value.Trim();
        if (Enum.IsDefined(typeof (T), value))
            return (T) Enum.Parse(typeof (T), value, true);
        return default(T);
    }

    public static Rect ToRectangle(string value)
    {
        value = value.Trim();
        var rvalues = value.Split(',');
        return new Rect(ToInt(rvalues[0]), ToInt(rvalues[1]), ToInt(rvalues[2]), ToInt(rvalues[3]));
    }

    public static bool ToBoolean(string value)
    {
        bool boolValue;
        bool.TryParse(value, out boolValue);
        return boolValue;
    }

    public static int ToInt(string value)
    {
        int intValue;
        int.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out intValue);
        return intValue;
    }

    public static float ToFloat(string value)
    {
        float floatValue;
        float.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out floatValue);
        return floatValue;
    }

    public static string[] RegexSplit(string value, string splitter)
    {
        return Regex.Split(value, splitter);
    }

    public static string Substring(string source, string sequence)
    {
        if (string.IsNullOrEmpty(source) || string.IsNullOrEmpty(sequence))
            return string.Empty;

        var index = source.LastIndexOf(sequence, StringComparison.Ordinal) + sequence.Length + 1;
        return source.Substring(index, source.Length - index);
    }
}
