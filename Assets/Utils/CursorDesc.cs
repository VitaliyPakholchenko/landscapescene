﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class CursorDesc : ScriptableObject
{
    [SerializeField]
    public Texture2D Texture;
    [SerializeField]
    public Vector2 Offset;
}