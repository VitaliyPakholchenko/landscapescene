﻿using System.IO;

    using UnityEngine;

    public class SpriteCollection
    {
        private Sprite[] _sprites;
        private string[] _names;
        
        public void Load(string spritesheet)
        {
            _sprites = Resources.LoadAll<Sprite>(spritesheet);
            _names = new string[_sprites.Length];

            for (var i = 0; i < _names.Length; i++)
                _names[i] = _sprites[i].name;
        }

        public void LoadTexture(string textureName, int frameWidth, int frameHeight, float pixelPerUnit)
        {
            var texture = Resources.Load<Texture2D>(textureName);
            var columns = texture.width / frameWidth;
            var rows = texture.height / frameHeight;
            var name = Path.GetFileNameWithoutExtension(textureName);

            _names = new string[columns * rows];
            _sprites = new Sprite[columns * rows];

            var frameIndex = 0;
            for (var y =0; y<rows; y++)
                for (var x = 0; x < columns; x++)
                {
                    _names[frameIndex] = name + frameIndex;
                    _sprites[frameIndex] = Sprite.Create(texture,
                        new Rect(x * frameWidth, y * frameHeight, frameWidth, frameHeight),new Vector2(0.5f,0.5f), pixelPerUnit);
                    frameIndex++;
                }
        }
        
        public Sprite GetSprite(string name)
        {
            return _sprites[System.Array.IndexOf(_names, name)];
        }

        public Sprite GetSprite(int index)
        {
            return _sprites[index];
        }
    }