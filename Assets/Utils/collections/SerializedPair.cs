﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public class NullablePair<TKey, TValue>
{
    public TKey Key;
    public TValue Value;

    public NullablePair(TKey key, TValue value)
    {
        Key = key;
        Value = value;
    }
}

[Serializable]
public struct SerializedPair <TKey, TValue> : IEquatable<SerializedPair<TKey, TValue>>
{
    [SerializeField]
    public TKey Key;

    [SerializeField]
    public TValue Value;

    public bool Equals(SerializedPair<TKey, TValue> other)
    {
        return EqualityComparer<TKey>.Default.Equals(Key, other.Key) && EqualityComparer<TValue>.Default.Equals(Value, other.Value);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is SerializedPair<TKey, TValue> && Equals((SerializedPair<TKey, TValue>) obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            return (EqualityComparer<TKey>.Default.GetHashCode(Key)*397) ^ EqualityComparer<TValue>.Default.GetHashCode(Value);
        }
    }

    public SerializedPair(TKey key, TValue value)
    {
        Key = key;
        Value = value;
    }

    public void Set(TKey key, TValue value)
    {
        Key = key;
        Value = value;
    }
    
    public static bool operator ==(SerializedPair<TKey, TValue> p1, SerializedPair<TKey, TValue> p2)
    {
        return p1.Key.Equals(p2.Key) && p1.Value.Equals(p2.Value);
    }

    public static bool operator !=(SerializedPair<TKey, TValue> p1, SerializedPair<TKey, TValue> p2)
    {
        return !p1.Key.Equals(p2.Key) || !p1.Value.Equals(p2.Value);
    }
}

[Serializable]
public class SerializedPairList<TKey, TValue>
{
    [SerializeField]
    public List<SerializedPair<TKey, TValue>> Pairs = new List<SerializedPair<TKey, TValue>>();

    public int Count
    {
        get { return Pairs.Count; }
    }

    public bool ContainsKey(TKey key)
    {
        return Pairs.Any(v => EqualityComparer<TKey>.Default.Equals(key, v.Key));
    }

    public TValue this[TKey index]
    {
        get
        {
            foreach (var i in Pairs)
            {
                if (EqualityComparer<TKey>.Default.Equals(index, i.Key))
                    return i.Value;
            }
            return default(TValue);
        }

        set
        {
            for (var i = 0; i < Pairs.Count; i++)
                if (EqualityComparer<TKey>.Default.Equals(index, Pairs[i].Key))
                {
                    Pairs[i] = new SerializedPair<TKey, TValue>(Pairs[i].Key, value);
                    return;
                }
            Add(index, value);
        }
    }

    public SerializedPair<TKey, TValue> Getpair(int index)
    {
        return Pairs[index];
    }

    public void SetPair(int index, TKey key, TValue value)
    {
        Pairs[index].Set(key, value);
    }

    public TKey GetKey(int index)
    {
        var iterator = Pairs.GetEnumerator();
        while (iterator.MoveNext())
        {
            index--;
            if (index == 0)
                return iterator.Current.Key;
        }

        return default(TKey);
    }

    public List<TValue> GetValues()
    {
        var result = new List<TValue>();
        Pairs.ForEach(p=> result.Add(p.Value));
        return result;
    }

    public List<TKey> GetKeys()
    {
        var result = new List<TKey>();
        Pairs.ForEach(p => result.Add(p.Key));
        return result;
    }

    public virtual void Add(TKey key, TValue value)
    {
        Pairs.Add(new SerializedPair<TKey, TValue>(key, value));
    }

    public virtual void Remove(TKey key)
    {
        Pairs.RemoveAll(v=>EqualityComparer<TKey>.Default.Equals(key, v.Key));
    }

    public void Clear()
    {
        Pairs.Clear();
    }

    public SerializedPair<TKey, TValue> FirstOrDefault(Func<SerializedPair<TKey, TValue>,bool> func)
    {
        return Pairs.FirstOrDefault(func);
    }

    public void ForEach(Action<SerializedPair<TKey, TValue>> action)
    {
        Pairs.ForEach(action);
    }

    public IEnumerator<SerializedPair<TKey, TValue>> GetEnumerator()
    {
        return Pairs.GetEnumerator();
    }

    public IEnumerable<SerializedPair<TKey, TValue>> GetReverse()
    {
        return Pairs.GetReverse();
    }

    public bool Any()
    {
        return Pairs.Any();
    }

    public bool Any(Func<SerializedPair<TKey, TValue>, bool> func)
    {
        return Pairs.Any(func);
    }

    public SerializedPair<TKey, TValue> First()
    {
        return Pairs.First();
    }

    public void RemoveAt(int index)
    {
        Pairs.RemoveAt(index);
    }
    
    public SerializedPairList<TKey, TValue> GetCopy()
    {
        var copy = new SerializedPairList<TKey, TValue>();
        foreach (var item in Pairs)
            copy.Add(item.Key, item.Value);
        return copy;
    }

    public void AddRange(SerializedPairList<TKey, TValue> dictionary)
    { }
}