﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class PrefabsList<T> : List<T> where T : MonoBehaviour
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static PrefabsList<T> Load(string path)
    {
        var objects = Resources.LoadAll<T>(path);
        if (objects == null || objects.Length == 0)
            return null;

        var array = new PrefabsList<T>();
        array.AddRange(objects);
        return array;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    /// <param name="parent"></param>
    /// <returns></returns>
    public T Instantiate(string name, Transform parent)
    {
        var obj = this.FirstOrDefault(p => p.name == name);
        if (obj == null)
        {
            Debug.LogError("Object " + name + " of type " + typeof(T) + " is absent.");
            return null;
        }

        if (parent==null)
            return (T) GameObject.Instantiate(obj);
        
        return (T)GameObject.Instantiate(obj, parent);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    public T Get(string name)
    {
        return this.FirstOrDefault(p => p.name == name);
    }
}

