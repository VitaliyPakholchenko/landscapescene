using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

[Serializable]
public class WeightedOptions : PairStringIntDictionary
{   
    [SerializeField]
    private int _totalWeight;

    public bool IsEmpty
    {
        get { return 0 == _totalWeight; }
    }

    public string Choose()
    {
        if (_totalWeight == 0)
            return string.Empty;
        var value = RandomHelper.Int(0, _totalWeight - 1);
        var iterator = Pairs.GetEnumerator();
        while (iterator.MoveNext())
        {
            if (value <= iterator.Current.Value)
                break;
            value -= iterator.Current.Value;
        }
        return iterator.Current.Key;
    }
    
    public override void Set(string key, int weight)
    {
        if (weight > 0)
        {
            if (ContainsKey(key))
                _totalWeight-=this[key];
            base.Set(key,weight);
            _totalWeight += weight;
        }
        Remove(key);
    }
}
