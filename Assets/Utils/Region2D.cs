using System;
using UnityEngine;
using System.Collections.Generic;

/*struct Triangle
{
	public int A;
	public int B;
	public int C;

	public Triangle(int a, int b, int c)
	{
		A = a;
		B = b;
		C = c;
	}
}*/

[Serializable]
public class Region2D
{
    [SerializeField]
    public List<Vector2> Points = new List<Vector2>();
    [SerializeField]
    private bool _cicled = false;
    public bool Ciled
    {
        get
        {
            return _cicled;
        }
        set
        {
            if (_cicled == value)
                return;

            if (_cicled && !value)
                if (Points.Count > 2)
                    Points.RemoveAt(Points.Count - 1);

            if (!_cicled && value)
                if (Points.Count > 2)
                    Points.Add(Points[0]);

            _cicled = value;
        }
    }

    public readonly List<Vector2> TransformedPoints = new List<Vector2>();

    public void UpdateTransform()
    {
        /*if (mParent != null)
        {
            mTransformedPoints.Clear();
            mTransformedPoints.AddRange(Points);

            Matrix4x4 trs = Matrix4x4.TRS(mParent.position, mParent.localRotation, mParent.localScale);

            for (int i = 0; i < mTransformedPoints.Count; i++)
            {
                Vector3 vec = mTransformedPoints[i];
                vec = trs.MultiplyPoint(vec);
                mTransformedPoints[i] = vec;
            }        
        }*/
    }

	public void Add(Vector2 point)
	{
		if (_cicled)
		{
			if (Points.Count >= 2)
			{                
				Points.Insert(Points.Count - 2, point);
				return;
			}
            Points.Add(point);
		}
		Points.Add(point);
	}
			
	public bool Inside(float x, float y)
	{
		return Inside(new Vector2(x, y));
	}

    public bool Inside(Vector2 point)
    {
        if (Points.Count < 3)
            return false;

        /*if (mParent != null)
            return InsideTrianleAlgorithm(ref mTransformedPoints, point);*/

        return InsideTrianleAlgorithm(ref Points, point);
    }

    public bool InsideTrianleAlgorithm (ref List<Vector2> points, Vector2 point)
	{
		var x = (int)point.x;
		var y = (int)point.y;
		var flag = false;
		var N = points.Count;

		for (var n = 0; n < N; n++)
		{
			flag = false;
			var i1 = n < N - 1 ? n + 1 : 0;

			while (!flag)
			{
				var i2 = i1 + 1;
				if (i2 >= N)
					i2 = 0;

                if (i2 == (n < N - 1 ? n + 1 : 0))
					break;

                var s = Mathf.Abs((int)points[i1].x * ((int)points[i2].y - (int)points[n].y) +
                         (int)points[i2].x * ((int)points[n].y - (int)points[i1].y) +
                         (int)points[n].x * ((int)points[i1].y - (int)points[i2].y));
                var s1 = Mathf.Abs((int)points[i1].x * ((int)points[i2].y - y) +
                          (int)points[i2].x * (y - (int)points[i1].y) +
                          x * ((int)points[i1].y - (int)points[i2].y));
                var s2 = Mathf.Abs((int)points[n].x * ((int)points[i2].y - y) +
                          (int)points[i2].x * (y - (int)points[n].y) +
                          x * ((int)points[n].y - (int)points[i2].y));
                var s3 = Mathf.Abs((int)points[i1].x * ((int)points[n].y - y) +
                          (int)points[n].x * (y - (int)points[i1].y) +
                          x * ((int)points[i1].y - (int)points[n].y));

				if (s == s1 + s2 + s3)
				{
					flag = true;
					break;
				}

				i1 = i1 + 1;
				if (i1 >= N)
					i1 = 0;
			}

			if (flag == false)
				break;
		}
		return flag;
	}	
}
