﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;


#if UNITY_EDITOR
using UnityEditor;
#endif

public class MiniMap : RawImage, IPointerClickHandler
{
    public Texture2D BackTexture;

    private Vector3 topLeftPosition;
    private Vector3 topRightPosition;
    private Vector3 bottomLeftPosition;
    private Vector3 bottomRightPosition;

    private Camera _globalCamera;
    private BoxCollider _floorCollider;
    private Material _material;
    private Camera _camera;

    private readonly List<MiniMapPoint> _points = new List<MiniMapPoint>();

    void Update()
    {
        if (_globalCamera == null)
        {
            var cobject = GameObject.FindWithTag("MainCamera");
            _globalCamera = cobject.GetComponent<Camera>();
        }

        if (_floorCollider == null)
        {
            var tobj = GameObject.FindWithTag("Terrain");
            _floorCollider = tobj.GetComponent<BoxCollider>();
        }

        if (_camera == null)
        {
            _camera = GetComponent<Camera>();
            _camera.targetTexture = new RenderTexture(256, 256, 16, RenderTextureFormat.ARGB32);
            _camera.targetTexture.Create();
        }

        if (_material == null)
        {
            var shader = Shader.Find("Hidden/Internal-Colored");
            _material = new Material(shader);
            _material.hideFlags = HideFlags.HideAndDontSave;

            // Set blend mode to invert destination colors.
           // _material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
           // _material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);

            // Turn off backface culling, depth writes, depth test.
            _material.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            _material.SetInt("_ZWrite", 0);
            _material.SetInt("_ZTest", (int)UnityEngine.Rendering.CompareFunction.Always);
            _material.SetColor("_Color", new Color(1, 0, 0, 1));
        }

        var topLeftCorner = _globalCamera.ScreenPointToRay(new Vector3(0f, 0f));
        var topRightCorner = _globalCamera.ScreenPointToRay(new Vector3(Screen.width, 0f));
        var bottomLeftCorner = _globalCamera.ScreenPointToRay(new Vector3(0, Screen.height));
        var bottomRightCorner = _globalCamera.ScreenPointToRay(new Vector3(Screen.width, Screen.height));

        var hits = new RaycastHit[4];
        if (_floorCollider.Raycast(topLeftCorner, out hits[0], 150f))
            topLeftPosition = hits[0].point;
        
        if (_floorCollider.Raycast(topRightCorner, out hits[1], 150f))
            topRightPosition = hits[1].point;

        if (_floorCollider.Raycast(bottomLeftCorner, out hits[2], 150f))
            bottomLeftPosition = hits[2].point;

        if (_floorCollider.Raycast(bottomRightCorner, out hits[3], 150f))
            bottomRightPosition = hits[3].point;

        topLeftPosition = (_camera.WorldToViewportPoint(topLeftPosition).SwapYZ() * 0.015f) + new Vector3(0.5f, 2f, 0);
        topLeftPosition.y *= 0.25f;
        topRightPosition = (_camera.WorldToViewportPoint(topRightPosition).SwapYZ() * 0.015f) + new Vector3(0.5f, 2f, 0);
        topRightPosition.y *= 0.25f;
        bottomLeftPosition = (_camera.WorldToViewportPoint(bottomLeftPosition).SwapYZ() * 0.015f) + new Vector3(0.5f, 2f, 0);
        bottomLeftPosition.y *= 0.25f;
        bottomRightPosition = (_camera.WorldToViewportPoint(bottomRightPosition).SwapYZ() * 0.015f) + new Vector3(0.5f, 2f, 0);
        bottomRightPosition.y *= 0.25f;

        topLeftPosition.z = 0.1f;
        topRightPosition.z = 0.1f;
        bottomLeftPosition.z = 0.1f;
        bottomRightPosition.z = 0.1f;

        _points.RemoveAll(p => p.Transform == null);

        Graphics.Blit(BackTexture, _camera.targetTexture);
        _camera.Render();
        texture = _camera.targetTexture;
    }

    public void AddPoint(Transform tranform, Color color, float size)
    {
        _points.Add(new MiniMapPoint {Transform = tranform, Color = color, Size = size});
    }

    public void OnPostRender()
    {
        GL.PushMatrix();
        GL.LoadOrtho();

        GL.Color(Color.red);
        _material.SetColor("_Color", new Color(1, 0, 0, 1));

        _material.SetPass(0);

        GL.Begin(GL.LINES);
        GL.Color(Color.red);
        GL.Vertex(topLeftPosition);
        GL.Vertex(topRightPosition);
        GL.Vertex(topRightPosition);
        GL.Vertex(bottomRightPosition);
        GL.Vertex(bottomRightPosition);
        GL.Vertex(bottomLeftPosition);
        GL.Vertex(bottomLeftPosition);
        GL.Vertex(topLeftPosition);
        GL.End();

        foreach (var miniMapPoint in _points)
        {
            GL.Begin(GL.QUADS);

            GL.Color(miniMapPoint.Color);
            _material.SetColor("_Color", miniMapPoint.Color);

            _material.SetPass(0);

            var pos = miniMapPoint.Transform.position;
            pos = (_camera.WorldToViewportPoint(pos).SwapYZ() * 0.015f) + new Vector3(0.52f, 2.05f, 0);
            pos.y *= 0.25f;
            pos.z = 0.1f;

            var size = miniMapPoint.Size/400f;

            GL.Vertex3(pos.x - size, pos.y - size, 0);
            GL.Vertex3(pos.x + size, pos.y - size, 0);
            GL.Vertex3(pos.x + size, pos.y + size, 0);
            GL.Vertex3(pos.x - size, pos.y + size, 0);

            GL.End();
        }

        GL.PopMatrix();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var rt = gameObject.GetComponent<RectTransform>();
        var vpos = eventData.enterEventCamera.WorldToScreenPoint(rt.position).ToVector2();
        vpos.y = Screen.height - vpos.y;
        var mpos = eventData.position - vpos;
        mpos.y += 103f;

       /* var cam = _globalCamera.gameObject.GetComponent<GlobalMapCamera>();
        if (cam != null)
            cam.SetPosition(new Vector3(mpos.x, _globalCamera.transform.position.y, mpos.y));*/
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(MiniMap))]
    public class MiniMapEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var map = (MiniMap)target;

            map.BackTexture =
                EditorGUILayout.ObjectField("BackTexture", map.BackTexture, typeof (Texture2D), false) as Texture2D;

            if (GUILayout.Button("CreateTarget"))
            {
                map._camera.targetTexture = new RenderTexture(256, 256, 16, RenderTextureFormat.ARGB32);
                map._camera.targetTexture.Create();
            }
        } 
    }

    [MenuItem("GameObject/2D Object/MiniMap")]
    public static void Create()
    {
        var vpObj = new GameObject("MiniMap");

        var vp = vpObj.AddComponent<MiniMap>();
        var vpcamera = vpObj.AddComponent<Camera>();

        //vpcamera.targetTexture = new RenderTexture(128, 128, 16, RenderTextureFormat.ARGB32);
        //vpcamera.targetTexture.Create();
        vpcamera.clearFlags = CameraClearFlags.SolidColor;
        vpcamera.cullingMask = LayerMask.NameToLayer("MiniMap");

        if (Selection.activeObject != null)
        {
            var pt = Selection.activeObject as GameObject;
            if (pt != null)
                vpObj.transform.SetParent(pt.transform, false);
        }
    }
#endif

    public class MiniMapPoint
    {
        public Transform Transform;
        public float Size;
        public Color Color;
    }
}
