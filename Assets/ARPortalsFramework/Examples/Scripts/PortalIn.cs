using UnityEngine;
using System.Collections;

public class PortalIn : MonoBehaviour
{
	void OnTriggerEnter(Collider other)
    {
        if (!ARGameManager.Instance.Inside)
        {
            ARGameManager.Instance.Inside = true;
            ARGameManager.Instance.ToUnderWorld();
            return;
        }

    	if (ARGameManager.Instance.Outside)
		{
			ARGameManager.Instance.Inside = false;
			ARGameManager.Instance.Outside = false;
			ARGameManager.Instance.ToRealWorld();
            return;
		}
    }
}
