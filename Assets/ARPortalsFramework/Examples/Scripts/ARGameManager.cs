﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ARGameManager : GameManager<ARGameManager>
{
    /// <summary>
    /// The ARP lane prefab.
    /// </summary>
    public GameObject DebugPlanePrefab;
    public GameObject ARPortal;

    public ARPlaneAnchorGameObject ARInstantiatedPlane { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public UnityARAnchorManager AnchorManager;

    /// <summary>
    /// The portal controlled.
    /// </summary>
    public ARPortalsController PortalController;

    /// <summary>
    /// The camera manager.
    /// </summary>
    public UnityARCameraManager CameraManager;

    /// <summary>
    /// The ARC amera.
    /// </summary>
    public Camera MainCamera;

	/// <summary>
	/// The ARC amera.
	/// </summary>
	public Camera CutDepthCamera;

	/// <summary>
	/// The ARC amera.
	/// </summary>
	public Camera UnderworldCamera;

    /// <summary>
    /// The inside.
    /// </summary>
    public bool Inside;

    /// <summary>
    /// The outside.
    /// </summary>
    public bool Outside;

    /// <summary>
    /// 
    /// </summary>
    protected override void Initialize()
    {
        base.Initialize();

        AnchorManager = new UnityARAnchorManager();
        UnityARUtility.InitializePlanePrefab(DebugPlanePrefab);
        PortalController.Initialize();   
    }

    public override void Update()
    {
        base.Update();

        /*
         * if (PortalController == null || !Initialized)
         * return; 
         */

        if (ARInstantiatedPlane == null)
        {
            var arpags = AnchorManager.GetCurrentPlaneAnchors();
            if (arpags.Count > 0)
            {
                ARInstantiatedPlane = arpags[0];
                ARPortal.gameObject.SetActive(true);
                UnityARUtility.InitializePlanePrefab(null);

            }
        }
    }

    public void ToUnderWorld()
    {
		MainCamera.GetComponent<ARDepthFill>().TurnOff();
		MainCamera.depth = -1;
        MainCamera.clearFlags = CameraClearFlags.Nothing;

		CutDepthCamera.depth = -2;
		CutDepthCamera.clearFlags = CameraClearFlags.Nothing;

		UnderworldCamera.GetComponent<ARDepthFill>().TurnOn();
		UnderworldCamera.depth = -3;
        UnderworldCamera.clearFlags = CameraClearFlags.Color;			
	}

    public void ToRealWorld()
    {
		MainCamera.GetComponent<ARDepthFill>().TurnOn();
        /*MainCamera.depth = -3;
        MainCamera.clearFlags = CameraClearFlags.Depth;

        CutDepthCamera.depth = -2;
        CutDepthCamera.clearFlags = CameraClearFlags.Nothing;

        UnderworldCamera.GetComponent<ARDepthFill>().TurnOff();
        UnderworldCamera.depth = -1;
        UnderworldCamera.clearFlags = CameraClearFlags.Nothing;	*/
    }

    /// <summary>
    /// 
    /// </summary>
    void OnDestroy()
    {
        AnchorManager.Destroy();
    }

    
}
