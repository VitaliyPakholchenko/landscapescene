using UnityEngine;
using System.Collections;

public class PortalOut : MonoBehaviour
{
	void OnTriggerEnter(Collider other)
	{
		if (ARGameManager.Instance.Inside)
		{
            //ARGameManager.Instance.Inside = true;
			ARGameManager.Instance.Outside = true;
		}
	}
}
