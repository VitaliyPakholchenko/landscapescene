﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ARDepthFill : MonoBehaviour
{
    public Material m_ClearDepthMaterial;  
    private CommandBuffer m_VideoCommandBuffer;
    private bool _on;

	public void OnEnable()
	{
        if (m_VideoCommandBuffer == null)
        {
            m_VideoCommandBuffer = new CommandBuffer();
            m_VideoCommandBuffer.Blit(null, BuiltinRenderTextureType.CurrentActive, m_ClearDepthMaterial);
        }
        TurnOn();
	}

    public void OnDisable()
    {
        TurnOff();
    }

    public void TurnOn()
    {
        if (_on)
            return;
        
        _on = true;
		GetComponent<Camera>().AddCommandBuffer(CameraEvent.AfterForwardOpaque, m_VideoCommandBuffer); 
    }

    public void TurnOff()
    {
        if (!_on)
            return;
        _on = false;
       GetComponent<Camera>().RemoveCommandBuffer(CameraEvent.AfterForwardOpaque, m_VideoCommandBuffer); 
    }

    void OnDestroy()
    {
       GetComponent<Camera>().RemoveCommandBuffer(CameraEvent.AfterForwardOpaque, m_VideoCommandBuffer);
    }	
}
