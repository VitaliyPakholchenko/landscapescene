﻿using System;
using UnityEngine;
using UnityEngine.XR.iOS;

/// <summary>
/// Location mathod type.
/// </summary>
public enum PortalLocationMathodType
{
    Dynamics,
    Static
}

/// <summary>
/// Portal placement type.
/// </summary>
public enum PortalPlacementType
{
    Horizontal,
    Vertical
}

/// <summary>
/// ARP ortal controller.
/// </summary>
public class ARPortal : MonoBehaviour
{
    /// <summary>
    /// The location method.
    /// </summary>
    public PortalLocationMathodType LocationMethod;

    /// <summary>
    /// The placement.
    /// </summary>
    public PortalPlacementType Placement;

    /// <summary>
    /// The m anchor identifier.
    /// </summary>
	private string m_AnchorId;
    public string AnchorId { get { return m_AnchorId; } }

    void Start()
    {
        gameObject.transform.position = Camera.main.transform.position + gameObject.transform.position;

        UnityARSessionNativeInterface.ARUserAnchorUpdatedEvent += GameObjectAnchorUpdated;
        UnityARSessionNativeInterface.ARUserAnchorRemovedEvent += AnchorRemoved;

        this.m_AnchorId = UnityARSessionNativeInterface.GetARSessionNativeInterface().
            AddUserAnchorFromGameObject(gameObject).identifierStr;

        Console.WriteLine("UnityARUserAnchorComponent.Start(): " + m_AnchorId);
    }

    public void AnchorRemoved(ARUserAnchor anchor)
    {
        Console.WriteLine("Anchor Being Removed: " + anchor.identifier);
        Console.WriteLine("Our Anchor ID: " + m_AnchorId);
        if (anchor.identifier.Equals(m_AnchorId))
        {
            Console.WriteLine("AnchorComponent.AnchorRemoved: " + m_AnchorId);
            Destroy(this.gameObject);
        }
    }

    void OnDestroy()
    {
        UnityARSessionNativeInterface.ARUserAnchorUpdatedEvent -= GameObjectAnchorUpdated;
        UnityARSessionNativeInterface.ARUserAnchorRemovedEvent -= AnchorRemoved;
        Console.WriteLine("OnDestroy: " + this.m_AnchorId);
        UnityARSessionNativeInterface.GetARSessionNativeInterface().RemoveUserAnchor(this.m_AnchorId);
    }

    private void GameObjectAnchorUpdated(ARUserAnchor anchor)
    {
        gameObject.transform.rotation = anchor.transform.ExtractRotation();
        gameObject.transform.position = anchor.transform.ExtractPosition();
        Debug.Log("Update portal!!!!!!!");
        gameObject.transform.localScale = anchor.transform.ExtractScale();
    }
}


