﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ARPortalsController : MonoBehaviour 
{
	/// <summary>
	/// The portals.
	/// </summary>
	private List<ARPortal> _portals;

    /// <summary>
    /// Initialize this instance.
    /// </summary>
    public void Initialize()
    {
		var portalsObjects = GetComponentsInChildren<ARPortal>();

		_portals = new List<ARPortal>();
		_portals.AddRange(portalsObjects);
        _portals.ForEach(p => p.GetComponentInChildren<MeshRenderer>().enabled = false);
	}

    /// <summary>
    /// Inits the actor.
    /// </summary>
     ///<param name="pobject">Pobject.</param>
	public void InitActor(ARPlaneAnchorGameObject pobject)
    {
        var portal = _portals.FirstOrDefault();
        if (portal != null)
        {
            portal.GetComponentInChildren<MeshRenderer>().enabled=true;
            portal.gameObject.name = pobject.planeAnchor.identifier;
            Destroy(pobject.gameObject);
            pobject.gameObject = portal.gameObject;
            UnityARUtility.UpdatePlaneWithAnchorTransform(pobject.gameObject, pobject.planeAnchor);
        }
    }	

    /// <summary>
    /// Update this instance.
    /// </summary>
    public void Update()
    {
        if (_portals != null && _portals.Count > 0)
            Debug.Log(_portals[0].isActiveAndEnabled);
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(ARPortalsController))]
    public class ARPortalsControllerEditor : Editor
    {
        public static void CreateARPortal()
        {}


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            var gameManager = (ARPortalsController)target;
            if (GUILayout.Button("Create Portal"))
            {
                
            }

            if (GUILayout.Button("Initialize portals(Debug)"))
            {               
            }
        }
    }

#endif
}
