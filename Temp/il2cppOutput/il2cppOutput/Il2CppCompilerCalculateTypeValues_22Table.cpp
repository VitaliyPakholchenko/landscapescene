﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// System.Collections.Generic.List`1<PairStringInt>
struct List_1_t3590122670;
// System.Func`2<PairStringInt,System.Int32>
struct Func_2_t4016156815;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// UnityEngine.Material
struct Material_t193706927;
// VoxelTemplate[]
struct VoxelTemplateU5BU5D_t2866079843;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct List_1_t2501136733;
// System.Func`3<VoxelTemplate,System.Int32,System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>
struct Func_3_t4100446949;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>,System.Int32>
struct Func_2_t1901072402;
// System.Func`3<System.String,System.String,System.String>
struct Func_3_t1214888473;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// ParticlesAutoDestruct
struct ParticlesAutoDestruct_t857371774;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<PairIntInt>
struct List_1_t2683532254;
// System.Void
struct Void_t1841601450;
// System.Collections.Generic.Dictionary`2<DirectionTypes,Vector3i>
struct Dictionary_2_t2807614054;
// System.Collections.Generic.Dictionary`2<DirectionTypes2D,Vector2i>
struct Dictionary_2_t651551717;
// System.Char[]
struct CharU5BU5D_t1328083999;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// WeightedOptions
struct WeightedOptions_t481347131;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t1558332529;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// VoxelStructure[0...,0...,0...]
struct VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636;
// VoxelPallette
struct VoxelPallette_t4284593999;
// VoxelSpace
struct VoxelSpace_t930815766;
// System.Int32[0...,0...,0...]
struct Int32U5B0___U2C0___U2C0___U5D_t3030399643;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// VoxelTemplate
struct VoxelTemplate_t594004422;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// System.Action`1<TimeBombMBH>
struct Action_1_t1078897012;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CCONTAINSKEYU3EC__ANONSTOREY0_T153965376_H
#define U3CCONTAINSKEYU3EC__ANONSTOREY0_T153965376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringIntDictionary/<ContainsKey>c__AnonStorey0
struct  U3CContainsKeyU3Ec__AnonStorey0_t153965376  : public RuntimeObject
{
public:
	// System.String PairStringIntDictionary/<ContainsKey>c__AnonStorey0::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CContainsKeyU3Ec__AnonStorey0_t153965376, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTAINSKEYU3EC__ANONSTOREY0_T153965376_H
#ifndef U3CU3EC__ANONSTOREY1_T402337725_H
#define U3CU3EC__ANONSTOREY1_T402337725_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringIntDictionary/<>c__AnonStorey1
struct  U3CU3Ec__AnonStorey1_t402337725  : public RuntimeObject
{
public:
	// System.String PairStringIntDictionary/<>c__AnonStorey1::index
	String_t* ___index_0;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__AnonStorey1_t402337725, ___index_0)); }
	inline String_t* get_index_0() const { return ___index_0; }
	inline String_t** get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(String_t* value)
	{
		___index_0 = value;
		Il2CppCodeGenWriteBarrier((&___index_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ANONSTOREY1_T402337725_H
#ifndef PAIRSTRINGINTDICTIONARY_T1323851614_H
#define PAIRSTRINGINTDICTIONARY_T1323851614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringIntDictionary
struct  PairStringIntDictionary_t1323851614  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<PairStringInt> PairStringIntDictionary::Pairs
	List_1_t3590122670 * ___Pairs_0;

public:
	inline static int32_t get_offset_of_Pairs_0() { return static_cast<int32_t>(offsetof(PairStringIntDictionary_t1323851614, ___Pairs_0)); }
	inline List_1_t3590122670 * get_Pairs_0() const { return ___Pairs_0; }
	inline List_1_t3590122670 ** get_address_of_Pairs_0() { return &___Pairs_0; }
	inline void set_Pairs_0(List_1_t3590122670 * value)
	{
		___Pairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___Pairs_0), value);
	}
};

struct PairStringIntDictionary_t1323851614_StaticFields
{
public:
	// System.Func`2<PairStringInt,System.Int32> PairStringIntDictionary::<>f__am$cache0
	Func_2_t4016156815 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(PairStringIntDictionary_t1323851614_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t4016156815 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t4016156815 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t4016156815 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRSTRINGINTDICTIONARY_T1323851614_H
#ifndef U3CGETVALUESU3EC__ANONSTOREY1_T1165659093_H
#define U3CGETVALUESU3EC__ANONSTOREY1_T1165659093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairIntIntDictionary/<GetValues>c__AnonStorey1
struct  U3CGetValuesU3Ec__AnonStorey1_t1165659093  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> PairIntIntDictionary/<GetValues>c__AnonStorey1::result
	List_1_t1440998580 * ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CGetValuesU3Ec__AnonStorey1_t1165659093, ___result_0)); }
	inline List_1_t1440998580 * get_result_0() const { return ___result_0; }
	inline List_1_t1440998580 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(List_1_t1440998580 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETVALUESU3EC__ANONSTOREY1_T1165659093_H
#ifndef U3CGETKEYSU3EC__ANONSTOREY2_T2671560958_H
#define U3CGETKEYSU3EC__ANONSTOREY2_T2671560958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairIntIntDictionary/<GetKeys>c__AnonStorey2
struct  U3CGetKeysU3Ec__AnonStorey2_t2671560958  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> PairIntIntDictionary/<GetKeys>c__AnonStorey2::result
	List_1_t1440998580 * ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CGetKeysU3Ec__AnonStorey2_t2671560958, ___result_0)); }
	inline List_1_t1440998580 * get_result_0() const { return ___result_0; }
	inline List_1_t1440998580 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(List_1_t1440998580 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETKEYSU3EC__ANONSTOREY2_T2671560958_H
#ifndef U3CREMOVEU3EC__ANONSTOREY5_T4162925351_H
#define U3CREMOVEU3EC__ANONSTOREY5_T4162925351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringIntDictionary/<Remove>c__AnonStorey5
struct  U3CRemoveU3Ec__AnonStorey5_t4162925351  : public RuntimeObject
{
public:
	// System.String PairStringIntDictionary/<Remove>c__AnonStorey5::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CRemoveU3Ec__AnonStorey5_t4162925351, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEU3EC__ANONSTOREY5_T4162925351_H
#ifndef VOXELPALLETTE_T4284593999_H
#define VOXELPALLETTE_T4284593999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelPallette
struct  VoxelPallette_t4284593999  : public RuntimeObject
{
public:
	// UnityEngine.Material VoxelPallette::AtlasMaterial
	Material_t193706927 * ___AtlasMaterial_0;
	// VoxelTemplate[] VoxelPallette::voxelTemplates
	VoxelTemplateU5BU5D_t2866079843* ___voxelTemplates_1;
	// System.Collections.Generic.List`1<System.String> VoxelPallette::voxelNames
	List_1_t1398341365 * ___voxelNames_2;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>> VoxelPallette::_lookup
	List_1_t2501136733 * ____lookup_3;

public:
	inline static int32_t get_offset_of_AtlasMaterial_0() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999, ___AtlasMaterial_0)); }
	inline Material_t193706927 * get_AtlasMaterial_0() const { return ___AtlasMaterial_0; }
	inline Material_t193706927 ** get_address_of_AtlasMaterial_0() { return &___AtlasMaterial_0; }
	inline void set_AtlasMaterial_0(Material_t193706927 * value)
	{
		___AtlasMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___AtlasMaterial_0), value);
	}

	inline static int32_t get_offset_of_voxelTemplates_1() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999, ___voxelTemplates_1)); }
	inline VoxelTemplateU5BU5D_t2866079843* get_voxelTemplates_1() const { return ___voxelTemplates_1; }
	inline VoxelTemplateU5BU5D_t2866079843** get_address_of_voxelTemplates_1() { return &___voxelTemplates_1; }
	inline void set_voxelTemplates_1(VoxelTemplateU5BU5D_t2866079843* value)
	{
		___voxelTemplates_1 = value;
		Il2CppCodeGenWriteBarrier((&___voxelTemplates_1), value);
	}

	inline static int32_t get_offset_of_voxelNames_2() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999, ___voxelNames_2)); }
	inline List_1_t1398341365 * get_voxelNames_2() const { return ___voxelNames_2; }
	inline List_1_t1398341365 ** get_address_of_voxelNames_2() { return &___voxelNames_2; }
	inline void set_voxelNames_2(List_1_t1398341365 * value)
	{
		___voxelNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___voxelNames_2), value);
	}

	inline static int32_t get_offset_of__lookup_3() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999, ____lookup_3)); }
	inline List_1_t2501136733 * get__lookup_3() const { return ____lookup_3; }
	inline List_1_t2501136733 ** get_address_of__lookup_3() { return &____lookup_3; }
	inline void set__lookup_3(List_1_t2501136733 * value)
	{
		____lookup_3 = value;
		Il2CppCodeGenWriteBarrier((&____lookup_3), value);
	}
};

struct VoxelPallette_t4284593999_StaticFields
{
public:
	// System.Func`3<VoxelTemplate,System.Int32,System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>> VoxelPallette::<>f__am$cache0
	Func_3_t4100446949 * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>,System.Int32> VoxelPallette::<>f__am$cache1
	Func_2_t1901072402 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_3_t4100446949 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_3_t4100446949 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_3_t4100446949 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_t1901072402 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_t1901072402 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_t1901072402 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELPALLETTE_T4284593999_H
#ifndef U3CGETKEYSU3EC__ANONSTOREY4_T2027276052_H
#define U3CGETKEYSU3EC__ANONSTOREY4_T2027276052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringIntDictionary/<GetKeys>c__AnonStorey4
struct  U3CGetKeysU3Ec__AnonStorey4_t2027276052  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> PairStringIntDictionary/<GetKeys>c__AnonStorey4::result
	List_1_t1398341365 * ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CGetKeysU3Ec__AnonStorey4_t2027276052, ___result_0)); }
	inline List_1_t1398341365 * get_result_0() const { return ___result_0; }
	inline List_1_t1398341365 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(List_1_t1398341365 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETKEYSU3EC__ANONSTOREY4_T2027276052_H
#ifndef U3CSETU3EC__ANONSTOREY2_T3921341386_H
#define U3CSETU3EC__ANONSTOREY2_T3921341386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringIntDictionary/<Set>c__AnonStorey2
struct  U3CSetU3Ec__AnonStorey2_t3921341386  : public RuntimeObject
{
public:
	// System.String PairStringIntDictionary/<Set>c__AnonStorey2::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CSetU3Ec__AnonStorey2_t3921341386, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETU3EC__ANONSTOREY2_T3921341386_H
#ifndef U3CGETVALUESU3EC__ANONSTOREY3_T2450800505_H
#define U3CGETVALUESU3EC__ANONSTOREY3_T2450800505_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringIntDictionary/<GetValues>c__AnonStorey3
struct  U3CGetValuesU3Ec__AnonStorey3_t2450800505  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Int32> PairStringIntDictionary/<GetValues>c__AnonStorey3::result
	List_1_t1440998580 * ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CGetValuesU3Ec__AnonStorey3_t2450800505, ___result_0)); }
	inline List_1_t1440998580 * get_result_0() const { return ___result_0; }
	inline List_1_t1440998580 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(List_1_t1440998580 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETVALUESU3EC__ANONSTOREY3_T2450800505_H
#ifndef RANDOMHELPER_T2807717491_H
#define RANDOMHELPER_T2807717491_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RandomHelper
struct  RandomHelper_t2807717491  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANDOMHELPER_T2807717491_H
#ifndef STRINGHELPER_T3272835469_H
#define STRINGHELPER_T3272835469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StringHelper
struct  StringHelper_t3272835469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGHELPER_T3272835469_H
#ifndef PROFILES_T3461944550_H
#define PROFILES_T3461944550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Profiles
struct  Profiles_t3461944550  : public RuntimeObject
{
public:

public:
};

struct Profiles_t3461944550_StaticFields
{
public:
	// System.String Profiles::<UserName>k__BackingField
	String_t* ___U3CUserNameU3Ek__BackingField_0;
	// System.Func`3<System.String,System.String,System.String> Profiles::<>f__am$cache0
	Func_3_t1214888473 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CUserNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Profiles_t3461944550_StaticFields, ___U3CUserNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CUserNameU3Ek__BackingField_0() const { return ___U3CUserNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CUserNameU3Ek__BackingField_0() { return &___U3CUserNameU3Ek__BackingField_0; }
	inline void set_U3CUserNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CUserNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(Profiles_t3461944550_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_3_t1214888473 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_3_t1214888473 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_3_t1214888473 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROFILES_T3461944550_H
#ifndef U3CREMOVEU3EC__ANONSTOREY3_T2381651523_H
#define U3CREMOVEU3EC__ANONSTOREY3_T2381651523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairIntIntDictionary/<Remove>c__AnonStorey3
struct  U3CRemoveU3Ec__AnonStorey3_t2381651523  : public RuntimeObject
{
public:
	// System.Int32 PairIntIntDictionary/<Remove>c__AnonStorey3::key
	int32_t ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CRemoveU3Ec__AnonStorey3_t2381651523, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVEU3EC__ANONSTOREY3_T2381651523_H
#ifndef U3CCHECKIFALIVEU3EC__ITERATOR0_T816385830_H
#define U3CCHECKIFALIVEU3EC__ITERATOR0_T816385830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlesAutoDestruct/<CheckIfAlive>c__Iterator0
struct  U3CCheckIfAliveU3Ec__Iterator0_t816385830  : public RuntimeObject
{
public:
	// UnityEngine.ParticleSystem ParticlesAutoDestruct/<CheckIfAlive>c__Iterator0::<ps>__0
	ParticleSystem_t3394631041 * ___U3CpsU3E__0_0;
	// ParticlesAutoDestruct ParticlesAutoDestruct/<CheckIfAlive>c__Iterator0::$this
	ParticlesAutoDestruct_t857371774 * ___U24this_1;
	// System.Object ParticlesAutoDestruct/<CheckIfAlive>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean ParticlesAutoDestruct/<CheckIfAlive>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 ParticlesAutoDestruct/<CheckIfAlive>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CpsU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator0_t816385830, ___U3CpsU3E__0_0)); }
	inline ParticleSystem_t3394631041 * get_U3CpsU3E__0_0() const { return ___U3CpsU3E__0_0; }
	inline ParticleSystem_t3394631041 ** get_address_of_U3CpsU3E__0_0() { return &___U3CpsU3E__0_0; }
	inline void set_U3CpsU3E__0_0(ParticleSystem_t3394631041 * value)
	{
		___U3CpsU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpsU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator0_t816385830, ___U24this_1)); }
	inline ParticlesAutoDestruct_t857371774 * get_U24this_1() const { return ___U24this_1; }
	inline ParticlesAutoDestruct_t857371774 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ParticlesAutoDestruct_t857371774 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator0_t816385830, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator0_t816385830, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CCheckIfAliveU3Ec__Iterator0_t816385830, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKIFALIVEU3EC__ITERATOR0_T816385830_H
#ifndef REGION2D_T3281579718_H
#define REGION2D_T3281579718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Region2D
struct  Region2D_t3281579718  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Region2D::Points
	List_1_t1612828711 * ___Points_0;
	// System.Boolean Region2D::_cicled
	bool ____cicled_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> Region2D::TransformedPoints
	List_1_t1612828711 * ___TransformedPoints_2;

public:
	inline static int32_t get_offset_of_Points_0() { return static_cast<int32_t>(offsetof(Region2D_t3281579718, ___Points_0)); }
	inline List_1_t1612828711 * get_Points_0() const { return ___Points_0; }
	inline List_1_t1612828711 ** get_address_of_Points_0() { return &___Points_0; }
	inline void set_Points_0(List_1_t1612828711 * value)
	{
		___Points_0 = value;
		Il2CppCodeGenWriteBarrier((&___Points_0), value);
	}

	inline static int32_t get_offset_of__cicled_1() { return static_cast<int32_t>(offsetof(Region2D_t3281579718, ____cicled_1)); }
	inline bool get__cicled_1() const { return ____cicled_1; }
	inline bool* get_address_of__cicled_1() { return &____cicled_1; }
	inline void set__cicled_1(bool value)
	{
		____cicled_1 = value;
	}

	inline static int32_t get_offset_of_TransformedPoints_2() { return static_cast<int32_t>(offsetof(Region2D_t3281579718, ___TransformedPoints_2)); }
	inline List_1_t1612828711 * get_TransformedPoints_2() const { return ___TransformedPoints_2; }
	inline List_1_t1612828711 ** get_address_of_TransformedPoints_2() { return &___TransformedPoints_2; }
	inline void set_TransformedPoints_2(List_1_t1612828711 * value)
	{
		___TransformedPoints_2 = value;
		Il2CppCodeGenWriteBarrier((&___TransformedPoints_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGION2D_T3281579718_H
#ifndef U3CCONTAINSKEYU3EC__ANONSTOREY0_T2167278752_H
#define U3CCONTAINSKEYU3EC__ANONSTOREY0_T2167278752_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairIntIntDictionary/<ContainsKey>c__AnonStorey0
struct  U3CContainsKeyU3Ec__AnonStorey0_t2167278752  : public RuntimeObject
{
public:
	// System.Int32 PairIntIntDictionary/<ContainsKey>c__AnonStorey0::key
	int32_t ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CContainsKeyU3Ec__AnonStorey0_t2167278752, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONTAINSKEYU3EC__ANONSTOREY0_T2167278752_H
#ifndef PAIRINTINTDICTIONARY_T2858279954_H
#define PAIRINTINTDICTIONARY_T2858279954_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairIntIntDictionary
struct  PairIntIntDictionary_t2858279954  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<PairIntInt> PairIntIntDictionary::Pairs
	List_1_t2683532254 * ___Pairs_0;

public:
	inline static int32_t get_offset_of_Pairs_0() { return static_cast<int32_t>(offsetof(PairIntIntDictionary_t2858279954, ___Pairs_0)); }
	inline List_1_t2683532254 * get_Pairs_0() const { return ___Pairs_0; }
	inline List_1_t2683532254 ** get_address_of_Pairs_0() { return &___Pairs_0; }
	inline void set_Pairs_0(List_1_t2683532254 * value)
	{
		___Pairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___Pairs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRINTINTDICTIONARY_T2858279954_H
#ifndef SCRIPTABLEOBJECTUTILITY_T2260201772_H
#define SCRIPTABLEOBJECTUTILITY_T2260201772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScriptableObjectUtility
struct  ScriptableObjectUtility_t2260201772  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTUTILITY_T2260201772_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR3I_T3488636705_H
#define VECTOR3I_T3488636705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vector3i
struct  Vector3i_t3488636705 
{
public:
	// System.Int32 Vector3i::x
	int32_t ___x_0;
	// System.Int32 Vector3i::y
	int32_t ___y_1;
	// System.Int32 Vector3i::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};

struct Vector3i_t3488636705_StaticFields
{
public:
	// Vector3i Vector3i::zero
	Vector3i_t3488636705  ___zero_3;
	// Vector3i Vector3i::one
	Vector3i_t3488636705  ___one_4;
	// Vector3i Vector3i::oneTileNorth
	Vector3i_t3488636705  ___oneTileNorth_5;
	// Vector3i Vector3i::oneTileEast
	Vector3i_t3488636705  ___oneTileEast_6;
	// Vector3i Vector3i::oneTileSouth
	Vector3i_t3488636705  ___oneTileSouth_7;
	// Vector3i Vector3i::oneTileWest
	Vector3i_t3488636705  ___oneTileWest_8;
	// System.Collections.Generic.Dictionary`2<DirectionTypes,Vector3i> Vector3i::Deltas
	Dictionary_2_t2807614054 * ___Deltas_9;

public:
	inline static int32_t get_offset_of_zero_3() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___zero_3)); }
	inline Vector3i_t3488636705  get_zero_3() const { return ___zero_3; }
	inline Vector3i_t3488636705 * get_address_of_zero_3() { return &___zero_3; }
	inline void set_zero_3(Vector3i_t3488636705  value)
	{
		___zero_3 = value;
	}

	inline static int32_t get_offset_of_one_4() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___one_4)); }
	inline Vector3i_t3488636705  get_one_4() const { return ___one_4; }
	inline Vector3i_t3488636705 * get_address_of_one_4() { return &___one_4; }
	inline void set_one_4(Vector3i_t3488636705  value)
	{
		___one_4 = value;
	}

	inline static int32_t get_offset_of_oneTileNorth_5() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___oneTileNorth_5)); }
	inline Vector3i_t3488636705  get_oneTileNorth_5() const { return ___oneTileNorth_5; }
	inline Vector3i_t3488636705 * get_address_of_oneTileNorth_5() { return &___oneTileNorth_5; }
	inline void set_oneTileNorth_5(Vector3i_t3488636705  value)
	{
		___oneTileNorth_5 = value;
	}

	inline static int32_t get_offset_of_oneTileEast_6() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___oneTileEast_6)); }
	inline Vector3i_t3488636705  get_oneTileEast_6() const { return ___oneTileEast_6; }
	inline Vector3i_t3488636705 * get_address_of_oneTileEast_6() { return &___oneTileEast_6; }
	inline void set_oneTileEast_6(Vector3i_t3488636705  value)
	{
		___oneTileEast_6 = value;
	}

	inline static int32_t get_offset_of_oneTileSouth_7() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___oneTileSouth_7)); }
	inline Vector3i_t3488636705  get_oneTileSouth_7() const { return ___oneTileSouth_7; }
	inline Vector3i_t3488636705 * get_address_of_oneTileSouth_7() { return &___oneTileSouth_7; }
	inline void set_oneTileSouth_7(Vector3i_t3488636705  value)
	{
		___oneTileSouth_7 = value;
	}

	inline static int32_t get_offset_of_oneTileWest_8() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___oneTileWest_8)); }
	inline Vector3i_t3488636705  get_oneTileWest_8() const { return ___oneTileWest_8; }
	inline Vector3i_t3488636705 * get_address_of_oneTileWest_8() { return &___oneTileWest_8; }
	inline void set_oneTileWest_8(Vector3i_t3488636705  value)
	{
		___oneTileWest_8 = value;
	}

	inline static int32_t get_offset_of_Deltas_9() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___Deltas_9)); }
	inline Dictionary_2_t2807614054 * get_Deltas_9() const { return ___Deltas_9; }
	inline Dictionary_2_t2807614054 ** get_address_of_Deltas_9() { return &___Deltas_9; }
	inline void set_Deltas_9(Dictionary_2_t2807614054 * value)
	{
		___Deltas_9 = value;
		Il2CppCodeGenWriteBarrier((&___Deltas_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3I_T3488636705_H
#ifndef VECTOR2I_T3488636706_H
#define VECTOR2I_T3488636706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vector2i
struct  Vector2i_t3488636706 
{
public:
	// System.Int32 Vector2i::X
	int32_t ___X_0;
	// System.Int32 Vector2i::Y
	int32_t ___Y_1;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(Vector2i_t3488636706, ___X_0)); }
	inline int32_t get_X_0() const { return ___X_0; }
	inline int32_t* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(int32_t value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(Vector2i_t3488636706, ___Y_1)); }
	inline int32_t get_Y_1() const { return ___Y_1; }
	inline int32_t* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(int32_t value)
	{
		___Y_1 = value;
	}
};

struct Vector2i_t3488636706_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<DirectionTypes2D,Vector2i> Vector2i::_deltas
	Dictionary_2_t651551717 * ____deltas_2;

public:
	inline static int32_t get_offset_of__deltas_2() { return static_cast<int32_t>(offsetof(Vector2i_t3488636706_StaticFields, ____deltas_2)); }
	inline Dictionary_2_t651551717 * get__deltas_2() const { return ____deltas_2; }
	inline Dictionary_2_t651551717 ** get_address_of__deltas_2() { return &____deltas_2; }
	inline void set__deltas_2(Dictionary_2_t651551717 * value)
	{
		____deltas_2 = value;
		Il2CppCodeGenWriteBarrier((&____deltas_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2I_T3488636706_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef U24ARRAYTYPEU3D24_T762068664_H
#define U24ARRAYTYPEU3D24_T762068664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t762068664 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t762068664__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T762068664_H
#ifndef COLOR32_T874517518_H
#define COLOR32_T874517518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t874517518 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T874517518_H
#ifndef PAIRSTRINGINT_T4221001538_H
#define PAIRSTRINGINT_T4221001538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringInt
struct  PairStringInt_t4221001538 
{
public:
	// System.String PairStringInt::Key
	String_t* ___Key_0;
	// System.Int32 PairStringInt::Value
	int32_t ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairStringInt_t4221001538, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairStringInt_t4221001538, ___Value_1)); }
	inline int32_t get_Value_1() const { return ___Value_1; }
	inline int32_t* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(int32_t value)
	{
		___Value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PairStringInt
struct PairStringInt_t4221001538_marshaled_pinvoke
{
	char* ___Key_0;
	int32_t ___Value_1;
};
// Native definition for COM marshalling of PairStringInt
struct PairStringInt_t4221001538_marshaled_com
{
	Il2CppChar* ___Key_0;
	int32_t ___Value_1;
};
#endif // PAIRSTRINGINT_T4221001538_H
#ifndef PAIRINTINT_T3314411122_H
#define PAIRINTINT_T3314411122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairIntInt
struct  PairIntInt_t3314411122 
{
public:
	// System.Int32 PairIntInt::Key
	int32_t ___Key_0;
	// System.Int32 PairIntInt::Value
	int32_t ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairIntInt_t3314411122, ___Key_0)); }
	inline int32_t get_Key_0() const { return ___Key_0; }
	inline int32_t* get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(int32_t value)
	{
		___Key_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairIntInt_t3314411122, ___Value_1)); }
	inline int32_t get_Value_1() const { return ___Value_1; }
	inline int32_t* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(int32_t value)
	{
		___Value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRINTINT_T3314411122_H
#ifndef PAIRINTFLOAT_T3853530191_H
#define PAIRINTFLOAT_T3853530191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairIntFloat
struct  PairIntFloat_t3853530191 
{
public:
	// System.Int32 PairIntFloat::Key
	int32_t ___Key_0;
	// System.Single PairIntFloat::Value
	float ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairIntFloat_t3853530191, ___Key_0)); }
	inline int32_t get_Key_0() const { return ___Key_0; }
	inline int32_t* get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(int32_t value)
	{
		___Key_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairIntFloat_t3853530191, ___Value_1)); }
	inline float get_Value_1() const { return ___Value_1; }
	inline float* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(float value)
	{
		___Value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRINTFLOAT_T3853530191_H
#ifndef PAIRSTRINGAUDIO_T4289727603_H
#define PAIRSTRINGAUDIO_T4289727603_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringAudio
struct  PairStringAudio_t4289727603 
{
public:
	// System.String PairStringAudio::Key
	String_t* ___Key_0;
	// UnityEngine.AudioClip PairStringAudio::Value
	AudioClip_t1932558630 * ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairStringAudio_t4289727603, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairStringAudio_t4289727603, ___Value_1)); }
	inline AudioClip_t1932558630 * get_Value_1() const { return ___Value_1; }
	inline AudioClip_t1932558630 ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(AudioClip_t1932558630 * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PairStringAudio
struct PairStringAudio_t4289727603_marshaled_pinvoke
{
	char* ___Key_0;
	AudioClip_t1932558630 * ___Value_1;
};
// Native definition for COM marshalling of PairStringAudio
struct PairStringAudio_t4289727603_marshaled_com
{
	Il2CppChar* ___Key_0;
	AudioClip_t1932558630 * ___Value_1;
};
#endif // PAIRSTRINGAUDIO_T4289727603_H
#ifndef PAIRINTLISTINT_T1008085406_H
#define PAIRINTLISTINT_T1008085406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairIntListInt
struct  PairIntListInt_t1008085406 
{
public:
	// System.Int32 PairIntListInt::Key
	int32_t ___Key_0;
	// System.Collections.Generic.List`1<System.Int32> PairIntListInt::Value
	List_1_t1440998580 * ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairIntListInt_t1008085406, ___Key_0)); }
	inline int32_t get_Key_0() const { return ___Key_0; }
	inline int32_t* get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(int32_t value)
	{
		___Key_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairIntListInt_t1008085406, ___Value_1)); }
	inline List_1_t1440998580 * get_Value_1() const { return ___Value_1; }
	inline List_1_t1440998580 ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(List_1_t1440998580 * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PairIntListInt
struct PairIntListInt_t1008085406_marshaled_pinvoke
{
	int32_t ___Key_0;
	List_1_t1440998580 * ___Value_1;
};
// Native definition for COM marshalling of PairIntListInt
struct PairIntListInt_t1008085406_marshaled_com
{
	int32_t ___Key_0;
	List_1_t1440998580 * ___Value_1;
};
#endif // PAIRINTLISTINT_T1008085406_H
#ifndef PAIRINTWEIGHTEDOPTIONS_T126247562_H
#define PAIRINTWEIGHTEDOPTIONS_T126247562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairIntWeightedOptions
struct  PairIntWeightedOptions_t126247562 
{
public:
	// System.Int32 PairIntWeightedOptions::Key
	int32_t ___Key_0;
	// WeightedOptions PairIntWeightedOptions::Value
	WeightedOptions_t481347131 * ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairIntWeightedOptions_t126247562, ___Key_0)); }
	inline int32_t get_Key_0() const { return ___Key_0; }
	inline int32_t* get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(int32_t value)
	{
		___Key_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairIntWeightedOptions_t126247562, ___Value_1)); }
	inline WeightedOptions_t481347131 * get_Value_1() const { return ___Value_1; }
	inline WeightedOptions_t481347131 ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(WeightedOptions_t481347131 * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PairIntWeightedOptions
struct PairIntWeightedOptions_t126247562_marshaled_pinvoke
{
	int32_t ___Key_0;
	WeightedOptions_t481347131 * ___Value_1;
};
// Native definition for COM marshalling of PairIntWeightedOptions
struct PairIntWeightedOptions_t126247562_marshaled_com
{
	int32_t ___Key_0;
	WeightedOptions_t481347131 * ___Value_1;
};
#endif // PAIRINTWEIGHTEDOPTIONS_T126247562_H
#ifndef PAIRSTRINGBOOL_T2722701971_H
#define PAIRSTRINGBOOL_T2722701971_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringBool
struct  PairStringBool_t2722701971 
{
public:
	// System.String PairStringBool::Key
	String_t* ___Key_0;
	// System.Boolean PairStringBool::Value
	bool ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairStringBool_t2722701971, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairStringBool_t2722701971, ___Value_1)); }
	inline bool get_Value_1() const { return ___Value_1; }
	inline bool* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(bool value)
	{
		___Value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PairStringBool
struct PairStringBool_t2722701971_marshaled_pinvoke
{
	char* ___Key_0;
	int32_t ___Value_1;
};
// Native definition for COM marshalling of PairStringBool
struct PairStringBool_t2722701971_marshaled_com
{
	Il2CppChar* ___Key_0;
	int32_t ___Value_1;
};
#endif // PAIRSTRINGBOOL_T2722701971_H
#ifndef PAIRSTRINGGAMEOBJECT_T2091526800_H
#define PAIRSTRINGGAMEOBJECT_T2091526800_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringGameObject
struct  PairStringGameObject_t2091526800 
{
public:
	// System.String PairStringGameObject::Key
	String_t* ___Key_0;
	// UnityEngine.GameObject PairStringGameObject::Value
	GameObject_t1756533147 * ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairStringGameObject_t2091526800, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairStringGameObject_t2091526800, ___Value_1)); }
	inline GameObject_t1756533147 * get_Value_1() const { return ___Value_1; }
	inline GameObject_t1756533147 ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(GameObject_t1756533147 * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PairStringGameObject
struct PairStringGameObject_t2091526800_marshaled_pinvoke
{
	char* ___Key_0;
	GameObject_t1756533147 * ___Value_1;
};
// Native definition for COM marshalling of PairStringGameObject
struct PairStringGameObject_t2091526800_marshaled_com
{
	Il2CppChar* ___Key_0;
	GameObject_t1756533147 * ___Value_1;
};
#endif // PAIRSTRINGGAMEOBJECT_T2091526800_H
#ifndef RANGE_T624945109_H
#define RANGE_T624945109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Range
struct  Range_t624945109 
{
public:
	// System.Single Range::Minimum
	float ___Minimum_0;
	// System.Single Range::Maximum
	float ___Maximum_1;

public:
	inline static int32_t get_offset_of_Minimum_0() { return static_cast<int32_t>(offsetof(Range_t624945109, ___Minimum_0)); }
	inline float get_Minimum_0() const { return ___Minimum_0; }
	inline float* get_address_of_Minimum_0() { return &___Minimum_0; }
	inline void set_Minimum_0(float value)
	{
		___Minimum_0 = value;
	}

	inline static int32_t get_offset_of_Maximum_1() { return static_cast<int32_t>(offsetof(Range_t624945109, ___Maximum_1)); }
	inline float get_Maximum_1() const { return ___Maximum_1; }
	inline float* get_address_of_Maximum_1() { return &___Maximum_1; }
	inline void set_Maximum_1(float value)
	{
		___Maximum_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGE_T624945109_H
#ifndef PAIRSTRINGSTRING_T2726841396_H
#define PAIRSTRINGSTRING_T2726841396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringString
struct  PairStringString_t2726841396 
{
public:
	// System.String PairStringString::Key
	String_t* ___Key_0;
	// System.String PairStringString::Value
	String_t* ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairStringString_t2726841396, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairStringString_t2726841396, ___Value_1)); }
	inline String_t* get_Value_1() const { return ___Value_1; }
	inline String_t** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(String_t* value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PairStringString
struct PairStringString_t2726841396_marshaled_pinvoke
{
	char* ___Key_0;
	char* ___Value_1;
};
// Native definition for COM marshalling of PairStringString
struct PairStringString_t2726841396_marshaled_com
{
	Il2CppChar* ___Key_0;
	Il2CppChar* ___Value_1;
};
#endif // PAIRSTRINGSTRING_T2726841396_H
#ifndef ACTION_T4052865656_H
#define ACTION_T4052865656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelStructure/Action
struct  Action_t4052865656 
{
public:
	// System.Int32 VoxelStructure/Action::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Action_t4052865656, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T4052865656_H
#ifndef VOXELFACE_T2735811281_H
#define VOXELFACE_T2735811281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelStructure/VoxelFace
struct  VoxelFace_t2735811281 
{
public:
	// System.Int32 VoxelStructure/VoxelFace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VoxelFace_t2735811281, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELFACE_T2735811281_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305143  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-8E7629AD5AF686202B8CB7C014505C432FFE31E6
	U24ArrayTypeU3D24_t762068664  ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0;

public:
	inline static int32_t get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0)); }
	inline U24ArrayTypeU3D24_t762068664  get_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() const { return ___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline U24ArrayTypeU3D24_t762068664 * get_address_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0() { return &___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0; }
	inline void set_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(U24ArrayTypeU3D24_t762068664  value)
	{
		___U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1486305143_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef PAIRVECTOR2INT_T2786601292_H
#define PAIRVECTOR2INT_T2786601292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairVector2Int
struct  PairVector2Int_t2786601292 
{
public:
	// UnityEngine.Vector2 PairVector2Int::Key
	Vector2_t2243707579  ___Key_0;
	// System.Int32 PairVector2Int::Value
	int32_t ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairVector2Int_t2786601292, ___Key_0)); }
	inline Vector2_t2243707579  get_Key_0() const { return ___Key_0; }
	inline Vector2_t2243707579 * get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(Vector2_t2243707579  value)
	{
		___Key_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairVector2Int_t2786601292, ___Value_1)); }
	inline int32_t get_Value_1() const { return ___Value_1; }
	inline int32_t* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(int32_t value)
	{
		___Value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRVECTOR2INT_T2786601292_H
#ifndef DIRECTIONTYPES2D_T4012228328_H
#define DIRECTIONTYPES2D_T4012228328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DirectionTypes2D
struct  DirectionTypes2D_t4012228328 
{
public:
	// System.Int32 DirectionTypes2D::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DirectionTypes2D_t4012228328, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONTYPES2D_T4012228328_H
#ifndef DIRECTIONTYPES_T1731595534_H
#define DIRECTIONTYPES_T1731595534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DirectionTypes
struct  DirectionTypes_t1731595534 
{
public:
	// System.Int32 DirectionTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DirectionTypes_t1731595534, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIRECTIONTYPES_T1731595534_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef VOXELTEMPLATE_T594004422_H
#define VOXELTEMPLATE_T594004422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelTemplate
struct  VoxelTemplate_t594004422  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 VoxelTemplate::DisplayOrder
	int32_t ___DisplayOrder_2;
	// UnityEngine.Color32 VoxelTemplate::color
	Color32_t874517518  ___color_3;
	// System.Boolean VoxelTemplate::useFrontUvsForAllFaces
	bool ___useFrontUvsForAllFaces_4;
	// System.Boolean VoxelTemplate::drawFacesInCenter
	bool ___drawFacesInCenter_5;
	// UnityEngine.Vector2 VoxelTemplate::atlasScale
	Vector2_t2243707579  ___atlasScale_6;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetFront
	Vector2_t2243707579  ___UVOffsetFront_7;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetBack
	Vector2_t2243707579  ___UVOffsetBack_8;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetTop
	Vector2_t2243707579  ___UVOffsetTop_9;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetBottom
	Vector2_t2243707579  ___UVOffsetBottom_10;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetLeft
	Vector2_t2243707579  ___UVOffsetLeft_11;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetRight
	Vector2_t2243707579  ___UVOffsetRight_12;
	// System.Boolean VoxelTemplate::shouldDraw
	bool ___shouldDraw_13;
	// System.Boolean VoxelTemplate::drawFront
	bool ___drawFront_14;
	// System.Boolean VoxelTemplate::drawBack
	bool ___drawBack_15;
	// System.Boolean VoxelTemplate::drawTop
	bool ___drawTop_16;
	// System.Boolean VoxelTemplate::drawBottom
	bool ___drawBottom_17;
	// System.Boolean VoxelTemplate::drawLeft
	bool ___drawLeft_18;
	// System.Boolean VoxelTemplate::drawRight
	bool ___drawRight_19;

public:
	inline static int32_t get_offset_of_DisplayOrder_2() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___DisplayOrder_2)); }
	inline int32_t get_DisplayOrder_2() const { return ___DisplayOrder_2; }
	inline int32_t* get_address_of_DisplayOrder_2() { return &___DisplayOrder_2; }
	inline void set_DisplayOrder_2(int32_t value)
	{
		___DisplayOrder_2 = value;
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___color_3)); }
	inline Color32_t874517518  get_color_3() const { return ___color_3; }
	inline Color32_t874517518 * get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(Color32_t874517518  value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_useFrontUvsForAllFaces_4() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___useFrontUvsForAllFaces_4)); }
	inline bool get_useFrontUvsForAllFaces_4() const { return ___useFrontUvsForAllFaces_4; }
	inline bool* get_address_of_useFrontUvsForAllFaces_4() { return &___useFrontUvsForAllFaces_4; }
	inline void set_useFrontUvsForAllFaces_4(bool value)
	{
		___useFrontUvsForAllFaces_4 = value;
	}

	inline static int32_t get_offset_of_drawFacesInCenter_5() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawFacesInCenter_5)); }
	inline bool get_drawFacesInCenter_5() const { return ___drawFacesInCenter_5; }
	inline bool* get_address_of_drawFacesInCenter_5() { return &___drawFacesInCenter_5; }
	inline void set_drawFacesInCenter_5(bool value)
	{
		___drawFacesInCenter_5 = value;
	}

	inline static int32_t get_offset_of_atlasScale_6() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___atlasScale_6)); }
	inline Vector2_t2243707579  get_atlasScale_6() const { return ___atlasScale_6; }
	inline Vector2_t2243707579 * get_address_of_atlasScale_6() { return &___atlasScale_6; }
	inline void set_atlasScale_6(Vector2_t2243707579  value)
	{
		___atlasScale_6 = value;
	}

	inline static int32_t get_offset_of_UVOffsetFront_7() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetFront_7)); }
	inline Vector2_t2243707579  get_UVOffsetFront_7() const { return ___UVOffsetFront_7; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetFront_7() { return &___UVOffsetFront_7; }
	inline void set_UVOffsetFront_7(Vector2_t2243707579  value)
	{
		___UVOffsetFront_7 = value;
	}

	inline static int32_t get_offset_of_UVOffsetBack_8() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetBack_8)); }
	inline Vector2_t2243707579  get_UVOffsetBack_8() const { return ___UVOffsetBack_8; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetBack_8() { return &___UVOffsetBack_8; }
	inline void set_UVOffsetBack_8(Vector2_t2243707579  value)
	{
		___UVOffsetBack_8 = value;
	}

	inline static int32_t get_offset_of_UVOffsetTop_9() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetTop_9)); }
	inline Vector2_t2243707579  get_UVOffsetTop_9() const { return ___UVOffsetTop_9; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetTop_9() { return &___UVOffsetTop_9; }
	inline void set_UVOffsetTop_9(Vector2_t2243707579  value)
	{
		___UVOffsetTop_9 = value;
	}

	inline static int32_t get_offset_of_UVOffsetBottom_10() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetBottom_10)); }
	inline Vector2_t2243707579  get_UVOffsetBottom_10() const { return ___UVOffsetBottom_10; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetBottom_10() { return &___UVOffsetBottom_10; }
	inline void set_UVOffsetBottom_10(Vector2_t2243707579  value)
	{
		___UVOffsetBottom_10 = value;
	}

	inline static int32_t get_offset_of_UVOffsetLeft_11() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetLeft_11)); }
	inline Vector2_t2243707579  get_UVOffsetLeft_11() const { return ___UVOffsetLeft_11; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetLeft_11() { return &___UVOffsetLeft_11; }
	inline void set_UVOffsetLeft_11(Vector2_t2243707579  value)
	{
		___UVOffsetLeft_11 = value;
	}

	inline static int32_t get_offset_of_UVOffsetRight_12() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetRight_12)); }
	inline Vector2_t2243707579  get_UVOffsetRight_12() const { return ___UVOffsetRight_12; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetRight_12() { return &___UVOffsetRight_12; }
	inline void set_UVOffsetRight_12(Vector2_t2243707579  value)
	{
		___UVOffsetRight_12 = value;
	}

	inline static int32_t get_offset_of_shouldDraw_13() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___shouldDraw_13)); }
	inline bool get_shouldDraw_13() const { return ___shouldDraw_13; }
	inline bool* get_address_of_shouldDraw_13() { return &___shouldDraw_13; }
	inline void set_shouldDraw_13(bool value)
	{
		___shouldDraw_13 = value;
	}

	inline static int32_t get_offset_of_drawFront_14() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawFront_14)); }
	inline bool get_drawFront_14() const { return ___drawFront_14; }
	inline bool* get_address_of_drawFront_14() { return &___drawFront_14; }
	inline void set_drawFront_14(bool value)
	{
		___drawFront_14 = value;
	}

	inline static int32_t get_offset_of_drawBack_15() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawBack_15)); }
	inline bool get_drawBack_15() const { return ___drawBack_15; }
	inline bool* get_address_of_drawBack_15() { return &___drawBack_15; }
	inline void set_drawBack_15(bool value)
	{
		___drawBack_15 = value;
	}

	inline static int32_t get_offset_of_drawTop_16() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawTop_16)); }
	inline bool get_drawTop_16() const { return ___drawTop_16; }
	inline bool* get_address_of_drawTop_16() { return &___drawTop_16; }
	inline void set_drawTop_16(bool value)
	{
		___drawTop_16 = value;
	}

	inline static int32_t get_offset_of_drawBottom_17() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawBottom_17)); }
	inline bool get_drawBottom_17() const { return ___drawBottom_17; }
	inline bool* get_address_of_drawBottom_17() { return &___drawBottom_17; }
	inline void set_drawBottom_17(bool value)
	{
		___drawBottom_17 = value;
	}

	inline static int32_t get_offset_of_drawLeft_18() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawLeft_18)); }
	inline bool get_drawLeft_18() const { return ___drawLeft_18; }
	inline bool* get_address_of_drawLeft_18() { return &___drawLeft_18; }
	inline void set_drawLeft_18(bool value)
	{
		___drawLeft_18 = value;
	}

	inline static int32_t get_offset_of_drawRight_19() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawRight_19)); }
	inline bool get_drawRight_19() const { return ___drawRight_19; }
	inline bool* get_address_of_drawRight_19() { return &___drawRight_19; }
	inline void set_drawRight_19(bool value)
	{
		___drawRight_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELTEMPLATE_T594004422_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef STRONGFOLLOWOBJECTCAMERA_T1302344390_H
#define STRONGFOLLOWOBJECTCAMERA_T1302344390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StrongFollowObjectCamera
struct  StrongFollowObjectCamera_t1302344390  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject StrongFollowObjectCamera::<Target>k__BackingField
	GameObject_t1756533147 * ___U3CTargetU3Ek__BackingField_2;
	// System.Single StrongFollowObjectCamera::SmoothXPlus
	float ___SmoothXPlus_3;
	// System.Single StrongFollowObjectCamera::SmoothYPlus
	float ___SmoothYPlus_4;
	// System.Single StrongFollowObjectCamera::SmoothZPlus
	float ___SmoothZPlus_5;
	// System.Single StrongFollowObjectCamera::SmoothXMinus
	float ___SmoothXMinus_6;
	// System.Single StrongFollowObjectCamera::SmoothYMinus
	float ___SmoothYMinus_7;
	// System.Single StrongFollowObjectCamera::SmoothZMinus
	float ___SmoothZMinus_8;
	// System.Single StrongFollowObjectCamera::Damping
	float ___Damping_9;
	// System.Boolean StrongFollowObjectCamera::ControllX
	bool ___ControllX_10;
	// System.Boolean StrongFollowObjectCamera::ControllY
	bool ___ControllY_11;
	// System.Boolean StrongFollowObjectCamera::ControllZ
	bool ___ControllZ_12;

public:
	inline static int32_t get_offset_of_U3CTargetU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___U3CTargetU3Ek__BackingField_2)); }
	inline GameObject_t1756533147 * get_U3CTargetU3Ek__BackingField_2() const { return ___U3CTargetU3Ek__BackingField_2; }
	inline GameObject_t1756533147 ** get_address_of_U3CTargetU3Ek__BackingField_2() { return &___U3CTargetU3Ek__BackingField_2; }
	inline void set_U3CTargetU3Ek__BackingField_2(GameObject_t1756533147 * value)
	{
		___U3CTargetU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_SmoothXPlus_3() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___SmoothXPlus_3)); }
	inline float get_SmoothXPlus_3() const { return ___SmoothXPlus_3; }
	inline float* get_address_of_SmoothXPlus_3() { return &___SmoothXPlus_3; }
	inline void set_SmoothXPlus_3(float value)
	{
		___SmoothXPlus_3 = value;
	}

	inline static int32_t get_offset_of_SmoothYPlus_4() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___SmoothYPlus_4)); }
	inline float get_SmoothYPlus_4() const { return ___SmoothYPlus_4; }
	inline float* get_address_of_SmoothYPlus_4() { return &___SmoothYPlus_4; }
	inline void set_SmoothYPlus_4(float value)
	{
		___SmoothYPlus_4 = value;
	}

	inline static int32_t get_offset_of_SmoothZPlus_5() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___SmoothZPlus_5)); }
	inline float get_SmoothZPlus_5() const { return ___SmoothZPlus_5; }
	inline float* get_address_of_SmoothZPlus_5() { return &___SmoothZPlus_5; }
	inline void set_SmoothZPlus_5(float value)
	{
		___SmoothZPlus_5 = value;
	}

	inline static int32_t get_offset_of_SmoothXMinus_6() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___SmoothXMinus_6)); }
	inline float get_SmoothXMinus_6() const { return ___SmoothXMinus_6; }
	inline float* get_address_of_SmoothXMinus_6() { return &___SmoothXMinus_6; }
	inline void set_SmoothXMinus_6(float value)
	{
		___SmoothXMinus_6 = value;
	}

	inline static int32_t get_offset_of_SmoothYMinus_7() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___SmoothYMinus_7)); }
	inline float get_SmoothYMinus_7() const { return ___SmoothYMinus_7; }
	inline float* get_address_of_SmoothYMinus_7() { return &___SmoothYMinus_7; }
	inline void set_SmoothYMinus_7(float value)
	{
		___SmoothYMinus_7 = value;
	}

	inline static int32_t get_offset_of_SmoothZMinus_8() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___SmoothZMinus_8)); }
	inline float get_SmoothZMinus_8() const { return ___SmoothZMinus_8; }
	inline float* get_address_of_SmoothZMinus_8() { return &___SmoothZMinus_8; }
	inline void set_SmoothZMinus_8(float value)
	{
		___SmoothZMinus_8 = value;
	}

	inline static int32_t get_offset_of_Damping_9() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___Damping_9)); }
	inline float get_Damping_9() const { return ___Damping_9; }
	inline float* get_address_of_Damping_9() { return &___Damping_9; }
	inline void set_Damping_9(float value)
	{
		___Damping_9 = value;
	}

	inline static int32_t get_offset_of_ControllX_10() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___ControllX_10)); }
	inline bool get_ControllX_10() const { return ___ControllX_10; }
	inline bool* get_address_of_ControllX_10() { return &___ControllX_10; }
	inline void set_ControllX_10(bool value)
	{
		___ControllX_10 = value;
	}

	inline static int32_t get_offset_of_ControllY_11() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___ControllY_11)); }
	inline bool get_ControllY_11() const { return ___ControllY_11; }
	inline bool* get_address_of_ControllY_11() { return &___ControllY_11; }
	inline void set_ControllY_11(bool value)
	{
		___ControllY_11 = value;
	}

	inline static int32_t get_offset_of_ControllZ_12() { return static_cast<int32_t>(offsetof(StrongFollowObjectCamera_t1302344390, ___ControllZ_12)); }
	inline bool get_ControllZ_12() const { return ___ControllZ_12; }
	inline bool* get_address_of_ControllZ_12() { return &___ControllZ_12; }
	inline void set_ControllZ_12(bool value)
	{
		___ControllZ_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRONGFOLLOWOBJECTCAMERA_T1302344390_H
#ifndef PARTICLESAUTODESTRUCT_T857371774_H
#define PARTICLESAUTODESTRUCT_T857371774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlesAutoDestruct
struct  ParticlesAutoDestruct_t857371774  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean ParticlesAutoDestruct::OnlyDeactivate
	bool ___OnlyDeactivate_2;
	// System.Action`1<UnityEngine.GameObject> ParticlesAutoDestruct::OnComplete
	Action_1_t1558332529 * ___OnComplete_3;

public:
	inline static int32_t get_offset_of_OnlyDeactivate_2() { return static_cast<int32_t>(offsetof(ParticlesAutoDestruct_t857371774, ___OnlyDeactivate_2)); }
	inline bool get_OnlyDeactivate_2() const { return ___OnlyDeactivate_2; }
	inline bool* get_address_of_OnlyDeactivate_2() { return &___OnlyDeactivate_2; }
	inline void set_OnlyDeactivate_2(bool value)
	{
		___OnlyDeactivate_2 = value;
	}

	inline static int32_t get_offset_of_OnComplete_3() { return static_cast<int32_t>(offsetof(ParticlesAutoDestruct_t857371774, ___OnComplete_3)); }
	inline Action_1_t1558332529 * get_OnComplete_3() const { return ___OnComplete_3; }
	inline Action_1_t1558332529 ** get_address_of_OnComplete_3() { return &___OnComplete_3; }
	inline void set_OnComplete_3(Action_1_t1558332529 * value)
	{
		___OnComplete_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESAUTODESTRUCT_T857371774_H
#ifndef PRESSEDUIELEMENT_T3939191172_H
#define PRESSEDUIELEMENT_T3939191172_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PressedUIElement
struct  PressedUIElement_t3939191172  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESSEDUIELEMENT_T3939191172_H
#ifndef VECTORPATH_T923964688_H
#define VECTORPATH_T923964688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VectorPath
struct  VectorPath_t923964688  : public MonoBehaviour_t1158329972
{
public:
	// System.Single VectorPath::Tolerance
	float ___Tolerance_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> VectorPath::Path
	List_1_t1612828712 * ___Path_3;

public:
	inline static int32_t get_offset_of_Tolerance_2() { return static_cast<int32_t>(offsetof(VectorPath_t923964688, ___Tolerance_2)); }
	inline float get_Tolerance_2() const { return ___Tolerance_2; }
	inline float* get_address_of_Tolerance_2() { return &___Tolerance_2; }
	inline void set_Tolerance_2(float value)
	{
		___Tolerance_2 = value;
	}

	inline static int32_t get_offset_of_Path_3() { return static_cast<int32_t>(offsetof(VectorPath_t923964688, ___Path_3)); }
	inline List_1_t1612828712 * get_Path_3() const { return ___Path_3; }
	inline List_1_t1612828712 ** get_address_of_Path_3() { return &___Path_3; }
	inline void set_Path_3(List_1_t1612828712 * value)
	{
		___Path_3 = value;
		Il2CppCodeGenWriteBarrier((&___Path_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTORPATH_T923964688_H
#ifndef VOXELSPACE_T930815766_H
#define VOXELSPACE_T930815766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelSpace
struct  VoxelSpace_t930815766  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 VoxelSpace::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_2;
	// System.Int32 VoxelSpace::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_3;
	// System.Int32 VoxelSpace::<Depth>k__BackingField
	int32_t ___U3CDepthU3Ek__BackingField_4;
	// System.Int32 VoxelSpace::_chunkSizeW
	int32_t ____chunkSizeW_5;
	// System.Int32 VoxelSpace::_chunkSizeH
	int32_t ____chunkSizeH_6;
	// System.Int32 VoxelSpace::_chunkSizeD
	int32_t ____chunkSizeD_7;
	// System.Int32 VoxelSpace::_chunkCountW
	int32_t ____chunkCountW_8;
	// System.Int32 VoxelSpace::_chunkCountH
	int32_t ____chunkCountH_9;
	// System.Int32 VoxelSpace::_chunkCountD
	int32_t ____chunkCountD_10;
	// VoxelStructure[0...,0...,0...] VoxelSpace::_chunks
	VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* ____chunks_11;
	// System.Boolean VoxelSpace::DrawFront
	bool ___DrawFront_12;
	// System.Boolean VoxelSpace::DrawBack
	bool ___DrawBack_13;
	// System.Boolean VoxelSpace::DrawTop
	bool ___DrawTop_14;
	// System.Boolean VoxelSpace::DrawBottom
	bool ___DrawBottom_15;
	// System.Boolean VoxelSpace::DrawLeft
	bool ___DrawLeft_16;
	// System.Boolean VoxelSpace::DrawRight
	bool ___DrawRight_17;
	// System.Boolean VoxelSpace::GenerateSecondaryUvSet
	bool ___GenerateSecondaryUvSet_18;
	// System.Boolean VoxelSpace::UsePalletteTexture
	bool ___UsePalletteTexture_19;

public:
	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___U3CWidthU3Ek__BackingField_2)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_2() const { return ___U3CWidthU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_2() { return &___U3CWidthU3Ek__BackingField_2; }
	inline void set_U3CWidthU3Ek__BackingField_2(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___U3CHeightU3Ek__BackingField_3)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_3() const { return ___U3CHeightU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_3() { return &___U3CHeightU3Ek__BackingField_3; }
	inline void set_U3CHeightU3Ek__BackingField_3(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDepthU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___U3CDepthU3Ek__BackingField_4)); }
	inline int32_t get_U3CDepthU3Ek__BackingField_4() const { return ___U3CDepthU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CDepthU3Ek__BackingField_4() { return &___U3CDepthU3Ek__BackingField_4; }
	inline void set_U3CDepthU3Ek__BackingField_4(int32_t value)
	{
		___U3CDepthU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of__chunkSizeW_5() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkSizeW_5)); }
	inline int32_t get__chunkSizeW_5() const { return ____chunkSizeW_5; }
	inline int32_t* get_address_of__chunkSizeW_5() { return &____chunkSizeW_5; }
	inline void set__chunkSizeW_5(int32_t value)
	{
		____chunkSizeW_5 = value;
	}

	inline static int32_t get_offset_of__chunkSizeH_6() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkSizeH_6)); }
	inline int32_t get__chunkSizeH_6() const { return ____chunkSizeH_6; }
	inline int32_t* get_address_of__chunkSizeH_6() { return &____chunkSizeH_6; }
	inline void set__chunkSizeH_6(int32_t value)
	{
		____chunkSizeH_6 = value;
	}

	inline static int32_t get_offset_of__chunkSizeD_7() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkSizeD_7)); }
	inline int32_t get__chunkSizeD_7() const { return ____chunkSizeD_7; }
	inline int32_t* get_address_of__chunkSizeD_7() { return &____chunkSizeD_7; }
	inline void set__chunkSizeD_7(int32_t value)
	{
		____chunkSizeD_7 = value;
	}

	inline static int32_t get_offset_of__chunkCountW_8() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkCountW_8)); }
	inline int32_t get__chunkCountW_8() const { return ____chunkCountW_8; }
	inline int32_t* get_address_of__chunkCountW_8() { return &____chunkCountW_8; }
	inline void set__chunkCountW_8(int32_t value)
	{
		____chunkCountW_8 = value;
	}

	inline static int32_t get_offset_of__chunkCountH_9() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkCountH_9)); }
	inline int32_t get__chunkCountH_9() const { return ____chunkCountH_9; }
	inline int32_t* get_address_of__chunkCountH_9() { return &____chunkCountH_9; }
	inline void set__chunkCountH_9(int32_t value)
	{
		____chunkCountH_9 = value;
	}

	inline static int32_t get_offset_of__chunkCountD_10() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkCountD_10)); }
	inline int32_t get__chunkCountD_10() const { return ____chunkCountD_10; }
	inline int32_t* get_address_of__chunkCountD_10() { return &____chunkCountD_10; }
	inline void set__chunkCountD_10(int32_t value)
	{
		____chunkCountD_10 = value;
	}

	inline static int32_t get_offset_of__chunks_11() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunks_11)); }
	inline VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* get__chunks_11() const { return ____chunks_11; }
	inline VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636** get_address_of__chunks_11() { return &____chunks_11; }
	inline void set__chunks_11(VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* value)
	{
		____chunks_11 = value;
		Il2CppCodeGenWriteBarrier((&____chunks_11), value);
	}

	inline static int32_t get_offset_of_DrawFront_12() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawFront_12)); }
	inline bool get_DrawFront_12() const { return ___DrawFront_12; }
	inline bool* get_address_of_DrawFront_12() { return &___DrawFront_12; }
	inline void set_DrawFront_12(bool value)
	{
		___DrawFront_12 = value;
	}

	inline static int32_t get_offset_of_DrawBack_13() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawBack_13)); }
	inline bool get_DrawBack_13() const { return ___DrawBack_13; }
	inline bool* get_address_of_DrawBack_13() { return &___DrawBack_13; }
	inline void set_DrawBack_13(bool value)
	{
		___DrawBack_13 = value;
	}

	inline static int32_t get_offset_of_DrawTop_14() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawTop_14)); }
	inline bool get_DrawTop_14() const { return ___DrawTop_14; }
	inline bool* get_address_of_DrawTop_14() { return &___DrawTop_14; }
	inline void set_DrawTop_14(bool value)
	{
		___DrawTop_14 = value;
	}

	inline static int32_t get_offset_of_DrawBottom_15() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawBottom_15)); }
	inline bool get_DrawBottom_15() const { return ___DrawBottom_15; }
	inline bool* get_address_of_DrawBottom_15() { return &___DrawBottom_15; }
	inline void set_DrawBottom_15(bool value)
	{
		___DrawBottom_15 = value;
	}

	inline static int32_t get_offset_of_DrawLeft_16() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawLeft_16)); }
	inline bool get_DrawLeft_16() const { return ___DrawLeft_16; }
	inline bool* get_address_of_DrawLeft_16() { return &___DrawLeft_16; }
	inline void set_DrawLeft_16(bool value)
	{
		___DrawLeft_16 = value;
	}

	inline static int32_t get_offset_of_DrawRight_17() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawRight_17)); }
	inline bool get_DrawRight_17() const { return ___DrawRight_17; }
	inline bool* get_address_of_DrawRight_17() { return &___DrawRight_17; }
	inline void set_DrawRight_17(bool value)
	{
		___DrawRight_17 = value;
	}

	inline static int32_t get_offset_of_GenerateSecondaryUvSet_18() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___GenerateSecondaryUvSet_18)); }
	inline bool get_GenerateSecondaryUvSet_18() const { return ___GenerateSecondaryUvSet_18; }
	inline bool* get_address_of_GenerateSecondaryUvSet_18() { return &___GenerateSecondaryUvSet_18; }
	inline void set_GenerateSecondaryUvSet_18(bool value)
	{
		___GenerateSecondaryUvSet_18 = value;
	}

	inline static int32_t get_offset_of_UsePalletteTexture_19() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___UsePalletteTexture_19)); }
	inline bool get_UsePalletteTexture_19() const { return ___UsePalletteTexture_19; }
	inline bool* get_address_of_UsePalletteTexture_19() { return &___UsePalletteTexture_19; }
	inline void set_UsePalletteTexture_19(bool value)
	{
		___UsePalletteTexture_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELSPACE_T930815766_H
#ifndef VOXELSTRUCTURE_T2247263099_H
#define VOXELSTRUCTURE_T2247263099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelStructure
struct  VoxelStructure_t2247263099  : public MonoBehaviour_t1158329972
{
public:
	// VoxelPallette VoxelStructure::pallette
	VoxelPallette_t4284593999 * ___pallette_2;
	// VoxelSpace VoxelStructure::<VoxelSpace>k__BackingField
	VoxelSpace_t930815766 * ___U3CVoxelSpaceU3Ek__BackingField_3;
	// System.Int32[0...,0...,0...] VoxelStructure::flatVoxels
	Int32U5B0___U2C0___U2C0___U5D_t3030399643* ___flatVoxels_7;
	// System.Int32 VoxelStructure::Width
	int32_t ___Width_8;
	// System.Int32 VoxelStructure::Height
	int32_t ___Height_9;
	// System.Int32 VoxelStructure::Depth
	int32_t ___Depth_10;
	// UnityEngine.Vector3[] VoxelStructure::_verts
	Vector3U5BU5D_t1172311765* ____verts_11;
	// UnityEngine.Vector2[] VoxelStructure::_uvs
	Vector2U5BU5D_t686124026* ____uvs_12;
	// UnityEngine.Color32[] VoxelStructure::_colors
	Color32U5BU5D_t30278651* ____colors_13;
	// UnityEngine.MeshFilter VoxelStructure::_mf
	MeshFilter_t3026937449 * ____mf_14;
	// System.Int32[] VoxelStructure::_tris
	Int32U5BU5D_t3030399641* ____tris_15;
	// VoxelTemplate VoxelStructure::fType
	VoxelTemplate_t594004422 * ___fType_16;
	// VoxelTemplate VoxelStructure::kType
	VoxelTemplate_t594004422 * ___kType_17;
	// VoxelTemplate VoxelStructure::rType
	VoxelTemplate_t594004422 * ___rType_18;
	// VoxelTemplate VoxelStructure::lType
	VoxelTemplate_t594004422 * ___lType_19;
	// VoxelTemplate VoxelStructure::tType
	VoxelTemplate_t594004422 * ___tType_20;
	// VoxelTemplate VoxelStructure::bType
	VoxelTemplate_t594004422 * ___bType_21;
	// VoxelTemplate VoxelStructure::voxelType
	VoxelTemplate_t594004422 * ___voxelType_22;
	// UnityEngine.Color32 VoxelStructure::voxelColor
	Color32_t874517518  ___voxelColor_23;
	// System.Boolean VoxelStructure::IsDirty
	bool ___IsDirty_24;
	// System.Single VoxelStructure::ONE_PIXEL
	float ___ONE_PIXEL_25;

public:
	inline static int32_t get_offset_of_pallette_2() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___pallette_2)); }
	inline VoxelPallette_t4284593999 * get_pallette_2() const { return ___pallette_2; }
	inline VoxelPallette_t4284593999 ** get_address_of_pallette_2() { return &___pallette_2; }
	inline void set_pallette_2(VoxelPallette_t4284593999 * value)
	{
		___pallette_2 = value;
		Il2CppCodeGenWriteBarrier((&___pallette_2), value);
	}

	inline static int32_t get_offset_of_U3CVoxelSpaceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___U3CVoxelSpaceU3Ek__BackingField_3)); }
	inline VoxelSpace_t930815766 * get_U3CVoxelSpaceU3Ek__BackingField_3() const { return ___U3CVoxelSpaceU3Ek__BackingField_3; }
	inline VoxelSpace_t930815766 ** get_address_of_U3CVoxelSpaceU3Ek__BackingField_3() { return &___U3CVoxelSpaceU3Ek__BackingField_3; }
	inline void set_U3CVoxelSpaceU3Ek__BackingField_3(VoxelSpace_t930815766 * value)
	{
		___U3CVoxelSpaceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVoxelSpaceU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_flatVoxels_7() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___flatVoxels_7)); }
	inline Int32U5B0___U2C0___U2C0___U5D_t3030399643* get_flatVoxels_7() const { return ___flatVoxels_7; }
	inline Int32U5B0___U2C0___U2C0___U5D_t3030399643** get_address_of_flatVoxels_7() { return &___flatVoxels_7; }
	inline void set_flatVoxels_7(Int32U5B0___U2C0___U2C0___U5D_t3030399643* value)
	{
		___flatVoxels_7 = value;
		Il2CppCodeGenWriteBarrier((&___flatVoxels_7), value);
	}

	inline static int32_t get_offset_of_Width_8() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___Width_8)); }
	inline int32_t get_Width_8() const { return ___Width_8; }
	inline int32_t* get_address_of_Width_8() { return &___Width_8; }
	inline void set_Width_8(int32_t value)
	{
		___Width_8 = value;
	}

	inline static int32_t get_offset_of_Height_9() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___Height_9)); }
	inline int32_t get_Height_9() const { return ___Height_9; }
	inline int32_t* get_address_of_Height_9() { return &___Height_9; }
	inline void set_Height_9(int32_t value)
	{
		___Height_9 = value;
	}

	inline static int32_t get_offset_of_Depth_10() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___Depth_10)); }
	inline int32_t get_Depth_10() const { return ___Depth_10; }
	inline int32_t* get_address_of_Depth_10() { return &___Depth_10; }
	inline void set_Depth_10(int32_t value)
	{
		___Depth_10 = value;
	}

	inline static int32_t get_offset_of__verts_11() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ____verts_11)); }
	inline Vector3U5BU5D_t1172311765* get__verts_11() const { return ____verts_11; }
	inline Vector3U5BU5D_t1172311765** get_address_of__verts_11() { return &____verts_11; }
	inline void set__verts_11(Vector3U5BU5D_t1172311765* value)
	{
		____verts_11 = value;
		Il2CppCodeGenWriteBarrier((&____verts_11), value);
	}

	inline static int32_t get_offset_of__uvs_12() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ____uvs_12)); }
	inline Vector2U5BU5D_t686124026* get__uvs_12() const { return ____uvs_12; }
	inline Vector2U5BU5D_t686124026** get_address_of__uvs_12() { return &____uvs_12; }
	inline void set__uvs_12(Vector2U5BU5D_t686124026* value)
	{
		____uvs_12 = value;
		Il2CppCodeGenWriteBarrier((&____uvs_12), value);
	}

	inline static int32_t get_offset_of__colors_13() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ____colors_13)); }
	inline Color32U5BU5D_t30278651* get__colors_13() const { return ____colors_13; }
	inline Color32U5BU5D_t30278651** get_address_of__colors_13() { return &____colors_13; }
	inline void set__colors_13(Color32U5BU5D_t30278651* value)
	{
		____colors_13 = value;
		Il2CppCodeGenWriteBarrier((&____colors_13), value);
	}

	inline static int32_t get_offset_of__mf_14() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ____mf_14)); }
	inline MeshFilter_t3026937449 * get__mf_14() const { return ____mf_14; }
	inline MeshFilter_t3026937449 ** get_address_of__mf_14() { return &____mf_14; }
	inline void set__mf_14(MeshFilter_t3026937449 * value)
	{
		____mf_14 = value;
		Il2CppCodeGenWriteBarrier((&____mf_14), value);
	}

	inline static int32_t get_offset_of__tris_15() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ____tris_15)); }
	inline Int32U5BU5D_t3030399641* get__tris_15() const { return ____tris_15; }
	inline Int32U5BU5D_t3030399641** get_address_of__tris_15() { return &____tris_15; }
	inline void set__tris_15(Int32U5BU5D_t3030399641* value)
	{
		____tris_15 = value;
		Il2CppCodeGenWriteBarrier((&____tris_15), value);
	}

	inline static int32_t get_offset_of_fType_16() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___fType_16)); }
	inline VoxelTemplate_t594004422 * get_fType_16() const { return ___fType_16; }
	inline VoxelTemplate_t594004422 ** get_address_of_fType_16() { return &___fType_16; }
	inline void set_fType_16(VoxelTemplate_t594004422 * value)
	{
		___fType_16 = value;
		Il2CppCodeGenWriteBarrier((&___fType_16), value);
	}

	inline static int32_t get_offset_of_kType_17() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___kType_17)); }
	inline VoxelTemplate_t594004422 * get_kType_17() const { return ___kType_17; }
	inline VoxelTemplate_t594004422 ** get_address_of_kType_17() { return &___kType_17; }
	inline void set_kType_17(VoxelTemplate_t594004422 * value)
	{
		___kType_17 = value;
		Il2CppCodeGenWriteBarrier((&___kType_17), value);
	}

	inline static int32_t get_offset_of_rType_18() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___rType_18)); }
	inline VoxelTemplate_t594004422 * get_rType_18() const { return ___rType_18; }
	inline VoxelTemplate_t594004422 ** get_address_of_rType_18() { return &___rType_18; }
	inline void set_rType_18(VoxelTemplate_t594004422 * value)
	{
		___rType_18 = value;
		Il2CppCodeGenWriteBarrier((&___rType_18), value);
	}

	inline static int32_t get_offset_of_lType_19() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___lType_19)); }
	inline VoxelTemplate_t594004422 * get_lType_19() const { return ___lType_19; }
	inline VoxelTemplate_t594004422 ** get_address_of_lType_19() { return &___lType_19; }
	inline void set_lType_19(VoxelTemplate_t594004422 * value)
	{
		___lType_19 = value;
		Il2CppCodeGenWriteBarrier((&___lType_19), value);
	}

	inline static int32_t get_offset_of_tType_20() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___tType_20)); }
	inline VoxelTemplate_t594004422 * get_tType_20() const { return ___tType_20; }
	inline VoxelTemplate_t594004422 ** get_address_of_tType_20() { return &___tType_20; }
	inline void set_tType_20(VoxelTemplate_t594004422 * value)
	{
		___tType_20 = value;
		Il2CppCodeGenWriteBarrier((&___tType_20), value);
	}

	inline static int32_t get_offset_of_bType_21() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___bType_21)); }
	inline VoxelTemplate_t594004422 * get_bType_21() const { return ___bType_21; }
	inline VoxelTemplate_t594004422 ** get_address_of_bType_21() { return &___bType_21; }
	inline void set_bType_21(VoxelTemplate_t594004422 * value)
	{
		___bType_21 = value;
		Il2CppCodeGenWriteBarrier((&___bType_21), value);
	}

	inline static int32_t get_offset_of_voxelType_22() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___voxelType_22)); }
	inline VoxelTemplate_t594004422 * get_voxelType_22() const { return ___voxelType_22; }
	inline VoxelTemplate_t594004422 ** get_address_of_voxelType_22() { return &___voxelType_22; }
	inline void set_voxelType_22(VoxelTemplate_t594004422 * value)
	{
		___voxelType_22 = value;
		Il2CppCodeGenWriteBarrier((&___voxelType_22), value);
	}

	inline static int32_t get_offset_of_voxelColor_23() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___voxelColor_23)); }
	inline Color32_t874517518  get_voxelColor_23() const { return ___voxelColor_23; }
	inline Color32_t874517518 * get_address_of_voxelColor_23() { return &___voxelColor_23; }
	inline void set_voxelColor_23(Color32_t874517518  value)
	{
		___voxelColor_23 = value;
	}

	inline static int32_t get_offset_of_IsDirty_24() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___IsDirty_24)); }
	inline bool get_IsDirty_24() const { return ___IsDirty_24; }
	inline bool* get_address_of_IsDirty_24() { return &___IsDirty_24; }
	inline void set_IsDirty_24(bool value)
	{
		___IsDirty_24 = value;
	}

	inline static int32_t get_offset_of_ONE_PIXEL_25() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___ONE_PIXEL_25)); }
	inline float get_ONE_PIXEL_25() const { return ___ONE_PIXEL_25; }
	inline float* get_address_of_ONE_PIXEL_25() { return &___ONE_PIXEL_25; }
	inline void set_ONE_PIXEL_25(float value)
	{
		___ONE_PIXEL_25 = value;
	}
};

struct VoxelStructure_t2247263099_StaticFields
{
public:
	// VoxelStructure/Action VoxelStructure::LMB_Action
	int32_t ___LMB_Action_4;
	// VoxelStructure/Action VoxelStructure::RMB_Action
	int32_t ___RMB_Action_5;
	// VoxelStructure/Action VoxelStructure::Static_Action
	int32_t ___Static_Action_6;

public:
	inline static int32_t get_offset_of_LMB_Action_4() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099_StaticFields, ___LMB_Action_4)); }
	inline int32_t get_LMB_Action_4() const { return ___LMB_Action_4; }
	inline int32_t* get_address_of_LMB_Action_4() { return &___LMB_Action_4; }
	inline void set_LMB_Action_4(int32_t value)
	{
		___LMB_Action_4 = value;
	}

	inline static int32_t get_offset_of_RMB_Action_5() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099_StaticFields, ___RMB_Action_5)); }
	inline int32_t get_RMB_Action_5() const { return ___RMB_Action_5; }
	inline int32_t* get_address_of_RMB_Action_5() { return &___RMB_Action_5; }
	inline void set_RMB_Action_5(int32_t value)
	{
		___RMB_Action_5 = value;
	}

	inline static int32_t get_offset_of_Static_Action_6() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099_StaticFields, ___Static_Action_6)); }
	inline int32_t get_Static_Action_6() const { return ___Static_Action_6; }
	inline int32_t* get_address_of_Static_Action_6() { return &___Static_Action_6; }
	inline void set_Static_Action_6(int32_t value)
	{
		___Static_Action_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELSTRUCTURE_T2247263099_H
#ifndef TIMEBOMB_T2617047665_H
#define TIMEBOMB_T2617047665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeBomb
struct  TimeBomb_t2617047665  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TimeBomb::Duration
	float ___Duration_2;
	// System.Single TimeBomb::_elapsed
	float ____elapsed_3;
	// System.Boolean TimeBomb::_complete
	bool ____complete_4;
	// UnityEngine.GameObject TimeBomb::<Owner>k__BackingField
	GameObject_t1756533147 * ___U3COwnerU3Ek__BackingField_5;
	// System.Action`1<UnityEngine.GameObject> TimeBomb::OnComplete
	Action_1_t1558332529 * ___OnComplete_6;

public:
	inline static int32_t get_offset_of_Duration_2() { return static_cast<int32_t>(offsetof(TimeBomb_t2617047665, ___Duration_2)); }
	inline float get_Duration_2() const { return ___Duration_2; }
	inline float* get_address_of_Duration_2() { return &___Duration_2; }
	inline void set_Duration_2(float value)
	{
		___Duration_2 = value;
	}

	inline static int32_t get_offset_of__elapsed_3() { return static_cast<int32_t>(offsetof(TimeBomb_t2617047665, ____elapsed_3)); }
	inline float get__elapsed_3() const { return ____elapsed_3; }
	inline float* get_address_of__elapsed_3() { return &____elapsed_3; }
	inline void set__elapsed_3(float value)
	{
		____elapsed_3 = value;
	}

	inline static int32_t get_offset_of__complete_4() { return static_cast<int32_t>(offsetof(TimeBomb_t2617047665, ____complete_4)); }
	inline bool get__complete_4() const { return ____complete_4; }
	inline bool* get_address_of__complete_4() { return &____complete_4; }
	inline void set__complete_4(bool value)
	{
		____complete_4 = value;
	}

	inline static int32_t get_offset_of_U3COwnerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TimeBomb_t2617047665, ___U3COwnerU3Ek__BackingField_5)); }
	inline GameObject_t1756533147 * get_U3COwnerU3Ek__BackingField_5() const { return ___U3COwnerU3Ek__BackingField_5; }
	inline GameObject_t1756533147 ** get_address_of_U3COwnerU3Ek__BackingField_5() { return &___U3COwnerU3Ek__BackingField_5; }
	inline void set_U3COwnerU3Ek__BackingField_5(GameObject_t1756533147 * value)
	{
		___U3COwnerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3COwnerU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_OnComplete_6() { return static_cast<int32_t>(offsetof(TimeBomb_t2617047665, ___OnComplete_6)); }
	inline Action_1_t1558332529 * get_OnComplete_6() const { return ___OnComplete_6; }
	inline Action_1_t1558332529 ** get_address_of_OnComplete_6() { return &___OnComplete_6; }
	inline void set_OnComplete_6(Action_1_t1558332529 * value)
	{
		___OnComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEBOMB_T2617047665_H
#ifndef SCREENSHOT_T3429867996_H
#define SCREENSHOT_T3429867996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenShot
struct  ScreenShot_t3429867996  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct ScreenShot_t3429867996_StaticFields
{
public:
	// System.Int32 ScreenShot::number
	int32_t ___number_2;

public:
	inline static int32_t get_offset_of_number_2() { return static_cast<int32_t>(offsetof(ScreenShot_t3429867996_StaticFields, ___number_2)); }
	inline int32_t get_number_2() const { return ___number_2; }
	inline int32_t* get_address_of_number_2() { return &___number_2; }
	inline void set_number_2(int32_t value)
	{
		___number_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSHOT_T3429867996_H
#ifndef TIMEBOMBMBH_T1277097630_H
#define TIMEBOMBMBH_T1277097630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeBombMBH
struct  TimeBombMBH_t1277097630  : public MonoBehaviour_t1158329972
{
public:
	// System.Single TimeBombMBH::Duration
	float ___Duration_2;
	// System.Single TimeBombMBH::_elapsed
	float ____elapsed_3;
	// System.Boolean TimeBombMBH::_complete
	bool ____complete_4;
	// UnityEngine.MonoBehaviour TimeBombMBH::<Owner>k__BackingField
	MonoBehaviour_t1158329972 * ___U3COwnerU3Ek__BackingField_5;
	// System.Action`1<TimeBombMBH> TimeBombMBH::OnComplete
	Action_1_t1078897012 * ___OnComplete_6;

public:
	inline static int32_t get_offset_of_Duration_2() { return static_cast<int32_t>(offsetof(TimeBombMBH_t1277097630, ___Duration_2)); }
	inline float get_Duration_2() const { return ___Duration_2; }
	inline float* get_address_of_Duration_2() { return &___Duration_2; }
	inline void set_Duration_2(float value)
	{
		___Duration_2 = value;
	}

	inline static int32_t get_offset_of__elapsed_3() { return static_cast<int32_t>(offsetof(TimeBombMBH_t1277097630, ____elapsed_3)); }
	inline float get__elapsed_3() const { return ____elapsed_3; }
	inline float* get_address_of__elapsed_3() { return &____elapsed_3; }
	inline void set__elapsed_3(float value)
	{
		____elapsed_3 = value;
	}

	inline static int32_t get_offset_of__complete_4() { return static_cast<int32_t>(offsetof(TimeBombMBH_t1277097630, ____complete_4)); }
	inline bool get__complete_4() const { return ____complete_4; }
	inline bool* get_address_of__complete_4() { return &____complete_4; }
	inline void set__complete_4(bool value)
	{
		____complete_4 = value;
	}

	inline static int32_t get_offset_of_U3COwnerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TimeBombMBH_t1277097630, ___U3COwnerU3Ek__BackingField_5)); }
	inline MonoBehaviour_t1158329972 * get_U3COwnerU3Ek__BackingField_5() const { return ___U3COwnerU3Ek__BackingField_5; }
	inline MonoBehaviour_t1158329972 ** get_address_of_U3COwnerU3Ek__BackingField_5() { return &___U3COwnerU3Ek__BackingField_5; }
	inline void set_U3COwnerU3Ek__BackingField_5(MonoBehaviour_t1158329972 * value)
	{
		___U3COwnerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3COwnerU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_OnComplete_6() { return static_cast<int32_t>(offsetof(TimeBombMBH_t1277097630, ___OnComplete_6)); }
	inline Action_1_t1078897012 * get_OnComplete_6() const { return ___OnComplete_6; }
	inline Action_1_t1078897012 ** get_address_of_OnComplete_6() { return &___OnComplete_6; }
	inline void set_OnComplete_6(Action_1_t1078897012 * value)
	{
		___OnComplete_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnComplete_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEBOMBMBH_T1277097630_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2200 = { sizeof (ParticlesAutoDestruct_t857371774), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2200[2] = 
{
	ParticlesAutoDestruct_t857371774::get_offset_of_OnlyDeactivate_2(),
	ParticlesAutoDestruct_t857371774::get_offset_of_OnComplete_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2201 = { sizeof (U3CCheckIfAliveU3Ec__Iterator0_t816385830), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2201[5] = 
{
	U3CCheckIfAliveU3Ec__Iterator0_t816385830::get_offset_of_U3CpsU3E__0_0(),
	U3CCheckIfAliveU3Ec__Iterator0_t816385830::get_offset_of_U24this_1(),
	U3CCheckIfAliveU3Ec__Iterator0_t816385830::get_offset_of_U24current_2(),
	U3CCheckIfAliveU3Ec__Iterator0_t816385830::get_offset_of_U24disposing_3(),
	U3CCheckIfAliveU3Ec__Iterator0_t816385830::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2202 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2203 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2203[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2204 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2204[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2205 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2205[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2206 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2206[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2207 = { sizeof (PressedUIElement_t3939191172), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2208 = { sizeof (Profiles_t3461944550), -1, sizeof(Profiles_t3461944550_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2208[2] = 
{
	Profiles_t3461944550_StaticFields::get_offset_of_U3CUserNameU3Ek__BackingField_0(),
	Profiles_t3461944550_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2209 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2210 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2210[13] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2211 = { sizeof (RandomHelper_t2807717491), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2212 = { sizeof (Range_t624945109)+ sizeof (RuntimeObject), sizeof(Range_t624945109 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2212[2] = 
{
	Range_t624945109::get_offset_of_Minimum_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Range_t624945109::get_offset_of_Maximum_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2213 = { sizeof (Region2D_t3281579718), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2213[3] = 
{
	Region2D_t3281579718::get_offset_of_Points_0(),
	Region2D_t3281579718::get_offset_of__cicled_1(),
	Region2D_t3281579718::get_offset_of_TransformedPoints_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2214 = { sizeof (ScreenShot_t3429867996), -1, sizeof(ScreenShot_t3429867996_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2214[1] = 
{
	ScreenShot_t3429867996_StaticFields::get_offset_of_number_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2215 = { sizeof (ScriptableObjectUtility_t2260201772), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2216 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2217 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2218 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2218[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2219 = { sizeof (PairIntIntDictionary_t2858279954), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2219[1] = 
{
	PairIntIntDictionary_t2858279954::get_offset_of_Pairs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2220 = { sizeof (U3CContainsKeyU3Ec__AnonStorey0_t2167278752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2220[1] = 
{
	U3CContainsKeyU3Ec__AnonStorey0_t2167278752::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2221 = { sizeof (U3CGetValuesU3Ec__AnonStorey1_t1165659093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2221[1] = 
{
	U3CGetValuesU3Ec__AnonStorey1_t1165659093::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2222 = { sizeof (U3CGetKeysU3Ec__AnonStorey2_t2671560958), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2222[1] = 
{
	U3CGetKeysU3Ec__AnonStorey2_t2671560958::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2223 = { sizeof (U3CRemoveU3Ec__AnonStorey3_t2381651523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2223[1] = 
{
	U3CRemoveU3Ec__AnonStorey3_t2381651523::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2224 = { sizeof (PairStringIntDictionary_t1323851614), -1, sizeof(PairStringIntDictionary_t1323851614_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2224[2] = 
{
	PairStringIntDictionary_t1323851614::get_offset_of_Pairs_0(),
	PairStringIntDictionary_t1323851614_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2225 = { sizeof (U3CContainsKeyU3Ec__AnonStorey0_t153965376), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2225[1] = 
{
	U3CContainsKeyU3Ec__AnonStorey0_t153965376::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2226 = { sizeof (U3CU3Ec__AnonStorey1_t402337725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2226[1] = 
{
	U3CU3Ec__AnonStorey1_t402337725::get_offset_of_index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2227 = { sizeof (U3CSetU3Ec__AnonStorey2_t3921341386), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2227[1] = 
{
	U3CSetU3Ec__AnonStorey2_t3921341386::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2228 = { sizeof (U3CGetValuesU3Ec__AnonStorey3_t2450800505), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2228[1] = 
{
	U3CGetValuesU3Ec__AnonStorey3_t2450800505::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2229 = { sizeof (U3CGetKeysU3Ec__AnonStorey4_t2027276052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2229[1] = 
{
	U3CGetKeysU3Ec__AnonStorey4_t2027276052::get_offset_of_result_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2230 = { sizeof (U3CRemoveU3Ec__AnonStorey5_t4162925351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2230[1] = 
{
	U3CRemoveU3Ec__AnonStorey5_t4162925351::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2231 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2231[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2232 = { sizeof (PairIntFloat_t3853530191)+ sizeof (RuntimeObject), sizeof(PairIntFloat_t3853530191 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2232[2] = 
{
	PairIntFloat_t3853530191::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PairIntFloat_t3853530191::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2233 = { sizeof (PairIntInt_t3314411122)+ sizeof (RuntimeObject), sizeof(PairIntInt_t3314411122 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2233[2] = 
{
	PairIntInt_t3314411122::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PairIntInt_t3314411122::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2234 = { sizeof (PairIntListInt_t1008085406)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2234[2] = 
{
	PairIntListInt_t1008085406::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PairIntListInt_t1008085406::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2235 = { sizeof (PairIntWeightedOptions_t126247562)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2235[2] = 
{
	PairIntWeightedOptions_t126247562::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PairIntWeightedOptions_t126247562::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2236 = { sizeof (PairStringAudio_t4289727603)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2236[2] = 
{
	PairStringAudio_t4289727603::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PairStringAudio_t4289727603::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2237 = { sizeof (PairStringBool_t2722701971)+ sizeof (RuntimeObject), sizeof(PairStringBool_t2722701971_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2237[2] = 
{
	PairStringBool_t2722701971::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PairStringBool_t2722701971::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2238 = { sizeof (PairStringGameObject_t2091526800)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2238[2] = 
{
	PairStringGameObject_t2091526800::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PairStringGameObject_t2091526800::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2239 = { sizeof (PairStringInt_t4221001538)+ sizeof (RuntimeObject), sizeof(PairStringInt_t4221001538_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2239[2] = 
{
	PairStringInt_t4221001538::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PairStringInt_t4221001538::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2240 = { sizeof (PairStringString_t2726841396)+ sizeof (RuntimeObject), sizeof(PairStringString_t2726841396_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2240[2] = 
{
	PairStringString_t2726841396::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PairStringString_t2726841396::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2241 = { sizeof (PairVector2Int_t2786601292)+ sizeof (RuntimeObject), sizeof(PairVector2Int_t2786601292 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2241[2] = 
{
	PairVector2Int_t2786601292::get_offset_of_Key_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PairVector2Int_t2786601292::get_offset_of_Value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2242 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2242[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2243 = { sizeof (StringHelper_t3272835469), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2244 = { sizeof (StrongFollowObjectCamera_t1302344390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2244[11] = 
{
	StrongFollowObjectCamera_t1302344390::get_offset_of_U3CTargetU3Ek__BackingField_2(),
	StrongFollowObjectCamera_t1302344390::get_offset_of_SmoothXPlus_3(),
	StrongFollowObjectCamera_t1302344390::get_offset_of_SmoothYPlus_4(),
	StrongFollowObjectCamera_t1302344390::get_offset_of_SmoothZPlus_5(),
	StrongFollowObjectCamera_t1302344390::get_offset_of_SmoothXMinus_6(),
	StrongFollowObjectCamera_t1302344390::get_offset_of_SmoothYMinus_7(),
	StrongFollowObjectCamera_t1302344390::get_offset_of_SmoothZMinus_8(),
	StrongFollowObjectCamera_t1302344390::get_offset_of_Damping_9(),
	StrongFollowObjectCamera_t1302344390::get_offset_of_ControllX_10(),
	StrongFollowObjectCamera_t1302344390::get_offset_of_ControllY_11(),
	StrongFollowObjectCamera_t1302344390::get_offset_of_ControllZ_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2245 = { sizeof (TimeBomb_t2617047665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2245[5] = 
{
	TimeBomb_t2617047665::get_offset_of_Duration_2(),
	TimeBomb_t2617047665::get_offset_of__elapsed_3(),
	TimeBomb_t2617047665::get_offset_of__complete_4(),
	TimeBomb_t2617047665::get_offset_of_U3COwnerU3Ek__BackingField_5(),
	TimeBomb_t2617047665::get_offset_of_OnComplete_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2246 = { sizeof (TimeBombMBH_t1277097630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2246[5] = 
{
	TimeBombMBH_t1277097630::get_offset_of_Duration_2(),
	TimeBombMBH_t1277097630::get_offset_of__elapsed_3(),
	TimeBombMBH_t1277097630::get_offset_of__complete_4(),
	TimeBombMBH_t1277097630::get_offset_of_U3COwnerU3Ek__BackingField_5(),
	TimeBombMBH_t1277097630::get_offset_of_OnComplete_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2247 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2248 = { sizeof (DirectionTypes2D_t4012228328)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2248[10] = 
{
	DirectionTypes2D_t4012228328::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2249 = { sizeof (Vector2i_t3488636706)+ sizeof (RuntimeObject), sizeof(Vector2i_t3488636706 ), sizeof(Vector2i_t3488636706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2249[3] = 
{
	Vector2i_t3488636706::get_offset_of_X_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector2i_t3488636706::get_offset_of_Y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector2i_t3488636706_StaticFields::get_offset_of__deltas_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2250 = { sizeof (DirectionTypes_t1731595534)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2250[28] = 
{
	DirectionTypes_t1731595534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2251 = { sizeof (Vector3i_t3488636705)+ sizeof (RuntimeObject), sizeof(Vector3i_t3488636705 ), sizeof(Vector3i_t3488636705_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2251[10] = 
{
	Vector3i_t3488636705::get_offset_of_x_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3i_t3488636705::get_offset_of_y_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3i_t3488636705::get_offset_of_z_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3i_t3488636705_StaticFields::get_offset_of_zero_3(),
	Vector3i_t3488636705_StaticFields::get_offset_of_one_4(),
	Vector3i_t3488636705_StaticFields::get_offset_of_oneTileNorth_5(),
	Vector3i_t3488636705_StaticFields::get_offset_of_oneTileEast_6(),
	Vector3i_t3488636705_StaticFields::get_offset_of_oneTileSouth_7(),
	Vector3i_t3488636705_StaticFields::get_offset_of_oneTileWest_8(),
	Vector3i_t3488636705_StaticFields::get_offset_of_Deltas_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2252 = { sizeof (VectorPath_t923964688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2252[2] = 
{
	VectorPath_t923964688::get_offset_of_Tolerance_2(),
	VectorPath_t923964688::get_offset_of_Path_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2253 = { sizeof (VoxelPallette_t4284593999), -1, sizeof(VoxelPallette_t4284593999_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2253[6] = 
{
	VoxelPallette_t4284593999::get_offset_of_AtlasMaterial_0(),
	VoxelPallette_t4284593999::get_offset_of_voxelTemplates_1(),
	VoxelPallette_t4284593999::get_offset_of_voxelNames_2(),
	VoxelPallette_t4284593999::get_offset_of__lookup_3(),
	VoxelPallette_t4284593999_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	VoxelPallette_t4284593999_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2254 = { sizeof (VoxelSpace_t930815766), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2254[18] = 
{
	VoxelSpace_t930815766::get_offset_of_U3CWidthU3Ek__BackingField_2(),
	VoxelSpace_t930815766::get_offset_of_U3CHeightU3Ek__BackingField_3(),
	VoxelSpace_t930815766::get_offset_of_U3CDepthU3Ek__BackingField_4(),
	VoxelSpace_t930815766::get_offset_of__chunkSizeW_5(),
	VoxelSpace_t930815766::get_offset_of__chunkSizeH_6(),
	VoxelSpace_t930815766::get_offset_of__chunkSizeD_7(),
	VoxelSpace_t930815766::get_offset_of__chunkCountW_8(),
	VoxelSpace_t930815766::get_offset_of__chunkCountH_9(),
	VoxelSpace_t930815766::get_offset_of__chunkCountD_10(),
	VoxelSpace_t930815766::get_offset_of__chunks_11(),
	VoxelSpace_t930815766::get_offset_of_DrawFront_12(),
	VoxelSpace_t930815766::get_offset_of_DrawBack_13(),
	VoxelSpace_t930815766::get_offset_of_DrawTop_14(),
	VoxelSpace_t930815766::get_offset_of_DrawBottom_15(),
	VoxelSpace_t930815766::get_offset_of_DrawLeft_16(),
	VoxelSpace_t930815766::get_offset_of_DrawRight_17(),
	VoxelSpace_t930815766::get_offset_of_GenerateSecondaryUvSet_18(),
	VoxelSpace_t930815766::get_offset_of_UsePalletteTexture_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2255 = { sizeof (VoxelStructure_t2247263099), -1, sizeof(VoxelStructure_t2247263099_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2255[24] = 
{
	VoxelStructure_t2247263099::get_offset_of_pallette_2(),
	VoxelStructure_t2247263099::get_offset_of_U3CVoxelSpaceU3Ek__BackingField_3(),
	VoxelStructure_t2247263099_StaticFields::get_offset_of_LMB_Action_4(),
	VoxelStructure_t2247263099_StaticFields::get_offset_of_RMB_Action_5(),
	VoxelStructure_t2247263099_StaticFields::get_offset_of_Static_Action_6(),
	VoxelStructure_t2247263099::get_offset_of_flatVoxels_7(),
	VoxelStructure_t2247263099::get_offset_of_Width_8(),
	VoxelStructure_t2247263099::get_offset_of_Height_9(),
	VoxelStructure_t2247263099::get_offset_of_Depth_10(),
	VoxelStructure_t2247263099::get_offset_of__verts_11(),
	VoxelStructure_t2247263099::get_offset_of__uvs_12(),
	VoxelStructure_t2247263099::get_offset_of__colors_13(),
	VoxelStructure_t2247263099::get_offset_of__mf_14(),
	VoxelStructure_t2247263099::get_offset_of__tris_15(),
	VoxelStructure_t2247263099::get_offset_of_fType_16(),
	VoxelStructure_t2247263099::get_offset_of_kType_17(),
	VoxelStructure_t2247263099::get_offset_of_rType_18(),
	VoxelStructure_t2247263099::get_offset_of_lType_19(),
	VoxelStructure_t2247263099::get_offset_of_tType_20(),
	VoxelStructure_t2247263099::get_offset_of_bType_21(),
	VoxelStructure_t2247263099::get_offset_of_voxelType_22(),
	VoxelStructure_t2247263099::get_offset_of_voxelColor_23(),
	VoxelStructure_t2247263099::get_offset_of_IsDirty_24(),
	VoxelStructure_t2247263099::get_offset_of_ONE_PIXEL_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2256 = { sizeof (Action_t4052865656)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2256[8] = 
{
	Action_t4052865656::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2257 = { sizeof (VoxelFace_t2735811281)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2257[7] = 
{
	VoxelFace_t2735811281::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2258 = { sizeof (VoxelTemplate_t594004422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2258[18] = 
{
	VoxelTemplate_t594004422::get_offset_of_DisplayOrder_2(),
	VoxelTemplate_t594004422::get_offset_of_color_3(),
	VoxelTemplate_t594004422::get_offset_of_useFrontUvsForAllFaces_4(),
	VoxelTemplate_t594004422::get_offset_of_drawFacesInCenter_5(),
	VoxelTemplate_t594004422::get_offset_of_atlasScale_6(),
	VoxelTemplate_t594004422::get_offset_of_UVOffsetFront_7(),
	VoxelTemplate_t594004422::get_offset_of_UVOffsetBack_8(),
	VoxelTemplate_t594004422::get_offset_of_UVOffsetTop_9(),
	VoxelTemplate_t594004422::get_offset_of_UVOffsetBottom_10(),
	VoxelTemplate_t594004422::get_offset_of_UVOffsetLeft_11(),
	VoxelTemplate_t594004422::get_offset_of_UVOffsetRight_12(),
	VoxelTemplate_t594004422::get_offset_of_shouldDraw_13(),
	VoxelTemplate_t594004422::get_offset_of_drawFront_14(),
	VoxelTemplate_t594004422::get_offset_of_drawBack_15(),
	VoxelTemplate_t594004422::get_offset_of_drawTop_16(),
	VoxelTemplate_t594004422::get_offset_of_drawBottom_17(),
	VoxelTemplate_t594004422::get_offset_of_drawLeft_18(),
	VoxelTemplate_t594004422::get_offset_of_drawRight_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2259 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305143), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2259[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields::get_offset_of_U24fieldU2D8E7629AD5AF686202B8CB7C014505C432FFE31E6_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2260 = { sizeof (U24ArrayTypeU3D24_t762068664)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t762068664 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
