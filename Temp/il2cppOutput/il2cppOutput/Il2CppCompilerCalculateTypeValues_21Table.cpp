﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t2919945039;
// System.Action`1<UnityEngine.Texture>
struct Action_1_t2045425701;
// System.Collections.Generic.List`1<PairStringInt>
struct List_1_t3590122670;
// System.Func`2<PairStringInt,System.Int32>
struct Func_2_t4016156815;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// CoroutinerInstance
struct CoroutinerInstance_t1991176683;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// Player
struct Player_t1147783557;
// IModel
struct IModel_t2236798000;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// NotificationItem
struct NotificationItem_t205362912;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject>
struct Dictionary_2_t4220005149;
// System.Security.Cryptography.MD5
struct MD5_t1507972490;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t3359083662;
// System.String[]
struct StringU5BU5D_t1642385972;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.Void
struct Void_t1841601450;
// System.Char[]
struct CharU5BU5D_t1328083999;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.Collections.Generic.List`1<PairStringAudio>
struct List_1_t3658848735;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Collections.Generic.List`1<LocalizationLanguageData>
struct List_1_t2158850971;
// LocalizationLanguageData
struct LocalizationLanguageData_t2789729839;
// System.Predicate`1<LocalizationLanguageData>
struct Predicate_1_t1232699954;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t1125654279;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.ParticleSystem
struct ParticleSystem_t3394631041;
// System.Action`2<UnityEngine.GameObject,UnityEngine.Transform>
struct Action_2_t2601038636;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Font[]
struct FontU5BU5D_t4130692050;
// UnityEngine.TextMesh
struct TextMesh_t1641806576;
// ADS.ADHost/ADEvent
struct ADEvent_t127799521;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1204166949;
// UnityEngine.XR.iOS.UnityARSessionNativeInterface
struct UnityARSessionNativeInterface_t1130867170;
// UnityEngine.XR.iOS.UnityARSessionRunOption[]
struct UnityARSessionRunOptionU5BU5D_t3114965901;
// UnityEngine.XR.iOS.UnityARAlignment[]
struct UnityARAlignmentU5BU5D_t218994990;
// UnityEngine.XR.iOS.UnityARPlaneDetection[]
struct UnityARPlaneDetectionU5BU5D_t191549612;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Camera
struct Camera_t189460977;
// System.Collections.Generic.List`1<IModel>
struct List_1_t1605919132;
// NotificationPanel
struct NotificationPanel_t4176433925;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.Action
struct Action_t3226471752;
// DG.Tweening.Tweener
struct Tweener_t760404022;
// UnityEngine.Sprite
struct Sprite_t309593783;
// System.Collections.Generic.Dictionary`2<System.String,NotificationItem>
struct Dictionary_2_t2120142174;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t3244290001;
// UnityEngine.Audio.AudioMixerGroup
struct AudioMixerGroup_t959546644;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// SoundCollection
struct SoundCollection_t2692967059;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.Func`1<System.Boolean>
struct Func_1_t1485000104;
// UnityEngine.XR.iOS.UnityARAnchorManager
struct UnityARAnchorManager_t1086564192;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// ViewManager
struct ViewManager_t2308013472;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.Events.UnityAction`1<IModel>
struct UnityAction_1_t3603383751;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;
// UnityEngine.BoxCollider
struct BoxCollider_t22920061;
// System.Collections.Generic.List`1<MiniMap/MiniMapPoint>
struct List_1_t4033011997;
// System.Predicate`1<MiniMap/MiniMapPoint>
struct Predicate_1_t3106860980;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef LOADREMOTESOURCE_T3573554701_H
#define LOADREMOTESOURCE_T3573554701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadRemoteSource
struct  LoadRemoteSource_t3573554701  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADREMOTESOURCE_T3573554701_H
#ifndef U3CLOADIMGENUMERATORU3EC__ITERATOR0_T2613224866_H
#define U3CLOADIMGENUMERATORU3EC__ITERATOR0_T2613224866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadRemoteSource/<LoadImgEnumerator>c__Iterator0
struct  U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866  : public RuntimeObject
{
public:
	// System.String LoadRemoteSource/<LoadImgEnumerator>c__Iterator0::imgURL
	String_t* ___imgURL_0;
	// UnityEngine.WWW LoadRemoteSource/<LoadImgEnumerator>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// System.Action`1<UnityEngine.Texture> LoadRemoteSource/<LoadImgEnumerator>c__Iterator0::callback
	Action_1_t2045425701 * ___callback_2;
	// System.Object LoadRemoteSource/<LoadImgEnumerator>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean LoadRemoteSource/<LoadImgEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 LoadRemoteSource/<LoadImgEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_imgURL_0() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866, ___imgURL_0)); }
	inline String_t* get_imgURL_0() const { return ___imgURL_0; }
	inline String_t** get_address_of_imgURL_0() { return &___imgURL_0; }
	inline void set_imgURL_0(String_t* value)
	{
		___imgURL_0 = value;
		Il2CppCodeGenWriteBarrier((&___imgURL_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866, ___callback_2)); }
	inline Action_1_t2045425701 * get_callback_2() const { return ___callback_2; }
	inline Action_1_t2045425701 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_1_t2045425701 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMGENUMERATORU3EC__ITERATOR0_T2613224866_H
#ifndef PAIRSTRINGINTDICTIONARY_T1323851614_H
#define PAIRSTRINGINTDICTIONARY_T1323851614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringIntDictionary
struct  PairStringIntDictionary_t1323851614  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<PairStringInt> PairStringIntDictionary::Pairs
	List_1_t3590122670 * ___Pairs_0;

public:
	inline static int32_t get_offset_of_Pairs_0() { return static_cast<int32_t>(offsetof(PairStringIntDictionary_t1323851614, ___Pairs_0)); }
	inline List_1_t3590122670 * get_Pairs_0() const { return ___Pairs_0; }
	inline List_1_t3590122670 ** get_address_of_Pairs_0() { return &___Pairs_0; }
	inline void set_Pairs_0(List_1_t3590122670 * value)
	{
		___Pairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___Pairs_0), value);
	}
};

struct PairStringIntDictionary_t1323851614_StaticFields
{
public:
	// System.Func`2<PairStringInt,System.Int32> PairStringIntDictionary::<>f__am$cache0
	Func_2_t4016156815 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(PairStringIntDictionary_t1323851614_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t4016156815 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t4016156815 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t4016156815 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRSTRINGINTDICTIONARY_T1323851614_H
#ifndef COROUTINER_T3262866136_H
#define COROUTINER_T3262866136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Coroutiner
struct  Coroutiner_t3262866136  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COROUTINER_T3262866136_H
#ifndef DATABASEACCESS_T3594830723_H
#define DATABASEACCESS_T3594830723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataBaseAccess
struct  DataBaseAccess_t3594830723  : public RuntimeObject
{
public:
	// System.String DataBaseAccess::ServerURL
	String_t* ___ServerURL_0;

public:
	inline static int32_t get_offset_of_ServerURL_0() { return static_cast<int32_t>(offsetof(DataBaseAccess_t3594830723, ___ServerURL_0)); }
	inline String_t* get_ServerURL_0() const { return ___ServerURL_0; }
	inline String_t** get_address_of_ServerURL_0() { return &___ServerURL_0; }
	inline void set_ServerURL_0(String_t* value)
	{
		___ServerURL_0 = value;
		Il2CppCodeGenWriteBarrier((&___ServerURL_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATABASEACCESS_T3594830723_H
#ifndef EDGEHELPERS_T748135790_H
#define EDGEHELPERS_T748135790_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EdgeHelpers
struct  EdgeHelpers_t748135790  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGEHELPERS_T748135790_H
#ifndef EXTENSIONS_T612262650_H
#define EXTENSIONS_T612262650_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Extensions
struct  Extensions_t612262650  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONS_T612262650_H
#ifndef FASTLINECLIP_T2452686242_H
#define FASTLINECLIP_T2452686242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FastLineClip
struct  FastLineClip_t2452686242  : public RuntimeObject
{
public:
	// System.Single FastLineClip::FC_xn
	float ___FC_xn_0;
	// System.Single FastLineClip::FC_yn
	float ___FC_yn_1;
	// System.Single FastLineClip::FC_xk
	float ___FC_xk_2;
	// System.Single FastLineClip::FC_yk
	float ___FC_yk_3;
	// System.Single FastLineClip::Wxlef
	float ___Wxlef_4;
	// System.Single FastLineClip::Wxrig
	float ___Wxrig_5;
	// System.Single FastLineClip::Wytop
	float ___Wytop_6;
	// System.Single FastLineClip::Wybot
	float ___Wybot_7;

public:
	inline static int32_t get_offset_of_FC_xn_0() { return static_cast<int32_t>(offsetof(FastLineClip_t2452686242, ___FC_xn_0)); }
	inline float get_FC_xn_0() const { return ___FC_xn_0; }
	inline float* get_address_of_FC_xn_0() { return &___FC_xn_0; }
	inline void set_FC_xn_0(float value)
	{
		___FC_xn_0 = value;
	}

	inline static int32_t get_offset_of_FC_yn_1() { return static_cast<int32_t>(offsetof(FastLineClip_t2452686242, ___FC_yn_1)); }
	inline float get_FC_yn_1() const { return ___FC_yn_1; }
	inline float* get_address_of_FC_yn_1() { return &___FC_yn_1; }
	inline void set_FC_yn_1(float value)
	{
		___FC_yn_1 = value;
	}

	inline static int32_t get_offset_of_FC_xk_2() { return static_cast<int32_t>(offsetof(FastLineClip_t2452686242, ___FC_xk_2)); }
	inline float get_FC_xk_2() const { return ___FC_xk_2; }
	inline float* get_address_of_FC_xk_2() { return &___FC_xk_2; }
	inline void set_FC_xk_2(float value)
	{
		___FC_xk_2 = value;
	}

	inline static int32_t get_offset_of_FC_yk_3() { return static_cast<int32_t>(offsetof(FastLineClip_t2452686242, ___FC_yk_3)); }
	inline float get_FC_yk_3() const { return ___FC_yk_3; }
	inline float* get_address_of_FC_yk_3() { return &___FC_yk_3; }
	inline void set_FC_yk_3(float value)
	{
		___FC_yk_3 = value;
	}

	inline static int32_t get_offset_of_Wxlef_4() { return static_cast<int32_t>(offsetof(FastLineClip_t2452686242, ___Wxlef_4)); }
	inline float get_Wxlef_4() const { return ___Wxlef_4; }
	inline float* get_address_of_Wxlef_4() { return &___Wxlef_4; }
	inline void set_Wxlef_4(float value)
	{
		___Wxlef_4 = value;
	}

	inline static int32_t get_offset_of_Wxrig_5() { return static_cast<int32_t>(offsetof(FastLineClip_t2452686242, ___Wxrig_5)); }
	inline float get_Wxrig_5() const { return ___Wxrig_5; }
	inline float* get_address_of_Wxrig_5() { return &___Wxrig_5; }
	inline void set_Wxrig_5(float value)
	{
		___Wxrig_5 = value;
	}

	inline static int32_t get_offset_of_Wytop_6() { return static_cast<int32_t>(offsetof(FastLineClip_t2452686242, ___Wytop_6)); }
	inline float get_Wytop_6() const { return ___Wytop_6; }
	inline float* get_address_of_Wytop_6() { return &___Wytop_6; }
	inline void set_Wytop_6(float value)
	{
		___Wytop_6 = value;
	}

	inline static int32_t get_offset_of_Wybot_7() { return static_cast<int32_t>(offsetof(FastLineClip_t2452686242, ___Wybot_7)); }
	inline float get_Wybot_7() const { return ___Wybot_7; }
	inline float* get_address_of_Wybot_7() { return &___Wybot_7; }
	inline void set_Wybot_7(float value)
	{
		___Wybot_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FASTLINECLIP_T2452686242_H
#ifndef EXTENTIONS_T847016823_H
#define EXTENTIONS_T847016823_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Extentions
struct  Extentions_t847016823  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENTIONS_T847016823_H
#ifndef U3CDESTROYWHENCOMPLETEU3EC__ITERATOR0_T3483178810_H
#define U3CDESTROYWHENCOMPLETEU3EC__ITERATOR0_T3483178810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoroutinerInstance/<DestroyWhenComplete>c__Iterator0
struct  U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810  : public RuntimeObject
{
public:
	// System.Collections.IEnumerator CoroutinerInstance/<DestroyWhenComplete>c__Iterator0::iterationResult
	RuntimeObject* ___iterationResult_0;
	// CoroutinerInstance CoroutinerInstance/<DestroyWhenComplete>c__Iterator0::$this
	CoroutinerInstance_t1991176683 * ___U24this_1;
	// System.Object CoroutinerInstance/<DestroyWhenComplete>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean CoroutinerInstance/<DestroyWhenComplete>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 CoroutinerInstance/<DestroyWhenComplete>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_iterationResult_0() { return static_cast<int32_t>(offsetof(U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810, ___iterationResult_0)); }
	inline RuntimeObject* get_iterationResult_0() const { return ___iterationResult_0; }
	inline RuntimeObject** get_address_of_iterationResult_0() { return &___iterationResult_0; }
	inline void set_iterationResult_0(RuntimeObject* value)
	{
		___iterationResult_0 = value;
		Il2CppCodeGenWriteBarrier((&___iterationResult_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810, ___U24this_1)); }
	inline CoroutinerInstance_t1991176683 * get_U24this_1() const { return ___U24this_1; }
	inline CoroutinerInstance_t1991176683 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(CoroutinerInstance_t1991176683 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDESTROYWHENCOMPLETEU3EC__ITERATOR0_T3483178810_H
#ifndef U3CGETCLIPU3EC__ANONSTOREY0_T3223970893_H
#define U3CGETCLIPU3EC__ANONSTOREY0_T3223970893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundCollection/<GetClip>c__AnonStorey0
struct  U3CGetClipU3Ec__AnonStorey0_t3223970893  : public RuntimeObject
{
public:
	// System.String SoundCollection/<GetClip>c__AnonStorey0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CGetClipU3Ec__AnonStorey0_t3223970893, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETCLIPU3EC__ANONSTOREY0_T3223970893_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CGETU3EC__ANONSTOREY1_T2426557540_H
#define U3CGETU3EC__ANONSTOREY1_T2426557540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Localization/<Get>c__AnonStorey1
struct  U3CGetU3Ec__AnonStorey1_t2426557540  : public RuntimeObject
{
public:
	// System.String Localization/<Get>c__AnonStorey1::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CGetU3Ec__AnonStorey1_t2426557540, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETU3EC__ANONSTOREY1_T2426557540_H
#ifndef U3CEMMITU3EC__ANONSTOREY0_T1718196965_H
#define U3CEMMITU3EC__ANONSTOREY0_T1718196965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlesCollection/<Emmit>c__AnonStorey0
struct  U3CEmmitU3Ec__AnonStorey0_t1718196965  : public RuntimeObject
{
public:
	// System.String ParticlesCollection/<Emmit>c__AnonStorey0::particleName
	String_t* ___particleName_0;

public:
	inline static int32_t get_offset_of_particleName_0() { return static_cast<int32_t>(offsetof(U3CEmmitU3Ec__AnonStorey0_t1718196965, ___particleName_0)); }
	inline String_t* get_particleName_0() const { return ___particleName_0; }
	inline String_t** get_address_of_particleName_0() { return &___particleName_0; }
	inline void set_particleName_0(String_t* value)
	{
		___particleName_0 = value;
		Il2CppCodeGenWriteBarrier((&___particleName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEMMITU3EC__ANONSTOREY0_T1718196965_H
#ifndef U3CGETU3EC__ANONSTOREY2_T860473599_H
#define U3CGETU3EC__ANONSTOREY2_T860473599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Localization/<Get>c__AnonStorey2
struct  U3CGetU3Ec__AnonStorey2_t860473599  : public RuntimeObject
{
public:
	// System.String Localization/<Get>c__AnonStorey2::key
	String_t* ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(U3CGetU3Ec__AnonStorey2_t860473599, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETU3EC__ANONSTOREY2_T860473599_H
#ifndef U3CLOADIMGENUMERATORU3EC__ITERATOR0_T1326606583_H
#define U3CLOADIMGENUMERATORU3EC__ITERATOR0_T1326606583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GraphUtil/<LoadImgEnumerator>c__Iterator0
struct  U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583  : public RuntimeObject
{
public:
	// System.String GraphUtil/<LoadImgEnumerator>c__Iterator0::imgURL
	String_t* ___imgURL_0;
	// UnityEngine.WWW GraphUtil/<LoadImgEnumerator>c__Iterator0::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_1;
	// System.Action`1<UnityEngine.Texture> GraphUtil/<LoadImgEnumerator>c__Iterator0::callback
	Action_1_t2045425701 * ___callback_2;
	// System.Object GraphUtil/<LoadImgEnumerator>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean GraphUtil/<LoadImgEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 GraphUtil/<LoadImgEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_imgURL_0() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583, ___imgURL_0)); }
	inline String_t* get_imgURL_0() const { return ___imgURL_0; }
	inline String_t** get_address_of_imgURL_0() { return &___imgURL_0; }
	inline void set_imgURL_0(String_t* value)
	{
		___imgURL_0 = value;
		Il2CppCodeGenWriteBarrier((&___imgURL_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583, ___U3CwwwU3E__0_1)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583, ___callback_2)); }
	inline Action_1_t2045425701 * get_callback_2() const { return ___callback_2; }
	inline Action_1_t2045425701 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_1_t2045425701 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADIMGENUMERATORU3EC__ITERATOR0_T1326606583_H
#ifndef U3CPLAYSOUNDU3EC__ANONSTOREY0_T3955832979_H
#define U3CPLAYSOUNDU3EC__ANONSTOREY0_T3955832979_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager/<PlaySound>c__AnonStorey0
struct  U3CPlaySoundU3Ec__AnonStorey0_t3955832979  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip SoundManager/<PlaySound>c__AnonStorey0::clip
	AudioClip_t1932558630 * ___clip_0;

public:
	inline static int32_t get_offset_of_clip_0() { return static_cast<int32_t>(offsetof(U3CPlaySoundU3Ec__AnonStorey0_t3955832979, ___clip_0)); }
	inline AudioClip_t1932558630 * get_clip_0() const { return ___clip_0; }
	inline AudioClip_t1932558630 ** get_address_of_clip_0() { return &___clip_0; }
	inline void set_clip_0(AudioClip_t1932558630 * value)
	{
		___clip_0 = value;
		Il2CppCodeGenWriteBarrier((&___clip_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYSOUNDU3EC__ANONSTOREY0_T3955832979_H
#ifndef PLAYER_T1147783557_H
#define PLAYER_T1147783557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t1147783557  : public RuntimeObject
{
public:
	// UnityEngine.Texture Player::Icon
	Texture_t2243626319 * ___Icon_0;
	// System.String Player::PatchIcon
	String_t* ___PatchIcon_1;
	// System.String Player::UserName
	String_t* ___UserName_2;
	// System.String Player::UserForname
	String_t* ___UserForname_3;
	// System.Boolean Player::InGame
	bool ___InGame_4;
	// System.Int32 Player::LiveCount
	int32_t ___LiveCount_5;
	// System.Int32 Player::LevelIndex
	int32_t ___LevelIndex_6;
	// System.Int32 Player::Score
	int32_t ___Score_7;
	// System.Int32 Player::WallPassLevel
	int32_t ___WallPassLevel_8;

public:
	inline static int32_t get_offset_of_Icon_0() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___Icon_0)); }
	inline Texture_t2243626319 * get_Icon_0() const { return ___Icon_0; }
	inline Texture_t2243626319 ** get_address_of_Icon_0() { return &___Icon_0; }
	inline void set_Icon_0(Texture_t2243626319 * value)
	{
		___Icon_0 = value;
		Il2CppCodeGenWriteBarrier((&___Icon_0), value);
	}

	inline static int32_t get_offset_of_PatchIcon_1() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___PatchIcon_1)); }
	inline String_t* get_PatchIcon_1() const { return ___PatchIcon_1; }
	inline String_t** get_address_of_PatchIcon_1() { return &___PatchIcon_1; }
	inline void set_PatchIcon_1(String_t* value)
	{
		___PatchIcon_1 = value;
		Il2CppCodeGenWriteBarrier((&___PatchIcon_1), value);
	}

	inline static int32_t get_offset_of_UserName_2() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___UserName_2)); }
	inline String_t* get_UserName_2() const { return ___UserName_2; }
	inline String_t** get_address_of_UserName_2() { return &___UserName_2; }
	inline void set_UserName_2(String_t* value)
	{
		___UserName_2 = value;
		Il2CppCodeGenWriteBarrier((&___UserName_2), value);
	}

	inline static int32_t get_offset_of_UserForname_3() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___UserForname_3)); }
	inline String_t* get_UserForname_3() const { return ___UserForname_3; }
	inline String_t** get_address_of_UserForname_3() { return &___UserForname_3; }
	inline void set_UserForname_3(String_t* value)
	{
		___UserForname_3 = value;
		Il2CppCodeGenWriteBarrier((&___UserForname_3), value);
	}

	inline static int32_t get_offset_of_InGame_4() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___InGame_4)); }
	inline bool get_InGame_4() const { return ___InGame_4; }
	inline bool* get_address_of_InGame_4() { return &___InGame_4; }
	inline void set_InGame_4(bool value)
	{
		___InGame_4 = value;
	}

	inline static int32_t get_offset_of_LiveCount_5() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___LiveCount_5)); }
	inline int32_t get_LiveCount_5() const { return ___LiveCount_5; }
	inline int32_t* get_address_of_LiveCount_5() { return &___LiveCount_5; }
	inline void set_LiveCount_5(int32_t value)
	{
		___LiveCount_5 = value;
	}

	inline static int32_t get_offset_of_LevelIndex_6() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___LevelIndex_6)); }
	inline int32_t get_LevelIndex_6() const { return ___LevelIndex_6; }
	inline int32_t* get_address_of_LevelIndex_6() { return &___LevelIndex_6; }
	inline void set_LevelIndex_6(int32_t value)
	{
		___LevelIndex_6 = value;
	}

	inline static int32_t get_offset_of_Score_7() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___Score_7)); }
	inline int32_t get_Score_7() const { return ___Score_7; }
	inline int32_t* get_address_of_Score_7() { return &___Score_7; }
	inline void set_Score_7(int32_t value)
	{
		___Score_7 = value;
	}

	inline static int32_t get_offset_of_WallPassLevel_8() { return static_cast<int32_t>(offsetof(Player_t1147783557, ___WallPassLevel_8)); }
	inline int32_t get_WallPassLevel_8() const { return ___WallPassLevel_8; }
	inline int32_t* get_address_of_WallPassLevel_8() { return &___WallPassLevel_8; }
	inline void set_WallPassLevel_8(int32_t value)
	{
		___WallPassLevel_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T1147783557_H
#ifndef PRODUCTSTASH_T505074360_H
#define PRODUCTSTASH_T505074360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductStash
struct  ProductStash_t505074360  : public RuntimeObject
{
public:
	// System.Int32 ProductStash::count
	int32_t ___count_0;
	// System.Collections.Generic.List`1<System.String> ProductStash::IDs
	List_1_t1398341365 * ___IDs_1;
	// System.Collections.Generic.List`1<System.String> ProductStash::receipts
	List_1_t1398341365 * ___receipts_2;
	// System.String ProductStash::noData
	String_t* ___noData_3;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(ProductStash_t505074360, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_IDs_1() { return static_cast<int32_t>(offsetof(ProductStash_t505074360, ___IDs_1)); }
	inline List_1_t1398341365 * get_IDs_1() const { return ___IDs_1; }
	inline List_1_t1398341365 ** get_address_of_IDs_1() { return &___IDs_1; }
	inline void set_IDs_1(List_1_t1398341365 * value)
	{
		___IDs_1 = value;
		Il2CppCodeGenWriteBarrier((&___IDs_1), value);
	}

	inline static int32_t get_offset_of_receipts_2() { return static_cast<int32_t>(offsetof(ProductStash_t505074360, ___receipts_2)); }
	inline List_1_t1398341365 * get_receipts_2() const { return ___receipts_2; }
	inline List_1_t1398341365 ** get_address_of_receipts_2() { return &___receipts_2; }
	inline void set_receipts_2(List_1_t1398341365 * value)
	{
		___receipts_2 = value;
		Il2CppCodeGenWriteBarrier((&___receipts_2), value);
	}

	inline static int32_t get_offset_of_noData_3() { return static_cast<int32_t>(offsetof(ProductStash_t505074360, ___noData_3)); }
	inline String_t* get_noData_3() const { return ___noData_3; }
	inline String_t** get_address_of_noData_3() { return &___noData_3; }
	inline void set_noData_3(String_t* value)
	{
		___noData_3 = value;
		Il2CppCodeGenWriteBarrier((&___noData_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTSTASH_T505074360_H
#ifndef U3CLOADICONU3EC__ANONSTOREY0_T1644789636_H
#define U3CLOADICONU3EC__ANONSTOREY0_T1644789636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player/<LoadIcon>c__AnonStorey0
struct  U3CLoadIconU3Ec__AnonStorey0_t1644789636  : public RuntimeObject
{
public:
	// System.Action`1<UnityEngine.Texture> Player/<LoadIcon>c__AnonStorey0::func
	Action_1_t2045425701 * ___func_0;
	// Player Player/<LoadIcon>c__AnonStorey0::$this
	Player_t1147783557 * ___U24this_1;

public:
	inline static int32_t get_offset_of_func_0() { return static_cast<int32_t>(offsetof(U3CLoadIconU3Ec__AnonStorey0_t1644789636, ___func_0)); }
	inline Action_1_t2045425701 * get_func_0() const { return ___func_0; }
	inline Action_1_t2045425701 ** get_address_of_func_0() { return &___func_0; }
	inline void set_func_0(Action_1_t2045425701 * value)
	{
		___func_0 = value;
		Il2CppCodeGenWriteBarrier((&___func_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CLoadIconU3Ec__AnonStorey0_t1644789636, ___U24this_1)); }
	inline Player_t1147783557 * get_U24this_1() const { return ___U24this_1; }
	inline Player_t1147783557 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Player_t1147783557 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADICONU3EC__ANONSTOREY0_T1644789636_H
#ifndef U3CU3EC__ANONSTOREY0_T4058529692_H
#define U3CU3EC__ANONSTOREY0_T4058529692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewManager/<>c__AnonStorey0
struct  U3CU3Ec__AnonStorey0_t4058529692  : public RuntimeObject
{
public:
	// IModel ViewManager/<>c__AnonStorey0::upper
	RuntimeObject* ___upper_0;

public:
	inline static int32_t get_offset_of_upper_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__AnonStorey0_t4058529692, ___upper_0)); }
	inline RuntimeObject* get_upper_0() const { return ___upper_0; }
	inline RuntimeObject** get_address_of_upper_0() { return &___upper_0; }
	inline void set_upper_0(RuntimeObject* value)
	{
		___upper_0 = value;
		Il2CppCodeGenWriteBarrier((&___upper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__ANONSTOREY0_T4058529692_H
#ifndef UNITYARUTILITY_T3608388148_H
#define UNITYARUTILITY_T3608388148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUtility
struct  UnityARUtility_t3608388148  : public RuntimeObject
{
public:
	// UnityEngine.MeshCollider UnityEngine.XR.iOS.UnityARUtility::meshCollider
	MeshCollider_t2718867283 * ___meshCollider_0;
	// UnityEngine.MeshFilter UnityEngine.XR.iOS.UnityARUtility::meshFilter
	MeshFilter_t3026937449 * ___meshFilter_1;

public:
	inline static int32_t get_offset_of_meshCollider_0() { return static_cast<int32_t>(offsetof(UnityARUtility_t3608388148, ___meshCollider_0)); }
	inline MeshCollider_t2718867283 * get_meshCollider_0() const { return ___meshCollider_0; }
	inline MeshCollider_t2718867283 ** get_address_of_meshCollider_0() { return &___meshCollider_0; }
	inline void set_meshCollider_0(MeshCollider_t2718867283 * value)
	{
		___meshCollider_0 = value;
		Il2CppCodeGenWriteBarrier((&___meshCollider_0), value);
	}

	inline static int32_t get_offset_of_meshFilter_1() { return static_cast<int32_t>(offsetof(UnityARUtility_t3608388148, ___meshFilter_1)); }
	inline MeshFilter_t3026937449 * get_meshFilter_1() const { return ___meshFilter_1; }
	inline MeshFilter_t3026937449 ** get_address_of_meshFilter_1() { return &___meshFilter_1; }
	inline void set_meshFilter_1(MeshFilter_t3026937449 * value)
	{
		___meshFilter_1 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_1), value);
	}
};

struct UnityARUtility_t3608388148_StaticFields
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARUtility::planePrefab
	GameObject_t1756533147 * ___planePrefab_2;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARUtility_t3608388148_StaticFields, ___planePrefab_2)); }
	inline GameObject_t1756533147 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t1756533147 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUTILITY_T3608388148_H
#ifndef U3CNOTIFYU3EC__ANONSTOREY0_T3711104398_H
#define U3CNOTIFYU3EC__ANONSTOREY0_T3711104398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationPanel/<Notify>c__AnonStorey0
struct  U3CNotifyU3Ec__AnonStorey0_t3711104398  : public RuntimeObject
{
public:
	// NotificationItem NotificationPanel/<Notify>c__AnonStorey0::item
	NotificationItem_t205362912 * ___item_0;

public:
	inline static int32_t get_offset_of_item_0() { return static_cast<int32_t>(offsetof(U3CNotifyU3Ec__AnonStorey0_t3711104398, ___item_0)); }
	inline NotificationItem_t205362912 * get_item_0() const { return ___item_0; }
	inline NotificationItem_t205362912 ** get_address_of_item_0() { return &___item_0; }
	inline void set_item_0(NotificationItem_t205362912 * value)
	{
		___item_0 = value;
		Il2CppCodeGenWriteBarrier((&___item_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CNOTIFYU3EC__ANONSTOREY0_T3711104398_H
#ifndef MESHEXTENSION_T3538562906_H
#define MESHEXTENSION_T3538562906_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MeshExtension
struct  MeshExtension_t3538562906  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHEXTENSION_T3538562906_H
#ifndef UNITYARMATRIXOPS_T4039665643_H
#define UNITYARMATRIXOPS_T4039665643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARMatrixOps
struct  UnityARMatrixOps_t4039665643  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARMATRIXOPS_T4039665643_H
#ifndef MATHHELPER_T2977985342_H
#define MATHHELPER_T2977985342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MathHelper
struct  MathHelper_t2977985342  : public RuntimeObject
{
public:

public:
};

struct MathHelper_t2977985342_StaticFields
{
public:
	// System.Single MathHelper::TwoPi
	float ___TwoPi_0;
	// System.Single MathHelper::PiOwer4
	float ___PiOwer4_1;

public:
	inline static int32_t get_offset_of_TwoPi_0() { return static_cast<int32_t>(offsetof(MathHelper_t2977985342_StaticFields, ___TwoPi_0)); }
	inline float get_TwoPi_0() const { return ___TwoPi_0; }
	inline float* get_address_of_TwoPi_0() { return &___TwoPi_0; }
	inline void set_TwoPi_0(float value)
	{
		___TwoPi_0 = value;
	}

	inline static int32_t get_offset_of_PiOwer4_1() { return static_cast<int32_t>(offsetof(MathHelper_t2977985342_StaticFields, ___PiOwer4_1)); }
	inline float get_PiOwer4_1() const { return ___PiOwer4_1; }
	inline float* get_address_of_PiOwer4_1() { return &___PiOwer4_1; }
	inline void set_PiOwer4_1(float value)
	{
		___PiOwer4_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHHELPER_T2977985342_H
#ifndef MATRIX4X4HELPER_T2359692911_H
#define MATRIX4X4HELPER_T2359692911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Matrix4x4Helper
struct  Matrix4x4Helper_t2359692911  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4HELPER_T2359692911_H
#ifndef UNITYARANCHORMANAGER_T1086564192_H
#define UNITYARANCHORMANAGER_T1086564192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAnchorManager
struct  UnityARAnchorManager_t1086564192  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.XR.iOS.ARPlaneAnchorGameObject> UnityEngine.XR.iOS.UnityARAnchorManager::planeAnchorMap
	Dictionary_2_t4220005149 * ___planeAnchorMap_0;

public:
	inline static int32_t get_offset_of_planeAnchorMap_0() { return static_cast<int32_t>(offsetof(UnityARAnchorManager_t1086564192, ___planeAnchorMap_0)); }
	inline Dictionary_2_t4220005149 * get_planeAnchorMap_0() const { return ___planeAnchorMap_0; }
	inline Dictionary_2_t4220005149 ** get_address_of_planeAnchorMap_0() { return &___planeAnchorMap_0; }
	inline void set_planeAnchorMap_0(Dictionary_2_t4220005149 * value)
	{
		___planeAnchorMap_0 = value;
		Il2CppCodeGenWriteBarrier((&___planeAnchorMap_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARANCHORMANAGER_T1086564192_H
#ifndef MATRIX4X4EXT_T3978015834_H
#define MATRIX4X4EXT_T3978015834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Matrix4x4Ext
struct  Matrix4x4Ext_t3978015834  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4EXT_T3978015834_H
#ifndef UTILS_T4194145797_H
#define UTILS_T4194145797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils
struct  Utils_t4194145797  : public RuntimeObject
{
public:

public:
};

struct Utils_t4194145797_StaticFields
{
public:
	// System.Security.Cryptography.MD5 Utils::_md5
	MD5_t1507972490 * ____md5_0;

public:
	inline static int32_t get_offset_of__md5_0() { return static_cast<int32_t>(offsetof(Utils_t4194145797_StaticFields, ____md5_0)); }
	inline MD5_t1507972490 * get__md5_0() const { return ____md5_0; }
	inline MD5_t1507972490 ** get_address_of__md5_0() { return &____md5_0; }
	inline void set__md5_0(MD5_t1507972490 * value)
	{
		____md5_0 = value;
		Il2CppCodeGenWriteBarrier((&____md5_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILS_T4194145797_H
#ifndef U3CREMOVENOTIFYU3EC__ANONSTOREY1_T2304509167_H
#define U3CREMOVENOTIFYU3EC__ANONSTOREY1_T2304509167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationPanel/<RemoveNotify>c__AnonStorey1
struct  U3CRemoveNotifyU3Ec__AnonStorey1_t2304509167  : public RuntimeObject
{
public:
	// NotificationItem NotificationPanel/<RemoveNotify>c__AnonStorey1::notify
	NotificationItem_t205362912 * ___notify_0;

public:
	inline static int32_t get_offset_of_notify_0() { return static_cast<int32_t>(offsetof(U3CRemoveNotifyU3Ec__AnonStorey1_t2304509167, ___notify_0)); }
	inline NotificationItem_t205362912 * get_notify_0() const { return ___notify_0; }
	inline NotificationItem_t205362912 ** get_address_of_notify_0() { return &___notify_0; }
	inline void set_notify_0(NotificationItem_t205362912 * value)
	{
		___notify_0 = value;
		Il2CppCodeGenWriteBarrier((&___notify_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CREMOVENOTIFYU3EC__ANONSTOREY1_T2304509167_H
#ifndef SPRITECOLLECTION_T2762286977_H
#define SPRITECOLLECTION_T2762286977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpriteCollection
struct  SpriteCollection_t2762286977  : public RuntimeObject
{
public:
	// UnityEngine.Sprite[] SpriteCollection::_sprites
	SpriteU5BU5D_t3359083662* ____sprites_0;
	// System.String[] SpriteCollection::_names
	StringU5BU5D_t1642385972* ____names_1;

public:
	inline static int32_t get_offset_of__sprites_0() { return static_cast<int32_t>(offsetof(SpriteCollection_t2762286977, ____sprites_0)); }
	inline SpriteU5BU5D_t3359083662* get__sprites_0() const { return ____sprites_0; }
	inline SpriteU5BU5D_t3359083662** get_address_of__sprites_0() { return &____sprites_0; }
	inline void set__sprites_0(SpriteU5BU5D_t3359083662* value)
	{
		____sprites_0 = value;
		Il2CppCodeGenWriteBarrier((&____sprites_0), value);
	}

	inline static int32_t get_offset_of__names_1() { return static_cast<int32_t>(offsetof(SpriteCollection_t2762286977, ____names_1)); }
	inline StringU5BU5D_t1642385972* get__names_1() const { return ____names_1; }
	inline StringU5BU5D_t1642385972** get_address_of__names_1() { return &____names_1; }
	inline void set__names_1(StringU5BU5D_t1642385972* value)
	{
		____names_1 = value;
		Il2CppCodeGenWriteBarrier((&____names_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITECOLLECTION_T2762286977_H
#ifndef DRAWING_T724524520_H
#define DRAWING_T724524520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Utils/Drawing
struct  Drawing_t724524520  : public RuntimeObject
{
public:

public:
};

struct Drawing_t724524520_StaticFields
{
public:
	// UnityEngine.Texture2D Utils/Drawing::lineTex
	Texture2D_t3542995729 * ___lineTex_0;

public:
	inline static int32_t get_offset_of_lineTex_0() { return static_cast<int32_t>(offsetof(Drawing_t724524520_StaticFields, ___lineTex_0)); }
	inline Texture2D_t3542995729 * get_lineTex_0() const { return ___lineTex_0; }
	inline Texture2D_t3542995729 ** get_address_of_lineTex_0() { return &___lineTex_0; }
	inline void set_lineTex_0(Texture2D_t3542995729 * value)
	{
		___lineTex_0 = value;
		Il2CppCodeGenWriteBarrier((&___lineTex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWING_T724524520_H
#ifndef EDGE_T3361754850_H
#define EDGE_T3361754850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EdgeHelpers/Edge
struct  Edge_t3361754850 
{
public:
	// System.Int32 EdgeHelpers/Edge::v1
	int32_t ___v1_0;
	// System.Int32 EdgeHelpers/Edge::v2
	int32_t ___v2_1;
	// System.Int32 EdgeHelpers/Edge::triangleIndex
	int32_t ___triangleIndex_2;

public:
	inline static int32_t get_offset_of_v1_0() { return static_cast<int32_t>(offsetof(Edge_t3361754850, ___v1_0)); }
	inline int32_t get_v1_0() const { return ___v1_0; }
	inline int32_t* get_address_of_v1_0() { return &___v1_0; }
	inline void set_v1_0(int32_t value)
	{
		___v1_0 = value;
	}

	inline static int32_t get_offset_of_v2_1() { return static_cast<int32_t>(offsetof(Edge_t3361754850, ___v2_1)); }
	inline int32_t get_v2_1() const { return ___v2_1; }
	inline int32_t* get_address_of_v2_1() { return &___v2_1; }
	inline void set_v2_1(int32_t value)
	{
		___v2_1 = value;
	}

	inline static int32_t get_offset_of_triangleIndex_2() { return static_cast<int32_t>(offsetof(Edge_t3361754850, ___triangleIndex_2)); }
	inline int32_t get_triangleIndex_2() const { return ___triangleIndex_2; }
	inline int32_t* get_address_of_triangleIndex_2() { return &___triangleIndex_2; }
	inline void set_triangleIndex_2(int32_t value)
	{
		___triangleIndex_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGE_T3361754850_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef WEIGHTEDOPTIONS_T481347131_H
#define WEIGHTEDOPTIONS_T481347131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WeightedOptions
struct  WeightedOptions_t481347131  : public PairStringIntDictionary_t1323851614
{
public:
	// System.Int32 WeightedOptions::_totalWeight
	int32_t ____totalWeight_2;

public:
	inline static int32_t get_offset_of__totalWeight_2() { return static_cast<int32_t>(offsetof(WeightedOptions_t481347131, ____totalWeight_2)); }
	inline int32_t get__totalWeight_2() const { return ____totalWeight_2; }
	inline int32_t* get_address_of__totalWeight_2() { return &____totalWeight_2; }
	inline void set__totalWeight_2(int32_t value)
	{
		____totalWeight_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEIGHTEDOPTIONS_T481347131_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef RANGE_T624945109_H
#define RANGE_T624945109_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Range
struct  Range_t624945109 
{
public:
	// System.Single Range::Minimum
	float ___Minimum_0;
	// System.Single Range::Maximum
	float ___Maximum_1;

public:
	inline static int32_t get_offset_of_Minimum_0() { return static_cast<int32_t>(offsetof(Range_t624945109, ___Minimum_0)); }
	inline float get_Minimum_0() const { return ___Minimum_0; }
	inline float* get_address_of_Minimum_0() { return &___Minimum_0; }
	inline void set_Minimum_0(float value)
	{
		___Minimum_0 = value;
	}

	inline static int32_t get_offset_of_Maximum_1() { return static_cast<int32_t>(offsetof(Range_t624945109, ___Maximum_1)); }
	inline float get_Maximum_1() const { return ___Maximum_1; }
	inline float* get_address_of_Maximum_1() { return &___Maximum_1; }
	inline void set_Maximum_1(float value)
	{
		___Maximum_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGE_T624945109_H
#ifndef OPTIONTYPE_T243031973_H
#define OPTIONTYPE_T243031973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OptionType
struct  OptionType_t243031973 
{
public:
	// System.Int32 OptionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OptionType_t243031973, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONTYPE_T243031973_H
#ifndef DIALOGRESULTTYPE_T3218470507_H
#define DIALOGRESULTTYPE_T3218470507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DialogResultType
struct  DialogResultType_t3218470507 
{
public:
	// System.Int32 DialogResultType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DialogResultType_t3218470507, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGRESULTTYPE_T3218470507_H
#ifndef MINIMAPPOINT_T368923569_H
#define MINIMAPPOINT_T368923569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniMap/MiniMapPoint
struct  MiniMapPoint_t368923569  : public RuntimeObject
{
public:
	// UnityEngine.Transform MiniMap/MiniMapPoint::Transform
	Transform_t3275118058 * ___Transform_0;
	// System.Single MiniMap/MiniMapPoint::Size
	float ___Size_1;
	// UnityEngine.Color MiniMap/MiniMapPoint::Color
	Color_t2020392075  ___Color_2;

public:
	inline static int32_t get_offset_of_Transform_0() { return static_cast<int32_t>(offsetof(MiniMapPoint_t368923569, ___Transform_0)); }
	inline Transform_t3275118058 * get_Transform_0() const { return ___Transform_0; }
	inline Transform_t3275118058 ** get_address_of_Transform_0() { return &___Transform_0; }
	inline void set_Transform_0(Transform_t3275118058 * value)
	{
		___Transform_0 = value;
		Il2CppCodeGenWriteBarrier((&___Transform_0), value);
	}

	inline static int32_t get_offset_of_Size_1() { return static_cast<int32_t>(offsetof(MiniMapPoint_t368923569, ___Size_1)); }
	inline float get_Size_1() const { return ___Size_1; }
	inline float* get_address_of_Size_1() { return &___Size_1; }
	inline void set_Size_1(float value)
	{
		___Size_1 = value;
	}

	inline static int32_t get_offset_of_Color_2() { return static_cast<int32_t>(offsetof(MiniMapPoint_t368923569, ___Color_2)); }
	inline Color_t2020392075  get_Color_2() const { return ___Color_2; }
	inline Color_t2020392075 * get_address_of_Color_2() { return &___Color_2; }
	inline void set_Color_2(Color_t2020392075  value)
	{
		___Color_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMAPPOINT_T368923569_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef GENDER_T14695945_H
#define GENDER_T14695945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.Gender
struct  Gender_t14695945 
{
public:
	// System.Int32 UnityEngine.Analytics.Gender::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Gender_t14695945, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENDER_T14695945_H
#ifndef AUDIOGROUPTYPES_T423657466_H
#define AUDIOGROUPTYPES_T423657466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioGroupTypes
struct  AudioGroupTypes_t423657466 
{
public:
	// System.Int32 AudioGroupTypes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AudioGroupTypes_t423657466, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUDIOGROUPTYPES_T423657466_H
#ifndef SYSTEMLANGUAGE_T2428294669_H
#define SYSTEMLANGUAGE_T2428294669_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SystemLanguage
struct  SystemLanguage_t2428294669 
{
public:
	// System.Int32 UnityEngine.SystemLanguage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SystemLanguage_t2428294669, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMLANGUAGE_T2428294669_H
#ifndef KEYCODE_T2283395152_H
#define KEYCODE_T2283395152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_t2283395152 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KeyCode_t2283395152, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_T2283395152_H
#ifndef THELOOPBEHAVIOUR_T801943279_H
#define THELOOPBEHAVIOUR_T801943279_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatedSprite/theLoopBehaviour
struct  theLoopBehaviour_t801943279 
{
public:
	// System.Int32 AnimatedSprite/theLoopBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(theLoopBehaviour_t801943279, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THELOOPBEHAVIOUR_T801943279_H
#ifndef OPTIONINFO_T3879499943_H
#define OPTIONINFO_T3879499943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OptionInfo
struct  OptionInfo_t3879499943  : public RuntimeObject
{
public:
	// System.String OptionInfo::_id
	String_t* ____id_0;
	// System.String OptionInfo::_desc
	String_t* ____desc_1;
	// System.String OptionInfo::_cat
	String_t* ____cat_2;
	// OptionType OptionInfo::_type
	int32_t ____type_3;
	// System.Boolean OptionInfo::b_ref
	bool ___b_ref_4;
	// System.Int32 OptionInfo::i_ref
	int32_t ___i_ref_5;
	// System.String OptionInfo::s_ref
	String_t* ___s_ref_6;
	// UnityEngine.KeyCode OptionInfo::k_ref
	int32_t ___k_ref_7;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(OptionInfo_t3879499943, ____id_0)); }
	inline String_t* get__id_0() const { return ____id_0; }
	inline String_t** get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(String_t* value)
	{
		____id_0 = value;
		Il2CppCodeGenWriteBarrier((&____id_0), value);
	}

	inline static int32_t get_offset_of__desc_1() { return static_cast<int32_t>(offsetof(OptionInfo_t3879499943, ____desc_1)); }
	inline String_t* get__desc_1() const { return ____desc_1; }
	inline String_t** get_address_of__desc_1() { return &____desc_1; }
	inline void set__desc_1(String_t* value)
	{
		____desc_1 = value;
		Il2CppCodeGenWriteBarrier((&____desc_1), value);
	}

	inline static int32_t get_offset_of__cat_2() { return static_cast<int32_t>(offsetof(OptionInfo_t3879499943, ____cat_2)); }
	inline String_t* get__cat_2() const { return ____cat_2; }
	inline String_t** get_address_of__cat_2() { return &____cat_2; }
	inline void set__cat_2(String_t* value)
	{
		____cat_2 = value;
		Il2CppCodeGenWriteBarrier((&____cat_2), value);
	}

	inline static int32_t get_offset_of__type_3() { return static_cast<int32_t>(offsetof(OptionInfo_t3879499943, ____type_3)); }
	inline int32_t get__type_3() const { return ____type_3; }
	inline int32_t* get_address_of__type_3() { return &____type_3; }
	inline void set__type_3(int32_t value)
	{
		____type_3 = value;
	}

	inline static int32_t get_offset_of_b_ref_4() { return static_cast<int32_t>(offsetof(OptionInfo_t3879499943, ___b_ref_4)); }
	inline bool get_b_ref_4() const { return ___b_ref_4; }
	inline bool* get_address_of_b_ref_4() { return &___b_ref_4; }
	inline void set_b_ref_4(bool value)
	{
		___b_ref_4 = value;
	}

	inline static int32_t get_offset_of_i_ref_5() { return static_cast<int32_t>(offsetof(OptionInfo_t3879499943, ___i_ref_5)); }
	inline int32_t get_i_ref_5() const { return ___i_ref_5; }
	inline int32_t* get_address_of_i_ref_5() { return &___i_ref_5; }
	inline void set_i_ref_5(int32_t value)
	{
		___i_ref_5 = value;
	}

	inline static int32_t get_offset_of_s_ref_6() { return static_cast<int32_t>(offsetof(OptionInfo_t3879499943, ___s_ref_6)); }
	inline String_t* get_s_ref_6() const { return ___s_ref_6; }
	inline String_t** get_address_of_s_ref_6() { return &___s_ref_6; }
	inline void set_s_ref_6(String_t* value)
	{
		___s_ref_6 = value;
		Il2CppCodeGenWriteBarrier((&___s_ref_6), value);
	}

	inline static int32_t get_offset_of_k_ref_7() { return static_cast<int32_t>(offsetof(OptionInfo_t3879499943, ___k_ref_7)); }
	inline int32_t get_k_ref_7() const { return ___k_ref_7; }
	inline int32_t* get_address_of_k_ref_7() { return &___k_ref_7; }
	inline void set_k_ref_7(int32_t value)
	{
		___k_ref_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPTIONINFO_T3879499943_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef U3CSETLANGUAGEU3EC__ANONSTOREY0_T266885935_H
#define U3CSETLANGUAGEU3EC__ANONSTOREY0_T266885935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Localization/<SetLanguage>c__AnonStorey0
struct  U3CSetLanguageU3Ec__AnonStorey0_t266885935  : public RuntimeObject
{
public:
	// UnityEngine.SystemLanguage Localization/<SetLanguage>c__AnonStorey0::lang
	int32_t ___lang_0;

public:
	inline static int32_t get_offset_of_lang_0() { return static_cast<int32_t>(offsetof(U3CSetLanguageU3Ec__AnonStorey0_t266885935, ___lang_0)); }
	inline int32_t get_lang_0() const { return ___lang_0; }
	inline int32_t* get_address_of_lang_0() { return &___lang_0; }
	inline void set_lang_0(int32_t value)
	{
		___lang_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETLANGUAGEU3EC__ANONSTOREY0_T266885935_H
#ifndef LOCALIZATIONLANGUAGEDATA_T2789729839_H
#define LOCALIZATIONLANGUAGEDATA_T2789729839_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalizationLanguageData
struct  LocalizationLanguageData_t2789729839  : public RuntimeObject
{
public:
	// UnityEngine.SystemLanguage LocalizationLanguageData::Lang
	int32_t ___Lang_0;
	// System.Collections.Generic.List`1<System.String> LocalizationLanguageData::Value
	List_1_t1398341365 * ___Value_1;

public:
	inline static int32_t get_offset_of_Lang_0() { return static_cast<int32_t>(offsetof(LocalizationLanguageData_t2789729839, ___Lang_0)); }
	inline int32_t get_Lang_0() const { return ___Lang_0; }
	inline int32_t* get_address_of_Lang_0() { return &___Lang_0; }
	inline void set_Lang_0(int32_t value)
	{
		___Lang_0 = value;
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(LocalizationLanguageData_t2789729839, ___Value_1)); }
	inline List_1_t1398341365 * get_Value_1() const { return ___Value_1; }
	inline List_1_t1398341365 ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(List_1_t1398341365 * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZATIONLANGUAGEDATA_T2789729839_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef ANALYTICSCONTROLLER_T3574332020_H
#define ANALYTICSCONTROLLER_T3574332020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnalyticsController
struct  AnalyticsController_t3574332020  : public RuntimeObject
{
public:
	// System.Boolean AnalyticsController::isValidState
	bool ___isValidState_0;
	// System.Double AnalyticsController::sessionTime
	double ___sessionTime_1;
	// System.Int32 AnalyticsController::sessionStartedWithLevelIndex
	int32_t ___sessionStartedWithLevelIndex_2;
	// UnityEngine.Analytics.Gender AnalyticsController::<UserGender>k__BackingField
	int32_t ___U3CUserGenderU3Ek__BackingField_5;
	// System.Int32 AnalyticsController::<UserAge>k__BackingField
	int32_t ___U3CUserAgeU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_isValidState_0() { return static_cast<int32_t>(offsetof(AnalyticsController_t3574332020, ___isValidState_0)); }
	inline bool get_isValidState_0() const { return ___isValidState_0; }
	inline bool* get_address_of_isValidState_0() { return &___isValidState_0; }
	inline void set_isValidState_0(bool value)
	{
		___isValidState_0 = value;
	}

	inline static int32_t get_offset_of_sessionTime_1() { return static_cast<int32_t>(offsetof(AnalyticsController_t3574332020, ___sessionTime_1)); }
	inline double get_sessionTime_1() const { return ___sessionTime_1; }
	inline double* get_address_of_sessionTime_1() { return &___sessionTime_1; }
	inline void set_sessionTime_1(double value)
	{
		___sessionTime_1 = value;
	}

	inline static int32_t get_offset_of_sessionStartedWithLevelIndex_2() { return static_cast<int32_t>(offsetof(AnalyticsController_t3574332020, ___sessionStartedWithLevelIndex_2)); }
	inline int32_t get_sessionStartedWithLevelIndex_2() const { return ___sessionStartedWithLevelIndex_2; }
	inline int32_t* get_address_of_sessionStartedWithLevelIndex_2() { return &___sessionStartedWithLevelIndex_2; }
	inline void set_sessionStartedWithLevelIndex_2(int32_t value)
	{
		___sessionStartedWithLevelIndex_2 = value;
	}

	inline static int32_t get_offset_of_U3CUserGenderU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AnalyticsController_t3574332020, ___U3CUserGenderU3Ek__BackingField_5)); }
	inline int32_t get_U3CUserGenderU3Ek__BackingField_5() const { return ___U3CUserGenderU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CUserGenderU3Ek__BackingField_5() { return &___U3CUserGenderU3Ek__BackingField_5; }
	inline void set_U3CUserGenderU3Ek__BackingField_5(int32_t value)
	{
		___U3CUserGenderU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CUserAgeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AnalyticsController_t3574332020, ___U3CUserAgeU3Ek__BackingField_6)); }
	inline int32_t get_U3CUserAgeU3Ek__BackingField_6() const { return ___U3CUserAgeU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CUserAgeU3Ek__BackingField_6() { return &___U3CUserAgeU3Ek__BackingField_6; }
	inline void set_U3CUserAgeU3Ek__BackingField_6(int32_t value)
	{
		___U3CUserAgeU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSCONTROLLER_T3574332020_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef GRAPHUTIL_T2142534788_H
#define GRAPHUTIL_T2142534788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GraphUtil
struct  GraphUtil_t2142534788  : public ScriptableObject_t1975622470
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHUTIL_T2142534788_H
#ifndef SOUNDCOLLECTION_T2692967059_H
#define SOUNDCOLLECTION_T2692967059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundCollection
struct  SoundCollection_t2692967059  : public ScriptableObject_t1975622470
{
public:
	// System.Collections.Generic.List`1<PairStringAudio> SoundCollection::_clips
	List_1_t3658848735 * ____clips_2;

public:
	inline static int32_t get_offset_of__clips_2() { return static_cast<int32_t>(offsetof(SoundCollection_t2692967059, ____clips_2)); }
	inline List_1_t3658848735 * get__clips_2() const { return ____clips_2; }
	inline List_1_t3658848735 ** get_address_of__clips_2() { return &____clips_2; }
	inline void set__clips_2(List_1_t3658848735 * value)
	{
		____clips_2 = value;
		Il2CppCodeGenWriteBarrier((&____clips_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDCOLLECTION_T2692967059_H
#ifndef ADEVENT_T127799521_H
#define ADEVENT_T127799521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADS.ADHost/ADEvent
struct  ADEvent_t127799521  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADEVENT_T127799521_H
#ifndef LOCALIZATION_T3725902693_H
#define LOCALIZATION_T3725902693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Localization
struct  Localization_t3725902693  : public ScriptableObject_t1975622470
{
public:
	// System.Collections.Generic.List`1<LocalizationLanguageData> Localization::Data
	List_1_t2158850971 * ___Data_2;
	// System.Collections.Generic.List`1<System.String> Localization::Keys
	List_1_t1398341365 * ___Keys_3;
	// LocalizationLanguageData Localization::currentLanguageData
	LocalizationLanguageData_t2789729839 * ___currentLanguageData_4;

public:
	inline static int32_t get_offset_of_Data_2() { return static_cast<int32_t>(offsetof(Localization_t3725902693, ___Data_2)); }
	inline List_1_t2158850971 * get_Data_2() const { return ___Data_2; }
	inline List_1_t2158850971 ** get_address_of_Data_2() { return &___Data_2; }
	inline void set_Data_2(List_1_t2158850971 * value)
	{
		___Data_2 = value;
		Il2CppCodeGenWriteBarrier((&___Data_2), value);
	}

	inline static int32_t get_offset_of_Keys_3() { return static_cast<int32_t>(offsetof(Localization_t3725902693, ___Keys_3)); }
	inline List_1_t1398341365 * get_Keys_3() const { return ___Keys_3; }
	inline List_1_t1398341365 ** get_address_of_Keys_3() { return &___Keys_3; }
	inline void set_Keys_3(List_1_t1398341365 * value)
	{
		___Keys_3 = value;
		Il2CppCodeGenWriteBarrier((&___Keys_3), value);
	}

	inline static int32_t get_offset_of_currentLanguageData_4() { return static_cast<int32_t>(offsetof(Localization_t3725902693, ___currentLanguageData_4)); }
	inline LocalizationLanguageData_t2789729839 * get_currentLanguageData_4() const { return ___currentLanguageData_4; }
	inline LocalizationLanguageData_t2789729839 ** get_address_of_currentLanguageData_4() { return &___currentLanguageData_4; }
	inline void set_currentLanguageData_4(LocalizationLanguageData_t2789729839 * value)
	{
		___currentLanguageData_4 = value;
		Il2CppCodeGenWriteBarrier((&___currentLanguageData_4), value);
	}
};

struct Localization_t3725902693_StaticFields
{
public:
	// Localization Localization::_instance
	Localization_t3725902693 * ____instance_5;
	// System.Predicate`1<LocalizationLanguageData> Localization::<>f__am$cache0
	Predicate_1_t1232699954 * ___U3CU3Ef__amU24cache0_6;

public:
	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(Localization_t3725902693_StaticFields, ____instance_5)); }
	inline Localization_t3725902693 * get__instance_5() const { return ____instance_5; }
	inline Localization_t3725902693 ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(Localization_t3725902693 * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier((&____instance_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_6() { return static_cast<int32_t>(offsetof(Localization_t3725902693_StaticFields, ___U3CU3Ef__amU24cache0_6)); }
	inline Predicate_1_t1232699954 * get_U3CU3Ef__amU24cache0_6() const { return ___U3CU3Ef__amU24cache0_6; }
	inline Predicate_1_t1232699954 ** get_address_of_U3CU3Ef__amU24cache0_6() { return &___U3CU3Ef__amU24cache0_6; }
	inline void set_U3CU3Ef__amU24cache0_6(Predicate_1_t1232699954 * value)
	{
		___U3CU3Ef__amU24cache0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCALIZATION_T3725902693_H
#ifndef CURSORDESC_T4292080425_H
#define CURSORDESC_T4292080425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CursorDesc
struct  CursorDesc_t4292080425  : public ScriptableObject_t1975622470
{
public:
	// UnityEngine.Texture2D CursorDesc::Texture
	Texture2D_t3542995729 * ___Texture_2;
	// UnityEngine.Vector2 CursorDesc::Offset
	Vector2_t2243707579  ___Offset_3;

public:
	inline static int32_t get_offset_of_Texture_2() { return static_cast<int32_t>(offsetof(CursorDesc_t4292080425, ___Texture_2)); }
	inline Texture2D_t3542995729 * get_Texture_2() const { return ___Texture_2; }
	inline Texture2D_t3542995729 ** get_address_of_Texture_2() { return &___Texture_2; }
	inline void set_Texture_2(Texture2D_t3542995729 * value)
	{
		___Texture_2 = value;
		Il2CppCodeGenWriteBarrier((&___Texture_2), value);
	}

	inline static int32_t get_offset_of_Offset_3() { return static_cast<int32_t>(offsetof(CursorDesc_t4292080425, ___Offset_3)); }
	inline Vector2_t2243707579  get_Offset_3() const { return ___Offset_3; }
	inline Vector2_t2243707579 * get_address_of_Offset_3() { return &___Offset_3; }
	inline void set_Offset_3(Vector2_t2243707579  value)
	{
		___Offset_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURSORDESC_T4292080425_H
#ifndef PARTICLESCOLLECTION_T658639267_H
#define PARTICLESCOLLECTION_T658639267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticlesCollection
struct  ParticlesCollection_t658639267  : public ScriptableObject_t1975622470
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ParticlesCollection::_effects
	List_1_t1125654279 * ____effects_2;

public:
	inline static int32_t get_offset_of__effects_2() { return static_cast<int32_t>(offsetof(ParticlesCollection_t658639267, ____effects_2)); }
	inline List_1_t1125654279 * get__effects_2() const { return ____effects_2; }
	inline List_1_t1125654279 ** get_address_of__effects_2() { return &____effects_2; }
	inline void set__effects_2(List_1_t1125654279 * value)
	{
		____effects_2 = value;
		Il2CppCodeGenWriteBarrier((&____effects_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESCOLLECTION_T658639267_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef PARTICLEALPHACORRECT_T1759117296_H
#define PARTICLEALPHACORRECT_T1759117296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleAlphaCorrect
struct  ParticleAlphaCorrect_t1759117296  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Renderer ParticleAlphaCorrect::_renderer
	Renderer_t257310565 * ____renderer_2;
	// UnityEngine.ParticleSystem ParticleAlphaCorrect::_particleSystem
	ParticleSystem_t3394631041 * ____particleSystem_3;

public:
	inline static int32_t get_offset_of__renderer_2() { return static_cast<int32_t>(offsetof(ParticleAlphaCorrect_t1759117296, ____renderer_2)); }
	inline Renderer_t257310565 * get__renderer_2() const { return ____renderer_2; }
	inline Renderer_t257310565 ** get_address_of__renderer_2() { return &____renderer_2; }
	inline void set__renderer_2(Renderer_t257310565 * value)
	{
		____renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&____renderer_2), value);
	}

	inline static int32_t get_offset_of__particleSystem_3() { return static_cast<int32_t>(offsetof(ParticleAlphaCorrect_t1759117296, ____particleSystem_3)); }
	inline ParticleSystem_t3394631041 * get__particleSystem_3() const { return ____particleSystem_3; }
	inline ParticleSystem_t3394631041 ** get_address_of__particleSystem_3() { return &____particleSystem_3; }
	inline void set__particleSystem_3(ParticleSystem_t3394631041 * value)
	{
		____particleSystem_3 = value;
		Il2CppCodeGenWriteBarrier((&____particleSystem_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEALPHACORRECT_T1759117296_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef U421HASEMOVER_T4119473191_H
#define U421HASEMOVER_T4119473191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// СhaseMover
struct  U421haseMover_t4119473191  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform СhaseMover::_destination
	Transform_t3275118058 * ____destination_2;
	// System.Single СhaseMover::_speed
	float ____speed_3;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.Transform> СhaseMover::OnCatch
	Action_2_t2601038636 * ___OnCatch_4;

public:
	inline static int32_t get_offset_of__destination_2() { return static_cast<int32_t>(offsetof(U421haseMover_t4119473191, ____destination_2)); }
	inline Transform_t3275118058 * get__destination_2() const { return ____destination_2; }
	inline Transform_t3275118058 ** get_address_of__destination_2() { return &____destination_2; }
	inline void set__destination_2(Transform_t3275118058 * value)
	{
		____destination_2 = value;
		Il2CppCodeGenWriteBarrier((&____destination_2), value);
	}

	inline static int32_t get_offset_of__speed_3() { return static_cast<int32_t>(offsetof(U421haseMover_t4119473191, ____speed_3)); }
	inline float get__speed_3() const { return ____speed_3; }
	inline float* get_address_of__speed_3() { return &____speed_3; }
	inline void set__speed_3(float value)
	{
		____speed_3 = value;
	}

	inline static int32_t get_offset_of_OnCatch_4() { return static_cast<int32_t>(offsetof(U421haseMover_t4119473191, ___OnCatch_4)); }
	inline Action_2_t2601038636 * get_OnCatch_4() const { return ___OnCatch_4; }
	inline Action_2_t2601038636 ** get_address_of_OnCatch_4() { return &___OnCatch_4; }
	inline void set_OnCatch_4(Action_2_t2601038636 * value)
	{
		___OnCatch_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnCatch_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U421HASEMOVER_T4119473191_H
#ifndef CAMERAFOLLOWTARGET_T514838119_H
#define CAMERAFOLLOWTARGET_T514838119_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFollowTarget
struct  CameraFollowTarget_t514838119  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Vector3 CameraFollowTarget::Offset
	Vector3_t2243707580  ___Offset_2;
	// UnityEngine.Vector3 CameraFollowTarget::Rotation
	Vector3_t2243707580  ___Rotation_3;
	// System.Single CameraFollowTarget::PositionSoftness
	float ___PositionSoftness_4;
	// System.Single CameraFollowTarget::RotationSoftness
	float ___RotationSoftness_5;

public:
	inline static int32_t get_offset_of_Offset_2() { return static_cast<int32_t>(offsetof(CameraFollowTarget_t514838119, ___Offset_2)); }
	inline Vector3_t2243707580  get_Offset_2() const { return ___Offset_2; }
	inline Vector3_t2243707580 * get_address_of_Offset_2() { return &___Offset_2; }
	inline void set_Offset_2(Vector3_t2243707580  value)
	{
		___Offset_2 = value;
	}

	inline static int32_t get_offset_of_Rotation_3() { return static_cast<int32_t>(offsetof(CameraFollowTarget_t514838119, ___Rotation_3)); }
	inline Vector3_t2243707580  get_Rotation_3() const { return ___Rotation_3; }
	inline Vector3_t2243707580 * get_address_of_Rotation_3() { return &___Rotation_3; }
	inline void set_Rotation_3(Vector3_t2243707580  value)
	{
		___Rotation_3 = value;
	}

	inline static int32_t get_offset_of_PositionSoftness_4() { return static_cast<int32_t>(offsetof(CameraFollowTarget_t514838119, ___PositionSoftness_4)); }
	inline float get_PositionSoftness_4() const { return ___PositionSoftness_4; }
	inline float* get_address_of_PositionSoftness_4() { return &___PositionSoftness_4; }
	inline void set_PositionSoftness_4(float value)
	{
		___PositionSoftness_4 = value;
	}

	inline static int32_t get_offset_of_RotationSoftness_5() { return static_cast<int32_t>(offsetof(CameraFollowTarget_t514838119, ___RotationSoftness_5)); }
	inline float get_RotationSoftness_5() const { return ___RotationSoftness_5; }
	inline float* get_address_of_RotationSoftness_5() { return &___RotationSoftness_5; }
	inline void set_RotationSoftness_5(float value)
	{
		___RotationSoftness_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAFOLLOWTARGET_T514838119_H
#ifndef ANIMATEDSPRITE_T2222380888_H
#define ANIMATEDSPRITE_T2222380888_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AnimatedSprite
struct  AnimatedSprite_t2222380888  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material AnimatedSprite::spriteMaterial
	Material_t193706927 * ___spriteMaterial_2;
	// UnityEngine.Font[] AnimatedSprite::animations
	FontU5BU5D_t4130692050* ___animations_3;
	// System.String[] AnimatedSprite::animNames
	StringU5BU5D_t1642385972* ___animNames_4;
	// System.Single AnimatedSprite::lastAnimFrameTime
	float ___lastAnimFrameTime_5;
	// System.Int32 AnimatedSprite::frame
	int32_t ___frame_6;
	// System.Int32 AnimatedSprite::frameStep
	int32_t ___frameStep_7;
	// System.Int32 AnimatedSprite::anim
	int32_t ___anim_8;
	// UnityEngine.TextMesh AnimatedSprite::TM
	TextMesh_t1641806576 * ___TM_9;
	// UnityEngine.Renderer AnimatedSprite::TMR
	Renderer_t257310565 * ___TMR_10;

public:
	inline static int32_t get_offset_of_spriteMaterial_2() { return static_cast<int32_t>(offsetof(AnimatedSprite_t2222380888, ___spriteMaterial_2)); }
	inline Material_t193706927 * get_spriteMaterial_2() const { return ___spriteMaterial_2; }
	inline Material_t193706927 ** get_address_of_spriteMaterial_2() { return &___spriteMaterial_2; }
	inline void set_spriteMaterial_2(Material_t193706927 * value)
	{
		___spriteMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___spriteMaterial_2), value);
	}

	inline static int32_t get_offset_of_animations_3() { return static_cast<int32_t>(offsetof(AnimatedSprite_t2222380888, ___animations_3)); }
	inline FontU5BU5D_t4130692050* get_animations_3() const { return ___animations_3; }
	inline FontU5BU5D_t4130692050** get_address_of_animations_3() { return &___animations_3; }
	inline void set_animations_3(FontU5BU5D_t4130692050* value)
	{
		___animations_3 = value;
		Il2CppCodeGenWriteBarrier((&___animations_3), value);
	}

	inline static int32_t get_offset_of_animNames_4() { return static_cast<int32_t>(offsetof(AnimatedSprite_t2222380888, ___animNames_4)); }
	inline StringU5BU5D_t1642385972* get_animNames_4() const { return ___animNames_4; }
	inline StringU5BU5D_t1642385972** get_address_of_animNames_4() { return &___animNames_4; }
	inline void set_animNames_4(StringU5BU5D_t1642385972* value)
	{
		___animNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___animNames_4), value);
	}

	inline static int32_t get_offset_of_lastAnimFrameTime_5() { return static_cast<int32_t>(offsetof(AnimatedSprite_t2222380888, ___lastAnimFrameTime_5)); }
	inline float get_lastAnimFrameTime_5() const { return ___lastAnimFrameTime_5; }
	inline float* get_address_of_lastAnimFrameTime_5() { return &___lastAnimFrameTime_5; }
	inline void set_lastAnimFrameTime_5(float value)
	{
		___lastAnimFrameTime_5 = value;
	}

	inline static int32_t get_offset_of_frame_6() { return static_cast<int32_t>(offsetof(AnimatedSprite_t2222380888, ___frame_6)); }
	inline int32_t get_frame_6() const { return ___frame_6; }
	inline int32_t* get_address_of_frame_6() { return &___frame_6; }
	inline void set_frame_6(int32_t value)
	{
		___frame_6 = value;
	}

	inline static int32_t get_offset_of_frameStep_7() { return static_cast<int32_t>(offsetof(AnimatedSprite_t2222380888, ___frameStep_7)); }
	inline int32_t get_frameStep_7() const { return ___frameStep_7; }
	inline int32_t* get_address_of_frameStep_7() { return &___frameStep_7; }
	inline void set_frameStep_7(int32_t value)
	{
		___frameStep_7 = value;
	}

	inline static int32_t get_offset_of_anim_8() { return static_cast<int32_t>(offsetof(AnimatedSprite_t2222380888, ___anim_8)); }
	inline int32_t get_anim_8() const { return ___anim_8; }
	inline int32_t* get_address_of_anim_8() { return &___anim_8; }
	inline void set_anim_8(int32_t value)
	{
		___anim_8 = value;
	}

	inline static int32_t get_offset_of_TM_9() { return static_cast<int32_t>(offsetof(AnimatedSprite_t2222380888, ___TM_9)); }
	inline TextMesh_t1641806576 * get_TM_9() const { return ___TM_9; }
	inline TextMesh_t1641806576 ** get_address_of_TM_9() { return &___TM_9; }
	inline void set_TM_9(TextMesh_t1641806576 * value)
	{
		___TM_9 = value;
		Il2CppCodeGenWriteBarrier((&___TM_9), value);
	}

	inline static int32_t get_offset_of_TMR_10() { return static_cast<int32_t>(offsetof(AnimatedSprite_t2222380888, ___TMR_10)); }
	inline Renderer_t257310565 * get_TMR_10() const { return ___TMR_10; }
	inline Renderer_t257310565 ** get_address_of_TMR_10() { return &___TMR_10; }
	inline void set_TMR_10(Renderer_t257310565 * value)
	{
		___TMR_10 = value;
		Il2CppCodeGenWriteBarrier((&___TMR_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATEDSPRITE_T2222380888_H
#ifndef COROUTINERINSTANCE_T1991176683_H
#define COROUTINERINSTANCE_T1991176683_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoroutinerInstance
struct  CoroutinerInstance_t1991176683  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COROUTINERINSTANCE_T1991176683_H
#ifndef ADHOST_T4034195185_H
#define ADHOST_T4034195185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ADS.ADHost
struct  ADHost_t4034195185  : public MonoBehaviour_t1158329972
{
public:
	// ADS.ADHost/ADEvent ADS.ADHost::onRewardedVideoAdOpened
	ADEvent_t127799521 * ___onRewardedVideoAdOpened_2;
	// ADS.ADHost/ADEvent ADS.ADHost::onRewardedVideoAdClosed
	ADEvent_t127799521 * ___onRewardedVideoAdClosed_3;
	// ADS.ADHost/ADEvent ADS.ADHost::onRewardedVideoStart
	ADEvent_t127799521 * ___onRewardedVideoStart_4;
	// ADS.ADHost/ADEvent ADS.ADHost::onRewardedVideoStop
	ADEvent_t127799521 * ___onRewardedVideoStop_5;
	// ADS.ADHost/ADEvent ADS.ADHost::onRewardedVideoDidFinished
	ADEvent_t127799521 * ___onRewardedVideoDidFinished_6;

public:
	inline static int32_t get_offset_of_onRewardedVideoAdOpened_2() { return static_cast<int32_t>(offsetof(ADHost_t4034195185, ___onRewardedVideoAdOpened_2)); }
	inline ADEvent_t127799521 * get_onRewardedVideoAdOpened_2() const { return ___onRewardedVideoAdOpened_2; }
	inline ADEvent_t127799521 ** get_address_of_onRewardedVideoAdOpened_2() { return &___onRewardedVideoAdOpened_2; }
	inline void set_onRewardedVideoAdOpened_2(ADEvent_t127799521 * value)
	{
		___onRewardedVideoAdOpened_2 = value;
		Il2CppCodeGenWriteBarrier((&___onRewardedVideoAdOpened_2), value);
	}

	inline static int32_t get_offset_of_onRewardedVideoAdClosed_3() { return static_cast<int32_t>(offsetof(ADHost_t4034195185, ___onRewardedVideoAdClosed_3)); }
	inline ADEvent_t127799521 * get_onRewardedVideoAdClosed_3() const { return ___onRewardedVideoAdClosed_3; }
	inline ADEvent_t127799521 ** get_address_of_onRewardedVideoAdClosed_3() { return &___onRewardedVideoAdClosed_3; }
	inline void set_onRewardedVideoAdClosed_3(ADEvent_t127799521 * value)
	{
		___onRewardedVideoAdClosed_3 = value;
		Il2CppCodeGenWriteBarrier((&___onRewardedVideoAdClosed_3), value);
	}

	inline static int32_t get_offset_of_onRewardedVideoStart_4() { return static_cast<int32_t>(offsetof(ADHost_t4034195185, ___onRewardedVideoStart_4)); }
	inline ADEvent_t127799521 * get_onRewardedVideoStart_4() const { return ___onRewardedVideoStart_4; }
	inline ADEvent_t127799521 ** get_address_of_onRewardedVideoStart_4() { return &___onRewardedVideoStart_4; }
	inline void set_onRewardedVideoStart_4(ADEvent_t127799521 * value)
	{
		___onRewardedVideoStart_4 = value;
		Il2CppCodeGenWriteBarrier((&___onRewardedVideoStart_4), value);
	}

	inline static int32_t get_offset_of_onRewardedVideoStop_5() { return static_cast<int32_t>(offsetof(ADHost_t4034195185, ___onRewardedVideoStop_5)); }
	inline ADEvent_t127799521 * get_onRewardedVideoStop_5() const { return ___onRewardedVideoStop_5; }
	inline ADEvent_t127799521 ** get_address_of_onRewardedVideoStop_5() { return &___onRewardedVideoStop_5; }
	inline void set_onRewardedVideoStop_5(ADEvent_t127799521 * value)
	{
		___onRewardedVideoStop_5 = value;
		Il2CppCodeGenWriteBarrier((&___onRewardedVideoStop_5), value);
	}

	inline static int32_t get_offset_of_onRewardedVideoDidFinished_6() { return static_cast<int32_t>(offsetof(ADHost_t4034195185, ___onRewardedVideoDidFinished_6)); }
	inline ADEvent_t127799521 * get_onRewardedVideoDidFinished_6() const { return ___onRewardedVideoDidFinished_6; }
	inline ADEvent_t127799521 ** get_address_of_onRewardedVideoDidFinished_6() { return &___onRewardedVideoDidFinished_6; }
	inline void set_onRewardedVideoDidFinished_6(ADEvent_t127799521 * value)
	{
		___onRewardedVideoDidFinished_6 = value;
		Il2CppCodeGenWriteBarrier((&___onRewardedVideoDidFinished_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADHOST_T4034195185_H
#ifndef UNITYARHITTESTEXAMPLE_T146867607_H
#define UNITYARHITTESTEXAMPLE_T146867607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARHitTestExample
struct  UnityARHitTestExample_t146867607  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform UnityEngine.XR.iOS.UnityARHitTestExample::m_HitTransform
	Transform_t3275118058 * ___m_HitTransform_2;

public:
	inline static int32_t get_offset_of_m_HitTransform_2() { return static_cast<int32_t>(offsetof(UnityARHitTestExample_t146867607, ___m_HitTransform_2)); }
	inline Transform_t3275118058 * get_m_HitTransform_2() const { return ___m_HitTransform_2; }
	inline Transform_t3275118058 ** get_address_of_m_HitTransform_2() { return &___m_HitTransform_2; }
	inline void set_m_HitTransform_2(Transform_t3275118058 * value)
	{
		___m_HitTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_HitTransform_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARHITTESTEXAMPLE_T146867607_H
#ifndef SEQUENCES_T3565862770_H
#define SEQUENCES_T3565862770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sequences
struct  Sequences_t3565862770  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Sequences::target
	Transform_t3275118058 * ___target_2;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(Sequences_t3565862770, ___target_2)); }
	inline Transform_t3275118058 * get_target_2() const { return ___target_2; }
	inline Transform_t3275118058 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3275118058 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCES_T3565862770_H
#ifndef UNITYARVIDEO_T2351297253_H
#define UNITYARVIDEO_T2351297253_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARVideo
struct  UnityARVideo_t2351297253  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material UnityEngine.XR.iOS.UnityARVideo::m_ClearMaterial
	Material_t193706927 * ___m_ClearMaterial_2;
	// UnityEngine.Rendering.CommandBuffer UnityEngine.XR.iOS.UnityARVideo::m_VideoCommandBuffer
	CommandBuffer_t1204166949 * ___m_VideoCommandBuffer_3;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureY
	Texture2D_t3542995729 * ____videoTextureY_4;
	// UnityEngine.Texture2D UnityEngine.XR.iOS.UnityARVideo::_videoTextureCbCr
	Texture2D_t3542995729 * ____videoTextureCbCr_5;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARVideo::m_Session
	UnityARSessionNativeInterface_t1130867170 * ___m_Session_6;
	// System.Boolean UnityEngine.XR.iOS.UnityARVideo::bCommandBufferInitialized
	bool ___bCommandBufferInitialized_7;

public:
	inline static int32_t get_offset_of_m_ClearMaterial_2() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___m_ClearMaterial_2)); }
	inline Material_t193706927 * get_m_ClearMaterial_2() const { return ___m_ClearMaterial_2; }
	inline Material_t193706927 ** get_address_of_m_ClearMaterial_2() { return &___m_ClearMaterial_2; }
	inline void set_m_ClearMaterial_2(Material_t193706927 * value)
	{
		___m_ClearMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_ClearMaterial_2), value);
	}

	inline static int32_t get_offset_of_m_VideoCommandBuffer_3() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___m_VideoCommandBuffer_3)); }
	inline CommandBuffer_t1204166949 * get_m_VideoCommandBuffer_3() const { return ___m_VideoCommandBuffer_3; }
	inline CommandBuffer_t1204166949 ** get_address_of_m_VideoCommandBuffer_3() { return &___m_VideoCommandBuffer_3; }
	inline void set_m_VideoCommandBuffer_3(CommandBuffer_t1204166949 * value)
	{
		___m_VideoCommandBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_VideoCommandBuffer_3), value);
	}

	inline static int32_t get_offset_of__videoTextureY_4() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ____videoTextureY_4)); }
	inline Texture2D_t3542995729 * get__videoTextureY_4() const { return ____videoTextureY_4; }
	inline Texture2D_t3542995729 ** get_address_of__videoTextureY_4() { return &____videoTextureY_4; }
	inline void set__videoTextureY_4(Texture2D_t3542995729 * value)
	{
		____videoTextureY_4 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureY_4), value);
	}

	inline static int32_t get_offset_of__videoTextureCbCr_5() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ____videoTextureCbCr_5)); }
	inline Texture2D_t3542995729 * get__videoTextureCbCr_5() const { return ____videoTextureCbCr_5; }
	inline Texture2D_t3542995729 ** get_address_of__videoTextureCbCr_5() { return &____videoTextureCbCr_5; }
	inline void set__videoTextureCbCr_5(Texture2D_t3542995729 * value)
	{
		____videoTextureCbCr_5 = value;
		Il2CppCodeGenWriteBarrier((&____videoTextureCbCr_5), value);
	}

	inline static int32_t get_offset_of_m_Session_6() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___m_Session_6)); }
	inline UnityARSessionNativeInterface_t1130867170 * get_m_Session_6() const { return ___m_Session_6; }
	inline UnityARSessionNativeInterface_t1130867170 ** get_address_of_m_Session_6() { return &___m_Session_6; }
	inline void set_m_Session_6(UnityARSessionNativeInterface_t1130867170 * value)
	{
		___m_Session_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_6), value);
	}

	inline static int32_t get_offset_of_bCommandBufferInitialized_7() { return static_cast<int32_t>(offsetof(UnityARVideo_t2351297253, ___bCommandBufferInitialized_7)); }
	inline bool get_bCommandBufferInitialized_7() const { return ___bCommandBufferInitialized_7; }
	inline bool* get_address_of_bCommandBufferInitialized_7() { return &___bCommandBufferInitialized_7; }
	inline void set_bCommandBufferInitialized_7(bool value)
	{
		___bCommandBufferInitialized_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARVIDEO_T2351297253_H
#ifndef UNITYARUSERANCHORCOMPONENT_T3596724887_H
#define UNITYARUSERANCHORCOMPONENT_T3596724887_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARUserAnchorComponent
struct  UnityARUserAnchorComponent_t3596724887  : public MonoBehaviour_t1158329972
{
public:
	// System.String UnityEngine.XR.iOS.UnityARUserAnchorComponent::m_AnchorId
	String_t* ___m_AnchorId_2;

public:
	inline static int32_t get_offset_of_m_AnchorId_2() { return static_cast<int32_t>(offsetof(UnityARUserAnchorComponent_t3596724887, ___m_AnchorId_2)); }
	inline String_t* get_m_AnchorId_2() const { return ___m_AnchorId_2; }
	inline String_t** get_address_of_m_AnchorId_2() { return &___m_AnchorId_2; }
	inline void set_m_AnchorId_2(String_t* value)
	{
		___m_AnchorId_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnchorId_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARUSERANCHORCOMPONENT_T3596724887_H
#ifndef UNITYARKITCONTROL_T1698990409_H
#define UNITYARKITCONTROL_T1698990409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARKitControl
struct  UnityARKitControl_t1698990409  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.XR.iOS.UnityARSessionRunOption[] UnityEngine.XR.iOS.UnityARKitControl::runOptions
	UnityARSessionRunOptionU5BU5D_t3114965901* ___runOptions_2;
	// UnityEngine.XR.iOS.UnityARAlignment[] UnityEngine.XR.iOS.UnityARKitControl::alignmentOptions
	UnityARAlignmentU5BU5D_t218994990* ___alignmentOptions_3;
	// UnityEngine.XR.iOS.UnityARPlaneDetection[] UnityEngine.XR.iOS.UnityARKitControl::planeOptions
	UnityARPlaneDetectionU5BU5D_t191549612* ___planeOptions_4;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentOptionIndex
	int32_t ___currentOptionIndex_5;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentAlignmentIndex
	int32_t ___currentAlignmentIndex_6;
	// System.Int32 UnityEngine.XR.iOS.UnityARKitControl::currentPlaneIndex
	int32_t ___currentPlaneIndex_7;

public:
	inline static int32_t get_offset_of_runOptions_2() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___runOptions_2)); }
	inline UnityARSessionRunOptionU5BU5D_t3114965901* get_runOptions_2() const { return ___runOptions_2; }
	inline UnityARSessionRunOptionU5BU5D_t3114965901** get_address_of_runOptions_2() { return &___runOptions_2; }
	inline void set_runOptions_2(UnityARSessionRunOptionU5BU5D_t3114965901* value)
	{
		___runOptions_2 = value;
		Il2CppCodeGenWriteBarrier((&___runOptions_2), value);
	}

	inline static int32_t get_offset_of_alignmentOptions_3() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___alignmentOptions_3)); }
	inline UnityARAlignmentU5BU5D_t218994990* get_alignmentOptions_3() const { return ___alignmentOptions_3; }
	inline UnityARAlignmentU5BU5D_t218994990** get_address_of_alignmentOptions_3() { return &___alignmentOptions_3; }
	inline void set_alignmentOptions_3(UnityARAlignmentU5BU5D_t218994990* value)
	{
		___alignmentOptions_3 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentOptions_3), value);
	}

	inline static int32_t get_offset_of_planeOptions_4() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___planeOptions_4)); }
	inline UnityARPlaneDetectionU5BU5D_t191549612* get_planeOptions_4() const { return ___planeOptions_4; }
	inline UnityARPlaneDetectionU5BU5D_t191549612** get_address_of_planeOptions_4() { return &___planeOptions_4; }
	inline void set_planeOptions_4(UnityARPlaneDetectionU5BU5D_t191549612* value)
	{
		___planeOptions_4 = value;
		Il2CppCodeGenWriteBarrier((&___planeOptions_4), value);
	}

	inline static int32_t get_offset_of_currentOptionIndex_5() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___currentOptionIndex_5)); }
	inline int32_t get_currentOptionIndex_5() const { return ___currentOptionIndex_5; }
	inline int32_t* get_address_of_currentOptionIndex_5() { return &___currentOptionIndex_5; }
	inline void set_currentOptionIndex_5(int32_t value)
	{
		___currentOptionIndex_5 = value;
	}

	inline static int32_t get_offset_of_currentAlignmentIndex_6() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___currentAlignmentIndex_6)); }
	inline int32_t get_currentAlignmentIndex_6() const { return ___currentAlignmentIndex_6; }
	inline int32_t* get_address_of_currentAlignmentIndex_6() { return &___currentAlignmentIndex_6; }
	inline void set_currentAlignmentIndex_6(int32_t value)
	{
		___currentAlignmentIndex_6 = value;
	}

	inline static int32_t get_offset_of_currentPlaneIndex_7() { return static_cast<int32_t>(offsetof(UnityARKitControl_t1698990409, ___currentPlaneIndex_7)); }
	inline int32_t get_currentPlaneIndex_7() const { return ___currentPlaneIndex_7; }
	inline int32_t* get_address_of_currentPlaneIndex_7() { return &___currentPlaneIndex_7; }
	inline void set_currentPlaneIndex_7(int32_t value)
	{
		___currentPlaneIndex_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARKITCONTROL_T1698990409_H
#ifndef BASICS_T2766259351_H
#define BASICS_T2766259351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Basics
struct  Basics_t2766259351  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform Basics::cubeA
	Transform_t3275118058 * ___cubeA_2;
	// UnityEngine.Transform Basics::cubeB
	Transform_t3275118058 * ___cubeB_3;

public:
	inline static int32_t get_offset_of_cubeA_2() { return static_cast<int32_t>(offsetof(Basics_t2766259351, ___cubeA_2)); }
	inline Transform_t3275118058 * get_cubeA_2() const { return ___cubeA_2; }
	inline Transform_t3275118058 ** get_address_of_cubeA_2() { return &___cubeA_2; }
	inline void set_cubeA_2(Transform_t3275118058 * value)
	{
		___cubeA_2 = value;
		Il2CppCodeGenWriteBarrier((&___cubeA_2), value);
	}

	inline static int32_t get_offset_of_cubeB_3() { return static_cast<int32_t>(offsetof(Basics_t2766259351, ___cubeB_3)); }
	inline Transform_t3275118058 * get_cubeB_3() const { return ___cubeB_3; }
	inline Transform_t3275118058 ** get_address_of_cubeB_3() { return &___cubeB_3; }
	inline void set_cubeB_3(Transform_t3275118058 * value)
	{
		___cubeB_3 = value;
		Il2CppCodeGenWriteBarrier((&___cubeB_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICS_T2766259351_H
#ifndef UNITYARAMBIENT_T680084560_H
#define UNITYARAMBIENT_T680084560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARAmbient
struct  UnityARAmbient_t680084560  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Light UnityEngine.XR.iOS.UnityARAmbient::l
	Light_t494725636 * ___l_2;
	// UnityEngine.XR.iOS.UnityARSessionNativeInterface UnityEngine.XR.iOS.UnityARAmbient::m_Session
	UnityARSessionNativeInterface_t1130867170 * ___m_Session_3;

public:
	inline static int32_t get_offset_of_l_2() { return static_cast<int32_t>(offsetof(UnityARAmbient_t680084560, ___l_2)); }
	inline Light_t494725636 * get_l_2() const { return ___l_2; }
	inline Light_t494725636 ** get_address_of_l_2() { return &___l_2; }
	inline void set_l_2(Light_t494725636 * value)
	{
		___l_2 = value;
		Il2CppCodeGenWriteBarrier((&___l_2), value);
	}

	inline static int32_t get_offset_of_m_Session_3() { return static_cast<int32_t>(offsetof(UnityARAmbient_t680084560, ___m_Session_3)); }
	inline UnityARSessionNativeInterface_t1130867170 * get_m_Session_3() const { return ___m_Session_3; }
	inline UnityARSessionNativeInterface_t1130867170 ** get_address_of_m_Session_3() { return &___m_Session_3; }
	inline void set_m_Session_3(UnityARSessionNativeInterface_t1130867170 * value)
	{
		___m_Session_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Session_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARAMBIENT_T680084560_H
#ifndef UNITYPOINTCLOUDEXAMPLE_T3196264220_H
#define UNITYPOINTCLOUDEXAMPLE_T3196264220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityPointCloudExample
struct  UnityPointCloudExample_t3196264220  : public MonoBehaviour_t1158329972
{
public:
	// System.UInt32 UnityPointCloudExample::numPointsToShow
	uint32_t ___numPointsToShow_2;
	// UnityEngine.GameObject UnityPointCloudExample::PointCloudPrefab
	GameObject_t1756533147 * ___PointCloudPrefab_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> UnityPointCloudExample::pointCloudObjects
	List_1_t1125654279 * ___pointCloudObjects_4;
	// UnityEngine.Vector3[] UnityPointCloudExample::m_PointCloudData
	Vector3U5BU5D_t1172311765* ___m_PointCloudData_5;

public:
	inline static int32_t get_offset_of_numPointsToShow_2() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___numPointsToShow_2)); }
	inline uint32_t get_numPointsToShow_2() const { return ___numPointsToShow_2; }
	inline uint32_t* get_address_of_numPointsToShow_2() { return &___numPointsToShow_2; }
	inline void set_numPointsToShow_2(uint32_t value)
	{
		___numPointsToShow_2 = value;
	}

	inline static int32_t get_offset_of_PointCloudPrefab_3() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___PointCloudPrefab_3)); }
	inline GameObject_t1756533147 * get_PointCloudPrefab_3() const { return ___PointCloudPrefab_3; }
	inline GameObject_t1756533147 ** get_address_of_PointCloudPrefab_3() { return &___PointCloudPrefab_3; }
	inline void set_PointCloudPrefab_3(GameObject_t1756533147 * value)
	{
		___PointCloudPrefab_3 = value;
		Il2CppCodeGenWriteBarrier((&___PointCloudPrefab_3), value);
	}

	inline static int32_t get_offset_of_pointCloudObjects_4() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___pointCloudObjects_4)); }
	inline List_1_t1125654279 * get_pointCloudObjects_4() const { return ___pointCloudObjects_4; }
	inline List_1_t1125654279 ** get_address_of_pointCloudObjects_4() { return &___pointCloudObjects_4; }
	inline void set_pointCloudObjects_4(List_1_t1125654279 * value)
	{
		___pointCloudObjects_4 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudObjects_4), value);
	}

	inline static int32_t get_offset_of_m_PointCloudData_5() { return static_cast<int32_t>(offsetof(UnityPointCloudExample_t3196264220, ___m_PointCloudData_5)); }
	inline Vector3U5BU5D_t1172311765* get_m_PointCloudData_5() const { return ___m_PointCloudData_5; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_PointCloudData_5() { return &___m_PointCloudData_5; }
	inline void set_m_PointCloudData_5(Vector3U5BU5D_t1172311765* value)
	{
		___m_PointCloudData_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_PointCloudData_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPOINTCLOUDEXAMPLE_T3196264220_H
#ifndef VIEWMANAGER_T2308013472_H
#define VIEWMANAGER_T2308013472_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewManager
struct  ViewManager_t2308013472  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera ViewManager::_camera
	Camera_t189460977 * ____camera_2;
	// System.Collections.Generic.List`1<IModel> ViewManager::_modelsList
	List_1_t1605919132 * ____modelsList_3;
	// NotificationPanel ViewManager::NotifyPanel
	NotificationPanel_t4176433925 * ___NotifyPanel_4;
	// UnityEngine.RectTransform ViewManager::FadePanel
	RectTransform_t3349966182 * ___FadePanel_5;
	// System.Boolean ViewManager::FadeScreen
	bool ___FadeScreen_6;
	// System.Single ViewManager::ScreenFadeTime
	float ___ScreenFadeTime_7;
	// System.Single ViewManager::ScreenFadeMax
	float ___ScreenFadeMax_8;
	// System.Single ViewManager::ViewsFadeTime
	float ___ViewsFadeTime_9;
	// System.Single ViewManager::ViewsFadeMax
	float ___ViewsFadeMax_10;
	// UnityEngine.Color ViewManager::FadeColor
	Color_t2020392075  ___FadeColor_11;
	// System.Boolean ViewManager::OutsideTap
	bool ___OutsideTap_12;
	// System.Action ViewManager::OnAllClosed
	Action_t3226471752 * ___OnAllClosed_13;
	// DG.Tweening.Tweener ViewManager::_fadeTweener
	Tweener_t760404022 * ____fadeTweener_14;
	// System.Boolean ViewManager::_fadeIn
	bool ____fadeIn_15;
	// System.Boolean ViewManager::_fadeOut
	bool ____fadeOut_16;

public:
	inline static int32_t get_offset_of__camera_2() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ____camera_2)); }
	inline Camera_t189460977 * get__camera_2() const { return ____camera_2; }
	inline Camera_t189460977 ** get_address_of__camera_2() { return &____camera_2; }
	inline void set__camera_2(Camera_t189460977 * value)
	{
		____camera_2 = value;
		Il2CppCodeGenWriteBarrier((&____camera_2), value);
	}

	inline static int32_t get_offset_of__modelsList_3() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ____modelsList_3)); }
	inline List_1_t1605919132 * get__modelsList_3() const { return ____modelsList_3; }
	inline List_1_t1605919132 ** get_address_of__modelsList_3() { return &____modelsList_3; }
	inline void set__modelsList_3(List_1_t1605919132 * value)
	{
		____modelsList_3 = value;
		Il2CppCodeGenWriteBarrier((&____modelsList_3), value);
	}

	inline static int32_t get_offset_of_NotifyPanel_4() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ___NotifyPanel_4)); }
	inline NotificationPanel_t4176433925 * get_NotifyPanel_4() const { return ___NotifyPanel_4; }
	inline NotificationPanel_t4176433925 ** get_address_of_NotifyPanel_4() { return &___NotifyPanel_4; }
	inline void set_NotifyPanel_4(NotificationPanel_t4176433925 * value)
	{
		___NotifyPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___NotifyPanel_4), value);
	}

	inline static int32_t get_offset_of_FadePanel_5() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ___FadePanel_5)); }
	inline RectTransform_t3349966182 * get_FadePanel_5() const { return ___FadePanel_5; }
	inline RectTransform_t3349966182 ** get_address_of_FadePanel_5() { return &___FadePanel_5; }
	inline void set_FadePanel_5(RectTransform_t3349966182 * value)
	{
		___FadePanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___FadePanel_5), value);
	}

	inline static int32_t get_offset_of_FadeScreen_6() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ___FadeScreen_6)); }
	inline bool get_FadeScreen_6() const { return ___FadeScreen_6; }
	inline bool* get_address_of_FadeScreen_6() { return &___FadeScreen_6; }
	inline void set_FadeScreen_6(bool value)
	{
		___FadeScreen_6 = value;
	}

	inline static int32_t get_offset_of_ScreenFadeTime_7() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ___ScreenFadeTime_7)); }
	inline float get_ScreenFadeTime_7() const { return ___ScreenFadeTime_7; }
	inline float* get_address_of_ScreenFadeTime_7() { return &___ScreenFadeTime_7; }
	inline void set_ScreenFadeTime_7(float value)
	{
		___ScreenFadeTime_7 = value;
	}

	inline static int32_t get_offset_of_ScreenFadeMax_8() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ___ScreenFadeMax_8)); }
	inline float get_ScreenFadeMax_8() const { return ___ScreenFadeMax_8; }
	inline float* get_address_of_ScreenFadeMax_8() { return &___ScreenFadeMax_8; }
	inline void set_ScreenFadeMax_8(float value)
	{
		___ScreenFadeMax_8 = value;
	}

	inline static int32_t get_offset_of_ViewsFadeTime_9() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ___ViewsFadeTime_9)); }
	inline float get_ViewsFadeTime_9() const { return ___ViewsFadeTime_9; }
	inline float* get_address_of_ViewsFadeTime_9() { return &___ViewsFadeTime_9; }
	inline void set_ViewsFadeTime_9(float value)
	{
		___ViewsFadeTime_9 = value;
	}

	inline static int32_t get_offset_of_ViewsFadeMax_10() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ___ViewsFadeMax_10)); }
	inline float get_ViewsFadeMax_10() const { return ___ViewsFadeMax_10; }
	inline float* get_address_of_ViewsFadeMax_10() { return &___ViewsFadeMax_10; }
	inline void set_ViewsFadeMax_10(float value)
	{
		___ViewsFadeMax_10 = value;
	}

	inline static int32_t get_offset_of_FadeColor_11() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ___FadeColor_11)); }
	inline Color_t2020392075  get_FadeColor_11() const { return ___FadeColor_11; }
	inline Color_t2020392075 * get_address_of_FadeColor_11() { return &___FadeColor_11; }
	inline void set_FadeColor_11(Color_t2020392075  value)
	{
		___FadeColor_11 = value;
	}

	inline static int32_t get_offset_of_OutsideTap_12() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ___OutsideTap_12)); }
	inline bool get_OutsideTap_12() const { return ___OutsideTap_12; }
	inline bool* get_address_of_OutsideTap_12() { return &___OutsideTap_12; }
	inline void set_OutsideTap_12(bool value)
	{
		___OutsideTap_12 = value;
	}

	inline static int32_t get_offset_of_OnAllClosed_13() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ___OnAllClosed_13)); }
	inline Action_t3226471752 * get_OnAllClosed_13() const { return ___OnAllClosed_13; }
	inline Action_t3226471752 ** get_address_of_OnAllClosed_13() { return &___OnAllClosed_13; }
	inline void set_OnAllClosed_13(Action_t3226471752 * value)
	{
		___OnAllClosed_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnAllClosed_13), value);
	}

	inline static int32_t get_offset_of__fadeTweener_14() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ____fadeTweener_14)); }
	inline Tweener_t760404022 * get__fadeTweener_14() const { return ____fadeTweener_14; }
	inline Tweener_t760404022 ** get_address_of__fadeTweener_14() { return &____fadeTweener_14; }
	inline void set__fadeTweener_14(Tweener_t760404022 * value)
	{
		____fadeTweener_14 = value;
		Il2CppCodeGenWriteBarrier((&____fadeTweener_14), value);
	}

	inline static int32_t get_offset_of__fadeIn_15() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ____fadeIn_15)); }
	inline bool get__fadeIn_15() const { return ____fadeIn_15; }
	inline bool* get_address_of__fadeIn_15() { return &____fadeIn_15; }
	inline void set__fadeIn_15(bool value)
	{
		____fadeIn_15 = value;
	}

	inline static int32_t get_offset_of__fadeOut_16() { return static_cast<int32_t>(offsetof(ViewManager_t2308013472, ____fadeOut_16)); }
	inline bool get__fadeOut_16() const { return ____fadeOut_16; }
	inline bool* get_address_of__fadeOut_16() { return &____fadeOut_16; }
	inline void set__fadeOut_16(bool value)
	{
		____fadeOut_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWMANAGER_T2308013472_H
#ifndef VIEWCAMERA_T2275596458_H
#define VIEWCAMERA_T2275596458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewCamera
struct  ViewCamera_t2275596458  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWCAMERA_T2275596458_H
#ifndef NOTIFICATIONPANEL_T4176433925_H
#define NOTIFICATIONPANEL_T4176433925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationPanel
struct  NotificationPanel_t4176433925  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Sprite NotificationPanel::NotifySprite
	Sprite_t309593783 * ___NotifySprite_2;
	// UnityEngine.RectTransform NotificationPanel::NotifyZone
	RectTransform_t3349966182 * ___NotifyZone_3;
	// System.Collections.Generic.Dictionary`2<System.String,NotificationItem> NotificationPanel::_items
	Dictionary_2_t2120142174 * ____items_4;

public:
	inline static int32_t get_offset_of_NotifySprite_2() { return static_cast<int32_t>(offsetof(NotificationPanel_t4176433925, ___NotifySprite_2)); }
	inline Sprite_t309593783 * get_NotifySprite_2() const { return ___NotifySprite_2; }
	inline Sprite_t309593783 ** get_address_of_NotifySprite_2() { return &___NotifySprite_2; }
	inline void set_NotifySprite_2(Sprite_t309593783 * value)
	{
		___NotifySprite_2 = value;
		Il2CppCodeGenWriteBarrier((&___NotifySprite_2), value);
	}

	inline static int32_t get_offset_of_NotifyZone_3() { return static_cast<int32_t>(offsetof(NotificationPanel_t4176433925, ___NotifyZone_3)); }
	inline RectTransform_t3349966182 * get_NotifyZone_3() const { return ___NotifyZone_3; }
	inline RectTransform_t3349966182 ** get_address_of_NotifyZone_3() { return &___NotifyZone_3; }
	inline void set_NotifyZone_3(RectTransform_t3349966182 * value)
	{
		___NotifyZone_3 = value;
		Il2CppCodeGenWriteBarrier((&___NotifyZone_3), value);
	}

	inline static int32_t get_offset_of__items_4() { return static_cast<int32_t>(offsetof(NotificationPanel_t4176433925, ____items_4)); }
	inline Dictionary_2_t2120142174 * get__items_4() const { return ____items_4; }
	inline Dictionary_2_t2120142174 ** get_address_of__items_4() { return &____items_4; }
	inline void set__items_4(Dictionary_2_t2120142174 * value)
	{
		____items_4 = value;
		Il2CppCodeGenWriteBarrier((&____items_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONPANEL_T4176433925_H
#ifndef CIRCLERENDERER_T1868761029_H
#define CIRCLERENDERER_T1868761029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CircleRenderer
struct  CircleRenderer_t1868761029  : public MonoBehaviour_t1158329972
{
public:
	// System.Single CircleRenderer::_slices
	float ____slices_2;
	// System.Single CircleRenderer::_outRadius
	float ____outRadius_3;
	// System.Single CircleRenderer::_inRadius
	float ____inRadius_4;

public:
	inline static int32_t get_offset_of__slices_2() { return static_cast<int32_t>(offsetof(CircleRenderer_t1868761029, ____slices_2)); }
	inline float get__slices_2() const { return ____slices_2; }
	inline float* get_address_of__slices_2() { return &____slices_2; }
	inline void set__slices_2(float value)
	{
		____slices_2 = value;
	}

	inline static int32_t get_offset_of__outRadius_3() { return static_cast<int32_t>(offsetof(CircleRenderer_t1868761029, ____outRadius_3)); }
	inline float get__outRadius_3() const { return ____outRadius_3; }
	inline float* get_address_of__outRadius_3() { return &____outRadius_3; }
	inline void set__outRadius_3(float value)
	{
		____outRadius_3 = value;
	}

	inline static int32_t get_offset_of__inRadius_4() { return static_cast<int32_t>(offsetof(CircleRenderer_t1868761029, ____inRadius_4)); }
	inline float get__inRadius_4() const { return ____inRadius_4; }
	inline float* get_address_of__inRadius_4() { return &____inRadius_4; }
	inline void set__inRadius_4(float value)
	{
		____inRadius_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CIRCLERENDERER_T1868761029_H
#ifndef LINEDRAWER_T3216095815_H
#define LINEDRAWER_T3216095815_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LineDrawer
struct  LineDrawer_t3216095815  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material LineDrawer::lineMat
	Material_t193706927 * ___lineMat_2;
	// UnityEngine.GameObject LineDrawer::mainPoint
	GameObject_t1756533147 * ___mainPoint_3;
	// UnityEngine.GameObject[] LineDrawer::points
	GameObjectU5BU5D_t3057952154* ___points_4;

public:
	inline static int32_t get_offset_of_lineMat_2() { return static_cast<int32_t>(offsetof(LineDrawer_t3216095815, ___lineMat_2)); }
	inline Material_t193706927 * get_lineMat_2() const { return ___lineMat_2; }
	inline Material_t193706927 ** get_address_of_lineMat_2() { return &___lineMat_2; }
	inline void set_lineMat_2(Material_t193706927 * value)
	{
		___lineMat_2 = value;
		Il2CppCodeGenWriteBarrier((&___lineMat_2), value);
	}

	inline static int32_t get_offset_of_mainPoint_3() { return static_cast<int32_t>(offsetof(LineDrawer_t3216095815, ___mainPoint_3)); }
	inline GameObject_t1756533147 * get_mainPoint_3() const { return ___mainPoint_3; }
	inline GameObject_t1756533147 ** get_address_of_mainPoint_3() { return &___mainPoint_3; }
	inline void set_mainPoint_3(GameObject_t1756533147 * value)
	{
		___mainPoint_3 = value;
		Il2CppCodeGenWriteBarrier((&___mainPoint_3), value);
	}

	inline static int32_t get_offset_of_points_4() { return static_cast<int32_t>(offsetof(LineDrawer_t3216095815, ___points_4)); }
	inline GameObjectU5BU5D_t3057952154* get_points_4() const { return ___points_4; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_points_4() { return &___points_4; }
	inline void set_points_4(GameObjectU5BU5D_t3057952154* value)
	{
		___points_4 = value;
		Il2CppCodeGenWriteBarrier((&___points_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEDRAWER_T3216095815_H
#ifndef INSTANCEMATERIAL_T2647119882_H
#define INSTANCEMATERIAL_T2647119882_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstanceMaterial
struct  InstanceMaterial_t2647119882  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEMATERIAL_T2647119882_H
#ifndef SOUNDMANAGER_T654432262_H
#define SOUNDMANAGER_T654432262_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t654432262  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Audio.AudioMixer SoundManager::AudioMixer
	AudioMixer_t3244290001 * ___AudioMixer_4;
	// UnityEngine.Audio.AudioMixerGroup SoundManager::GroupMaster
	AudioMixerGroup_t959546644 * ___GroupMaster_5;
	// UnityEngine.Audio.AudioMixerGroup SoundManager::GroupMusic
	AudioMixerGroup_t959546644 * ___GroupMusic_6;
	// UnityEngine.Audio.AudioMixerGroup SoundManager::GroupSFX
	AudioMixerGroup_t959546644 * ___GroupSFX_7;
	// UnityEngine.Audio.AudioMixerGroup SoundManager::GroupSpeaking
	AudioMixerGroup_t959546644 * ___GroupSpeaking_8;
	// System.Boolean SoundManager::<AutoChangeMusics>k__BackingField
	bool ___U3CAutoChangeMusicsU3Ek__BackingField_9;
	// Range SoundManager::PitchVariationsRange
	Range_t624945109  ___PitchVariationsRange_10;
	// UnityEngine.AudioSource SoundManager::<_masterMusicSource>k__BackingField
	AudioSource_t1135106623 * ___U3C_masterMusicSourceU3Ek__BackingField_11;
	// UnityEngine.AudioSource SoundManager::<_secondMusicSource>k__BackingField
	AudioSource_t1135106623 * ___U3C_secondMusicSourceU3Ek__BackingField_12;
	// System.Boolean SoundManager::<_fadeMusics>k__BackingField
	bool ___U3C_fadeMusicsU3Ek__BackingField_13;
	// System.Single SoundManager::_audioSourcesCleanap
	float ____audioSourcesCleanap_14;
	// SoundCollection SoundManager::_musics
	SoundCollection_t2692967059 * ____musics_15;
	// SoundCollection SoundManager::_sounds
	SoundCollection_t2692967059 * ____sounds_16;

public:
	inline static int32_t get_offset_of_AudioMixer_4() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___AudioMixer_4)); }
	inline AudioMixer_t3244290001 * get_AudioMixer_4() const { return ___AudioMixer_4; }
	inline AudioMixer_t3244290001 ** get_address_of_AudioMixer_4() { return &___AudioMixer_4; }
	inline void set_AudioMixer_4(AudioMixer_t3244290001 * value)
	{
		___AudioMixer_4 = value;
		Il2CppCodeGenWriteBarrier((&___AudioMixer_4), value);
	}

	inline static int32_t get_offset_of_GroupMaster_5() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___GroupMaster_5)); }
	inline AudioMixerGroup_t959546644 * get_GroupMaster_5() const { return ___GroupMaster_5; }
	inline AudioMixerGroup_t959546644 ** get_address_of_GroupMaster_5() { return &___GroupMaster_5; }
	inline void set_GroupMaster_5(AudioMixerGroup_t959546644 * value)
	{
		___GroupMaster_5 = value;
		Il2CppCodeGenWriteBarrier((&___GroupMaster_5), value);
	}

	inline static int32_t get_offset_of_GroupMusic_6() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___GroupMusic_6)); }
	inline AudioMixerGroup_t959546644 * get_GroupMusic_6() const { return ___GroupMusic_6; }
	inline AudioMixerGroup_t959546644 ** get_address_of_GroupMusic_6() { return &___GroupMusic_6; }
	inline void set_GroupMusic_6(AudioMixerGroup_t959546644 * value)
	{
		___GroupMusic_6 = value;
		Il2CppCodeGenWriteBarrier((&___GroupMusic_6), value);
	}

	inline static int32_t get_offset_of_GroupSFX_7() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___GroupSFX_7)); }
	inline AudioMixerGroup_t959546644 * get_GroupSFX_7() const { return ___GroupSFX_7; }
	inline AudioMixerGroup_t959546644 ** get_address_of_GroupSFX_7() { return &___GroupSFX_7; }
	inline void set_GroupSFX_7(AudioMixerGroup_t959546644 * value)
	{
		___GroupSFX_7 = value;
		Il2CppCodeGenWriteBarrier((&___GroupSFX_7), value);
	}

	inline static int32_t get_offset_of_GroupSpeaking_8() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___GroupSpeaking_8)); }
	inline AudioMixerGroup_t959546644 * get_GroupSpeaking_8() const { return ___GroupSpeaking_8; }
	inline AudioMixerGroup_t959546644 ** get_address_of_GroupSpeaking_8() { return &___GroupSpeaking_8; }
	inline void set_GroupSpeaking_8(AudioMixerGroup_t959546644 * value)
	{
		___GroupSpeaking_8 = value;
		Il2CppCodeGenWriteBarrier((&___GroupSpeaking_8), value);
	}

	inline static int32_t get_offset_of_U3CAutoChangeMusicsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___U3CAutoChangeMusicsU3Ek__BackingField_9)); }
	inline bool get_U3CAutoChangeMusicsU3Ek__BackingField_9() const { return ___U3CAutoChangeMusicsU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CAutoChangeMusicsU3Ek__BackingField_9() { return &___U3CAutoChangeMusicsU3Ek__BackingField_9; }
	inline void set_U3CAutoChangeMusicsU3Ek__BackingField_9(bool value)
	{
		___U3CAutoChangeMusicsU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_PitchVariationsRange_10() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___PitchVariationsRange_10)); }
	inline Range_t624945109  get_PitchVariationsRange_10() const { return ___PitchVariationsRange_10; }
	inline Range_t624945109 * get_address_of_PitchVariationsRange_10() { return &___PitchVariationsRange_10; }
	inline void set_PitchVariationsRange_10(Range_t624945109  value)
	{
		___PitchVariationsRange_10 = value;
	}

	inline static int32_t get_offset_of_U3C_masterMusicSourceU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___U3C_masterMusicSourceU3Ek__BackingField_11)); }
	inline AudioSource_t1135106623 * get_U3C_masterMusicSourceU3Ek__BackingField_11() const { return ___U3C_masterMusicSourceU3Ek__BackingField_11; }
	inline AudioSource_t1135106623 ** get_address_of_U3C_masterMusicSourceU3Ek__BackingField_11() { return &___U3C_masterMusicSourceU3Ek__BackingField_11; }
	inline void set_U3C_masterMusicSourceU3Ek__BackingField_11(AudioSource_t1135106623 * value)
	{
		___U3C_masterMusicSourceU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_masterMusicSourceU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3C_secondMusicSourceU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___U3C_secondMusicSourceU3Ek__BackingField_12)); }
	inline AudioSource_t1135106623 * get_U3C_secondMusicSourceU3Ek__BackingField_12() const { return ___U3C_secondMusicSourceU3Ek__BackingField_12; }
	inline AudioSource_t1135106623 ** get_address_of_U3C_secondMusicSourceU3Ek__BackingField_12() { return &___U3C_secondMusicSourceU3Ek__BackingField_12; }
	inline void set_U3C_secondMusicSourceU3Ek__BackingField_12(AudioSource_t1135106623 * value)
	{
		___U3C_secondMusicSourceU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_secondMusicSourceU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3C_fadeMusicsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ___U3C_fadeMusicsU3Ek__BackingField_13)); }
	inline bool get_U3C_fadeMusicsU3Ek__BackingField_13() const { return ___U3C_fadeMusicsU3Ek__BackingField_13; }
	inline bool* get_address_of_U3C_fadeMusicsU3Ek__BackingField_13() { return &___U3C_fadeMusicsU3Ek__BackingField_13; }
	inline void set_U3C_fadeMusicsU3Ek__BackingField_13(bool value)
	{
		___U3C_fadeMusicsU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of__audioSourcesCleanap_14() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ____audioSourcesCleanap_14)); }
	inline float get__audioSourcesCleanap_14() const { return ____audioSourcesCleanap_14; }
	inline float* get_address_of__audioSourcesCleanap_14() { return &____audioSourcesCleanap_14; }
	inline void set__audioSourcesCleanap_14(float value)
	{
		____audioSourcesCleanap_14 = value;
	}

	inline static int32_t get_offset_of__musics_15() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ____musics_15)); }
	inline SoundCollection_t2692967059 * get__musics_15() const { return ____musics_15; }
	inline SoundCollection_t2692967059 ** get_address_of__musics_15() { return &____musics_15; }
	inline void set__musics_15(SoundCollection_t2692967059 * value)
	{
		____musics_15 = value;
		Il2CppCodeGenWriteBarrier((&____musics_15), value);
	}

	inline static int32_t get_offset_of__sounds_16() { return static_cast<int32_t>(offsetof(SoundManager_t654432262, ____sounds_16)); }
	inline SoundCollection_t2692967059 * get__sounds_16() const { return ____sounds_16; }
	inline SoundCollection_t2692967059 ** get_address_of__sounds_16() { return &____sounds_16; }
	inline void set__sounds_16(SoundCollection_t2692967059 * value)
	{
		____sounds_16 = value;
		Il2CppCodeGenWriteBarrier((&____sounds_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGER_T654432262_H
#ifndef CANVASFADECOLORGROUP_T1202652588_H
#define CANVASFADECOLORGROUP_T1202652588_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CanvasFadeColorGroup
struct  CanvasFadeColorGroup_t1202652588  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Color CanvasFadeColorGroup::_color
	Color_t2020392075  ____color_2;
	// UnityEngine.Color CanvasFadeColorGroup::_fadeColor
	Color_t2020392075  ____fadeColor_3;
	// System.Boolean CanvasFadeColorGroup::_fadeIn
	bool ____fadeIn_4;
	// System.Boolean CanvasFadeColorGroup::_fadeOut
	bool ____fadeOut_5;
	// DG.Tweening.Tweener CanvasFadeColorGroup::_fadeTweener
	Tweener_t760404022 * ____fadeTweener_6;

public:
	inline static int32_t get_offset_of__color_2() { return static_cast<int32_t>(offsetof(CanvasFadeColorGroup_t1202652588, ____color_2)); }
	inline Color_t2020392075  get__color_2() const { return ____color_2; }
	inline Color_t2020392075 * get_address_of__color_2() { return &____color_2; }
	inline void set__color_2(Color_t2020392075  value)
	{
		____color_2 = value;
	}

	inline static int32_t get_offset_of__fadeColor_3() { return static_cast<int32_t>(offsetof(CanvasFadeColorGroup_t1202652588, ____fadeColor_3)); }
	inline Color_t2020392075  get__fadeColor_3() const { return ____fadeColor_3; }
	inline Color_t2020392075 * get_address_of__fadeColor_3() { return &____fadeColor_3; }
	inline void set__fadeColor_3(Color_t2020392075  value)
	{
		____fadeColor_3 = value;
	}

	inline static int32_t get_offset_of__fadeIn_4() { return static_cast<int32_t>(offsetof(CanvasFadeColorGroup_t1202652588, ____fadeIn_4)); }
	inline bool get__fadeIn_4() const { return ____fadeIn_4; }
	inline bool* get_address_of__fadeIn_4() { return &____fadeIn_4; }
	inline void set__fadeIn_4(bool value)
	{
		____fadeIn_4 = value;
	}

	inline static int32_t get_offset_of__fadeOut_5() { return static_cast<int32_t>(offsetof(CanvasFadeColorGroup_t1202652588, ____fadeOut_5)); }
	inline bool get__fadeOut_5() const { return ____fadeOut_5; }
	inline bool* get_address_of__fadeOut_5() { return &____fadeOut_5; }
	inline void set__fadeOut_5(bool value)
	{
		____fadeOut_5 = value;
	}

	inline static int32_t get_offset_of__fadeTweener_6() { return static_cast<int32_t>(offsetof(CanvasFadeColorGroup_t1202652588, ____fadeTweener_6)); }
	inline Tweener_t760404022 * get__fadeTweener_6() const { return ____fadeTweener_6; }
	inline Tweener_t760404022 ** get_address_of__fadeTweener_6() { return &____fadeTweener_6; }
	inline void set__fadeTweener_6(Tweener_t760404022 * value)
	{
		____fadeTweener_6 = value;
		Il2CppCodeGenWriteBarrier((&____fadeTweener_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASFADECOLORGROUP_T1202652588_H
#ifndef NOTIFICATIONITEM_T205362912_H
#define NOTIFICATIONITEM_T205362912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NotificationItem
struct  NotificationItem_t205362912  : public MonoBehaviour_t1158329972
{
public:
	// System.String NotificationItem::<HintKey>k__BackingField
	String_t* ___U3CHintKeyU3Ek__BackingField_2;
	// UnityEngine.UI.Image NotificationItem::Image
	Image_t2042527209 * ___Image_3;
	// UnityEngine.UI.Text NotificationItem::Text
	Text_t356221433 * ___Text_4;
	// System.Func`1<System.Boolean> NotificationItem::ReceiveCondition
	Func_1_t1485000104 * ___ReceiveCondition_5;
	// System.Action NotificationItem::OnClick
	Action_t3226471752 * ___OnClick_6;
	// System.Action NotificationItem::OnReceived
	Action_t3226471752 * ___OnReceived_7;
	// DG.Tweening.Tweener NotificationItem::Pulse
	Tweener_t760404022 * ___Pulse_8;
	// NotificationPanel NotificationItem::_panel
	NotificationPanel_t4176433925 * ____panel_9;
	// System.Single NotificationItem::_hintTime
	float ____hintTime_10;
	// System.Boolean NotificationItem::_mouseOver
	bool ____mouseOver_11;
	// System.Single NotificationItem::_showTime
	float ____showTime_12;
	// System.Boolean NotificationItem::_timeMode
	bool ____timeMode_13;
	// System.Boolean NotificationItem::_received
	bool ____received_14;

public:
	inline static int32_t get_offset_of_U3CHintKeyU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ___U3CHintKeyU3Ek__BackingField_2)); }
	inline String_t* get_U3CHintKeyU3Ek__BackingField_2() const { return ___U3CHintKeyU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CHintKeyU3Ek__BackingField_2() { return &___U3CHintKeyU3Ek__BackingField_2; }
	inline void set_U3CHintKeyU3Ek__BackingField_2(String_t* value)
	{
		___U3CHintKeyU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CHintKeyU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_Image_3() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ___Image_3)); }
	inline Image_t2042527209 * get_Image_3() const { return ___Image_3; }
	inline Image_t2042527209 ** get_address_of_Image_3() { return &___Image_3; }
	inline void set_Image_3(Image_t2042527209 * value)
	{
		___Image_3 = value;
		Il2CppCodeGenWriteBarrier((&___Image_3), value);
	}

	inline static int32_t get_offset_of_Text_4() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ___Text_4)); }
	inline Text_t356221433 * get_Text_4() const { return ___Text_4; }
	inline Text_t356221433 ** get_address_of_Text_4() { return &___Text_4; }
	inline void set_Text_4(Text_t356221433 * value)
	{
		___Text_4 = value;
		Il2CppCodeGenWriteBarrier((&___Text_4), value);
	}

	inline static int32_t get_offset_of_ReceiveCondition_5() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ___ReceiveCondition_5)); }
	inline Func_1_t1485000104 * get_ReceiveCondition_5() const { return ___ReceiveCondition_5; }
	inline Func_1_t1485000104 ** get_address_of_ReceiveCondition_5() { return &___ReceiveCondition_5; }
	inline void set_ReceiveCondition_5(Func_1_t1485000104 * value)
	{
		___ReceiveCondition_5 = value;
		Il2CppCodeGenWriteBarrier((&___ReceiveCondition_5), value);
	}

	inline static int32_t get_offset_of_OnClick_6() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ___OnClick_6)); }
	inline Action_t3226471752 * get_OnClick_6() const { return ___OnClick_6; }
	inline Action_t3226471752 ** get_address_of_OnClick_6() { return &___OnClick_6; }
	inline void set_OnClick_6(Action_t3226471752 * value)
	{
		___OnClick_6 = value;
		Il2CppCodeGenWriteBarrier((&___OnClick_6), value);
	}

	inline static int32_t get_offset_of_OnReceived_7() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ___OnReceived_7)); }
	inline Action_t3226471752 * get_OnReceived_7() const { return ___OnReceived_7; }
	inline Action_t3226471752 ** get_address_of_OnReceived_7() { return &___OnReceived_7; }
	inline void set_OnReceived_7(Action_t3226471752 * value)
	{
		___OnReceived_7 = value;
		Il2CppCodeGenWriteBarrier((&___OnReceived_7), value);
	}

	inline static int32_t get_offset_of_Pulse_8() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ___Pulse_8)); }
	inline Tweener_t760404022 * get_Pulse_8() const { return ___Pulse_8; }
	inline Tweener_t760404022 ** get_address_of_Pulse_8() { return &___Pulse_8; }
	inline void set_Pulse_8(Tweener_t760404022 * value)
	{
		___Pulse_8 = value;
		Il2CppCodeGenWriteBarrier((&___Pulse_8), value);
	}

	inline static int32_t get_offset_of__panel_9() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ____panel_9)); }
	inline NotificationPanel_t4176433925 * get__panel_9() const { return ____panel_9; }
	inline NotificationPanel_t4176433925 ** get_address_of__panel_9() { return &____panel_9; }
	inline void set__panel_9(NotificationPanel_t4176433925 * value)
	{
		____panel_9 = value;
		Il2CppCodeGenWriteBarrier((&____panel_9), value);
	}

	inline static int32_t get_offset_of__hintTime_10() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ____hintTime_10)); }
	inline float get__hintTime_10() const { return ____hintTime_10; }
	inline float* get_address_of__hintTime_10() { return &____hintTime_10; }
	inline void set__hintTime_10(float value)
	{
		____hintTime_10 = value;
	}

	inline static int32_t get_offset_of__mouseOver_11() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ____mouseOver_11)); }
	inline bool get__mouseOver_11() const { return ____mouseOver_11; }
	inline bool* get_address_of__mouseOver_11() { return &____mouseOver_11; }
	inline void set__mouseOver_11(bool value)
	{
		____mouseOver_11 = value;
	}

	inline static int32_t get_offset_of__showTime_12() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ____showTime_12)); }
	inline float get__showTime_12() const { return ____showTime_12; }
	inline float* get_address_of__showTime_12() { return &____showTime_12; }
	inline void set__showTime_12(float value)
	{
		____showTime_12 = value;
	}

	inline static int32_t get_offset_of__timeMode_13() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ____timeMode_13)); }
	inline bool get__timeMode_13() const { return ____timeMode_13; }
	inline bool* get_address_of__timeMode_13() { return &____timeMode_13; }
	inline void set__timeMode_13(bool value)
	{
		____timeMode_13 = value;
	}

	inline static int32_t get_offset_of__received_14() { return static_cast<int32_t>(offsetof(NotificationItem_t205362912, ____received_14)); }
	inline bool get__received_14() const { return ____received_14; }
	inline bool* get_address_of__received_14() { return &____received_14; }
	inline void set__received_14(bool value)
	{
		____received_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTIFICATIONITEM_T205362912_H
#ifndef PURCHASABLE_T3960141696_H
#define PURCHASABLE_T3960141696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Purchasable
struct  Purchasable_t3960141696  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASABLE_T3960141696_H
#ifndef UNITYARGENERATEPLANE_T3368998101_H
#define UNITYARGENERATEPLANE_T3368998101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.iOS.UnityARGeneratePlane
struct  UnityARGeneratePlane_t3368998101  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject UnityEngine.XR.iOS.UnityARGeneratePlane::planePrefab
	GameObject_t1756533147 * ___planePrefab_2;
	// UnityEngine.XR.iOS.UnityARAnchorManager UnityEngine.XR.iOS.UnityARGeneratePlane::unityARAnchorManager
	UnityARAnchorManager_t1086564192 * ___unityARAnchorManager_3;

public:
	inline static int32_t get_offset_of_planePrefab_2() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t3368998101, ___planePrefab_2)); }
	inline GameObject_t1756533147 * get_planePrefab_2() const { return ___planePrefab_2; }
	inline GameObject_t1756533147 ** get_address_of_planePrefab_2() { return &___planePrefab_2; }
	inline void set_planePrefab_2(GameObject_t1756533147 * value)
	{
		___planePrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___planePrefab_2), value);
	}

	inline static int32_t get_offset_of_unityARAnchorManager_3() { return static_cast<int32_t>(offsetof(UnityARGeneratePlane_t3368998101, ___unityARAnchorManager_3)); }
	inline UnityARAnchorManager_t1086564192 * get_unityARAnchorManager_3() const { return ___unityARAnchorManager_3; }
	inline UnityARAnchorManager_t1086564192 ** get_address_of_unityARAnchorManager_3() { return &___unityARAnchorManager_3; }
	inline void set_unityARAnchorManager_3(UnityARAnchorManager_t1086564192 * value)
	{
		___unityARAnchorManager_3 = value;
		Il2CppCodeGenWriteBarrier((&___unityARAnchorManager_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYARGENERATEPLANE_T3368998101_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef MODEL_1_T3210376967_H
#define MODEL_1_T3210376967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Model`1<ConditionMessage>
struct  Model_1_t3210376967  : public CanvasFadeColorGroup_t1202652588
{
public:
	// ViewManager Model`1::<IModel.ViewManager>k__BackingField
	ViewManager_t2308013472 * ___U3CIModel_ViewManagerU3Ek__BackingField_7;
	// DialogResultType Model`1::<DialogResult>k__BackingField
	int32_t ___U3CDialogResultU3Ek__BackingField_8;
	// IModel Model`1::<Parent>k__BackingField
	RuntimeObject* ___U3CParentU3Ek__BackingField_9;
	// System.Boolean Model`1::IsTransparent
	bool ___IsTransparent_10;
	// UnityEngine.Events.UnityEvent Model`1::OnOutsideTap
	UnityEvent_t408735097 * ___OnOutsideTap_11;
	// UnityEngine.Events.UnityAction`1<IModel> Model`1::OnClosed
	UnityAction_1_t3603383751 * ___OnClosed_12;
	// UnityEngine.Events.UnityAction Model`1::OnShowed
	UnityAction_t4025899511 * ___OnShowed_13;
	// System.Func`1<System.Boolean> Model`1::CloseCondition
	Func_1_t1485000104 * ___CloseCondition_14;

public:
	inline static int32_t get_offset_of_U3CIModel_ViewManagerU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Model_1_t3210376967, ___U3CIModel_ViewManagerU3Ek__BackingField_7)); }
	inline ViewManager_t2308013472 * get_U3CIModel_ViewManagerU3Ek__BackingField_7() const { return ___U3CIModel_ViewManagerU3Ek__BackingField_7; }
	inline ViewManager_t2308013472 ** get_address_of_U3CIModel_ViewManagerU3Ek__BackingField_7() { return &___U3CIModel_ViewManagerU3Ek__BackingField_7; }
	inline void set_U3CIModel_ViewManagerU3Ek__BackingField_7(ViewManager_t2308013472 * value)
	{
		___U3CIModel_ViewManagerU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIModel_ViewManagerU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CDialogResultU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Model_1_t3210376967, ___U3CDialogResultU3Ek__BackingField_8)); }
	inline int32_t get_U3CDialogResultU3Ek__BackingField_8() const { return ___U3CDialogResultU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CDialogResultU3Ek__BackingField_8() { return &___U3CDialogResultU3Ek__BackingField_8; }
	inline void set_U3CDialogResultU3Ek__BackingField_8(int32_t value)
	{
		___U3CDialogResultU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Model_1_t3210376967, ___U3CParentU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CParentU3Ek__BackingField_9() const { return ___U3CParentU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CParentU3Ek__BackingField_9() { return &___U3CParentU3Ek__BackingField_9; }
	inline void set_U3CParentU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CParentU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_IsTransparent_10() { return static_cast<int32_t>(offsetof(Model_1_t3210376967, ___IsTransparent_10)); }
	inline bool get_IsTransparent_10() const { return ___IsTransparent_10; }
	inline bool* get_address_of_IsTransparent_10() { return &___IsTransparent_10; }
	inline void set_IsTransparent_10(bool value)
	{
		___IsTransparent_10 = value;
	}

	inline static int32_t get_offset_of_OnOutsideTap_11() { return static_cast<int32_t>(offsetof(Model_1_t3210376967, ___OnOutsideTap_11)); }
	inline UnityEvent_t408735097 * get_OnOutsideTap_11() const { return ___OnOutsideTap_11; }
	inline UnityEvent_t408735097 ** get_address_of_OnOutsideTap_11() { return &___OnOutsideTap_11; }
	inline void set_OnOutsideTap_11(UnityEvent_t408735097 * value)
	{
		___OnOutsideTap_11 = value;
		Il2CppCodeGenWriteBarrier((&___OnOutsideTap_11), value);
	}

	inline static int32_t get_offset_of_OnClosed_12() { return static_cast<int32_t>(offsetof(Model_1_t3210376967, ___OnClosed_12)); }
	inline UnityAction_1_t3603383751 * get_OnClosed_12() const { return ___OnClosed_12; }
	inline UnityAction_1_t3603383751 ** get_address_of_OnClosed_12() { return &___OnClosed_12; }
	inline void set_OnClosed_12(UnityAction_1_t3603383751 * value)
	{
		___OnClosed_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnClosed_12), value);
	}

	inline static int32_t get_offset_of_OnShowed_13() { return static_cast<int32_t>(offsetof(Model_1_t3210376967, ___OnShowed_13)); }
	inline UnityAction_t4025899511 * get_OnShowed_13() const { return ___OnShowed_13; }
	inline UnityAction_t4025899511 ** get_address_of_OnShowed_13() { return &___OnShowed_13; }
	inline void set_OnShowed_13(UnityAction_t4025899511 * value)
	{
		___OnShowed_13 = value;
		Il2CppCodeGenWriteBarrier((&___OnShowed_13), value);
	}

	inline static int32_t get_offset_of_CloseCondition_14() { return static_cast<int32_t>(offsetof(Model_1_t3210376967, ___CloseCondition_14)); }
	inline Func_1_t1485000104 * get_CloseCondition_14() const { return ___CloseCondition_14; }
	inline Func_1_t1485000104 ** get_address_of_CloseCondition_14() { return &___CloseCondition_14; }
	inline void set_CloseCondition_14(Func_1_t1485000104 * value)
	{
		___CloseCondition_14 = value;
		Il2CppCodeGenWriteBarrier((&___CloseCondition_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODEL_1_T3210376967_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef CONDITIONMESSAGE_T972384076_H
#define CONDITIONMESSAGE_T972384076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConditionMessage
struct  ConditionMessage_t972384076  : public Model_1_t3210376967
{
public:
	// System.Func`1<System.Boolean> ConditionMessage::ReceiveCondition
	Func_1_t1485000104 * ___ReceiveCondition_15;

public:
	inline static int32_t get_offset_of_ReceiveCondition_15() { return static_cast<int32_t>(offsetof(ConditionMessage_t972384076, ___ReceiveCondition_15)); }
	inline Func_1_t1485000104 * get_ReceiveCondition_15() const { return ___ReceiveCondition_15; }
	inline Func_1_t1485000104 ** get_address_of_ReceiveCondition_15() { return &___ReceiveCondition_15; }
	inline void set_ReceiveCondition_15(Func_1_t1485000104 * value)
	{
		___ReceiveCondition_15 = value;
		Il2CppCodeGenWriteBarrier((&___ReceiveCondition_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONMESSAGE_T972384076_H
#ifndef RAWIMAGE_T2749640213_H
#define RAWIMAGE_T2749640213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RawImage
struct  RawImage_t2749640213  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t2243626319 * ___m_Texture_28;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t3681755626  ___m_UVRect_29;

public:
	inline static int32_t get_offset_of_m_Texture_28() { return static_cast<int32_t>(offsetof(RawImage_t2749640213, ___m_Texture_28)); }
	inline Texture_t2243626319 * get_m_Texture_28() const { return ___m_Texture_28; }
	inline Texture_t2243626319 ** get_address_of_m_Texture_28() { return &___m_Texture_28; }
	inline void set_m_Texture_28(Texture_t2243626319 * value)
	{
		___m_Texture_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_28), value);
	}

	inline static int32_t get_offset_of_m_UVRect_29() { return static_cast<int32_t>(offsetof(RawImage_t2749640213, ___m_UVRect_29)); }
	inline Rect_t3681755626  get_m_UVRect_29() const { return ___m_UVRect_29; }
	inline Rect_t3681755626 * get_address_of_m_UVRect_29() { return &___m_UVRect_29; }
	inline void set_m_UVRect_29(Rect_t3681755626  value)
	{
		___m_UVRect_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T2749640213_H
#ifndef MINIMAP_T2890438425_H
#define MINIMAP_T2890438425_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniMap
struct  MiniMap_t2890438425  : public RawImage_t2749640213
{
public:
	// UnityEngine.Texture2D MiniMap::BackTexture
	Texture2D_t3542995729 * ___BackTexture_30;
	// UnityEngine.Vector3 MiniMap::topLeftPosition
	Vector3_t2243707580  ___topLeftPosition_31;
	// UnityEngine.Vector3 MiniMap::topRightPosition
	Vector3_t2243707580  ___topRightPosition_32;
	// UnityEngine.Vector3 MiniMap::bottomLeftPosition
	Vector3_t2243707580  ___bottomLeftPosition_33;
	// UnityEngine.Vector3 MiniMap::bottomRightPosition
	Vector3_t2243707580  ___bottomRightPosition_34;
	// UnityEngine.Camera MiniMap::_globalCamera
	Camera_t189460977 * ____globalCamera_35;
	// UnityEngine.BoxCollider MiniMap::_floorCollider
	BoxCollider_t22920061 * ____floorCollider_36;
	// UnityEngine.Material MiniMap::_material
	Material_t193706927 * ____material_37;
	// UnityEngine.Camera MiniMap::_camera
	Camera_t189460977 * ____camera_38;
	// System.Collections.Generic.List`1<MiniMap/MiniMapPoint> MiniMap::_points
	List_1_t4033011997 * ____points_39;

public:
	inline static int32_t get_offset_of_BackTexture_30() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425, ___BackTexture_30)); }
	inline Texture2D_t3542995729 * get_BackTexture_30() const { return ___BackTexture_30; }
	inline Texture2D_t3542995729 ** get_address_of_BackTexture_30() { return &___BackTexture_30; }
	inline void set_BackTexture_30(Texture2D_t3542995729 * value)
	{
		___BackTexture_30 = value;
		Il2CppCodeGenWriteBarrier((&___BackTexture_30), value);
	}

	inline static int32_t get_offset_of_topLeftPosition_31() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425, ___topLeftPosition_31)); }
	inline Vector3_t2243707580  get_topLeftPosition_31() const { return ___topLeftPosition_31; }
	inline Vector3_t2243707580 * get_address_of_topLeftPosition_31() { return &___topLeftPosition_31; }
	inline void set_topLeftPosition_31(Vector3_t2243707580  value)
	{
		___topLeftPosition_31 = value;
	}

	inline static int32_t get_offset_of_topRightPosition_32() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425, ___topRightPosition_32)); }
	inline Vector3_t2243707580  get_topRightPosition_32() const { return ___topRightPosition_32; }
	inline Vector3_t2243707580 * get_address_of_topRightPosition_32() { return &___topRightPosition_32; }
	inline void set_topRightPosition_32(Vector3_t2243707580  value)
	{
		___topRightPosition_32 = value;
	}

	inline static int32_t get_offset_of_bottomLeftPosition_33() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425, ___bottomLeftPosition_33)); }
	inline Vector3_t2243707580  get_bottomLeftPosition_33() const { return ___bottomLeftPosition_33; }
	inline Vector3_t2243707580 * get_address_of_bottomLeftPosition_33() { return &___bottomLeftPosition_33; }
	inline void set_bottomLeftPosition_33(Vector3_t2243707580  value)
	{
		___bottomLeftPosition_33 = value;
	}

	inline static int32_t get_offset_of_bottomRightPosition_34() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425, ___bottomRightPosition_34)); }
	inline Vector3_t2243707580  get_bottomRightPosition_34() const { return ___bottomRightPosition_34; }
	inline Vector3_t2243707580 * get_address_of_bottomRightPosition_34() { return &___bottomRightPosition_34; }
	inline void set_bottomRightPosition_34(Vector3_t2243707580  value)
	{
		___bottomRightPosition_34 = value;
	}

	inline static int32_t get_offset_of__globalCamera_35() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425, ____globalCamera_35)); }
	inline Camera_t189460977 * get__globalCamera_35() const { return ____globalCamera_35; }
	inline Camera_t189460977 ** get_address_of__globalCamera_35() { return &____globalCamera_35; }
	inline void set__globalCamera_35(Camera_t189460977 * value)
	{
		____globalCamera_35 = value;
		Il2CppCodeGenWriteBarrier((&____globalCamera_35), value);
	}

	inline static int32_t get_offset_of__floorCollider_36() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425, ____floorCollider_36)); }
	inline BoxCollider_t22920061 * get__floorCollider_36() const { return ____floorCollider_36; }
	inline BoxCollider_t22920061 ** get_address_of__floorCollider_36() { return &____floorCollider_36; }
	inline void set__floorCollider_36(BoxCollider_t22920061 * value)
	{
		____floorCollider_36 = value;
		Il2CppCodeGenWriteBarrier((&____floorCollider_36), value);
	}

	inline static int32_t get_offset_of__material_37() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425, ____material_37)); }
	inline Material_t193706927 * get__material_37() const { return ____material_37; }
	inline Material_t193706927 ** get_address_of__material_37() { return &____material_37; }
	inline void set__material_37(Material_t193706927 * value)
	{
		____material_37 = value;
		Il2CppCodeGenWriteBarrier((&____material_37), value);
	}

	inline static int32_t get_offset_of__camera_38() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425, ____camera_38)); }
	inline Camera_t189460977 * get__camera_38() const { return ____camera_38; }
	inline Camera_t189460977 ** get_address_of__camera_38() { return &____camera_38; }
	inline void set__camera_38(Camera_t189460977 * value)
	{
		____camera_38 = value;
		Il2CppCodeGenWriteBarrier((&____camera_38), value);
	}

	inline static int32_t get_offset_of__points_39() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425, ____points_39)); }
	inline List_1_t4033011997 * get__points_39() const { return ____points_39; }
	inline List_1_t4033011997 ** get_address_of__points_39() { return &____points_39; }
	inline void set__points_39(List_1_t4033011997 * value)
	{
		____points_39 = value;
		Il2CppCodeGenWriteBarrier((&____points_39), value);
	}
};

struct MiniMap_t2890438425_StaticFields
{
public:
	// System.Predicate`1<MiniMap/MiniMapPoint> MiniMap::<>f__am$cache0
	Predicate_1_t3106860980 * ___U3CU3Ef__amU24cache0_40;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_40() { return static_cast<int32_t>(offsetof(MiniMap_t2890438425_StaticFields, ___U3CU3Ef__amU24cache0_40)); }
	inline Predicate_1_t3106860980 * get_U3CU3Ef__amU24cache0_40() const { return ___U3CU3Ef__amU24cache0_40; }
	inline Predicate_1_t3106860980 ** get_address_of_U3CU3Ef__amU24cache0_40() { return &___U3CU3Ef__amU24cache0_40; }
	inline void set_U3CU3Ef__amU24cache0_40(Predicate_1_t3106860980 * value)
	{
		___U3CU3Ef__amU24cache0_40 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_40), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIMAP_T2890438425_H
#ifndef VIEWPORT3D_T2082711749_H
#define VIEWPORT3D_T2082711749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewPort3D
struct  ViewPort3D_t2082711749  : public RawImage_t2749640213
{
public:
	// UnityEngine.Camera ViewPort3D::<_camera>k__BackingField
	Camera_t189460977 * ___U3C_cameraU3Ek__BackingField_30;
	// UnityEngine.GameObject ViewPort3D::<Model>k__BackingField
	GameObject_t1756533147 * ___U3CModelU3Ek__BackingField_31;

public:
	inline static int32_t get_offset_of_U3C_cameraU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(ViewPort3D_t2082711749, ___U3C_cameraU3Ek__BackingField_30)); }
	inline Camera_t189460977 * get_U3C_cameraU3Ek__BackingField_30() const { return ___U3C_cameraU3Ek__BackingField_30; }
	inline Camera_t189460977 ** get_address_of_U3C_cameraU3Ek__BackingField_30() { return &___U3C_cameraU3Ek__BackingField_30; }
	inline void set_U3C_cameraU3Ek__BackingField_30(Camera_t189460977 * value)
	{
		___U3C_cameraU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_cameraU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CModelU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(ViewPort3D_t2082711749, ___U3CModelU3Ek__BackingField_31)); }
	inline GameObject_t1756533147 * get_U3CModelU3Ek__BackingField_31() const { return ___U3CModelU3Ek__BackingField_31; }
	inline GameObject_t1756533147 ** get_address_of_U3CModelU3Ek__BackingField_31() { return &___U3CModelU3Ek__BackingField_31; }
	inline void set_U3CModelU3Ek__BackingField_31(GameObject_t1756533147 * value)
	{
		___U3CModelU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CModelU3Ek__BackingField_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWPORT3D_T2082711749_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (UnityARGeneratePlane_t3368998101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[2] = 
{
	UnityARGeneratePlane_t3368998101::get_offset_of_planePrefab_2(),
	UnityARGeneratePlane_t3368998101::get_offset_of_unityARAnchorManager_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (UnityARHitTestExample_t146867607), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[1] = 
{
	UnityARHitTestExample_t146867607::get_offset_of_m_HitTransform_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (UnityARKitControl_t1698990409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[6] = 
{
	UnityARKitControl_t1698990409::get_offset_of_runOptions_2(),
	UnityARKitControl_t1698990409::get_offset_of_alignmentOptions_3(),
	UnityARKitControl_t1698990409::get_offset_of_planeOptions_4(),
	UnityARKitControl_t1698990409::get_offset_of_currentOptionIndex_5(),
	UnityARKitControl_t1698990409::get_offset_of_currentAlignmentIndex_6(),
	UnityARKitControl_t1698990409::get_offset_of_currentPlaneIndex_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (UnityARUserAnchorComponent_t3596724887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	UnityARUserAnchorComponent_t3596724887::get_offset_of_m_AnchorId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (UnityARVideo_t2351297253), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[6] = 
{
	UnityARVideo_t2351297253::get_offset_of_m_ClearMaterial_2(),
	UnityARVideo_t2351297253::get_offset_of_m_VideoCommandBuffer_3(),
	UnityARVideo_t2351297253::get_offset_of__videoTextureY_4(),
	UnityARVideo_t2351297253::get_offset_of__videoTextureCbCr_5(),
	UnityARVideo_t2351297253::get_offset_of_m_Session_6(),
	UnityARVideo_t2351297253::get_offset_of_bCommandBufferInitialized_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (UnityPointCloudExample_t3196264220), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[4] = 
{
	UnityPointCloudExample_t3196264220::get_offset_of_numPointsToShow_2(),
	UnityPointCloudExample_t3196264220::get_offset_of_PointCloudPrefab_3(),
	UnityPointCloudExample_t3196264220::get_offset_of_pointCloudObjects_4(),
	UnityPointCloudExample_t3196264220::get_offset_of_m_PointCloudData_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (UnityARAnchorManager_t1086564192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[1] = 
{
	UnityARAnchorManager_t1086564192::get_offset_of_planeAnchorMap_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (UnityARMatrixOps_t4039665643), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (UnityARUtility_t3608388148), -1, sizeof(UnityARUtility_t3608388148_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2108[3] = 
{
	UnityARUtility_t3608388148::get_offset_of_meshCollider_0(),
	UnityARUtility_t3608388148::get_offset_of_meshFilter_1(),
	UnityARUtility_t3608388148_StaticFields::get_offset_of_planePrefab_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (UnityARAmbient_t680084560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2109[2] = 
{
	UnityARAmbient_t680084560::get_offset_of_l_2(),
	UnityARAmbient_t680084560::get_offset_of_m_Session_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (Basics_t2766259351), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[2] = 
{
	Basics_t2766259351::get_offset_of_cubeA_2(),
	Basics_t2766259351::get_offset_of_cubeB_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (Sequences_t3565862770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[1] = 
{
	Sequences_t3565862770::get_offset_of_target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (AnimatedSprite_t2222380888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2112[9] = 
{
	AnimatedSprite_t2222380888::get_offset_of_spriteMaterial_2(),
	AnimatedSprite_t2222380888::get_offset_of_animations_3(),
	AnimatedSprite_t2222380888::get_offset_of_animNames_4(),
	AnimatedSprite_t2222380888::get_offset_of_lastAnimFrameTime_5(),
	AnimatedSprite_t2222380888::get_offset_of_frame_6(),
	AnimatedSprite_t2222380888::get_offset_of_frameStep_7(),
	AnimatedSprite_t2222380888::get_offset_of_anim_8(),
	AnimatedSprite_t2222380888::get_offset_of_TM_9(),
	AnimatedSprite_t2222380888::get_offset_of_TMR_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (theLoopBehaviour_t801943279)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2113[5] = 
{
	theLoopBehaviour_t801943279::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (Utils_t4194145797), -1, sizeof(Utils_t4194145797_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2114[1] = 
{
	Utils_t4194145797_StaticFields::get_offset_of__md5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (Drawing_t724524520), -1, sizeof(Drawing_t724524520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2116[1] = 
{
	Drawing_t724524520_StaticFields::get_offset_of_lineTex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (CameraFollowTarget_t514838119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[4] = 
{
	CameraFollowTarget_t514838119::get_offset_of_Offset_2(),
	CameraFollowTarget_t514838119::get_offset_of_Rotation_3(),
	CameraFollowTarget_t514838119::get_offset_of_PositionSoftness_4(),
	CameraFollowTarget_t514838119::get_offset_of_RotationSoftness_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (U421haseMover_t4119473191), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2119[3] = 
{
	U421haseMover_t4119473191::get_offset_of__destination_2(),
	U421haseMover_t4119473191::get_offset_of__speed_3(),
	U421haseMover_t4119473191::get_offset_of_OnCatch_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (CircleRenderer_t1868761029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[3] = 
{
	CircleRenderer_t1868761029::get_offset_of__slices_2(),
	CircleRenderer_t1868761029::get_offset_of__outRadius_3(),
	CircleRenderer_t1868761029::get_offset_of__inRadius_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2130[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (SpriteCollection_t2762286977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[2] = 
{
	SpriteCollection_t2762286977::get_offset_of__sprites_0(),
	SpriteCollection_t2762286977::get_offset_of__names_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (WeightedOptions_t481347131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[1] = 
{
	WeightedOptions_t481347131::get_offset_of__totalWeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (CursorDesc_t4292080425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2134[2] = 
{
	CursorDesc_t4292080425::get_offset_of_Texture_2(),
	CursorDesc_t4292080425::get_offset_of_Offset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (EdgeHelpers_t748135790), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (Edge_t3361754850)+ sizeof (RuntimeObject), sizeof(Edge_t3361754850 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2136[3] = 
{
	Edge_t3361754850::get_offset_of_v1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Edge_t3361754850::get_offset_of_v2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Edge_t3361754850::get_offset_of_triangleIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (Extentions_t847016823), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (FastLineClip_t2452686242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2140[8] = 
{
	FastLineClip_t2452686242::get_offset_of_FC_xn_0(),
	FastLineClip_t2452686242::get_offset_of_FC_yn_1(),
	FastLineClip_t2452686242::get_offset_of_FC_xk_2(),
	FastLineClip_t2452686242::get_offset_of_FC_yk_3(),
	FastLineClip_t2452686242::get_offset_of_Wxlef_4(),
	FastLineClip_t2452686242::get_offset_of_Wxrig_5(),
	FastLineClip_t2452686242::get_offset_of_Wytop_6(),
	FastLineClip_t2452686242::get_offset_of_Wybot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2142[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (ADHost_t4034195185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[5] = 
{
	ADHost_t4034195185::get_offset_of_onRewardedVideoAdOpened_2(),
	ADHost_t4034195185::get_offset_of_onRewardedVideoAdClosed_3(),
	ADHost_t4034195185::get_offset_of_onRewardedVideoStart_4(),
	ADHost_t4034195185::get_offset_of_onRewardedVideoStop_5(),
	ADHost_t4034195185::get_offset_of_onRewardedVideoDidFinished_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (ADEvent_t127799521), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (AnalyticsController_t3574332020), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2145[7] = 
{
	AnalyticsController_t3574332020::get_offset_of_isValidState_0(),
	AnalyticsController_t3574332020::get_offset_of_sessionTime_1(),
	AnalyticsController_t3574332020::get_offset_of_sessionStartedWithLevelIndex_2(),
	0,
	0,
	AnalyticsController_t3574332020::get_offset_of_U3CUserGenderU3Ek__BackingField_5(),
	AnalyticsController_t3574332020::get_offset_of_U3CUserAgeU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (DataBaseAccess_t3594830723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2147[1] = 
{
	DataBaseAccess_t3594830723::get_offset_of_ServerURL_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (LoadRemoteSource_t3573554701), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2149[6] = 
{
	U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866::get_offset_of_imgURL_0(),
	U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866::get_offset_of_U3CwwwU3E__0_1(),
	U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866::get_offset_of_callback_2(),
	U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866::get_offset_of_U24current_3(),
	U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866::get_offset_of_U24disposing_4(),
	U3CLoadImgEnumeratorU3Ec__Iterator0_t2613224866::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (Coroutiner_t3262866136), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (CoroutinerInstance_t1991176683), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[5] = 
{
	U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810::get_offset_of_iterationResult_0(),
	U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810::get_offset_of_U24this_1(),
	U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810::get_offset_of_U24current_2(),
	U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810::get_offset_of_U24disposing_3(),
	U3CDestroyWhenCompleteU3Ec__Iterator0_t3483178810::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (GraphUtil_t2142534788), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[6] = 
{
	U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583::get_offset_of_imgURL_0(),
	U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583::get_offset_of_U3CwwwU3E__0_1(),
	U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583::get_offset_of_callback_2(),
	U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583::get_offset_of_U24current_3(),
	U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583::get_offset_of_U24disposing_4(),
	U3CLoadImgEnumeratorU3Ec__Iterator0_t1326606583::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (Player_t1147783557), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[9] = 
{
	Player_t1147783557::get_offset_of_Icon_0(),
	Player_t1147783557::get_offset_of_PatchIcon_1(),
	Player_t1147783557::get_offset_of_UserName_2(),
	Player_t1147783557::get_offset_of_UserForname_3(),
	Player_t1147783557::get_offset_of_InGame_4(),
	Player_t1147783557::get_offset_of_LiveCount_5(),
	Player_t1147783557::get_offset_of_LevelIndex_6(),
	Player_t1147783557::get_offset_of_Score_7(),
	Player_t1147783557::get_offset_of_WallPassLevel_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (U3CLoadIconU3Ec__AnonStorey0_t1644789636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[2] = 
{
	U3CLoadIconU3Ec__AnonStorey0_t1644789636::get_offset_of_func_0(),
	U3CLoadIconU3Ec__AnonStorey0_t1644789636::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (ProductStash_t505074360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[4] = 
{
	ProductStash_t505074360::get_offset_of_count_0(),
	ProductStash_t505074360::get_offset_of_IDs_1(),
	ProductStash_t505074360::get_offset_of_receipts_2(),
	ProductStash_t505074360::get_offset_of_noData_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (Purchasable_t3960141696), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (Localization_t3725902693), -1, sizeof(Localization_t3725902693_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2159[5] = 
{
	Localization_t3725902693::get_offset_of_Data_2(),
	Localization_t3725902693::get_offset_of_Keys_3(),
	Localization_t3725902693::get_offset_of_currentLanguageData_4(),
	Localization_t3725902693_StaticFields::get_offset_of__instance_5(),
	Localization_t3725902693_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (U3CSetLanguageU3Ec__AnonStorey0_t266885935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2160[1] = 
{
	U3CSetLanguageU3Ec__AnonStorey0_t266885935::get_offset_of_lang_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (U3CGetU3Ec__AnonStorey1_t2426557540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[1] = 
{
	U3CGetU3Ec__AnonStorey1_t2426557540::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (U3CGetU3Ec__AnonStorey2_t860473599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[1] = 
{
	U3CGetU3Ec__AnonStorey2_t860473599::get_offset_of_key_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (LocalizationLanguageData_t2789729839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[2] = 
{
	LocalizationLanguageData_t2789729839::get_offset_of_Lang_0(),
	LocalizationLanguageData_t2789729839::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (ParticlesCollection_t658639267), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2164[1] = 
{
	ParticlesCollection_t658639267::get_offset_of__effects_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (U3CEmmitU3Ec__AnonStorey0_t1718196965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[1] = 
{
	U3CEmmitU3Ec__AnonStorey0_t1718196965::get_offset_of_particleName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (SoundCollection_t2692967059), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	SoundCollection_t2692967059::get_offset_of__clips_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (U3CGetClipU3Ec__AnonStorey0_t3223970893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[1] = 
{
	U3CGetClipU3Ec__AnonStorey0_t3223970893::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (AudioGroupTypes_t423657466)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2168[5] = 
{
	AudioGroupTypes_t423657466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (SoundManager_t654432262), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[15] = 
{
	0,
	0,
	SoundManager_t654432262::get_offset_of_AudioMixer_4(),
	SoundManager_t654432262::get_offset_of_GroupMaster_5(),
	SoundManager_t654432262::get_offset_of_GroupMusic_6(),
	SoundManager_t654432262::get_offset_of_GroupSFX_7(),
	SoundManager_t654432262::get_offset_of_GroupSpeaking_8(),
	SoundManager_t654432262::get_offset_of_U3CAutoChangeMusicsU3Ek__BackingField_9(),
	SoundManager_t654432262::get_offset_of_PitchVariationsRange_10(),
	SoundManager_t654432262::get_offset_of_U3C_masterMusicSourceU3Ek__BackingField_11(),
	SoundManager_t654432262::get_offset_of_U3C_secondMusicSourceU3Ek__BackingField_12(),
	SoundManager_t654432262::get_offset_of_U3C_fadeMusicsU3Ek__BackingField_13(),
	SoundManager_t654432262::get_offset_of__audioSourcesCleanap_14(),
	SoundManager_t654432262::get_offset_of__musics_15(),
	SoundManager_t654432262::get_offset_of__sounds_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (U3CPlaySoundU3Ec__AnonStorey0_t3955832979), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[1] = 
{
	U3CPlaySoundU3Ec__AnonStorey0_t3955832979::get_offset_of_clip_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (CanvasFadeColorGroup_t1202652588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[5] = 
{
	CanvasFadeColorGroup_t1202652588::get_offset_of__color_2(),
	CanvasFadeColorGroup_t1202652588::get_offset_of__fadeColor_3(),
	CanvasFadeColorGroup_t1202652588::get_offset_of__fadeIn_4(),
	CanvasFadeColorGroup_t1202652588::get_offset_of__fadeOut_5(),
	CanvasFadeColorGroup_t1202652588::get_offset_of__fadeTweener_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (ConditionMessage_t972384076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[1] = 
{
	ConditionMessage_t972384076::get_offset_of_ReceiveCondition_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (DialogResultType_t3218470507)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2174[6] = 
{
	DialogResultType_t3218470507::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (Extensions_t612262650), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (NotificationItem_t205362912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2178[13] = 
{
	NotificationItem_t205362912::get_offset_of_U3CHintKeyU3Ek__BackingField_2(),
	NotificationItem_t205362912::get_offset_of_Image_3(),
	NotificationItem_t205362912::get_offset_of_Text_4(),
	NotificationItem_t205362912::get_offset_of_ReceiveCondition_5(),
	NotificationItem_t205362912::get_offset_of_OnClick_6(),
	NotificationItem_t205362912::get_offset_of_OnReceived_7(),
	NotificationItem_t205362912::get_offset_of_Pulse_8(),
	NotificationItem_t205362912::get_offset_of__panel_9(),
	NotificationItem_t205362912::get_offset_of__hintTime_10(),
	NotificationItem_t205362912::get_offset_of__mouseOver_11(),
	NotificationItem_t205362912::get_offset_of__showTime_12(),
	NotificationItem_t205362912::get_offset_of__timeMode_13(),
	NotificationItem_t205362912::get_offset_of__received_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (NotificationPanel_t4176433925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[3] = 
{
	NotificationPanel_t4176433925::get_offset_of_NotifySprite_2(),
	NotificationPanel_t4176433925::get_offset_of_NotifyZone_3(),
	NotificationPanel_t4176433925::get_offset_of__items_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (U3CNotifyU3Ec__AnonStorey0_t3711104398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2180[1] = 
{
	U3CNotifyU3Ec__AnonStorey0_t3711104398::get_offset_of_item_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (U3CRemoveNotifyU3Ec__AnonStorey1_t2304509167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2181[1] = 
{
	U3CRemoveNotifyU3Ec__AnonStorey1_t2304509167::get_offset_of_notify_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (ViewCamera_t2275596458), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (ViewManager_t2308013472), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2183[15] = 
{
	ViewManager_t2308013472::get_offset_of__camera_2(),
	ViewManager_t2308013472::get_offset_of__modelsList_3(),
	ViewManager_t2308013472::get_offset_of_NotifyPanel_4(),
	ViewManager_t2308013472::get_offset_of_FadePanel_5(),
	ViewManager_t2308013472::get_offset_of_FadeScreen_6(),
	ViewManager_t2308013472::get_offset_of_ScreenFadeTime_7(),
	ViewManager_t2308013472::get_offset_of_ScreenFadeMax_8(),
	ViewManager_t2308013472::get_offset_of_ViewsFadeTime_9(),
	ViewManager_t2308013472::get_offset_of_ViewsFadeMax_10(),
	ViewManager_t2308013472::get_offset_of_FadeColor_11(),
	ViewManager_t2308013472::get_offset_of_OutsideTap_12(),
	ViewManager_t2308013472::get_offset_of_OnAllClosed_13(),
	ViewManager_t2308013472::get_offset_of__fadeTweener_14(),
	ViewManager_t2308013472::get_offset_of__fadeIn_15(),
	ViewManager_t2308013472::get_offset_of__fadeOut_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (U3CU3Ec__AnonStorey0_t4058529692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2184[1] = 
{
	U3CU3Ec__AnonStorey0_t4058529692::get_offset_of_upper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (ViewPort3D_t2082711749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[2] = 
{
	ViewPort3D_t2082711749::get_offset_of_U3C_cameraU3Ek__BackingField_30(),
	ViewPort3D_t2082711749::get_offset_of_U3CModelU3Ek__BackingField_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (InstanceMaterial_t2647119882), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (LineDrawer_t3216095815), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2187[3] = 
{
	LineDrawer_t3216095815::get_offset_of_lineMat_2(),
	LineDrawer_t3216095815::get_offset_of_mainPoint_3(),
	LineDrawer_t3216095815::get_offset_of_points_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (MathHelper_t2977985342), -1, sizeof(MathHelper_t2977985342_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2188[2] = 
{
	MathHelper_t2977985342_StaticFields::get_offset_of_TwoPi_0(),
	MathHelper_t2977985342_StaticFields::get_offset_of_PiOwer4_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (Matrix4x4Helper_t2359692911), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (MeshExtension_t3538562906), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (MiniMap_t2890438425), -1, sizeof(MiniMap_t2890438425_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2191[11] = 
{
	MiniMap_t2890438425::get_offset_of_BackTexture_30(),
	MiniMap_t2890438425::get_offset_of_topLeftPosition_31(),
	MiniMap_t2890438425::get_offset_of_topRightPosition_32(),
	MiniMap_t2890438425::get_offset_of_bottomLeftPosition_33(),
	MiniMap_t2890438425::get_offset_of_bottomRightPosition_34(),
	MiniMap_t2890438425::get_offset_of__globalCamera_35(),
	MiniMap_t2890438425::get_offset_of__floorCollider_36(),
	MiniMap_t2890438425::get_offset_of__material_37(),
	MiniMap_t2890438425::get_offset_of__camera_38(),
	MiniMap_t2890438425::get_offset_of__points_39(),
	MiniMap_t2890438425_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (MiniMapPoint_t368923569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[3] = 
{
	MiniMapPoint_t368923569::get_offset_of_Transform_0(),
	MiniMapPoint_t368923569::get_offset_of_Size_1(),
	MiniMapPoint_t368923569::get_offset_of_Color_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (Matrix4x4Ext_t3978015834), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2194[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2195[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (OptionType_t243031973)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2197[5] = 
{
	OptionType_t243031973::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (OptionInfo_t3879499943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[8] = 
{
	OptionInfo_t3879499943::get_offset_of__id_0(),
	OptionInfo_t3879499943::get_offset_of__desc_1(),
	OptionInfo_t3879499943::get_offset_of__cat_2(),
	OptionInfo_t3879499943::get_offset_of__type_3(),
	OptionInfo_t3879499943::get_offset_of_b_ref_4(),
	OptionInfo_t3879499943::get_offset_of_i_ref_5(),
	OptionInfo_t3879499943::get_offset_of_s_ref_6(),
	OptionInfo_t3879499943::get_offset_of_k_ref_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (ParticleAlphaCorrect_t1759117296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[2] = 
{
	ParticleAlphaCorrect_t1759117296::get_offset_of__renderer_2(),
	ParticleAlphaCorrect_t1759117296::get_offset_of__particleSystem_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
