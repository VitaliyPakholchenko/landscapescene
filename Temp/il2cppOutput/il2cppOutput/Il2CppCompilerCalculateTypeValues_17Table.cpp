﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.Transform
struct Transform_t3275118058;
// DG.Tweening.Tween
struct Tween_t278478013;
// DG.Tweening.Plugins.Core.ITweenPlugin
struct ITweenPlugin_t2991430675;
// System.Collections.Generic.Dictionary`2<System.Type,DG.Tweening.Plugins.Core.ITweenPlugin>
struct Dictionary_2_t633821276;
// System.Char[]
struct CharU5BU5D_t1328083999;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.TrailRenderer
struct TrailRenderer_t2490637367;
// System.Void
struct Void_t1841601450;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t2823602470;
// UnityEngine.Material
struct Material_t193706927;
// System.String
struct String_t;
// DG.Tweening.Sequence
struct Sequence_t110643099;
// UnityEngine.Light
struct Light_t494725636;
// DG.Tweening.TweenCallback
struct TweenCallback_t3697142134;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t3418705418;
// DG.Tweening.EaseFunction
struct EaseFunction_t3306356708;
// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct CatmullRomDecoder_t3014762178;
// DG.Tweening.Plugins.Core.PathCore.LinearDecoder
struct LinearDecoder_t2073524639;
// System.Single[]
struct SingleU5BU5D_t577127397;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// DG.Tweening.Plugins.Core.PathCore.ControlPoint[]
struct ControlPointU5BU5D_t246481150;
// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct ABSPathDecoder_t3294469411;
// System.Type
struct Type_t;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ABSTWEENPLUGIN_3_T2158331857_H
#define ABSTWEENPLUGIN_3_T2158331857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions>
struct  ABSTweenPlugin_3_t2158331857  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2158331857_H
#ifndef ABSTWEENPLUGIN_3_T2168888377_H
#define ABSTWEENPLUGIN_3_T2168888377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector4,UnityEngine.Vector4,DG.Tweening.Plugins.Options.VectorOptions>
struct  ABSTweenPlugin_3_t2168888377  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2168888377_H
#ifndef ABSTWEENPLUGIN_3_T3790859801_H
#define ABSTWEENPLUGIN_3_T3790859801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions>
struct  ABSTweenPlugin_3_t3790859801  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T3790859801_H
#ifndef ABSTWEENPLUGIN_3_T882645641_H
#define ABSTWEENPLUGIN_3_T882645641_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.RectOffset,UnityEngine.RectOffset,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t882645641  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T882645641_H
#ifndef TWEENSETTINGSEXTENSIONS_T2285462830_H
#define TWEENSETTINGSEXTENSIONS_T2285462830_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenSettingsExtensions
struct  TweenSettingsExtensions_t2285462830  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENSETTINGSEXTENSIONS_T2285462830_H
#ifndef ABSTWEENPLUGIN_3_T556292748_H
#define ABSTWEENPLUGIN_3_T556292748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt32,System.UInt32,DG.Tweening.Plugins.Options.UintOptions>
struct  ABSTweenPlugin_3_t556292748  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T556292748_H
#ifndef ABSTWEENPLUGIN_3_T1972650634_H
#define ABSTWEENPLUGIN_3_T1972650634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Rect,UnityEngine.Rect,DG.Tweening.Plugins.Options.RectOptions>
struct  ABSTweenPlugin_3_t1972650634  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1972650634_H
#ifndef ABSTWEENPLUGIN_3_T1186869890_H
#define ABSTWEENPLUGIN_3_T1186869890_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions>
struct  ABSTweenPlugin_3_t1186869890  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1186869890_H
#ifndef U3CU3EC__DISPLAYCLASS71_0_T1424691497_H
#define U3CU3EC__DISPLAYCLASS71_0_T1424691497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0
struct  U3CU3Ec__DisplayClass71_0_t1424691497  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass71_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass71_0_t1424691497, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS71_0_T1424691497_H
#ifndef U3CU3EC__DISPLAYCLASS70_0_T3010978414_H
#define U3CU3EC__DISPLAYCLASS70_0_T3010978414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0
struct  U3CU3Ec__DisplayClass70_0_t3010978414  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass70_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass70_0_t3010978414, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS70_0_T3010978414_H
#ifndef ABSTWEENPLUGIN_3_T3304718885_H
#define ABSTWEENPLUGIN_3_T3304718885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Quaternion,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t3304718885  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T3304718885_H
#ifndef U3CU3EC__DISPLAYCLASS72_0_T2728653412_H
#define U3CU3EC__DISPLAYCLASS72_0_T2728653412_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0
struct  U3CU3Ec__DisplayClass72_0_t2728653412  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass72_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass72_0_t2728653412, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS72_0_T2728653412_H
#ifndef ABSTWEENPLUGIN_3_T16126469_H
#define ABSTWEENPLUGIN_3_T16126469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct  ABSTweenPlugin_3_t16126469  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T16126469_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef U3CU3EC__DISPLAYCLASS73_0_T1142366495_H
#define U3CU3EC__DISPLAYCLASS73_0_T1142366495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0
struct  U3CU3Ec__DisplayClass73_0_t1142366495  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass73_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_t1142366495, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_0_T1142366495_H
#ifndef DEBUGGER_T1404542751_H
#define DEBUGGER_T1404542751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Debugger
struct  Debugger_t1404542751  : public RuntimeObject
{
public:

public:
};

struct Debugger_t1404542751_StaticFields
{
public:
	// System.Int32 DG.Tweening.Core.Debugger::logPriority
	int32_t ___logPriority_0;

public:
	inline static int32_t get_offset_of_logPriority_0() { return static_cast<int32_t>(offsetof(Debugger_t1404542751_StaticFields, ___logPriority_0)); }
	inline int32_t get_logPriority_0() const { return ___logPriority_0; }
	inline int32_t* get_address_of_logPriority_0() { return &___logPriority_0; }
	inline void set_logPriority_0(int32_t value)
	{
		___logPriority_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGER_T1404542751_H
#ifndef U3CWAITFORCOMPLETIONU3ED__13_T639731943_H
#define U3CWAITFORCOMPLETIONU3ED__13_T639731943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__13
struct  U3CWaitForCompletionU3Ed__13_t639731943  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// DG.Tweening.Tween DG.Tweening.Core.DOTweenComponent/<WaitForCompletion>d__13::t
	Tween_t278478013 * ___t_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__13_t639731943, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__13_t639731943, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CWaitForCompletionU3Ed__13_t639731943, ___t_2)); }
	inline Tween_t278478013 * get_t_2() const { return ___t_2; }
	inline Tween_t278478013 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_t278478013 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((&___t_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORCOMPLETIONU3ED__13_T639731943_H
#ifndef ABSTWEENPLUGIN_3_T2833266637_H
#define ABSTWEENPLUGIN_3_T2833266637_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<DG.Tweening.Color2,DG.Tweening.Color2,DG.Tweening.Plugins.Options.ColorOptions>
struct  ABSTweenPlugin_3_t2833266637  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2833266637_H
#ifndef ABSPATHDECODER_T3294469411_H
#define ABSPATHDECODER_T3294469411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder
struct  ABSPathDecoder_t3294469411  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSPATHDECODER_T3294469411_H
#ifndef SPECIALPLUGINSUTILS_T2241999250_H
#define SPECIALPLUGINSUTILS_T2241999250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.SpecialPluginsUtils
struct  SpecialPluginsUtils_t2241999250  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALPLUGINSUTILS_T2241999250_H
#ifndef PLUGINSMANAGER_T3052451537_H
#define PLUGINSMANAGER_T3052451537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PluginsManager
struct  PluginsManager_t3052451537  : public RuntimeObject
{
public:

public:
};

struct PluginsManager_t3052451537_StaticFields
{
public:
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_floatPlugin
	RuntimeObject* ____floatPlugin_0;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_doublePlugin
	RuntimeObject* ____doublePlugin_1;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_intPlugin
	RuntimeObject* ____intPlugin_2;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_uintPlugin
	RuntimeObject* ____uintPlugin_3;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_longPlugin
	RuntimeObject* ____longPlugin_4;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_ulongPlugin
	RuntimeObject* ____ulongPlugin_5;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector2Plugin
	RuntimeObject* ____vector2Plugin_6;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector3Plugin
	RuntimeObject* ____vector3Plugin_7;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector4Plugin
	RuntimeObject* ____vector4Plugin_8;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_quaternionPlugin
	RuntimeObject* ____quaternionPlugin_9;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_colorPlugin
	RuntimeObject* ____colorPlugin_10;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_rectPlugin
	RuntimeObject* ____rectPlugin_11;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_rectOffsetPlugin
	RuntimeObject* ____rectOffsetPlugin_12;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_stringPlugin
	RuntimeObject* ____stringPlugin_13;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_vector3ArrayPlugin
	RuntimeObject* ____vector3ArrayPlugin_14;
	// DG.Tweening.Plugins.Core.ITweenPlugin DG.Tweening.Plugins.Core.PluginsManager::_color2Plugin
	RuntimeObject* ____color2Plugin_15;
	// System.Collections.Generic.Dictionary`2<System.Type,DG.Tweening.Plugins.Core.ITweenPlugin> DG.Tweening.Plugins.Core.PluginsManager::_customPlugins
	Dictionary_2_t633821276 * ____customPlugins_17;

public:
	inline static int32_t get_offset_of__floatPlugin_0() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____floatPlugin_0)); }
	inline RuntimeObject* get__floatPlugin_0() const { return ____floatPlugin_0; }
	inline RuntimeObject** get_address_of__floatPlugin_0() { return &____floatPlugin_0; }
	inline void set__floatPlugin_0(RuntimeObject* value)
	{
		____floatPlugin_0 = value;
		Il2CppCodeGenWriteBarrier((&____floatPlugin_0), value);
	}

	inline static int32_t get_offset_of__doublePlugin_1() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____doublePlugin_1)); }
	inline RuntimeObject* get__doublePlugin_1() const { return ____doublePlugin_1; }
	inline RuntimeObject** get_address_of__doublePlugin_1() { return &____doublePlugin_1; }
	inline void set__doublePlugin_1(RuntimeObject* value)
	{
		____doublePlugin_1 = value;
		Il2CppCodeGenWriteBarrier((&____doublePlugin_1), value);
	}

	inline static int32_t get_offset_of__intPlugin_2() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____intPlugin_2)); }
	inline RuntimeObject* get__intPlugin_2() const { return ____intPlugin_2; }
	inline RuntimeObject** get_address_of__intPlugin_2() { return &____intPlugin_2; }
	inline void set__intPlugin_2(RuntimeObject* value)
	{
		____intPlugin_2 = value;
		Il2CppCodeGenWriteBarrier((&____intPlugin_2), value);
	}

	inline static int32_t get_offset_of__uintPlugin_3() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____uintPlugin_3)); }
	inline RuntimeObject* get__uintPlugin_3() const { return ____uintPlugin_3; }
	inline RuntimeObject** get_address_of__uintPlugin_3() { return &____uintPlugin_3; }
	inline void set__uintPlugin_3(RuntimeObject* value)
	{
		____uintPlugin_3 = value;
		Il2CppCodeGenWriteBarrier((&____uintPlugin_3), value);
	}

	inline static int32_t get_offset_of__longPlugin_4() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____longPlugin_4)); }
	inline RuntimeObject* get__longPlugin_4() const { return ____longPlugin_4; }
	inline RuntimeObject** get_address_of__longPlugin_4() { return &____longPlugin_4; }
	inline void set__longPlugin_4(RuntimeObject* value)
	{
		____longPlugin_4 = value;
		Il2CppCodeGenWriteBarrier((&____longPlugin_4), value);
	}

	inline static int32_t get_offset_of__ulongPlugin_5() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____ulongPlugin_5)); }
	inline RuntimeObject* get__ulongPlugin_5() const { return ____ulongPlugin_5; }
	inline RuntimeObject** get_address_of__ulongPlugin_5() { return &____ulongPlugin_5; }
	inline void set__ulongPlugin_5(RuntimeObject* value)
	{
		____ulongPlugin_5 = value;
		Il2CppCodeGenWriteBarrier((&____ulongPlugin_5), value);
	}

	inline static int32_t get_offset_of__vector2Plugin_6() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____vector2Plugin_6)); }
	inline RuntimeObject* get__vector2Plugin_6() const { return ____vector2Plugin_6; }
	inline RuntimeObject** get_address_of__vector2Plugin_6() { return &____vector2Plugin_6; }
	inline void set__vector2Plugin_6(RuntimeObject* value)
	{
		____vector2Plugin_6 = value;
		Il2CppCodeGenWriteBarrier((&____vector2Plugin_6), value);
	}

	inline static int32_t get_offset_of__vector3Plugin_7() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____vector3Plugin_7)); }
	inline RuntimeObject* get__vector3Plugin_7() const { return ____vector3Plugin_7; }
	inline RuntimeObject** get_address_of__vector3Plugin_7() { return &____vector3Plugin_7; }
	inline void set__vector3Plugin_7(RuntimeObject* value)
	{
		____vector3Plugin_7 = value;
		Il2CppCodeGenWriteBarrier((&____vector3Plugin_7), value);
	}

	inline static int32_t get_offset_of__vector4Plugin_8() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____vector4Plugin_8)); }
	inline RuntimeObject* get__vector4Plugin_8() const { return ____vector4Plugin_8; }
	inline RuntimeObject** get_address_of__vector4Plugin_8() { return &____vector4Plugin_8; }
	inline void set__vector4Plugin_8(RuntimeObject* value)
	{
		____vector4Plugin_8 = value;
		Il2CppCodeGenWriteBarrier((&____vector4Plugin_8), value);
	}

	inline static int32_t get_offset_of__quaternionPlugin_9() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____quaternionPlugin_9)); }
	inline RuntimeObject* get__quaternionPlugin_9() const { return ____quaternionPlugin_9; }
	inline RuntimeObject** get_address_of__quaternionPlugin_9() { return &____quaternionPlugin_9; }
	inline void set__quaternionPlugin_9(RuntimeObject* value)
	{
		____quaternionPlugin_9 = value;
		Il2CppCodeGenWriteBarrier((&____quaternionPlugin_9), value);
	}

	inline static int32_t get_offset_of__colorPlugin_10() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____colorPlugin_10)); }
	inline RuntimeObject* get__colorPlugin_10() const { return ____colorPlugin_10; }
	inline RuntimeObject** get_address_of__colorPlugin_10() { return &____colorPlugin_10; }
	inline void set__colorPlugin_10(RuntimeObject* value)
	{
		____colorPlugin_10 = value;
		Il2CppCodeGenWriteBarrier((&____colorPlugin_10), value);
	}

	inline static int32_t get_offset_of__rectPlugin_11() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____rectPlugin_11)); }
	inline RuntimeObject* get__rectPlugin_11() const { return ____rectPlugin_11; }
	inline RuntimeObject** get_address_of__rectPlugin_11() { return &____rectPlugin_11; }
	inline void set__rectPlugin_11(RuntimeObject* value)
	{
		____rectPlugin_11 = value;
		Il2CppCodeGenWriteBarrier((&____rectPlugin_11), value);
	}

	inline static int32_t get_offset_of__rectOffsetPlugin_12() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____rectOffsetPlugin_12)); }
	inline RuntimeObject* get__rectOffsetPlugin_12() const { return ____rectOffsetPlugin_12; }
	inline RuntimeObject** get_address_of__rectOffsetPlugin_12() { return &____rectOffsetPlugin_12; }
	inline void set__rectOffsetPlugin_12(RuntimeObject* value)
	{
		____rectOffsetPlugin_12 = value;
		Il2CppCodeGenWriteBarrier((&____rectOffsetPlugin_12), value);
	}

	inline static int32_t get_offset_of__stringPlugin_13() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____stringPlugin_13)); }
	inline RuntimeObject* get__stringPlugin_13() const { return ____stringPlugin_13; }
	inline RuntimeObject** get_address_of__stringPlugin_13() { return &____stringPlugin_13; }
	inline void set__stringPlugin_13(RuntimeObject* value)
	{
		____stringPlugin_13 = value;
		Il2CppCodeGenWriteBarrier((&____stringPlugin_13), value);
	}

	inline static int32_t get_offset_of__vector3ArrayPlugin_14() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____vector3ArrayPlugin_14)); }
	inline RuntimeObject* get__vector3ArrayPlugin_14() const { return ____vector3ArrayPlugin_14; }
	inline RuntimeObject** get_address_of__vector3ArrayPlugin_14() { return &____vector3ArrayPlugin_14; }
	inline void set__vector3ArrayPlugin_14(RuntimeObject* value)
	{
		____vector3ArrayPlugin_14 = value;
		Il2CppCodeGenWriteBarrier((&____vector3ArrayPlugin_14), value);
	}

	inline static int32_t get_offset_of__color2Plugin_15() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____color2Plugin_15)); }
	inline RuntimeObject* get__color2Plugin_15() const { return ____color2Plugin_15; }
	inline RuntimeObject** get_address_of__color2Plugin_15() { return &____color2Plugin_15; }
	inline void set__color2Plugin_15(RuntimeObject* value)
	{
		____color2Plugin_15 = value;
		Il2CppCodeGenWriteBarrier((&____color2Plugin_15), value);
	}

	inline static int32_t get_offset_of__customPlugins_17() { return static_cast<int32_t>(offsetof(PluginsManager_t3052451537_StaticFields, ____customPlugins_17)); }
	inline Dictionary_2_t633821276 * get__customPlugins_17() const { return ____customPlugins_17; }
	inline Dictionary_2_t633821276 ** get_address_of__customPlugins_17() { return &____customPlugins_17; }
	inline void set__customPlugins_17(Dictionary_2_t633821276 * value)
	{
		____customPlugins_17 = value;
		Il2CppCodeGenWriteBarrier((&____customPlugins_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINSMANAGER_T3052451537_H
#ifndef STRINGPLUGINEXTENSIONS_T3910942986_H
#define STRINGPLUGINEXTENSIONS_T3910942986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.StringPluginExtensions
struct  StringPluginExtensions_t3910942986  : public RuntimeObject
{
public:

public:
};

struct StringPluginExtensions_t3910942986_StaticFields
{
public:
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsAll
	CharU5BU5D_t1328083999* ___ScrambledCharsAll_0;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsUppercase
	CharU5BU5D_t1328083999* ___ScrambledCharsUppercase_1;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsLowercase
	CharU5BU5D_t1328083999* ___ScrambledCharsLowercase_2;
	// System.Char[] DG.Tweening.Plugins.StringPluginExtensions::ScrambledCharsNumerals
	CharU5BU5D_t1328083999* ___ScrambledCharsNumerals_3;
	// System.Int32 DG.Tweening.Plugins.StringPluginExtensions::_lastRndSeed
	int32_t ____lastRndSeed_4;

public:
	inline static int32_t get_offset_of_ScrambledCharsAll_0() { return static_cast<int32_t>(offsetof(StringPluginExtensions_t3910942986_StaticFields, ___ScrambledCharsAll_0)); }
	inline CharU5BU5D_t1328083999* get_ScrambledCharsAll_0() const { return ___ScrambledCharsAll_0; }
	inline CharU5BU5D_t1328083999** get_address_of_ScrambledCharsAll_0() { return &___ScrambledCharsAll_0; }
	inline void set_ScrambledCharsAll_0(CharU5BU5D_t1328083999* value)
	{
		___ScrambledCharsAll_0 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsAll_0), value);
	}

	inline static int32_t get_offset_of_ScrambledCharsUppercase_1() { return static_cast<int32_t>(offsetof(StringPluginExtensions_t3910942986_StaticFields, ___ScrambledCharsUppercase_1)); }
	inline CharU5BU5D_t1328083999* get_ScrambledCharsUppercase_1() const { return ___ScrambledCharsUppercase_1; }
	inline CharU5BU5D_t1328083999** get_address_of_ScrambledCharsUppercase_1() { return &___ScrambledCharsUppercase_1; }
	inline void set_ScrambledCharsUppercase_1(CharU5BU5D_t1328083999* value)
	{
		___ScrambledCharsUppercase_1 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsUppercase_1), value);
	}

	inline static int32_t get_offset_of_ScrambledCharsLowercase_2() { return static_cast<int32_t>(offsetof(StringPluginExtensions_t3910942986_StaticFields, ___ScrambledCharsLowercase_2)); }
	inline CharU5BU5D_t1328083999* get_ScrambledCharsLowercase_2() const { return ___ScrambledCharsLowercase_2; }
	inline CharU5BU5D_t1328083999** get_address_of_ScrambledCharsLowercase_2() { return &___ScrambledCharsLowercase_2; }
	inline void set_ScrambledCharsLowercase_2(CharU5BU5D_t1328083999* value)
	{
		___ScrambledCharsLowercase_2 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsLowercase_2), value);
	}

	inline static int32_t get_offset_of_ScrambledCharsNumerals_3() { return static_cast<int32_t>(offsetof(StringPluginExtensions_t3910942986_StaticFields, ___ScrambledCharsNumerals_3)); }
	inline CharU5BU5D_t1328083999* get_ScrambledCharsNumerals_3() const { return ___ScrambledCharsNumerals_3; }
	inline CharU5BU5D_t1328083999** get_address_of_ScrambledCharsNumerals_3() { return &___ScrambledCharsNumerals_3; }
	inline void set_ScrambledCharsNumerals_3(CharU5BU5D_t1328083999* value)
	{
		___ScrambledCharsNumerals_3 = value;
		Il2CppCodeGenWriteBarrier((&___ScrambledCharsNumerals_3), value);
	}

	inline static int32_t get_offset_of__lastRndSeed_4() { return static_cast<int32_t>(offsetof(StringPluginExtensions_t3910942986_StaticFields, ____lastRndSeed_4)); }
	inline int32_t get__lastRndSeed_4() const { return ____lastRndSeed_4; }
	inline int32_t* get_address_of__lastRndSeed_4() { return &____lastRndSeed_4; }
	inline void set__lastRndSeed_4(int32_t value)
	{
		____lastRndSeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPLUGINEXTENSIONS_T3910942986_H
#ifndef ABSTWEENPLUGIN_3_T3659029185_H
#define ABSTWEENPLUGIN_3_T3659029185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Double,System.Double,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t3659029185  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T3659029185_H
#ifndef ABSTWEENPLUGIN_3_T1905502397_H
#define ABSTWEENPLUGIN_3_T1905502397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions>
struct  ABSTweenPlugin_3_t1905502397  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1905502397_H
#ifndef ABSTWEENPLUGIN_3_T2067571757_H
#define ABSTWEENPLUGIN_3_T2067571757_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int32,System.Int32,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t2067571757  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2067571757_H
#ifndef ABSTWEENPLUGIN_3_T580261006_H
#define ABSTWEENPLUGIN_3_T580261006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions>
struct  ABSTweenPlugin_3_t580261006  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T580261006_H
#ifndef ABSTWEENPLUGIN_3_T1942951492_H
#define ABSTWEENPLUGIN_3_T1942951492_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct  ABSTweenPlugin_3_t1942951492  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1942951492_H
#ifndef ABSTWEENPLUGIN_3_T2106574801_H
#define ABSTWEENPLUGIN_3_T2106574801_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.Int64,System.Int64,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t2106574801  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2106574801_H
#ifndef ABSTWEENPLUGIN_3_T1845120181_H
#define ABSTWEENPLUGIN_3_T1845120181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<System.UInt64,System.UInt64,DG.Tweening.Plugins.Options.NoOptions>
struct  ABSTweenPlugin_3_t1845120181  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T1845120181_H
#ifndef ABSTWEENPLUGIN_3_T2700540022_H
#define ABSTWEENPLUGIN_3_T2700540022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3[],DG.Tweening.Plugins.Options.Vector3ArrayOptions>
struct  ABSTweenPlugin_3_t2700540022  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTWEENPLUGIN_3_T2700540022_H
#ifndef U3CU3EC__DISPLAYCLASS48_0_T1881678375_H
#define U3CU3EC__DISPLAYCLASS48_0_T1881678375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0
struct  U3CU3Ec__DisplayClass48_0_t1881678375  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass48_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass48_0_t1881678375, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS48_0_T1881678375_H
#ifndef U3CU3EC__DISPLAYCLASS46_0_T3293303385_H
#define U3CU3EC__DISPLAYCLASS46_0_T3293303385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0
struct  U3CU3Ec__DisplayClass46_0_t3293303385  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass46_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_0_t3293303385, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS46_0_T3293303385_H
#ifndef U3CU3EC__DISPLAYCLASS45_0_T1989341470_H
#define U3CU3EC__DISPLAYCLASS45_0_T1989341470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0
struct  U3CU3Ec__DisplayClass45_0_t1989341470  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass45_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass45_0_t1989341470, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS45_0_T1989341470_H
#ifndef U3CU3EC__DISPLAYCLASS49_0_T295391458_H
#define U3CU3EC__DISPLAYCLASS49_0_T295391458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass49_0
struct  U3CU3Ec__DisplayClass49_0_t295391458  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass49_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass49_0_t295391458, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS49_0_T295391458_H
#ifndef U3CU3EC__DISPLAYCLASS52_0_T2728653222_H
#define U3CU3EC__DISPLAYCLASS52_0_T2728653222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0
struct  U3CU3Ec__DisplayClass52_0_t2728653222  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass52_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_t2728653222, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS52_0_T2728653222_H
#ifndef U3CU3EC__DISPLAYCLASS51_0_T1424691307_H
#define U3CU3EC__DISPLAYCLASS51_0_T1424691307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0
struct  U3CU3Ec__DisplayClass51_0_t1424691307  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass51_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass51_0_t1424691307, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS51_0_T1424691307_H
#ifndef U3CU3EC__DISPLAYCLASS50_0_T3010978224_H
#define U3CU3EC__DISPLAYCLASS50_0_T3010978224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass50_0
struct  U3CU3Ec__DisplayClass50_0_t3010978224  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass50_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass50_0_t3010978224, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS50_0_T3010978224_H
#ifndef U3CU3EC__DISPLAYCLASS44_0_T3575628387_H
#define U3CU3EC__DISPLAYCLASS44_0_T3575628387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0
struct  U3CU3Ec__DisplayClass44_0_t3575628387  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass44_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass44_0_t3575628387, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS44_0_T3575628387_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T1881678530_H
#define U3CU3EC__DISPLAYCLASS38_0_T1881678530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t1881678530  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0::trans
	Transform_t3275118058 * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass38_0::target
	Rigidbody_t4233889191 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t1881678530, ___trans_0)); }
	inline Transform_t3275118058 * get_trans_0() const { return ___trans_0; }
	inline Transform_t3275118058 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_t3275118058 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((&___trans_0), value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t1881678530, ___target_1)); }
	inline Rigidbody_t4233889191 * get_target_1() const { return ___target_1; }
	inline Rigidbody_t4233889191 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_t4233889191 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T1881678530_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_T295391613_H
#define U3CU3EC__DISPLAYCLASS39_0_T295391613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_t295391613  : public RuntimeObject
{
public:
	// UnityEngine.TrailRenderer DG.Tweening.ShortcutExtensions/<>c__DisplayClass39_0::target
	TrailRenderer_t2490637367 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t295391613, ___target_0)); }
	inline TrailRenderer_t2490637367 * get_target_0() const { return ___target_0; }
	inline TrailRenderer_t2490637367 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(TrailRenderer_t2490637367 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_T295391613_H
#ifndef U3CU3EC__DISPLAYCLASS40_0_T3010978383_H
#define U3CU3EC__DISPLAYCLASS40_0_T3010978383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_t3010978383  : public RuntimeObject
{
public:
	// UnityEngine.TrailRenderer DG.Tweening.ShortcutExtensions/<>c__DisplayClass40_0::target
	TrailRenderer_t2490637367 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t3010978383, ___target_0)); }
	inline TrailRenderer_t2490637367 * get_target_0() const { return ___target_0; }
	inline TrailRenderer_t2490637367 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(TrailRenderer_t2490637367 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_0_T3010978383_H
#ifndef U3CU3EC__DISPLAYCLASS41_0_T1424691466_H
#define U3CU3EC__DISPLAYCLASS41_0_T1424691466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0
struct  U3CU3Ec__DisplayClass41_0_t1424691466  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass41_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t1424691466, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_0_T1424691466_H
#ifndef U3CU3EC__DISPLAYCLASS37_0_T1707016623_H
#define U3CU3EC__DISPLAYCLASS37_0_T1707016623_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass37_0
struct  U3CU3Ec__DisplayClass37_0_t1707016623  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.ShortcutExtensions/<>c__DisplayClass37_0::target
	Rigidbody_t4233889191 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t1707016623, ___target_0)); }
	inline Rigidbody_t4233889191 * get_target_0() const { return ___target_0; }
	inline Rigidbody_t4233889191 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t4233889191 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS37_0_T1707016623_H
#ifndef U3CU3EC__DISPLAYCLASS43_0_T1142366464_H
#define U3CU3EC__DISPLAYCLASS43_0_T1142366464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0
struct  U3CU3Ec__DisplayClass43_0_t1142366464  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass43_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass43_0_t1142366464, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS43_0_T1142366464_H
#ifndef U3CU3EC__DISPLAYCLASS42_0_T2728653381_H
#define U3CU3EC__DISPLAYCLASS42_0_T2728653381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0
struct  U3CU3Ec__DisplayClass42_0_t2728653381  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass42_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass42_0_t2728653381, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS42_0_T2728653381_H
#ifndef U3CU3EC__DISPLAYCLASS53_0_T1142366305_H
#define U3CU3EC__DISPLAYCLASS53_0_T1142366305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0
struct  U3CU3Ec__DisplayClass53_0_t1142366305  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass53_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass53_0_t1142366305, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS53_0_T1142366305_H
#ifndef U3CU3EC__DISPLAYCLASS62_0_T2728653315_H
#define U3CU3EC__DISPLAYCLASS62_0_T2728653315_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0
struct  U3CU3Ec__DisplayClass62_0_t2728653315  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass62_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass62_0_t2728653315, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS62_0_T2728653315_H
#ifndef U3CU3EC__DISPLAYCLASS63_0_T1142366398_H
#define U3CU3EC__DISPLAYCLASS63_0_T1142366398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0
struct  U3CU3Ec__DisplayClass63_0_t1142366398  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass63_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass63_0_t1142366398, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS63_0_T1142366398_H
#ifndef U3CU3EC__DISPLAYCLASS61_0_T1424691400_H
#define U3CU3EC__DISPLAYCLASS61_0_T1424691400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0
struct  U3CU3Ec__DisplayClass61_0_t1424691400  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass61_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass61_0_t1424691400, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS61_0_T1424691400_H
#ifndef U3CU3EC__DISPLAYCLASS47_0_T1707016468_H
#define U3CU3EC__DISPLAYCLASS47_0_T1707016468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0
struct  U3CU3Ec__DisplayClass47_0_t1707016468  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass47_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass47_0_t1707016468, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS47_0_T1707016468_H
#ifndef U3CU3EC__DISPLAYCLASS66_0_T3293303319_H
#define U3CU3EC__DISPLAYCLASS66_0_T3293303319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0
struct  U3CU3Ec__DisplayClass66_0_t3293303319  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass66_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass66_0_t3293303319, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS66_0_T3293303319_H
#ifndef U3CU3EC__DISPLAYCLASS67_0_T1707016402_H
#define U3CU3EC__DISPLAYCLASS67_0_T1707016402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0
struct  U3CU3Ec__DisplayClass67_0_t1707016402  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass67_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass67_0_t1707016402, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS67_0_T1707016402_H
#ifndef U3CU3EC__DISPLAYCLASS64_0_T3575628321_H
#define U3CU3EC__DISPLAYCLASS64_0_T3575628321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0
struct  U3CU3Ec__DisplayClass64_0_t3575628321  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass64_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass64_0_t3575628321, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS64_0_T3575628321_H
#ifndef U3CU3EC__DISPLAYCLASS65_0_T1989341404_H
#define U3CU3EC__DISPLAYCLASS65_0_T1989341404_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0
struct  U3CU3Ec__DisplayClass65_0_t1989341404  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass65_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_t1989341404, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS65_0_T1989341404_H
#ifndef U3CU3EC__DISPLAYCLASS57_0_T1707016309_H
#define U3CU3EC__DISPLAYCLASS57_0_T1707016309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0
struct  U3CU3Ec__DisplayClass57_0_t1707016309  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass57_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t1707016309, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS57_0_T1707016309_H
#ifndef U3CU3EC__DISPLAYCLASS58_0_T1881678216_H
#define U3CU3EC__DISPLAYCLASS58_0_T1881678216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0
struct  U3CU3Ec__DisplayClass58_0_t1881678216  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass58_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass58_0_t1881678216, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS58_0_T1881678216_H
#ifndef U3CU3EC__DISPLAYCLASS56_0_T3293303226_H
#define U3CU3EC__DISPLAYCLASS56_0_T3293303226_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0
struct  U3CU3Ec__DisplayClass56_0_t3293303226  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass56_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass56_0_t3293303226, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS56_0_T3293303226_H
#ifndef U3CU3EC__DISPLAYCLASS54_0_T3575628228_H
#define U3CU3EC__DISPLAYCLASS54_0_T3575628228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0
struct  U3CU3Ec__DisplayClass54_0_t3575628228  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass54_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass54_0_t3575628228, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS54_0_T3575628228_H
#ifndef U3CU3EC__DISPLAYCLASS55_0_T1989341311_H
#define U3CU3EC__DISPLAYCLASS55_0_T1989341311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0
struct  U3CU3Ec__DisplayClass55_0_t1989341311  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass55_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass55_0_t1989341311, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS55_0_T1989341311_H
#ifndef U3CU3EC__DISPLAYCLASS59_0_T295391299_H
#define U3CU3EC__DISPLAYCLASS59_0_T295391299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0
struct  U3CU3Ec__DisplayClass59_0_t295391299  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass59_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass59_0_t295391299, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS59_0_T295391299_H
#ifndef U3CU3EC__DISPLAYCLASS60_0_T3010978317_H
#define U3CU3EC__DISPLAYCLASS60_0_T3010978317_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0
struct  U3CU3Ec__DisplayClass60_0_t3010978317  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass60_0::target
	Transform_t3275118058 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass60_0_t3010978317, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS60_0_T3010978317_H
#ifndef COLOROPTIONS_T2213017305_H
#define COLOROPTIONS_T2213017305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.ColorOptions
struct  ColorOptions_t2213017305 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.ColorOptions::alphaOnly
	bool ___alphaOnly_0;

public:
	inline static int32_t get_offset_of_alphaOnly_0() { return static_cast<int32_t>(offsetof(ColorOptions_t2213017305, ___alphaOnly_0)); }
	inline bool get_alphaOnly_0() const { return ___alphaOnly_0; }
	inline bool* get_address_of_alphaOnly_0() { return &___alphaOnly_0; }
	inline void set_alphaOnly_0(bool value)
	{
		___alphaOnly_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t2213017305_marshaled_pinvoke
{
	int32_t ___alphaOnly_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.ColorOptions
struct ColorOptions_t2213017305_marshaled_com
{
	int32_t ___alphaOnly_0;
};
#endif // COLOROPTIONS_T2213017305_H
#ifndef NOOPTIONS_T2508431845_H
#define NOOPTIONS_T2508431845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.NoOptions
struct  NoOptions_t2508431845 
{
public:
	union
	{
		struct
		{
		};
		uint8_t NoOptions_t2508431845__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOOPTIONS_T2508431845_H
#ifndef RECTOPTIONS_T3393635162_H
#define RECTOPTIONS_T3393635162_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.RectOptions
struct  RectOptions_t3393635162 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.RectOptions::snapping
	bool ___snapping_0;

public:
	inline static int32_t get_offset_of_snapping_0() { return static_cast<int32_t>(offsetof(RectOptions_t3393635162, ___snapping_0)); }
	inline bool get_snapping_0() const { return ___snapping_0; }
	inline bool* get_address_of_snapping_0() { return &___snapping_0; }
	inline void set_snapping_0(bool value)
	{
		___snapping_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_t3393635162_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.RectOptions
struct RectOptions_t3393635162_marshaled_com
{
	int32_t ___snapping_0;
};
#endif // RECTOPTIONS_T3393635162_H
#ifndef FLOATOPTIONS_T1421548266_H
#define FLOATOPTIONS_T1421548266_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.FloatOptions
struct  FloatOptions_t1421548266 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.FloatOptions::snapping
	bool ___snapping_0;

public:
	inline static int32_t get_offset_of_snapping_0() { return static_cast<int32_t>(offsetof(FloatOptions_t1421548266, ___snapping_0)); }
	inline bool get_snapping_0() const { return ___snapping_0; }
	inline bool* get_address_of_snapping_0() { return &___snapping_0; }
	inline void set_snapping_0(bool value)
	{
		___snapping_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t1421548266_marshaled_pinvoke
{
	int32_t ___snapping_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.FloatOptions
struct FloatOptions_t1421548266_marshaled_com
{
	int32_t ___snapping_0;
};
#endif // FLOATOPTIONS_T1421548266_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef LINEARDECODER_T2073524639_H
#define LINEARDECODER_T2073524639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.LinearDecoder
struct  LinearDecoder_t2073524639  : public ABSPathDecoder_t3294469411
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEARDECODER_T2073524639_H
#ifndef CATMULLROMDECODER_T3014762178_H
#define CATMULLROMDECODER_T3014762178_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder
struct  CatmullRomDecoder_t3014762178  : public ABSPathDecoder_t3294469411
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATMULLROMDECODER_T3014762178_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef PUREQUATERNIONPLUGIN_T3400666973_H
#define PUREQUATERNIONPLUGIN_T3400666973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.CustomPlugins.PureQuaternionPlugin
struct  PureQuaternionPlugin_t3400666973  : public ABSTweenPlugin_3_t3304718885
{
public:

public:
};

struct PureQuaternionPlugin_t3400666973_StaticFields
{
public:
	// DG.Tweening.CustomPlugins.PureQuaternionPlugin DG.Tweening.CustomPlugins.PureQuaternionPlugin::_plug
	PureQuaternionPlugin_t3400666973 * ____plug_0;

public:
	inline static int32_t get_offset_of__plug_0() { return static_cast<int32_t>(offsetof(PureQuaternionPlugin_t3400666973_StaticFields, ____plug_0)); }
	inline PureQuaternionPlugin_t3400666973 * get__plug_0() const { return ____plug_0; }
	inline PureQuaternionPlugin_t3400666973 ** get_address_of__plug_0() { return &____plug_0; }
	inline void set__plug_0(PureQuaternionPlugin_t3400666973 * value)
	{
		____plug_0 = value;
		Il2CppCodeGenWriteBarrier((&____plug_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUREQUATERNIONPLUGIN_T3400666973_H
#ifndef ULONGPLUGIN_T3231465400_H
#define ULONGPLUGIN_T3231465400_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.UlongPlugin
struct  UlongPlugin_t3231465400  : public ABSTweenPlugin_3_t1845120181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ULONGPLUGIN_T3231465400_H
#ifndef RECTPLUGIN_T391797831_H
#define RECTPLUGIN_T391797831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.RectPlugin
struct  RectPlugin_t391797831  : public ABSTweenPlugin_3_t1972650634
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTPLUGIN_T391797831_H
#ifndef UINTPLUGIN_T1040977389_H
#define UINTPLUGIN_T1040977389_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.UintPlugin
struct  UintPlugin_t1040977389  : public ABSTweenPlugin_3_t556292748
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTPLUGIN_T1040977389_H
#ifndef VECTOR4PLUGIN_T2164361360_H
#define VECTOR4PLUGIN_T2164361360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector4Plugin
struct  Vector4Plugin_t2164361360  : public ABSTweenPlugin_3_t2168888377
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4PLUGIN_T2164361360_H
#ifndef VECTOR2PLUGIN_T2164285386_H
#define VECTOR2PLUGIN_T2164285386_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector2Plugin
struct  Vector2Plugin_t2164285386  : public ABSTweenPlugin_3_t2158331857
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2PLUGIN_T2164285386_H
#ifndef RECTOFFSETPLUGIN_T664509336_H
#define RECTOFFSETPLUGIN_T664509336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.RectOffsetPlugin
struct  RectOffsetPlugin_t664509336  : public ABSTweenPlugin_3_t882645641
{
public:

public:
};

struct RectOffsetPlugin_t664509336_StaticFields
{
public:
	// UnityEngine.RectOffset DG.Tweening.Plugins.RectOffsetPlugin::_r
	RectOffset_t3387826427 * ____r_0;

public:
	inline static int32_t get_offset_of__r_0() { return static_cast<int32_t>(offsetof(RectOffsetPlugin_t664509336_StaticFields, ____r_0)); }
	inline RectOffset_t3387826427 * get__r_0() const { return ____r_0; }
	inline RectOffset_t3387826427 ** get_address_of__r_0() { return &____r_0; }
	inline void set__r_0(RectOffset_t3387826427 * value)
	{
		____r_0 = value;
		Il2CppCodeGenWriteBarrier((&____r_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTOFFSETPLUGIN_T664509336_H
#ifndef PATHPLUGIN_T4171842066_H
#define PATHPLUGIN_T4171842066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.PathPlugin
struct  PathPlugin_t4171842066  : public ABSTweenPlugin_3_t1942951492
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHPLUGIN_T4171842066_H
#ifndef VECTOR3ARRAYPLUGIN_T2378569512_H
#define VECTOR3ARRAYPLUGIN_T2378569512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector3ArrayPlugin
struct  Vector3ArrayPlugin_t2378569512  : public ABSTweenPlugin_3_t2700540022
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ARRAYPLUGIN_T2378569512_H
#ifndef COLORPLUGIN_T4063724482_H
#define COLORPLUGIN_T4063724482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.ColorPlugin
struct  ColorPlugin_t4063724482  : public ABSTweenPlugin_3_t1905502397
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORPLUGIN_T4063724482_H
#ifndef QUATERNIONPLUGIN_T1696644323_H
#define QUATERNIONPLUGIN_T1696644323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.QuaternionPlugin
struct  QuaternionPlugin_t1696644323  : public ABSTweenPlugin_3_t580261006
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONPLUGIN_T1696644323_H
#ifndef INTPLUGIN_T180838436_H
#define INTPLUGIN_T180838436_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.IntPlugin
struct  IntPlugin_t180838436  : public ABSTweenPlugin_3_t2067571757
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPLUGIN_T180838436_H
#ifndef COLOR2PLUGIN_T3433430606_H
#define COLOR2PLUGIN_T3433430606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Color2Plugin
struct  Color2Plugin_t3433430606  : public ABSTweenPlugin_3_t2833266637
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR2PLUGIN_T3433430606_H
#ifndef VECTOR3PLUGIN_T2164530409_H
#define VECTOR3PLUGIN_T2164530409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Vector3Plugin
struct  Vector3Plugin_t2164530409  : public ABSTweenPlugin_3_t16126469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3PLUGIN_T2164530409_H
#ifndef UINTOPTIONS_T2267095136_H
#define UINTOPTIONS_T2267095136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.UintOptions
struct  UintOptions_t2267095136 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.UintOptions::isNegativeChangeValue
	bool ___isNegativeChangeValue_0;

public:
	inline static int32_t get_offset_of_isNegativeChangeValue_0() { return static_cast<int32_t>(offsetof(UintOptions_t2267095136, ___isNegativeChangeValue_0)); }
	inline bool get_isNegativeChangeValue_0() const { return ___isNegativeChangeValue_0; }
	inline bool* get_address_of_isNegativeChangeValue_0() { return &___isNegativeChangeValue_0; }
	inline void set_isNegativeChangeValue_0(bool value)
	{
		___isNegativeChangeValue_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.UintOptions
struct UintOptions_t2267095136_marshaled_pinvoke
{
	int32_t ___isNegativeChangeValue_0;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.UintOptions
struct UintOptions_t2267095136_marshaled_com
{
	int32_t ___isNegativeChangeValue_0;
};
#endif // UINTOPTIONS_T2267095136_H
#ifndef LONGPLUGIN_T1941283029_H
#define LONGPLUGIN_T1941283029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.LongPlugin
struct  LongPlugin_t1941283029  : public ABSTweenPlugin_3_t2106574801
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONGPLUGIN_T1941283029_H
#ifndef DOUBLEPLUGIN_T266400784_H
#define DOUBLEPLUGIN_T266400784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.DoublePlugin
struct  DoublePlugin_t266400784  : public ABSTweenPlugin_3_t3659029185
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLEPLUGIN_T266400784_H
#ifndef STRINGPLUGIN_T3620786088_H
#define STRINGPLUGIN_T3620786088_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.StringPlugin
struct  StringPlugin_t3620786088  : public ABSTweenPlugin_3_t3790859801
{
public:

public:
};

struct StringPlugin_t3620786088_StaticFields
{
public:
	// System.Text.StringBuilder DG.Tweening.Plugins.StringPlugin::_Buffer
	StringBuilder_t1221177846 * ____Buffer_0;
	// System.Collections.Generic.List`1<System.Char> DG.Tweening.Plugins.StringPlugin::_OpenedTags
	List_1_t2823602470 * ____OpenedTags_1;

public:
	inline static int32_t get_offset_of__Buffer_0() { return static_cast<int32_t>(offsetof(StringPlugin_t3620786088_StaticFields, ____Buffer_0)); }
	inline StringBuilder_t1221177846 * get__Buffer_0() const { return ____Buffer_0; }
	inline StringBuilder_t1221177846 ** get_address_of__Buffer_0() { return &____Buffer_0; }
	inline void set__Buffer_0(StringBuilder_t1221177846 * value)
	{
		____Buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&____Buffer_0), value);
	}

	inline static int32_t get_offset_of__OpenedTags_1() { return static_cast<int32_t>(offsetof(StringPlugin_t3620786088_StaticFields, ____OpenedTags_1)); }
	inline List_1_t2823602470 * get__OpenedTags_1() const { return ____OpenedTags_1; }
	inline List_1_t2823602470 ** get_address_of__OpenedTags_1() { return &____OpenedTags_1; }
	inline void set__OpenedTags_1(List_1_t2823602470 * value)
	{
		____OpenedTags_1 = value;
		Il2CppCodeGenWriteBarrier((&____OpenedTags_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPLUGIN_T3620786088_H
#ifndef FLOATPLUGIN_T3639480371_H
#define FLOATPLUGIN_T3639480371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.FloatPlugin
struct  FloatPlugin_t3639480371  : public ABSTweenPlugin_3_t1186869890
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPLUGIN_T3639480371_H
#ifndef LOGBEHAVIOUR_T3505725029_H
#define LOGBEHAVIOUR_T3505725029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LogBehaviour
struct  LogBehaviour_t3505725029 
{
public:
	// System.Int32 DG.Tweening.LogBehaviour::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LogBehaviour_t3505725029, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGBEHAVIOUR_T3505725029_H
#ifndef U3CU3EC__DISPLAYCLASS77_0_T1707016499_H
#define U3CU3EC__DISPLAYCLASS77_0_T1707016499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0
struct  U3CU3Ec__DisplayClass77_0_t1707016499  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::to
	Vector3_t2243707580  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass77_0::target
	Transform_t3275118058 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass77_0_t1707016499, ___to_0)); }
	inline Vector3_t2243707580  get_to_0() const { return ___to_0; }
	inline Vector3_t2243707580 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Vector3_t2243707580  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass77_0_t1707016499, ___target_1)); }
	inline Transform_t3275118058 * get_target_1() const { return ___target_1; }
	inline Transform_t3275118058 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3275118058 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS77_0_T1707016499_H
#ifndef U3CU3EC__DISPLAYCLASS81_0_T1424691838_H
#define U3CU3EC__DISPLAYCLASS81_0_T1424691838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0
struct  U3CU3Ec__DisplayClass81_0_t1424691838  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0::to
	Vector3_t2243707580  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass81_0::target
	Transform_t3275118058 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass81_0_t1424691838, ___to_0)); }
	inline Vector3_t2243707580  get_to_0() const { return ___to_0; }
	inline Vector3_t2243707580 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Vector3_t2243707580  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass81_0_t1424691838, ___target_1)); }
	inline Transform_t3275118058 * get_target_1() const { return ___target_1; }
	inline Transform_t3275118058 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3275118058 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS81_0_T1424691838_H
#ifndef U3CU3EC__DISPLAYCLASS78_0_T1881678406_H
#define U3CU3EC__DISPLAYCLASS78_0_T1881678406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0
struct  U3CU3Ec__DisplayClass78_0_t1881678406  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0::to
	Vector3_t2243707580  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass78_0::target
	Transform_t3275118058 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_t1881678406, ___to_0)); }
	inline Vector3_t2243707580  get_to_0() const { return ___to_0; }
	inline Vector3_t2243707580 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Vector3_t2243707580  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass78_0_t1881678406, ___target_1)); }
	inline Transform_t3275118058 * get_target_1() const { return ___target_1; }
	inline Transform_t3275118058 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3275118058 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS78_0_T1881678406_H
#ifndef U3CU3EC__DISPLAYCLASS79_0_T295391489_H
#define U3CU3EC__DISPLAYCLASS79_0_T295391489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0
struct  U3CU3Ec__DisplayClass79_0_t295391489  : public RuntimeObject
{
public:
	// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0::to
	Quaternion_t4030073918  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass79_0::target
	Transform_t3275118058 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass79_0_t295391489, ___to_0)); }
	inline Quaternion_t4030073918  get_to_0() const { return ___to_0; }
	inline Quaternion_t4030073918 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Quaternion_t4030073918  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass79_0_t295391489, ___target_1)); }
	inline Transform_t3275118058 * get_target_1() const { return ___target_1; }
	inline Transform_t3275118058 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3275118058 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS79_0_T295391489_H
#ifndef U3CU3EC__DISPLAYCLASS80_0_T3010978755_H
#define U3CU3EC__DISPLAYCLASS80_0_T3010978755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0
struct  U3CU3Ec__DisplayClass80_0_t3010978755  : public RuntimeObject
{
public:
	// UnityEngine.Quaternion DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0::to
	Quaternion_t4030073918  ___to_0;
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass80_0::target
	Transform_t3275118058 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t3010978755, ___to_0)); }
	inline Quaternion_t4030073918  get_to_0() const { return ___to_0; }
	inline Quaternion_t4030073918 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Quaternion_t4030073918  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass80_0_t3010978755, ___target_1)); }
	inline Transform_t3275118058 * get_target_1() const { return ___target_1; }
	inline Transform_t3275118058 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Transform_t3275118058 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS80_0_T3010978755_H
#ifndef U3CU3EC__DISPLAYCLASS76_0_T3293303416_H
#define U3CU3EC__DISPLAYCLASS76_0_T3293303416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0
struct  U3CU3Ec__DisplayClass76_0_t3293303416  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::to
	Color_t2020392075  ___to_0;
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::target
	Material_t193706927 * ___target_1;
	// System.String DG.Tweening.ShortcutExtensions/<>c__DisplayClass76_0::property
	String_t* ___property_2;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t3293303416, ___to_0)); }
	inline Color_t2020392075  get_to_0() const { return ___to_0; }
	inline Color_t2020392075 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2020392075  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t3293303416, ___target_1)); }
	inline Material_t193706927 * get_target_1() const { return ___target_1; }
	inline Material_t193706927 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Material_t193706927 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}

	inline static int32_t get_offset_of_property_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass76_0_t3293303416, ___property_2)); }
	inline String_t* get_property_2() const { return ___property_2; }
	inline String_t** get_address_of_property_2() { return &___property_2; }
	inline void set_property_2(String_t* value)
	{
		___property_2 = value;
		Il2CppCodeGenWriteBarrier((&___property_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS76_0_T3293303416_H
#ifndef PATHTYPE_T2815988833_H
#define PATHTYPE_T2815988833_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathType
struct  PathType_t2815988833 
{
public:
	// System.Int32 DG.Tweening.PathType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathType_t2815988833, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHTYPE_T2815988833_H
#ifndef SCRAMBLEMODE_T385206138_H
#define SCRAMBLEMODE_T385206138_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ScrambleMode
struct  ScrambleMode_t385206138 
{
public:
	// System.Int32 DG.Tweening.ScrambleMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrambleMode_t385206138, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRAMBLEMODE_T385206138_H
#ifndef NULLABLE_1_T506773895_H
#define NULLABLE_1_T506773895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_t506773895 
{
public:
	// T System.Nullable`1::value
	Vector3_t2243707580  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t506773895, ___value_0)); }
	inline Vector3_t2243707580  get_value_0() const { return ___value_0; }
	inline Vector3_t2243707580 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_t2243707580  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t506773895, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T506773895_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef U3CU3EC__DISPLAYCLASS69_0_T295391392_H
#define U3CU3EC__DISPLAYCLASS69_0_T295391392_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0
struct  U3CU3Ec__DisplayClass69_0_t295391392  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::target
	Transform_t3275118058 * ___target_0;
	// System.Boolean DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::offsetYSet
	bool ___offsetYSet_1;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::offsetY
	float ___offsetY_2;
	// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::s
	Sequence_t110643099 * ___s_3;
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::endValue
	Vector3_t2243707580  ___endValue_4;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass69_0::startPosY
	float ___startPosY_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t295391392, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_offsetYSet_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t295391392, ___offsetYSet_1)); }
	inline bool get_offsetYSet_1() const { return ___offsetYSet_1; }
	inline bool* get_address_of_offsetYSet_1() { return &___offsetYSet_1; }
	inline void set_offsetYSet_1(bool value)
	{
		___offsetYSet_1 = value;
	}

	inline static int32_t get_offset_of_offsetY_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t295391392, ___offsetY_2)); }
	inline float get_offsetY_2() const { return ___offsetY_2; }
	inline float* get_address_of_offsetY_2() { return &___offsetY_2; }
	inline void set_offsetY_2(float value)
	{
		___offsetY_2 = value;
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t295391392, ___s_3)); }
	inline Sequence_t110643099 * get_s_3() const { return ___s_3; }
	inline Sequence_t110643099 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(Sequence_t110643099 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_endValue_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t295391392, ___endValue_4)); }
	inline Vector3_t2243707580  get_endValue_4() const { return ___endValue_4; }
	inline Vector3_t2243707580 * get_address_of_endValue_4() { return &___endValue_4; }
	inline void set_endValue_4(Vector3_t2243707580  value)
	{
		___endValue_4 = value;
	}

	inline static int32_t get_offset_of_startPosY_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t295391392, ___startPosY_5)); }
	inline float get_startPosY_5() const { return ___startPosY_5; }
	inline float* get_address_of_startPosY_5() { return &___startPosY_5; }
	inline void set_startPosY_5(float value)
	{
		___startPosY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS69_0_T295391392_H
#ifndef U3CU3EC__DISPLAYCLASS74_0_T3575628418_H
#define U3CU3EC__DISPLAYCLASS74_0_T3575628418_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0
struct  U3CU3Ec__DisplayClass74_0_t3575628418  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0::to
	Color_t2020392075  ___to_0;
	// UnityEngine.Light DG.Tweening.ShortcutExtensions/<>c__DisplayClass74_0::target
	Light_t494725636 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass74_0_t3575628418, ___to_0)); }
	inline Color_t2020392075  get_to_0() const { return ___to_0; }
	inline Color_t2020392075 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2020392075  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass74_0_t3575628418, ___target_1)); }
	inline Light_t494725636 * get_target_1() const { return ___target_1; }
	inline Light_t494725636 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Light_t494725636 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS74_0_T3575628418_H
#ifndef U3CU3EC__DISPLAYCLASS75_0_T1989341501_H
#define U3CU3EC__DISPLAYCLASS75_0_T1989341501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0
struct  U3CU3Ec__DisplayClass75_0_t1989341501  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::to
	Color_t2020392075  ___to_0;
	// UnityEngine.Material DG.Tweening.ShortcutExtensions/<>c__DisplayClass75_0::target
	Material_t193706927 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass75_0_t1989341501, ___to_0)); }
	inline Color_t2020392075  get_to_0() const { return ___to_0; }
	inline Color_t2020392075 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_t2020392075  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass75_0_t1989341501, ___target_1)); }
	inline Material_t193706927 * get_target_1() const { return ___target_1; }
	inline Material_t193706927 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Material_t193706927 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((&___target_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS75_0_T1989341501_H
#ifndef PATHMODE_T1545785466_H
#define PATHMODE_T1545785466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.PathMode
struct  PathMode_t1545785466 
{
public:
	// System.Int32 DG.Tweening.PathMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PathMode_t1545785466, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHMODE_T1545785466_H
#ifndef ROTATEMODE_T1177727514_H
#define ROTATEMODE_T1177727514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.RotateMode
struct  RotateMode_t1177727514 
{
public:
	// System.Int32 DG.Tweening.RotateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotateMode_t1177727514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T1177727514_H
#ifndef AXISCONSTRAINT_T1244566668_H
#define AXISCONSTRAINT_T1244566668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.AxisConstraint
struct  AxisConstraint_t1244566668 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AxisConstraint_t1244566668, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AXISCONSTRAINT_T1244566668_H
#ifndef CONTROLPOINT_T168081159_H
#define CONTROLPOINT_T168081159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.ControlPoint
struct  ControlPoint_t168081159 
{
public:
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::a
	Vector3_t2243707580  ___a_0;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.ControlPoint::b
	Vector3_t2243707580  ___b_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(ControlPoint_t168081159, ___a_0)); }
	inline Vector3_t2243707580  get_a_0() const { return ___a_0; }
	inline Vector3_t2243707580 * get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(Vector3_t2243707580  value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_b_1() { return static_cast<int32_t>(offsetof(ControlPoint_t168081159, ___b_1)); }
	inline Vector3_t2243707580  get_b_1() const { return ___b_1; }
	inline Vector3_t2243707580 * get_address_of_b_1() { return &___b_1; }
	inline void set_b_1(Vector3_t2243707580  value)
	{
		___b_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTROLPOINT_T168081159_H
#ifndef U3CU3EC__DISPLAYCLASS68_0_T1881678309_H
#define U3CU3EC__DISPLAYCLASS68_0_T1881678309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0
struct  U3CU3Ec__DisplayClass68_0_t1881678309  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::target
	Transform_t3275118058 * ___target_0;
	// System.Boolean DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::offsetYSet
	bool ___offsetYSet_1;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::offsetY
	float ___offsetY_2;
	// DG.Tweening.Sequence DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::s
	Sequence_t110643099 * ___s_3;
	// UnityEngine.Vector3 DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::endValue
	Vector3_t2243707580  ___endValue_4;
	// System.Single DG.Tweening.ShortcutExtensions/<>c__DisplayClass68_0::startPosY
	float ___startPosY_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t1881678309, ___target_0)); }
	inline Transform_t3275118058 * get_target_0() const { return ___target_0; }
	inline Transform_t3275118058 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Transform_t3275118058 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((&___target_0), value);
	}

	inline static int32_t get_offset_of_offsetYSet_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t1881678309, ___offsetYSet_1)); }
	inline bool get_offsetYSet_1() const { return ___offsetYSet_1; }
	inline bool* get_address_of_offsetYSet_1() { return &___offsetYSet_1; }
	inline void set_offsetYSet_1(bool value)
	{
		___offsetYSet_1 = value;
	}

	inline static int32_t get_offset_of_offsetY_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t1881678309, ___offsetY_2)); }
	inline float get_offsetY_2() const { return ___offsetY_2; }
	inline float* get_address_of_offsetY_2() { return &___offsetY_2; }
	inline void set_offsetY_2(float value)
	{
		___offsetY_2 = value;
	}

	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t1881678309, ___s_3)); }
	inline Sequence_t110643099 * get_s_3() const { return ___s_3; }
	inline Sequence_t110643099 ** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(Sequence_t110643099 * value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_endValue_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t1881678309, ___endValue_4)); }
	inline Vector3_t2243707580  get_endValue_4() const { return ___endValue_4; }
	inline Vector3_t2243707580 * get_address_of_endValue_4() { return &___endValue_4; }
	inline void set_endValue_4(Vector3_t2243707580  value)
	{
		___endValue_4 = value;
	}

	inline static int32_t get_offset_of_startPosY_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_t1881678309, ___startPosY_5)); }
	inline float get_startPosY_5() const { return ___startPosY_5; }
	inline float* get_address_of_startPosY_5() { return &___startPosY_5; }
	inline void set_startPosY_5(float value)
	{
		___startPosY_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS68_0_T1881678309_H
#ifndef EASE_T2502520296_H
#define EASE_T2502520296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Ease
struct  Ease_t2502520296 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Ease_t2502520296, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EASE_T2502520296_H
#ifndef ORIENTTYPE_T1755667719_H
#define ORIENTTYPE_T1755667719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.OrientType
struct  OrientType_t1755667719 
{
public:
	// System.Int32 DG.Tweening.Plugins.Options.OrientType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OrientType_t1755667719, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ORIENTTYPE_T1755667719_H
#ifndef LOOPTYPE_T2249218064_H
#define LOOPTYPE_T2249218064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.LoopType
struct  LoopType_t2249218064 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LoopType_t2249218064, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOOPTYPE_T2249218064_H
#ifndef SPECIALSTARTUPMODE_T1501334721_H
#define SPECIALSTARTUPMODE_T1501334721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_t1501334721 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpecialStartupMode_t1501334721, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALSTARTUPMODE_T1501334721_H
#ifndef TWEENTYPE_T169444141_H
#define TWEENTYPE_T169444141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenType
struct  TweenType_t169444141 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TweenType_t169444141, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENTYPE_T169444141_H
#ifndef UPDATETYPE_T3357224513_H
#define UPDATETYPE_T3357224513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.UpdateType
struct  UpdateType_t3357224513 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UpdateType_t3357224513, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATETYPE_T3357224513_H
#ifndef QUATERNIONOPTIONS_T466049668_H
#define QUATERNIONOPTIONS_T466049668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.QuaternionOptions
struct  QuaternionOptions_t466049668 
{
public:
	// DG.Tweening.RotateMode DG.Tweening.Plugins.Options.QuaternionOptions::rotateMode
	int32_t ___rotateMode_0;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.QuaternionOptions::axisConstraint
	int32_t ___axisConstraint_1;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.QuaternionOptions::up
	Vector3_t2243707580  ___up_2;

public:
	inline static int32_t get_offset_of_rotateMode_0() { return static_cast<int32_t>(offsetof(QuaternionOptions_t466049668, ___rotateMode_0)); }
	inline int32_t get_rotateMode_0() const { return ___rotateMode_0; }
	inline int32_t* get_address_of_rotateMode_0() { return &___rotateMode_0; }
	inline void set_rotateMode_0(int32_t value)
	{
		___rotateMode_0 = value;
	}

	inline static int32_t get_offset_of_axisConstraint_1() { return static_cast<int32_t>(offsetof(QuaternionOptions_t466049668, ___axisConstraint_1)); }
	inline int32_t get_axisConstraint_1() const { return ___axisConstraint_1; }
	inline int32_t* get_address_of_axisConstraint_1() { return &___axisConstraint_1; }
	inline void set_axisConstraint_1(int32_t value)
	{
		___axisConstraint_1 = value;
	}

	inline static int32_t get_offset_of_up_2() { return static_cast<int32_t>(offsetof(QuaternionOptions_t466049668, ___up_2)); }
	inline Vector3_t2243707580  get_up_2() const { return ___up_2; }
	inline Vector3_t2243707580 * get_address_of_up_2() { return &___up_2; }
	inline void set_up_2(Vector3_t2243707580  value)
	{
		___up_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNIONOPTIONS_T466049668_H
#ifndef VECTOROPTIONS_T293385261_H
#define VECTOROPTIONS_T293385261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.VectorOptions
struct  VectorOptions_t293385261 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.VectorOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.VectorOptions::snapping
	bool ___snapping_1;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(VectorOptions_t293385261, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(VectorOptions_t293385261, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t293385261_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t293385261_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
#endif // VECTOROPTIONS_T293385261_H
#ifndef STRINGOPTIONS_T2885323933_H
#define STRINGOPTIONS_T2885323933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.StringOptions
struct  StringOptions_t2885323933 
{
public:
	// System.Boolean DG.Tweening.Plugins.Options.StringOptions::richTextEnabled
	bool ___richTextEnabled_0;
	// DG.Tweening.ScrambleMode DG.Tweening.Plugins.Options.StringOptions::scrambleMode
	int32_t ___scrambleMode_1;
	// System.Char[] DG.Tweening.Plugins.Options.StringOptions::scrambledChars
	CharU5BU5D_t1328083999* ___scrambledChars_2;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::startValueStrippedLength
	int32_t ___startValueStrippedLength_3;
	// System.Int32 DG.Tweening.Plugins.Options.StringOptions::changeValueStrippedLength
	int32_t ___changeValueStrippedLength_4;

public:
	inline static int32_t get_offset_of_richTextEnabled_0() { return static_cast<int32_t>(offsetof(StringOptions_t2885323933, ___richTextEnabled_0)); }
	inline bool get_richTextEnabled_0() const { return ___richTextEnabled_0; }
	inline bool* get_address_of_richTextEnabled_0() { return &___richTextEnabled_0; }
	inline void set_richTextEnabled_0(bool value)
	{
		___richTextEnabled_0 = value;
	}

	inline static int32_t get_offset_of_scrambleMode_1() { return static_cast<int32_t>(offsetof(StringOptions_t2885323933, ___scrambleMode_1)); }
	inline int32_t get_scrambleMode_1() const { return ___scrambleMode_1; }
	inline int32_t* get_address_of_scrambleMode_1() { return &___scrambleMode_1; }
	inline void set_scrambleMode_1(int32_t value)
	{
		___scrambleMode_1 = value;
	}

	inline static int32_t get_offset_of_scrambledChars_2() { return static_cast<int32_t>(offsetof(StringOptions_t2885323933, ___scrambledChars_2)); }
	inline CharU5BU5D_t1328083999* get_scrambledChars_2() const { return ___scrambledChars_2; }
	inline CharU5BU5D_t1328083999** get_address_of_scrambledChars_2() { return &___scrambledChars_2; }
	inline void set_scrambledChars_2(CharU5BU5D_t1328083999* value)
	{
		___scrambledChars_2 = value;
		Il2CppCodeGenWriteBarrier((&___scrambledChars_2), value);
	}

	inline static int32_t get_offset_of_startValueStrippedLength_3() { return static_cast<int32_t>(offsetof(StringOptions_t2885323933, ___startValueStrippedLength_3)); }
	inline int32_t get_startValueStrippedLength_3() const { return ___startValueStrippedLength_3; }
	inline int32_t* get_address_of_startValueStrippedLength_3() { return &___startValueStrippedLength_3; }
	inline void set_startValueStrippedLength_3(int32_t value)
	{
		___startValueStrippedLength_3 = value;
	}

	inline static int32_t get_offset_of_changeValueStrippedLength_4() { return static_cast<int32_t>(offsetof(StringOptions_t2885323933, ___changeValueStrippedLength_4)); }
	inline int32_t get_changeValueStrippedLength_4() const { return ___changeValueStrippedLength_4; }
	inline int32_t* get_address_of_changeValueStrippedLength_4() { return &___changeValueStrippedLength_4; }
	inline void set_changeValueStrippedLength_4(int32_t value)
	{
		___changeValueStrippedLength_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t2885323933_marshaled_pinvoke
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.StringOptions
struct StringOptions_t2885323933_marshaled_com
{
	int32_t ___richTextEnabled_0;
	int32_t ___scrambleMode_1;
	uint8_t* ___scrambledChars_2;
	int32_t ___startValueStrippedLength_3;
	int32_t ___changeValueStrippedLength_4;
};
#endif // STRINGOPTIONS_T2885323933_H
#ifndef PATHOPTIONS_T2659884781_H
#define PATHOPTIONS_T2659884781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.PathOptions
struct  PathOptions_t2659884781 
{
public:
	// DG.Tweening.PathMode DG.Tweening.Plugins.Options.PathOptions::mode
	int32_t ___mode_0;
	// DG.Tweening.Plugins.Options.OrientType DG.Tweening.Plugins.Options.PathOptions::orientType
	int32_t ___orientType_1;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockPositionAxis
	int32_t ___lockPositionAxis_2;
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.PathOptions::lockRotationAxis
	int32_t ___lockRotationAxis_3;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isClosedPath
	bool ___isClosedPath_4;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Options.PathOptions::lookAtPosition
	Vector3_t2243707580  ___lookAtPosition_5;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::lookAtTransform
	Transform_t3275118058 * ___lookAtTransform_6;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::lookAhead
	float ___lookAhead_7;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::hasCustomForwardDirection
	bool ___hasCustomForwardDirection_8;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::forward
	Quaternion_t4030073918  ___forward_9;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::useLocalPosition
	bool ___useLocalPosition_10;
	// UnityEngine.Transform DG.Tweening.Plugins.Options.PathOptions::parent
	Transform_t3275118058 * ___parent_11;
	// System.Boolean DG.Tweening.Plugins.Options.PathOptions::isRigidbody
	bool ___isRigidbody_12;
	// UnityEngine.Quaternion DG.Tweening.Plugins.Options.PathOptions::startupRot
	Quaternion_t4030073918  ___startupRot_13;
	// System.Single DG.Tweening.Plugins.Options.PathOptions::startupZRot
	float ___startupZRot_14;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_orientType_1() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___orientType_1)); }
	inline int32_t get_orientType_1() const { return ___orientType_1; }
	inline int32_t* get_address_of_orientType_1() { return &___orientType_1; }
	inline void set_orientType_1(int32_t value)
	{
		___orientType_1 = value;
	}

	inline static int32_t get_offset_of_lockPositionAxis_2() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___lockPositionAxis_2)); }
	inline int32_t get_lockPositionAxis_2() const { return ___lockPositionAxis_2; }
	inline int32_t* get_address_of_lockPositionAxis_2() { return &___lockPositionAxis_2; }
	inline void set_lockPositionAxis_2(int32_t value)
	{
		___lockPositionAxis_2 = value;
	}

	inline static int32_t get_offset_of_lockRotationAxis_3() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___lockRotationAxis_3)); }
	inline int32_t get_lockRotationAxis_3() const { return ___lockRotationAxis_3; }
	inline int32_t* get_address_of_lockRotationAxis_3() { return &___lockRotationAxis_3; }
	inline void set_lockRotationAxis_3(int32_t value)
	{
		___lockRotationAxis_3 = value;
	}

	inline static int32_t get_offset_of_isClosedPath_4() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___isClosedPath_4)); }
	inline bool get_isClosedPath_4() const { return ___isClosedPath_4; }
	inline bool* get_address_of_isClosedPath_4() { return &___isClosedPath_4; }
	inline void set_isClosedPath_4(bool value)
	{
		___isClosedPath_4 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_5() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___lookAtPosition_5)); }
	inline Vector3_t2243707580  get_lookAtPosition_5() const { return ___lookAtPosition_5; }
	inline Vector3_t2243707580 * get_address_of_lookAtPosition_5() { return &___lookAtPosition_5; }
	inline void set_lookAtPosition_5(Vector3_t2243707580  value)
	{
		___lookAtPosition_5 = value;
	}

	inline static int32_t get_offset_of_lookAtTransform_6() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___lookAtTransform_6)); }
	inline Transform_t3275118058 * get_lookAtTransform_6() const { return ___lookAtTransform_6; }
	inline Transform_t3275118058 ** get_address_of_lookAtTransform_6() { return &___lookAtTransform_6; }
	inline void set_lookAtTransform_6(Transform_t3275118058 * value)
	{
		___lookAtTransform_6 = value;
		Il2CppCodeGenWriteBarrier((&___lookAtTransform_6), value);
	}

	inline static int32_t get_offset_of_lookAhead_7() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___lookAhead_7)); }
	inline float get_lookAhead_7() const { return ___lookAhead_7; }
	inline float* get_address_of_lookAhead_7() { return &___lookAhead_7; }
	inline void set_lookAhead_7(float value)
	{
		___lookAhead_7 = value;
	}

	inline static int32_t get_offset_of_hasCustomForwardDirection_8() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___hasCustomForwardDirection_8)); }
	inline bool get_hasCustomForwardDirection_8() const { return ___hasCustomForwardDirection_8; }
	inline bool* get_address_of_hasCustomForwardDirection_8() { return &___hasCustomForwardDirection_8; }
	inline void set_hasCustomForwardDirection_8(bool value)
	{
		___hasCustomForwardDirection_8 = value;
	}

	inline static int32_t get_offset_of_forward_9() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___forward_9)); }
	inline Quaternion_t4030073918  get_forward_9() const { return ___forward_9; }
	inline Quaternion_t4030073918 * get_address_of_forward_9() { return &___forward_9; }
	inline void set_forward_9(Quaternion_t4030073918  value)
	{
		___forward_9 = value;
	}

	inline static int32_t get_offset_of_useLocalPosition_10() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___useLocalPosition_10)); }
	inline bool get_useLocalPosition_10() const { return ___useLocalPosition_10; }
	inline bool* get_address_of_useLocalPosition_10() { return &___useLocalPosition_10; }
	inline void set_useLocalPosition_10(bool value)
	{
		___useLocalPosition_10 = value;
	}

	inline static int32_t get_offset_of_parent_11() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___parent_11)); }
	inline Transform_t3275118058 * get_parent_11() const { return ___parent_11; }
	inline Transform_t3275118058 ** get_address_of_parent_11() { return &___parent_11; }
	inline void set_parent_11(Transform_t3275118058 * value)
	{
		___parent_11 = value;
		Il2CppCodeGenWriteBarrier((&___parent_11), value);
	}

	inline static int32_t get_offset_of_isRigidbody_12() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___isRigidbody_12)); }
	inline bool get_isRigidbody_12() const { return ___isRigidbody_12; }
	inline bool* get_address_of_isRigidbody_12() { return &___isRigidbody_12; }
	inline void set_isRigidbody_12(bool value)
	{
		___isRigidbody_12 = value;
	}

	inline static int32_t get_offset_of_startupRot_13() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___startupRot_13)); }
	inline Quaternion_t4030073918  get_startupRot_13() const { return ___startupRot_13; }
	inline Quaternion_t4030073918 * get_address_of_startupRot_13() { return &___startupRot_13; }
	inline void set_startupRot_13(Quaternion_t4030073918  value)
	{
		___startupRot_13 = value;
	}

	inline static int32_t get_offset_of_startupZRot_14() { return static_cast<int32_t>(offsetof(PathOptions_t2659884781, ___startupZRot_14)); }
	inline float get_startupZRot_14() const { return ___startupZRot_14; }
	inline float* get_address_of_startupZRot_14() { return &___startupZRot_14; }
	inline void set_startupZRot_14(float value)
	{
		___startupZRot_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_t2659884781_marshaled_pinvoke
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t2243707580  ___lookAtPosition_5;
	Transform_t3275118058 * ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_t4030073918  ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_t3275118058 * ___parent_11;
	int32_t ___isRigidbody_12;
	Quaternion_t4030073918  ___startupRot_13;
	float ___startupZRot_14;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.PathOptions
struct PathOptions_t2659884781_marshaled_com
{
	int32_t ___mode_0;
	int32_t ___orientType_1;
	int32_t ___lockPositionAxis_2;
	int32_t ___lockRotationAxis_3;
	int32_t ___isClosedPath_4;
	Vector3_t2243707580  ___lookAtPosition_5;
	Transform_t3275118058 * ___lookAtTransform_6;
	float ___lookAhead_7;
	int32_t ___hasCustomForwardDirection_8;
	Quaternion_t4030073918  ___forward_9;
	int32_t ___useLocalPosition_10;
	Transform_t3275118058 * ___parent_11;
	int32_t ___isRigidbody_12;
	Quaternion_t4030073918  ___startupRot_13;
	float ___startupZRot_14;
};
#endif // PATHOPTIONS_T2659884781_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef ABSSEQUENTIABLE_T2284140720_H
#define ABSSEQUENTIABLE_T2284140720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_t2284140720  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_t3697142134 * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_t2284140720, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_t2284140720, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_t2284140720, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_t2284140720, ___onStart_3)); }
	inline TweenCallback_t3697142134 * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_t3697142134 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_t3697142134 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSSEQUENTIABLE_T2284140720_H
#ifndef TWEENPARAMS_T2944325381_H
#define TWEENPARAMS_T2944325381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.TweenParams
struct  TweenParams_t2944325381  : public RuntimeObject
{
public:
	// System.Object DG.Tweening.TweenParams::id
	RuntimeObject * ___id_1;
	// System.Object DG.Tweening.TweenParams::target
	RuntimeObject * ___target_2;
	// DG.Tweening.UpdateType DG.Tweening.TweenParams::updateType
	int32_t ___updateType_3;
	// System.Boolean DG.Tweening.TweenParams::isIndependentUpdate
	bool ___isIndependentUpdate_4;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onStart
	TweenCallback_t3697142134 * ___onStart_5;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onPlay
	TweenCallback_t3697142134 * ___onPlay_6;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onRewind
	TweenCallback_t3697142134 * ___onRewind_7;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onUpdate
	TweenCallback_t3697142134 * ___onUpdate_8;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onStepComplete
	TweenCallback_t3697142134 * ___onStepComplete_9;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onComplete
	TweenCallback_t3697142134 * ___onComplete_10;
	// DG.Tweening.TweenCallback DG.Tweening.TweenParams::onKill
	TweenCallback_t3697142134 * ___onKill_11;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.TweenParams::onWaypointChange
	TweenCallback_1_t3418705418 * ___onWaypointChange_12;
	// System.Boolean DG.Tweening.TweenParams::isRecyclable
	bool ___isRecyclable_13;
	// System.Boolean DG.Tweening.TweenParams::isSpeedBased
	bool ___isSpeedBased_14;
	// System.Boolean DG.Tweening.TweenParams::autoKill
	bool ___autoKill_15;
	// System.Int32 DG.Tweening.TweenParams::loops
	int32_t ___loops_16;
	// DG.Tweening.LoopType DG.Tweening.TweenParams::loopType
	int32_t ___loopType_17;
	// System.Single DG.Tweening.TweenParams::delay
	float ___delay_18;
	// System.Boolean DG.Tweening.TweenParams::isRelative
	bool ___isRelative_19;
	// DG.Tweening.Ease DG.Tweening.TweenParams::easeType
	int32_t ___easeType_20;
	// DG.Tweening.EaseFunction DG.Tweening.TweenParams::customEase
	EaseFunction_t3306356708 * ___customEase_21;
	// System.Single DG.Tweening.TweenParams::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_22;
	// System.Single DG.Tweening.TweenParams::easePeriod
	float ___easePeriod_23;

public:
	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___id_1)); }
	inline RuntimeObject * get_id_1() const { return ___id_1; }
	inline RuntimeObject ** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(RuntimeObject * value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___target_2)); }
	inline RuntimeObject * get_target_2() const { return ___target_2; }
	inline RuntimeObject ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(RuntimeObject * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_updateType_3() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___updateType_3)); }
	inline int32_t get_updateType_3() const { return ___updateType_3; }
	inline int32_t* get_address_of_updateType_3() { return &___updateType_3; }
	inline void set_updateType_3(int32_t value)
	{
		___updateType_3 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_4() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___isIndependentUpdate_4)); }
	inline bool get_isIndependentUpdate_4() const { return ___isIndependentUpdate_4; }
	inline bool* get_address_of_isIndependentUpdate_4() { return &___isIndependentUpdate_4; }
	inline void set_isIndependentUpdate_4(bool value)
	{
		___isIndependentUpdate_4 = value;
	}

	inline static int32_t get_offset_of_onStart_5() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___onStart_5)); }
	inline TweenCallback_t3697142134 * get_onStart_5() const { return ___onStart_5; }
	inline TweenCallback_t3697142134 ** get_address_of_onStart_5() { return &___onStart_5; }
	inline void set_onStart_5(TweenCallback_t3697142134 * value)
	{
		___onStart_5 = value;
		Il2CppCodeGenWriteBarrier((&___onStart_5), value);
	}

	inline static int32_t get_offset_of_onPlay_6() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___onPlay_6)); }
	inline TweenCallback_t3697142134 * get_onPlay_6() const { return ___onPlay_6; }
	inline TweenCallback_t3697142134 ** get_address_of_onPlay_6() { return &___onPlay_6; }
	inline void set_onPlay_6(TweenCallback_t3697142134 * value)
	{
		___onPlay_6 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_6), value);
	}

	inline static int32_t get_offset_of_onRewind_7() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___onRewind_7)); }
	inline TweenCallback_t3697142134 * get_onRewind_7() const { return ___onRewind_7; }
	inline TweenCallback_t3697142134 ** get_address_of_onRewind_7() { return &___onRewind_7; }
	inline void set_onRewind_7(TweenCallback_t3697142134 * value)
	{
		___onRewind_7 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_7), value);
	}

	inline static int32_t get_offset_of_onUpdate_8() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___onUpdate_8)); }
	inline TweenCallback_t3697142134 * get_onUpdate_8() const { return ___onUpdate_8; }
	inline TweenCallback_t3697142134 ** get_address_of_onUpdate_8() { return &___onUpdate_8; }
	inline void set_onUpdate_8(TweenCallback_t3697142134 * value)
	{
		___onUpdate_8 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_8), value);
	}

	inline static int32_t get_offset_of_onStepComplete_9() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___onStepComplete_9)); }
	inline TweenCallback_t3697142134 * get_onStepComplete_9() const { return ___onStepComplete_9; }
	inline TweenCallback_t3697142134 ** get_address_of_onStepComplete_9() { return &___onStepComplete_9; }
	inline void set_onStepComplete_9(TweenCallback_t3697142134 * value)
	{
		___onStepComplete_9 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_9), value);
	}

	inline static int32_t get_offset_of_onComplete_10() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___onComplete_10)); }
	inline TweenCallback_t3697142134 * get_onComplete_10() const { return ___onComplete_10; }
	inline TweenCallback_t3697142134 ** get_address_of_onComplete_10() { return &___onComplete_10; }
	inline void set_onComplete_10(TweenCallback_t3697142134 * value)
	{
		___onComplete_10 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_10), value);
	}

	inline static int32_t get_offset_of_onKill_11() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___onKill_11)); }
	inline TweenCallback_t3697142134 * get_onKill_11() const { return ___onKill_11; }
	inline TweenCallback_t3697142134 ** get_address_of_onKill_11() { return &___onKill_11; }
	inline void set_onKill_11(TweenCallback_t3697142134 * value)
	{
		___onKill_11 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_11), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_12() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___onWaypointChange_12)); }
	inline TweenCallback_1_t3418705418 * get_onWaypointChange_12() const { return ___onWaypointChange_12; }
	inline TweenCallback_1_t3418705418 ** get_address_of_onWaypointChange_12() { return &___onWaypointChange_12; }
	inline void set_onWaypointChange_12(TweenCallback_1_t3418705418 * value)
	{
		___onWaypointChange_12 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_12), value);
	}

	inline static int32_t get_offset_of_isRecyclable_13() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___isRecyclable_13)); }
	inline bool get_isRecyclable_13() const { return ___isRecyclable_13; }
	inline bool* get_address_of_isRecyclable_13() { return &___isRecyclable_13; }
	inline void set_isRecyclable_13(bool value)
	{
		___isRecyclable_13 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_14() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___isSpeedBased_14)); }
	inline bool get_isSpeedBased_14() const { return ___isSpeedBased_14; }
	inline bool* get_address_of_isSpeedBased_14() { return &___isSpeedBased_14; }
	inline void set_isSpeedBased_14(bool value)
	{
		___isSpeedBased_14 = value;
	}

	inline static int32_t get_offset_of_autoKill_15() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___autoKill_15)); }
	inline bool get_autoKill_15() const { return ___autoKill_15; }
	inline bool* get_address_of_autoKill_15() { return &___autoKill_15; }
	inline void set_autoKill_15(bool value)
	{
		___autoKill_15 = value;
	}

	inline static int32_t get_offset_of_loops_16() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___loops_16)); }
	inline int32_t get_loops_16() const { return ___loops_16; }
	inline int32_t* get_address_of_loops_16() { return &___loops_16; }
	inline void set_loops_16(int32_t value)
	{
		___loops_16 = value;
	}

	inline static int32_t get_offset_of_loopType_17() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___loopType_17)); }
	inline int32_t get_loopType_17() const { return ___loopType_17; }
	inline int32_t* get_address_of_loopType_17() { return &___loopType_17; }
	inline void set_loopType_17(int32_t value)
	{
		___loopType_17 = value;
	}

	inline static int32_t get_offset_of_delay_18() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___delay_18)); }
	inline float get_delay_18() const { return ___delay_18; }
	inline float* get_address_of_delay_18() { return &___delay_18; }
	inline void set_delay_18(float value)
	{
		___delay_18 = value;
	}

	inline static int32_t get_offset_of_isRelative_19() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___isRelative_19)); }
	inline bool get_isRelative_19() const { return ___isRelative_19; }
	inline bool* get_address_of_isRelative_19() { return &___isRelative_19; }
	inline void set_isRelative_19(bool value)
	{
		___isRelative_19 = value;
	}

	inline static int32_t get_offset_of_easeType_20() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___easeType_20)); }
	inline int32_t get_easeType_20() const { return ___easeType_20; }
	inline int32_t* get_address_of_easeType_20() { return &___easeType_20; }
	inline void set_easeType_20(int32_t value)
	{
		___easeType_20 = value;
	}

	inline static int32_t get_offset_of_customEase_21() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___customEase_21)); }
	inline EaseFunction_t3306356708 * get_customEase_21() const { return ___customEase_21; }
	inline EaseFunction_t3306356708 ** get_address_of_customEase_21() { return &___customEase_21; }
	inline void set_customEase_21(EaseFunction_t3306356708 * value)
	{
		___customEase_21 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_21), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_22() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___easeOvershootOrAmplitude_22)); }
	inline float get_easeOvershootOrAmplitude_22() const { return ___easeOvershootOrAmplitude_22; }
	inline float* get_address_of_easeOvershootOrAmplitude_22() { return &___easeOvershootOrAmplitude_22; }
	inline void set_easeOvershootOrAmplitude_22(float value)
	{
		___easeOvershootOrAmplitude_22 = value;
	}

	inline static int32_t get_offset_of_easePeriod_23() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381, ___easePeriod_23)); }
	inline float get_easePeriod_23() const { return ___easePeriod_23; }
	inline float* get_address_of_easePeriod_23() { return &___easePeriod_23; }
	inline void set_easePeriod_23(float value)
	{
		___easePeriod_23 = value;
	}
};

struct TweenParams_t2944325381_StaticFields
{
public:
	// DG.Tweening.TweenParams DG.Tweening.TweenParams::Params
	TweenParams_t2944325381 * ___Params_0;

public:
	inline static int32_t get_offset_of_Params_0() { return static_cast<int32_t>(offsetof(TweenParams_t2944325381_StaticFields, ___Params_0)); }
	inline TweenParams_t2944325381 * get_Params_0() const { return ___Params_0; }
	inline TweenParams_t2944325381 ** get_address_of_Params_0() { return &___Params_0; }
	inline void set_Params_0(TweenParams_t2944325381 * value)
	{
		___Params_0 = value;
		Il2CppCodeGenWriteBarrier((&___Params_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENPARAMS_T2944325381_H
#ifndef PATH_T2828565993_H
#define PATH_T2828565993_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Core.PathCore.Path
struct  Path_t2828565993  : public RuntimeObject
{
public:
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::wpLengths
	SingleU5BU5D_t577127397* ___wpLengths_2;
	// DG.Tweening.PathType DG.Tweening.Plugins.Core.PathCore.Path::type
	int32_t ___type_3;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisionsXSegment
	int32_t ___subdivisionsXSegment_4;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::subdivisions
	int32_t ___subdivisions_5;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::wps
	Vector3U5BU5D_t1172311765* ___wps_6;
	// DG.Tweening.Plugins.Core.PathCore.ControlPoint[] DG.Tweening.Plugins.Core.PathCore.Path::controlPoints
	ControlPointU5BU5D_t246481150* ___controlPoints_7;
	// System.Single DG.Tweening.Plugins.Core.PathCore.Path::length
	float ___length_8;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::isFinalized
	bool ___isFinalized_9;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::timesTable
	SingleU5BU5D_t577127397* ___timesTable_10;
	// System.Single[] DG.Tweening.Plugins.Core.PathCore.Path::lengthsTable
	SingleU5BU5D_t577127397* ___lengthsTable_11;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::linearWPIndex
	int32_t ___linearWPIndex_12;
	// DG.Tweening.Plugins.Core.PathCore.Path DG.Tweening.Plugins.Core.PathCore.Path::_incrementalClone
	Path_t2828565993 * ____incrementalClone_13;
	// System.Int32 DG.Tweening.Plugins.Core.PathCore.Path::_incrementalIndex
	int32_t ____incrementalIndex_14;
	// DG.Tweening.Plugins.Core.PathCore.ABSPathDecoder DG.Tweening.Plugins.Core.PathCore.Path::_decoder
	ABSPathDecoder_t3294469411 * ____decoder_15;
	// System.Boolean DG.Tweening.Plugins.Core.PathCore.Path::_changed
	bool ____changed_16;
	// UnityEngine.Vector3[] DG.Tweening.Plugins.Core.PathCore.Path::nonLinearDrawWps
	Vector3U5BU5D_t1172311765* ___nonLinearDrawWps_17;
	// UnityEngine.Vector3 DG.Tweening.Plugins.Core.PathCore.Path::targetPosition
	Vector3_t2243707580  ___targetPosition_18;
	// System.Nullable`1<UnityEngine.Vector3> DG.Tweening.Plugins.Core.PathCore.Path::lookAtPosition
	Nullable_1_t506773895  ___lookAtPosition_19;
	// UnityEngine.Color DG.Tweening.Plugins.Core.PathCore.Path::gizmoColor
	Color_t2020392075  ___gizmoColor_20;

public:
	inline static int32_t get_offset_of_wpLengths_2() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___wpLengths_2)); }
	inline SingleU5BU5D_t577127397* get_wpLengths_2() const { return ___wpLengths_2; }
	inline SingleU5BU5D_t577127397** get_address_of_wpLengths_2() { return &___wpLengths_2; }
	inline void set_wpLengths_2(SingleU5BU5D_t577127397* value)
	{
		___wpLengths_2 = value;
		Il2CppCodeGenWriteBarrier((&___wpLengths_2), value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_subdivisionsXSegment_4() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___subdivisionsXSegment_4)); }
	inline int32_t get_subdivisionsXSegment_4() const { return ___subdivisionsXSegment_4; }
	inline int32_t* get_address_of_subdivisionsXSegment_4() { return &___subdivisionsXSegment_4; }
	inline void set_subdivisionsXSegment_4(int32_t value)
	{
		___subdivisionsXSegment_4 = value;
	}

	inline static int32_t get_offset_of_subdivisions_5() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___subdivisions_5)); }
	inline int32_t get_subdivisions_5() const { return ___subdivisions_5; }
	inline int32_t* get_address_of_subdivisions_5() { return &___subdivisions_5; }
	inline void set_subdivisions_5(int32_t value)
	{
		___subdivisions_5 = value;
	}

	inline static int32_t get_offset_of_wps_6() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___wps_6)); }
	inline Vector3U5BU5D_t1172311765* get_wps_6() const { return ___wps_6; }
	inline Vector3U5BU5D_t1172311765** get_address_of_wps_6() { return &___wps_6; }
	inline void set_wps_6(Vector3U5BU5D_t1172311765* value)
	{
		___wps_6 = value;
		Il2CppCodeGenWriteBarrier((&___wps_6), value);
	}

	inline static int32_t get_offset_of_controlPoints_7() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___controlPoints_7)); }
	inline ControlPointU5BU5D_t246481150* get_controlPoints_7() const { return ___controlPoints_7; }
	inline ControlPointU5BU5D_t246481150** get_address_of_controlPoints_7() { return &___controlPoints_7; }
	inline void set_controlPoints_7(ControlPointU5BU5D_t246481150* value)
	{
		___controlPoints_7 = value;
		Il2CppCodeGenWriteBarrier((&___controlPoints_7), value);
	}

	inline static int32_t get_offset_of_length_8() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___length_8)); }
	inline float get_length_8() const { return ___length_8; }
	inline float* get_address_of_length_8() { return &___length_8; }
	inline void set_length_8(float value)
	{
		___length_8 = value;
	}

	inline static int32_t get_offset_of_isFinalized_9() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___isFinalized_9)); }
	inline bool get_isFinalized_9() const { return ___isFinalized_9; }
	inline bool* get_address_of_isFinalized_9() { return &___isFinalized_9; }
	inline void set_isFinalized_9(bool value)
	{
		___isFinalized_9 = value;
	}

	inline static int32_t get_offset_of_timesTable_10() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___timesTable_10)); }
	inline SingleU5BU5D_t577127397* get_timesTable_10() const { return ___timesTable_10; }
	inline SingleU5BU5D_t577127397** get_address_of_timesTable_10() { return &___timesTable_10; }
	inline void set_timesTable_10(SingleU5BU5D_t577127397* value)
	{
		___timesTable_10 = value;
		Il2CppCodeGenWriteBarrier((&___timesTable_10), value);
	}

	inline static int32_t get_offset_of_lengthsTable_11() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___lengthsTable_11)); }
	inline SingleU5BU5D_t577127397* get_lengthsTable_11() const { return ___lengthsTable_11; }
	inline SingleU5BU5D_t577127397** get_address_of_lengthsTable_11() { return &___lengthsTable_11; }
	inline void set_lengthsTable_11(SingleU5BU5D_t577127397* value)
	{
		___lengthsTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___lengthsTable_11), value);
	}

	inline static int32_t get_offset_of_linearWPIndex_12() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___linearWPIndex_12)); }
	inline int32_t get_linearWPIndex_12() const { return ___linearWPIndex_12; }
	inline int32_t* get_address_of_linearWPIndex_12() { return &___linearWPIndex_12; }
	inline void set_linearWPIndex_12(int32_t value)
	{
		___linearWPIndex_12 = value;
	}

	inline static int32_t get_offset_of__incrementalClone_13() { return static_cast<int32_t>(offsetof(Path_t2828565993, ____incrementalClone_13)); }
	inline Path_t2828565993 * get__incrementalClone_13() const { return ____incrementalClone_13; }
	inline Path_t2828565993 ** get_address_of__incrementalClone_13() { return &____incrementalClone_13; }
	inline void set__incrementalClone_13(Path_t2828565993 * value)
	{
		____incrementalClone_13 = value;
		Il2CppCodeGenWriteBarrier((&____incrementalClone_13), value);
	}

	inline static int32_t get_offset_of__incrementalIndex_14() { return static_cast<int32_t>(offsetof(Path_t2828565993, ____incrementalIndex_14)); }
	inline int32_t get__incrementalIndex_14() const { return ____incrementalIndex_14; }
	inline int32_t* get_address_of__incrementalIndex_14() { return &____incrementalIndex_14; }
	inline void set__incrementalIndex_14(int32_t value)
	{
		____incrementalIndex_14 = value;
	}

	inline static int32_t get_offset_of__decoder_15() { return static_cast<int32_t>(offsetof(Path_t2828565993, ____decoder_15)); }
	inline ABSPathDecoder_t3294469411 * get__decoder_15() const { return ____decoder_15; }
	inline ABSPathDecoder_t3294469411 ** get_address_of__decoder_15() { return &____decoder_15; }
	inline void set__decoder_15(ABSPathDecoder_t3294469411 * value)
	{
		____decoder_15 = value;
		Il2CppCodeGenWriteBarrier((&____decoder_15), value);
	}

	inline static int32_t get_offset_of__changed_16() { return static_cast<int32_t>(offsetof(Path_t2828565993, ____changed_16)); }
	inline bool get__changed_16() const { return ____changed_16; }
	inline bool* get_address_of__changed_16() { return &____changed_16; }
	inline void set__changed_16(bool value)
	{
		____changed_16 = value;
	}

	inline static int32_t get_offset_of_nonLinearDrawWps_17() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___nonLinearDrawWps_17)); }
	inline Vector3U5BU5D_t1172311765* get_nonLinearDrawWps_17() const { return ___nonLinearDrawWps_17; }
	inline Vector3U5BU5D_t1172311765** get_address_of_nonLinearDrawWps_17() { return &___nonLinearDrawWps_17; }
	inline void set_nonLinearDrawWps_17(Vector3U5BU5D_t1172311765* value)
	{
		___nonLinearDrawWps_17 = value;
		Il2CppCodeGenWriteBarrier((&___nonLinearDrawWps_17), value);
	}

	inline static int32_t get_offset_of_targetPosition_18() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___targetPosition_18)); }
	inline Vector3_t2243707580  get_targetPosition_18() const { return ___targetPosition_18; }
	inline Vector3_t2243707580 * get_address_of_targetPosition_18() { return &___targetPosition_18; }
	inline void set_targetPosition_18(Vector3_t2243707580  value)
	{
		___targetPosition_18 = value;
	}

	inline static int32_t get_offset_of_lookAtPosition_19() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___lookAtPosition_19)); }
	inline Nullable_1_t506773895  get_lookAtPosition_19() const { return ___lookAtPosition_19; }
	inline Nullable_1_t506773895 * get_address_of_lookAtPosition_19() { return &___lookAtPosition_19; }
	inline void set_lookAtPosition_19(Nullable_1_t506773895  value)
	{
		___lookAtPosition_19 = value;
	}

	inline static int32_t get_offset_of_gizmoColor_20() { return static_cast<int32_t>(offsetof(Path_t2828565993, ___gizmoColor_20)); }
	inline Color_t2020392075  get_gizmoColor_20() const { return ___gizmoColor_20; }
	inline Color_t2020392075 * get_address_of_gizmoColor_20() { return &___gizmoColor_20; }
	inline void set_gizmoColor_20(Color_t2020392075  value)
	{
		___gizmoColor_20 = value;
	}
};

struct Path_t2828565993_StaticFields
{
public:
	// DG.Tweening.Plugins.Core.PathCore.CatmullRomDecoder DG.Tweening.Plugins.Core.PathCore.Path::_catmullRomDecoder
	CatmullRomDecoder_t3014762178 * ____catmullRomDecoder_0;
	// DG.Tweening.Plugins.Core.PathCore.LinearDecoder DG.Tweening.Plugins.Core.PathCore.Path::_linearDecoder
	LinearDecoder_t2073524639 * ____linearDecoder_1;

public:
	inline static int32_t get_offset_of__catmullRomDecoder_0() { return static_cast<int32_t>(offsetof(Path_t2828565993_StaticFields, ____catmullRomDecoder_0)); }
	inline CatmullRomDecoder_t3014762178 * get__catmullRomDecoder_0() const { return ____catmullRomDecoder_0; }
	inline CatmullRomDecoder_t3014762178 ** get_address_of__catmullRomDecoder_0() { return &____catmullRomDecoder_0; }
	inline void set__catmullRomDecoder_0(CatmullRomDecoder_t3014762178 * value)
	{
		____catmullRomDecoder_0 = value;
		Il2CppCodeGenWriteBarrier((&____catmullRomDecoder_0), value);
	}

	inline static int32_t get_offset_of__linearDecoder_1() { return static_cast<int32_t>(offsetof(Path_t2828565993_StaticFields, ____linearDecoder_1)); }
	inline LinearDecoder_t2073524639 * get__linearDecoder_1() const { return ____linearDecoder_1; }
	inline LinearDecoder_t2073524639 ** get_address_of__linearDecoder_1() { return &____linearDecoder_1; }
	inline void set__linearDecoder_1(LinearDecoder_t2073524639 * value)
	{
		____linearDecoder_1 = value;
		Il2CppCodeGenWriteBarrier((&____linearDecoder_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATH_T2828565993_H
#ifndef VECTOR3ARRAYOPTIONS_T2672570171_H
#define VECTOR3ARRAYOPTIONS_T2672570171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct  Vector3ArrayOptions_t2672570171 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.Vector3ArrayOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.Vector3ArrayOptions::snapping
	bool ___snapping_1;
	// System.Single[] DG.Tweening.Plugins.Options.Vector3ArrayOptions::durations
	SingleU5BU5D_t577127397* ___durations_2;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t2672570171, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t2672570171, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}

	inline static int32_t get_offset_of_durations_2() { return static_cast<int32_t>(offsetof(Vector3ArrayOptions_t2672570171, ___durations_2)); }
	inline SingleU5BU5D_t577127397* get_durations_2() const { return ___durations_2; }
	inline SingleU5BU5D_t577127397** get_address_of_durations_2() { return &___durations_2; }
	inline void set_durations_2(SingleU5BU5D_t577127397* value)
	{
		___durations_2 = value;
		Il2CppCodeGenWriteBarrier((&___durations_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t2672570171_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	float* ___durations_2;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.Vector3ArrayOptions
struct Vector3ArrayOptions_t2672570171_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
	float* ___durations_2;
};
#endif // VECTOR3ARRAYOPTIONS_T2672570171_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef TWEEN_T278478013_H
#define TWEEN_T278478013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tween
struct  Tween_t278478013  : public ABSSequentiable_t2284140720
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_7;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_8;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_9;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_t3697142134 * ___onPlay_10;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_t3697142134 * ___onPause_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_t3697142134 * ___onRewind_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_t3697142134 * ___onUpdate_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_t3697142134 * ___onStepComplete_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_t3697142134 * ___onComplete_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_t3697142134 * ___onKill_16;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t3418705418 * ___onWaypointChange_17;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_18;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_19;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_20;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_21;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_22;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_23;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_24;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_25;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_26;
	// System.Boolean DG.Tweening.Tween::isRelative
	bool ___isRelative_27;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_28;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_t3306356708 * ___customEase_29;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_30;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_31;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_32;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_33;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_34;
	// System.Boolean DG.Tweening.Tween::active
	bool ___active_35;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_36;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_t110643099 * ___sequenceParent_37;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_38;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_39;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_40;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_41;
	// System.Boolean DG.Tweening.Tween::playedOnce
	bool ___playedOnce_42;
	// System.Single DG.Tweening.Tween::position
	float ___position_43;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_44;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_45;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_46;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_47;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_48;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_49;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_50;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((&___id_6), value);
	}

	inline static int32_t get_offset_of_target_7() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___target_7)); }
	inline RuntimeObject * get_target_7() const { return ___target_7; }
	inline RuntimeObject ** get_address_of_target_7() { return &___target_7; }
	inline void set_target_7(RuntimeObject * value)
	{
		___target_7 = value;
		Il2CppCodeGenWriteBarrier((&___target_7), value);
	}

	inline static int32_t get_offset_of_updateType_8() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___updateType_8)); }
	inline int32_t get_updateType_8() const { return ___updateType_8; }
	inline int32_t* get_address_of_updateType_8() { return &___updateType_8; }
	inline void set_updateType_8(int32_t value)
	{
		___updateType_8 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_9() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___isIndependentUpdate_9)); }
	inline bool get_isIndependentUpdate_9() const { return ___isIndependentUpdate_9; }
	inline bool* get_address_of_isIndependentUpdate_9() { return &___isIndependentUpdate_9; }
	inline void set_isIndependentUpdate_9(bool value)
	{
		___isIndependentUpdate_9 = value;
	}

	inline static int32_t get_offset_of_onPlay_10() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___onPlay_10)); }
	inline TweenCallback_t3697142134 * get_onPlay_10() const { return ___onPlay_10; }
	inline TweenCallback_t3697142134 ** get_address_of_onPlay_10() { return &___onPlay_10; }
	inline void set_onPlay_10(TweenCallback_t3697142134 * value)
	{
		___onPlay_10 = value;
		Il2CppCodeGenWriteBarrier((&___onPlay_10), value);
	}

	inline static int32_t get_offset_of_onPause_11() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___onPause_11)); }
	inline TweenCallback_t3697142134 * get_onPause_11() const { return ___onPause_11; }
	inline TweenCallback_t3697142134 ** get_address_of_onPause_11() { return &___onPause_11; }
	inline void set_onPause_11(TweenCallback_t3697142134 * value)
	{
		___onPause_11 = value;
		Il2CppCodeGenWriteBarrier((&___onPause_11), value);
	}

	inline static int32_t get_offset_of_onRewind_12() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___onRewind_12)); }
	inline TweenCallback_t3697142134 * get_onRewind_12() const { return ___onRewind_12; }
	inline TweenCallback_t3697142134 ** get_address_of_onRewind_12() { return &___onRewind_12; }
	inline void set_onRewind_12(TweenCallback_t3697142134 * value)
	{
		___onRewind_12 = value;
		Il2CppCodeGenWriteBarrier((&___onRewind_12), value);
	}

	inline static int32_t get_offset_of_onUpdate_13() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___onUpdate_13)); }
	inline TweenCallback_t3697142134 * get_onUpdate_13() const { return ___onUpdate_13; }
	inline TweenCallback_t3697142134 ** get_address_of_onUpdate_13() { return &___onUpdate_13; }
	inline void set_onUpdate_13(TweenCallback_t3697142134 * value)
	{
		___onUpdate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onUpdate_13), value);
	}

	inline static int32_t get_offset_of_onStepComplete_14() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___onStepComplete_14)); }
	inline TweenCallback_t3697142134 * get_onStepComplete_14() const { return ___onStepComplete_14; }
	inline TweenCallback_t3697142134 ** get_address_of_onStepComplete_14() { return &___onStepComplete_14; }
	inline void set_onStepComplete_14(TweenCallback_t3697142134 * value)
	{
		___onStepComplete_14 = value;
		Il2CppCodeGenWriteBarrier((&___onStepComplete_14), value);
	}

	inline static int32_t get_offset_of_onComplete_15() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___onComplete_15)); }
	inline TweenCallback_t3697142134 * get_onComplete_15() const { return ___onComplete_15; }
	inline TweenCallback_t3697142134 ** get_address_of_onComplete_15() { return &___onComplete_15; }
	inline void set_onComplete_15(TweenCallback_t3697142134 * value)
	{
		___onComplete_15 = value;
		Il2CppCodeGenWriteBarrier((&___onComplete_15), value);
	}

	inline static int32_t get_offset_of_onKill_16() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___onKill_16)); }
	inline TweenCallback_t3697142134 * get_onKill_16() const { return ___onKill_16; }
	inline TweenCallback_t3697142134 ** get_address_of_onKill_16() { return &___onKill_16; }
	inline void set_onKill_16(TweenCallback_t3697142134 * value)
	{
		___onKill_16 = value;
		Il2CppCodeGenWriteBarrier((&___onKill_16), value);
	}

	inline static int32_t get_offset_of_onWaypointChange_17() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___onWaypointChange_17)); }
	inline TweenCallback_1_t3418705418 * get_onWaypointChange_17() const { return ___onWaypointChange_17; }
	inline TweenCallback_1_t3418705418 ** get_address_of_onWaypointChange_17() { return &___onWaypointChange_17; }
	inline void set_onWaypointChange_17(TweenCallback_1_t3418705418 * value)
	{
		___onWaypointChange_17 = value;
		Il2CppCodeGenWriteBarrier((&___onWaypointChange_17), value);
	}

	inline static int32_t get_offset_of_isFrom_18() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___isFrom_18)); }
	inline bool get_isFrom_18() const { return ___isFrom_18; }
	inline bool* get_address_of_isFrom_18() { return &___isFrom_18; }
	inline void set_isFrom_18(bool value)
	{
		___isFrom_18 = value;
	}

	inline static int32_t get_offset_of_isBlendable_19() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___isBlendable_19)); }
	inline bool get_isBlendable_19() const { return ___isBlendable_19; }
	inline bool* get_address_of_isBlendable_19() { return &___isBlendable_19; }
	inline void set_isBlendable_19(bool value)
	{
		___isBlendable_19 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_20() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___isRecyclable_20)); }
	inline bool get_isRecyclable_20() const { return ___isRecyclable_20; }
	inline bool* get_address_of_isRecyclable_20() { return &___isRecyclable_20; }
	inline void set_isRecyclable_20(bool value)
	{
		___isRecyclable_20 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_21() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___isSpeedBased_21)); }
	inline bool get_isSpeedBased_21() const { return ___isSpeedBased_21; }
	inline bool* get_address_of_isSpeedBased_21() { return &___isSpeedBased_21; }
	inline void set_isSpeedBased_21(bool value)
	{
		___isSpeedBased_21 = value;
	}

	inline static int32_t get_offset_of_autoKill_22() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___autoKill_22)); }
	inline bool get_autoKill_22() const { return ___autoKill_22; }
	inline bool* get_address_of_autoKill_22() { return &___autoKill_22; }
	inline void set_autoKill_22(bool value)
	{
		___autoKill_22 = value;
	}

	inline static int32_t get_offset_of_duration_23() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___duration_23)); }
	inline float get_duration_23() const { return ___duration_23; }
	inline float* get_address_of_duration_23() { return &___duration_23; }
	inline void set_duration_23(float value)
	{
		___duration_23 = value;
	}

	inline static int32_t get_offset_of_loops_24() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___loops_24)); }
	inline int32_t get_loops_24() const { return ___loops_24; }
	inline int32_t* get_address_of_loops_24() { return &___loops_24; }
	inline void set_loops_24(int32_t value)
	{
		___loops_24 = value;
	}

	inline static int32_t get_offset_of_loopType_25() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___loopType_25)); }
	inline int32_t get_loopType_25() const { return ___loopType_25; }
	inline int32_t* get_address_of_loopType_25() { return &___loopType_25; }
	inline void set_loopType_25(int32_t value)
	{
		___loopType_25 = value;
	}

	inline static int32_t get_offset_of_delay_26() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___delay_26)); }
	inline float get_delay_26() const { return ___delay_26; }
	inline float* get_address_of_delay_26() { return &___delay_26; }
	inline void set_delay_26(float value)
	{
		___delay_26 = value;
	}

	inline static int32_t get_offset_of_isRelative_27() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___isRelative_27)); }
	inline bool get_isRelative_27() const { return ___isRelative_27; }
	inline bool* get_address_of_isRelative_27() { return &___isRelative_27; }
	inline void set_isRelative_27(bool value)
	{
		___isRelative_27 = value;
	}

	inline static int32_t get_offset_of_easeType_28() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___easeType_28)); }
	inline int32_t get_easeType_28() const { return ___easeType_28; }
	inline int32_t* get_address_of_easeType_28() { return &___easeType_28; }
	inline void set_easeType_28(int32_t value)
	{
		___easeType_28 = value;
	}

	inline static int32_t get_offset_of_customEase_29() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___customEase_29)); }
	inline EaseFunction_t3306356708 * get_customEase_29() const { return ___customEase_29; }
	inline EaseFunction_t3306356708 ** get_address_of_customEase_29() { return &___customEase_29; }
	inline void set_customEase_29(EaseFunction_t3306356708 * value)
	{
		___customEase_29 = value;
		Il2CppCodeGenWriteBarrier((&___customEase_29), value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_30() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___easeOvershootOrAmplitude_30)); }
	inline float get_easeOvershootOrAmplitude_30() const { return ___easeOvershootOrAmplitude_30; }
	inline float* get_address_of_easeOvershootOrAmplitude_30() { return &___easeOvershootOrAmplitude_30; }
	inline void set_easeOvershootOrAmplitude_30(float value)
	{
		___easeOvershootOrAmplitude_30 = value;
	}

	inline static int32_t get_offset_of_easePeriod_31() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___easePeriod_31)); }
	inline float get_easePeriod_31() const { return ___easePeriod_31; }
	inline float* get_address_of_easePeriod_31() { return &___easePeriod_31; }
	inline void set_easePeriod_31(float value)
	{
		___easePeriod_31 = value;
	}

	inline static int32_t get_offset_of_typeofT1_32() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___typeofT1_32)); }
	inline Type_t * get_typeofT1_32() const { return ___typeofT1_32; }
	inline Type_t ** get_address_of_typeofT1_32() { return &___typeofT1_32; }
	inline void set_typeofT1_32(Type_t * value)
	{
		___typeofT1_32 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT1_32), value);
	}

	inline static int32_t get_offset_of_typeofT2_33() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___typeofT2_33)); }
	inline Type_t * get_typeofT2_33() const { return ___typeofT2_33; }
	inline Type_t ** get_address_of_typeofT2_33() { return &___typeofT2_33; }
	inline void set_typeofT2_33(Type_t * value)
	{
		___typeofT2_33 = value;
		Il2CppCodeGenWriteBarrier((&___typeofT2_33), value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_34() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___typeofTPlugOptions_34)); }
	inline Type_t * get_typeofTPlugOptions_34() const { return ___typeofTPlugOptions_34; }
	inline Type_t ** get_address_of_typeofTPlugOptions_34() { return &___typeofTPlugOptions_34; }
	inline void set_typeofTPlugOptions_34(Type_t * value)
	{
		___typeofTPlugOptions_34 = value;
		Il2CppCodeGenWriteBarrier((&___typeofTPlugOptions_34), value);
	}

	inline static int32_t get_offset_of_active_35() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___active_35)); }
	inline bool get_active_35() const { return ___active_35; }
	inline bool* get_address_of_active_35() { return &___active_35; }
	inline void set_active_35(bool value)
	{
		___active_35 = value;
	}

	inline static int32_t get_offset_of_isSequenced_36() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___isSequenced_36)); }
	inline bool get_isSequenced_36() const { return ___isSequenced_36; }
	inline bool* get_address_of_isSequenced_36() { return &___isSequenced_36; }
	inline void set_isSequenced_36(bool value)
	{
		___isSequenced_36 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_37() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___sequenceParent_37)); }
	inline Sequence_t110643099 * get_sequenceParent_37() const { return ___sequenceParent_37; }
	inline Sequence_t110643099 ** get_address_of_sequenceParent_37() { return &___sequenceParent_37; }
	inline void set_sequenceParent_37(Sequence_t110643099 * value)
	{
		___sequenceParent_37 = value;
		Il2CppCodeGenWriteBarrier((&___sequenceParent_37), value);
	}

	inline static int32_t get_offset_of_activeId_38() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___activeId_38)); }
	inline int32_t get_activeId_38() const { return ___activeId_38; }
	inline int32_t* get_address_of_activeId_38() { return &___activeId_38; }
	inline void set_activeId_38(int32_t value)
	{
		___activeId_38 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_39() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___specialStartupMode_39)); }
	inline int32_t get_specialStartupMode_39() const { return ___specialStartupMode_39; }
	inline int32_t* get_address_of_specialStartupMode_39() { return &___specialStartupMode_39; }
	inline void set_specialStartupMode_39(int32_t value)
	{
		___specialStartupMode_39 = value;
	}

	inline static int32_t get_offset_of_creationLocked_40() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___creationLocked_40)); }
	inline bool get_creationLocked_40() const { return ___creationLocked_40; }
	inline bool* get_address_of_creationLocked_40() { return &___creationLocked_40; }
	inline void set_creationLocked_40(bool value)
	{
		___creationLocked_40 = value;
	}

	inline static int32_t get_offset_of_startupDone_41() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___startupDone_41)); }
	inline bool get_startupDone_41() const { return ___startupDone_41; }
	inline bool* get_address_of_startupDone_41() { return &___startupDone_41; }
	inline void set_startupDone_41(bool value)
	{
		___startupDone_41 = value;
	}

	inline static int32_t get_offset_of_playedOnce_42() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___playedOnce_42)); }
	inline bool get_playedOnce_42() const { return ___playedOnce_42; }
	inline bool* get_address_of_playedOnce_42() { return &___playedOnce_42; }
	inline void set_playedOnce_42(bool value)
	{
		___playedOnce_42 = value;
	}

	inline static int32_t get_offset_of_position_43() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___position_43)); }
	inline float get_position_43() const { return ___position_43; }
	inline float* get_address_of_position_43() { return &___position_43; }
	inline void set_position_43(float value)
	{
		___position_43 = value;
	}

	inline static int32_t get_offset_of_fullDuration_44() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___fullDuration_44)); }
	inline float get_fullDuration_44() const { return ___fullDuration_44; }
	inline float* get_address_of_fullDuration_44() { return &___fullDuration_44; }
	inline void set_fullDuration_44(float value)
	{
		___fullDuration_44 = value;
	}

	inline static int32_t get_offset_of_completedLoops_45() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___completedLoops_45)); }
	inline int32_t get_completedLoops_45() const { return ___completedLoops_45; }
	inline int32_t* get_address_of_completedLoops_45() { return &___completedLoops_45; }
	inline void set_completedLoops_45(int32_t value)
	{
		___completedLoops_45 = value;
	}

	inline static int32_t get_offset_of_isPlaying_46() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___isPlaying_46)); }
	inline bool get_isPlaying_46() const { return ___isPlaying_46; }
	inline bool* get_address_of_isPlaying_46() { return &___isPlaying_46; }
	inline void set_isPlaying_46(bool value)
	{
		___isPlaying_46 = value;
	}

	inline static int32_t get_offset_of_isComplete_47() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___isComplete_47)); }
	inline bool get_isComplete_47() const { return ___isComplete_47; }
	inline bool* get_address_of_isComplete_47() { return &___isComplete_47; }
	inline void set_isComplete_47(bool value)
	{
		___isComplete_47 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_48() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___elapsedDelay_48)); }
	inline float get_elapsedDelay_48() const { return ___elapsedDelay_48; }
	inline float* get_address_of_elapsedDelay_48() { return &___elapsedDelay_48; }
	inline void set_elapsedDelay_48(float value)
	{
		___elapsedDelay_48 = value;
	}

	inline static int32_t get_offset_of_delayComplete_49() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___delayComplete_49)); }
	inline bool get_delayComplete_49() const { return ___delayComplete_49; }
	inline bool* get_address_of_delayComplete_49() { return &___delayComplete_49; }
	inline void set_delayComplete_49(bool value)
	{
		___delayComplete_49 = value;
	}

	inline static int32_t get_offset_of_miscInt_50() { return static_cast<int32_t>(offsetof(Tween_t278478013, ___miscInt_50)); }
	inline int32_t get_miscInt_50() const { return ___miscInt_50; }
	inline int32_t* get_address_of_miscInt_50() { return &___miscInt_50; }
	inline void set_miscInt_50(int32_t value)
	{
		___miscInt_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEEN_T278478013_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef TWEENER_T760404022_H
#define TWEENER_T760404022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Tweener
struct  Tweener_t760404022  : public Tween_t278478013
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_51;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_52;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_51() { return static_cast<int32_t>(offsetof(Tweener_t760404022, ___hasManuallySetStartValue_51)); }
	inline bool get_hasManuallySetStartValue_51() const { return ___hasManuallySetStartValue_51; }
	inline bool* get_address_of_hasManuallySetStartValue_51() { return &___hasManuallySetStartValue_51; }
	inline void set_hasManuallySetStartValue_51(bool value)
	{
		___hasManuallySetStartValue_51 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_52() { return static_cast<int32_t>(offsetof(Tweener_t760404022, ___isFromAllowed_52)); }
	inline bool get_isFromAllowed_52() const { return ___isFromAllowed_52; }
	inline bool* get_address_of_isFromAllowed_52() { return &___isFromAllowed_52; }
	inline void set_isFromAllowed_52(bool value)
	{
		___isFromAllowed_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWEENER_T760404022_H
#ifndef DOTWEENCOMPONENT_T696744215_H
#define DOTWEENCOMPONENT_T696744215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DG.Tweening.Core.DOTweenComponent
struct  DOTweenComponent_t696744215  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 DG.Tweening.Core.DOTweenComponent::inspectorUpdater
	int32_t ___inspectorUpdater_2;
	// System.Single DG.Tweening.Core.DOTweenComponent::_unscaledTime
	float ____unscaledTime_3;
	// System.Single DG.Tweening.Core.DOTweenComponent::_unscaledDeltaTime
	float ____unscaledDeltaTime_4;
	// System.Boolean DG.Tweening.Core.DOTweenComponent::_duplicateToDestroy
	bool ____duplicateToDestroy_5;

public:
	inline static int32_t get_offset_of_inspectorUpdater_2() { return static_cast<int32_t>(offsetof(DOTweenComponent_t696744215, ___inspectorUpdater_2)); }
	inline int32_t get_inspectorUpdater_2() const { return ___inspectorUpdater_2; }
	inline int32_t* get_address_of_inspectorUpdater_2() { return &___inspectorUpdater_2; }
	inline void set_inspectorUpdater_2(int32_t value)
	{
		___inspectorUpdater_2 = value;
	}

	inline static int32_t get_offset_of__unscaledTime_3() { return static_cast<int32_t>(offsetof(DOTweenComponent_t696744215, ____unscaledTime_3)); }
	inline float get__unscaledTime_3() const { return ____unscaledTime_3; }
	inline float* get_address_of__unscaledTime_3() { return &____unscaledTime_3; }
	inline void set__unscaledTime_3(float value)
	{
		____unscaledTime_3 = value;
	}

	inline static int32_t get_offset_of__unscaledDeltaTime_4() { return static_cast<int32_t>(offsetof(DOTweenComponent_t696744215, ____unscaledDeltaTime_4)); }
	inline float get__unscaledDeltaTime_4() const { return ____unscaledDeltaTime_4; }
	inline float* get_address_of__unscaledDeltaTime_4() { return &____unscaledDeltaTime_4; }
	inline void set__unscaledDeltaTime_4(float value)
	{
		____unscaledDeltaTime_4 = value;
	}

	inline static int32_t get_offset_of__duplicateToDestroy_5() { return static_cast<int32_t>(offsetof(DOTweenComponent_t696744215, ____duplicateToDestroy_5)); }
	inline bool get__duplicateToDestroy_5() const { return ____duplicateToDestroy_5; }
	inline bool* get_address_of__duplicateToDestroy_5() { return &____duplicateToDestroy_5; }
	inline void set__duplicateToDestroy_5(bool value)
	{
		____duplicateToDestroy_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOTWEENCOMPONENT_T696744215_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (U3CU3Ec__DisplayClass37_0_t1707016623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[1] = 
{
	U3CU3Ec__DisplayClass37_0_t1707016623::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (U3CU3Ec__DisplayClass38_0_t1881678530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[2] = 
{
	U3CU3Ec__DisplayClass38_0_t1881678530::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass38_0_t1881678530::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (U3CU3Ec__DisplayClass39_0_t295391613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[1] = 
{
	U3CU3Ec__DisplayClass39_0_t295391613::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (U3CU3Ec__DisplayClass40_0_t3010978383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[1] = 
{
	U3CU3Ec__DisplayClass40_0_t3010978383::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (U3CU3Ec__DisplayClass41_0_t1424691466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[1] = 
{
	U3CU3Ec__DisplayClass41_0_t1424691466::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (U3CU3Ec__DisplayClass42_0_t2728653381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[1] = 
{
	U3CU3Ec__DisplayClass42_0_t2728653381::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (U3CU3Ec__DisplayClass43_0_t1142366464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[1] = 
{
	U3CU3Ec__DisplayClass43_0_t1142366464::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (U3CU3Ec__DisplayClass44_0_t3575628387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[1] = 
{
	U3CU3Ec__DisplayClass44_0_t3575628387::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (U3CU3Ec__DisplayClass45_0_t1989341470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[1] = 
{
	U3CU3Ec__DisplayClass45_0_t1989341470::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (U3CU3Ec__DisplayClass46_0_t3293303385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[1] = 
{
	U3CU3Ec__DisplayClass46_0_t3293303385::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (U3CU3Ec__DisplayClass47_0_t1707016468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[1] = 
{
	U3CU3Ec__DisplayClass47_0_t1707016468::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (U3CU3Ec__DisplayClass48_0_t1881678375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1711[1] = 
{
	U3CU3Ec__DisplayClass48_0_t1881678375::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (U3CU3Ec__DisplayClass49_0_t295391458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1712[1] = 
{
	U3CU3Ec__DisplayClass49_0_t295391458::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (U3CU3Ec__DisplayClass50_0_t3010978224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1713[1] = 
{
	U3CU3Ec__DisplayClass50_0_t3010978224::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (U3CU3Ec__DisplayClass51_0_t1424691307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1714[1] = 
{
	U3CU3Ec__DisplayClass51_0_t1424691307::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (U3CU3Ec__DisplayClass52_0_t2728653222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1715[1] = 
{
	U3CU3Ec__DisplayClass52_0_t2728653222::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (U3CU3Ec__DisplayClass53_0_t1142366305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[1] = 
{
	U3CU3Ec__DisplayClass53_0_t1142366305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (U3CU3Ec__DisplayClass54_0_t3575628228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[1] = 
{
	U3CU3Ec__DisplayClass54_0_t3575628228::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (U3CU3Ec__DisplayClass55_0_t1989341311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[1] = 
{
	U3CU3Ec__DisplayClass55_0_t1989341311::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (U3CU3Ec__DisplayClass56_0_t3293303226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1719[1] = 
{
	U3CU3Ec__DisplayClass56_0_t3293303226::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (U3CU3Ec__DisplayClass57_0_t1707016309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[1] = 
{
	U3CU3Ec__DisplayClass57_0_t1707016309::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (U3CU3Ec__DisplayClass58_0_t1881678216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[1] = 
{
	U3CU3Ec__DisplayClass58_0_t1881678216::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (U3CU3Ec__DisplayClass59_0_t295391299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[1] = 
{
	U3CU3Ec__DisplayClass59_0_t295391299::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (U3CU3Ec__DisplayClass60_0_t3010978317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1723[1] = 
{
	U3CU3Ec__DisplayClass60_0_t3010978317::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (U3CU3Ec__DisplayClass61_0_t1424691400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1724[1] = 
{
	U3CU3Ec__DisplayClass61_0_t1424691400::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (U3CU3Ec__DisplayClass62_0_t2728653315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[1] = 
{
	U3CU3Ec__DisplayClass62_0_t2728653315::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (U3CU3Ec__DisplayClass63_0_t1142366398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1726[1] = 
{
	U3CU3Ec__DisplayClass63_0_t1142366398::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (U3CU3Ec__DisplayClass64_0_t3575628321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1727[1] = 
{
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (U3CU3Ec__DisplayClass65_0_t1989341404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1728[1] = 
{
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (U3CU3Ec__DisplayClass66_0_t3293303319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[1] = 
{
	U3CU3Ec__DisplayClass66_0_t3293303319::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (U3CU3Ec__DisplayClass67_0_t1707016402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[1] = 
{
	U3CU3Ec__DisplayClass67_0_t1707016402::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (U3CU3Ec__DisplayClass68_0_t1881678309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1731[6] = 
{
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (U3CU3Ec__DisplayClass69_0_t295391392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[6] = 
{
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (U3CU3Ec__DisplayClass70_0_t3010978414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1733[1] = 
{
	U3CU3Ec__DisplayClass70_0_t3010978414::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (U3CU3Ec__DisplayClass71_0_t1424691497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[1] = 
{
	U3CU3Ec__DisplayClass71_0_t1424691497::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (U3CU3Ec__DisplayClass72_0_t2728653412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[1] = 
{
	U3CU3Ec__DisplayClass72_0_t2728653412::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (U3CU3Ec__DisplayClass73_0_t1142366495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[1] = 
{
	U3CU3Ec__DisplayClass73_0_t1142366495::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (U3CU3Ec__DisplayClass74_0_t3575628418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[2] = 
{
	U3CU3Ec__DisplayClass74_0_t3575628418::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass74_0_t3575628418::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (U3CU3Ec__DisplayClass75_0_t1989341501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[2] = 
{
	U3CU3Ec__DisplayClass75_0_t1989341501::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass75_0_t1989341501::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (U3CU3Ec__DisplayClass76_0_t3293303416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1739[3] = 
{
	U3CU3Ec__DisplayClass76_0_t3293303416::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass76_0_t3293303416::get_offset_of_target_1(),
	U3CU3Ec__DisplayClass76_0_t3293303416::get_offset_of_property_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (U3CU3Ec__DisplayClass77_0_t1707016499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[2] = 
{
	U3CU3Ec__DisplayClass77_0_t1707016499::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass77_0_t1707016499::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (U3CU3Ec__DisplayClass78_0_t1881678406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[2] = 
{
	U3CU3Ec__DisplayClass78_0_t1881678406::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass78_0_t1881678406::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (U3CU3Ec__DisplayClass79_0_t295391489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1742[2] = 
{
	U3CU3Ec__DisplayClass79_0_t295391489::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass79_0_t295391489::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (U3CU3Ec__DisplayClass80_0_t3010978755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[2] = 
{
	U3CU3Ec__DisplayClass80_0_t3010978755::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass80_0_t3010978755::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (U3CU3Ec__DisplayClass81_0_t1424691838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1744[2] = 
{
	U3CU3Ec__DisplayClass81_0_t1424691838::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass81_0_t1424691838::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (TweenParams_t2944325381), -1, sizeof(TweenParams_t2944325381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1745[24] = 
{
	TweenParams_t2944325381_StaticFields::get_offset_of_Params_0(),
	TweenParams_t2944325381::get_offset_of_id_1(),
	TweenParams_t2944325381::get_offset_of_target_2(),
	TweenParams_t2944325381::get_offset_of_updateType_3(),
	TweenParams_t2944325381::get_offset_of_isIndependentUpdate_4(),
	TweenParams_t2944325381::get_offset_of_onStart_5(),
	TweenParams_t2944325381::get_offset_of_onPlay_6(),
	TweenParams_t2944325381::get_offset_of_onRewind_7(),
	TweenParams_t2944325381::get_offset_of_onUpdate_8(),
	TweenParams_t2944325381::get_offset_of_onStepComplete_9(),
	TweenParams_t2944325381::get_offset_of_onComplete_10(),
	TweenParams_t2944325381::get_offset_of_onKill_11(),
	TweenParams_t2944325381::get_offset_of_onWaypointChange_12(),
	TweenParams_t2944325381::get_offset_of_isRecyclable_13(),
	TweenParams_t2944325381::get_offset_of_isSpeedBased_14(),
	TweenParams_t2944325381::get_offset_of_autoKill_15(),
	TweenParams_t2944325381::get_offset_of_loops_16(),
	TweenParams_t2944325381::get_offset_of_loopType_17(),
	TweenParams_t2944325381::get_offset_of_delay_18(),
	TweenParams_t2944325381::get_offset_of_isRelative_19(),
	TweenParams_t2944325381::get_offset_of_easeType_20(),
	TweenParams_t2944325381::get_offset_of_customEase_21(),
	TweenParams_t2944325381::get_offset_of_easeOvershootOrAmplitude_22(),
	TweenParams_t2944325381::get_offset_of_easePeriod_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (TweenSettingsExtensions_t2285462830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (LogBehaviour_t3505725029)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1747[4] = 
{
	LogBehaviour_t3505725029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (Tween_t278478013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[47] = 
{
	Tween_t278478013::get_offset_of_timeScale_4(),
	Tween_t278478013::get_offset_of_isBackwards_5(),
	Tween_t278478013::get_offset_of_id_6(),
	Tween_t278478013::get_offset_of_target_7(),
	Tween_t278478013::get_offset_of_updateType_8(),
	Tween_t278478013::get_offset_of_isIndependentUpdate_9(),
	Tween_t278478013::get_offset_of_onPlay_10(),
	Tween_t278478013::get_offset_of_onPause_11(),
	Tween_t278478013::get_offset_of_onRewind_12(),
	Tween_t278478013::get_offset_of_onUpdate_13(),
	Tween_t278478013::get_offset_of_onStepComplete_14(),
	Tween_t278478013::get_offset_of_onComplete_15(),
	Tween_t278478013::get_offset_of_onKill_16(),
	Tween_t278478013::get_offset_of_onWaypointChange_17(),
	Tween_t278478013::get_offset_of_isFrom_18(),
	Tween_t278478013::get_offset_of_isBlendable_19(),
	Tween_t278478013::get_offset_of_isRecyclable_20(),
	Tween_t278478013::get_offset_of_isSpeedBased_21(),
	Tween_t278478013::get_offset_of_autoKill_22(),
	Tween_t278478013::get_offset_of_duration_23(),
	Tween_t278478013::get_offset_of_loops_24(),
	Tween_t278478013::get_offset_of_loopType_25(),
	Tween_t278478013::get_offset_of_delay_26(),
	Tween_t278478013::get_offset_of_isRelative_27(),
	Tween_t278478013::get_offset_of_easeType_28(),
	Tween_t278478013::get_offset_of_customEase_29(),
	Tween_t278478013::get_offset_of_easeOvershootOrAmplitude_30(),
	Tween_t278478013::get_offset_of_easePeriod_31(),
	Tween_t278478013::get_offset_of_typeofT1_32(),
	Tween_t278478013::get_offset_of_typeofT2_33(),
	Tween_t278478013::get_offset_of_typeofTPlugOptions_34(),
	Tween_t278478013::get_offset_of_active_35(),
	Tween_t278478013::get_offset_of_isSequenced_36(),
	Tween_t278478013::get_offset_of_sequenceParent_37(),
	Tween_t278478013::get_offset_of_activeId_38(),
	Tween_t278478013::get_offset_of_specialStartupMode_39(),
	Tween_t278478013::get_offset_of_creationLocked_40(),
	Tween_t278478013::get_offset_of_startupDone_41(),
	Tween_t278478013::get_offset_of_playedOnce_42(),
	Tween_t278478013::get_offset_of_position_43(),
	Tween_t278478013::get_offset_of_fullDuration_44(),
	Tween_t278478013::get_offset_of_completedLoops_45(),
	Tween_t278478013::get_offset_of_isPlaying_46(),
	Tween_t278478013::get_offset_of_isComplete_47(),
	Tween_t278478013::get_offset_of_elapsedDelay_48(),
	Tween_t278478013::get_offset_of_delayComplete_49(),
	Tween_t278478013::get_offset_of_miscInt_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (Tweener_t760404022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[2] = 
{
	Tweener_t760404022::get_offset_of_hasManuallySetStartValue_51(),
	Tweener_t760404022::get_offset_of_isFromAllowed_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (TweenType_t169444141)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1750[4] = 
{
	TweenType_t169444141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (UpdateType_t3357224513)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1751[4] = 
{
	UpdateType_t3357224513::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (Color2Plugin_t3433430606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (DoublePlugin_t266400784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (LongPlugin_t1941283029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (UlongPlugin_t3231465400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (Vector3ArrayPlugin_t2378569512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (PathPlugin_t4171842066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (ColorPlugin_t4063724482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (IntPlugin_t180838436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (QuaternionPlugin_t1696644323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (RectOffsetPlugin_t664509336), -1, sizeof(RectOffsetPlugin_t664509336_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1761[1] = 
{
	RectOffsetPlugin_t664509336_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (RectPlugin_t391797831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (UintPlugin_t1040977389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (Vector2Plugin_t2164285386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (Vector4Plugin_t2164361360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (StringPlugin_t3620786088), -1, sizeof(StringPlugin_t3620786088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1766[2] = 
{
	StringPlugin_t3620786088_StaticFields::get_offset_of__Buffer_0(),
	StringPlugin_t3620786088_StaticFields::get_offset_of__OpenedTags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (StringPluginExtensions_t3910942986), -1, sizeof(StringPluginExtensions_t3910942986_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1767[5] = 
{
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsAll_0(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsUppercase_1(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsLowercase_2(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsNumerals_3(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of__lastRndSeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (FloatPlugin_t3639480371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (Vector3Plugin_t2164530409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (OrientType_t1755667719)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1771[5] = 
{
	OrientType_t1755667719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (PathOptions_t2659884781)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[15] = 
{
	PathOptions_t2659884781::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_orientType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_lockPositionAxis_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_lockRotationAxis_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_isClosedPath_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_lookAtPosition_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_lookAtTransform_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_lookAhead_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_hasCustomForwardDirection_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_forward_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_useLocalPosition_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_parent_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_isRigidbody_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_startupRot_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PathOptions_t2659884781::get_offset_of_startupZRot_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (QuaternionOptions_t466049668)+ sizeof (RuntimeObject), sizeof(QuaternionOptions_t466049668 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1773[3] = 
{
	QuaternionOptions_t466049668::get_offset_of_rotateMode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuaternionOptions_t466049668::get_offset_of_axisConstraint_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	QuaternionOptions_t466049668::get_offset_of_up_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (UintOptions_t2267095136)+ sizeof (RuntimeObject), sizeof(UintOptions_t2267095136_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1774[1] = 
{
	UintOptions_t2267095136::get_offset_of_isNegativeChangeValue_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (Vector3ArrayOptions_t2672570171)+ sizeof (RuntimeObject), sizeof(Vector3ArrayOptions_t2672570171_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1775[3] = 
{
	Vector3ArrayOptions_t2672570171::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_durations_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (NoOptions_t2508431845)+ sizeof (RuntimeObject), sizeof(NoOptions_t2508431845 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (ColorOptions_t2213017305)+ sizeof (RuntimeObject), sizeof(ColorOptions_t2213017305_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1777[1] = 
{
	ColorOptions_t2213017305::get_offset_of_alphaOnly_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (FloatOptions_t1421548266)+ sizeof (RuntimeObject), sizeof(FloatOptions_t1421548266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1778[1] = 
{
	FloatOptions_t1421548266::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (RectOptions_t3393635162)+ sizeof (RuntimeObject), sizeof(RectOptions_t3393635162_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1779[1] = 
{
	RectOptions_t3393635162::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (StringOptions_t2885323933)+ sizeof (RuntimeObject), sizeof(StringOptions_t2885323933_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1780[5] = 
{
	StringOptions_t2885323933::get_offset_of_richTextEnabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t2885323933::get_offset_of_scrambleMode_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t2885323933::get_offset_of_scrambledChars_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t2885323933::get_offset_of_startValueStrippedLength_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StringOptions_t2885323933::get_offset_of_changeValueStrippedLength_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (VectorOptions_t293385261)+ sizeof (RuntimeObject), sizeof(VectorOptions_t293385261_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1781[2] = 
{
	VectorOptions_t293385261::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VectorOptions_t293385261::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (SpecialPluginsUtils_t2241999250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (PluginsManager_t3052451537), -1, sizeof(PluginsManager_t3052451537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1787[18] = 
{
	PluginsManager_t3052451537_StaticFields::get_offset_of__floatPlugin_0(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__doublePlugin_1(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__intPlugin_2(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__uintPlugin_3(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__longPlugin_4(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__ulongPlugin_5(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector2Plugin_6(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector3Plugin_7(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector4Plugin_8(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__quaternionPlugin_9(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__colorPlugin_10(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__rectPlugin_11(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__rectOffsetPlugin_12(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__stringPlugin_13(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector3ArrayPlugin_14(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__color2Plugin_15(),
	0,
	PluginsManager_t3052451537_StaticFields::get_offset_of__customPlugins_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (ControlPoint_t168081159)+ sizeof (RuntimeObject), sizeof(ControlPoint_t168081159 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1788[2] = 
{
	ControlPoint_t168081159::get_offset_of_a_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ControlPoint_t168081159::get_offset_of_b_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (ABSPathDecoder_t3294469411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (CatmullRomDecoder_t3014762178), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (LinearDecoder_t2073524639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (Path_t2828565993), -1, sizeof(Path_t2828565993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1792[21] = 
{
	Path_t2828565993_StaticFields::get_offset_of__catmullRomDecoder_0(),
	Path_t2828565993_StaticFields::get_offset_of__linearDecoder_1(),
	Path_t2828565993::get_offset_of_wpLengths_2(),
	Path_t2828565993::get_offset_of_type_3(),
	Path_t2828565993::get_offset_of_subdivisionsXSegment_4(),
	Path_t2828565993::get_offset_of_subdivisions_5(),
	Path_t2828565993::get_offset_of_wps_6(),
	Path_t2828565993::get_offset_of_controlPoints_7(),
	Path_t2828565993::get_offset_of_length_8(),
	Path_t2828565993::get_offset_of_isFinalized_9(),
	Path_t2828565993::get_offset_of_timesTable_10(),
	Path_t2828565993::get_offset_of_lengthsTable_11(),
	Path_t2828565993::get_offset_of_linearWPIndex_12(),
	Path_t2828565993::get_offset_of__incrementalClone_13(),
	Path_t2828565993::get_offset_of__incrementalIndex_14(),
	Path_t2828565993::get_offset_of__decoder_15(),
	Path_t2828565993::get_offset_of__changed_16(),
	Path_t2828565993::get_offset_of_nonLinearDrawWps_17(),
	Path_t2828565993::get_offset_of_targetPosition_18(),
	Path_t2828565993::get_offset_of_lookAtPosition_19(),
	Path_t2828565993::get_offset_of_gizmoColor_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (PureQuaternionPlugin_t3400666973), -1, sizeof(PureQuaternionPlugin_t3400666973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1793[1] = 
{
	PureQuaternionPlugin_t3400666973_StaticFields::get_offset_of__plug_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (ABSSequentiable_t2284140720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[4] = 
{
	ABSSequentiable_t2284140720::get_offset_of_tweenType_0(),
	ABSSequentiable_t2284140720::get_offset_of_sequencedPosition_1(),
	ABSSequentiable_t2284140720::get_offset_of_sequencedEndPosition_2(),
	ABSSequentiable_t2284140720::get_offset_of_onStart_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (Debugger_t1404542751), -1, sizeof(Debugger_t1404542751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1797[1] = 
{
	Debugger_t1404542751_StaticFields::get_offset_of_logPriority_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (DOTweenComponent_t696744215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[4] = 
{
	DOTweenComponent_t696744215::get_offset_of_inspectorUpdater_2(),
	DOTweenComponent_t696744215::get_offset_of__unscaledTime_3(),
	DOTweenComponent_t696744215::get_offset_of__unscaledDeltaTime_4(),
	DOTweenComponent_t696744215::get_offset_of__duplicateToDestroy_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (U3CWaitForCompletionU3Ed__13_t639731943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[3] = 
{
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForCompletionU3Ed__13_t639731943::get_offset_of_t_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
