﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// ViewPort3D
struct ViewPort3D_t2082711749;
// UnityEngine.UI.RawImage
struct RawImage_t2749640213;
// UnityEngine.Camera
struct Camera_t189460977;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Texture
struct Texture_t2243626319;
// UnityEngine.RenderTexture
struct RenderTexture_t2666733923;
// UnityEngine.Transform
struct Transform_t3275118058;
// System.String
struct String_t;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1268241104;
// UnityEngine.Renderer
struct Renderer_t257310565;
// VoxelPallette
struct VoxelPallette_t4284593999;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct List_1_t2501136733;
// VoxelTemplate
struct VoxelTemplate_t594004422;
// System.Func`3<VoxelTemplate,System.Int32,System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>
struct Func_3_t4100446949;
// System.Func`3<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Func_3_t3811912889;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>
struct IEnumerable_1_t3259822848;
// System.Collections.Generic.IEnumerable`1<VoxelTemplate>
struct IEnumerable_1_t886131467;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerable_1_t4008377139;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>,System.Int32>
struct Func_2_t1901072402;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>,System.Int32>
struct Func_2_t863984051;
// System.Linq.IOrderedEnumerable`1<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>
struct IOrderedEnumerable_1_t2258915583;
// System.Linq.IOrderedEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IOrderedEnumerable_1_t3007469874;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>
struct List_1_t2336816935;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct List_1_t3085371226;
// VoxelSpace
struct VoxelSpace_t930815766;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// VoxelStructure
struct VoxelStructure_t2247263099;
// System.Single[0...,0...]
struct SingleU5B0___U2C0___U5D_t577127398;
// System.Globalization.CultureInfo
struct CultureInfo_t3500843524;
// System.IFormatProvider
struct IFormatProvider_t2849799027;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// UnityEngine.Collider
struct Collider_t3497673348;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t2535834625;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// UnityEngine.Material
struct Material_t193706927;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t2364004493;
// System.Int32[]
struct Int32U5BU5D_t3030399641;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector2>
struct IEnumerable_1_t2535834624;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Color32>
struct IEnumerable_1_t1166644563;
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470;
// WeightedOptions
struct WeightedOptions_t481347131;
// PairStringIntDictionary
struct PairStringIntDictionary_t1323851614;
// System.Collections.Generic.List`1<PairStringInt>
struct List_1_t3590122670;
// СhaseMover
struct U421haseMover_t4119473191;
// System.Action`2<UnityEngine.GameObject,UnityEngine.Transform>
struct Action_2_t2601038636;
// System.Delegate
struct Delegate_t3022476291;
// System.Action`2<System.Object,System.Object>
struct Action_2_t2572051853;
// System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3307774202;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t104580544;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t2187473504;
// System.Globalization.TextInfo
struct TextInfo_t3620182823;
// System.Globalization.CompareInfo
struct CompareInfo_t2310920157;
// System.Globalization.Calendar[]
struct CalendarU5BU5D_t3654442685;
// System.Globalization.Calendar
struct Calendar_t585061108;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Hashtable
struct Hashtable_t909839986;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;
// System.Int32
struct Int32_t2071877448;
// System.Void
struct Void_t1841601450;
// PairStringInt[]
struct PairStringIntU5BU5D_t1127891319;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t14295340;
// System.String[]
struct StringU5BU5D_t1642385972;
// VoxelTemplate[]
struct VoxelTemplateU5BU5D_t2866079843;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.Func`2<PairStringInt,System.Int32>
struct Func_2_t4016156815;
// System.Collections.Generic.Dictionary`2<DirectionTypes,Vector3i>
struct Dictionary_2_t2807614054;
// System.Byte
struct Byte_t3683104436;
// System.Double
struct Double_t4078015681;
// System.UInt16
struct UInt16_t986882611;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1572802995;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t834278767;
// System.Int32[0...,0...,0...]
struct Int32U5B0___U2C0___U2C0___U5D_t3030399643;
// VoxelStructure[0...,0...,0...]
struct VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t261436805;
// UnityEngine.Canvas
struct Canvas_t209405766;
// UnityEngine.Events.UnityAction
struct UnityAction_t4025899511;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t385374196;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t3177091249;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t1156185964;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3778758259;

extern RuntimeClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern RuntimeClass* Graphics_t2412809155_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisCamera_t189460977_m3276577584_RuntimeMethod_var;
extern const uint32_t ViewPort3D_Update_m858759021_MetadataUsageId;
extern RuntimeClass* Quaternion_t4030073918_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3495770132;
extern const uint32_t ViewPort3D_AddViewObject_m506564281_MetadataUsageId;
extern const uint32_t ViewPort3D_Destroy_m3568382482_MetadataUsageId;
extern RuntimeClass* VoxelTemplateU5BU5D_t2866079843_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1398341365_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2501136733_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m3854603248_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3048104478_RuntimeMethod_var;
extern const uint32_t VoxelPallette__ctor_m431966492_MetadataUsageId;
extern RuntimeClass* VoxelPallette_t4284593999_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_3_t4100446949_il2cpp_TypeInfo_var;
extern RuntimeClass* Func_2_t1901072402_il2cpp_TypeInfo_var;
extern const RuntimeMethod* VoxelPallette_U3CArrangeVoxelTemplatesU3Em__0_m1642453984_RuntimeMethod_var;
extern const RuntimeMethod* Func_3__ctor_m2369188720_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Select_TisVoxelTemplate_t594004422_TisKeyValuePair_2_t2967695803_m971871174_RuntimeMethod_var;
extern const RuntimeMethod* VoxelPallette_U3CArrangeVoxelTemplatesU3Em__1_m925405037_RuntimeMethod_var;
extern const RuntimeMethod* Func_2__ctor_m3619279935_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_OrderBy_TisKeyValuePair_2_t2967695803_TisInt32_t2071877448_m3022342042_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToList_TisKeyValuePair_2_t2967695803_m3352129643_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m1068147322_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Key_m1519167095_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2_get_Value_m3509700530_RuntimeMethod_var;
extern const RuntimeMethod* KeyValuePair_2__ctor_m629383883_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3599575194_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m4061286785_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m1521282969_RuntimeMethod_var;
extern const uint32_t VoxelPallette_ArrangeVoxelTemplates_m3016297437_MetadataUsageId;
extern const RuntimeMethod* KeyValuePair_2__ctor_m3535713998_RuntimeMethod_var;
extern const uint32_t VoxelPallette_U3CArrangeVoxelTemplatesU3Em__0_m1642453984_MetadataUsageId;
extern const uint32_t VoxelPallette_U3CArrangeVoxelTemplatesU3Em__1_m925405037_MetadataUsageId;
extern RuntimeClass* VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636_il2cpp_TypeInfo_var;
extern RuntimeClass* VoxelStructure_t2247263099_il2cpp_TypeInfo_var;
extern const uint32_t VoxelSpace_Create_m1052405448_MetadataUsageId;
extern const uint32_t VoxelSpace_SetVoxel_m624077049_MetadataUsageId;
extern const uint32_t VoxelSpace_GetVoxel_m2941864988_MetadataUsageId;
extern RuntimeClass* SingleU5B0___U2C0___U5D_t577127398_il2cpp_TypeInfo_var;
extern const uint32_t VoxelSpace_GetHeightMap_m3406387659_MetadataUsageId;
extern RuntimeClass* CultureInfo_t3500843524_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32U5B0___U2C0___U2C0___U5D_t3030399643_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_AddComponent_TisVoxelStructure_t2247263099_m1788057469_RuntimeMethod_var;
extern const RuntimeMethod* GameObject_GetComponent_TisMeshCollider_t2718867283_m4281307913_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3110053185;
extern const uint32_t VoxelStructure_Create_m3231949789_MetadataUsageId;
extern RuntimeClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var;
extern RuntimeClass* Color32U5BU5D_t30278651_il2cpp_TypeInfo_var;
extern RuntimeClass* Mesh_t1356156583_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Component_GetComponent_TisMeshFilter_t3026937449_m1170669043_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Take_TisVector3_t2243707580_m1945563610_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisVector3_t2243707580_m1930596601_RuntimeMethod_var;
extern const RuntimeMethod* Component_GetComponent_TisRenderer_t257310565_m772028041_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Take_TisInt32_t2071877448_m1592882544_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisInt32_t2071877448_m513246933_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Take_TisVector2_t2243707579_m1080133109_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisVector2_t2243707579_m1715071610_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Take_TisColor32_t874517518_m543186848_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisColor32_t874517518_m1885434799_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral3445844385;
extern Il2CppCodeGenString* _stringLiteral2176050822;
extern Il2CppCodeGenString* _stringLiteral3332643267;
extern const uint32_t VoxelStructure_Draw_m1865268436_MetadataUsageId;
extern RuntimeClass* Vector3i_t3488636705_il2cpp_TypeInfo_var;
extern const uint32_t VoxelStructure_MaxExtrude_m1439246306_MetadataUsageId;
extern const uint32_t VoxelStructure_GetVoxelUvs_m4193062824_MetadataUsageId;
extern RuntimeClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern const uint32_t VoxelStructure_ClampDimensions_m3734676430_MetadataUsageId;
extern const uint32_t VoxelStructure__cctor_m683238421_MetadataUsageId;
extern const RuntimeMethod* List_1_GetEnumerator_m1313580170_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_get_Current_m784304568_RuntimeMethod_var;
extern const RuntimeMethod* Enumerator_MoveNext_m775896506_RuntimeMethod_var;
extern const uint32_t WeightedOptions_Choose_m1410328636_MetadataUsageId;
extern RuntimeClass* Action_2_t2601038636_il2cpp_TypeInfo_var;
extern const uint32_t U421haseMover_add_OnCatch_m615591784_MetadataUsageId;
extern const uint32_t U421haseMover_remove_OnCatch_m2286981733_MetadataUsageId;
extern const RuntimeMethod* Action_2_Invoke_m2336330442_RuntimeMethod_var;
extern const uint32_t U421haseMover_Catch_m3551204615_MetadataUsageId;
extern const uint32_t U421haseMover_Update_m982869159_MetadataUsageId;
extern const RuntimeMethod* GameObject_AddComponent_TisU421haseMover_t4119473191_m1934158225_RuntimeMethod_var;
extern const uint32_t U421haseMover_Create_m3555304436_MetadataUsageId;

struct VoxelTemplateU5BU5D_t2866079843;
struct VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636;
struct SingleU5B0___U2C0___U5D_t577127398;
struct Int32U5B0___U2C0___U2C0___U5D_t3030399643;
struct Vector2U5BU5D_t686124026;
struct Vector3U5BU5D_t1172311765;
struct Int32U5BU5D_t3030399641;
struct Color32U5BU5D_t30278651;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T3507792607_H
#define VALUETYPE_T3507792607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3507792607  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3507792607_marshaled_com
{
};
#endif // VALUETYPE_T3507792607_H
#ifndef LIST_1_T2336816935_H
#define LIST_1_T2336816935_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>
struct  List_1_t2336816935  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t3307774202* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2336816935, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t3307774202* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t3307774202** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t3307774202* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2336816935, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2336816935, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2336816935_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	KeyValuePair_2U5BU5D_t3307774202* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2336816935_StaticFields, ___EmptyArray_4)); }
	inline KeyValuePair_2U5BU5D_t3307774202* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline KeyValuePair_2U5BU5D_t3307774202** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(KeyValuePair_2U5BU5D_t3307774202* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2336816935_H
#ifndef CULTUREINFO_T3500843524_H
#define CULTUREINFO_T3500843524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Globalization.CultureInfo
struct  CultureInfo_t3500843524  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_7;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_8;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_9;
	// System.Int32 System.Globalization.CultureInfo::specific_lcid
	int32_t ___specific_lcid_10;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_11;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_12;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_13;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t104580544 * ___numInfo_14;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t2187473504 * ___dateTimeInfo_15;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_t3620182823 * ___textInfo_16;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_17;
	// System.String System.Globalization.CultureInfo::displayname
	String_t* ___displayname_18;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_19;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_20;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_21;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_22;
	// System.String System.Globalization.CultureInfo::icu_name
	String_t* ___icu_name_23;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_24;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_25;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t2310920157 * ___compareInfo_26;
	// System.Int32* System.Globalization.CultureInfo::calendar_data
	int32_t* ___calendar_data_27;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_28;
	// System.Globalization.Calendar[] System.Globalization.CultureInfo::optional_calendars
	CalendarU5BU5D_t3654442685* ___optional_calendars_29;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t3500843524 * ___parent_culture_30;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_31;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t585061108 * ___calendar_32;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_33;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_t3397334013* ___cached_serialized_form_34;

public:
	inline static int32_t get_offset_of_m_isReadOnly_7() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___m_isReadOnly_7)); }
	inline bool get_m_isReadOnly_7() const { return ___m_isReadOnly_7; }
	inline bool* get_address_of_m_isReadOnly_7() { return &___m_isReadOnly_7; }
	inline void set_m_isReadOnly_7(bool value)
	{
		___m_isReadOnly_7 = value;
	}

	inline static int32_t get_offset_of_cultureID_8() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___cultureID_8)); }
	inline int32_t get_cultureID_8() const { return ___cultureID_8; }
	inline int32_t* get_address_of_cultureID_8() { return &___cultureID_8; }
	inline void set_cultureID_8(int32_t value)
	{
		___cultureID_8 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_9() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___parent_lcid_9)); }
	inline int32_t get_parent_lcid_9() const { return ___parent_lcid_9; }
	inline int32_t* get_address_of_parent_lcid_9() { return &___parent_lcid_9; }
	inline void set_parent_lcid_9(int32_t value)
	{
		___parent_lcid_9 = value;
	}

	inline static int32_t get_offset_of_specific_lcid_10() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___specific_lcid_10)); }
	inline int32_t get_specific_lcid_10() const { return ___specific_lcid_10; }
	inline int32_t* get_address_of_specific_lcid_10() { return &___specific_lcid_10; }
	inline void set_specific_lcid_10(int32_t value)
	{
		___specific_lcid_10 = value;
	}

	inline static int32_t get_offset_of_datetime_index_11() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___datetime_index_11)); }
	inline int32_t get_datetime_index_11() const { return ___datetime_index_11; }
	inline int32_t* get_address_of_datetime_index_11() { return &___datetime_index_11; }
	inline void set_datetime_index_11(int32_t value)
	{
		___datetime_index_11 = value;
	}

	inline static int32_t get_offset_of_number_index_12() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___number_index_12)); }
	inline int32_t get_number_index_12() const { return ___number_index_12; }
	inline int32_t* get_address_of_number_index_12() { return &___number_index_12; }
	inline void set_number_index_12(int32_t value)
	{
		___number_index_12 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_13() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___m_useUserOverride_13)); }
	inline bool get_m_useUserOverride_13() const { return ___m_useUserOverride_13; }
	inline bool* get_address_of_m_useUserOverride_13() { return &___m_useUserOverride_13; }
	inline void set_m_useUserOverride_13(bool value)
	{
		___m_useUserOverride_13 = value;
	}

	inline static int32_t get_offset_of_numInfo_14() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___numInfo_14)); }
	inline NumberFormatInfo_t104580544 * get_numInfo_14() const { return ___numInfo_14; }
	inline NumberFormatInfo_t104580544 ** get_address_of_numInfo_14() { return &___numInfo_14; }
	inline void set_numInfo_14(NumberFormatInfo_t104580544 * value)
	{
		___numInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&___numInfo_14), value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_15() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___dateTimeInfo_15)); }
	inline DateTimeFormatInfo_t2187473504 * get_dateTimeInfo_15() const { return ___dateTimeInfo_15; }
	inline DateTimeFormatInfo_t2187473504 ** get_address_of_dateTimeInfo_15() { return &___dateTimeInfo_15; }
	inline void set_dateTimeInfo_15(DateTimeFormatInfo_t2187473504 * value)
	{
		___dateTimeInfo_15 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeInfo_15), value);
	}

	inline static int32_t get_offset_of_textInfo_16() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___textInfo_16)); }
	inline TextInfo_t3620182823 * get_textInfo_16() const { return ___textInfo_16; }
	inline TextInfo_t3620182823 ** get_address_of_textInfo_16() { return &___textInfo_16; }
	inline void set_textInfo_16(TextInfo_t3620182823 * value)
	{
		___textInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___textInfo_16), value);
	}

	inline static int32_t get_offset_of_m_name_17() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___m_name_17)); }
	inline String_t* get_m_name_17() const { return ___m_name_17; }
	inline String_t** get_address_of_m_name_17() { return &___m_name_17; }
	inline void set_m_name_17(String_t* value)
	{
		___m_name_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_name_17), value);
	}

	inline static int32_t get_offset_of_displayname_18() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___displayname_18)); }
	inline String_t* get_displayname_18() const { return ___displayname_18; }
	inline String_t** get_address_of_displayname_18() { return &___displayname_18; }
	inline void set_displayname_18(String_t* value)
	{
		___displayname_18 = value;
		Il2CppCodeGenWriteBarrier((&___displayname_18), value);
	}

	inline static int32_t get_offset_of_englishname_19() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___englishname_19)); }
	inline String_t* get_englishname_19() const { return ___englishname_19; }
	inline String_t** get_address_of_englishname_19() { return &___englishname_19; }
	inline void set_englishname_19(String_t* value)
	{
		___englishname_19 = value;
		Il2CppCodeGenWriteBarrier((&___englishname_19), value);
	}

	inline static int32_t get_offset_of_nativename_20() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___nativename_20)); }
	inline String_t* get_nativename_20() const { return ___nativename_20; }
	inline String_t** get_address_of_nativename_20() { return &___nativename_20; }
	inline void set_nativename_20(String_t* value)
	{
		___nativename_20 = value;
		Il2CppCodeGenWriteBarrier((&___nativename_20), value);
	}

	inline static int32_t get_offset_of_iso3lang_21() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___iso3lang_21)); }
	inline String_t* get_iso3lang_21() const { return ___iso3lang_21; }
	inline String_t** get_address_of_iso3lang_21() { return &___iso3lang_21; }
	inline void set_iso3lang_21(String_t* value)
	{
		___iso3lang_21 = value;
		Il2CppCodeGenWriteBarrier((&___iso3lang_21), value);
	}

	inline static int32_t get_offset_of_iso2lang_22() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___iso2lang_22)); }
	inline String_t* get_iso2lang_22() const { return ___iso2lang_22; }
	inline String_t** get_address_of_iso2lang_22() { return &___iso2lang_22; }
	inline void set_iso2lang_22(String_t* value)
	{
		___iso2lang_22 = value;
		Il2CppCodeGenWriteBarrier((&___iso2lang_22), value);
	}

	inline static int32_t get_offset_of_icu_name_23() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___icu_name_23)); }
	inline String_t* get_icu_name_23() const { return ___icu_name_23; }
	inline String_t** get_address_of_icu_name_23() { return &___icu_name_23; }
	inline void set_icu_name_23(String_t* value)
	{
		___icu_name_23 = value;
		Il2CppCodeGenWriteBarrier((&___icu_name_23), value);
	}

	inline static int32_t get_offset_of_win3lang_24() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___win3lang_24)); }
	inline String_t* get_win3lang_24() const { return ___win3lang_24; }
	inline String_t** get_address_of_win3lang_24() { return &___win3lang_24; }
	inline void set_win3lang_24(String_t* value)
	{
		___win3lang_24 = value;
		Il2CppCodeGenWriteBarrier((&___win3lang_24), value);
	}

	inline static int32_t get_offset_of_territory_25() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___territory_25)); }
	inline String_t* get_territory_25() const { return ___territory_25; }
	inline String_t** get_address_of_territory_25() { return &___territory_25; }
	inline void set_territory_25(String_t* value)
	{
		___territory_25 = value;
		Il2CppCodeGenWriteBarrier((&___territory_25), value);
	}

	inline static int32_t get_offset_of_compareInfo_26() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___compareInfo_26)); }
	inline CompareInfo_t2310920157 * get_compareInfo_26() const { return ___compareInfo_26; }
	inline CompareInfo_t2310920157 ** get_address_of_compareInfo_26() { return &___compareInfo_26; }
	inline void set_compareInfo_26(CompareInfo_t2310920157 * value)
	{
		___compareInfo_26 = value;
		Il2CppCodeGenWriteBarrier((&___compareInfo_26), value);
	}

	inline static int32_t get_offset_of_calendar_data_27() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___calendar_data_27)); }
	inline int32_t* get_calendar_data_27() const { return ___calendar_data_27; }
	inline int32_t** get_address_of_calendar_data_27() { return &___calendar_data_27; }
	inline void set_calendar_data_27(int32_t* value)
	{
		___calendar_data_27 = value;
	}

	inline static int32_t get_offset_of_textinfo_data_28() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___textinfo_data_28)); }
	inline void* get_textinfo_data_28() const { return ___textinfo_data_28; }
	inline void** get_address_of_textinfo_data_28() { return &___textinfo_data_28; }
	inline void set_textinfo_data_28(void* value)
	{
		___textinfo_data_28 = value;
	}

	inline static int32_t get_offset_of_optional_calendars_29() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___optional_calendars_29)); }
	inline CalendarU5BU5D_t3654442685* get_optional_calendars_29() const { return ___optional_calendars_29; }
	inline CalendarU5BU5D_t3654442685** get_address_of_optional_calendars_29() { return &___optional_calendars_29; }
	inline void set_optional_calendars_29(CalendarU5BU5D_t3654442685* value)
	{
		___optional_calendars_29 = value;
		Il2CppCodeGenWriteBarrier((&___optional_calendars_29), value);
	}

	inline static int32_t get_offset_of_parent_culture_30() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___parent_culture_30)); }
	inline CultureInfo_t3500843524 * get_parent_culture_30() const { return ___parent_culture_30; }
	inline CultureInfo_t3500843524 ** get_address_of_parent_culture_30() { return &___parent_culture_30; }
	inline void set_parent_culture_30(CultureInfo_t3500843524 * value)
	{
		___parent_culture_30 = value;
		Il2CppCodeGenWriteBarrier((&___parent_culture_30), value);
	}

	inline static int32_t get_offset_of_m_dataItem_31() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___m_dataItem_31)); }
	inline int32_t get_m_dataItem_31() const { return ___m_dataItem_31; }
	inline int32_t* get_address_of_m_dataItem_31() { return &___m_dataItem_31; }
	inline void set_m_dataItem_31(int32_t value)
	{
		___m_dataItem_31 = value;
	}

	inline static int32_t get_offset_of_calendar_32() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___calendar_32)); }
	inline Calendar_t585061108 * get_calendar_32() const { return ___calendar_32; }
	inline Calendar_t585061108 ** get_address_of_calendar_32() { return &___calendar_32; }
	inline void set_calendar_32(Calendar_t585061108 * value)
	{
		___calendar_32 = value;
		Il2CppCodeGenWriteBarrier((&___calendar_32), value);
	}

	inline static int32_t get_offset_of_constructed_33() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___constructed_33)); }
	inline bool get_constructed_33() const { return ___constructed_33; }
	inline bool* get_address_of_constructed_33() { return &___constructed_33; }
	inline void set_constructed_33(bool value)
	{
		___constructed_33 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_34() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524, ___cached_serialized_form_34)); }
	inline ByteU5BU5D_t3397334013* get_cached_serialized_form_34() const { return ___cached_serialized_form_34; }
	inline ByteU5BU5D_t3397334013** get_address_of_cached_serialized_form_34() { return &___cached_serialized_form_34; }
	inline void set_cached_serialized_form_34(ByteU5BU5D_t3397334013* value)
	{
		___cached_serialized_form_34 = value;
		Il2CppCodeGenWriteBarrier((&___cached_serialized_form_34), value);
	}
};

struct CultureInfo_t3500843524_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t3500843524 * ___invariant_culture_info_4;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_5;
	// System.Int32 System.Globalization.CultureInfo::BootstrapCultureID
	int32_t ___BootstrapCultureID_6;
	// System.String System.Globalization.CultureInfo::MSG_READONLY
	String_t* ___MSG_READONLY_35;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_number
	Hashtable_t909839986 * ___shared_by_number_36;
	// System.Collections.Hashtable System.Globalization.CultureInfo::shared_by_name
	Hashtable_t909839986 * ___shared_by_name_37;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map19
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map19_38;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Globalization.CultureInfo::<>f__switch$map1A
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map1A_39;

public:
	inline static int32_t get_offset_of_invariant_culture_info_4() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___invariant_culture_info_4)); }
	inline CultureInfo_t3500843524 * get_invariant_culture_info_4() const { return ___invariant_culture_info_4; }
	inline CultureInfo_t3500843524 ** get_address_of_invariant_culture_info_4() { return &___invariant_culture_info_4; }
	inline void set_invariant_culture_info_4(CultureInfo_t3500843524 * value)
	{
		___invariant_culture_info_4 = value;
		Il2CppCodeGenWriteBarrier((&___invariant_culture_info_4), value);
	}

	inline static int32_t get_offset_of_shared_table_lock_5() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___shared_table_lock_5)); }
	inline RuntimeObject * get_shared_table_lock_5() const { return ___shared_table_lock_5; }
	inline RuntimeObject ** get_address_of_shared_table_lock_5() { return &___shared_table_lock_5; }
	inline void set_shared_table_lock_5(RuntimeObject * value)
	{
		___shared_table_lock_5 = value;
		Il2CppCodeGenWriteBarrier((&___shared_table_lock_5), value);
	}

	inline static int32_t get_offset_of_BootstrapCultureID_6() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___BootstrapCultureID_6)); }
	inline int32_t get_BootstrapCultureID_6() const { return ___BootstrapCultureID_6; }
	inline int32_t* get_address_of_BootstrapCultureID_6() { return &___BootstrapCultureID_6; }
	inline void set_BootstrapCultureID_6(int32_t value)
	{
		___BootstrapCultureID_6 = value;
	}

	inline static int32_t get_offset_of_MSG_READONLY_35() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___MSG_READONLY_35)); }
	inline String_t* get_MSG_READONLY_35() const { return ___MSG_READONLY_35; }
	inline String_t** get_address_of_MSG_READONLY_35() { return &___MSG_READONLY_35; }
	inline void set_MSG_READONLY_35(String_t* value)
	{
		___MSG_READONLY_35 = value;
		Il2CppCodeGenWriteBarrier((&___MSG_READONLY_35), value);
	}

	inline static int32_t get_offset_of_shared_by_number_36() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___shared_by_number_36)); }
	inline Hashtable_t909839986 * get_shared_by_number_36() const { return ___shared_by_number_36; }
	inline Hashtable_t909839986 ** get_address_of_shared_by_number_36() { return &___shared_by_number_36; }
	inline void set_shared_by_number_36(Hashtable_t909839986 * value)
	{
		___shared_by_number_36 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_number_36), value);
	}

	inline static int32_t get_offset_of_shared_by_name_37() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___shared_by_name_37)); }
	inline Hashtable_t909839986 * get_shared_by_name_37() const { return ___shared_by_name_37; }
	inline Hashtable_t909839986 ** get_address_of_shared_by_name_37() { return &___shared_by_name_37; }
	inline void set_shared_by_name_37(Hashtable_t909839986 * value)
	{
		___shared_by_name_37 = value;
		Il2CppCodeGenWriteBarrier((&___shared_by_name_37), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map19_38() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___U3CU3Ef__switchU24map19_38)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map19_38() const { return ___U3CU3Ef__switchU24map19_38; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map19_38() { return &___U3CU3Ef__switchU24map19_38; }
	inline void set_U3CU3Ef__switchU24map19_38(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map19_38 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map19_38), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1A_39() { return static_cast<int32_t>(offsetof(CultureInfo_t3500843524_StaticFields, ___U3CU3Ef__switchU24map1A_39)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map1A_39() const { return ___U3CU3Ef__switchU24map1A_39; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map1A_39() { return &___U3CU3Ef__switchU24map1A_39; }
	inline void set_U3CU3Ef__switchU24map1A_39(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map1A_39 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map1A_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CULTUREINFO_T3500843524_H
#ifndef LIST_1_T3590122670_H
#define LIST_1_T3590122670_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<PairStringInt>
struct  List_1_t3590122670  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	PairStringIntU5BU5D_t1127891319* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3590122670, ____items_1)); }
	inline PairStringIntU5BU5D_t1127891319* get__items_1() const { return ____items_1; }
	inline PairStringIntU5BU5D_t1127891319** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(PairStringIntU5BU5D_t1127891319* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3590122670, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3590122670, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t3590122670_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	PairStringIntU5BU5D_t1127891319* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t3590122670_StaticFields, ___EmptyArray_4)); }
	inline PairStringIntU5BU5D_t1127891319* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline PairStringIntU5BU5D_t1127891319** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(PairStringIntU5BU5D_t1127891319* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3590122670_H
#ifndef LIST_1_T2501136733_H
#define LIST_1_T2501136733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct  List_1_t2501136733  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t14295340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2501136733, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t14295340* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t14295340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t14295340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2501136733, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2501136733, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2501136733_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	KeyValuePair_2U5BU5D_t14295340* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2501136733_StaticFields, ___EmptyArray_4)); }
	inline KeyValuePair_2U5BU5D_t14295340* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline KeyValuePair_2U5BU5D_t14295340** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(KeyValuePair_2U5BU5D_t14295340* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2501136733_H
#ifndef LIST_1_T1398341365_H
#define LIST_1_T1398341365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t1398341365  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1642385972* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1398341365, ____items_1)); }
	inline StringU5BU5D_t1642385972* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1642385972** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1642385972* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1398341365, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1398341365, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1398341365_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	StringU5BU5D_t1642385972* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1398341365_StaticFields, ___EmptyArray_4)); }
	inline StringU5BU5D_t1642385972* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline StringU5BU5D_t1642385972** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(StringU5BU5D_t1642385972* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1398341365_H
#ifndef VOXELPALLETTE_T4284593999_H
#define VOXELPALLETTE_T4284593999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelPallette
struct  VoxelPallette_t4284593999  : public RuntimeObject
{
public:
	// UnityEngine.Material VoxelPallette::AtlasMaterial
	Material_t193706927 * ___AtlasMaterial_0;
	// VoxelTemplate[] VoxelPallette::voxelTemplates
	VoxelTemplateU5BU5D_t2866079843* ___voxelTemplates_1;
	// System.Collections.Generic.List`1<System.String> VoxelPallette::voxelNames
	List_1_t1398341365 * ___voxelNames_2;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>> VoxelPallette::_lookup
	List_1_t2501136733 * ____lookup_3;

public:
	inline static int32_t get_offset_of_AtlasMaterial_0() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999, ___AtlasMaterial_0)); }
	inline Material_t193706927 * get_AtlasMaterial_0() const { return ___AtlasMaterial_0; }
	inline Material_t193706927 ** get_address_of_AtlasMaterial_0() { return &___AtlasMaterial_0; }
	inline void set_AtlasMaterial_0(Material_t193706927 * value)
	{
		___AtlasMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___AtlasMaterial_0), value);
	}

	inline static int32_t get_offset_of_voxelTemplates_1() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999, ___voxelTemplates_1)); }
	inline VoxelTemplateU5BU5D_t2866079843* get_voxelTemplates_1() const { return ___voxelTemplates_1; }
	inline VoxelTemplateU5BU5D_t2866079843** get_address_of_voxelTemplates_1() { return &___voxelTemplates_1; }
	inline void set_voxelTemplates_1(VoxelTemplateU5BU5D_t2866079843* value)
	{
		___voxelTemplates_1 = value;
		Il2CppCodeGenWriteBarrier((&___voxelTemplates_1), value);
	}

	inline static int32_t get_offset_of_voxelNames_2() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999, ___voxelNames_2)); }
	inline List_1_t1398341365 * get_voxelNames_2() const { return ___voxelNames_2; }
	inline List_1_t1398341365 ** get_address_of_voxelNames_2() { return &___voxelNames_2; }
	inline void set_voxelNames_2(List_1_t1398341365 * value)
	{
		___voxelNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___voxelNames_2), value);
	}

	inline static int32_t get_offset_of__lookup_3() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999, ____lookup_3)); }
	inline List_1_t2501136733 * get__lookup_3() const { return ____lookup_3; }
	inline List_1_t2501136733 ** get_address_of__lookup_3() { return &____lookup_3; }
	inline void set__lookup_3(List_1_t2501136733 * value)
	{
		____lookup_3 = value;
		Il2CppCodeGenWriteBarrier((&____lookup_3), value);
	}
};

struct VoxelPallette_t4284593999_StaticFields
{
public:
	// System.Func`3<VoxelTemplate,System.Int32,System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>> VoxelPallette::<>f__am$cache0
	Func_3_t4100446949 * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>,System.Int32> VoxelPallette::<>f__am$cache1
	Func_2_t1901072402 * ___U3CU3Ef__amU24cache1_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_3_t4100446949 * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_3_t4100446949 ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_3_t4100446949 * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(VoxelPallette_t4284593999_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_t1901072402 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_t1901072402 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_t1901072402 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELPALLETTE_T4284593999_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t1328083999* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t1328083999* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t1328083999** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t1328083999* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef PAIRSTRINGINTDICTIONARY_T1323851614_H
#define PAIRSTRINGINTDICTIONARY_T1323851614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringIntDictionary
struct  PairStringIntDictionary_t1323851614  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<PairStringInt> PairStringIntDictionary::Pairs
	List_1_t3590122670 * ___Pairs_0;

public:
	inline static int32_t get_offset_of_Pairs_0() { return static_cast<int32_t>(offsetof(PairStringIntDictionary_t1323851614, ___Pairs_0)); }
	inline List_1_t3590122670 * get_Pairs_0() const { return ___Pairs_0; }
	inline List_1_t3590122670 ** get_address_of_Pairs_0() { return &___Pairs_0; }
	inline void set_Pairs_0(List_1_t3590122670 * value)
	{
		___Pairs_0 = value;
		Il2CppCodeGenWriteBarrier((&___Pairs_0), value);
	}
};

struct PairStringIntDictionary_t1323851614_StaticFields
{
public:
	// System.Func`2<PairStringInt,System.Int32> PairStringIntDictionary::<>f__am$cache0
	Func_2_t4016156815 * ___U3CU3Ef__amU24cache0_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_1() { return static_cast<int32_t>(offsetof(PairStringIntDictionary_t1323851614_StaticFields, ___U3CU3Ef__amU24cache0_1)); }
	inline Func_2_t4016156815 * get_U3CU3Ef__amU24cache0_1() const { return ___U3CU3Ef__amU24cache0_1; }
	inline Func_2_t4016156815 ** get_address_of_U3CU3Ef__amU24cache0_1() { return &___U3CU3Ef__amU24cache0_1; }
	inline void set_U3CU3Ef__amU24cache0_1(Func_2_t4016156815 * value)
	{
		___U3CU3Ef__amU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAIRSTRINGINTDICTIONARY_T1323851614_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef KEYVALUEPAIR_2_T2967695803_H
#define KEYVALUEPAIR_2_T2967695803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>
struct  KeyValuePair_2_t2967695803 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	VoxelTemplate_t594004422 * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2967695803, ___key_0)); }
	inline VoxelTemplate_t594004422 * get_key_0() const { return ___key_0; }
	inline VoxelTemplate_t594004422 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(VoxelTemplate_t594004422 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t2967695803, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T2967695803_H
#ifndef KEYVALUEPAIR_2_T3716250094_H
#define KEYVALUEPAIR_2_T3716250094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>
struct  KeyValuePair_2_t3716250094 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3716250094, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3716250094, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3716250094_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef PAIRSTRINGINT_T4221001538_H
#define PAIRSTRINGINT_T4221001538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PairStringInt
struct  PairStringInt_t4221001538 
{
public:
	// System.String PairStringInt::Key
	String_t* ___Key_0;
	// System.Int32 PairStringInt::Value
	int32_t ___Value_1;

public:
	inline static int32_t get_offset_of_Key_0() { return static_cast<int32_t>(offsetof(PairStringInt_t4221001538, ___Key_0)); }
	inline String_t* get_Key_0() const { return ___Key_0; }
	inline String_t** get_address_of_Key_0() { return &___Key_0; }
	inline void set_Key_0(String_t* value)
	{
		___Key_0 = value;
		Il2CppCodeGenWriteBarrier((&___Key_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(PairStringInt_t4221001538, ___Value_1)); }
	inline int32_t get_Value_1() const { return ___Value_1; }
	inline int32_t* get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(int32_t value)
	{
		___Value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of PairStringInt
struct PairStringInt_t4221001538_marshaled_pinvoke
{
	char* ___Key_0;
	int32_t ___Value_1;
};
// Native definition for COM marshalling of PairStringInt
struct PairStringInt_t4221001538_marshaled_com
{
	Il2CppChar* ___Key_0;
	int32_t ___Value_1;
};
#endif // PAIRSTRINGINT_T4221001538_H
#ifndef VECTOR2_T2243707579_H
#define VECTOR2_T2243707579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2243707579 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2243707579, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2243707579_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2243707579  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2243707579  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2243707579  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2243707579  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2243707579  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2243707579  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2243707579  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2243707579  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2243707579  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2243707579 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2243707579  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___oneVector_3)); }
	inline Vector2_t2243707579  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2243707579 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2243707579  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___upVector_4)); }
	inline Vector2_t2243707579  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2243707579 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2243707579  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___downVector_5)); }
	inline Vector2_t2243707579  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2243707579 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2243707579  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___leftVector_6)); }
	inline Vector2_t2243707579  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2243707579 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2243707579  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___rightVector_7)); }
	inline Vector2_t2243707579  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2243707579 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2243707579  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2243707579  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2243707579 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2243707579  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2243707579_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2243707579  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2243707579 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2243707579  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2243707579_H
#ifndef VECTOR3I_T3488636705_H
#define VECTOR3I_T3488636705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vector3i
struct  Vector3i_t3488636705 
{
public:
	// System.Int32 Vector3i::x
	int32_t ___x_0;
	// System.Int32 Vector3i::y
	int32_t ___y_1;
	// System.Int32 Vector3i::z
	int32_t ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705, ___z_2)); }
	inline int32_t get_z_2() const { return ___z_2; }
	inline int32_t* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(int32_t value)
	{
		___z_2 = value;
	}
};

struct Vector3i_t3488636705_StaticFields
{
public:
	// Vector3i Vector3i::zero
	Vector3i_t3488636705  ___zero_3;
	// Vector3i Vector3i::one
	Vector3i_t3488636705  ___one_4;
	// Vector3i Vector3i::oneTileNorth
	Vector3i_t3488636705  ___oneTileNorth_5;
	// Vector3i Vector3i::oneTileEast
	Vector3i_t3488636705  ___oneTileEast_6;
	// Vector3i Vector3i::oneTileSouth
	Vector3i_t3488636705  ___oneTileSouth_7;
	// Vector3i Vector3i::oneTileWest
	Vector3i_t3488636705  ___oneTileWest_8;
	// System.Collections.Generic.Dictionary`2<DirectionTypes,Vector3i> Vector3i::Deltas
	Dictionary_2_t2807614054 * ___Deltas_9;

public:
	inline static int32_t get_offset_of_zero_3() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___zero_3)); }
	inline Vector3i_t3488636705  get_zero_3() const { return ___zero_3; }
	inline Vector3i_t3488636705 * get_address_of_zero_3() { return &___zero_3; }
	inline void set_zero_3(Vector3i_t3488636705  value)
	{
		___zero_3 = value;
	}

	inline static int32_t get_offset_of_one_4() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___one_4)); }
	inline Vector3i_t3488636705  get_one_4() const { return ___one_4; }
	inline Vector3i_t3488636705 * get_address_of_one_4() { return &___one_4; }
	inline void set_one_4(Vector3i_t3488636705  value)
	{
		___one_4 = value;
	}

	inline static int32_t get_offset_of_oneTileNorth_5() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___oneTileNorth_5)); }
	inline Vector3i_t3488636705  get_oneTileNorth_5() const { return ___oneTileNorth_5; }
	inline Vector3i_t3488636705 * get_address_of_oneTileNorth_5() { return &___oneTileNorth_5; }
	inline void set_oneTileNorth_5(Vector3i_t3488636705  value)
	{
		___oneTileNorth_5 = value;
	}

	inline static int32_t get_offset_of_oneTileEast_6() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___oneTileEast_6)); }
	inline Vector3i_t3488636705  get_oneTileEast_6() const { return ___oneTileEast_6; }
	inline Vector3i_t3488636705 * get_address_of_oneTileEast_6() { return &___oneTileEast_6; }
	inline void set_oneTileEast_6(Vector3i_t3488636705  value)
	{
		___oneTileEast_6 = value;
	}

	inline static int32_t get_offset_of_oneTileSouth_7() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___oneTileSouth_7)); }
	inline Vector3i_t3488636705  get_oneTileSouth_7() const { return ___oneTileSouth_7; }
	inline Vector3i_t3488636705 * get_address_of_oneTileSouth_7() { return &___oneTileSouth_7; }
	inline void set_oneTileSouth_7(Vector3i_t3488636705  value)
	{
		___oneTileSouth_7 = value;
	}

	inline static int32_t get_offset_of_oneTileWest_8() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___oneTileWest_8)); }
	inline Vector3i_t3488636705  get_oneTileWest_8() const { return ___oneTileWest_8; }
	inline Vector3i_t3488636705 * get_address_of_oneTileWest_8() { return &___oneTileWest_8; }
	inline void set_oneTileWest_8(Vector3i_t3488636705  value)
	{
		___oneTileWest_8 = value;
	}

	inline static int32_t get_offset_of_Deltas_9() { return static_cast<int32_t>(offsetof(Vector3i_t3488636705_StaticFields, ___Deltas_9)); }
	inline Dictionary_2_t2807614054 * get_Deltas_9() const { return ___Deltas_9; }
	inline Dictionary_2_t2807614054 ** get_address_of_Deltas_9() { return &___Deltas_9; }
	inline void set_Deltas_9(Dictionary_2_t2807614054 * value)
	{
		___Deltas_9 = value;
		Il2CppCodeGenWriteBarrier((&___Deltas_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3I_T3488636705_H
#ifndef CHAR_T3454481338_H
#define CHAR_T3454481338_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3454481338 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3454481338, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3454481338_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3454481338_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3454481338_H
#ifndef COLOR32_T874517518_H
#define COLOR32_T874517518_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t874517518 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t874517518, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T874517518_H
#ifndef KEYVALUEPAIR_2_T3132015601_H
#define KEYVALUEPAIR_2_T3132015601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>
struct  KeyValuePair_2_t3132015601 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	int32_t ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3132015601, ___key_0)); }
	inline int32_t get_key_0() const { return ___key_0; }
	inline int32_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int32_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t3132015601, ___value_1)); }
	inline int32_t get_value_1() const { return ___value_1; }
	inline int32_t* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(int32_t value)
	{
		___value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T3132015601_H
#ifndef WEIGHTEDOPTIONS_T481347131_H
#define WEIGHTEDOPTIONS_T481347131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WeightedOptions
struct  WeightedOptions_t481347131  : public PairStringIntDictionary_t1323851614
{
public:
	// System.Int32 WeightedOptions::_totalWeight
	int32_t ____totalWeight_2;

public:
	inline static int32_t get_offset_of__totalWeight_2() { return static_cast<int32_t>(offsetof(WeightedOptions_t481347131, ____totalWeight_2)); }
	inline int32_t get__totalWeight_2() const { return ____totalWeight_2; }
	inline int32_t* get_address_of__totalWeight_2() { return &____totalWeight_2; }
	inline void set__totalWeight_2(int32_t value)
	{
		____totalWeight_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEIGHTEDOPTIONS_T481347131_H
#ifndef COLOR_T2020392075_H
#define COLOR_T2020392075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2020392075 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2020392075, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2020392075_H
#ifndef QUATERNION_T4030073918_H
#define QUATERNION_T4030073918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t4030073918 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t4030073918_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t4030073918  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t4030073918_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t4030073918  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t4030073918 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t4030073918  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T4030073918_H
#ifndef VOID_T1841601450_H
#define VOID_T1841601450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1841601450 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1841601450_H
#ifndef BOOLEAN_T3825574718_H
#define BOOLEAN_T3825574718_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t3825574718 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t3825574718, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t3825574718_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t3825574718_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T3825574718_H
#ifndef VECTOR3_T2243707580_H
#define VECTOR3_T2243707580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2243707580 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2243707580, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2243707580_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2243707580  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2243707580  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2243707580  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2243707580  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2243707580  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2243707580  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2243707580  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2243707580  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2243707580  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2243707580  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2243707580  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2243707580 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2243707580  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___oneVector_5)); }
	inline Vector3_t2243707580  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2243707580 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2243707580  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___upVector_6)); }
	inline Vector3_t2243707580  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2243707580 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2243707580  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___downVector_7)); }
	inline Vector3_t2243707580  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2243707580 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2243707580  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___leftVector_8)); }
	inline Vector3_t2243707580  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2243707580 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2243707580  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___rightVector_9)); }
	inline Vector3_t2243707580  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2243707580 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2243707580  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2243707580  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2243707580 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2243707580  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___backVector_11)); }
	inline Vector3_t2243707580  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2243707580 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2243707580  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2243707580  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2243707580 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2243707580  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2243707580_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2243707580  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2243707580 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2243707580  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2243707580_H
#ifndef RECT_T3681755626_H
#define RECT_T3681755626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t3681755626 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t3681755626, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T3681755626_H
#ifndef SINGLE_T2076509932_H
#define SINGLE_T2076509932_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t2076509932 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t2076509932, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T2076509932_H
#ifndef INT32_T2071877448_H
#define INT32_T2071877448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2071877448 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2071877448, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2071877448_H
#ifndef ENUM_T2459695545_H
#define ENUM_T2459695545_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2459695545  : public ValueType_t3507792607
{
public:

public:
};

struct Enum_t2459695545_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t1328083999* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t2459695545_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t1328083999* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t1328083999** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t1328083999* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2459695545_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2459695545_marshaled_com
{
};
#endif // ENUM_T2459695545_H
#ifndef SHADOWCASTINGMODE_T4042762198_H
#define SHADOWCASTINGMODE_T4042762198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.ShadowCastingMode
struct  ShadowCastingMode_t4042762198 
{
public:
	// System.Int32 UnityEngine.Rendering.ShadowCastingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShadowCastingMode_t4042762198, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADOWCASTINGMODE_T4042762198_H
#ifndef LIGHTPROBEUSAGE_T664674855_H
#define LIGHTPROBEUSAGE_T664674855_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.LightProbeUsage
struct  LightProbeUsage_t664674855 
{
public:
	// System.Int32 UnityEngine.Rendering.LightProbeUsage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(LightProbeUsage_t664674855, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIGHTPROBEUSAGE_T664674855_H
#ifndef OBJECT_T1021602117_H
#define OBJECT_T1021602117_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t1021602117  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t1021602117, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t1021602117_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t1021602117_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t1021602117_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T1021602117_H
#ifndef ACTION_T4052865656_H
#define ACTION_T4052865656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelStructure/Action
struct  Action_t4052865656 
{
public:
	// System.Int32 VoxelStructure/Action::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Action_t4052865656, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T4052865656_H
#ifndef REFLECTIONPROBEUSAGE_T1870040434_H
#define REFLECTIONPROBEUSAGE_T1870040434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rendering.ReflectionProbeUsage
struct  ReflectionProbeUsage_t1870040434 
{
public:
	// System.Int32 UnityEngine.Rendering.ReflectionProbeUsage::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ReflectionProbeUsage_t1870040434, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONPROBEUSAGE_T1870040434_H
#ifndef ENUMERATOR_T3124852344_H
#define ENUMERATOR_T3124852344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<PairStringInt>
struct  Enumerator_t3124852344 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3590122670 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	PairStringInt_t4221001538  ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3124852344, ___l_0)); }
	inline List_1_t3590122670 * get_l_0() const { return ___l_0; }
	inline List_1_t3590122670 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3590122670 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3124852344, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3124852344, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3124852344, ___current_3)); }
	inline PairStringInt_t4221001538  get_current_3() const { return ___current_3; }
	inline PairStringInt_t4221001538 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(PairStringInt_t4221001538  value)
	{
		___current_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3124852344_H
#ifndef BOUNDS_T3033363703_H
#define BOUNDS_T3033363703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Bounds
struct  Bounds_t3033363703 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t2243707580  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t2243707580  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Center_0)); }
	inline Vector3_t2243707580  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t2243707580 * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t2243707580  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t3033363703, ___m_Extents_1)); }
	inline Vector3_t2243707580  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t2243707580 * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t2243707580  value)
	{
		___m_Extents_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDS_T3033363703_H
#ifndef VOXELFACE_T2735811281_H
#define VOXELFACE_T2735811281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelStructure/VoxelFace
struct  VoxelFace_t2735811281 
{
public:
	// System.Int32 VoxelStructure/VoxelFace::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VoxelFace_t2735811281, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELFACE_T2735811281_H
#ifndef DELEGATE_T3022476291_H
#define DELEGATE_T3022476291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t3022476291  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1572802995 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t3022476291, ___data_8)); }
	inline DelegateData_t1572802995 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1572802995 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1572802995 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T3022476291_H
#ifndef TEXTURE_T2243626319_H
#define TEXTURE_T2243626319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t2243626319  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T2243626319_H
#ifndef GAMEOBJECT_T1756533147_H
#define GAMEOBJECT_T1756533147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1756533147  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1756533147_H
#ifndef SCRIPTABLEOBJECT_T1975622470_H
#define SCRIPTABLEOBJECT_T1975622470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t1975622470  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_pinvoke : public Object_t1021602117_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t1975622470_marshaled_com : public Object_t1021602117_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T1975622470_H
#ifndef MULTICASTDELEGATE_T3201952435_H
#define MULTICASTDELEGATE_T3201952435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t3201952435  : public Delegate_t3022476291
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t3201952435 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t3201952435 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___prev_9)); }
	inline MulticastDelegate_t3201952435 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t3201952435 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t3201952435 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t3201952435, ___kpm_next_10)); }
	inline MulticastDelegate_t3201952435 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t3201952435 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t3201952435 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T3201952435_H
#ifndef COMPONENT_T3819376471_H
#define COMPONENT_T3819376471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t3819376471  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T3819376471_H
#ifndef MESH_T1356156583_H
#define MESH_T1356156583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t1356156583  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T1356156583_H
#ifndef MATERIAL_T193706927_H
#define MATERIAL_T193706927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t193706927  : public Object_t1021602117
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T193706927_H
#ifndef FUNC_2_T1901072402_H
#define FUNC_2_T1901072402_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>,System.Int32>
struct  Func_2_t1901072402  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T1901072402_H
#ifndef FUNC_3_T4100446949_H
#define FUNC_3_T4100446949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`3<VoxelTemplate,System.Int32,System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>
struct  Func_3_t4100446949  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_3_T4100446949_H
#ifndef RENDERER_T257310565_H
#define RENDERER_T257310565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t257310565  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T257310565_H
#ifndef VOXELTEMPLATE_T594004422_H
#define VOXELTEMPLATE_T594004422_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelTemplate
struct  VoxelTemplate_t594004422  : public ScriptableObject_t1975622470
{
public:
	// System.Int32 VoxelTemplate::DisplayOrder
	int32_t ___DisplayOrder_2;
	// UnityEngine.Color32 VoxelTemplate::color
	Color32_t874517518  ___color_3;
	// System.Boolean VoxelTemplate::useFrontUvsForAllFaces
	bool ___useFrontUvsForAllFaces_4;
	// System.Boolean VoxelTemplate::drawFacesInCenter
	bool ___drawFacesInCenter_5;
	// UnityEngine.Vector2 VoxelTemplate::atlasScale
	Vector2_t2243707579  ___atlasScale_6;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetFront
	Vector2_t2243707579  ___UVOffsetFront_7;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetBack
	Vector2_t2243707579  ___UVOffsetBack_8;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetTop
	Vector2_t2243707579  ___UVOffsetTop_9;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetBottom
	Vector2_t2243707579  ___UVOffsetBottom_10;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetLeft
	Vector2_t2243707579  ___UVOffsetLeft_11;
	// UnityEngine.Vector2 VoxelTemplate::UVOffsetRight
	Vector2_t2243707579  ___UVOffsetRight_12;
	// System.Boolean VoxelTemplate::shouldDraw
	bool ___shouldDraw_13;
	// System.Boolean VoxelTemplate::drawFront
	bool ___drawFront_14;
	// System.Boolean VoxelTemplate::drawBack
	bool ___drawBack_15;
	// System.Boolean VoxelTemplate::drawTop
	bool ___drawTop_16;
	// System.Boolean VoxelTemplate::drawBottom
	bool ___drawBottom_17;
	// System.Boolean VoxelTemplate::drawLeft
	bool ___drawLeft_18;
	// System.Boolean VoxelTemplate::drawRight
	bool ___drawRight_19;

public:
	inline static int32_t get_offset_of_DisplayOrder_2() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___DisplayOrder_2)); }
	inline int32_t get_DisplayOrder_2() const { return ___DisplayOrder_2; }
	inline int32_t* get_address_of_DisplayOrder_2() { return &___DisplayOrder_2; }
	inline void set_DisplayOrder_2(int32_t value)
	{
		___DisplayOrder_2 = value;
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___color_3)); }
	inline Color32_t874517518  get_color_3() const { return ___color_3; }
	inline Color32_t874517518 * get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(Color32_t874517518  value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_useFrontUvsForAllFaces_4() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___useFrontUvsForAllFaces_4)); }
	inline bool get_useFrontUvsForAllFaces_4() const { return ___useFrontUvsForAllFaces_4; }
	inline bool* get_address_of_useFrontUvsForAllFaces_4() { return &___useFrontUvsForAllFaces_4; }
	inline void set_useFrontUvsForAllFaces_4(bool value)
	{
		___useFrontUvsForAllFaces_4 = value;
	}

	inline static int32_t get_offset_of_drawFacesInCenter_5() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawFacesInCenter_5)); }
	inline bool get_drawFacesInCenter_5() const { return ___drawFacesInCenter_5; }
	inline bool* get_address_of_drawFacesInCenter_5() { return &___drawFacesInCenter_5; }
	inline void set_drawFacesInCenter_5(bool value)
	{
		___drawFacesInCenter_5 = value;
	}

	inline static int32_t get_offset_of_atlasScale_6() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___atlasScale_6)); }
	inline Vector2_t2243707579  get_atlasScale_6() const { return ___atlasScale_6; }
	inline Vector2_t2243707579 * get_address_of_atlasScale_6() { return &___atlasScale_6; }
	inline void set_atlasScale_6(Vector2_t2243707579  value)
	{
		___atlasScale_6 = value;
	}

	inline static int32_t get_offset_of_UVOffsetFront_7() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetFront_7)); }
	inline Vector2_t2243707579  get_UVOffsetFront_7() const { return ___UVOffsetFront_7; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetFront_7() { return &___UVOffsetFront_7; }
	inline void set_UVOffsetFront_7(Vector2_t2243707579  value)
	{
		___UVOffsetFront_7 = value;
	}

	inline static int32_t get_offset_of_UVOffsetBack_8() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetBack_8)); }
	inline Vector2_t2243707579  get_UVOffsetBack_8() const { return ___UVOffsetBack_8; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetBack_8() { return &___UVOffsetBack_8; }
	inline void set_UVOffsetBack_8(Vector2_t2243707579  value)
	{
		___UVOffsetBack_8 = value;
	}

	inline static int32_t get_offset_of_UVOffsetTop_9() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetTop_9)); }
	inline Vector2_t2243707579  get_UVOffsetTop_9() const { return ___UVOffsetTop_9; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetTop_9() { return &___UVOffsetTop_9; }
	inline void set_UVOffsetTop_9(Vector2_t2243707579  value)
	{
		___UVOffsetTop_9 = value;
	}

	inline static int32_t get_offset_of_UVOffsetBottom_10() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetBottom_10)); }
	inline Vector2_t2243707579  get_UVOffsetBottom_10() const { return ___UVOffsetBottom_10; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetBottom_10() { return &___UVOffsetBottom_10; }
	inline void set_UVOffsetBottom_10(Vector2_t2243707579  value)
	{
		___UVOffsetBottom_10 = value;
	}

	inline static int32_t get_offset_of_UVOffsetLeft_11() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetLeft_11)); }
	inline Vector2_t2243707579  get_UVOffsetLeft_11() const { return ___UVOffsetLeft_11; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetLeft_11() { return &___UVOffsetLeft_11; }
	inline void set_UVOffsetLeft_11(Vector2_t2243707579  value)
	{
		___UVOffsetLeft_11 = value;
	}

	inline static int32_t get_offset_of_UVOffsetRight_12() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___UVOffsetRight_12)); }
	inline Vector2_t2243707579  get_UVOffsetRight_12() const { return ___UVOffsetRight_12; }
	inline Vector2_t2243707579 * get_address_of_UVOffsetRight_12() { return &___UVOffsetRight_12; }
	inline void set_UVOffsetRight_12(Vector2_t2243707579  value)
	{
		___UVOffsetRight_12 = value;
	}

	inline static int32_t get_offset_of_shouldDraw_13() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___shouldDraw_13)); }
	inline bool get_shouldDraw_13() const { return ___shouldDraw_13; }
	inline bool* get_address_of_shouldDraw_13() { return &___shouldDraw_13; }
	inline void set_shouldDraw_13(bool value)
	{
		___shouldDraw_13 = value;
	}

	inline static int32_t get_offset_of_drawFront_14() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawFront_14)); }
	inline bool get_drawFront_14() const { return ___drawFront_14; }
	inline bool* get_address_of_drawFront_14() { return &___drawFront_14; }
	inline void set_drawFront_14(bool value)
	{
		___drawFront_14 = value;
	}

	inline static int32_t get_offset_of_drawBack_15() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawBack_15)); }
	inline bool get_drawBack_15() const { return ___drawBack_15; }
	inline bool* get_address_of_drawBack_15() { return &___drawBack_15; }
	inline void set_drawBack_15(bool value)
	{
		___drawBack_15 = value;
	}

	inline static int32_t get_offset_of_drawTop_16() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawTop_16)); }
	inline bool get_drawTop_16() const { return ___drawTop_16; }
	inline bool* get_address_of_drawTop_16() { return &___drawTop_16; }
	inline void set_drawTop_16(bool value)
	{
		___drawTop_16 = value;
	}

	inline static int32_t get_offset_of_drawBottom_17() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawBottom_17)); }
	inline bool get_drawBottom_17() const { return ___drawBottom_17; }
	inline bool* get_address_of_drawBottom_17() { return &___drawBottom_17; }
	inline void set_drawBottom_17(bool value)
	{
		___drawBottom_17 = value;
	}

	inline static int32_t get_offset_of_drawLeft_18() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawLeft_18)); }
	inline bool get_drawLeft_18() const { return ___drawLeft_18; }
	inline bool* get_address_of_drawLeft_18() { return &___drawLeft_18; }
	inline void set_drawLeft_18(bool value)
	{
		___drawLeft_18 = value;
	}

	inline static int32_t get_offset_of_drawRight_19() { return static_cast<int32_t>(offsetof(VoxelTemplate_t594004422, ___drawRight_19)); }
	inline bool get_drawRight_19() const { return ___drawRight_19; }
	inline bool* get_address_of_drawRight_19() { return &___drawRight_19; }
	inline void set_drawRight_19(bool value)
	{
		___drawRight_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELTEMPLATE_T594004422_H
#ifndef TRANSFORM_T3275118058_H
#define TRANSFORM_T3275118058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3275118058  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3275118058_H
#ifndef MESHFILTER_T3026937449_H
#define MESHFILTER_T3026937449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshFilter
struct  MeshFilter_t3026937449  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHFILTER_T3026937449_H
#ifndef RENDERTEXTURE_T2666733923_H
#define RENDERTEXTURE_T2666733923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTexture
struct  RenderTexture_t2666733923  : public Texture_t2243626319
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTURE_T2666733923_H
#ifndef COLLIDER_T3497673348_H
#define COLLIDER_T3497673348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Collider
struct  Collider_t3497673348  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLIDER_T3497673348_H
#ifndef BEHAVIOUR_T955675639_H
#define BEHAVIOUR_T955675639_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t955675639  : public Component_t3819376471
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T955675639_H
#ifndef ACTION_2_T2601038636_H
#define ACTION_2_T2601038636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`2<UnityEngine.GameObject,UnityEngine.Transform>
struct  Action_2_t2601038636  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_2_T2601038636_H
#ifndef MESHRENDERER_T1268241104_H
#define MESHRENDERER_T1268241104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshRenderer
struct  MeshRenderer_t1268241104  : public Renderer_t257310565
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERER_T1268241104_H
#ifndef MONOBEHAVIOUR_T1158329972_H
#define MONOBEHAVIOUR_T1158329972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t1158329972  : public Behaviour_t955675639
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T1158329972_H
#ifndef MESHCOLLIDER_T2718867283_H
#define MESHCOLLIDER_T2718867283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshCollider
struct  MeshCollider_t2718867283  : public Collider_t3497673348
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHCOLLIDER_T2718867283_H
#ifndef CAMERA_T189460977_H
#define CAMERA_T189460977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t189460977  : public Behaviour_t955675639
{
public:

public:
};

struct Camera_t189460977_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t834278767 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t834278767 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t834278767 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t834278767 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t834278767 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t834278767 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t834278767 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t834278767 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t834278767 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t189460977_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t834278767 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t834278767 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t834278767 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T189460977_H
#ifndef UIBEHAVIOUR_T3960014691_H
#define UIBEHAVIOUR_T3960014691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3960014691  : public MonoBehaviour_t1158329972
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3960014691_H
#ifndef U421HASEMOVER_T4119473191_H
#define U421HASEMOVER_T4119473191_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// СhaseMover
struct  U421haseMover_t4119473191  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Transform СhaseMover::_destination
	Transform_t3275118058 * ____destination_2;
	// System.Single СhaseMover::_speed
	float ____speed_3;
	// System.Action`2<UnityEngine.GameObject,UnityEngine.Transform> СhaseMover::OnCatch
	Action_2_t2601038636 * ___OnCatch_4;

public:
	inline static int32_t get_offset_of__destination_2() { return static_cast<int32_t>(offsetof(U421haseMover_t4119473191, ____destination_2)); }
	inline Transform_t3275118058 * get__destination_2() const { return ____destination_2; }
	inline Transform_t3275118058 ** get_address_of__destination_2() { return &____destination_2; }
	inline void set__destination_2(Transform_t3275118058 * value)
	{
		____destination_2 = value;
		Il2CppCodeGenWriteBarrier((&____destination_2), value);
	}

	inline static int32_t get_offset_of__speed_3() { return static_cast<int32_t>(offsetof(U421haseMover_t4119473191, ____speed_3)); }
	inline float get__speed_3() const { return ____speed_3; }
	inline float* get_address_of__speed_3() { return &____speed_3; }
	inline void set__speed_3(float value)
	{
		____speed_3 = value;
	}

	inline static int32_t get_offset_of_OnCatch_4() { return static_cast<int32_t>(offsetof(U421haseMover_t4119473191, ___OnCatch_4)); }
	inline Action_2_t2601038636 * get_OnCatch_4() const { return ___OnCatch_4; }
	inline Action_2_t2601038636 ** get_address_of_OnCatch_4() { return &___OnCatch_4; }
	inline void set_OnCatch_4(Action_2_t2601038636 * value)
	{
		___OnCatch_4 = value;
		Il2CppCodeGenWriteBarrier((&___OnCatch_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U421HASEMOVER_T4119473191_H
#ifndef VOXELSTRUCTURE_T2247263099_H
#define VOXELSTRUCTURE_T2247263099_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelStructure
struct  VoxelStructure_t2247263099  : public MonoBehaviour_t1158329972
{
public:
	// VoxelPallette VoxelStructure::pallette
	VoxelPallette_t4284593999 * ___pallette_2;
	// VoxelSpace VoxelStructure::<VoxelSpace>k__BackingField
	VoxelSpace_t930815766 * ___U3CVoxelSpaceU3Ek__BackingField_3;
	// System.Int32[0...,0...,0...] VoxelStructure::flatVoxels
	Int32U5B0___U2C0___U2C0___U5D_t3030399643* ___flatVoxels_7;
	// System.Int32 VoxelStructure::Width
	int32_t ___Width_8;
	// System.Int32 VoxelStructure::Height
	int32_t ___Height_9;
	// System.Int32 VoxelStructure::Depth
	int32_t ___Depth_10;
	// UnityEngine.Vector3[] VoxelStructure::_verts
	Vector3U5BU5D_t1172311765* ____verts_11;
	// UnityEngine.Vector2[] VoxelStructure::_uvs
	Vector2U5BU5D_t686124026* ____uvs_12;
	// UnityEngine.Color32[] VoxelStructure::_colors
	Color32U5BU5D_t30278651* ____colors_13;
	// UnityEngine.MeshFilter VoxelStructure::_mf
	MeshFilter_t3026937449 * ____mf_14;
	// System.Int32[] VoxelStructure::_tris
	Int32U5BU5D_t3030399641* ____tris_15;
	// VoxelTemplate VoxelStructure::fType
	VoxelTemplate_t594004422 * ___fType_16;
	// VoxelTemplate VoxelStructure::kType
	VoxelTemplate_t594004422 * ___kType_17;
	// VoxelTemplate VoxelStructure::rType
	VoxelTemplate_t594004422 * ___rType_18;
	// VoxelTemplate VoxelStructure::lType
	VoxelTemplate_t594004422 * ___lType_19;
	// VoxelTemplate VoxelStructure::tType
	VoxelTemplate_t594004422 * ___tType_20;
	// VoxelTemplate VoxelStructure::bType
	VoxelTemplate_t594004422 * ___bType_21;
	// VoxelTemplate VoxelStructure::voxelType
	VoxelTemplate_t594004422 * ___voxelType_22;
	// UnityEngine.Color32 VoxelStructure::voxelColor
	Color32_t874517518  ___voxelColor_23;
	// System.Boolean VoxelStructure::IsDirty
	bool ___IsDirty_24;
	// System.Single VoxelStructure::ONE_PIXEL
	float ___ONE_PIXEL_25;

public:
	inline static int32_t get_offset_of_pallette_2() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___pallette_2)); }
	inline VoxelPallette_t4284593999 * get_pallette_2() const { return ___pallette_2; }
	inline VoxelPallette_t4284593999 ** get_address_of_pallette_2() { return &___pallette_2; }
	inline void set_pallette_2(VoxelPallette_t4284593999 * value)
	{
		___pallette_2 = value;
		Il2CppCodeGenWriteBarrier((&___pallette_2), value);
	}

	inline static int32_t get_offset_of_U3CVoxelSpaceU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___U3CVoxelSpaceU3Ek__BackingField_3)); }
	inline VoxelSpace_t930815766 * get_U3CVoxelSpaceU3Ek__BackingField_3() const { return ___U3CVoxelSpaceU3Ek__BackingField_3; }
	inline VoxelSpace_t930815766 ** get_address_of_U3CVoxelSpaceU3Ek__BackingField_3() { return &___U3CVoxelSpaceU3Ek__BackingField_3; }
	inline void set_U3CVoxelSpaceU3Ek__BackingField_3(VoxelSpace_t930815766 * value)
	{
		___U3CVoxelSpaceU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVoxelSpaceU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_flatVoxels_7() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___flatVoxels_7)); }
	inline Int32U5B0___U2C0___U2C0___U5D_t3030399643* get_flatVoxels_7() const { return ___flatVoxels_7; }
	inline Int32U5B0___U2C0___U2C0___U5D_t3030399643** get_address_of_flatVoxels_7() { return &___flatVoxels_7; }
	inline void set_flatVoxels_7(Int32U5B0___U2C0___U2C0___U5D_t3030399643* value)
	{
		___flatVoxels_7 = value;
		Il2CppCodeGenWriteBarrier((&___flatVoxels_7), value);
	}

	inline static int32_t get_offset_of_Width_8() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___Width_8)); }
	inline int32_t get_Width_8() const { return ___Width_8; }
	inline int32_t* get_address_of_Width_8() { return &___Width_8; }
	inline void set_Width_8(int32_t value)
	{
		___Width_8 = value;
	}

	inline static int32_t get_offset_of_Height_9() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___Height_9)); }
	inline int32_t get_Height_9() const { return ___Height_9; }
	inline int32_t* get_address_of_Height_9() { return &___Height_9; }
	inline void set_Height_9(int32_t value)
	{
		___Height_9 = value;
	}

	inline static int32_t get_offset_of_Depth_10() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___Depth_10)); }
	inline int32_t get_Depth_10() const { return ___Depth_10; }
	inline int32_t* get_address_of_Depth_10() { return &___Depth_10; }
	inline void set_Depth_10(int32_t value)
	{
		___Depth_10 = value;
	}

	inline static int32_t get_offset_of__verts_11() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ____verts_11)); }
	inline Vector3U5BU5D_t1172311765* get__verts_11() const { return ____verts_11; }
	inline Vector3U5BU5D_t1172311765** get_address_of__verts_11() { return &____verts_11; }
	inline void set__verts_11(Vector3U5BU5D_t1172311765* value)
	{
		____verts_11 = value;
		Il2CppCodeGenWriteBarrier((&____verts_11), value);
	}

	inline static int32_t get_offset_of__uvs_12() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ____uvs_12)); }
	inline Vector2U5BU5D_t686124026* get__uvs_12() const { return ____uvs_12; }
	inline Vector2U5BU5D_t686124026** get_address_of__uvs_12() { return &____uvs_12; }
	inline void set__uvs_12(Vector2U5BU5D_t686124026* value)
	{
		____uvs_12 = value;
		Il2CppCodeGenWriteBarrier((&____uvs_12), value);
	}

	inline static int32_t get_offset_of__colors_13() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ____colors_13)); }
	inline Color32U5BU5D_t30278651* get__colors_13() const { return ____colors_13; }
	inline Color32U5BU5D_t30278651** get_address_of__colors_13() { return &____colors_13; }
	inline void set__colors_13(Color32U5BU5D_t30278651* value)
	{
		____colors_13 = value;
		Il2CppCodeGenWriteBarrier((&____colors_13), value);
	}

	inline static int32_t get_offset_of__mf_14() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ____mf_14)); }
	inline MeshFilter_t3026937449 * get__mf_14() const { return ____mf_14; }
	inline MeshFilter_t3026937449 ** get_address_of__mf_14() { return &____mf_14; }
	inline void set__mf_14(MeshFilter_t3026937449 * value)
	{
		____mf_14 = value;
		Il2CppCodeGenWriteBarrier((&____mf_14), value);
	}

	inline static int32_t get_offset_of__tris_15() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ____tris_15)); }
	inline Int32U5BU5D_t3030399641* get__tris_15() const { return ____tris_15; }
	inline Int32U5BU5D_t3030399641** get_address_of__tris_15() { return &____tris_15; }
	inline void set__tris_15(Int32U5BU5D_t3030399641* value)
	{
		____tris_15 = value;
		Il2CppCodeGenWriteBarrier((&____tris_15), value);
	}

	inline static int32_t get_offset_of_fType_16() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___fType_16)); }
	inline VoxelTemplate_t594004422 * get_fType_16() const { return ___fType_16; }
	inline VoxelTemplate_t594004422 ** get_address_of_fType_16() { return &___fType_16; }
	inline void set_fType_16(VoxelTemplate_t594004422 * value)
	{
		___fType_16 = value;
		Il2CppCodeGenWriteBarrier((&___fType_16), value);
	}

	inline static int32_t get_offset_of_kType_17() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___kType_17)); }
	inline VoxelTemplate_t594004422 * get_kType_17() const { return ___kType_17; }
	inline VoxelTemplate_t594004422 ** get_address_of_kType_17() { return &___kType_17; }
	inline void set_kType_17(VoxelTemplate_t594004422 * value)
	{
		___kType_17 = value;
		Il2CppCodeGenWriteBarrier((&___kType_17), value);
	}

	inline static int32_t get_offset_of_rType_18() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___rType_18)); }
	inline VoxelTemplate_t594004422 * get_rType_18() const { return ___rType_18; }
	inline VoxelTemplate_t594004422 ** get_address_of_rType_18() { return &___rType_18; }
	inline void set_rType_18(VoxelTemplate_t594004422 * value)
	{
		___rType_18 = value;
		Il2CppCodeGenWriteBarrier((&___rType_18), value);
	}

	inline static int32_t get_offset_of_lType_19() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___lType_19)); }
	inline VoxelTemplate_t594004422 * get_lType_19() const { return ___lType_19; }
	inline VoxelTemplate_t594004422 ** get_address_of_lType_19() { return &___lType_19; }
	inline void set_lType_19(VoxelTemplate_t594004422 * value)
	{
		___lType_19 = value;
		Il2CppCodeGenWriteBarrier((&___lType_19), value);
	}

	inline static int32_t get_offset_of_tType_20() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___tType_20)); }
	inline VoxelTemplate_t594004422 * get_tType_20() const { return ___tType_20; }
	inline VoxelTemplate_t594004422 ** get_address_of_tType_20() { return &___tType_20; }
	inline void set_tType_20(VoxelTemplate_t594004422 * value)
	{
		___tType_20 = value;
		Il2CppCodeGenWriteBarrier((&___tType_20), value);
	}

	inline static int32_t get_offset_of_bType_21() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___bType_21)); }
	inline VoxelTemplate_t594004422 * get_bType_21() const { return ___bType_21; }
	inline VoxelTemplate_t594004422 ** get_address_of_bType_21() { return &___bType_21; }
	inline void set_bType_21(VoxelTemplate_t594004422 * value)
	{
		___bType_21 = value;
		Il2CppCodeGenWriteBarrier((&___bType_21), value);
	}

	inline static int32_t get_offset_of_voxelType_22() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___voxelType_22)); }
	inline VoxelTemplate_t594004422 * get_voxelType_22() const { return ___voxelType_22; }
	inline VoxelTemplate_t594004422 ** get_address_of_voxelType_22() { return &___voxelType_22; }
	inline void set_voxelType_22(VoxelTemplate_t594004422 * value)
	{
		___voxelType_22 = value;
		Il2CppCodeGenWriteBarrier((&___voxelType_22), value);
	}

	inline static int32_t get_offset_of_voxelColor_23() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___voxelColor_23)); }
	inline Color32_t874517518  get_voxelColor_23() const { return ___voxelColor_23; }
	inline Color32_t874517518 * get_address_of_voxelColor_23() { return &___voxelColor_23; }
	inline void set_voxelColor_23(Color32_t874517518  value)
	{
		___voxelColor_23 = value;
	}

	inline static int32_t get_offset_of_IsDirty_24() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___IsDirty_24)); }
	inline bool get_IsDirty_24() const { return ___IsDirty_24; }
	inline bool* get_address_of_IsDirty_24() { return &___IsDirty_24; }
	inline void set_IsDirty_24(bool value)
	{
		___IsDirty_24 = value;
	}

	inline static int32_t get_offset_of_ONE_PIXEL_25() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099, ___ONE_PIXEL_25)); }
	inline float get_ONE_PIXEL_25() const { return ___ONE_PIXEL_25; }
	inline float* get_address_of_ONE_PIXEL_25() { return &___ONE_PIXEL_25; }
	inline void set_ONE_PIXEL_25(float value)
	{
		___ONE_PIXEL_25 = value;
	}
};

struct VoxelStructure_t2247263099_StaticFields
{
public:
	// VoxelStructure/Action VoxelStructure::LMB_Action
	int32_t ___LMB_Action_4;
	// VoxelStructure/Action VoxelStructure::RMB_Action
	int32_t ___RMB_Action_5;
	// VoxelStructure/Action VoxelStructure::Static_Action
	int32_t ___Static_Action_6;

public:
	inline static int32_t get_offset_of_LMB_Action_4() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099_StaticFields, ___LMB_Action_4)); }
	inline int32_t get_LMB_Action_4() const { return ___LMB_Action_4; }
	inline int32_t* get_address_of_LMB_Action_4() { return &___LMB_Action_4; }
	inline void set_LMB_Action_4(int32_t value)
	{
		___LMB_Action_4 = value;
	}

	inline static int32_t get_offset_of_RMB_Action_5() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099_StaticFields, ___RMB_Action_5)); }
	inline int32_t get_RMB_Action_5() const { return ___RMB_Action_5; }
	inline int32_t* get_address_of_RMB_Action_5() { return &___RMB_Action_5; }
	inline void set_RMB_Action_5(int32_t value)
	{
		___RMB_Action_5 = value;
	}

	inline static int32_t get_offset_of_Static_Action_6() { return static_cast<int32_t>(offsetof(VoxelStructure_t2247263099_StaticFields, ___Static_Action_6)); }
	inline int32_t get_Static_Action_6() const { return ___Static_Action_6; }
	inline int32_t* get_address_of_Static_Action_6() { return &___Static_Action_6; }
	inline void set_Static_Action_6(int32_t value)
	{
		___Static_Action_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELSTRUCTURE_T2247263099_H
#ifndef VOXELSPACE_T930815766_H
#define VOXELSPACE_T930815766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VoxelSpace
struct  VoxelSpace_t930815766  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 VoxelSpace::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_2;
	// System.Int32 VoxelSpace::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_3;
	// System.Int32 VoxelSpace::<Depth>k__BackingField
	int32_t ___U3CDepthU3Ek__BackingField_4;
	// System.Int32 VoxelSpace::_chunkSizeW
	int32_t ____chunkSizeW_5;
	// System.Int32 VoxelSpace::_chunkSizeH
	int32_t ____chunkSizeH_6;
	// System.Int32 VoxelSpace::_chunkSizeD
	int32_t ____chunkSizeD_7;
	// System.Int32 VoxelSpace::_chunkCountW
	int32_t ____chunkCountW_8;
	// System.Int32 VoxelSpace::_chunkCountH
	int32_t ____chunkCountH_9;
	// System.Int32 VoxelSpace::_chunkCountD
	int32_t ____chunkCountD_10;
	// VoxelStructure[0...,0...,0...] VoxelSpace::_chunks
	VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* ____chunks_11;
	// System.Boolean VoxelSpace::DrawFront
	bool ___DrawFront_12;
	// System.Boolean VoxelSpace::DrawBack
	bool ___DrawBack_13;
	// System.Boolean VoxelSpace::DrawTop
	bool ___DrawTop_14;
	// System.Boolean VoxelSpace::DrawBottom
	bool ___DrawBottom_15;
	// System.Boolean VoxelSpace::DrawLeft
	bool ___DrawLeft_16;
	// System.Boolean VoxelSpace::DrawRight
	bool ___DrawRight_17;
	// System.Boolean VoxelSpace::GenerateSecondaryUvSet
	bool ___GenerateSecondaryUvSet_18;
	// System.Boolean VoxelSpace::UsePalletteTexture
	bool ___UsePalletteTexture_19;

public:
	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___U3CWidthU3Ek__BackingField_2)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_2() const { return ___U3CWidthU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_2() { return &___U3CWidthU3Ek__BackingField_2; }
	inline void set_U3CWidthU3Ek__BackingField_2(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___U3CHeightU3Ek__BackingField_3)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_3() const { return ___U3CHeightU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_3() { return &___U3CHeightU3Ek__BackingField_3; }
	inline void set_U3CHeightU3Ek__BackingField_3(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDepthU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___U3CDepthU3Ek__BackingField_4)); }
	inline int32_t get_U3CDepthU3Ek__BackingField_4() const { return ___U3CDepthU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CDepthU3Ek__BackingField_4() { return &___U3CDepthU3Ek__BackingField_4; }
	inline void set_U3CDepthU3Ek__BackingField_4(int32_t value)
	{
		___U3CDepthU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of__chunkSizeW_5() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkSizeW_5)); }
	inline int32_t get__chunkSizeW_5() const { return ____chunkSizeW_5; }
	inline int32_t* get_address_of__chunkSizeW_5() { return &____chunkSizeW_5; }
	inline void set__chunkSizeW_5(int32_t value)
	{
		____chunkSizeW_5 = value;
	}

	inline static int32_t get_offset_of__chunkSizeH_6() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkSizeH_6)); }
	inline int32_t get__chunkSizeH_6() const { return ____chunkSizeH_6; }
	inline int32_t* get_address_of__chunkSizeH_6() { return &____chunkSizeH_6; }
	inline void set__chunkSizeH_6(int32_t value)
	{
		____chunkSizeH_6 = value;
	}

	inline static int32_t get_offset_of__chunkSizeD_7() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkSizeD_7)); }
	inline int32_t get__chunkSizeD_7() const { return ____chunkSizeD_7; }
	inline int32_t* get_address_of__chunkSizeD_7() { return &____chunkSizeD_7; }
	inline void set__chunkSizeD_7(int32_t value)
	{
		____chunkSizeD_7 = value;
	}

	inline static int32_t get_offset_of__chunkCountW_8() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkCountW_8)); }
	inline int32_t get__chunkCountW_8() const { return ____chunkCountW_8; }
	inline int32_t* get_address_of__chunkCountW_8() { return &____chunkCountW_8; }
	inline void set__chunkCountW_8(int32_t value)
	{
		____chunkCountW_8 = value;
	}

	inline static int32_t get_offset_of__chunkCountH_9() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkCountH_9)); }
	inline int32_t get__chunkCountH_9() const { return ____chunkCountH_9; }
	inline int32_t* get_address_of__chunkCountH_9() { return &____chunkCountH_9; }
	inline void set__chunkCountH_9(int32_t value)
	{
		____chunkCountH_9 = value;
	}

	inline static int32_t get_offset_of__chunkCountD_10() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunkCountD_10)); }
	inline int32_t get__chunkCountD_10() const { return ____chunkCountD_10; }
	inline int32_t* get_address_of__chunkCountD_10() { return &____chunkCountD_10; }
	inline void set__chunkCountD_10(int32_t value)
	{
		____chunkCountD_10 = value;
	}

	inline static int32_t get_offset_of__chunks_11() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ____chunks_11)); }
	inline VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* get__chunks_11() const { return ____chunks_11; }
	inline VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636** get_address_of__chunks_11() { return &____chunks_11; }
	inline void set__chunks_11(VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* value)
	{
		____chunks_11 = value;
		Il2CppCodeGenWriteBarrier((&____chunks_11), value);
	}

	inline static int32_t get_offset_of_DrawFront_12() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawFront_12)); }
	inline bool get_DrawFront_12() const { return ___DrawFront_12; }
	inline bool* get_address_of_DrawFront_12() { return &___DrawFront_12; }
	inline void set_DrawFront_12(bool value)
	{
		___DrawFront_12 = value;
	}

	inline static int32_t get_offset_of_DrawBack_13() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawBack_13)); }
	inline bool get_DrawBack_13() const { return ___DrawBack_13; }
	inline bool* get_address_of_DrawBack_13() { return &___DrawBack_13; }
	inline void set_DrawBack_13(bool value)
	{
		___DrawBack_13 = value;
	}

	inline static int32_t get_offset_of_DrawTop_14() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawTop_14)); }
	inline bool get_DrawTop_14() const { return ___DrawTop_14; }
	inline bool* get_address_of_DrawTop_14() { return &___DrawTop_14; }
	inline void set_DrawTop_14(bool value)
	{
		___DrawTop_14 = value;
	}

	inline static int32_t get_offset_of_DrawBottom_15() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawBottom_15)); }
	inline bool get_DrawBottom_15() const { return ___DrawBottom_15; }
	inline bool* get_address_of_DrawBottom_15() { return &___DrawBottom_15; }
	inline void set_DrawBottom_15(bool value)
	{
		___DrawBottom_15 = value;
	}

	inline static int32_t get_offset_of_DrawLeft_16() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawLeft_16)); }
	inline bool get_DrawLeft_16() const { return ___DrawLeft_16; }
	inline bool* get_address_of_DrawLeft_16() { return &___DrawLeft_16; }
	inline void set_DrawLeft_16(bool value)
	{
		___DrawLeft_16 = value;
	}

	inline static int32_t get_offset_of_DrawRight_17() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___DrawRight_17)); }
	inline bool get_DrawRight_17() const { return ___DrawRight_17; }
	inline bool* get_address_of_DrawRight_17() { return &___DrawRight_17; }
	inline void set_DrawRight_17(bool value)
	{
		___DrawRight_17 = value;
	}

	inline static int32_t get_offset_of_GenerateSecondaryUvSet_18() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___GenerateSecondaryUvSet_18)); }
	inline bool get_GenerateSecondaryUvSet_18() const { return ___GenerateSecondaryUvSet_18; }
	inline bool* get_address_of_GenerateSecondaryUvSet_18() { return &___GenerateSecondaryUvSet_18; }
	inline void set_GenerateSecondaryUvSet_18(bool value)
	{
		___GenerateSecondaryUvSet_18 = value;
	}

	inline static int32_t get_offset_of_UsePalletteTexture_19() { return static_cast<int32_t>(offsetof(VoxelSpace_t930815766, ___UsePalletteTexture_19)); }
	inline bool get_UsePalletteTexture_19() const { return ___UsePalletteTexture_19; }
	inline bool* get_address_of_UsePalletteTexture_19() { return &___UsePalletteTexture_19; }
	inline void set_UsePalletteTexture_19(bool value)
	{
		___UsePalletteTexture_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOXELSPACE_T930815766_H
#ifndef GRAPHIC_T2426225576_H
#define GRAPHIC_T2426225576_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t2426225576  : public UIBehaviour_t3960014691
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t193706927 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t2020392075  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t3349966182 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t261436805 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t209405766 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4025899511 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4025899511 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4025899511 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t3177091249 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Material_4)); }
	inline Material_t193706927 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t193706927 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t193706927 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Color_5)); }
	inline Color_t2020392075  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t2020392075 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t2020392075  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_RectTransform_7)); }
	inline RectTransform_t3349966182 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t3349966182 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t3349966182 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t261436805 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t261436805 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t261436805 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_Canvas_9)); }
	inline Canvas_t209405766 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t209405766 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t209405766 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4025899511 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4025899511 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4025899511 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t3177091249 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t3177091249 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t3177091249 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t2426225576, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t2426225576_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t193706927 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t3542995729 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t1356156583 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t385374196 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t193706927 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t193706927 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t193706927 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t3542995729 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t3542995729 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t3542995729 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t1356156583 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t1356156583 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t1356156583 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t2426225576_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t385374196 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t385374196 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t385374196 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T2426225576_H
#ifndef MASKABLEGRAPHIC_T540192618_H
#define MASKABLEGRAPHIC_T540192618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t540192618  : public Graphic_t2426225576
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t193706927 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t1156185964 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3778758259 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t1172311765* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_MaskMaterial_20)); }
	inline Material_t193706927 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t193706927 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t193706927 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ParentMask_21)); }
	inline RectMask2D_t1156185964 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t1156185964 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t1156185964 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3778758259 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3778758259 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3778758259 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t540192618, ___m_Corners_27)); }
	inline Vector3U5BU5D_t1172311765* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t1172311765** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t1172311765* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T540192618_H
#ifndef RAWIMAGE_T2749640213_H
#define RAWIMAGE_T2749640213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.RawImage
struct  RawImage_t2749640213  : public MaskableGraphic_t540192618
{
public:
	// UnityEngine.Texture UnityEngine.UI.RawImage::m_Texture
	Texture_t2243626319 * ___m_Texture_28;
	// UnityEngine.Rect UnityEngine.UI.RawImage::m_UVRect
	Rect_t3681755626  ___m_UVRect_29;

public:
	inline static int32_t get_offset_of_m_Texture_28() { return static_cast<int32_t>(offsetof(RawImage_t2749640213, ___m_Texture_28)); }
	inline Texture_t2243626319 * get_m_Texture_28() const { return ___m_Texture_28; }
	inline Texture_t2243626319 ** get_address_of_m_Texture_28() { return &___m_Texture_28; }
	inline void set_m_Texture_28(Texture_t2243626319 * value)
	{
		___m_Texture_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Texture_28), value);
	}

	inline static int32_t get_offset_of_m_UVRect_29() { return static_cast<int32_t>(offsetof(RawImage_t2749640213, ___m_UVRect_29)); }
	inline Rect_t3681755626  get_m_UVRect_29() const { return ___m_UVRect_29; }
	inline Rect_t3681755626 * get_address_of_m_UVRect_29() { return &___m_UVRect_29; }
	inline void set_m_UVRect_29(Rect_t3681755626  value)
	{
		___m_UVRect_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAWIMAGE_T2749640213_H
#ifndef VIEWPORT3D_T2082711749_H
#define VIEWPORT3D_T2082711749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ViewPort3D
struct  ViewPort3D_t2082711749  : public RawImage_t2749640213
{
public:
	// UnityEngine.Camera ViewPort3D::<_camera>k__BackingField
	Camera_t189460977 * ___U3C_cameraU3Ek__BackingField_30;
	// UnityEngine.GameObject ViewPort3D::<Model>k__BackingField
	GameObject_t1756533147 * ___U3CModelU3Ek__BackingField_31;

public:
	inline static int32_t get_offset_of_U3C_cameraU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(ViewPort3D_t2082711749, ___U3C_cameraU3Ek__BackingField_30)); }
	inline Camera_t189460977 * get_U3C_cameraU3Ek__BackingField_30() const { return ___U3C_cameraU3Ek__BackingField_30; }
	inline Camera_t189460977 ** get_address_of_U3C_cameraU3Ek__BackingField_30() { return &___U3C_cameraU3Ek__BackingField_30; }
	inline void set_U3C_cameraU3Ek__BackingField_30(Camera_t189460977 * value)
	{
		___U3C_cameraU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_cameraU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CModelU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(ViewPort3D_t2082711749, ___U3CModelU3Ek__BackingField_31)); }
	inline GameObject_t1756533147 * get_U3CModelU3Ek__BackingField_31() const { return ___U3CModelU3Ek__BackingField_31; }
	inline GameObject_t1756533147 ** get_address_of_U3CModelU3Ek__BackingField_31() { return &___U3CModelU3Ek__BackingField_31; }
	inline void set_U3CModelU3Ek__BackingField_31(GameObject_t1756533147 * value)
	{
		___U3CModelU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CModelU3Ek__BackingField_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWPORT3D_T2082711749_H
// VoxelTemplate[]
struct VoxelTemplateU5BU5D_t2866079843  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) VoxelTemplate_t594004422 * m_Items[1];

public:
	inline VoxelTemplate_t594004422 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline VoxelTemplate_t594004422 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, VoxelTemplate_t594004422 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline VoxelTemplate_t594004422 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline VoxelTemplate_t594004422 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, VoxelTemplate_t594004422 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// VoxelStructure[0...,0...,0...]
struct VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) VoxelStructure_t2247263099 * m_Items[1];

public:
	inline VoxelStructure_t2247263099 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline VoxelStructure_t2247263099 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, VoxelStructure_t2247263099 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline VoxelStructure_t2247263099 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline VoxelStructure_t2247263099 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, VoxelStructure_t2247263099 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline VoxelStructure_t2247263099 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline VoxelStructure_t2247263099 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, VoxelStructure_t2247263099 * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline VoxelStructure_t2247263099 * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline VoxelStructure_t2247263099 ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, VoxelStructure_t2247263099 * value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Single[0...,0...]
struct SingleU5B0___U2C0___U5D_t577127398  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
	inline float GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, float value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, float value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
};
// System.Int32[0...,0...,0...]
struct Int32U5B0___U2C0___U2C0___U5D_t3030399643  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
	inline int32_t GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, int32_t value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);
		il2cpp_array_size_t kBound = bounds[2].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(k, kBound);

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, il2cpp_array_size_t k, int32_t value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;
		il2cpp_array_size_t kBound = bounds[2].length;

		il2cpp_array_size_t index = (i * jBound + j) * kBound + k;
		m_Items[index] = value;
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t686124026  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_t2243707579  m_Items[1];

public:
	inline Vector2_t2243707579  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_t2243707579 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_t2243707579  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_t2243707579  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_t2243707579 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_t2243707579  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t3030399641  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Color32[]
struct Color32U5BU5D_t30278651  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color32_t874517518  m_Items[1];

public:
	inline Color32_t874517518  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color32_t874517518 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color32_t874517518  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color32_t874517518  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color32_t874517518 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color32_t874517518  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m4109961936_gshared (Component_t3819376471 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  RuntimeObject * Object_Instantiate_TisRuntimeObject_m3829784634_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, Vector3_t2243707580  p1, Quaternion_t4030073918  p2, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor()
extern "C"  void List_1__ctor_m3048104478_gshared (List_1_t2501136733 * __this, const RuntimeMethod* method);
// System.Void System.Func`3<System.Object,System.Int32,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_3__ctor_m119210186_gshared (Func_3_t3811912889 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`3<!!0,System.Int32,!!1>)
extern "C"  RuntimeObject* Enumerable_Select_TisRuntimeObject_TisKeyValuePair_2_t3716250094_m1204168198_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_3_t3811912889 * p1, const RuntimeMethod* method);
// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>,System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m2032212577_gshared (Func_2_t863984051 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// System.Linq.IOrderedEnumerable`1<!!0> System.Linq.Enumerable::OrderBy<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
extern "C"  RuntimeObject* Enumerable_OrderBy_TisKeyValuePair_2_t3716250094_TisInt32_t2071877448_m2585386606_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, Func_2_t863984051 * p1, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t3085371226 * Enumerable_ToList_TisKeyValuePair_2_t3716250094_m1315919073_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3716250094  List_1_get_Item_m2794064263_gshared (List_1_t3085371226 * __this, int32_t p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  RuntimeObject * KeyValuePair_2_get_Key_m2959920334_gshared (KeyValuePair_2_t3716250094 * __this, const RuntimeMethod* method);
// !1 System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1057652735_gshared (KeyValuePair_2_t3716250094 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(!0,!1)
extern "C"  void KeyValuePair_2__ctor_m629383883_gshared (KeyValuePair_2_t3132015601 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Add(!0)
extern "C"  void List_1_Add_m3599575194_gshared (List_1_t2501136733 * __this, KeyValuePair_2_t3132015601  p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Count()
extern "C"  int32_t List_1_get_Count_m4118589774_gshared (List_1_t3085371226 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(!0,!1)
extern "C"  void KeyValuePair_2__ctor_m1391475603_gshared (KeyValuePair_2_t3716250094 * __this, RuntimeObject * p0, int32_t p1, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m2049635786_gshared (GameObject_t1756533147 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  RuntimeObject* Enumerable_Take_TisVector3_t2243707580_m1945563610_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, int32_t p1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Vector3U5BU5D_t1172311765* Enumerable_ToArray_TisVector3_t2243707580_m1930596601_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  RuntimeObject* Enumerable_Take_TisInt32_t2071877448_m1592882544_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, int32_t p1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Int32U5BU5D_t3030399641* Enumerable_ToArray_TisInt32_t2071877448_m513246933_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  RuntimeObject* Enumerable_Take_TisVector2_t2243707579_m1080133109_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, int32_t p1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Vector2U5BU5D_t686124026* Enumerable_ToArray_TisVector2_t2243707579_m1715071610_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<UnityEngine.Color32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
extern "C"  RuntimeObject* Enumerable_Take_TisColor32_t874517518_m543186848_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, int32_t p1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Color32>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Color32U5BU5D_t30278651* Enumerable_ToArray_TisColor32_t874517518_m1885434799_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<PairStringInt>::GetEnumerator()
extern "C"  Enumerator_t3124852344  List_1_GetEnumerator_m1313580170_gshared (List_1_t3590122670 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<PairStringInt>::get_Current()
extern "C"  PairStringInt_t4221001538  Enumerator_get_Current_m784304568_gshared (Enumerator_t3124852344 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<PairStringInt>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m775896506_gshared (Enumerator_t3124852344 * __this, const RuntimeMethod* method);
// System.Void System.Action`2<System.Object,System.Object>::Invoke(!0,!1)
extern "C"  void Action_2_Invoke_m2406183663_gshared (Action_2_t2572051853 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);

// System.Void UnityEngine.UI.RawImage::.ctor()
extern "C"  void RawImage__ctor_m527845386 (RawImage_t2749640213 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera ViewPort3D::get__camera()
extern "C"  Camera_t189460977 * ViewPort3D_get__camera_m3886794412 (ViewPort3D_t2082711749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t189460977_m3276577584(__this, method) ((  Camera_t189460977 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m4109961936_gshared)(__this, method)
// System.Void ViewPort3D::set__camera(UnityEngine.Camera)
extern "C"  void ViewPort3D_set__camera_m3900663245 (ViewPort3D_t2082711749 * __this, Camera_t189460977 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::Render()
extern "C"  void Camera_Render_m2021402646 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UnityEngine.UI.RawImage::get_texture()
extern "C"  Texture_t2243626319 * RawImage_get_texture_m2258734143 (RawImage_t2749640213 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t2666733923 * Camera_get_targetTexture_m705925974 (Camera_t189460977 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
extern "C"  void Graphics_Blit_m2123328641 (RuntimeObject * __this /* static, unused */, Texture_t2243626319 * p0, RenderTexture_t2666733923 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t4030073918  Quaternion_get_identity_m1561886418 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
#define Object_Instantiate_TisGameObject_t1756533147_m3064851704(__this /* static, unused */, p0, p1, p2, method) ((  GameObject_t1756533147 * (*) (RuntimeObject * /* static, unused */, GameObject_t1756533147 *, Vector3_t2243707580 , Quaternion_t4030073918 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_m3829784634_gshared)(__this /* static, unused */, p0, p1, p2, method)
// System.Void ViewPort3D::set_Model(UnityEngine.GameObject)
extern "C"  void ViewPort3D_set_Model_m1330772836 (ViewPort3D_t2082711749 * __this, GameObject_t1756533147 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject ViewPort3D::get_Model()
extern "C"  GameObject_t1756533147 * ViewPort3D_get_Model_m774326957 (ViewPort3D_t2082711749 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern "C"  int32_t LayerMask_NameToLayer_m1506372053 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C"  void GameObject_set_layer_m2712461877 (GameObject_t1756533147 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3275118058 * GameObject_get_transform_m909382139 (GameObject_t1756533147 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m4124909910 (Transform_t3275118058 * __this, Transform_t3275118058 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
#define GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632(__this, method) ((  MeshRenderer_t1268241104 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C"  Bounds_t3033363703  Renderer_get_bounds_m3832626589 (Renderer_t257310565 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t2243707580  Bounds_get_center_m129401026 (Bounds_t3033363703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t2243707580  Bounds_get_min_m2405290441 (Bounds_t3033363703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m3146764857 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Transform_TransformPoint_m3272254198 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Camera_WorldToScreenPoint_m638747266 (Camera_t189460977 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t2243707580  Bounds_get_max_m4247050707 (Bounds_t3033363703 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
#define List_1__ctor_m3854603248(__this, method) ((  void (*) (List_1_t1398341365 *, const RuntimeMethod*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::.ctor()
#define List_1__ctor_m3048104478(__this, method) ((  void (*) (List_1_t2501136733 *, const RuntimeMethod*))List_1__ctor_m3048104478_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Func`3<VoxelTemplate,System.Int32,System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>::.ctor(System.Object,System.IntPtr)
#define Func_3__ctor_m2369188720(__this, p0, p1, method) ((  void (*) (Func_3_t4100446949 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))Func_3__ctor_m119210186_gshared)(__this, p0, p1, method)
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<VoxelTemplate,System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`3<!!0,System.Int32,!!1>)
#define Enumerable_Select_TisVoxelTemplate_t594004422_TisKeyValuePair_2_t2967695803_m971871174(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_3_t4100446949 *, const RuntimeMethod*))Enumerable_Select_TisRuntimeObject_TisKeyValuePair_2_t3716250094_m1204168198_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void System.Func`2<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3619279935(__this, p0, p1, method) ((  void (*) (Func_2_t1901072402 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))Func_2__ctor_m2032212577_gshared)(__this, p0, p1, method)
// System.Linq.IOrderedEnumerable`1<!!0> System.Linq.Enumerable::OrderBy<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>,System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
#define Enumerable_OrderBy_TisKeyValuePair_2_t2967695803_TisInt32_t2071877448_m3022342042(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, Func_2_t1901072402 *, const RuntimeMethod*))Enumerable_OrderBy_TisKeyValuePair_2_t3716250094_TisInt32_t2071877448_m2585386606_gshared)(__this /* static, unused */, p0, p1, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisKeyValuePair_2_t2967695803_m3352129643(__this /* static, unused */, p0, method) ((  List_1_t2336816935 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisKeyValuePair_2_t3716250094_m1315919073_gshared)(__this /* static, unused */, p0, method)
// !0 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>::get_Item(System.Int32)
#define List_1_get_Item_m1068147322(__this, p0, method) ((  KeyValuePair_2_t2967695803  (*) (List_1_t2336816935 *, int32_t, const RuntimeMethod*))List_1_get_Item_m2794064263_gshared)(__this, p0, method)
// !0 System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m1519167095(__this, method) ((  VoxelTemplate_t594004422 * (*) (KeyValuePair_2_t2967695803 *, const RuntimeMethod*))KeyValuePair_2_get_Key_m2959920334_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (RuntimeObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !1 System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m3509700530(__this, method) ((  int32_t (*) (KeyValuePair_2_t2967695803 *, const RuntimeMethod*))KeyValuePair_2_get_Value_m1057652735_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(!0,!1)
#define KeyValuePair_2__ctor_m629383883(__this, p0, p1, method) ((  void (*) (KeyValuePair_2_t3132015601 *, int32_t, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m629383883_gshared)(__this, p0, p1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>::Add(!0)
#define List_1_Add_m3599575194(__this, p0, method) ((  void (*) (List_1_t2501136733 *, KeyValuePair_2_t3132015601 , const RuntimeMethod*))List_1_Add_m3599575194_gshared)(__this, p0, method)
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
#define List_1_Add_m4061286785(__this, p0, method) ((  void (*) (List_1_t1398341365 *, String_t*, const RuntimeMethod*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>>::get_Count()
#define List_1_get_Count_m1521282969(__this, method) ((  int32_t (*) (List_1_t2336816935 *, const RuntimeMethod*))List_1_get_Count_m4118589774_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>::.ctor(!0,!1)
#define KeyValuePair_2__ctor_m3535713998(__this, p0, p1, method) ((  void (*) (KeyValuePair_2_t2967695803 *, VoxelTemplate_t594004422 *, int32_t, const RuntimeMethod*))KeyValuePair_2__ctor_m1391475603_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelSpace::set_Width(System.Int32)
extern "C"  void VoxelSpace_set_Width_m4138915389 (VoxelSpace_t930815766 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelSpace::set_Height(System.Int32)
extern "C"  void VoxelSpace_set_Height_m3967447862 (VoxelSpace_t930815766 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelSpace::set_Depth(System.Int32)
extern "C"  void VoxelSpace_set_Depth_m1637323958 (VoxelSpace_t930815766 * __this, int32_t ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelSpace::get_Width()
extern "C"  int32_t VoxelSpace_get_Width_m4024942492 (VoxelSpace_t930815766 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelSpace::get_Height()
extern "C"  int32_t VoxelSpace_get_Height_m2390752487 (VoxelSpace_t930815766 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelSpace::get_Depth()
extern "C"  int32_t VoxelSpace_get_Depth_m4270141865 (VoxelSpace_t930815766 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 VoxelSpace::get_ScreenOffset()
extern "C"  Vector3_t2243707580  VoxelSpace_get_ScreenOffset_m994430329 (VoxelSpace_t930815766 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// VoxelStructure VoxelStructure::Create(UnityEngine.GameObject,System.Int32,System.Single,System.Single,System.Single,System.Int32,System.Int32,System.Int32,UnityEngine.Vector3,VoxelPallette,VoxelSpace)
extern "C"  VoxelStructure_t2247263099 * VoxelStructure_Create_m3231949789 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___parent0, int32_t ___index1, float ___x2, float ___y3, float ___z4, int32_t ___chunkSizeW5, int32_t ___chunkSizeH6, int32_t ___chunkSizeD7, Vector3_t2243707580  ___offset8, VoxelPallette_t4284593999 * ___palette9, VoxelSpace_t930815766 * ___space10, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelStructure::SetVoxel(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VoxelStructure_SetVoxel_m945962940 (VoxelStructure_t2247263099 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, int32_t ___newVoxel3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelStructure::GetVoxel(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t VoxelStructure_GetVoxel_m3191480667 (VoxelStructure_t2247263099 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 VoxelSpace::GetVoxel(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t VoxelSpace_GetVoxel_m2941864988 (VoxelSpace_t930815766 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelSpace::SetVoxel(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VoxelSpace_SetVoxel_m624077049 (VoxelSpace_t930815766 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, int32_t ___newVoxel3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::GetLength(System.Int32)
extern "C"  int32_t Array_GetLength_m2083296647 (RuntimeArray * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void VoxelStructure::Draw()
extern "C"  void VoxelStructure_Draw_m1865268436 (VoxelStructure_t2247263099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
extern "C"  CultureInfo_t3500843524 * CultureInfo_get_InvariantCulture_m398972276 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Int32::ToString(System.IFormatProvider)
extern "C"  String_t* Int32_ToString_m526790770 (int32_t* __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String)
extern "C"  String_t* String_Concat_m2596409543 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C"  void GameObject__ctor_m962601984 (GameObject_t1756533147 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C"  void Transform_set_parent_m3281327839 (Transform_t3275118058 * __this, Transform_t3275118058 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<VoxelStructure>()
#define GameObject_AddComponent_TisVoxelStructure_t2247263099_m1788057469(__this, method) ((  VoxelStructure_t2247263099 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2049635786_gshared)(__this, method)
// System.Void VoxelStructure::set_VoxelSpace(VoxelSpace)
extern "C"  void VoxelStructure_set_VoxelSpace_m3380696627 (VoxelStructure_t2247263099 * __this, VoxelSpace_t930815766 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
extern "C"  void Renderer_set_shadowCastingMode_m2840732934 (Renderer_t257310565 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_receiveShadows(System.Boolean)
extern "C"  void Renderer_set_receiveShadows_m2366149940 (Renderer_t257310565 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_lightProbeUsage(UnityEngine.Rendering.LightProbeUsage)
extern "C"  void Renderer_set_lightProbeUsage_m1212616902 (Renderer_t257310565 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::set_reflectionProbeUsage(UnityEngine.Rendering.ReflectionProbeUsage)
extern "C"  void Renderer_set_reflectionProbeUsage_m2405564324 (Renderer_t257310565 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshCollider>()
#define GameObject_GetComponent_TisMeshCollider_t2718867283_m4281307913(__this, method) ((  MeshCollider_t2718867283 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m2812611596_gshared)(__this, method)
// System.Void UnityEngine.MeshCollider::set_convex(System.Boolean)
extern "C"  void MeshCollider_set_convex_m2548365563 (MeshCollider_t2718867283 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Collider::set_isTrigger(System.Boolean)
extern "C"  void Collider_set_isTrigger_m1298573031 (Collider_t3497673348 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t3026937449_m1170669043(__this, method) ((  MeshFilter_t3026937449 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m4109961936_gshared)(__this, method)
// System.Void VoxelStructure::ClampDimensions()
extern "C"  void VoxelStructure_ClampDimensions_m3734676430 (VoxelStructure_t2247263099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
extern "C"  void Array_Clear_m782967417 (RuntimeObject * __this /* static, unused */, RuntimeArray * p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// VoxelTemplate VoxelStructure::GetVoxelType(System.Int32)
extern "C"  VoxelTemplate_t594004422 * VoxelStructure_GetVoxelType_m63795532 (VoxelStructure_t2247263099 * __this, int32_t ___v0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// VoxelSpace VoxelStructure::get_VoxelSpace()
extern "C"  VoxelSpace_t930815766 * VoxelStructure_get_VoxelSpace_m721980576 (VoxelStructure_t2247263099 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] VoxelStructure::GetVoxelUvs(VoxelTemplate,System.Char)
extern "C"  Vector2U5BU5D_t686124026* VoxelStructure_GetVoxelUvs_m4193062824 (VoxelStructure_t2247263099 * __this, VoxelTemplate_t594004422 * ___voxelType0, Il2CppChar ___dir1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Take_TisVector3_t2243707580_m1945563610(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t, const RuntimeMethod*))Enumerable_Take_TisVector3_t2243707580_m1945563610_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Vector3>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisVector3_t2243707580_m1930596601(__this /* static, unused */, p0, method) ((  Vector3U5BU5D_t1172311765* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisVector3_t2243707580_m1930596601_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t257310565_m772028041(__this, method) ((  Renderer_t257310565 * (*) (Component_t3819376471 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m4109961936_gshared)(__this, method)
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m1053097112 (Renderer_t257310565 * __this, Material_t193706927 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m2503577968 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Take_TisInt32_t2071877448_m1592882544(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t, const RuntimeMethod*))Enumerable_Take_TisInt32_t2071877448_m1592882544_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisInt32_t2071877448_m513246933(__this /* static, unused */, p0, method) ((  Int32U5BU5D_t3030399641* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisInt32_t2071877448_m513246933_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Take_TisVector2_t2243707579_m1080133109(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t, const RuntimeMethod*))Enumerable_Take_TisVector2_t2243707579_m1080133109_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Vector2>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisVector2_t2243707579_m1715071610(__this /* static, unused */, p0, method) ((  Vector2U5BU5D_t686124026* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisVector2_t2243707579_m1715071610_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Take<UnityEngine.Color32>(System.Collections.Generic.IEnumerable`1<!!0>,System.Int32)
#define Enumerable_Take_TisColor32_t874517518_m543186848(__this /* static, unused */, p0, p1, method) ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, int32_t, const RuntimeMethod*))Enumerable_Take_TisColor32_t874517518_m543186848_gshared)(__this /* static, unused */, p0, p1, method)
// !!0[] System.Linq.Enumerable::ToArray<UnityEngine.Color32>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisColor32_t874517518_m1885434799(__this /* static, unused */, p0, method) ((  Color32U5BU5D_t30278651* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisColor32_t874517518_m1885434799_gshared)(__this /* static, unused */, p0, method)
// UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern "C"  Mesh_t1356156583 * MeshFilter_get_sharedMesh_m1310789932 (MeshFilter_t3026937449 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Clear()
extern "C"  void Mesh_Clear_m231813403 (Mesh_t1356156583 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::.ctor()
extern "C"  void Mesh__ctor_m2975981674 (Mesh_t1356156583 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C"  void Mesh_set_vertices_m2936804213 (Mesh_t1356156583 * __this, Vector3U5BU5D_t1172311765* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C"  void Mesh_set_triangles_m3244966865 (Mesh_t1356156583 * __this, Int32U5BU5D_t3030399641* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C"  void Mesh_set_uv_m1497318906 (Mesh_t1356156583 * __this, Vector2U5BU5D_t686124026* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_colors32(UnityEngine.Color32[])
extern "C"  void Mesh_set_colors32_m1066151745 (Mesh_t1356156583 * __this, Color32U5BU5D_t30278651* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::RecalculateBounds()
extern "C"  void Mesh_RecalculateBounds_m3559909024 (Mesh_t1356156583 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::RecalculateNormals()
extern "C"  void Mesh_RecalculateNormals_m1034493793 (Mesh_t1356156583 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::RecalculateTangents()
extern "C"  void Mesh_RecalculateTangents_m2877337761 (Mesh_t1356156583 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern "C"  void MeshFilter_set_sharedMesh_m2225370173 (MeshFilter_t3026937449 * __this, Mesh_t1356156583 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Vector3i Vector3i::op_Addition(Vector3i,UnityEngine.Vector3)
extern "C"  Vector3i_t3488636705  Vector3i_op_Addition_m1178975490 (RuntimeObject * __this /* static, unused */, Vector3i_t3488636705  ___left0, Vector3_t2243707580  ___right1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Mathf_Clamp_m3542052159 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, int32_t p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2020392075  Color_get_white_m3987539815 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C"  Color32_t874517518  Color32_op_Implicit_m624191464 (RuntimeObject * __this /* static, unused */, Color_t2020392075  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m2671490429 (ScriptableObject_t1975622470 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PairStringIntDictionary::.ctor()
extern "C"  void PairStringIntDictionary__ctor_m289840511 (PairStringIntDictionary_t1323851614 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 RandomHelper::Int(System.Int32,System.Int32)
extern "C"  int32_t RandomHelper_Int_m2193030995 (RuntimeObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<PairStringInt>::GetEnumerator()
#define List_1_GetEnumerator_m1313580170(__this, method) ((  Enumerator_t3124852344  (*) (List_1_t3590122670 *, const RuntimeMethod*))List_1_GetEnumerator_m1313580170_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<PairStringInt>::get_Current()
#define Enumerator_get_Current_m784304568(__this, method) ((  PairStringInt_t4221001538  (*) (Enumerator_t3124852344 *, const RuntimeMethod*))Enumerator_get_Current_m784304568_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<PairStringInt>::MoveNext()
#define Enumerator_MoveNext_m775896506(__this, method) ((  bool (*) (Enumerator_t3124852344 *, const RuntimeMethod*))Enumerator_MoveNext_m775896506_gshared)(__this, method)
// System.Boolean PairStringIntDictionary::ContainsKey(System.String)
extern "C"  bool PairStringIntDictionary_ContainsKey_m3309051453 (PairStringIntDictionary_t1323851614 * __this, String_t* ___key0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 PairStringIntDictionary::get_Item(System.String)
extern "C"  int32_t PairStringIntDictionary_get_Item_m813766539 (PairStringIntDictionary_t1323851614 * __this, String_t* ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void PairStringIntDictionary::Set(System.String,System.Int32)
extern "C"  void PairStringIntDictionary_Set_m3793528756 (PairStringIntDictionary_t1323851614 * __this, String_t* ___key0, int32_t ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Combine_m3791207084 (RuntimeObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t3022476291 * Delegate_Remove_m2626518725 (RuntimeObject * __this /* static, unused */, Delegate_t3022476291 * p0, Delegate_t3022476291 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`2<UnityEngine.GameObject,UnityEngine.Transform>::Invoke(!0,!1)
#define Action_2_Invoke_m2336330442(__this, p0, p1, method) ((  void (*) (Action_2_t2601038636 *, GameObject_t1756533147 *, Transform_t3275118058 *, const RuntimeMethod*))Action_2_Invoke_m2406183663_gshared)(__this, p0, p1, method)
// System.Void СhaseMover::Catch()
extern "C"  void U421haseMover_Catch_m3551204615 (U421haseMover_t4119473191 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Subtraction_m2407545601 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_Normalize_m2140428981 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2243707580  Vector3_op_Multiply_m1351554733 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m1859670022 (RuntimeObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<СhaseMover>()
#define GameObject_AddComponent_TisU421haseMover_t4119473191_m1934158225(__this, method) ((  U421haseMover_t4119473191 * (*) (GameObject_t1756533147 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m2049635786_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ViewPort3D::.ctor()
extern "C"  void ViewPort3D__ctor_m685835588 (ViewPort3D_t2082711749 * __this, const RuntimeMethod* method)
{
	{
		RawImage__ctor_m527845386(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Camera ViewPort3D::get__camera()
extern "C"  Camera_t189460977 * ViewPort3D_get__camera_m3886794412 (ViewPort3D_t2082711749 * __this, const RuntimeMethod* method)
{
	{
		Camera_t189460977 * L_0 = __this->get_U3C_cameraU3Ek__BackingField_30();
		return L_0;
	}
}
// System.Void ViewPort3D::set__camera(UnityEngine.Camera)
extern "C"  void ViewPort3D_set__camera_m3900663245 (ViewPort3D_t2082711749 * __this, Camera_t189460977 * ___value0, const RuntimeMethod* method)
{
	{
		Camera_t189460977 * L_0 = ___value0;
		__this->set_U3C_cameraU3Ek__BackingField_30(L_0);
		return;
	}
}
// UnityEngine.GameObject ViewPort3D::get_Model()
extern "C"  GameObject_t1756533147 * ViewPort3D_get_Model_m774326957 (ViewPort3D_t2082711749 * __this, const RuntimeMethod* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_U3CModelU3Ek__BackingField_31();
		return L_0;
	}
}
// System.Void ViewPort3D::set_Model(UnityEngine.GameObject)
extern "C"  void ViewPort3D_set_Model_m1330772836 (ViewPort3D_t2082711749 * __this, GameObject_t1756533147 * ___value0, const RuntimeMethod* method)
{
	{
		GameObject_t1756533147 * L_0 = ___value0;
		__this->set_U3CModelU3Ek__BackingField_31(L_0);
		return;
	}
}
// System.Void ViewPort3D::Update()
extern "C"  void ViewPort3D_Update_m858759021 (ViewPort3D_t2082711749 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ViewPort3D_Update_m858759021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = ViewPort3D_get__camera_m3886794412(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Camera_t189460977 * L_2 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_RuntimeMethod_var);
		ViewPort3D_set__camera_m3900663245(__this, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		Camera_t189460977 * L_3 = ViewPort3D_get__camera_m3886794412(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Camera_Render_m2021402646(L_3, /*hidden argument*/NULL);
		Texture_t2243626319 * L_4 = RawImage_get_texture_m2258734143(__this, /*hidden argument*/NULL);
		Camera_t189460977 * L_5 = ViewPort3D_get__camera_m3886794412(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		RenderTexture_t2666733923 * L_6 = Camera_get_targetTexture_m705925974(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t2412809155_il2cpp_TypeInfo_var);
		Graphics_Blit_m2123328641(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ViewPort3D::AddViewObject(UnityEngine.GameObject)
extern "C"  void ViewPort3D_AddViewObject_m506564281 (ViewPort3D_t2082711749 * __this, GameObject_t1756533147 * ___object_0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ViewPort3D_AddViewObject_m506564281_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MeshRenderer_t1268241104 * V_0 = NULL;
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Bounds_t3033363703  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Bounds_t3033363703  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t2243707580  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Bounds_t3033363703  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Bounds_t3033363703  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t2243707580  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		GameObject_t1756533147 * L_0 = ___object_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		Camera_t189460977 * L_2 = ViewPort3D_get__camera_m3886794412(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		Camera_t189460977 * L_4 = Component_GetComponent_TisCamera_t189460977_m3276577584(__this, /*hidden argument*/Component_GetComponent_TisCamera_t189460977_m3276577584_RuntimeMethod_var);
		ViewPort3D_set__camera_m3900663245(__this, L_4, /*hidden argument*/NULL);
	}

IL_002a:
	{
		GameObject_t1756533147 * L_5 = ___object_0;
		Camera_t189460977 * L_6 = ViewPort3D_get__camera_m3886794412(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t4030073918_il2cpp_TypeInfo_var);
		Quaternion_t4030073918  L_9 = Quaternion_get_identity_m1561886418(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_10 = Object_Instantiate_TisGameObject_t1756533147_m3064851704(NULL /*static, unused*/, L_5, L_8, L_9, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3064851704_RuntimeMethod_var);
		ViewPort3D_set_Model_m1330772836(__this, L_10, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_11 = ViewPort3D_get_Model_m774326957(__this, /*hidden argument*/NULL);
		int32_t L_12 = LayerMask_NameToLayer_m1506372053(NULL /*static, unused*/, _stringLiteral3495770132, /*hidden argument*/NULL);
		NullCheck(L_11);
		GameObject_set_layer_m2712461877(L_11, L_12, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_13 = ViewPort3D_get_Model_m774326957(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Transform_t3275118058 * L_14 = GameObject_get_transform_m909382139(L_13, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_15 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_t3275118058 * L_16 = GameObject_get_transform_m909382139(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_SetParent_m4124909910(L_14, L_16, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_17 = ___object_0;
		NullCheck(L_17);
		MeshRenderer_t1268241104 * L_18 = GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632(L_17, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632_RuntimeMethod_var);
		V_0 = L_18;
	}

IL_0082:
	{
		Camera_t189460977 * L_19 = ViewPort3D_get__camera_m3886794412(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = ViewPort3D_get_Model_m774326957(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_t3275118058 * L_21 = GameObject_get_transform_m909382139(L_20, /*hidden argument*/NULL);
		MeshRenderer_t1268241104 * L_22 = V_0;
		NullCheck(L_22);
		Bounds_t3033363703  L_23 = Renderer_get_bounds_m3832626589(L_22, /*hidden argument*/NULL);
		V_2 = L_23;
		Vector3_t2243707580  L_24 = Bounds_get_center_m129401026((&V_2), /*hidden argument*/NULL);
		MeshRenderer_t1268241104 * L_25 = V_0;
		NullCheck(L_25);
		Bounds_t3033363703  L_26 = Renderer_get_bounds_m3832626589(L_25, /*hidden argument*/NULL);
		V_3 = L_26;
		Vector3_t2243707580  L_27 = Bounds_get_min_m2405290441((&V_3), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_28 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_24, L_27, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t2243707580  L_29 = Transform_TransformPoint_m3272254198(L_21, L_28, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t2243707580  L_30 = Camera_WorldToScreenPoint_m638747266(L_19, L_29, /*hidden argument*/NULL);
		V_1 = L_30;
		Camera_t189460977 * L_31 = ViewPort3D_get__camera_m3886794412(__this, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_32 = ViewPort3D_get_Model_m774326957(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		Transform_t3275118058 * L_33 = GameObject_get_transform_m909382139(L_32, /*hidden argument*/NULL);
		MeshRenderer_t1268241104 * L_34 = V_0;
		NullCheck(L_34);
		Bounds_t3033363703  L_35 = Renderer_get_bounds_m3832626589(L_34, /*hidden argument*/NULL);
		V_5 = L_35;
		Vector3_t2243707580  L_36 = Bounds_get_center_m129401026((&V_5), /*hidden argument*/NULL);
		MeshRenderer_t1268241104 * L_37 = V_0;
		NullCheck(L_37);
		Bounds_t3033363703  L_38 = Renderer_get_bounds_m3832626589(L_37, /*hidden argument*/NULL);
		V_6 = L_38;
		Vector3_t2243707580  L_39 = Bounds_get_max_m4247050707((&V_6), /*hidden argument*/NULL);
		Vector3_t2243707580  L_40 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_36, L_39, /*hidden argument*/NULL);
		NullCheck(L_33);
		Vector3_t2243707580  L_41 = Transform_TransformPoint_m3272254198(L_33, L_40, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t2243707580  L_42 = Camera_WorldToScreenPoint_m638747266(L_31, L_41, /*hidden argument*/NULL);
		V_4 = L_42;
		float L_43 = (&V_1)->get_x_1();
		if ((((float)L_43) < ((float)(0.0f))))
		{
			goto IL_0165;
		}
	}
	{
		float L_44 = (&V_4)->get_x_1();
		if ((((float)L_44) < ((float)(0.0f))))
		{
			goto IL_0165;
		}
	}
	{
		float L_45 = (&V_1)->get_y_2();
		if ((((float)L_45) < ((float)(0.0f))))
		{
			goto IL_0165;
		}
	}
	{
		float L_46 = (&V_4)->get_y_2();
		if ((((float)L_46) < ((float)(0.0f))))
		{
			goto IL_0165;
		}
	}
	{
		float L_47 = (&V_1)->get_z_3();
		if ((((float)L_47) <= ((float)(0.0f))))
		{
			goto IL_0165;
		}
	}
	{
		float L_48 = (&V_4)->get_z_3();
		if ((!(((float)L_48) <= ((float)(0.0f)))))
		{
			goto IL_01a1;
		}
	}

IL_0165:
	{
		GameObject_t1756533147 * L_49 = ViewPort3D_get_Model_m774326957(__this, /*hidden argument*/NULL);
		NullCheck(L_49);
		Transform_t3275118058 * L_50 = GameObject_get_transform_m909382139(L_49, /*hidden argument*/NULL);
		NullCheck(L_50);
		Vector3_t2243707580  L_51 = Transform_get_position_m1104419803(L_50, /*hidden argument*/NULL);
		V_7 = L_51;
		Vector3_t2243707580 * L_52 = (&V_7);
		float L_53 = L_52->get_z_3();
		L_52->set_z_3(((float)((float)L_53+(float)(0.1f))));
		GameObject_t1756533147 * L_54 = ViewPort3D_get_Model_m774326957(__this, /*hidden argument*/NULL);
		NullCheck(L_54);
		Transform_t3275118058 * L_55 = GameObject_get_transform_m909382139(L_54, /*hidden argument*/NULL);
		Vector3_t2243707580  L_56 = V_7;
		NullCheck(L_55);
		Transform_set_position_m2469242620(L_55, L_56, /*hidden argument*/NULL);
		goto IL_0082;
	}

IL_01a1:
	{
		goto IL_01ab;
	}
	// Dead block : IL_01a6: br IL_0082

IL_01ab:
	{
		return;
	}
}
// System.Void ViewPort3D::Destroy()
extern "C"  void ViewPort3D_Destroy_m3568382482 (ViewPort3D_t2082711749 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ViewPort3D_Destroy_m3568382482_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Camera_t189460977 * L_0 = ViewPort3D_get__camera_m3886794412(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = ViewPort3D_get_Model_m774326957(__this, /*hidden argument*/NULL);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelPallette::.ctor()
extern "C"  void VoxelPallette__ctor_m431966492 (VoxelPallette_t4284593999 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelPallette__ctor_m431966492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_voxelTemplates_1(((VoxelTemplateU5BU5D_t2866079843*)SZArrayNew(VoxelTemplateU5BU5D_t2866079843_il2cpp_TypeInfo_var, (uint32_t)1)));
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_RuntimeMethod_var);
		__this->set_voxelNames_2(L_0);
		List_1_t2501136733 * L_1 = (List_1_t2501136733 *)il2cpp_codegen_object_new(List_1_t2501136733_il2cpp_TypeInfo_var);
		List_1__ctor_m3048104478(L_1, /*hidden argument*/List_1__ctor_m3048104478_RuntimeMethod_var);
		__this->set__lookup_3(L_1);
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VoxelPallette::ArrangeVoxelTemplates()
extern "C"  void VoxelPallette_ArrangeVoxelTemplates_m3016297437 (VoxelPallette_t4284593999 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelPallette_ArrangeVoxelTemplates_m3016297437_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t2336816935 * V_0 = NULL;
	int32_t V_1 = 0;
	KeyValuePair_2_t2967695803  V_2;
	memset(&V_2, 0, sizeof(V_2));
	KeyValuePair_2_t2967695803  V_3;
	memset(&V_3, 0, sizeof(V_3));
	KeyValuePair_2_t2967695803  V_4;
	memset(&V_4, 0, sizeof(V_4));
	VoxelTemplateU5BU5D_t2866079843* G_B2_0 = NULL;
	VoxelTemplateU5BU5D_t2866079843* G_B1_0 = NULL;
	RuntimeObject* G_B4_0 = NULL;
	RuntimeObject* G_B3_0 = NULL;
	{
		List_1_t1398341365 * L_0 = (List_1_t1398341365 *)il2cpp_codegen_object_new(List_1_t1398341365_il2cpp_TypeInfo_var);
		List_1__ctor_m3854603248(L_0, /*hidden argument*/List_1__ctor_m3854603248_RuntimeMethod_var);
		__this->set_voxelNames_2(L_0);
		VoxelTemplateU5BU5D_t2866079843* L_1 = __this->get_voxelTemplates_1();
		Func_3_t4100446949 * L_2 = ((VoxelPallette_t4284593999_StaticFields*)il2cpp_codegen_static_fields_for(VoxelPallette_t4284593999_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_4();
		G_B1_0 = L_1;
		if (L_2)
		{
			G_B2_0 = L_1;
			goto IL_0029;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)VoxelPallette_U3CArrangeVoxelTemplatesU3Em__0_m1642453984_RuntimeMethod_var);
		Func_3_t4100446949 * L_4 = (Func_3_t4100446949 *)il2cpp_codegen_object_new(Func_3_t4100446949_il2cpp_TypeInfo_var);
		Func_3__ctor_m2369188720(L_4, NULL, L_3, /*hidden argument*/Func_3__ctor_m2369188720_RuntimeMethod_var);
		((VoxelPallette_t4284593999_StaticFields*)il2cpp_codegen_static_fields_for(VoxelPallette_t4284593999_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache0_4(L_4);
		G_B2_0 = G_B1_0;
	}

IL_0029:
	{
		Func_3_t4100446949 * L_5 = ((VoxelPallette_t4284593999_StaticFields*)il2cpp_codegen_static_fields_for(VoxelPallette_t4284593999_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache0_4();
		RuntimeObject* L_6 = Enumerable_Select_TisVoxelTemplate_t594004422_TisKeyValuePair_2_t2967695803_m971871174(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)G_B2_0, L_5, /*hidden argument*/Enumerable_Select_TisVoxelTemplate_t594004422_TisKeyValuePair_2_t2967695803_m971871174_RuntimeMethod_var);
		Func_2_t1901072402 * L_7 = ((VoxelPallette_t4284593999_StaticFields*)il2cpp_codegen_static_fields_for(VoxelPallette_t4284593999_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_5();
		G_B3_0 = L_6;
		if (L_7)
		{
			G_B4_0 = L_6;
			goto IL_004b;
		}
	}
	{
		IntPtr_t L_8;
		L_8.set_m_value_0((void*)(void*)VoxelPallette_U3CArrangeVoxelTemplatesU3Em__1_m925405037_RuntimeMethod_var);
		Func_2_t1901072402 * L_9 = (Func_2_t1901072402 *)il2cpp_codegen_object_new(Func_2_t1901072402_il2cpp_TypeInfo_var);
		Func_2__ctor_m3619279935(L_9, NULL, L_8, /*hidden argument*/Func_2__ctor_m3619279935_RuntimeMethod_var);
		((VoxelPallette_t4284593999_StaticFields*)il2cpp_codegen_static_fields_for(VoxelPallette_t4284593999_il2cpp_TypeInfo_var))->set_U3CU3Ef__amU24cache1_5(L_9);
		G_B4_0 = G_B3_0;
	}

IL_004b:
	{
		Func_2_t1901072402 * L_10 = ((VoxelPallette_t4284593999_StaticFields*)il2cpp_codegen_static_fields_for(VoxelPallette_t4284593999_il2cpp_TypeInfo_var))->get_U3CU3Ef__amU24cache1_5();
		RuntimeObject* L_11 = Enumerable_OrderBy_TisKeyValuePair_2_t2967695803_TisInt32_t2071877448_m3022342042(NULL /*static, unused*/, G_B4_0, L_10, /*hidden argument*/Enumerable_OrderBy_TisKeyValuePair_2_t2967695803_TisInt32_t2071877448_m3022342042_RuntimeMethod_var);
		List_1_t2336816935 * L_12 = Enumerable_ToList_TisKeyValuePair_2_t2967695803_m3352129643(NULL /*static, unused*/, L_11, /*hidden argument*/Enumerable_ToList_TisKeyValuePair_2_t2967695803_m3352129643_RuntimeMethod_var);
		V_0 = L_12;
		List_1_t2501136733 * L_13 = (List_1_t2501136733 *)il2cpp_codegen_object_new(List_1_t2501136733_il2cpp_TypeInfo_var);
		List_1__ctor_m3048104478(L_13, /*hidden argument*/List_1__ctor_m3048104478_RuntimeMethod_var);
		__this->set__lookup_3(L_13);
		V_1 = 0;
		goto IL_00cb;
	}

IL_006d:
	{
		List_1_t2336816935 * L_14 = V_0;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		KeyValuePair_2_t2967695803  L_16 = List_1_get_Item_m1068147322(L_14, L_15, /*hidden argument*/List_1_get_Item_m1068147322_RuntimeMethod_var);
		V_2 = L_16;
		VoxelTemplate_t594004422 * L_17 = KeyValuePair_2_get_Key_m1519167095((&V_2), /*hidden argument*/KeyValuePair_2_get_Key_m1519167095_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_17, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00c7;
		}
	}
	{
		List_1_t2501136733 * L_19 = __this->get__lookup_3();
		int32_t L_20 = V_1;
		List_1_t2336816935 * L_21 = V_0;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		KeyValuePair_2_t2967695803  L_23 = List_1_get_Item_m1068147322(L_21, L_22, /*hidden argument*/List_1_get_Item_m1068147322_RuntimeMethod_var);
		V_3 = L_23;
		int32_t L_24 = KeyValuePair_2_get_Value_m3509700530((&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m3509700530_RuntimeMethod_var);
		KeyValuePair_2_t3132015601  L_25;
		memset(&L_25, 0, sizeof(L_25));
		KeyValuePair_2__ctor_m629383883((&L_25), L_20, L_24, /*hidden argument*/KeyValuePair_2__ctor_m629383883_RuntimeMethod_var);
		NullCheck(L_19);
		List_1_Add_m3599575194(L_19, L_25, /*hidden argument*/List_1_Add_m3599575194_RuntimeMethod_var);
		List_1_t1398341365 * L_26 = __this->get_voxelNames_2();
		List_1_t2336816935 * L_27 = V_0;
		int32_t L_28 = V_1;
		NullCheck(L_27);
		KeyValuePair_2_t2967695803  L_29 = List_1_get_Item_m1068147322(L_27, L_28, /*hidden argument*/List_1_get_Item_m1068147322_RuntimeMethod_var);
		V_4 = L_29;
		VoxelTemplate_t594004422 * L_30 = KeyValuePair_2_get_Key_m1519167095((&V_4), /*hidden argument*/KeyValuePair_2_get_Key_m1519167095_RuntimeMethod_var);
		NullCheck(L_30);
		String_t* L_31 = Object_get_name_m2079638459(L_30, /*hidden argument*/NULL);
		NullCheck(L_26);
		List_1_Add_m4061286785(L_26, L_31, /*hidden argument*/List_1_Add_m4061286785_RuntimeMethod_var);
	}

IL_00c7:
	{
		int32_t L_32 = V_1;
		V_1 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00cb:
	{
		int32_t L_33 = V_1;
		List_1_t2336816935 * L_34 = V_0;
		NullCheck(L_34);
		int32_t L_35 = List_1_get_Count_m1521282969(L_34, /*hidden argument*/List_1_get_Count_m1521282969_RuntimeMethod_var);
		if ((((int32_t)L_33) < ((int32_t)L_35)))
		{
			goto IL_006d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32> VoxelPallette::<ArrangeVoxelTemplates>m__0(VoxelTemplate,System.Int32)
extern "C"  KeyValuePair_2_t2967695803  VoxelPallette_U3CArrangeVoxelTemplatesU3Em__0_m1642453984 (RuntimeObject * __this /* static, unused */, VoxelTemplate_t594004422 * ___x0, int32_t ___i1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelPallette_U3CArrangeVoxelTemplatesU3Em__0_m1642453984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VoxelTemplate_t594004422 * L_0 = ___x0;
		int32_t L_1 = ___i1;
		KeyValuePair_2_t2967695803  L_2;
		memset(&L_2, 0, sizeof(L_2));
		KeyValuePair_2__ctor_m3535713998((&L_2), L_0, L_1, /*hidden argument*/KeyValuePair_2__ctor_m3535713998_RuntimeMethod_var);
		return L_2;
	}
}
// System.Int32 VoxelPallette::<ArrangeVoxelTemplates>m__1(System.Collections.Generic.KeyValuePair`2<VoxelTemplate,System.Int32>)
extern "C"  int32_t VoxelPallette_U3CArrangeVoxelTemplatesU3Em__1_m925405037 (RuntimeObject * __this /* static, unused */, KeyValuePair_2_t2967695803  ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelPallette_U3CArrangeVoxelTemplatesU3Em__1_m925405037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VoxelTemplate_t594004422 * L_0 = KeyValuePair_2_get_Key_m1519167095((&___x0), /*hidden argument*/KeyValuePair_2_get_Key_m1519167095_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		VoxelTemplate_t594004422 * L_2 = KeyValuePair_2_get_Key_m1519167095((&___x0), /*hidden argument*/KeyValuePair_2_get_Key_m1519167095_RuntimeMethod_var);
		NullCheck(L_2);
		int32_t L_3 = L_2->get_DisplayOrder_2();
		return L_3;
	}

IL_001f:
	{
		return ((int32_t)100000);
	}
}
// System.Void VoxelSpace::.ctor()
extern "C"  void VoxelSpace__ctor_m4017741739 (VoxelSpace_t930815766 * __this, const RuntimeMethod* method)
{
	{
		__this->set_DrawFront_12((bool)1);
		__this->set_DrawBack_13((bool)1);
		__this->set_DrawTop_14((bool)1);
		__this->set_DrawBottom_15((bool)1);
		__this->set_DrawLeft_16((bool)1);
		__this->set_DrawRight_17((bool)1);
		__this->set_UsePalletteTexture_19((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 VoxelSpace::get_Width()
extern "C"  int32_t VoxelSpace_get_Width_m4024942492 (VoxelSpace_t930815766 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CWidthU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Void VoxelSpace::set_Width(System.Int32)
extern "C"  void VoxelSpace_set_Width_m4138915389 (VoxelSpace_t930815766 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CWidthU3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Int32 VoxelSpace::get_Height()
extern "C"  int32_t VoxelSpace_get_Height_m2390752487 (VoxelSpace_t930815766 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CHeightU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void VoxelSpace::set_Height(System.Int32)
extern "C"  void VoxelSpace_set_Height_m3967447862 (VoxelSpace_t930815766 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CHeightU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 VoxelSpace::get_Depth()
extern "C"  int32_t VoxelSpace_get_Depth_m4270141865 (VoxelSpace_t930815766 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CDepthU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void VoxelSpace::set_Depth(System.Int32)
extern "C"  void VoxelSpace_set_Depth_m1637323958 (VoxelSpace_t930815766 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CDepthU3Ek__BackingField_4(L_0);
		return;
	}
}
// UnityEngine.Vector3 VoxelSpace::get_ScreenOffset()
extern "C"  Vector3_t2243707580  VoxelSpace_get_ScreenOffset_m994430329 (VoxelSpace_t930815766 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__chunkSizeW_5();
		int32_t L_1 = __this->get__chunkCountW_8();
		int32_t L_2 = __this->get__chunkSizeH_6();
		int32_t L_3 = __this->get__chunkCountH_9();
		int32_t L_4 = __this->get__chunkSizeD_7();
		int32_t L_5 = __this->get__chunkCountD_10();
		Vector3_t2243707580  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m2638739322((&L_6), ((-((float)((float)(((float)((float)((int32_t)((int32_t)L_0*(int32_t)L_1)))))*(float)(0.5f))))), ((-((float)((float)(((float)((float)((int32_t)((int32_t)L_2*(int32_t)L_3)))))*(float)(0.5f))))), ((-((float)((float)(((float)((float)((int32_t)((int32_t)L_4*(int32_t)L_5)))))*(float)(0.5f))))), /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void VoxelSpace::Create(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,VoxelPallette)
extern "C"  void VoxelSpace_Create_m1052405448 (VoxelSpace_t930815766 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, int32_t ___chunkW3, int32_t ___chunkH4, int32_t ___chunkD5, VoxelPallette_t4284593999 * ___palette6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelSpace_Create_m1052405448_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	VoxelSpace_t930815766 * G_B2_0 = NULL;
	VoxelSpace_t930815766 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	VoxelSpace_t930815766 * G_B3_1 = NULL;
	VoxelSpace_t930815766 * G_B5_0 = NULL;
	VoxelSpace_t930815766 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	VoxelSpace_t930815766 * G_B6_1 = NULL;
	VoxelSpace_t930815766 * G_B8_0 = NULL;
	VoxelSpace_t930815766 * G_B7_0 = NULL;
	int32_t G_B9_0 = 0;
	VoxelSpace_t930815766 * G_B9_1 = NULL;
	{
		int32_t L_0 = ___width0;
		VoxelSpace_set_Width_m4138915389(__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height1;
		VoxelSpace_set_Height_m3967447862(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___depth2;
		VoxelSpace_set_Depth_m1637323958(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___chunkW3;
		int32_t L_4 = VoxelSpace_get_Width_m4024942492(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			G_B2_0 = __this;
			goto IL_002e;
		}
	}
	{
		int32_t L_5 = VoxelSpace_get_Width_m4024942492(__this, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		goto IL_0030;
	}

IL_002e:
	{
		int32_t L_6 = ___chunkW3;
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
	}

IL_0030:
	{
		NullCheck(G_B3_1);
		G_B3_1->set__chunkSizeW_5(G_B3_0);
		int32_t L_7 = ___chunkH4;
		int32_t L_8 = VoxelSpace_get_Height_m2390752487(__this, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if ((((int32_t)L_7) <= ((int32_t)L_8)))
		{
			G_B5_0 = __this;
			goto IL_004e;
		}
	}
	{
		int32_t L_9 = VoxelSpace_get_Height_m2390752487(__this, /*hidden argument*/NULL);
		G_B6_0 = L_9;
		G_B6_1 = G_B4_0;
		goto IL_0050;
	}

IL_004e:
	{
		int32_t L_10 = ___chunkH4;
		G_B6_0 = L_10;
		G_B6_1 = G_B5_0;
	}

IL_0050:
	{
		NullCheck(G_B6_1);
		G_B6_1->set__chunkSizeH_6(G_B6_0);
		int32_t L_11 = ___chunkD5;
		int32_t L_12 = VoxelSpace_get_Depth_m4270141865(__this, /*hidden argument*/NULL);
		G_B7_0 = __this;
		if ((((int32_t)L_11) <= ((int32_t)L_12)))
		{
			G_B8_0 = __this;
			goto IL_006e;
		}
	}
	{
		int32_t L_13 = VoxelSpace_get_Depth_m4270141865(__this, /*hidden argument*/NULL);
		G_B9_0 = L_13;
		G_B9_1 = G_B7_0;
		goto IL_0070;
	}

IL_006e:
	{
		int32_t L_14 = ___chunkD5;
		G_B9_0 = L_14;
		G_B9_1 = G_B8_0;
	}

IL_0070:
	{
		NullCheck(G_B9_1);
		G_B9_1->set__chunkSizeD_7(G_B9_0);
		int32_t L_15 = VoxelSpace_get_Width_m4024942492(__this, /*hidden argument*/NULL);
		int32_t L_16 = __this->get__chunkSizeW_5();
		__this->set__chunkCountW_8(((int32_t)((int32_t)L_15/(int32_t)L_16)));
		int32_t L_17 = VoxelSpace_get_Height_m2390752487(__this, /*hidden argument*/NULL);
		int32_t L_18 = __this->get__chunkSizeH_6();
		__this->set__chunkCountH_9(((int32_t)((int32_t)L_17/(int32_t)L_18)));
		int32_t L_19 = VoxelSpace_get_Depth_m4270141865(__this, /*hidden argument*/NULL);
		int32_t L_20 = __this->get__chunkSizeD_7();
		__this->set__chunkCountD_10(((int32_t)((int32_t)L_19/(int32_t)L_20)));
		int32_t L_21 = __this->get__chunkCountW_8();
		int32_t L_22 = __this->get__chunkCountH_9();
		int32_t L_23 = __this->get__chunkCountD_10();
		il2cpp_array_size_t L_25[] = { (il2cpp_array_size_t)L_21, (il2cpp_array_size_t)L_22, (il2cpp_array_size_t)L_23 };
		VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* L_24 = (VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)GenArrayNew(VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636_il2cpp_TypeInfo_var, L_25);
		__this->set__chunks_11((VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)L_24);
		V_0 = 0;
		int32_t L_26 = __this->get__chunkCountH_9();
		V_1 = ((int32_t)((int32_t)L_26-(int32_t)1));
		goto IL_014c;
	}

IL_00db:
	{
		V_2 = 0;
		goto IL_013c;
	}

IL_00e2:
	{
		V_3 = 0;
		goto IL_012c;
	}

IL_00e9:
	{
		VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* L_27 = __this->get__chunks_11();
		int32_t L_28 = V_3;
		int32_t L_29 = V_1;
		int32_t L_30 = V_2;
		GameObject_t1756533147 * L_31 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		int32_t L_32 = V_0;
		int32_t L_33 = V_3;
		int32_t L_34 = V_1;
		int32_t L_35 = V_2;
		int32_t L_36 = __this->get__chunkSizeW_5();
		int32_t L_37 = __this->get__chunkSizeH_6();
		int32_t L_38 = __this->get__chunkSizeD_7();
		Vector3_t2243707580  L_39 = VoxelSpace_get_ScreenOffset_m994430329(__this, /*hidden argument*/NULL);
		VoxelPallette_t4284593999 * L_40 = ___palette6;
		IL2CPP_RUNTIME_CLASS_INIT(VoxelStructure_t2247263099_il2cpp_TypeInfo_var);
		VoxelStructure_t2247263099 * L_41 = VoxelStructure_Create_m3231949789(NULL /*static, unused*/, L_31, L_32, (((float)((float)L_33))), (((float)((float)L_34))), (((float)((float)L_35))), L_36, L_37, L_38, L_39, L_40, __this, /*hidden argument*/NULL);
		NullCheck((VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)(VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)L_27);
		((VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)(VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)L_27)->SetAt(L_28, L_29, L_30, L_41);
		int32_t L_42 = V_0;
		V_0 = ((int32_t)((int32_t)L_42+(int32_t)1));
		int32_t L_43 = V_3;
		V_3 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_012c:
	{
		int32_t L_44 = V_3;
		int32_t L_45 = __this->get__chunkCountW_8();
		if ((((int32_t)L_44) < ((int32_t)L_45)))
		{
			goto IL_00e9;
		}
	}
	{
		int32_t L_46 = V_2;
		V_2 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_013c:
	{
		int32_t L_47 = V_2;
		int32_t L_48 = __this->get__chunkCountD_10();
		if ((((int32_t)L_47) < ((int32_t)L_48)))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_49 = V_1;
		V_1 = ((int32_t)((int32_t)L_49-(int32_t)1));
	}

IL_014c:
	{
		int32_t L_50 = V_1;
		if ((((int32_t)L_50) >= ((int32_t)0)))
		{
			goto IL_00db;
		}
	}
	{
		return;
	}
}
// System.Void VoxelSpace::SetVoxel(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VoxelSpace_SetVoxel_m624077049 (VoxelSpace_t930815766 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, int32_t ___newVoxel3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelSpace_SetVoxel_m624077049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	VoxelStructure_t2247263099 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		int32_t L_0 = VoxelSpace_get_Height_m2390752487(__this, /*hidden argument*/NULL);
		int32_t L_1 = ___y1;
		___y1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_0-(int32_t)L_1))-(int32_t)1));
		int32_t L_2 = ___x0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_3 = ___y1;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_4 = ___z2;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_5 = ___x0;
		int32_t L_6 = VoxelSpace_get_Width_m4024942492(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) >= ((int32_t)L_6)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_7 = ___y1;
		int32_t L_8 = VoxelSpace_get_Height_m2390752487(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_7) >= ((int32_t)L_8)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_9 = ___z2;
		int32_t L_10 = VoxelSpace_get_Depth_m4270141865(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0046;
		}
	}

IL_0045:
	{
		return;
	}

IL_0046:
	{
		int32_t L_11 = ___x0;
		int32_t L_12 = __this->get__chunkSizeW_5();
		V_0 = ((int32_t)((int32_t)L_11/(int32_t)L_12));
		int32_t L_13 = ___y1;
		int32_t L_14 = __this->get__chunkSizeH_6();
		V_1 = ((int32_t)((int32_t)L_13/(int32_t)L_14));
		int32_t L_15 = ___z2;
		int32_t L_16 = __this->get__chunkSizeD_7();
		V_2 = ((int32_t)((int32_t)L_15/(int32_t)L_16));
		VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* L_17 = __this->get__chunks_11();
		int32_t L_18 = V_0;
		int32_t L_19 = V_1;
		int32_t L_20 = V_2;
		NullCheck((VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)(VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)L_17);
		VoxelStructure_t2247263099 * L_21 = ((VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)(VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)L_17)->GetAt(L_18, L_19, L_20);
		V_3 = L_21;
		VoxelStructure_t2247263099 * L_22 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_23 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_22, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		int32_t L_24 = ___x0;
		int32_t L_25 = __this->get__chunkSizeW_5();
		V_4 = ((int32_t)((int32_t)L_24%(int32_t)L_25));
		int32_t L_26 = ___y1;
		int32_t L_27 = __this->get__chunkSizeH_6();
		V_5 = ((int32_t)((int32_t)L_26%(int32_t)L_27));
		int32_t L_28 = ___z2;
		int32_t L_29 = __this->get__chunkSizeD_7();
		V_6 = ((int32_t)((int32_t)L_28%(int32_t)L_29));
		VoxelStructure_t2247263099 * L_30 = V_3;
		int32_t L_31 = V_4;
		int32_t L_32 = V_5;
		int32_t L_33 = V_6;
		int32_t L_34 = ___newVoxel3;
		NullCheck(L_30);
		VoxelStructure_SetVoxel_m945962940(L_30, L_31, L_32, L_33, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 VoxelSpace::GetVoxel(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t VoxelSpace_GetVoxel_m2941864988 (VoxelSpace_t930815766 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelSpace_GetVoxel_m2941864988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	VoxelStructure_t2247263099 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		int32_t L_0 = ___x0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_1 = ___y1;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_2 = ___z2;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = ___x0;
		int32_t L_4 = VoxelSpace_get_Width_m4024942492(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_3) > ((int32_t)L_4)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_5 = ___y1;
		int32_t L_6 = VoxelSpace_get_Height_m2390752487(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_5) > ((int32_t)L_6)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_7 = ___z2;
		int32_t L_8 = VoxelSpace_get_Depth_m4270141865(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)L_8)))
		{
			goto IL_003b;
		}
	}

IL_0039:
	{
		return (-1);
	}

IL_003b:
	{
		int32_t L_9 = ___x0;
		int32_t L_10 = __this->get__chunkSizeW_5();
		V_0 = ((int32_t)((int32_t)L_9/(int32_t)L_10));
		int32_t L_11 = ___y1;
		int32_t L_12 = __this->get__chunkSizeH_6();
		V_1 = ((int32_t)((int32_t)L_11/(int32_t)L_12));
		int32_t L_13 = ___z2;
		int32_t L_14 = __this->get__chunkSizeD_7();
		V_2 = ((int32_t)((int32_t)L_13/(int32_t)L_14));
		VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* L_15 = __this->get__chunks_11();
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		int32_t L_18 = V_2;
		NullCheck((VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)(VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)L_15);
		VoxelStructure_t2247263099 * L_19 = ((VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)(VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)L_15)->GetAt(L_16, L_17, L_18);
		V_3 = L_19;
		VoxelStructure_t2247263099 * L_20 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_21 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_20, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0073;
		}
	}
	{
		return (-1);
	}

IL_0073:
	{
		int32_t L_22 = ___x0;
		int32_t L_23 = __this->get__chunkSizeW_5();
		V_4 = ((int32_t)((int32_t)L_22%(int32_t)L_23));
		int32_t L_24 = ___y1;
		int32_t L_25 = __this->get__chunkSizeH_6();
		V_5 = ((int32_t)((int32_t)L_24%(int32_t)L_25));
		int32_t L_26 = ___z2;
		int32_t L_27 = __this->get__chunkSizeD_7();
		V_6 = ((int32_t)((int32_t)L_26%(int32_t)L_27));
		VoxelStructure_t2247263099 * L_28 = V_3;
		int32_t L_29 = V_4;
		int32_t L_30 = V_5;
		int32_t L_31 = V_6;
		NullCheck(L_28);
		int32_t L_32 = VoxelStructure_GetVoxel_m3191480667(L_28, L_29, L_30, L_31, /*hidden argument*/NULL);
		return L_32;
	}
}
// System.Void VoxelSpace::ClearIfLess(System.Int32)
extern "C"  void VoxelSpace_ClearIfLess_m2135458409 (VoxelSpace_t930815766 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		goto IL_004c;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_003c;
	}

IL_000e:
	{
		int32_t L_0 = V_1;
		int32_t L_1 = ___value0;
		int32_t L_2 = V_0;
		int32_t L_3 = VoxelSpace_GetVoxel_m2941864988(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0038;
		}
	}
	{
		V_2 = 0;
		goto IL_0031;
	}

IL_0023:
	{
		int32_t L_4 = V_1;
		int32_t L_5 = V_2;
		int32_t L_6 = V_0;
		VoxelSpace_SetVoxel_m624077049(__this, L_4, L_5, L_6, 0, /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_8 = V_2;
		int32_t L_9 = ___value0;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0023;
		}
	}

IL_0038:
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = VoxelSpace_get_Width_m4024942492(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004c:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = VoxelSpace_get_Depth_m4270141865(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VoxelSpace::Border(System.Int32)
extern "C"  void VoxelSpace_Border_m1296076866 (VoxelSpace_t930815766 * __this, int32_t ___size0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_0 = 0;
		goto IL_0051;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_0046;
	}

IL_000e:
	{
		V_2 = 1;
		goto IL_0036;
	}

IL_0015:
	{
		int32_t L_0 = V_1;
		int32_t L_1 = V_2;
		int32_t L_2 = V_0;
		VoxelSpace_SetVoxel_m624077049(__this, L_0, L_1, L_2, 0, /*hidden argument*/NULL);
		int32_t L_3 = VoxelSpace_get_Width_m4024942492(__this, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		int32_t L_5 = V_2;
		int32_t L_6 = V_0;
		VoxelSpace_SetVoxel_m624077049(__this, ((int32_t)((int32_t)((int32_t)((int32_t)L_3-(int32_t)L_4))-(int32_t)1)), L_5, L_6, 0, /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0036:
	{
		int32_t L_8 = V_2;
		int32_t L_9 = VoxelSpace_get_Height_m2390752487(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = ___size0;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_14 = V_0;
		int32_t L_15 = VoxelSpace_get_Depth_m4270141865(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_0007;
		}
	}
	{
		V_3 = 0;
		goto IL_00ba;
	}

IL_0064:
	{
		V_4 = 0;
		goto IL_00ae;
	}

IL_006c:
	{
		V_5 = 1;
		goto IL_009b;
	}

IL_0074:
	{
		int32_t L_16 = V_3;
		int32_t L_17 = V_5;
		int32_t L_18 = V_4;
		VoxelSpace_SetVoxel_m624077049(__this, L_16, L_17, L_18, 0, /*hidden argument*/NULL);
		int32_t L_19 = V_3;
		int32_t L_20 = V_5;
		int32_t L_21 = VoxelSpace_get_Depth_m4270141865(__this, /*hidden argument*/NULL);
		int32_t L_22 = V_4;
		VoxelSpace_SetVoxel_m624077049(__this, L_19, L_20, ((int32_t)((int32_t)((int32_t)((int32_t)L_21-(int32_t)L_22))-(int32_t)1)), 0, /*hidden argument*/NULL);
		int32_t L_23 = V_5;
		V_5 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_009b:
	{
		int32_t L_24 = V_5;
		int32_t L_25 = VoxelSpace_get_Height_m2390752487(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_0074;
		}
	}
	{
		int32_t L_26 = V_4;
		V_4 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00ae:
	{
		int32_t L_27 = V_4;
		int32_t L_28 = ___size0;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_006c;
		}
	}
	{
		int32_t L_29 = V_3;
		V_3 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00ba:
	{
		int32_t L_30 = V_3;
		int32_t L_31 = VoxelSpace_get_Width_m4024942492(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_0064;
		}
	}
	{
		return;
	}
}
// System.Void VoxelSpace::Draw()
extern "C"  void VoxelSpace_Draw_m2141102255 (VoxelSpace_t930815766 * __this, const RuntimeMethod* method)
{
	VoxelStructure_t2247263099 * V_0 = NULL;
	VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	{
		VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* L_0 = __this->get__chunks_11();
		V_1 = (VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636*)L_0;
		VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* L_1 = V_1;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_1);
		int32_t L_2 = Array_GetLength_m2083296647((RuntimeArray *)(RuntimeArray *)L_1, 0, /*hidden argument*/NULL);
		V_2 = L_2;
		VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* L_3 = V_1;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_3);
		int32_t L_4 = Array_GetLength_m2083296647((RuntimeArray *)(RuntimeArray *)L_3, 1, /*hidden argument*/NULL);
		V_3 = L_4;
		VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* L_5 = V_1;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_5);
		int32_t L_6 = Array_GetLength_m2083296647((RuntimeArray *)(RuntimeArray *)L_5, 2, /*hidden argument*/NULL);
		V_4 = L_6;
		V_5 = 0;
		goto IL_0079;
	}

IL_0028:
	{
		V_6 = 0;
		goto IL_006b;
	}

IL_0030:
	{
		V_7 = 0;
		goto IL_005c;
	}

IL_0038:
	{
		VoxelStructureU5B0___U2C0___U2C0___U5D_t3888664636* L_7 = V_1;
		int32_t L_8 = V_5;
		int32_t L_9 = V_6;
		int32_t L_10 = V_7;
		NullCheck(L_7);
		VoxelStructure_t2247263099 * L_11 = (L_7)->GetAt(L_8, L_9, L_10);
		V_0 = L_11;
		VoxelStructure_t2247263099 * L_12 = V_0;
		NullCheck(L_12);
		bool L_13 = L_12->get_IsDirty_24();
		if (!L_13)
		{
			goto IL_0056;
		}
	}
	{
		VoxelStructure_t2247263099 * L_14 = V_0;
		NullCheck(L_14);
		VoxelStructure_Draw_m1865268436(L_14, /*hidden argument*/NULL);
	}

IL_0056:
	{
		int32_t L_15 = V_7;
		V_7 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_16 = V_7;
		int32_t L_17 = V_4;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_18 = V_6;
		V_6 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_006b:
	{
		int32_t L_19 = V_6;
		int32_t L_20 = V_3;
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0030;
		}
	}
	{
		int32_t L_21 = V_5;
		V_5 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_0079:
	{
		int32_t L_22 = V_5;
		int32_t L_23 = V_2;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0028;
		}
	}
	{
		return;
	}
}
// System.Void VoxelSpace::GetHeightMap(System.Single[0...,0...]&)
extern "C"  void VoxelSpace_GetHeightMap_m3406387659 (VoxelSpace_t930815766 * __this, SingleU5B0___U2C0___U5D_t577127398** ___array0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelSpace_GetHeightMap_m3406387659_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		SingleU5B0___U2C0___U5D_t577127398** L_0 = ___array0;
		int32_t L_1 = VoxelSpace_get_Width_m4024942492(__this, /*hidden argument*/NULL);
		int32_t L_2 = VoxelSpace_get_Height_m2390752487(__this, /*hidden argument*/NULL);
		il2cpp_array_size_t L_4[] = { (il2cpp_array_size_t)L_1, (il2cpp_array_size_t)L_2 };
		SingleU5B0___U2C0___U5D_t577127398* L_3 = (SingleU5B0___U2C0___U5D_t577127398*)GenArrayNew(SingleU5B0___U2C0___U5D_t577127398_il2cpp_TypeInfo_var, L_4);
		*((RuntimeObject **)(L_0)) = (RuntimeObject *)L_3;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)(L_0), (RuntimeObject *)L_3);
		V_0 = 0;
		goto IL_0068;
	}

IL_001a:
	{
		V_1 = 0;
		goto IL_0058;
	}

IL_0021:
	{
		V_2 = 0;
		goto IL_0048;
	}

IL_0028:
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_2;
		int32_t L_7 = V_0;
		int32_t L_8 = VoxelSpace_GetVoxel_m2941864988(__this, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_3 = L_8;
		int32_t L_9 = V_3;
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0044;
		}
	}
	{
		SingleU5B0___U2C0___U5D_t577127398** L_10 = ___array0;
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = V_2;
		NullCheck((*((SingleU5B0___U2C0___U5D_t577127398**)L_10)));
		((*((SingleU5B0___U2C0___U5D_t577127398**)L_10)))->SetAt(L_11, L_12, (((float)((float)L_13))));
	}

IL_0044:
	{
		int32_t L_14 = V_2;
		V_2 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = VoxelSpace_get_Height_m2390752487(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_17 = V_1;
		V_1 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_18 = V_1;
		int32_t L_19 = VoxelSpace_get_Depth_m4270141865(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_18) < ((int32_t)L_19)))
		{
			goto IL_0021;
		}
	}
	{
		int32_t L_20 = V_0;
		V_0 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0068:
	{
		int32_t L_21 = V_0;
		int32_t L_22 = VoxelSpace_get_Depth_m4270141865(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_001a;
		}
	}
	{
		return;
	}
}
// System.Void VoxelStructure::.ctor()
extern "C"  void VoxelStructure__ctor_m3902208472 (VoxelStructure_t2247263099 * __this, const RuntimeMethod* method)
{
	{
		__this->set_Width_8(8);
		__this->set_Height_9(8);
		__this->set_Depth_10(8);
		__this->set_ONE_PIXEL_25((0.0002f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// VoxelSpace VoxelStructure::get_VoxelSpace()
extern "C"  VoxelSpace_t930815766 * VoxelStructure_get_VoxelSpace_m721980576 (VoxelStructure_t2247263099 * __this, const RuntimeMethod* method)
{
	{
		VoxelSpace_t930815766 * L_0 = __this->get_U3CVoxelSpaceU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void VoxelStructure::set_VoxelSpace(VoxelSpace)
extern "C"  void VoxelStructure_set_VoxelSpace_m3380696627 (VoxelStructure_t2247263099 * __this, VoxelSpace_t930815766 * ___value0, const RuntimeMethod* method)
{
	{
		VoxelSpace_t930815766 * L_0 = ___value0;
		__this->set_U3CVoxelSpaceU3Ek__BackingField_3(L_0);
		return;
	}
}
// VoxelStructure VoxelStructure::Create(UnityEngine.GameObject,System.Int32,System.Single,System.Single,System.Single,System.Int32,System.Int32,System.Int32,UnityEngine.Vector3,VoxelPallette,VoxelSpace)
extern "C"  VoxelStructure_t2247263099 * VoxelStructure_Create_m3231949789 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___parent0, int32_t ___index1, float ___x2, float ___y3, float ___z4, int32_t ___chunkSizeW5, int32_t ___chunkSizeH6, int32_t ___chunkSizeD7, Vector3_t2243707580  ___offset8, VoxelPallette_t4284593999 * ___palette9, VoxelSpace_t930815766 * ___space10, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelStructure_Create_m3231949789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	VoxelStructure_t2247263099 * V_1 = NULL;
	MeshRenderer_t1268241104 * V_2 = NULL;
	MeshCollider_t2718867283 * V_3 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t3500843524_il2cpp_TypeInfo_var);
		CultureInfo_t3500843524 * L_0 = CultureInfo_get_InvariantCulture_m398972276(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_1 = Int32_ToString_m526790770((&___index1), L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral3110053185, L_1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_3, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		GameObject_t1756533147 * L_4 = V_0;
		NullCheck(L_4);
		Transform_t3275118058 * L_5 = GameObject_get_transform_m909382139(L_4, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_6 = ___parent0;
		NullCheck(L_6);
		Transform_t3275118058 * L_7 = GameObject_get_transform_m909382139(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_parent_m3281327839(L_5, L_7, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = V_0;
		NullCheck(L_8);
		Transform_t3275118058 * L_9 = GameObject_get_transform_m909382139(L_8, /*hidden argument*/NULL);
		float L_10 = ___x2;
		int32_t L_11 = ___chunkSizeW5;
		float L_12 = ___y3;
		int32_t L_13 = ___chunkSizeH6;
		float L_14 = ___z4;
		int32_t L_15 = ___chunkSizeD7;
		Vector3_t2243707580  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2638739322((&L_16), ((float)((float)L_10*(float)(((float)((float)L_11))))), ((float)((float)L_12*(float)(((float)((float)L_13))))), ((float)((float)L_14*(float)(((float)((float)L_15))))), /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = ___offset8;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_18 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_position_m2469242620(L_9, L_18, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = V_0;
		NullCheck(L_19);
		VoxelStructure_t2247263099 * L_20 = GameObject_AddComponent_TisVoxelStructure_t2247263099_m1788057469(L_19, /*hidden argument*/GameObject_AddComponent_TisVoxelStructure_t2247263099_m1788057469_RuntimeMethod_var);
		V_1 = L_20;
		VoxelStructure_t2247263099 * L_21 = V_1;
		VoxelPallette_t4284593999 * L_22 = ___palette9;
		NullCheck(L_21);
		L_21->set_pallette_2(L_22);
		VoxelStructure_t2247263099 * L_23 = V_1;
		int32_t L_24 = ___chunkSizeW5;
		NullCheck(L_23);
		L_23->set_Width_8(L_24);
		VoxelStructure_t2247263099 * L_25 = V_1;
		int32_t L_26 = ___chunkSizeH6;
		NullCheck(L_25);
		L_25->set_Height_9(L_26);
		VoxelStructure_t2247263099 * L_27 = V_1;
		int32_t L_28 = ___chunkSizeD7;
		NullCheck(L_27);
		L_27->set_Depth_10(L_28);
		VoxelStructure_t2247263099 * L_29 = V_1;
		VoxelSpace_t930815766 * L_30 = ___space10;
		NullCheck(L_29);
		VoxelStructure_set_VoxelSpace_m3380696627(L_29, L_30, /*hidden argument*/NULL);
		VoxelStructure_t2247263099 * L_31 = V_1;
		int32_t L_32 = ___chunkSizeW5;
		int32_t L_33 = ___chunkSizeH6;
		int32_t L_34 = ___chunkSizeD7;
		il2cpp_array_size_t L_36[] = { (il2cpp_array_size_t)L_32, (il2cpp_array_size_t)L_33, (il2cpp_array_size_t)L_34 };
		Int32U5B0___U2C0___U2C0___U5D_t3030399643* L_35 = (Int32U5B0___U2C0___U2C0___U5D_t3030399643*)GenArrayNew(Int32U5B0___U2C0___U2C0___U5D_t3030399643_il2cpp_TypeInfo_var, L_36);
		NullCheck(L_31);
		L_31->set_flatVoxels_7((Int32U5B0___U2C0___U2C0___U5D_t3030399643*)L_35);
		GameObject_t1756533147 * L_37 = V_0;
		NullCheck(L_37);
		MeshRenderer_t1268241104 * L_38 = GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632(L_37, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1268241104_m3528968632_RuntimeMethod_var);
		V_2 = L_38;
		MeshRenderer_t1268241104 * L_39 = V_2;
		NullCheck(L_39);
		Renderer_set_shadowCastingMode_m2840732934(L_39, 1, /*hidden argument*/NULL);
		MeshRenderer_t1268241104 * L_40 = V_2;
		NullCheck(L_40);
		Renderer_set_receiveShadows_m2366149940(L_40, (bool)1, /*hidden argument*/NULL);
		MeshRenderer_t1268241104 * L_41 = V_2;
		NullCheck(L_41);
		Renderer_set_lightProbeUsage_m1212616902(L_41, 0, /*hidden argument*/NULL);
		MeshRenderer_t1268241104 * L_42 = V_2;
		NullCheck(L_42);
		Renderer_set_reflectionProbeUsage_m2405564324(L_42, 1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_43 = V_0;
		NullCheck(L_43);
		MeshCollider_t2718867283 * L_44 = GameObject_GetComponent_TisMeshCollider_t2718867283_m4281307913(L_43, /*hidden argument*/GameObject_GetComponent_TisMeshCollider_t2718867283_m4281307913_RuntimeMethod_var);
		V_3 = L_44;
		MeshCollider_t2718867283 * L_45 = V_3;
		NullCheck(L_45);
		MeshCollider_set_convex_m2548365563(L_45, (bool)0, /*hidden argument*/NULL);
		MeshCollider_t2718867283 * L_46 = V_3;
		NullCheck(L_46);
		Collider_set_isTrigger_m1298573031(L_46, (bool)0, /*hidden argument*/NULL);
		VoxelStructure_t2247263099 * L_47 = V_1;
		return L_47;
	}
}
// System.Void VoxelStructure::Draw()
extern "C"  void VoxelStructure_Draw_m1865268436 (VoxelStructure_t2247263099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelStructure_Draw_m1865268436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	Vector2U5BU5D_t686124026* V_13 = NULL;
	Vector2U5BU5D_t686124026* V_14 = NULL;
	Vector2U5BU5D_t686124026* V_15 = NULL;
	Vector2U5BU5D_t686124026* V_16 = NULL;
	Vector2U5BU5D_t686124026* V_17 = NULL;
	Vector2U5BU5D_t686124026* V_18 = NULL;
	Vector2U5BU5D_t686124026* V_19 = NULL;
	Vector2U5BU5D_t686124026* V_20 = NULL;
	Vector2U5BU5D_t686124026* V_21 = NULL;
	Vector2U5BU5D_t686124026* V_22 = NULL;
	Vector2U5BU5D_t686124026* V_23 = NULL;
	Vector2U5BU5D_t686124026* V_24 = NULL;
	Vector3U5BU5D_t1172311765* V_25 = NULL;
	Int32U5BU5D_t3030399641* V_26 = NULL;
	Vector2U5BU5D_t686124026* V_27 = NULL;
	Color32U5BU5D_t30278651* V_28 = NULL;
	Mesh_t1356156583 * V_29 = NULL;
	{
		VoxelPallette_t4284593999 * L_0 = __this->get_pallette_2();
		if (L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3445844385, /*hidden argument*/NULL);
		goto IL_1c94;
	}

IL_001a:
	{
		MeshFilter_t3026937449 * L_1 = Component_GetComponent_TisMeshFilter_t3026937449_m1170669043(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1170669043_RuntimeMethod_var);
		__this->set__mf_14(L_1);
		VoxelStructure_ClampDimensions_m3734676430(__this, /*hidden argument*/NULL);
		V_0 = 1;
		int32_t L_2 = V_0;
		V_1 = ((int32_t)((int32_t)L_2*(int32_t)4));
		int32_t L_3 = V_0;
		V_2 = ((int32_t)((int32_t)L_3*(int32_t)6));
		Vector3U5BU5D_t1172311765* L_4 = __this->get__verts_11();
		if (!L_4)
		{
			goto IL_0065;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_5 = __this->get__verts_11();
		NullCheck(L_5);
		int32_t L_6 = __this->get_Width_8();
		int32_t L_7 = __this->get_Height_9();
		int32_t L_8 = __this->get_Depth_10();
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_5)->max_length))))) == ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6*(int32_t)L_7))*(int32_t)L_8))*(int32_t)((int32_t)24))))))
		{
			goto IL_00f2;
		}
	}

IL_0065:
	{
		int32_t L_9 = __this->get_Width_8();
		int32_t L_10 = __this->get_Height_9();
		int32_t L_11 = __this->get_Depth_10();
		__this->set__verts_11(((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_9*(int32_t)L_10))*(int32_t)L_11))*(int32_t)((int32_t)24))))));
		int32_t L_12 = __this->get_Width_8();
		int32_t L_13 = __this->get_Height_9();
		int32_t L_14 = __this->get_Depth_10();
		__this->set__tris_15(((Int32U5BU5D_t3030399641*)SZArrayNew(Int32U5BU5D_t3030399641_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_12*(int32_t)L_13))*(int32_t)L_14))*(int32_t)((int32_t)36))))));
		int32_t L_15 = __this->get_Width_8();
		int32_t L_16 = __this->get_Height_9();
		int32_t L_17 = __this->get_Depth_10();
		__this->set__uvs_12(((Vector2U5BU5D_t686124026*)SZArrayNew(Vector2U5BU5D_t686124026_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_15*(int32_t)L_16))*(int32_t)L_17))*(int32_t)((int32_t)24))))));
		int32_t L_18 = __this->get_Width_8();
		int32_t L_19 = __this->get_Height_9();
		int32_t L_20 = __this->get_Depth_10();
		__this->set__colors_13(((Color32U5BU5D_t30278651*)SZArrayNew(Color32U5BU5D_t30278651_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_18*(int32_t)L_19))*(int32_t)L_20))*(int32_t)((int32_t)24))))));
		goto IL_0142;
	}

IL_00f2:
	{
		Vector3U5BU5D_t1172311765* L_21 = __this->get__verts_11();
		Vector3U5BU5D_t1172311765* L_22 = __this->get__verts_11();
		NullCheck(L_22);
		Array_Clear_m782967417(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_21, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_22)->max_length)))), /*hidden argument*/NULL);
		Int32U5BU5D_t3030399641* L_23 = __this->get__tris_15();
		Int32U5BU5D_t3030399641* L_24 = __this->get__tris_15();
		NullCheck(L_24);
		Array_Clear_m782967417(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_23, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_24)->max_length)))), /*hidden argument*/NULL);
		Vector2U5BU5D_t686124026* L_25 = __this->get__uvs_12();
		Vector2U5BU5D_t686124026* L_26 = __this->get__uvs_12();
		NullCheck(L_26);
		Array_Clear_m782967417(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_25, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_26)->max_length)))), /*hidden argument*/NULL);
		Color32U5BU5D_t30278651* L_27 = __this->get__colors_13();
		Color32U5BU5D_t30278651* L_28 = __this->get__colors_13();
		NullCheck(L_28);
		Array_Clear_m782967417(NULL /*static, unused*/, (RuntimeArray *)(RuntimeArray *)L_27, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_28)->max_length)))), /*hidden argument*/NULL);
	}

IL_0142:
	{
		V_3 = 0;
		goto IL_1b59;
	}

IL_0149:
	{
		V_4 = 0;
		goto IL_1b48;
	}

IL_0151:
	{
		V_5 = 0;
		goto IL_1b35;
	}

IL_0159:
	{
		int32_t L_29 = V_3;
		int32_t L_30 = V_4;
		int32_t L_31 = V_5;
		int32_t L_32 = VoxelStructure_GetVoxel_m3191480667(__this, L_29, L_30, L_31, /*hidden argument*/NULL);
		V_6 = L_32;
		int32_t L_33 = V_6;
		VoxelTemplate_t594004422 * L_34 = VoxelStructure_GetVoxelType_m63795532(__this, L_33, /*hidden argument*/NULL);
		__this->set_voxelType_22(L_34);
		VoxelTemplate_t594004422 * L_35 = __this->get_voxelType_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_36 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_35, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_1b2f;
		}
	}
	{
		VoxelTemplate_t594004422 * L_37 = __this->get_voxelType_22();
		NullCheck(L_37);
		bool L_38 = L_37->get_shouldDraw_13();
		if (!L_38)
		{
			goto IL_1b2f;
		}
	}
	{
		VoxelTemplate_t594004422 * L_39 = __this->get_voxelType_22();
		NullCheck(L_39);
		Color32_t874517518  L_40 = L_39->get_color_3();
		__this->set_voxelColor_23(L_40);
		VoxelTemplate_t594004422 * L_41 = __this->get_voxelType_22();
		NullCheck(L_41);
		bool L_42 = L_41->get_drawFacesInCenter_5();
		if (L_42)
		{
			goto IL_0f23;
		}
	}
	{
		int32_t L_43 = V_3;
		int32_t L_44 = V_4;
		int32_t L_45 = V_5;
		int32_t L_46 = VoxelStructure_GetVoxel_m3191480667(__this, L_43, L_44, ((int32_t)((int32_t)L_45-(int32_t)1)), /*hidden argument*/NULL);
		V_7 = L_46;
		int32_t L_47 = V_7;
		VoxelTemplate_t594004422 * L_48 = VoxelStructure_GetVoxelType_m63795532(__this, L_47, /*hidden argument*/NULL);
		__this->set_fType_16(L_48);
		int32_t L_49 = V_3;
		int32_t L_50 = V_4;
		int32_t L_51 = V_5;
		int32_t L_52 = VoxelStructure_GetVoxel_m3191480667(__this, L_49, L_50, ((int32_t)((int32_t)L_51+(int32_t)1)), /*hidden argument*/NULL);
		V_8 = L_52;
		int32_t L_53 = V_8;
		VoxelTemplate_t594004422 * L_54 = VoxelStructure_GetVoxelType_m63795532(__this, L_53, /*hidden argument*/NULL);
		__this->set_kType_17(L_54);
		int32_t L_55 = V_3;
		int32_t L_56 = V_4;
		int32_t L_57 = V_5;
		int32_t L_58 = VoxelStructure_GetVoxel_m3191480667(__this, ((int32_t)((int32_t)L_55+(int32_t)1)), L_56, L_57, /*hidden argument*/NULL);
		V_9 = L_58;
		int32_t L_59 = V_9;
		VoxelTemplate_t594004422 * L_60 = VoxelStructure_GetVoxelType_m63795532(__this, L_59, /*hidden argument*/NULL);
		__this->set_rType_18(L_60);
		int32_t L_61 = V_3;
		int32_t L_62 = V_4;
		int32_t L_63 = V_5;
		int32_t L_64 = VoxelStructure_GetVoxel_m3191480667(__this, ((int32_t)((int32_t)L_61-(int32_t)1)), L_62, L_63, /*hidden argument*/NULL);
		V_10 = L_64;
		int32_t L_65 = V_10;
		VoxelTemplate_t594004422 * L_66 = VoxelStructure_GetVoxelType_m63795532(__this, L_65, /*hidden argument*/NULL);
		__this->set_lType_19(L_66);
		int32_t L_67 = V_3;
		int32_t L_68 = V_4;
		int32_t L_69 = V_5;
		int32_t L_70 = VoxelStructure_GetVoxel_m3191480667(__this, L_67, ((int32_t)((int32_t)L_68+(int32_t)1)), L_69, /*hidden argument*/NULL);
		V_11 = L_70;
		int32_t L_71 = V_11;
		VoxelTemplate_t594004422 * L_72 = VoxelStructure_GetVoxelType_m63795532(__this, L_71, /*hidden argument*/NULL);
		__this->set_tType_20(L_72);
		int32_t L_73 = V_3;
		int32_t L_74 = V_4;
		int32_t L_75 = V_5;
		int32_t L_76 = VoxelStructure_GetVoxel_m3191480667(__this, L_73, ((int32_t)((int32_t)L_74-(int32_t)1)), L_75, /*hidden argument*/NULL);
		V_12 = L_76;
		int32_t L_77 = V_12;
		VoxelTemplate_t594004422 * L_78 = VoxelStructure_GetVoxelType_m63795532(__this, L_77, /*hidden argument*/NULL);
		__this->set_bType_21(L_78);
		VoxelSpace_t930815766 * L_79 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_79);
		bool L_80 = L_79->get_DrawFront_12();
		if (!L_80)
		{
			goto IL_047f;
		}
	}
	{
		VoxelTemplate_t594004422 * L_81 = __this->get_voxelType_22();
		NullCheck(L_81);
		bool L_82 = L_81->get_drawFront_14();
		if (!L_82)
		{
			goto IL_047f;
		}
	}
	{
		VoxelTemplate_t594004422 * L_83 = __this->get_fType_16();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_84 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_83, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_84)
		{
			goto IL_02b5;
		}
	}
	{
		VoxelTemplate_t594004422 * L_85 = __this->get_fType_16();
		NullCheck(L_85);
		bool L_86 = L_85->get_drawFacesInCenter_5();
		if (L_86)
		{
			goto IL_02b5;
		}
	}
	{
		VoxelTemplate_t594004422 * L_87 = __this->get_fType_16();
		NullCheck(L_87);
		bool L_88 = L_87->get_shouldDraw_13();
		if (L_88)
		{
			goto IL_047f;
		}
	}

IL_02b5:
	{
		Vector3U5BU5D_t1172311765* L_89 = __this->get__verts_11();
		int32_t L_90 = V_1;
		NullCheck(L_89);
		int32_t L_91 = V_3;
		int32_t L_92 = V_4;
		int32_t L_93 = V_5;
		Vector3_t2243707580  L_94;
		memset(&L_94, 0, sizeof(L_94));
		Vector3__ctor_m2638739322((&L_94), (((float)((float)L_91))), (((float)((float)L_92))), (((float)((float)L_93))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_89)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_90))) = L_94;
		Vector3U5BU5D_t1172311765* L_95 = __this->get__verts_11();
		int32_t L_96 = V_1;
		NullCheck(L_95);
		int32_t L_97 = V_3;
		int32_t L_98 = V_4;
		int32_t L_99 = V_5;
		Vector3_t2243707580  L_100;
		memset(&L_100, 0, sizeof(L_100));
		Vector3__ctor_m2638739322((&L_100), (((float)((float)L_97))), (((float)((float)((int32_t)((int32_t)L_98+(int32_t)1))))), (((float)((float)L_99))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_95)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_96+(int32_t)1))))) = L_100;
		Vector3U5BU5D_t1172311765* L_101 = __this->get__verts_11();
		int32_t L_102 = V_1;
		NullCheck(L_101);
		int32_t L_103 = V_3;
		int32_t L_104 = V_4;
		int32_t L_105 = V_5;
		Vector3_t2243707580  L_106;
		memset(&L_106, 0, sizeof(L_106));
		Vector3__ctor_m2638739322((&L_106), (((float)((float)((int32_t)((int32_t)L_103+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_104+(int32_t)1))))), (((float)((float)L_105))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_101)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_102+(int32_t)2))))) = L_106;
		Vector3U5BU5D_t1172311765* L_107 = __this->get__verts_11();
		int32_t L_108 = V_1;
		NullCheck(L_107);
		int32_t L_109 = V_3;
		int32_t L_110 = V_4;
		int32_t L_111 = V_5;
		Vector3_t2243707580  L_112;
		memset(&L_112, 0, sizeof(L_112));
		Vector3__ctor_m2638739322((&L_112), (((float)((float)((int32_t)((int32_t)L_109+(int32_t)1))))), (((float)((float)L_110))), (((float)((float)L_111))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_107)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_108+(int32_t)3))))) = L_112;
		Int32U5BU5D_t3030399641* L_113 = __this->get__tris_15();
		int32_t L_114 = V_2;
		int32_t L_115 = V_1;
		NullCheck(L_113);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(L_114), (int32_t)L_115);
		Int32U5BU5D_t3030399641* L_116 = __this->get__tris_15();
		int32_t L_117 = V_2;
		int32_t L_118 = V_1;
		NullCheck(L_116);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_117+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_118+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_119 = __this->get__tris_15();
		int32_t L_120 = V_2;
		int32_t L_121 = V_1;
		NullCheck(L_119);
		(L_119)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_120+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_121+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_122 = __this->get__tris_15();
		int32_t L_123 = V_2;
		int32_t L_124 = V_1;
		NullCheck(L_122);
		(L_122)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_123+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_124+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_125 = __this->get__tris_15();
		int32_t L_126 = V_2;
		int32_t L_127 = V_1;
		NullCheck(L_125);
		(L_125)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_126+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_127+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_128 = __this->get__tris_15();
		int32_t L_129 = V_2;
		int32_t L_130 = V_1;
		NullCheck(L_128);
		(L_128)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_129+(int32_t)5))), (int32_t)L_130);
		VoxelTemplate_t594004422 * L_131 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_132 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_131, ((int32_t)102), /*hidden argument*/NULL);
		V_13 = L_132;
		Vector2U5BU5D_t686124026* L_133 = __this->get__uvs_12();
		int32_t L_134 = V_1;
		NullCheck(L_133);
		Vector2U5BU5D_t686124026* L_135 = V_13;
		NullCheck(L_135);
		*(Vector2_t2243707579 *)((L_133)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_134))) = (*(Vector2_t2243707579 *)((L_135)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_136 = __this->get__uvs_12();
		int32_t L_137 = V_1;
		NullCheck(L_136);
		Vector2U5BU5D_t686124026* L_138 = V_13;
		NullCheck(L_138);
		*(Vector2_t2243707579 *)((L_136)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_137+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_138)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_139 = __this->get__uvs_12();
		int32_t L_140 = V_1;
		NullCheck(L_139);
		Vector2U5BU5D_t686124026* L_141 = V_13;
		NullCheck(L_141);
		*(Vector2_t2243707579 *)((L_139)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_140+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_141)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_142 = __this->get__uvs_12();
		int32_t L_143 = V_1;
		NullCheck(L_142);
		Vector2U5BU5D_t686124026* L_144 = V_13;
		NullCheck(L_144);
		*(Vector2_t2243707579 *)((L_142)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_143+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_144)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_145 = __this->get__colors_13();
		int32_t L_146 = V_1;
		NullCheck(L_145);
		Color32_t874517518  L_147 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_145)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_146))) = L_147;
		Color32U5BU5D_t30278651* L_148 = __this->get__colors_13();
		int32_t L_149 = V_1;
		NullCheck(L_148);
		Color32_t874517518  L_150 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_148)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_149+(int32_t)1))))) = L_150;
		Color32U5BU5D_t30278651* L_151 = __this->get__colors_13();
		int32_t L_152 = V_1;
		NullCheck(L_151);
		Color32_t874517518  L_153 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_151)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_152+(int32_t)2))))) = L_153;
		Color32U5BU5D_t30278651* L_154 = __this->get__colors_13();
		int32_t L_155 = V_1;
		NullCheck(L_154);
		Color32_t874517518  L_156 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_154)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_155+(int32_t)3))))) = L_156;
		int32_t L_157 = V_0;
		V_0 = ((int32_t)((int32_t)L_157+(int32_t)1));
		int32_t L_158 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_158));
		int32_t L_159 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_159));
	}

IL_047f:
	{
		VoxelSpace_t930815766 * L_160 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_160);
		bool L_161 = L_160->get_DrawBack_13();
		if (!L_161)
		{
			goto IL_06a2;
		}
	}
	{
		VoxelTemplate_t594004422 * L_162 = __this->get_voxelType_22();
		NullCheck(L_162);
		bool L_163 = L_162->get_drawBack_15();
		if (!L_163)
		{
			goto IL_06a2;
		}
	}
	{
		VoxelTemplate_t594004422 * L_164 = __this->get_kType_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_165 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_164, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_165)
		{
			goto IL_04d0;
		}
	}
	{
		VoxelTemplate_t594004422 * L_166 = __this->get_kType_17();
		NullCheck(L_166);
		bool L_167 = L_166->get_drawFacesInCenter_5();
		if (L_167)
		{
			goto IL_04d0;
		}
	}
	{
		VoxelTemplate_t594004422 * L_168 = __this->get_kType_17();
		NullCheck(L_168);
		bool L_169 = L_168->get_shouldDraw_13();
		if (L_169)
		{
			goto IL_06a2;
		}
	}

IL_04d0:
	{
		Vector3U5BU5D_t1172311765* L_170 = __this->get__verts_11();
		int32_t L_171 = V_1;
		NullCheck(L_170);
		int32_t L_172 = V_3;
		int32_t L_173 = V_4;
		int32_t L_174 = V_5;
		Vector3_t2243707580  L_175;
		memset(&L_175, 0, sizeof(L_175));
		Vector3__ctor_m2638739322((&L_175), (((float)((float)((int32_t)((int32_t)L_172+(int32_t)1))))), (((float)((float)L_173))), (((float)((float)((int32_t)((int32_t)L_174+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_170)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_171))) = L_175;
		Vector3U5BU5D_t1172311765* L_176 = __this->get__verts_11();
		int32_t L_177 = V_1;
		NullCheck(L_176);
		int32_t L_178 = V_3;
		int32_t L_179 = V_4;
		int32_t L_180 = V_5;
		Vector3_t2243707580  L_181;
		memset(&L_181, 0, sizeof(L_181));
		Vector3__ctor_m2638739322((&L_181), (((float)((float)((int32_t)((int32_t)L_178+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_179+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_180+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_176)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_177+(int32_t)1))))) = L_181;
		Vector3U5BU5D_t1172311765* L_182 = __this->get__verts_11();
		int32_t L_183 = V_1;
		NullCheck(L_182);
		int32_t L_184 = V_3;
		int32_t L_185 = V_4;
		int32_t L_186 = V_5;
		Vector3_t2243707580  L_187;
		memset(&L_187, 0, sizeof(L_187));
		Vector3__ctor_m2638739322((&L_187), (((float)((float)L_184))), (((float)((float)((int32_t)((int32_t)L_185+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_186+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_182)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_183+(int32_t)2))))) = L_187;
		Vector3U5BU5D_t1172311765* L_188 = __this->get__verts_11();
		int32_t L_189 = V_1;
		NullCheck(L_188);
		int32_t L_190 = V_3;
		int32_t L_191 = V_4;
		int32_t L_192 = V_5;
		Vector3_t2243707580  L_193;
		memset(&L_193, 0, sizeof(L_193));
		Vector3__ctor_m2638739322((&L_193), (((float)((float)L_190))), (((float)((float)L_191))), (((float)((float)((int32_t)((int32_t)L_192+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_188)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_189+(int32_t)3))))) = L_193;
		Int32U5BU5D_t3030399641* L_194 = __this->get__tris_15();
		int32_t L_195 = V_2;
		int32_t L_196 = V_1;
		NullCheck(L_194);
		(L_194)->SetAt(static_cast<il2cpp_array_size_t>(L_195), (int32_t)L_196);
		Int32U5BU5D_t3030399641* L_197 = __this->get__tris_15();
		int32_t L_198 = V_2;
		int32_t L_199 = V_1;
		NullCheck(L_197);
		(L_197)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_198+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_199+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_200 = __this->get__tris_15();
		int32_t L_201 = V_2;
		int32_t L_202 = V_1;
		NullCheck(L_200);
		(L_200)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_201+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_202+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_203 = __this->get__tris_15();
		int32_t L_204 = V_2;
		int32_t L_205 = V_1;
		NullCheck(L_203);
		(L_203)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_204+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_205+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_206 = __this->get__tris_15();
		int32_t L_207 = V_2;
		int32_t L_208 = V_1;
		NullCheck(L_206);
		(L_206)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_207+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_208+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_209 = __this->get__tris_15();
		int32_t L_210 = V_2;
		int32_t L_211 = V_1;
		NullCheck(L_209);
		(L_209)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_210+(int32_t)5))), (int32_t)L_211);
		VoxelTemplate_t594004422 * L_212 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_213 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_212, ((int32_t)107), /*hidden argument*/NULL);
		V_14 = L_213;
		Vector2U5BU5D_t686124026* L_214 = __this->get__uvs_12();
		int32_t L_215 = V_1;
		NullCheck(L_214);
		Vector2U5BU5D_t686124026* L_216 = V_14;
		NullCheck(L_216);
		*(Vector2_t2243707579 *)((L_214)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_215))) = (*(Vector2_t2243707579 *)((L_216)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_217 = __this->get__uvs_12();
		int32_t L_218 = V_1;
		NullCheck(L_217);
		Vector2U5BU5D_t686124026* L_219 = V_14;
		NullCheck(L_219);
		*(Vector2_t2243707579 *)((L_217)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_218+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_219)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_220 = __this->get__uvs_12();
		int32_t L_221 = V_1;
		NullCheck(L_220);
		Vector2U5BU5D_t686124026* L_222 = V_14;
		NullCheck(L_222);
		*(Vector2_t2243707579 *)((L_220)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_221+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_222)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_223 = __this->get__uvs_12();
		int32_t L_224 = V_1;
		NullCheck(L_223);
		Vector2U5BU5D_t686124026* L_225 = V_14;
		NullCheck(L_225);
		*(Vector2_t2243707579 *)((L_223)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_224+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_225)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_226 = __this->get__colors_13();
		int32_t L_227 = V_1;
		NullCheck(L_226);
		Color32_t874517518  L_228 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_226)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_227))) = L_228;
		Color32U5BU5D_t30278651* L_229 = __this->get__colors_13();
		int32_t L_230 = V_1;
		NullCheck(L_229);
		Color32_t874517518  L_231 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_229)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_230+(int32_t)1))))) = L_231;
		Color32U5BU5D_t30278651* L_232 = __this->get__colors_13();
		int32_t L_233 = V_1;
		NullCheck(L_232);
		Color32_t874517518  L_234 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_232)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_233+(int32_t)2))))) = L_234;
		Color32U5BU5D_t30278651* L_235 = __this->get__colors_13();
		int32_t L_236 = V_1;
		NullCheck(L_235);
		Color32_t874517518  L_237 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_235)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_236+(int32_t)3))))) = L_237;
		int32_t L_238 = V_0;
		V_0 = ((int32_t)((int32_t)L_238+(int32_t)1));
		int32_t L_239 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_239));
		int32_t L_240 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_240));
	}

IL_06a2:
	{
		VoxelSpace_t930815766 * L_241 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_241);
		bool L_242 = L_241->get_DrawTop_14();
		if (!L_242)
		{
			goto IL_08c5;
		}
	}
	{
		VoxelTemplate_t594004422 * L_243 = __this->get_voxelType_22();
		NullCheck(L_243);
		bool L_244 = L_243->get_drawTop_16();
		if (!L_244)
		{
			goto IL_08c5;
		}
	}
	{
		VoxelTemplate_t594004422 * L_245 = __this->get_tType_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_246 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_245, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_246)
		{
			goto IL_06f3;
		}
	}
	{
		VoxelTemplate_t594004422 * L_247 = __this->get_tType_20();
		NullCheck(L_247);
		bool L_248 = L_247->get_drawFacesInCenter_5();
		if (L_248)
		{
			goto IL_06f3;
		}
	}
	{
		VoxelTemplate_t594004422 * L_249 = __this->get_tType_20();
		NullCheck(L_249);
		bool L_250 = L_249->get_shouldDraw_13();
		if (L_250)
		{
			goto IL_08c5;
		}
	}

IL_06f3:
	{
		Vector3U5BU5D_t1172311765* L_251 = __this->get__verts_11();
		int32_t L_252 = V_1;
		NullCheck(L_251);
		int32_t L_253 = V_3;
		int32_t L_254 = V_4;
		int32_t L_255 = V_5;
		Vector3_t2243707580  L_256;
		memset(&L_256, 0, sizeof(L_256));
		Vector3__ctor_m2638739322((&L_256), (((float)((float)L_253))), (((float)((float)((int32_t)((int32_t)L_254+(int32_t)1))))), (((float)((float)L_255))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_251)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_252))) = L_256;
		Vector3U5BU5D_t1172311765* L_257 = __this->get__verts_11();
		int32_t L_258 = V_1;
		NullCheck(L_257);
		int32_t L_259 = V_3;
		int32_t L_260 = V_4;
		int32_t L_261 = V_5;
		Vector3_t2243707580  L_262;
		memset(&L_262, 0, sizeof(L_262));
		Vector3__ctor_m2638739322((&L_262), (((float)((float)L_259))), (((float)((float)((int32_t)((int32_t)L_260+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_261+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_257)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_258+(int32_t)1))))) = L_262;
		Vector3U5BU5D_t1172311765* L_263 = __this->get__verts_11();
		int32_t L_264 = V_1;
		NullCheck(L_263);
		int32_t L_265 = V_3;
		int32_t L_266 = V_4;
		int32_t L_267 = V_5;
		Vector3_t2243707580  L_268;
		memset(&L_268, 0, sizeof(L_268));
		Vector3__ctor_m2638739322((&L_268), (((float)((float)((int32_t)((int32_t)L_265+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_266+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_267+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_263)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_264+(int32_t)2))))) = L_268;
		Vector3U5BU5D_t1172311765* L_269 = __this->get__verts_11();
		int32_t L_270 = V_1;
		NullCheck(L_269);
		int32_t L_271 = V_3;
		int32_t L_272 = V_4;
		int32_t L_273 = V_5;
		Vector3_t2243707580  L_274;
		memset(&L_274, 0, sizeof(L_274));
		Vector3__ctor_m2638739322((&L_274), (((float)((float)((int32_t)((int32_t)L_271+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_272+(int32_t)1))))), (((float)((float)L_273))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_269)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_270+(int32_t)3))))) = L_274;
		Int32U5BU5D_t3030399641* L_275 = __this->get__tris_15();
		int32_t L_276 = V_2;
		int32_t L_277 = V_1;
		NullCheck(L_275);
		(L_275)->SetAt(static_cast<il2cpp_array_size_t>(L_276), (int32_t)L_277);
		Int32U5BU5D_t3030399641* L_278 = __this->get__tris_15();
		int32_t L_279 = V_2;
		int32_t L_280 = V_1;
		NullCheck(L_278);
		(L_278)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_279+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_280+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_281 = __this->get__tris_15();
		int32_t L_282 = V_2;
		int32_t L_283 = V_1;
		NullCheck(L_281);
		(L_281)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_282+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_283+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_284 = __this->get__tris_15();
		int32_t L_285 = V_2;
		int32_t L_286 = V_1;
		NullCheck(L_284);
		(L_284)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_285+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_286+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_287 = __this->get__tris_15();
		int32_t L_288 = V_2;
		int32_t L_289 = V_1;
		NullCheck(L_287);
		(L_287)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_288+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_289+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_290 = __this->get__tris_15();
		int32_t L_291 = V_2;
		int32_t L_292 = V_1;
		NullCheck(L_290);
		(L_290)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_291+(int32_t)5))), (int32_t)L_292);
		VoxelTemplate_t594004422 * L_293 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_294 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_293, ((int32_t)116), /*hidden argument*/NULL);
		V_15 = L_294;
		Vector2U5BU5D_t686124026* L_295 = __this->get__uvs_12();
		int32_t L_296 = V_1;
		NullCheck(L_295);
		Vector2U5BU5D_t686124026* L_297 = V_15;
		NullCheck(L_297);
		*(Vector2_t2243707579 *)((L_295)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_296))) = (*(Vector2_t2243707579 *)((L_297)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_298 = __this->get__uvs_12();
		int32_t L_299 = V_1;
		NullCheck(L_298);
		Vector2U5BU5D_t686124026* L_300 = V_15;
		NullCheck(L_300);
		*(Vector2_t2243707579 *)((L_298)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_299+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_300)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_301 = __this->get__uvs_12();
		int32_t L_302 = V_1;
		NullCheck(L_301);
		Vector2U5BU5D_t686124026* L_303 = V_15;
		NullCheck(L_303);
		*(Vector2_t2243707579 *)((L_301)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_302+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_303)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_304 = __this->get__uvs_12();
		int32_t L_305 = V_1;
		NullCheck(L_304);
		Vector2U5BU5D_t686124026* L_306 = V_15;
		NullCheck(L_306);
		*(Vector2_t2243707579 *)((L_304)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_305+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_306)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_307 = __this->get__colors_13();
		int32_t L_308 = V_1;
		NullCheck(L_307);
		Color32_t874517518  L_309 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_307)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_308))) = L_309;
		Color32U5BU5D_t30278651* L_310 = __this->get__colors_13();
		int32_t L_311 = V_1;
		NullCheck(L_310);
		Color32_t874517518  L_312 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_310)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_311+(int32_t)1))))) = L_312;
		Color32U5BU5D_t30278651* L_313 = __this->get__colors_13();
		int32_t L_314 = V_1;
		NullCheck(L_313);
		Color32_t874517518  L_315 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_313)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_314+(int32_t)2))))) = L_315;
		Color32U5BU5D_t30278651* L_316 = __this->get__colors_13();
		int32_t L_317 = V_1;
		NullCheck(L_316);
		Color32_t874517518  L_318 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_316)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_317+(int32_t)3))))) = L_318;
		int32_t L_319 = V_0;
		V_0 = ((int32_t)((int32_t)L_319+(int32_t)1));
		int32_t L_320 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_320));
		int32_t L_321 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_321));
	}

IL_08c5:
	{
		VoxelSpace_t930815766 * L_322 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_322);
		bool L_323 = L_322->get_DrawBottom_15();
		if (!L_323)
		{
			goto IL_0ae0;
		}
	}
	{
		VoxelTemplate_t594004422 * L_324 = __this->get_voxelType_22();
		NullCheck(L_324);
		bool L_325 = L_324->get_drawBottom_17();
		if (!L_325)
		{
			goto IL_0ae0;
		}
	}
	{
		VoxelTemplate_t594004422 * L_326 = __this->get_bType_21();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_327 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_326, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_327)
		{
			goto IL_0916;
		}
	}
	{
		VoxelTemplate_t594004422 * L_328 = __this->get_bType_21();
		NullCheck(L_328);
		bool L_329 = L_328->get_drawFacesInCenter_5();
		if (L_329)
		{
			goto IL_0916;
		}
	}
	{
		VoxelTemplate_t594004422 * L_330 = __this->get_bType_21();
		NullCheck(L_330);
		bool L_331 = L_330->get_shouldDraw_13();
		if (L_331)
		{
			goto IL_0ae0;
		}
	}

IL_0916:
	{
		Vector3U5BU5D_t1172311765* L_332 = __this->get__verts_11();
		int32_t L_333 = V_1;
		NullCheck(L_332);
		int32_t L_334 = V_3;
		int32_t L_335 = V_4;
		int32_t L_336 = V_5;
		Vector3_t2243707580  L_337;
		memset(&L_337, 0, sizeof(L_337));
		Vector3__ctor_m2638739322((&L_337), (((float)((float)L_334))), (((float)((float)L_335))), (((float)((float)L_336))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_332)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_333+(int32_t)3))))) = L_337;
		Vector3U5BU5D_t1172311765* L_338 = __this->get__verts_11();
		int32_t L_339 = V_1;
		NullCheck(L_338);
		int32_t L_340 = V_3;
		int32_t L_341 = V_4;
		int32_t L_342 = V_5;
		Vector3_t2243707580  L_343;
		memset(&L_343, 0, sizeof(L_343));
		Vector3__ctor_m2638739322((&L_343), (((float)((float)L_340))), (((float)((float)L_341))), (((float)((float)((int32_t)((int32_t)L_342+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_338)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_339+(int32_t)2))))) = L_343;
		Vector3U5BU5D_t1172311765* L_344 = __this->get__verts_11();
		int32_t L_345 = V_1;
		NullCheck(L_344);
		int32_t L_346 = V_3;
		int32_t L_347 = V_4;
		int32_t L_348 = V_5;
		Vector3_t2243707580  L_349;
		memset(&L_349, 0, sizeof(L_349));
		Vector3__ctor_m2638739322((&L_349), (((float)((float)((int32_t)((int32_t)L_346+(int32_t)1))))), (((float)((float)L_347))), (((float)((float)((int32_t)((int32_t)L_348+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_344)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_345+(int32_t)1))))) = L_349;
		Vector3U5BU5D_t1172311765* L_350 = __this->get__verts_11();
		int32_t L_351 = V_1;
		NullCheck(L_350);
		int32_t L_352 = V_3;
		int32_t L_353 = V_4;
		int32_t L_354 = V_5;
		Vector3_t2243707580  L_355;
		memset(&L_355, 0, sizeof(L_355));
		Vector3__ctor_m2638739322((&L_355), (((float)((float)((int32_t)((int32_t)L_352+(int32_t)1))))), (((float)((float)L_353))), (((float)((float)L_354))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_350)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_351))) = L_355;
		Int32U5BU5D_t3030399641* L_356 = __this->get__tris_15();
		int32_t L_357 = V_2;
		int32_t L_358 = V_1;
		NullCheck(L_356);
		(L_356)->SetAt(static_cast<il2cpp_array_size_t>(L_357), (int32_t)L_358);
		Int32U5BU5D_t3030399641* L_359 = __this->get__tris_15();
		int32_t L_360 = V_2;
		int32_t L_361 = V_1;
		NullCheck(L_359);
		(L_359)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_360+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_361+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_362 = __this->get__tris_15();
		int32_t L_363 = V_2;
		int32_t L_364 = V_1;
		NullCheck(L_362);
		(L_362)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_363+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_364+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_365 = __this->get__tris_15();
		int32_t L_366 = V_2;
		int32_t L_367 = V_1;
		NullCheck(L_365);
		(L_365)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_366+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_367+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_368 = __this->get__tris_15();
		int32_t L_369 = V_2;
		int32_t L_370 = V_1;
		NullCheck(L_368);
		(L_368)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_369+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_370+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_371 = __this->get__tris_15();
		int32_t L_372 = V_2;
		int32_t L_373 = V_1;
		NullCheck(L_371);
		(L_371)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_372+(int32_t)5))), (int32_t)L_373);
		VoxelTemplate_t594004422 * L_374 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_375 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_374, ((int32_t)98), /*hidden argument*/NULL);
		V_16 = L_375;
		Vector2U5BU5D_t686124026* L_376 = __this->get__uvs_12();
		int32_t L_377 = V_1;
		NullCheck(L_376);
		Vector2U5BU5D_t686124026* L_378 = V_16;
		NullCheck(L_378);
		*(Vector2_t2243707579 *)((L_376)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_377))) = (*(Vector2_t2243707579 *)((L_378)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_379 = __this->get__uvs_12();
		int32_t L_380 = V_1;
		NullCheck(L_379);
		Vector2U5BU5D_t686124026* L_381 = V_16;
		NullCheck(L_381);
		*(Vector2_t2243707579 *)((L_379)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_380+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_381)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_382 = __this->get__uvs_12();
		int32_t L_383 = V_1;
		NullCheck(L_382);
		Vector2U5BU5D_t686124026* L_384 = V_16;
		NullCheck(L_384);
		*(Vector2_t2243707579 *)((L_382)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_383+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_384)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_385 = __this->get__uvs_12();
		int32_t L_386 = V_1;
		NullCheck(L_385);
		Vector2U5BU5D_t686124026* L_387 = V_16;
		NullCheck(L_387);
		*(Vector2_t2243707579 *)((L_385)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_386+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_387)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_388 = __this->get__colors_13();
		int32_t L_389 = V_1;
		NullCheck(L_388);
		Color32_t874517518  L_390 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_388)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_389))) = L_390;
		Color32U5BU5D_t30278651* L_391 = __this->get__colors_13();
		int32_t L_392 = V_1;
		NullCheck(L_391);
		Color32_t874517518  L_393 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_391)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_392+(int32_t)1))))) = L_393;
		Color32U5BU5D_t30278651* L_394 = __this->get__colors_13();
		int32_t L_395 = V_1;
		NullCheck(L_394);
		Color32_t874517518  L_396 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_394)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_395+(int32_t)2))))) = L_396;
		Color32U5BU5D_t30278651* L_397 = __this->get__colors_13();
		int32_t L_398 = V_1;
		NullCheck(L_397);
		Color32_t874517518  L_399 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_397)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_398+(int32_t)3))))) = L_399;
		int32_t L_400 = V_0;
		V_0 = ((int32_t)((int32_t)L_400+(int32_t)1));
		int32_t L_401 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_401));
		int32_t L_402 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_402));
	}

IL_0ae0:
	{
		VoxelSpace_t930815766 * L_403 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_403);
		bool L_404 = L_403->get_DrawRight_17();
		if (!L_404)
		{
			goto IL_0d03;
		}
	}
	{
		VoxelTemplate_t594004422 * L_405 = __this->get_voxelType_22();
		NullCheck(L_405);
		bool L_406 = L_405->get_drawRight_19();
		if (!L_406)
		{
			goto IL_0d03;
		}
	}
	{
		VoxelTemplate_t594004422 * L_407 = __this->get_rType_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_408 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_407, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_408)
		{
			goto IL_0b31;
		}
	}
	{
		VoxelTemplate_t594004422 * L_409 = __this->get_rType_18();
		NullCheck(L_409);
		bool L_410 = L_409->get_drawFacesInCenter_5();
		if (L_410)
		{
			goto IL_0b31;
		}
	}
	{
		VoxelTemplate_t594004422 * L_411 = __this->get_rType_18();
		NullCheck(L_411);
		bool L_412 = L_411->get_shouldDraw_13();
		if (L_412)
		{
			goto IL_0d03;
		}
	}

IL_0b31:
	{
		Vector3U5BU5D_t1172311765* L_413 = __this->get__verts_11();
		int32_t L_414 = V_1;
		NullCheck(L_413);
		int32_t L_415 = V_3;
		int32_t L_416 = V_4;
		int32_t L_417 = V_5;
		Vector3_t2243707580  L_418;
		memset(&L_418, 0, sizeof(L_418));
		Vector3__ctor_m2638739322((&L_418), (((float)((float)((int32_t)((int32_t)L_415+(int32_t)1))))), (((float)((float)L_416))), (((float)((float)L_417))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_413)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_414))) = L_418;
		Vector3U5BU5D_t1172311765* L_419 = __this->get__verts_11();
		int32_t L_420 = V_1;
		NullCheck(L_419);
		int32_t L_421 = V_3;
		int32_t L_422 = V_4;
		int32_t L_423 = V_5;
		Vector3_t2243707580  L_424;
		memset(&L_424, 0, sizeof(L_424));
		Vector3__ctor_m2638739322((&L_424), (((float)((float)((int32_t)((int32_t)L_421+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_422+(int32_t)1))))), (((float)((float)L_423))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_419)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_420+(int32_t)1))))) = L_424;
		Vector3U5BU5D_t1172311765* L_425 = __this->get__verts_11();
		int32_t L_426 = V_1;
		NullCheck(L_425);
		int32_t L_427 = V_3;
		int32_t L_428 = V_4;
		int32_t L_429 = V_5;
		Vector3_t2243707580  L_430;
		memset(&L_430, 0, sizeof(L_430));
		Vector3__ctor_m2638739322((&L_430), (((float)((float)((int32_t)((int32_t)L_427+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_428+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_429+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_425)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_426+(int32_t)2))))) = L_430;
		Vector3U5BU5D_t1172311765* L_431 = __this->get__verts_11();
		int32_t L_432 = V_1;
		NullCheck(L_431);
		int32_t L_433 = V_3;
		int32_t L_434 = V_4;
		int32_t L_435 = V_5;
		Vector3_t2243707580  L_436;
		memset(&L_436, 0, sizeof(L_436));
		Vector3__ctor_m2638739322((&L_436), (((float)((float)((int32_t)((int32_t)L_433+(int32_t)1))))), (((float)((float)L_434))), (((float)((float)((int32_t)((int32_t)L_435+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_431)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_432+(int32_t)3))))) = L_436;
		Int32U5BU5D_t3030399641* L_437 = __this->get__tris_15();
		int32_t L_438 = V_2;
		int32_t L_439 = V_1;
		NullCheck(L_437);
		(L_437)->SetAt(static_cast<il2cpp_array_size_t>(L_438), (int32_t)L_439);
		Int32U5BU5D_t3030399641* L_440 = __this->get__tris_15();
		int32_t L_441 = V_2;
		int32_t L_442 = V_1;
		NullCheck(L_440);
		(L_440)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_441+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_442+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_443 = __this->get__tris_15();
		int32_t L_444 = V_2;
		int32_t L_445 = V_1;
		NullCheck(L_443);
		(L_443)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_444+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_445+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_446 = __this->get__tris_15();
		int32_t L_447 = V_2;
		int32_t L_448 = V_1;
		NullCheck(L_446);
		(L_446)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_447+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_448+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_449 = __this->get__tris_15();
		int32_t L_450 = V_2;
		int32_t L_451 = V_1;
		NullCheck(L_449);
		(L_449)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_450+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_451+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_452 = __this->get__tris_15();
		int32_t L_453 = V_2;
		int32_t L_454 = V_1;
		NullCheck(L_452);
		(L_452)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_453+(int32_t)5))), (int32_t)L_454);
		VoxelTemplate_t594004422 * L_455 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_456 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_455, ((int32_t)114), /*hidden argument*/NULL);
		V_17 = L_456;
		Vector2U5BU5D_t686124026* L_457 = __this->get__uvs_12();
		int32_t L_458 = V_1;
		NullCheck(L_457);
		Vector2U5BU5D_t686124026* L_459 = V_17;
		NullCheck(L_459);
		*(Vector2_t2243707579 *)((L_457)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_458))) = (*(Vector2_t2243707579 *)((L_459)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_460 = __this->get__uvs_12();
		int32_t L_461 = V_1;
		NullCheck(L_460);
		Vector2U5BU5D_t686124026* L_462 = V_17;
		NullCheck(L_462);
		*(Vector2_t2243707579 *)((L_460)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_461+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_462)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_463 = __this->get__uvs_12();
		int32_t L_464 = V_1;
		NullCheck(L_463);
		Vector2U5BU5D_t686124026* L_465 = V_17;
		NullCheck(L_465);
		*(Vector2_t2243707579 *)((L_463)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_464+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_465)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_466 = __this->get__uvs_12();
		int32_t L_467 = V_1;
		NullCheck(L_466);
		Vector2U5BU5D_t686124026* L_468 = V_17;
		NullCheck(L_468);
		*(Vector2_t2243707579 *)((L_466)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_467+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_468)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_469 = __this->get__colors_13();
		int32_t L_470 = V_1;
		NullCheck(L_469);
		Color32_t874517518  L_471 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_469)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_470))) = L_471;
		Color32U5BU5D_t30278651* L_472 = __this->get__colors_13();
		int32_t L_473 = V_1;
		NullCheck(L_472);
		Color32_t874517518  L_474 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_472)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_473+(int32_t)1))))) = L_474;
		Color32U5BU5D_t30278651* L_475 = __this->get__colors_13();
		int32_t L_476 = V_1;
		NullCheck(L_475);
		Color32_t874517518  L_477 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_475)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_476+(int32_t)2))))) = L_477;
		Color32U5BU5D_t30278651* L_478 = __this->get__colors_13();
		int32_t L_479 = V_1;
		NullCheck(L_478);
		Color32_t874517518  L_480 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_478)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_479+(int32_t)3))))) = L_480;
		int32_t L_481 = V_0;
		V_0 = ((int32_t)((int32_t)L_481+(int32_t)1));
		int32_t L_482 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_482));
		int32_t L_483 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_483));
	}

IL_0d03:
	{
		VoxelSpace_t930815766 * L_484 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_484);
		bool L_485 = L_484->get_DrawLeft_16();
		if (!L_485)
		{
			goto IL_0f1e;
		}
	}
	{
		VoxelTemplate_t594004422 * L_486 = __this->get_voxelType_22();
		NullCheck(L_486);
		bool L_487 = L_486->get_drawLeft_18();
		if (!L_487)
		{
			goto IL_0f1e;
		}
	}
	{
		VoxelTemplate_t594004422 * L_488 = __this->get_lType_19();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_489 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_488, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (L_489)
		{
			goto IL_0d54;
		}
	}
	{
		VoxelTemplate_t594004422 * L_490 = __this->get_lType_19();
		NullCheck(L_490);
		bool L_491 = L_490->get_drawFacesInCenter_5();
		if (L_491)
		{
			goto IL_0d54;
		}
	}
	{
		VoxelTemplate_t594004422 * L_492 = __this->get_lType_19();
		NullCheck(L_492);
		bool L_493 = L_492->get_shouldDraw_13();
		if (L_493)
		{
			goto IL_0f1e;
		}
	}

IL_0d54:
	{
		Vector3U5BU5D_t1172311765* L_494 = __this->get__verts_11();
		int32_t L_495 = V_1;
		NullCheck(L_494);
		int32_t L_496 = V_3;
		int32_t L_497 = V_4;
		int32_t L_498 = V_5;
		Vector3_t2243707580  L_499;
		memset(&L_499, 0, sizeof(L_499));
		Vector3__ctor_m2638739322((&L_499), (((float)((float)L_496))), (((float)((float)L_497))), (((float)((float)L_498))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_494)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_495+(int32_t)3))))) = L_499;
		Vector3U5BU5D_t1172311765* L_500 = __this->get__verts_11();
		int32_t L_501 = V_1;
		NullCheck(L_500);
		int32_t L_502 = V_3;
		int32_t L_503 = V_4;
		int32_t L_504 = V_5;
		Vector3_t2243707580  L_505;
		memset(&L_505, 0, sizeof(L_505));
		Vector3__ctor_m2638739322((&L_505), (((float)((float)L_502))), (((float)((float)((int32_t)((int32_t)L_503+(int32_t)1))))), (((float)((float)L_504))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_500)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_501+(int32_t)2))))) = L_505;
		Vector3U5BU5D_t1172311765* L_506 = __this->get__verts_11();
		int32_t L_507 = V_1;
		NullCheck(L_506);
		int32_t L_508 = V_3;
		int32_t L_509 = V_4;
		int32_t L_510 = V_5;
		Vector3_t2243707580  L_511;
		memset(&L_511, 0, sizeof(L_511));
		Vector3__ctor_m2638739322((&L_511), (((float)((float)L_508))), (((float)((float)((int32_t)((int32_t)L_509+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_510+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_506)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_507+(int32_t)1))))) = L_511;
		Vector3U5BU5D_t1172311765* L_512 = __this->get__verts_11();
		int32_t L_513 = V_1;
		NullCheck(L_512);
		int32_t L_514 = V_3;
		int32_t L_515 = V_4;
		int32_t L_516 = V_5;
		Vector3_t2243707580  L_517;
		memset(&L_517, 0, sizeof(L_517));
		Vector3__ctor_m2638739322((&L_517), (((float)((float)L_514))), (((float)((float)L_515))), (((float)((float)((int32_t)((int32_t)L_516+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_512)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_513))) = L_517;
		Int32U5BU5D_t3030399641* L_518 = __this->get__tris_15();
		int32_t L_519 = V_2;
		int32_t L_520 = V_1;
		NullCheck(L_518);
		(L_518)->SetAt(static_cast<il2cpp_array_size_t>(L_519), (int32_t)L_520);
		Int32U5BU5D_t3030399641* L_521 = __this->get__tris_15();
		int32_t L_522 = V_2;
		int32_t L_523 = V_1;
		NullCheck(L_521);
		(L_521)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_522+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_523+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_524 = __this->get__tris_15();
		int32_t L_525 = V_2;
		int32_t L_526 = V_1;
		NullCheck(L_524);
		(L_524)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_525+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_526+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_527 = __this->get__tris_15();
		int32_t L_528 = V_2;
		int32_t L_529 = V_1;
		NullCheck(L_527);
		(L_527)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_528+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_529+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_530 = __this->get__tris_15();
		int32_t L_531 = V_2;
		int32_t L_532 = V_1;
		NullCheck(L_530);
		(L_530)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_531+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_532+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_533 = __this->get__tris_15();
		int32_t L_534 = V_2;
		int32_t L_535 = V_1;
		NullCheck(L_533);
		(L_533)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_534+(int32_t)5))), (int32_t)L_535);
		VoxelTemplate_t594004422 * L_536 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_537 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_536, ((int32_t)108), /*hidden argument*/NULL);
		V_18 = L_537;
		Vector2U5BU5D_t686124026* L_538 = __this->get__uvs_12();
		int32_t L_539 = V_1;
		NullCheck(L_538);
		Vector2U5BU5D_t686124026* L_540 = V_18;
		NullCheck(L_540);
		*(Vector2_t2243707579 *)((L_538)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_539))) = (*(Vector2_t2243707579 *)((L_540)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_541 = __this->get__uvs_12();
		int32_t L_542 = V_1;
		NullCheck(L_541);
		Vector2U5BU5D_t686124026* L_543 = V_18;
		NullCheck(L_543);
		*(Vector2_t2243707579 *)((L_541)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_542+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_543)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_544 = __this->get__uvs_12();
		int32_t L_545 = V_1;
		NullCheck(L_544);
		Vector2U5BU5D_t686124026* L_546 = V_18;
		NullCheck(L_546);
		*(Vector2_t2243707579 *)((L_544)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_545+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_546)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_547 = __this->get__uvs_12();
		int32_t L_548 = V_1;
		NullCheck(L_547);
		Vector2U5BU5D_t686124026* L_549 = V_18;
		NullCheck(L_549);
		*(Vector2_t2243707579 *)((L_547)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_548+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_549)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_550 = __this->get__colors_13();
		int32_t L_551 = V_1;
		NullCheck(L_550);
		Color32_t874517518  L_552 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_550)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_551))) = L_552;
		Color32U5BU5D_t30278651* L_553 = __this->get__colors_13();
		int32_t L_554 = V_1;
		NullCheck(L_553);
		Color32_t874517518  L_555 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_553)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_554+(int32_t)1))))) = L_555;
		Color32U5BU5D_t30278651* L_556 = __this->get__colors_13();
		int32_t L_557 = V_1;
		NullCheck(L_556);
		Color32_t874517518  L_558 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_556)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_557+(int32_t)2))))) = L_558;
		Color32U5BU5D_t30278651* L_559 = __this->get__colors_13();
		int32_t L_560 = V_1;
		NullCheck(L_559);
		Color32_t874517518  L_561 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_559)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_560+(int32_t)3))))) = L_561;
		int32_t L_562 = V_0;
		V_0 = ((int32_t)((int32_t)L_562+(int32_t)1));
		int32_t L_563 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_563));
		int32_t L_564 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_564));
	}

IL_0f1e:
	{
		goto IL_1b2f;
	}

IL_0f23:
	{
		VoxelSpace_t930815766 * L_565 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_565);
		bool L_566 = L_565->get_DrawFront_12();
		if (!L_566)
		{
			goto IL_1125;
		}
	}
	{
		VoxelTemplate_t594004422 * L_567 = __this->get_voxelType_22();
		NullCheck(L_567);
		bool L_568 = L_567->get_drawFront_14();
		if (!L_568)
		{
			goto IL_1125;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_569 = __this->get__verts_11();
		int32_t L_570 = V_1;
		NullCheck(L_569);
		int32_t L_571 = V_3;
		int32_t L_572 = V_4;
		int32_t L_573 = V_5;
		Vector3_t2243707580  L_574;
		memset(&L_574, 0, sizeof(L_574));
		Vector3__ctor_m2638739322((&L_574), (((float)((float)L_571))), (((float)((float)L_572))), ((float)((float)(((float)((float)L_573)))+(float)(0.5f))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_569)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_570))) = L_574;
		Vector3U5BU5D_t1172311765* L_575 = __this->get__verts_11();
		int32_t L_576 = V_1;
		NullCheck(L_575);
		int32_t L_577 = V_3;
		int32_t L_578 = V_4;
		int32_t L_579 = V_5;
		Vector3_t2243707580  L_580;
		memset(&L_580, 0, sizeof(L_580));
		Vector3__ctor_m2638739322((&L_580), (((float)((float)L_577))), (((float)((float)((int32_t)((int32_t)L_578+(int32_t)1))))), ((float)((float)(((float)((float)L_579)))+(float)(0.5f))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_575)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_576+(int32_t)1))))) = L_580;
		Vector3U5BU5D_t1172311765* L_581 = __this->get__verts_11();
		int32_t L_582 = V_1;
		NullCheck(L_581);
		int32_t L_583 = V_3;
		int32_t L_584 = V_4;
		int32_t L_585 = V_5;
		Vector3_t2243707580  L_586;
		memset(&L_586, 0, sizeof(L_586));
		Vector3__ctor_m2638739322((&L_586), (((float)((float)((int32_t)((int32_t)L_583+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_584+(int32_t)1))))), ((float)((float)(((float)((float)L_585)))+(float)(0.5f))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_581)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_582+(int32_t)2))))) = L_586;
		Vector3U5BU5D_t1172311765* L_587 = __this->get__verts_11();
		int32_t L_588 = V_1;
		NullCheck(L_587);
		int32_t L_589 = V_3;
		int32_t L_590 = V_4;
		int32_t L_591 = V_5;
		Vector3_t2243707580  L_592;
		memset(&L_592, 0, sizeof(L_592));
		Vector3__ctor_m2638739322((&L_592), (((float)((float)((int32_t)((int32_t)L_589+(int32_t)1))))), (((float)((float)L_590))), ((float)((float)(((float)((float)L_591)))+(float)(0.5f))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_587)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_588+(int32_t)3))))) = L_592;
		Int32U5BU5D_t3030399641* L_593 = __this->get__tris_15();
		int32_t L_594 = V_2;
		int32_t L_595 = V_1;
		NullCheck(L_593);
		(L_593)->SetAt(static_cast<il2cpp_array_size_t>(L_594), (int32_t)L_595);
		Int32U5BU5D_t3030399641* L_596 = __this->get__tris_15();
		int32_t L_597 = V_2;
		int32_t L_598 = V_1;
		NullCheck(L_596);
		(L_596)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_597+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_598+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_599 = __this->get__tris_15();
		int32_t L_600 = V_2;
		int32_t L_601 = V_1;
		NullCheck(L_599);
		(L_599)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_600+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_601+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_602 = __this->get__tris_15();
		int32_t L_603 = V_2;
		int32_t L_604 = V_1;
		NullCheck(L_602);
		(L_602)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_603+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_604+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_605 = __this->get__tris_15();
		int32_t L_606 = V_2;
		int32_t L_607 = V_1;
		NullCheck(L_605);
		(L_605)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_606+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_607+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_608 = __this->get__tris_15();
		int32_t L_609 = V_2;
		int32_t L_610 = V_1;
		NullCheck(L_608);
		(L_608)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_609+(int32_t)5))), (int32_t)L_610);
		VoxelTemplate_t594004422 * L_611 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_612 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_611, ((int32_t)102), /*hidden argument*/NULL);
		V_19 = L_612;
		Vector2U5BU5D_t686124026* L_613 = __this->get__uvs_12();
		int32_t L_614 = V_1;
		NullCheck(L_613);
		Vector2U5BU5D_t686124026* L_615 = V_19;
		NullCheck(L_615);
		*(Vector2_t2243707579 *)((L_613)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_614))) = (*(Vector2_t2243707579 *)((L_615)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_616 = __this->get__uvs_12();
		int32_t L_617 = V_1;
		NullCheck(L_616);
		Vector2U5BU5D_t686124026* L_618 = V_19;
		NullCheck(L_618);
		*(Vector2_t2243707579 *)((L_616)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_617+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_618)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_619 = __this->get__uvs_12();
		int32_t L_620 = V_1;
		NullCheck(L_619);
		Vector2U5BU5D_t686124026* L_621 = V_19;
		NullCheck(L_621);
		*(Vector2_t2243707579 *)((L_619)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_620+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_621)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_622 = __this->get__uvs_12();
		int32_t L_623 = V_1;
		NullCheck(L_622);
		Vector2U5BU5D_t686124026* L_624 = V_19;
		NullCheck(L_624);
		*(Vector2_t2243707579 *)((L_622)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_623+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_624)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_625 = __this->get__colors_13();
		int32_t L_626 = V_1;
		NullCheck(L_625);
		Color32_t874517518  L_627 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_625)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_626))) = L_627;
		Color32U5BU5D_t30278651* L_628 = __this->get__colors_13();
		int32_t L_629 = V_1;
		NullCheck(L_628);
		Color32_t874517518  L_630 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_628)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_629+(int32_t)1))))) = L_630;
		Color32U5BU5D_t30278651* L_631 = __this->get__colors_13();
		int32_t L_632 = V_1;
		NullCheck(L_631);
		Color32_t874517518  L_633 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_631)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_632+(int32_t)2))))) = L_633;
		Color32U5BU5D_t30278651* L_634 = __this->get__colors_13();
		int32_t L_635 = V_1;
		NullCheck(L_634);
		Color32_t874517518  L_636 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_634)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_635+(int32_t)3))))) = L_636;
		int32_t L_637 = V_0;
		V_0 = ((int32_t)((int32_t)L_637+(int32_t)1));
		int32_t L_638 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_638));
		int32_t L_639 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_639));
	}

IL_1125:
	{
		VoxelSpace_t930815766 * L_640 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_640);
		bool L_641 = L_640->get_DrawBack_13();
		if (!L_641)
		{
			goto IL_1327;
		}
	}
	{
		VoxelTemplate_t594004422 * L_642 = __this->get_voxelType_22();
		NullCheck(L_642);
		bool L_643 = L_642->get_drawBack_15();
		if (!L_643)
		{
			goto IL_1327;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_644 = __this->get__verts_11();
		int32_t L_645 = V_1;
		NullCheck(L_644);
		int32_t L_646 = V_3;
		int32_t L_647 = V_4;
		int32_t L_648 = V_5;
		Vector3_t2243707580  L_649;
		memset(&L_649, 0, sizeof(L_649));
		Vector3__ctor_m2638739322((&L_649), (((float)((float)L_646))), (((float)((float)L_647))), ((float)((float)(((float)((float)L_648)))+(float)(0.5f))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_644)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_645+(int32_t)3))))) = L_649;
		Vector3U5BU5D_t1172311765* L_650 = __this->get__verts_11();
		int32_t L_651 = V_1;
		NullCheck(L_650);
		int32_t L_652 = V_3;
		int32_t L_653 = V_4;
		int32_t L_654 = V_5;
		Vector3_t2243707580  L_655;
		memset(&L_655, 0, sizeof(L_655));
		Vector3__ctor_m2638739322((&L_655), (((float)((float)L_652))), (((float)((float)((int32_t)((int32_t)L_653+(int32_t)1))))), ((float)((float)(((float)((float)L_654)))+(float)(0.5f))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_650)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_651+(int32_t)2))))) = L_655;
		Vector3U5BU5D_t1172311765* L_656 = __this->get__verts_11();
		int32_t L_657 = V_1;
		NullCheck(L_656);
		int32_t L_658 = V_3;
		int32_t L_659 = V_4;
		int32_t L_660 = V_5;
		Vector3_t2243707580  L_661;
		memset(&L_661, 0, sizeof(L_661));
		Vector3__ctor_m2638739322((&L_661), (((float)((float)((int32_t)((int32_t)L_658+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_659+(int32_t)1))))), ((float)((float)(((float)((float)L_660)))+(float)(0.5f))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_656)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_657+(int32_t)1))))) = L_661;
		Vector3U5BU5D_t1172311765* L_662 = __this->get__verts_11();
		int32_t L_663 = V_1;
		NullCheck(L_662);
		int32_t L_664 = V_3;
		int32_t L_665 = V_4;
		int32_t L_666 = V_5;
		Vector3_t2243707580  L_667;
		memset(&L_667, 0, sizeof(L_667));
		Vector3__ctor_m2638739322((&L_667), (((float)((float)((int32_t)((int32_t)L_664+(int32_t)1))))), (((float)((float)L_665))), ((float)((float)(((float)((float)L_666)))+(float)(0.5f))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_662)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_663))) = L_667;
		Int32U5BU5D_t3030399641* L_668 = __this->get__tris_15();
		int32_t L_669 = V_2;
		int32_t L_670 = V_1;
		NullCheck(L_668);
		(L_668)->SetAt(static_cast<il2cpp_array_size_t>(L_669), (int32_t)L_670);
		Int32U5BU5D_t3030399641* L_671 = __this->get__tris_15();
		int32_t L_672 = V_2;
		int32_t L_673 = V_1;
		NullCheck(L_671);
		(L_671)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_672+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_673+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_674 = __this->get__tris_15();
		int32_t L_675 = V_2;
		int32_t L_676 = V_1;
		NullCheck(L_674);
		(L_674)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_675+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_676+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_677 = __this->get__tris_15();
		int32_t L_678 = V_2;
		int32_t L_679 = V_1;
		NullCheck(L_677);
		(L_677)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_678+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_679+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_680 = __this->get__tris_15();
		int32_t L_681 = V_2;
		int32_t L_682 = V_1;
		NullCheck(L_680);
		(L_680)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_681+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_682+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_683 = __this->get__tris_15();
		int32_t L_684 = V_2;
		int32_t L_685 = V_1;
		NullCheck(L_683);
		(L_683)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_684+(int32_t)5))), (int32_t)L_685);
		VoxelTemplate_t594004422 * L_686 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_687 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_686, ((int32_t)107), /*hidden argument*/NULL);
		V_20 = L_687;
		Vector2U5BU5D_t686124026* L_688 = __this->get__uvs_12();
		int32_t L_689 = V_1;
		NullCheck(L_688);
		Vector2U5BU5D_t686124026* L_690 = V_20;
		NullCheck(L_690);
		*(Vector2_t2243707579 *)((L_688)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_689))) = (*(Vector2_t2243707579 *)((L_690)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_691 = __this->get__uvs_12();
		int32_t L_692 = V_1;
		NullCheck(L_691);
		Vector2U5BU5D_t686124026* L_693 = V_20;
		NullCheck(L_693);
		*(Vector2_t2243707579 *)((L_691)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_692+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_693)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_694 = __this->get__uvs_12();
		int32_t L_695 = V_1;
		NullCheck(L_694);
		Vector2U5BU5D_t686124026* L_696 = V_20;
		NullCheck(L_696);
		*(Vector2_t2243707579 *)((L_694)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_695+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_696)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_697 = __this->get__uvs_12();
		int32_t L_698 = V_1;
		NullCheck(L_697);
		Vector2U5BU5D_t686124026* L_699 = V_20;
		NullCheck(L_699);
		*(Vector2_t2243707579 *)((L_697)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_698+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_699)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_700 = __this->get__colors_13();
		int32_t L_701 = V_1;
		NullCheck(L_700);
		Color32_t874517518  L_702 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_700)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_701))) = L_702;
		Color32U5BU5D_t30278651* L_703 = __this->get__colors_13();
		int32_t L_704 = V_1;
		NullCheck(L_703);
		Color32_t874517518  L_705 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_703)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_704+(int32_t)1))))) = L_705;
		Color32U5BU5D_t30278651* L_706 = __this->get__colors_13();
		int32_t L_707 = V_1;
		NullCheck(L_706);
		Color32_t874517518  L_708 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_706)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_707+(int32_t)2))))) = L_708;
		Color32U5BU5D_t30278651* L_709 = __this->get__colors_13();
		int32_t L_710 = V_1;
		NullCheck(L_709);
		Color32_t874517518  L_711 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_709)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_710+(int32_t)3))))) = L_711;
		int32_t L_712 = V_0;
		V_0 = ((int32_t)((int32_t)L_712+(int32_t)1));
		int32_t L_713 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_713));
		int32_t L_714 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_714));
	}

IL_1327:
	{
		VoxelSpace_t930815766 * L_715 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_715);
		bool L_716 = L_715->get_DrawTop_14();
		if (!L_716)
		{
			goto IL_1529;
		}
	}
	{
		VoxelTemplate_t594004422 * L_717 = __this->get_voxelType_22();
		NullCheck(L_717);
		bool L_718 = L_717->get_drawTop_16();
		if (!L_718)
		{
			goto IL_1529;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_719 = __this->get__verts_11();
		int32_t L_720 = V_1;
		NullCheck(L_719);
		int32_t L_721 = V_3;
		int32_t L_722 = V_4;
		int32_t L_723 = V_5;
		Vector3_t2243707580  L_724;
		memset(&L_724, 0, sizeof(L_724));
		Vector3__ctor_m2638739322((&L_724), (((float)((float)L_721))), ((float)((float)(((float)((float)L_722)))+(float)(0.5f))), (((float)((float)L_723))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_719)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_720))) = L_724;
		Vector3U5BU5D_t1172311765* L_725 = __this->get__verts_11();
		int32_t L_726 = V_1;
		NullCheck(L_725);
		int32_t L_727 = V_3;
		int32_t L_728 = V_4;
		int32_t L_729 = V_5;
		Vector3_t2243707580  L_730;
		memset(&L_730, 0, sizeof(L_730));
		Vector3__ctor_m2638739322((&L_730), (((float)((float)L_727))), ((float)((float)(((float)((float)L_728)))+(float)(0.5f))), (((float)((float)((int32_t)((int32_t)L_729+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_725)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_726+(int32_t)1))))) = L_730;
		Vector3U5BU5D_t1172311765* L_731 = __this->get__verts_11();
		int32_t L_732 = V_1;
		NullCheck(L_731);
		int32_t L_733 = V_3;
		int32_t L_734 = V_4;
		int32_t L_735 = V_5;
		Vector3_t2243707580  L_736;
		memset(&L_736, 0, sizeof(L_736));
		Vector3__ctor_m2638739322((&L_736), (((float)((float)((int32_t)((int32_t)L_733+(int32_t)1))))), ((float)((float)(((float)((float)L_734)))+(float)(0.5f))), (((float)((float)((int32_t)((int32_t)L_735+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_731)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_732+(int32_t)2))))) = L_736;
		Vector3U5BU5D_t1172311765* L_737 = __this->get__verts_11();
		int32_t L_738 = V_1;
		NullCheck(L_737);
		int32_t L_739 = V_3;
		int32_t L_740 = V_4;
		int32_t L_741 = V_5;
		Vector3_t2243707580  L_742;
		memset(&L_742, 0, sizeof(L_742));
		Vector3__ctor_m2638739322((&L_742), (((float)((float)((int32_t)((int32_t)L_739+(int32_t)1))))), ((float)((float)(((float)((float)L_740)))+(float)(0.5f))), (((float)((float)L_741))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_737)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_738+(int32_t)3))))) = L_742;
		Int32U5BU5D_t3030399641* L_743 = __this->get__tris_15();
		int32_t L_744 = V_2;
		int32_t L_745 = V_1;
		NullCheck(L_743);
		(L_743)->SetAt(static_cast<il2cpp_array_size_t>(L_744), (int32_t)L_745);
		Int32U5BU5D_t3030399641* L_746 = __this->get__tris_15();
		int32_t L_747 = V_2;
		int32_t L_748 = V_1;
		NullCheck(L_746);
		(L_746)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_747+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_748+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_749 = __this->get__tris_15();
		int32_t L_750 = V_2;
		int32_t L_751 = V_1;
		NullCheck(L_749);
		(L_749)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_750+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_751+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_752 = __this->get__tris_15();
		int32_t L_753 = V_2;
		int32_t L_754 = V_1;
		NullCheck(L_752);
		(L_752)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_753+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_754+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_755 = __this->get__tris_15();
		int32_t L_756 = V_2;
		int32_t L_757 = V_1;
		NullCheck(L_755);
		(L_755)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_756+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_757+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_758 = __this->get__tris_15();
		int32_t L_759 = V_2;
		int32_t L_760 = V_1;
		NullCheck(L_758);
		(L_758)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_759+(int32_t)5))), (int32_t)L_760);
		VoxelTemplate_t594004422 * L_761 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_762 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_761, ((int32_t)116), /*hidden argument*/NULL);
		V_21 = L_762;
		Vector2U5BU5D_t686124026* L_763 = __this->get__uvs_12();
		int32_t L_764 = V_1;
		NullCheck(L_763);
		Vector2U5BU5D_t686124026* L_765 = V_21;
		NullCheck(L_765);
		*(Vector2_t2243707579 *)((L_763)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_764))) = (*(Vector2_t2243707579 *)((L_765)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_766 = __this->get__uvs_12();
		int32_t L_767 = V_1;
		NullCheck(L_766);
		Vector2U5BU5D_t686124026* L_768 = V_21;
		NullCheck(L_768);
		*(Vector2_t2243707579 *)((L_766)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_767+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_768)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_769 = __this->get__uvs_12();
		int32_t L_770 = V_1;
		NullCheck(L_769);
		Vector2U5BU5D_t686124026* L_771 = V_21;
		NullCheck(L_771);
		*(Vector2_t2243707579 *)((L_769)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_770+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_771)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_772 = __this->get__uvs_12();
		int32_t L_773 = V_1;
		NullCheck(L_772);
		Vector2U5BU5D_t686124026* L_774 = V_21;
		NullCheck(L_774);
		*(Vector2_t2243707579 *)((L_772)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_773+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_774)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_775 = __this->get__colors_13();
		int32_t L_776 = V_1;
		NullCheck(L_775);
		Color32_t874517518  L_777 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_775)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_776))) = L_777;
		Color32U5BU5D_t30278651* L_778 = __this->get__colors_13();
		int32_t L_779 = V_1;
		NullCheck(L_778);
		Color32_t874517518  L_780 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_778)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_779+(int32_t)1))))) = L_780;
		Color32U5BU5D_t30278651* L_781 = __this->get__colors_13();
		int32_t L_782 = V_1;
		NullCheck(L_781);
		Color32_t874517518  L_783 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_781)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_782+(int32_t)2))))) = L_783;
		Color32U5BU5D_t30278651* L_784 = __this->get__colors_13();
		int32_t L_785 = V_1;
		NullCheck(L_784);
		Color32_t874517518  L_786 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_784)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_785+(int32_t)3))))) = L_786;
		int32_t L_787 = V_0;
		V_0 = ((int32_t)((int32_t)L_787+(int32_t)1));
		int32_t L_788 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_788));
		int32_t L_789 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_789));
	}

IL_1529:
	{
		VoxelSpace_t930815766 * L_790 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_790);
		bool L_791 = L_790->get_DrawBottom_15();
		if (!L_791)
		{
			goto IL_172b;
		}
	}
	{
		VoxelTemplate_t594004422 * L_792 = __this->get_voxelType_22();
		NullCheck(L_792);
		bool L_793 = L_792->get_drawBottom_17();
		if (!L_793)
		{
			goto IL_172b;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_794 = __this->get__verts_11();
		int32_t L_795 = V_1;
		NullCheck(L_794);
		int32_t L_796 = V_3;
		int32_t L_797 = V_4;
		int32_t L_798 = V_5;
		Vector3_t2243707580  L_799;
		memset(&L_799, 0, sizeof(L_799));
		Vector3__ctor_m2638739322((&L_799), (((float)((float)L_796))), ((float)((float)(((float)((float)L_797)))+(float)(0.5f))), (((float)((float)L_798))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_794)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_795+(int32_t)3))))) = L_799;
		Vector3U5BU5D_t1172311765* L_800 = __this->get__verts_11();
		int32_t L_801 = V_1;
		NullCheck(L_800);
		int32_t L_802 = V_3;
		int32_t L_803 = V_4;
		int32_t L_804 = V_5;
		Vector3_t2243707580  L_805;
		memset(&L_805, 0, sizeof(L_805));
		Vector3__ctor_m2638739322((&L_805), (((float)((float)L_802))), ((float)((float)(((float)((float)L_803)))+(float)(0.5f))), (((float)((float)((int32_t)((int32_t)L_804+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_800)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_801+(int32_t)2))))) = L_805;
		Vector3U5BU5D_t1172311765* L_806 = __this->get__verts_11();
		int32_t L_807 = V_1;
		NullCheck(L_806);
		int32_t L_808 = V_3;
		int32_t L_809 = V_4;
		int32_t L_810 = V_5;
		Vector3_t2243707580  L_811;
		memset(&L_811, 0, sizeof(L_811));
		Vector3__ctor_m2638739322((&L_811), (((float)((float)((int32_t)((int32_t)L_808+(int32_t)1))))), ((float)((float)(((float)((float)L_809)))+(float)(0.5f))), (((float)((float)((int32_t)((int32_t)L_810+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_806)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_807+(int32_t)1))))) = L_811;
		Vector3U5BU5D_t1172311765* L_812 = __this->get__verts_11();
		int32_t L_813 = V_1;
		NullCheck(L_812);
		int32_t L_814 = V_3;
		int32_t L_815 = V_4;
		int32_t L_816 = V_5;
		Vector3_t2243707580  L_817;
		memset(&L_817, 0, sizeof(L_817));
		Vector3__ctor_m2638739322((&L_817), (((float)((float)((int32_t)((int32_t)L_814+(int32_t)1))))), ((float)((float)(((float)((float)L_815)))+(float)(0.5f))), (((float)((float)L_816))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_812)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_813))) = L_817;
		Int32U5BU5D_t3030399641* L_818 = __this->get__tris_15();
		int32_t L_819 = V_2;
		int32_t L_820 = V_1;
		NullCheck(L_818);
		(L_818)->SetAt(static_cast<il2cpp_array_size_t>(L_819), (int32_t)L_820);
		Int32U5BU5D_t3030399641* L_821 = __this->get__tris_15();
		int32_t L_822 = V_2;
		int32_t L_823 = V_1;
		NullCheck(L_821);
		(L_821)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_822+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_823+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_824 = __this->get__tris_15();
		int32_t L_825 = V_2;
		int32_t L_826 = V_1;
		NullCheck(L_824);
		(L_824)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_825+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_826+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_827 = __this->get__tris_15();
		int32_t L_828 = V_2;
		int32_t L_829 = V_1;
		NullCheck(L_827);
		(L_827)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_828+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_829+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_830 = __this->get__tris_15();
		int32_t L_831 = V_2;
		int32_t L_832 = V_1;
		NullCheck(L_830);
		(L_830)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_831+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_832+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_833 = __this->get__tris_15();
		int32_t L_834 = V_2;
		int32_t L_835 = V_1;
		NullCheck(L_833);
		(L_833)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_834+(int32_t)5))), (int32_t)L_835);
		VoxelTemplate_t594004422 * L_836 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_837 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_836, ((int32_t)98), /*hidden argument*/NULL);
		V_22 = L_837;
		Vector2U5BU5D_t686124026* L_838 = __this->get__uvs_12();
		int32_t L_839 = V_1;
		NullCheck(L_838);
		Vector2U5BU5D_t686124026* L_840 = V_22;
		NullCheck(L_840);
		*(Vector2_t2243707579 *)((L_838)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_839))) = (*(Vector2_t2243707579 *)((L_840)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_841 = __this->get__uvs_12();
		int32_t L_842 = V_1;
		NullCheck(L_841);
		Vector2U5BU5D_t686124026* L_843 = V_22;
		NullCheck(L_843);
		*(Vector2_t2243707579 *)((L_841)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_842+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_843)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_844 = __this->get__uvs_12();
		int32_t L_845 = V_1;
		NullCheck(L_844);
		Vector2U5BU5D_t686124026* L_846 = V_22;
		NullCheck(L_846);
		*(Vector2_t2243707579 *)((L_844)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_845+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_846)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_847 = __this->get__uvs_12();
		int32_t L_848 = V_1;
		NullCheck(L_847);
		Vector2U5BU5D_t686124026* L_849 = V_22;
		NullCheck(L_849);
		*(Vector2_t2243707579 *)((L_847)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_848+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_849)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_850 = __this->get__colors_13();
		int32_t L_851 = V_1;
		NullCheck(L_850);
		Color32_t874517518  L_852 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_850)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_851))) = L_852;
		Color32U5BU5D_t30278651* L_853 = __this->get__colors_13();
		int32_t L_854 = V_1;
		NullCheck(L_853);
		Color32_t874517518  L_855 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_853)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_854+(int32_t)1))))) = L_855;
		Color32U5BU5D_t30278651* L_856 = __this->get__colors_13();
		int32_t L_857 = V_1;
		NullCheck(L_856);
		Color32_t874517518  L_858 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_856)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_857+(int32_t)2))))) = L_858;
		Color32U5BU5D_t30278651* L_859 = __this->get__colors_13();
		int32_t L_860 = V_1;
		NullCheck(L_859);
		Color32_t874517518  L_861 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_859)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_860+(int32_t)3))))) = L_861;
		int32_t L_862 = V_0;
		V_0 = ((int32_t)((int32_t)L_862+(int32_t)1));
		int32_t L_863 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_863));
		int32_t L_864 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_864));
	}

IL_172b:
	{
		VoxelSpace_t930815766 * L_865 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_865);
		bool L_866 = L_865->get_DrawRight_17();
		if (!L_866)
		{
			goto IL_192d;
		}
	}
	{
		VoxelTemplate_t594004422 * L_867 = __this->get_voxelType_22();
		NullCheck(L_867);
		bool L_868 = L_867->get_drawRight_19();
		if (!L_868)
		{
			goto IL_192d;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_869 = __this->get__verts_11();
		int32_t L_870 = V_1;
		NullCheck(L_869);
		int32_t L_871 = V_3;
		int32_t L_872 = V_4;
		int32_t L_873 = V_5;
		Vector3_t2243707580  L_874;
		memset(&L_874, 0, sizeof(L_874));
		Vector3__ctor_m2638739322((&L_874), ((float)((float)(((float)((float)L_871)))+(float)(0.5f))), (((float)((float)L_872))), (((float)((float)L_873))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_869)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_870))) = L_874;
		Vector3U5BU5D_t1172311765* L_875 = __this->get__verts_11();
		int32_t L_876 = V_1;
		NullCheck(L_875);
		int32_t L_877 = V_3;
		int32_t L_878 = V_4;
		int32_t L_879 = V_5;
		Vector3_t2243707580  L_880;
		memset(&L_880, 0, sizeof(L_880));
		Vector3__ctor_m2638739322((&L_880), ((float)((float)(((float)((float)L_877)))+(float)(0.5f))), (((float)((float)((int32_t)((int32_t)L_878+(int32_t)1))))), (((float)((float)L_879))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_875)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_876+(int32_t)1))))) = L_880;
		Vector3U5BU5D_t1172311765* L_881 = __this->get__verts_11();
		int32_t L_882 = V_1;
		NullCheck(L_881);
		int32_t L_883 = V_3;
		int32_t L_884 = V_4;
		int32_t L_885 = V_5;
		Vector3_t2243707580  L_886;
		memset(&L_886, 0, sizeof(L_886));
		Vector3__ctor_m2638739322((&L_886), ((float)((float)(((float)((float)L_883)))+(float)(0.5f))), (((float)((float)((int32_t)((int32_t)L_884+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_885+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_881)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_882+(int32_t)2))))) = L_886;
		Vector3U5BU5D_t1172311765* L_887 = __this->get__verts_11();
		int32_t L_888 = V_1;
		NullCheck(L_887);
		int32_t L_889 = V_3;
		int32_t L_890 = V_4;
		int32_t L_891 = V_5;
		Vector3_t2243707580  L_892;
		memset(&L_892, 0, sizeof(L_892));
		Vector3__ctor_m2638739322((&L_892), ((float)((float)(((float)((float)L_889)))+(float)(0.5f))), (((float)((float)L_890))), (((float)((float)((int32_t)((int32_t)L_891+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_887)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_888+(int32_t)3))))) = L_892;
		Int32U5BU5D_t3030399641* L_893 = __this->get__tris_15();
		int32_t L_894 = V_2;
		int32_t L_895 = V_1;
		NullCheck(L_893);
		(L_893)->SetAt(static_cast<il2cpp_array_size_t>(L_894), (int32_t)L_895);
		Int32U5BU5D_t3030399641* L_896 = __this->get__tris_15();
		int32_t L_897 = V_2;
		int32_t L_898 = V_1;
		NullCheck(L_896);
		(L_896)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_897+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_898+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_899 = __this->get__tris_15();
		int32_t L_900 = V_2;
		int32_t L_901 = V_1;
		NullCheck(L_899);
		(L_899)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_900+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_901+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_902 = __this->get__tris_15();
		int32_t L_903 = V_2;
		int32_t L_904 = V_1;
		NullCheck(L_902);
		(L_902)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_903+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_904+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_905 = __this->get__tris_15();
		int32_t L_906 = V_2;
		int32_t L_907 = V_1;
		NullCheck(L_905);
		(L_905)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_906+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_907+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_908 = __this->get__tris_15();
		int32_t L_909 = V_2;
		int32_t L_910 = V_1;
		NullCheck(L_908);
		(L_908)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_909+(int32_t)5))), (int32_t)L_910);
		VoxelTemplate_t594004422 * L_911 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_912 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_911, ((int32_t)114), /*hidden argument*/NULL);
		V_23 = L_912;
		Vector2U5BU5D_t686124026* L_913 = __this->get__uvs_12();
		int32_t L_914 = V_1;
		NullCheck(L_913);
		Vector2U5BU5D_t686124026* L_915 = V_23;
		NullCheck(L_915);
		*(Vector2_t2243707579 *)((L_913)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_914))) = (*(Vector2_t2243707579 *)((L_915)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_916 = __this->get__uvs_12();
		int32_t L_917 = V_1;
		NullCheck(L_916);
		Vector2U5BU5D_t686124026* L_918 = V_23;
		NullCheck(L_918);
		*(Vector2_t2243707579 *)((L_916)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_917+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_918)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_919 = __this->get__uvs_12();
		int32_t L_920 = V_1;
		NullCheck(L_919);
		Vector2U5BU5D_t686124026* L_921 = V_23;
		NullCheck(L_921);
		*(Vector2_t2243707579 *)((L_919)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_920+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_921)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_922 = __this->get__uvs_12();
		int32_t L_923 = V_1;
		NullCheck(L_922);
		Vector2U5BU5D_t686124026* L_924 = V_23;
		NullCheck(L_924);
		*(Vector2_t2243707579 *)((L_922)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_923+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_924)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_925 = __this->get__colors_13();
		int32_t L_926 = V_1;
		NullCheck(L_925);
		Color32_t874517518  L_927 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_925)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_926))) = L_927;
		Color32U5BU5D_t30278651* L_928 = __this->get__colors_13();
		int32_t L_929 = V_1;
		NullCheck(L_928);
		Color32_t874517518  L_930 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_928)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_929+(int32_t)1))))) = L_930;
		Color32U5BU5D_t30278651* L_931 = __this->get__colors_13();
		int32_t L_932 = V_1;
		NullCheck(L_931);
		Color32_t874517518  L_933 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_931)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_932+(int32_t)2))))) = L_933;
		Color32U5BU5D_t30278651* L_934 = __this->get__colors_13();
		int32_t L_935 = V_1;
		NullCheck(L_934);
		Color32_t874517518  L_936 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_934)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_935+(int32_t)3))))) = L_936;
		int32_t L_937 = V_0;
		V_0 = ((int32_t)((int32_t)L_937+(int32_t)1));
		int32_t L_938 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_938));
		int32_t L_939 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_939));
	}

IL_192d:
	{
		VoxelSpace_t930815766 * L_940 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_940);
		bool L_941 = L_940->get_DrawLeft_16();
		if (!L_941)
		{
			goto IL_1b2f;
		}
	}
	{
		VoxelTemplate_t594004422 * L_942 = __this->get_voxelType_22();
		NullCheck(L_942);
		bool L_943 = L_942->get_drawLeft_18();
		if (!L_943)
		{
			goto IL_1b2f;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_944 = __this->get__verts_11();
		int32_t L_945 = V_1;
		NullCheck(L_944);
		int32_t L_946 = V_3;
		int32_t L_947 = V_4;
		int32_t L_948 = V_5;
		Vector3_t2243707580  L_949;
		memset(&L_949, 0, sizeof(L_949));
		Vector3__ctor_m2638739322((&L_949), ((float)((float)(((float)((float)L_946)))+(float)(0.5f))), (((float)((float)L_947))), (((float)((float)L_948))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_944)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_945+(int32_t)3))))) = L_949;
		Vector3U5BU5D_t1172311765* L_950 = __this->get__verts_11();
		int32_t L_951 = V_1;
		NullCheck(L_950);
		int32_t L_952 = V_3;
		int32_t L_953 = V_4;
		int32_t L_954 = V_5;
		Vector3_t2243707580  L_955;
		memset(&L_955, 0, sizeof(L_955));
		Vector3__ctor_m2638739322((&L_955), ((float)((float)(((float)((float)L_952)))+(float)(0.5f))), (((float)((float)((int32_t)((int32_t)L_953+(int32_t)1))))), (((float)((float)L_954))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_950)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_951+(int32_t)2))))) = L_955;
		Vector3U5BU5D_t1172311765* L_956 = __this->get__verts_11();
		int32_t L_957 = V_1;
		NullCheck(L_956);
		int32_t L_958 = V_3;
		int32_t L_959 = V_4;
		int32_t L_960 = V_5;
		Vector3_t2243707580  L_961;
		memset(&L_961, 0, sizeof(L_961));
		Vector3__ctor_m2638739322((&L_961), ((float)((float)(((float)((float)L_958)))+(float)(0.5f))), (((float)((float)((int32_t)((int32_t)L_959+(int32_t)1))))), (((float)((float)((int32_t)((int32_t)L_960+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_956)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_957+(int32_t)1))))) = L_961;
		Vector3U5BU5D_t1172311765* L_962 = __this->get__verts_11();
		int32_t L_963 = V_1;
		NullCheck(L_962);
		int32_t L_964 = V_3;
		int32_t L_965 = V_4;
		int32_t L_966 = V_5;
		Vector3_t2243707580  L_967;
		memset(&L_967, 0, sizeof(L_967));
		Vector3__ctor_m2638739322((&L_967), ((float)((float)(((float)((float)L_964)))+(float)(0.5f))), (((float)((float)L_965))), (((float)((float)((int32_t)((int32_t)L_966+(int32_t)1))))), /*hidden argument*/NULL);
		*(Vector3_t2243707580 *)((L_962)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_963))) = L_967;
		Int32U5BU5D_t3030399641* L_968 = __this->get__tris_15();
		int32_t L_969 = V_2;
		int32_t L_970 = V_1;
		NullCheck(L_968);
		(L_968)->SetAt(static_cast<il2cpp_array_size_t>(L_969), (int32_t)L_970);
		Int32U5BU5D_t3030399641* L_971 = __this->get__tris_15();
		int32_t L_972 = V_2;
		int32_t L_973 = V_1;
		NullCheck(L_971);
		(L_971)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_972+(int32_t)1))), (int32_t)((int32_t)((int32_t)L_973+(int32_t)1)));
		Int32U5BU5D_t3030399641* L_974 = __this->get__tris_15();
		int32_t L_975 = V_2;
		int32_t L_976 = V_1;
		NullCheck(L_974);
		(L_974)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_975+(int32_t)2))), (int32_t)((int32_t)((int32_t)L_976+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_977 = __this->get__tris_15();
		int32_t L_978 = V_2;
		int32_t L_979 = V_1;
		NullCheck(L_977);
		(L_977)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_978+(int32_t)3))), (int32_t)((int32_t)((int32_t)L_979+(int32_t)2)));
		Int32U5BU5D_t3030399641* L_980 = __this->get__tris_15();
		int32_t L_981 = V_2;
		int32_t L_982 = V_1;
		NullCheck(L_980);
		(L_980)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_981+(int32_t)4))), (int32_t)((int32_t)((int32_t)L_982+(int32_t)3)));
		Int32U5BU5D_t3030399641* L_983 = __this->get__tris_15();
		int32_t L_984 = V_2;
		int32_t L_985 = V_1;
		NullCheck(L_983);
		(L_983)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_984+(int32_t)5))), (int32_t)L_985);
		VoxelTemplate_t594004422 * L_986 = __this->get_voxelType_22();
		Vector2U5BU5D_t686124026* L_987 = VoxelStructure_GetVoxelUvs_m4193062824(__this, L_986, ((int32_t)108), /*hidden argument*/NULL);
		V_24 = L_987;
		Vector2U5BU5D_t686124026* L_988 = __this->get__uvs_12();
		int32_t L_989 = V_1;
		NullCheck(L_988);
		Vector2U5BU5D_t686124026* L_990 = V_24;
		NullCheck(L_990);
		*(Vector2_t2243707579 *)((L_988)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_989))) = (*(Vector2_t2243707579 *)((L_990)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Vector2U5BU5D_t686124026* L_991 = __this->get__uvs_12();
		int32_t L_992 = V_1;
		NullCheck(L_991);
		Vector2U5BU5D_t686124026* L_993 = V_24;
		NullCheck(L_993);
		*(Vector2_t2243707579 *)((L_991)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_992+(int32_t)1))))) = (*(Vector2_t2243707579 *)((L_993)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))));
		Vector2U5BU5D_t686124026* L_994 = __this->get__uvs_12();
		int32_t L_995 = V_1;
		NullCheck(L_994);
		Vector2U5BU5D_t686124026* L_996 = V_24;
		NullCheck(L_996);
		*(Vector2_t2243707579 *)((L_994)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_995+(int32_t)2))))) = (*(Vector2_t2243707579 *)((L_996)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))));
		Vector2U5BU5D_t686124026* L_997 = __this->get__uvs_12();
		int32_t L_998 = V_1;
		NullCheck(L_997);
		Vector2U5BU5D_t686124026* L_999 = V_24;
		NullCheck(L_999);
		*(Vector2_t2243707579 *)((L_997)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_998+(int32_t)3))))) = (*(Vector2_t2243707579 *)((L_999)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))));
		Color32U5BU5D_t30278651* L_1000 = __this->get__colors_13();
		int32_t L_1001 = V_1;
		NullCheck(L_1000);
		Color32_t874517518  L_1002 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_1000)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_1001))) = L_1002;
		Color32U5BU5D_t30278651* L_1003 = __this->get__colors_13();
		int32_t L_1004 = V_1;
		NullCheck(L_1003);
		Color32_t874517518  L_1005 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_1003)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_1004+(int32_t)1))))) = L_1005;
		Color32U5BU5D_t30278651* L_1006 = __this->get__colors_13();
		int32_t L_1007 = V_1;
		NullCheck(L_1006);
		Color32_t874517518  L_1008 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_1006)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_1007+(int32_t)2))))) = L_1008;
		Color32U5BU5D_t30278651* L_1009 = __this->get__colors_13();
		int32_t L_1010 = V_1;
		NullCheck(L_1009);
		Color32_t874517518  L_1011 = __this->get_voxelColor_23();
		*(Color32_t874517518 *)((L_1009)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_1010+(int32_t)3))))) = L_1011;
		int32_t L_1012 = V_0;
		V_0 = ((int32_t)((int32_t)L_1012+(int32_t)1));
		int32_t L_1013 = V_0;
		V_1 = ((int32_t)((int32_t)4*(int32_t)L_1013));
		int32_t L_1014 = V_0;
		V_2 = ((int32_t)((int32_t)6*(int32_t)L_1014));
	}

IL_1b2f:
	{
		int32_t L_1015 = V_5;
		V_5 = ((int32_t)((int32_t)L_1015+(int32_t)1));
	}

IL_1b35:
	{
		int32_t L_1016 = V_5;
		int32_t L_1017 = __this->get_Depth_10();
		if ((((int32_t)L_1016) < ((int32_t)L_1017)))
		{
			goto IL_0159;
		}
	}
	{
		int32_t L_1018 = V_4;
		V_4 = ((int32_t)((int32_t)L_1018+(int32_t)1));
	}

IL_1b48:
	{
		int32_t L_1019 = V_4;
		int32_t L_1020 = __this->get_Height_9();
		if ((((int32_t)L_1019) < ((int32_t)L_1020)))
		{
			goto IL_0151;
		}
	}
	{
		int32_t L_1021 = V_3;
		V_3 = ((int32_t)((int32_t)L_1021+(int32_t)1));
	}

IL_1b59:
	{
		int32_t L_1022 = V_3;
		int32_t L_1023 = __this->get_Width_8();
		if ((((int32_t)L_1022) < ((int32_t)L_1023)))
		{
			goto IL_0149;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_1024 = __this->get__verts_11();
		int32_t L_1025 = V_0;
		RuntimeObject* L_1026 = Enumerable_Take_TisVector3_t2243707580_m1945563610(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_1024, ((int32_t)((int32_t)L_1025*(int32_t)4)), /*hidden argument*/Enumerable_Take_TisVector3_t2243707580_m1945563610_RuntimeMethod_var);
		Vector3U5BU5D_t1172311765* L_1027 = Enumerable_ToArray_TisVector3_t2243707580_m1930596601(NULL /*static, unused*/, L_1026, /*hidden argument*/Enumerable_ToArray_TisVector3_t2243707580_m1930596601_RuntimeMethod_var);
		V_25 = L_1027;
		Vector3U5BU5D_t1172311765* L_1028 = V_25;
		NullCheck(L_1028);
		if ((((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_1028)->max_length))))) >= ((int32_t)((int32_t)64000))))
		{
			goto IL_1c8a;
		}
	}
	{
		VoxelSpace_t930815766 * L_1029 = VoxelStructure_get_VoxelSpace_m721980576(__this, /*hidden argument*/NULL);
		NullCheck(L_1029);
		bool L_1030 = L_1029->get_UsePalletteTexture_19();
		if (!L_1030)
		{
			goto IL_1bd3;
		}
	}
	{
		VoxelPallette_t4284593999 * L_1031 = __this->get_pallette_2();
		NullCheck(L_1031);
		Material_t193706927 * L_1032 = L_1031->get_AtlasMaterial_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1033 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1032, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1033)
		{
			goto IL_1bc9;
		}
	}
	{
		Renderer_t257310565 * L_1034 = Component_GetComponent_TisRenderer_t257310565_m772028041(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t257310565_m772028041_RuntimeMethod_var);
		VoxelPallette_t4284593999 * L_1035 = __this->get_pallette_2();
		NullCheck(L_1035);
		Material_t193706927 * L_1036 = L_1035->get_AtlasMaterial_0();
		NullCheck(L_1034);
		Renderer_set_material_m1053097112(L_1034, L_1036, /*hidden argument*/NULL);
		goto IL_1bd3;
	}

IL_1bc9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral2176050822, /*hidden argument*/NULL);
	}

IL_1bd3:
	{
		Int32U5BU5D_t3030399641* L_1037 = __this->get__tris_15();
		int32_t L_1038 = V_0;
		RuntimeObject* L_1039 = Enumerable_Take_TisInt32_t2071877448_m1592882544(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_1037, ((int32_t)((int32_t)L_1038*(int32_t)6)), /*hidden argument*/Enumerable_Take_TisInt32_t2071877448_m1592882544_RuntimeMethod_var);
		Int32U5BU5D_t3030399641* L_1040 = Enumerable_ToArray_TisInt32_t2071877448_m513246933(NULL /*static, unused*/, L_1039, /*hidden argument*/Enumerable_ToArray_TisInt32_t2071877448_m513246933_RuntimeMethod_var);
		V_26 = L_1040;
		Vector2U5BU5D_t686124026* L_1041 = __this->get__uvs_12();
		int32_t L_1042 = V_0;
		RuntimeObject* L_1043 = Enumerable_Take_TisVector2_t2243707579_m1080133109(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_1041, ((int32_t)((int32_t)L_1042*(int32_t)4)), /*hidden argument*/Enumerable_Take_TisVector2_t2243707579_m1080133109_RuntimeMethod_var);
		Vector2U5BU5D_t686124026* L_1044 = Enumerable_ToArray_TisVector2_t2243707579_m1715071610(NULL /*static, unused*/, L_1043, /*hidden argument*/Enumerable_ToArray_TisVector2_t2243707579_m1715071610_RuntimeMethod_var);
		V_27 = L_1044;
		Color32U5BU5D_t30278651* L_1045 = __this->get__colors_13();
		int32_t L_1046 = V_0;
		RuntimeObject* L_1047 = Enumerable_Take_TisColor32_t874517518_m543186848(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_1045, ((int32_t)((int32_t)L_1046*(int32_t)4)), /*hidden argument*/Enumerable_Take_TisColor32_t874517518_m543186848_RuntimeMethod_var);
		Color32U5BU5D_t30278651* L_1048 = Enumerable_ToArray_TisColor32_t874517518_m1885434799(NULL /*static, unused*/, L_1047, /*hidden argument*/Enumerable_ToArray_TisColor32_t874517518_m1885434799_RuntimeMethod_var);
		V_28 = L_1048;
		MeshFilter_t3026937449 * L_1049 = __this->get__mf_14();
		NullCheck(L_1049);
		Mesh_t1356156583 * L_1050 = MeshFilter_get_sharedMesh_m1310789932(L_1049, /*hidden argument*/NULL);
		V_29 = L_1050;
		Mesh_t1356156583 * L_1051 = V_29;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1052 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1051, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1052)
		{
			goto IL_1c38;
		}
	}
	{
		Mesh_t1356156583 * L_1053 = V_29;
		NullCheck(L_1053);
		Mesh_Clear_m231813403(L_1053, /*hidden argument*/NULL);
		goto IL_1c3f;
	}

IL_1c38:
	{
		Mesh_t1356156583 * L_1054 = (Mesh_t1356156583 *)il2cpp_codegen_object_new(Mesh_t1356156583_il2cpp_TypeInfo_var);
		Mesh__ctor_m2975981674(L_1054, /*hidden argument*/NULL);
		V_29 = L_1054;
	}

IL_1c3f:
	{
		Mesh_t1356156583 * L_1055 = V_29;
		Vector3U5BU5D_t1172311765* L_1056 = V_25;
		NullCheck(L_1055);
		Mesh_set_vertices_m2936804213(L_1055, L_1056, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_1057 = V_29;
		Int32U5BU5D_t3030399641* L_1058 = V_26;
		NullCheck(L_1057);
		Mesh_set_triangles_m3244966865(L_1057, L_1058, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_1059 = V_29;
		Vector2U5BU5D_t686124026* L_1060 = V_27;
		NullCheck(L_1059);
		Mesh_set_uv_m1497318906(L_1059, L_1060, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_1061 = V_29;
		Color32U5BU5D_t30278651* L_1062 = V_28;
		NullCheck(L_1061);
		Mesh_set_colors32_m1066151745(L_1061, L_1062, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_1063 = V_29;
		NullCheck(L_1063);
		Mesh_RecalculateBounds_m3559909024(L_1063, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_1064 = V_29;
		NullCheck(L_1064);
		Mesh_RecalculateNormals_m1034493793(L_1064, /*hidden argument*/NULL);
		Mesh_t1356156583 * L_1065 = V_29;
		NullCheck(L_1065);
		Mesh_RecalculateTangents_m2877337761(L_1065, /*hidden argument*/NULL);
		MeshFilter_t3026937449 * L_1066 = __this->get__mf_14();
		Mesh_t1356156583 * L_1067 = V_29;
		NullCheck(L_1066);
		MeshFilter_set_sharedMesh_m2225370173(L_1066, L_1067, /*hidden argument*/NULL);
		goto IL_1c94;
	}

IL_1c8a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3332643267, /*hidden argument*/NULL);
	}

IL_1c94:
	{
		return;
	}
}
// System.Int32 VoxelStructure::GetVoxel(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t VoxelStructure_GetVoxel_m3191480667 (VoxelStructure_t2247263099 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = __this->get_Width_8();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_2 = ___x0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = ___y1;
		int32_t L_4 = __this->get_Height_9();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_5 = ___y1;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_6 = ___z2;
		int32_t L_7 = __this->get_Depth_10();
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_8 = ___z2;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_003b;
		}
	}

IL_0039:
	{
		return (-1);
	}

IL_003b:
	{
		Int32U5B0___U2C0___U2C0___U5D_t3030399643* L_9 = __this->get_flatVoxels_7();
		int32_t L_10 = ___x0;
		int32_t L_11 = ___y1;
		int32_t L_12 = ___z2;
		NullCheck((Int32U5B0___U2C0___U2C0___U5D_t3030399643*)(Int32U5B0___U2C0___U2C0___U5D_t3030399643*)L_9);
		int32_t L_13 = ((Int32U5B0___U2C0___U2C0___U5D_t3030399643*)(Int32U5B0___U2C0___U2C0___U5D_t3030399643*)L_9)->GetAt(L_10, L_11, L_12);
		return L_13;
	}
}
// System.Void VoxelStructure::SetVoxel(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VoxelStructure_SetVoxel_m945962940 (VoxelStructure_t2247263099 * __this, int32_t ___x0, int32_t ___y1, int32_t ___z2, int32_t ___newVoxel3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___x0;
		int32_t L_1 = __this->get_Width_8();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_2 = ___x0;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_3 = ___y1;
		int32_t L_4 = __this->get_Height_9();
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_5 = ___y1;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_6 = ___z2;
		int32_t L_7 = __this->get_Depth_10();
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_8 = ___z2;
		if ((((int32_t)L_8) >= ((int32_t)0)))
		{
			goto IL_003a;
		}
	}

IL_0039:
	{
		return;
	}

IL_003a:
	{
		__this->set_IsDirty_24((bool)1);
		Int32U5B0___U2C0___U2C0___U5D_t3030399643* L_9 = __this->get_flatVoxels_7();
		int32_t L_10 = ___x0;
		int32_t L_11 = ___y1;
		int32_t L_12 = ___z2;
		int32_t L_13 = ___newVoxel3;
		NullCheck((Int32U5B0___U2C0___U2C0___U5D_t3030399643*)(Int32U5B0___U2C0___U2C0___U5D_t3030399643*)L_9);
		((Int32U5B0___U2C0___U2C0___U5D_t3030399643*)(Int32U5B0___U2C0___U2C0___U5D_t3030399643*)L_9)->SetAt(L_10, L_11, L_12, L_13);
		return;
	}
}
// System.Void VoxelStructure::MaxExtrude(Vector3i,UnityEngine.Vector3,System.Int32)
extern "C"  void VoxelStructure_MaxExtrude_m1439246306 (VoxelStructure_t2247263099 * __this, Vector3i_t3488636705  ___v0, Vector3_t2243707580  ___dir1, int32_t ___selectedVoxelType2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelStructure_MaxExtrude_m1439246306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3i_t3488636705  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Vector3i_t3488636705  L_0 = ___v0;
		Vector3_t2243707580  L_1 = ___dir1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3i_t3488636705_il2cpp_TypeInfo_var);
		Vector3i_t3488636705  L_2 = Vector3i_op_Addition_m1178975490(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = (&___v0)->get_x_0();
		int32_t L_4 = (&___v0)->get_y_1();
		int32_t L_5 = (&___v0)->get_z_2();
		int32_t L_6 = VoxelStructure_GetVoxel_m3191480667(__this, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0069;
	}

IL_0029:
	{
		int32_t L_7 = (&___v0)->get_x_0();
		int32_t L_8 = (&___v0)->get_y_1();
		int32_t L_9 = (&___v0)->get_z_2();
		int32_t L_10 = ___selectedVoxelType2;
		VoxelStructure_SetVoxel_m945962940(__this, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		Vector3i_t3488636705  L_11 = V_0;
		Vector3_t2243707580  L_12 = ___dir1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3i_t3488636705_il2cpp_TypeInfo_var);
		Vector3i_t3488636705  L_13 = Vector3i_op_Addition_m1178975490(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = (&___v0)->get_x_0();
		int32_t L_15 = (&___v0)->get_y_1();
		int32_t L_16 = (&___v0)->get_z_2();
		int32_t L_17 = VoxelStructure_GetVoxel_m3191480667(__this, L_14, L_15, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
	}

IL_0069:
	{
		int32_t L_18 = V_1;
		if (!L_18)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}
}
// System.Void VoxelStructure::FloodBasic(System.Int32,System.Int32)
extern "C"  void VoxelStructure_FloodBasic_m2273882588 (VoxelStructure_t2247263099 * __this, int32_t ___y0, int32_t ___selectedVoxelType1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_003a;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_002a;
	}

IL_000e:
	{
		int32_t L_0 = V_0;
		int32_t L_1 = ___y0;
		int32_t L_2 = V_1;
		int32_t L_3 = VoxelStructure_GetVoxel_m3191480667(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0026;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = ___y0;
		int32_t L_6 = V_1;
		int32_t L_7 = ___selectedVoxelType1;
		VoxelStructure_SetVoxel_m945962940(__this, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0026:
	{
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_002a:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = __this->get_Depth_10();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003a:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = __this->get_Width_8();
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void VoxelStructure::ExportMesh()
extern "C"  void VoxelStructure_ExportMesh_m546741485 (VoxelStructure_t2247263099 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// UnityEngine.Vector2[] VoxelStructure::GetVoxelUvs(VoxelTemplate,System.Char)
extern "C"  Vector2U5BU5D_t686124026* VoxelStructure_GetVoxelUvs_m4193062824 (VoxelStructure_t2247263099 * __this, VoxelTemplate_t594004422 * ___voxelType0, Il2CppChar ___dir1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelStructure_GetVoxelUvs_m4193062824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		VoxelPallette_t4284593999 * L_0 = __this->get_pallette_2();
		if (!L_0)
		{
			goto IL_06cb;
		}
	}
	{
		VoxelTemplate_t594004422 * L_1 = ___voxelType0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_06cb;
		}
	}
	{
		Il2CppChar L_3 = ___dir1;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)102))))
		{
			goto IL_002a;
		}
	}
	{
		VoxelTemplate_t594004422 * L_4 = ___voxelType0;
		NullCheck(L_4);
		bool L_5 = L_4->get_useFrontUvsForAllFaces_4();
		if (!L_5)
		{
			goto IL_013f;
		}
	}

IL_002a:
	{
		Vector2U5BU5D_t686124026* L_6 = __this->get__uvs_12();
		NullCheck(L_6);
		VoxelTemplate_t594004422 * L_7 = ___voxelType0;
		NullCheck(L_7);
		Vector2_t2243707579 * L_8 = L_7->get_address_of_UVOffsetFront_7();
		float L_9 = L_8->get_x_0();
		((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_x_0(L_9);
		Vector2U5BU5D_t686124026* L_10 = __this->get__uvs_12();
		NullCheck(L_10);
		VoxelTemplate_t594004422 * L_11 = ___voxelType0;
		NullCheck(L_11);
		Vector2_t2243707579 * L_12 = L_11->get_address_of_UVOffsetFront_7();
		float L_13 = L_12->get_y_1();
		((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_y_1(L_13);
		Vector2U5BU5D_t686124026* L_14 = __this->get__uvs_12();
		NullCheck(L_14);
		VoxelTemplate_t594004422 * L_15 = ___voxelType0;
		NullCheck(L_15);
		Vector2_t2243707579 * L_16 = L_15->get_address_of_UVOffsetFront_7();
		float L_17 = L_16->get_x_0();
		((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_x_0(L_17);
		Vector2U5BU5D_t686124026* L_18 = __this->get__uvs_12();
		NullCheck(L_18);
		VoxelTemplate_t594004422 * L_19 = ___voxelType0;
		NullCheck(L_19);
		Vector2_t2243707579 * L_20 = L_19->get_address_of_UVOffsetFront_7();
		float L_21 = L_20->get_y_1();
		VoxelTemplate_t594004422 * L_22 = ___voxelType0;
		NullCheck(L_22);
		Vector2_t2243707579 * L_23 = L_22->get_address_of_atlasScale_6();
		float L_24 = L_23->get_y_1();
		((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_y_1(((float)((float)L_21+(float)L_24)));
		Vector2U5BU5D_t686124026* L_25 = __this->get__uvs_12();
		NullCheck(L_25);
		VoxelTemplate_t594004422 * L_26 = ___voxelType0;
		NullCheck(L_26);
		Vector2_t2243707579 * L_27 = L_26->get_address_of_UVOffsetFront_7();
		float L_28 = L_27->get_x_0();
		VoxelTemplate_t594004422 * L_29 = ___voxelType0;
		NullCheck(L_29);
		Vector2_t2243707579 * L_30 = L_29->get_address_of_atlasScale_6();
		float L_31 = L_30->get_x_0();
		((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_x_0(((float)((float)L_28+(float)L_31)));
		Vector2U5BU5D_t686124026* L_32 = __this->get__uvs_12();
		NullCheck(L_32);
		VoxelTemplate_t594004422 * L_33 = ___voxelType0;
		NullCheck(L_33);
		Vector2_t2243707579 * L_34 = L_33->get_address_of_UVOffsetFront_7();
		float L_35 = L_34->get_y_1();
		VoxelTemplate_t594004422 * L_36 = ___voxelType0;
		NullCheck(L_36);
		Vector2_t2243707579 * L_37 = L_36->get_address_of_atlasScale_6();
		float L_38 = L_37->get_y_1();
		((L_32)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_y_1(((float)((float)L_35+(float)L_38)));
		Vector2U5BU5D_t686124026* L_39 = __this->get__uvs_12();
		NullCheck(L_39);
		VoxelTemplate_t594004422 * L_40 = ___voxelType0;
		NullCheck(L_40);
		Vector2_t2243707579 * L_41 = L_40->get_address_of_UVOffsetFront_7();
		float L_42 = L_41->get_x_0();
		VoxelTemplate_t594004422 * L_43 = ___voxelType0;
		NullCheck(L_43);
		Vector2_t2243707579 * L_44 = L_43->get_address_of_atlasScale_6();
		float L_45 = L_44->get_x_0();
		((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_x_0(((float)((float)L_42+(float)L_45)));
		Vector2U5BU5D_t686124026* L_46 = __this->get__uvs_12();
		NullCheck(L_46);
		VoxelTemplate_t594004422 * L_47 = ___voxelType0;
		NullCheck(L_47);
		Vector2_t2243707579 * L_48 = L_47->get_address_of_UVOffsetFront_7();
		float L_49 = L_48->get_y_1();
		((L_46)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_y_1(L_49);
		goto IL_06cb;
	}

IL_013f:
	{
		Il2CppChar L_50 = ___dir1;
		if ((!(((uint32_t)L_50) == ((uint32_t)((int32_t)107)))))
		{
			goto IL_025c;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_51 = __this->get__uvs_12();
		NullCheck(L_51);
		VoxelTemplate_t594004422 * L_52 = ___voxelType0;
		NullCheck(L_52);
		Vector2_t2243707579 * L_53 = L_52->get_address_of_UVOffsetBack_8();
		float L_54 = L_53->get_x_0();
		((L_51)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_x_0(L_54);
		Vector2U5BU5D_t686124026* L_55 = __this->get__uvs_12();
		NullCheck(L_55);
		VoxelTemplate_t594004422 * L_56 = ___voxelType0;
		NullCheck(L_56);
		Vector2_t2243707579 * L_57 = L_56->get_address_of_UVOffsetBack_8();
		float L_58 = L_57->get_y_1();
		((L_55)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_y_1(L_58);
		Vector2U5BU5D_t686124026* L_59 = __this->get__uvs_12();
		NullCheck(L_59);
		VoxelTemplate_t594004422 * L_60 = ___voxelType0;
		NullCheck(L_60);
		Vector2_t2243707579 * L_61 = L_60->get_address_of_UVOffsetBack_8();
		float L_62 = L_61->get_x_0();
		((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_x_0(L_62);
		Vector2U5BU5D_t686124026* L_63 = __this->get__uvs_12();
		NullCheck(L_63);
		VoxelTemplate_t594004422 * L_64 = ___voxelType0;
		NullCheck(L_64);
		Vector2_t2243707579 * L_65 = L_64->get_address_of_UVOffsetBack_8();
		float L_66 = L_65->get_y_1();
		VoxelTemplate_t594004422 * L_67 = ___voxelType0;
		NullCheck(L_67);
		Vector2_t2243707579 * L_68 = L_67->get_address_of_atlasScale_6();
		float L_69 = L_68->get_y_1();
		((L_63)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_y_1(((float)((float)L_66+(float)L_69)));
		Vector2U5BU5D_t686124026* L_70 = __this->get__uvs_12();
		NullCheck(L_70);
		VoxelTemplate_t594004422 * L_71 = ___voxelType0;
		NullCheck(L_71);
		Vector2_t2243707579 * L_72 = L_71->get_address_of_UVOffsetBack_8();
		float L_73 = L_72->get_x_0();
		VoxelTemplate_t594004422 * L_74 = ___voxelType0;
		NullCheck(L_74);
		Vector2_t2243707579 * L_75 = L_74->get_address_of_atlasScale_6();
		float L_76 = L_75->get_x_0();
		((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_x_0(((float)((float)L_73+(float)L_76)));
		Vector2U5BU5D_t686124026* L_77 = __this->get__uvs_12();
		NullCheck(L_77);
		VoxelTemplate_t594004422 * L_78 = ___voxelType0;
		NullCheck(L_78);
		Vector2_t2243707579 * L_79 = L_78->get_address_of_UVOffsetBack_8();
		float L_80 = L_79->get_y_1();
		VoxelTemplate_t594004422 * L_81 = ___voxelType0;
		NullCheck(L_81);
		Vector2_t2243707579 * L_82 = L_81->get_address_of_atlasScale_6();
		float L_83 = L_82->get_y_1();
		((L_77)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_y_1(((float)((float)L_80+(float)L_83)));
		Vector2U5BU5D_t686124026* L_84 = __this->get__uvs_12();
		NullCheck(L_84);
		VoxelTemplate_t594004422 * L_85 = ___voxelType0;
		NullCheck(L_85);
		Vector2_t2243707579 * L_86 = L_85->get_address_of_UVOffsetBack_8();
		float L_87 = L_86->get_x_0();
		VoxelTemplate_t594004422 * L_88 = ___voxelType0;
		NullCheck(L_88);
		Vector2_t2243707579 * L_89 = L_88->get_address_of_atlasScale_6();
		float L_90 = L_89->get_x_0();
		((L_84)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_x_0(((float)((float)L_87+(float)L_90)));
		Vector2U5BU5D_t686124026* L_91 = __this->get__uvs_12();
		NullCheck(L_91);
		VoxelTemplate_t594004422 * L_92 = ___voxelType0;
		NullCheck(L_92);
		Vector2_t2243707579 * L_93 = L_92->get_address_of_UVOffsetBack_8();
		float L_94 = L_93->get_y_1();
		((L_91)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_y_1(L_94);
		goto IL_06cb;
	}

IL_025c:
	{
		Il2CppChar L_95 = ___dir1;
		if ((!(((uint32_t)L_95) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0379;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_96 = __this->get__uvs_12();
		NullCheck(L_96);
		VoxelTemplate_t594004422 * L_97 = ___voxelType0;
		NullCheck(L_97);
		Vector2_t2243707579 * L_98 = L_97->get_address_of_UVOffsetLeft_11();
		float L_99 = L_98->get_x_0();
		((L_96)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_x_0(L_99);
		Vector2U5BU5D_t686124026* L_100 = __this->get__uvs_12();
		NullCheck(L_100);
		VoxelTemplate_t594004422 * L_101 = ___voxelType0;
		NullCheck(L_101);
		Vector2_t2243707579 * L_102 = L_101->get_address_of_UVOffsetLeft_11();
		float L_103 = L_102->get_y_1();
		((L_100)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_y_1(L_103);
		Vector2U5BU5D_t686124026* L_104 = __this->get__uvs_12();
		NullCheck(L_104);
		VoxelTemplate_t594004422 * L_105 = ___voxelType0;
		NullCheck(L_105);
		Vector2_t2243707579 * L_106 = L_105->get_address_of_UVOffsetLeft_11();
		float L_107 = L_106->get_x_0();
		((L_104)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_x_0(L_107);
		Vector2U5BU5D_t686124026* L_108 = __this->get__uvs_12();
		NullCheck(L_108);
		VoxelTemplate_t594004422 * L_109 = ___voxelType0;
		NullCheck(L_109);
		Vector2_t2243707579 * L_110 = L_109->get_address_of_UVOffsetLeft_11();
		float L_111 = L_110->get_y_1();
		VoxelTemplate_t594004422 * L_112 = ___voxelType0;
		NullCheck(L_112);
		Vector2_t2243707579 * L_113 = L_112->get_address_of_atlasScale_6();
		float L_114 = L_113->get_y_1();
		((L_108)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_y_1(((float)((float)L_111+(float)L_114)));
		Vector2U5BU5D_t686124026* L_115 = __this->get__uvs_12();
		NullCheck(L_115);
		VoxelTemplate_t594004422 * L_116 = ___voxelType0;
		NullCheck(L_116);
		Vector2_t2243707579 * L_117 = L_116->get_address_of_UVOffsetLeft_11();
		float L_118 = L_117->get_x_0();
		VoxelTemplate_t594004422 * L_119 = ___voxelType0;
		NullCheck(L_119);
		Vector2_t2243707579 * L_120 = L_119->get_address_of_atlasScale_6();
		float L_121 = L_120->get_x_0();
		((L_115)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_x_0(((float)((float)L_118+(float)L_121)));
		Vector2U5BU5D_t686124026* L_122 = __this->get__uvs_12();
		NullCheck(L_122);
		VoxelTemplate_t594004422 * L_123 = ___voxelType0;
		NullCheck(L_123);
		Vector2_t2243707579 * L_124 = L_123->get_address_of_UVOffsetLeft_11();
		float L_125 = L_124->get_y_1();
		VoxelTemplate_t594004422 * L_126 = ___voxelType0;
		NullCheck(L_126);
		Vector2_t2243707579 * L_127 = L_126->get_address_of_atlasScale_6();
		float L_128 = L_127->get_y_1();
		((L_122)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_y_1(((float)((float)L_125+(float)L_128)));
		Vector2U5BU5D_t686124026* L_129 = __this->get__uvs_12();
		NullCheck(L_129);
		VoxelTemplate_t594004422 * L_130 = ___voxelType0;
		NullCheck(L_130);
		Vector2_t2243707579 * L_131 = L_130->get_address_of_UVOffsetLeft_11();
		float L_132 = L_131->get_x_0();
		VoxelTemplate_t594004422 * L_133 = ___voxelType0;
		NullCheck(L_133);
		Vector2_t2243707579 * L_134 = L_133->get_address_of_atlasScale_6();
		float L_135 = L_134->get_x_0();
		((L_129)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_x_0(((float)((float)L_132+(float)L_135)));
		Vector2U5BU5D_t686124026* L_136 = __this->get__uvs_12();
		NullCheck(L_136);
		VoxelTemplate_t594004422 * L_137 = ___voxelType0;
		NullCheck(L_137);
		Vector2_t2243707579 * L_138 = L_137->get_address_of_UVOffsetLeft_11();
		float L_139 = L_138->get_y_1();
		((L_136)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_y_1(L_139);
		goto IL_06cb;
	}

IL_0379:
	{
		Il2CppChar L_140 = ___dir1;
		if ((!(((uint32_t)L_140) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0496;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_141 = __this->get__uvs_12();
		NullCheck(L_141);
		VoxelTemplate_t594004422 * L_142 = ___voxelType0;
		NullCheck(L_142);
		Vector2_t2243707579 * L_143 = L_142->get_address_of_UVOffsetRight_12();
		float L_144 = L_143->get_x_0();
		((L_141)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_x_0(L_144);
		Vector2U5BU5D_t686124026* L_145 = __this->get__uvs_12();
		NullCheck(L_145);
		VoxelTemplate_t594004422 * L_146 = ___voxelType0;
		NullCheck(L_146);
		Vector2_t2243707579 * L_147 = L_146->get_address_of_UVOffsetRight_12();
		float L_148 = L_147->get_y_1();
		((L_145)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_y_1(L_148);
		Vector2U5BU5D_t686124026* L_149 = __this->get__uvs_12();
		NullCheck(L_149);
		VoxelTemplate_t594004422 * L_150 = ___voxelType0;
		NullCheck(L_150);
		Vector2_t2243707579 * L_151 = L_150->get_address_of_UVOffsetRight_12();
		float L_152 = L_151->get_x_0();
		((L_149)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_x_0(L_152);
		Vector2U5BU5D_t686124026* L_153 = __this->get__uvs_12();
		NullCheck(L_153);
		VoxelTemplate_t594004422 * L_154 = ___voxelType0;
		NullCheck(L_154);
		Vector2_t2243707579 * L_155 = L_154->get_address_of_UVOffsetRight_12();
		float L_156 = L_155->get_y_1();
		VoxelTemplate_t594004422 * L_157 = ___voxelType0;
		NullCheck(L_157);
		Vector2_t2243707579 * L_158 = L_157->get_address_of_atlasScale_6();
		float L_159 = L_158->get_y_1();
		((L_153)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_y_1(((float)((float)L_156+(float)L_159)));
		Vector2U5BU5D_t686124026* L_160 = __this->get__uvs_12();
		NullCheck(L_160);
		VoxelTemplate_t594004422 * L_161 = ___voxelType0;
		NullCheck(L_161);
		Vector2_t2243707579 * L_162 = L_161->get_address_of_UVOffsetRight_12();
		float L_163 = L_162->get_x_0();
		VoxelTemplate_t594004422 * L_164 = ___voxelType0;
		NullCheck(L_164);
		Vector2_t2243707579 * L_165 = L_164->get_address_of_atlasScale_6();
		float L_166 = L_165->get_x_0();
		((L_160)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_x_0(((float)((float)L_163+(float)L_166)));
		Vector2U5BU5D_t686124026* L_167 = __this->get__uvs_12();
		NullCheck(L_167);
		VoxelTemplate_t594004422 * L_168 = ___voxelType0;
		NullCheck(L_168);
		Vector2_t2243707579 * L_169 = L_168->get_address_of_UVOffsetRight_12();
		float L_170 = L_169->get_y_1();
		VoxelTemplate_t594004422 * L_171 = ___voxelType0;
		NullCheck(L_171);
		Vector2_t2243707579 * L_172 = L_171->get_address_of_atlasScale_6();
		float L_173 = L_172->get_y_1();
		((L_167)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_y_1(((float)((float)L_170+(float)L_173)));
		Vector2U5BU5D_t686124026* L_174 = __this->get__uvs_12();
		NullCheck(L_174);
		VoxelTemplate_t594004422 * L_175 = ___voxelType0;
		NullCheck(L_175);
		Vector2_t2243707579 * L_176 = L_175->get_address_of_UVOffsetRight_12();
		float L_177 = L_176->get_x_0();
		VoxelTemplate_t594004422 * L_178 = ___voxelType0;
		NullCheck(L_178);
		Vector2_t2243707579 * L_179 = L_178->get_address_of_atlasScale_6();
		float L_180 = L_179->get_x_0();
		((L_174)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_x_0(((float)((float)L_177+(float)L_180)));
		Vector2U5BU5D_t686124026* L_181 = __this->get__uvs_12();
		NullCheck(L_181);
		VoxelTemplate_t594004422 * L_182 = ___voxelType0;
		NullCheck(L_182);
		Vector2_t2243707579 * L_183 = L_182->get_address_of_UVOffsetRight_12();
		float L_184 = L_183->get_y_1();
		((L_181)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_y_1(L_184);
		goto IL_06cb;
	}

IL_0496:
	{
		Il2CppChar L_185 = ___dir1;
		if ((!(((uint32_t)L_185) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_05b3;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_186 = __this->get__uvs_12();
		NullCheck(L_186);
		VoxelTemplate_t594004422 * L_187 = ___voxelType0;
		NullCheck(L_187);
		Vector2_t2243707579 * L_188 = L_187->get_address_of_UVOffsetTop_9();
		float L_189 = L_188->get_x_0();
		((L_186)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_x_0(L_189);
		Vector2U5BU5D_t686124026* L_190 = __this->get__uvs_12();
		NullCheck(L_190);
		VoxelTemplate_t594004422 * L_191 = ___voxelType0;
		NullCheck(L_191);
		Vector2_t2243707579 * L_192 = L_191->get_address_of_UVOffsetTop_9();
		float L_193 = L_192->get_y_1();
		((L_190)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_y_1(L_193);
		Vector2U5BU5D_t686124026* L_194 = __this->get__uvs_12();
		NullCheck(L_194);
		VoxelTemplate_t594004422 * L_195 = ___voxelType0;
		NullCheck(L_195);
		Vector2_t2243707579 * L_196 = L_195->get_address_of_UVOffsetTop_9();
		float L_197 = L_196->get_x_0();
		((L_194)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_x_0(L_197);
		Vector2U5BU5D_t686124026* L_198 = __this->get__uvs_12();
		NullCheck(L_198);
		VoxelTemplate_t594004422 * L_199 = ___voxelType0;
		NullCheck(L_199);
		Vector2_t2243707579 * L_200 = L_199->get_address_of_UVOffsetTop_9();
		float L_201 = L_200->get_y_1();
		VoxelTemplate_t594004422 * L_202 = ___voxelType0;
		NullCheck(L_202);
		Vector2_t2243707579 * L_203 = L_202->get_address_of_atlasScale_6();
		float L_204 = L_203->get_y_1();
		((L_198)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_y_1(((float)((float)L_201+(float)L_204)));
		Vector2U5BU5D_t686124026* L_205 = __this->get__uvs_12();
		NullCheck(L_205);
		VoxelTemplate_t594004422 * L_206 = ___voxelType0;
		NullCheck(L_206);
		Vector2_t2243707579 * L_207 = L_206->get_address_of_UVOffsetTop_9();
		float L_208 = L_207->get_x_0();
		VoxelTemplate_t594004422 * L_209 = ___voxelType0;
		NullCheck(L_209);
		Vector2_t2243707579 * L_210 = L_209->get_address_of_atlasScale_6();
		float L_211 = L_210->get_x_0();
		((L_205)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_x_0(((float)((float)L_208+(float)L_211)));
		Vector2U5BU5D_t686124026* L_212 = __this->get__uvs_12();
		NullCheck(L_212);
		VoxelTemplate_t594004422 * L_213 = ___voxelType0;
		NullCheck(L_213);
		Vector2_t2243707579 * L_214 = L_213->get_address_of_UVOffsetTop_9();
		float L_215 = L_214->get_y_1();
		VoxelTemplate_t594004422 * L_216 = ___voxelType0;
		NullCheck(L_216);
		Vector2_t2243707579 * L_217 = L_216->get_address_of_atlasScale_6();
		float L_218 = L_217->get_y_1();
		((L_212)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_y_1(((float)((float)L_215+(float)L_218)));
		Vector2U5BU5D_t686124026* L_219 = __this->get__uvs_12();
		NullCheck(L_219);
		VoxelTemplate_t594004422 * L_220 = ___voxelType0;
		NullCheck(L_220);
		Vector2_t2243707579 * L_221 = L_220->get_address_of_UVOffsetTop_9();
		float L_222 = L_221->get_x_0();
		VoxelTemplate_t594004422 * L_223 = ___voxelType0;
		NullCheck(L_223);
		Vector2_t2243707579 * L_224 = L_223->get_address_of_atlasScale_6();
		float L_225 = L_224->get_x_0();
		((L_219)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_x_0(((float)((float)L_222+(float)L_225)));
		Vector2U5BU5D_t686124026* L_226 = __this->get__uvs_12();
		NullCheck(L_226);
		VoxelTemplate_t594004422 * L_227 = ___voxelType0;
		NullCheck(L_227);
		Vector2_t2243707579 * L_228 = L_227->get_address_of_UVOffsetTop_9();
		float L_229 = L_228->get_y_1();
		((L_226)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_y_1(L_229);
		goto IL_06cb;
	}

IL_05b3:
	{
		Il2CppChar L_230 = ___dir1;
		if ((!(((uint32_t)L_230) == ((uint32_t)((int32_t)98)))))
		{
			goto IL_06cb;
		}
	}
	{
		Vector2U5BU5D_t686124026* L_231 = __this->get__uvs_12();
		NullCheck(L_231);
		VoxelTemplate_t594004422 * L_232 = ___voxelType0;
		NullCheck(L_232);
		Vector2_t2243707579 * L_233 = L_232->get_address_of_UVOffsetBottom_10();
		float L_234 = L_233->get_x_0();
		((L_231)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_x_0(L_234);
		Vector2U5BU5D_t686124026* L_235 = __this->get__uvs_12();
		NullCheck(L_235);
		VoxelTemplate_t594004422 * L_236 = ___voxelType0;
		NullCheck(L_236);
		Vector2_t2243707579 * L_237 = L_236->get_address_of_UVOffsetBottom_10();
		float L_238 = L_237->get_y_1();
		((L_235)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->set_y_1(L_238);
		Vector2U5BU5D_t686124026* L_239 = __this->get__uvs_12();
		NullCheck(L_239);
		VoxelTemplate_t594004422 * L_240 = ___voxelType0;
		NullCheck(L_240);
		Vector2_t2243707579 * L_241 = L_240->get_address_of_UVOffsetBottom_10();
		float L_242 = L_241->get_x_0();
		((L_239)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_x_0(L_242);
		Vector2U5BU5D_t686124026* L_243 = __this->get__uvs_12();
		NullCheck(L_243);
		VoxelTemplate_t594004422 * L_244 = ___voxelType0;
		NullCheck(L_244);
		Vector2_t2243707579 * L_245 = L_244->get_address_of_UVOffsetBottom_10();
		float L_246 = L_245->get_y_1();
		VoxelTemplate_t594004422 * L_247 = ___voxelType0;
		NullCheck(L_247);
		Vector2_t2243707579 * L_248 = L_247->get_address_of_atlasScale_6();
		float L_249 = L_248->get_y_1();
		((L_243)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->set_y_1(((float)((float)L_246+(float)L_249)));
		Vector2U5BU5D_t686124026* L_250 = __this->get__uvs_12();
		NullCheck(L_250);
		VoxelTemplate_t594004422 * L_251 = ___voxelType0;
		NullCheck(L_251);
		Vector2_t2243707579 * L_252 = L_251->get_address_of_UVOffsetBottom_10();
		float L_253 = L_252->get_x_0();
		VoxelTemplate_t594004422 * L_254 = ___voxelType0;
		NullCheck(L_254);
		Vector2_t2243707579 * L_255 = L_254->get_address_of_atlasScale_6();
		float L_256 = L_255->get_x_0();
		((L_250)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_x_0(((float)((float)L_253+(float)L_256)));
		Vector2U5BU5D_t686124026* L_257 = __this->get__uvs_12();
		NullCheck(L_257);
		VoxelTemplate_t594004422 * L_258 = ___voxelType0;
		NullCheck(L_258);
		Vector2_t2243707579 * L_259 = L_258->get_address_of_UVOffsetBottom_10();
		float L_260 = L_259->get_y_1();
		VoxelTemplate_t594004422 * L_261 = ___voxelType0;
		NullCheck(L_261);
		Vector2_t2243707579 * L_262 = L_261->get_address_of_atlasScale_6();
		float L_263 = L_262->get_y_1();
		((L_257)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->set_y_1(((float)((float)L_260+(float)L_263)));
		Vector2U5BU5D_t686124026* L_264 = __this->get__uvs_12();
		NullCheck(L_264);
		VoxelTemplate_t594004422 * L_265 = ___voxelType0;
		NullCheck(L_265);
		Vector2_t2243707579 * L_266 = L_265->get_address_of_UVOffsetBottom_10();
		float L_267 = L_266->get_x_0();
		VoxelTemplate_t594004422 * L_268 = ___voxelType0;
		NullCheck(L_268);
		Vector2_t2243707579 * L_269 = L_268->get_address_of_atlasScale_6();
		float L_270 = L_269->get_x_0();
		((L_264)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_x_0(((float)((float)L_267+(float)L_270)));
		Vector2U5BU5D_t686124026* L_271 = __this->get__uvs_12();
		NullCheck(L_271);
		VoxelTemplate_t594004422 * L_272 = ___voxelType0;
		NullCheck(L_272);
		Vector2_t2243707579 * L_273 = L_272->get_address_of_UVOffsetBottom_10();
		float L_274 = L_273->get_y_1();
		((L_271)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->set_y_1(L_274);
	}

IL_06cb:
	{
		Vector2U5BU5D_t686124026* L_275 = __this->get__uvs_12();
		NullCheck(L_275);
		Vector2_t2243707579 * L_276 = ((L_275)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)));
		float L_277 = L_276->get_x_0();
		float L_278 = __this->get_ONE_PIXEL_25();
		L_276->set_x_0(((float)((float)L_277+(float)L_278)));
		Vector2U5BU5D_t686124026* L_279 = __this->get__uvs_12();
		NullCheck(L_279);
		Vector2_t2243707579 * L_280 = ((L_279)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)));
		float L_281 = L_280->get_y_1();
		float L_282 = __this->get_ONE_PIXEL_25();
		L_280->set_y_1(((float)((float)L_281+(float)L_282)));
		Vector2U5BU5D_t686124026* L_283 = __this->get__uvs_12();
		NullCheck(L_283);
		Vector2_t2243707579 * L_284 = ((L_283)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)));
		float L_285 = L_284->get_x_0();
		float L_286 = __this->get_ONE_PIXEL_25();
		L_284->set_x_0(((float)((float)L_285+(float)L_286)));
		Vector2U5BU5D_t686124026* L_287 = __this->get__uvs_12();
		NullCheck(L_287);
		Vector2_t2243707579 * L_288 = ((L_287)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)));
		float L_289 = L_288->get_y_1();
		float L_290 = __this->get_ONE_PIXEL_25();
		L_288->set_y_1(((float)((float)L_289-(float)L_290)));
		Vector2U5BU5D_t686124026* L_291 = __this->get__uvs_12();
		NullCheck(L_291);
		Vector2_t2243707579 * L_292 = ((L_291)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)));
		float L_293 = L_292->get_x_0();
		float L_294 = __this->get_ONE_PIXEL_25();
		L_292->set_x_0(((float)((float)L_293-(float)L_294)));
		Vector2U5BU5D_t686124026* L_295 = __this->get__uvs_12();
		NullCheck(L_295);
		Vector2_t2243707579 * L_296 = ((L_295)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)));
		float L_297 = L_296->get_y_1();
		float L_298 = __this->get_ONE_PIXEL_25();
		L_296->set_y_1(((float)((float)L_297-(float)L_298)));
		Vector2U5BU5D_t686124026* L_299 = __this->get__uvs_12();
		NullCheck(L_299);
		Vector2_t2243707579 * L_300 = ((L_299)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)));
		float L_301 = L_300->get_x_0();
		float L_302 = __this->get_ONE_PIXEL_25();
		L_300->set_x_0(((float)((float)L_301-(float)L_302)));
		Vector2U5BU5D_t686124026* L_303 = __this->get__uvs_12();
		NullCheck(L_303);
		Vector2_t2243707579 * L_304 = ((L_303)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)));
		float L_305 = L_304->get_y_1();
		float L_306 = __this->get_ONE_PIXEL_25();
		L_304->set_y_1(((float)((float)L_305+(float)L_306)));
		Vector2U5BU5D_t686124026* L_307 = __this->get__uvs_12();
		return L_307;
	}
}
// VoxelTemplate VoxelStructure::GetVoxelType(System.Int32)
extern "C"  VoxelTemplate_t594004422 * VoxelStructure_GetVoxelType_m63795532 (VoxelStructure_t2247263099 * __this, int32_t ___v0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___v0;
		VoxelPallette_t4284593999 * L_1 = __this->get_pallette_2();
		NullCheck(L_1);
		VoxelTemplateU5BU5D_t2866079843* L_2 = L_1->get_voxelTemplates_1();
		NullCheck(L_2);
		if ((((int32_t)L_0) >= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_2)->max_length)))))))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_3 = ___v0;
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0028;
		}
	}
	{
		VoxelPallette_t4284593999 * L_4 = __this->get_pallette_2();
		NullCheck(L_4);
		VoxelTemplateU5BU5D_t2866079843* L_5 = L_4->get_voxelTemplates_1();
		int32_t L_6 = ___v0;
		NullCheck(L_5);
		int32_t L_7 = L_6;
		VoxelTemplate_t594004422 * L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		return L_8;
	}

IL_0028:
	{
		return (VoxelTemplate_t594004422 *)NULL;
	}
}
// System.Void VoxelStructure::ClampDimensions()
extern "C"  void VoxelStructure_ClampDimensions_m3734676430 (VoxelStructure_t2247263099 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelStructure_ClampDimensions_m3734676430_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_Width_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_1 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, L_0, 0, ((int32_t)32), /*hidden argument*/NULL);
		__this->set_Width_8(L_1);
		int32_t L_2 = __this->get_Height_9();
		int32_t L_3 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, L_2, 0, ((int32_t)32), /*hidden argument*/NULL);
		__this->set_Height_9(L_3);
		int32_t L_4 = __this->get_Depth_10();
		int32_t L_5 = Mathf_Clamp_m3542052159(NULL /*static, unused*/, L_4, 0, ((int32_t)32), /*hidden argument*/NULL);
		__this->set_Depth_10(L_5);
		return;
	}
}
// System.Void VoxelStructure::.cctor()
extern "C"  void VoxelStructure__cctor_m683238421 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoxelStructure__cctor_m683238421_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((VoxelStructure_t2247263099_StaticFields*)il2cpp_codegen_static_fields_for(VoxelStructure_t2247263099_il2cpp_TypeInfo_var))->set_RMB_Action_5(1);
		((VoxelStructure_t2247263099_StaticFields*)il2cpp_codegen_static_fields_for(VoxelStructure_t2247263099_il2cpp_TypeInfo_var))->set_Static_Action_6(1);
		return;
	}
}
// System.Void VoxelTemplate::.ctor()
extern "C"  void VoxelTemplate__ctor_m160164139 (VoxelTemplate_t594004422 * __this, const RuntimeMethod* method)
{
	{
		Color_t2020392075  L_0 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color32_t874517518  L_1 = Color32_op_Implicit_m624191464(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_color_3(L_1);
		Vector2_t2243707579  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m3067419446((&L_2), (0.0625f), (0.0625f), /*hidden argument*/NULL);
		__this->set_atlasScale_6(L_2);
		__this->set_shouldDraw_13((bool)1);
		__this->set_drawFront_14((bool)1);
		__this->set_drawBack_15((bool)1);
		__this->set_drawTop_16((bool)1);
		__this->set_drawBottom_17((bool)1);
		__this->set_drawLeft_18((bool)1);
		__this->set_drawRight_19((bool)1);
		ScriptableObject__ctor_m2671490429(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WeightedOptions::.ctor()
extern "C"  void WeightedOptions__ctor_m323467996 (WeightedOptions_t481347131 * __this, const RuntimeMethod* method)
{
	{
		PairStringIntDictionary__ctor_m289840511(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean WeightedOptions::get_IsEmpty()
extern "C"  bool WeightedOptions_get_IsEmpty_m844179300 (WeightedOptions_t481347131 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get__totalWeight_2();
		return (bool)((((int32_t)0) == ((int32_t)L_0))? 1 : 0);
	}
}
// System.String WeightedOptions::Choose()
extern "C"  String_t* WeightedOptions_Choose_m1410328636 (WeightedOptions_t481347131 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WeightedOptions_Choose_m1410328636_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t3124852344  V_1;
	memset(&V_1, 0, sizeof(V_1));
	PairStringInt_t4221001538  V_2;
	memset(&V_2, 0, sizeof(V_2));
	PairStringInt_t4221001538  V_3;
	memset(&V_3, 0, sizeof(V_3));
	PairStringInt_t4221001538  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = __this->get__totalWeight_2();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_2();
		return L_1;
	}

IL_0011:
	{
		int32_t L_2 = __this->get__totalWeight_2();
		int32_t L_3 = RandomHelper_Int_m2193030995(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_2-(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_3;
		List_1_t3590122670 * L_4 = ((PairStringIntDictionary_t1323851614 *)__this)->get_Pairs_0();
		NullCheck(L_4);
		Enumerator_t3124852344  L_5 = List_1_GetEnumerator_m1313580170(L_4, /*hidden argument*/List_1_GetEnumerator_m1313580170_RuntimeMethod_var);
		V_1 = L_5;
		goto IL_005d;
	}

IL_0031:
	{
		int32_t L_6 = V_0;
		PairStringInt_t4221001538  L_7 = Enumerator_get_Current_m784304568((&V_1), /*hidden argument*/Enumerator_get_Current_m784304568_RuntimeMethod_var);
		V_2 = L_7;
		int32_t L_8 = (&V_2)->get_Value_1();
		if ((((int32_t)L_6) > ((int32_t)L_8)))
		{
			goto IL_004b;
		}
	}
	{
		goto IL_0069;
	}

IL_004b:
	{
		int32_t L_9 = V_0;
		PairStringInt_t4221001538  L_10 = Enumerator_get_Current_m784304568((&V_1), /*hidden argument*/Enumerator_get_Current_m784304568_RuntimeMethod_var);
		V_3 = L_10;
		int32_t L_11 = (&V_3)->get_Value_1();
		V_0 = ((int32_t)((int32_t)L_9-(int32_t)L_11));
	}

IL_005d:
	{
		bool L_12 = Enumerator_MoveNext_m775896506((&V_1), /*hidden argument*/Enumerator_MoveNext_m775896506_RuntimeMethod_var);
		if (L_12)
		{
			goto IL_0031;
		}
	}

IL_0069:
	{
		PairStringInt_t4221001538  L_13 = Enumerator_get_Current_m784304568((&V_1), /*hidden argument*/Enumerator_get_Current_m784304568_RuntimeMethod_var);
		V_4 = L_13;
		String_t* L_14 = (&V_4)->get_Key_0();
		return L_14;
	}
}
// System.Void WeightedOptions::Set(System.String,System.Int32)
extern "C"  void WeightedOptions_Set_m2271709975 (WeightedOptions_t481347131 * __this, String_t* ___key0, int32_t ___weight1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___weight1;
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		String_t* L_1 = ___key0;
		bool L_2 = PairStringIntDictionary_ContainsKey_m3309051453(__this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_3 = __this->get__totalWeight_2();
		String_t* L_4 = ___key0;
		int32_t L_5 = PairStringIntDictionary_get_Item_m813766539(__this, L_4, /*hidden argument*/NULL);
		__this->set__totalWeight_2(((int32_t)((int32_t)L_3-(int32_t)L_5)));
	}

IL_0027:
	{
		String_t* L_6 = ___key0;
		int32_t L_7 = ___weight1;
		PairStringIntDictionary_Set_m3793528756(__this, L_6, L_7, /*hidden argument*/NULL);
		int32_t L_8 = __this->get__totalWeight_2();
		int32_t L_9 = ___weight1;
		__this->set__totalWeight_2(((int32_t)((int32_t)L_8+(int32_t)L_9)));
	}

IL_003d:
	{
		String_t* L_10 = ___key0;
		VirtActionInvoker1< String_t* >::Invoke(8 /* System.Void PairStringIntDictionary::Remove(System.String) */, __this, L_10);
		return;
	}
}
// System.Void СhaseMover::.ctor()
extern "C"  void U421haseMover__ctor_m1224290240 (U421haseMover_t4119473191 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void СhaseMover::add_OnCatch(System.Action`2<UnityEngine.GameObject,UnityEngine.Transform>)
extern "C"  void U421haseMover_add_OnCatch_m615591784 (U421haseMover_t4119473191 * __this, Action_2_t2601038636 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U421haseMover_add_OnCatch_m615591784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_t2601038636 * V_0 = NULL;
	Action_2_t2601038636 * V_1 = NULL;
	{
		Action_2_t2601038636 * L_0 = __this->get_OnCatch_4();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_2_t2601038636 * L_1 = V_0;
		V_1 = L_1;
		Action_2_t2601038636 ** L_2 = __this->get_address_of_OnCatch_4();
		Action_2_t2601038636 * L_3 = V_1;
		Action_2_t2601038636 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Combine_m3791207084(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_2_t2601038636 * L_6 = V_0;
		Action_2_t2601038636 * L_7 = InterlockedCompareExchangeImpl<Action_2_t2601038636 *>(L_2, ((Action_2_t2601038636 *)CastclassSealed((RuntimeObject*)L_5, Action_2_t2601038636_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_2_t2601038636 * L_8 = V_0;
		Action_2_t2601038636 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_t2601038636 *)L_8) == ((RuntimeObject*)(Action_2_t2601038636 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void СhaseMover::remove_OnCatch(System.Action`2<UnityEngine.GameObject,UnityEngine.Transform>)
extern "C"  void U421haseMover_remove_OnCatch_m2286981733 (U421haseMover_t4119473191 * __this, Action_2_t2601038636 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U421haseMover_remove_OnCatch_m2286981733_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_2_t2601038636 * V_0 = NULL;
	Action_2_t2601038636 * V_1 = NULL;
	{
		Action_2_t2601038636 * L_0 = __this->get_OnCatch_4();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_2_t2601038636 * L_1 = V_0;
		V_1 = L_1;
		Action_2_t2601038636 ** L_2 = __this->get_address_of_OnCatch_4();
		Action_2_t2601038636 * L_3 = V_1;
		Action_2_t2601038636 * L_4 = ___value0;
		Delegate_t3022476291 * L_5 = Delegate_Remove_m2626518725(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_2_t2601038636 * L_6 = V_0;
		Action_2_t2601038636 * L_7 = InterlockedCompareExchangeImpl<Action_2_t2601038636 *>(L_2, ((Action_2_t2601038636 *)CastclassSealed((RuntimeObject*)L_5, Action_2_t2601038636_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_2_t2601038636 * L_8 = V_0;
		Action_2_t2601038636 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_2_t2601038636 *)L_8) == ((RuntimeObject*)(Action_2_t2601038636 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void СhaseMover::Catch()
extern "C"  void U421haseMover_Catch_m3551204615 (U421haseMover_t4119473191 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U421haseMover_Catch_m3551204615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_2_t2601038636 * L_0 = __this->get_OnCatch_4();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		Action_2_t2601038636 * L_1 = __this->get_OnCatch_4();
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = __this->get__destination_2();
		NullCheck(L_1);
		Action_2_Invoke_m2336330442(L_1, L_2, L_3, /*hidden argument*/Action_2_Invoke_m2336330442_RuntimeMethod_var);
	}

IL_0022:
	{
		__this->set_OnCatch_4((Action_2_t2601038636 *)NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void СhaseMover::Update()
extern "C"  void U421haseMover_Update_m982869159 (U421haseMover_t4119473191 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U421haseMover_Update_m982869159_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		Transform_t3275118058 * L_0 = __this->get__destination_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		U421haseMover_Catch_m3551204615(__this, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		Transform_t3275118058 * L_2 = __this->get__destination_2();
		NullCheck(L_2);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2243707580_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_6 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector3_t2243707580  L_7 = V_0;
		Vector3_t2243707580  L_8 = Vector3_Normalize_m2140428981(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = L_9;
		NullCheck(L_10);
		Vector3_t2243707580  L_11 = Transform_get_position_m1104419803(L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = V_1;
		float L_13 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_14 = __this->get__speed_3();
		Vector3_t2243707580  L_15 = Vector3_op_Multiply_m1351554733(NULL /*static, unused*/, L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		Vector3_t2243707580  L_16 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_11, L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_m2469242620(L_10, L_16, /*hidden argument*/NULL);
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t2243707580  L_18 = Transform_get_position_m1104419803(L_17, /*hidden argument*/NULL);
		Transform_t3275118058 * L_19 = __this->get__destination_2();
		NullCheck(L_19);
		Vector3_t2243707580  L_20 = Transform_get_position_m1104419803(L_19, /*hidden argument*/NULL);
		float L_21 = Vector3_Distance_m1859670022(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		float L_22 = V_2;
		if ((!(((float)L_22) < ((float)(0.2f)))))
		{
			goto IL_0090;
		}
	}
	{
		U421haseMover_Catch_m3551204615(__this, /*hidden argument*/NULL);
	}

IL_0090:
	{
		return;
	}
}
// СhaseMover СhaseMover::Create(UnityEngine.GameObject,UnityEngine.Transform,System.Single)
extern "C"  U421haseMover_t4119473191 * U421haseMover_Create_m3555304436 (RuntimeObject * __this /* static, unused */, GameObject_t1756533147 * ___moved0, Transform_t3275118058 * ___destination1, float ___speed2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U421haseMover_Create_m3555304436_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U421haseMover_t4119473191 * V_0 = NULL;
	{
		GameObject_t1756533147 * L_0 = ___moved0;
		NullCheck(L_0);
		U421haseMover_t4119473191 * L_1 = GameObject_AddComponent_TisU421haseMover_t4119473191_m1934158225(L_0, /*hidden argument*/GameObject_AddComponent_TisU421haseMover_t4119473191_m1934158225_RuntimeMethod_var);
		V_0 = L_1;
		U421haseMover_t4119473191 * L_2 = V_0;
		Transform_t3275118058 * L_3 = ___destination1;
		NullCheck(L_2);
		L_2->set__destination_2(L_3);
		U421haseMover_t4119473191 * L_4 = V_0;
		float L_5 = ___speed2;
		NullCheck(L_4);
		L_4->set__speed_3(L_5);
		U421haseMover_t4119473191 * L_6 = V_0;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
